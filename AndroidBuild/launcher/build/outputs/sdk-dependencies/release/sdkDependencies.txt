# List of SDK dependencies of this app, this information is also included in an encrypted form in the APK.
# For more information visit: https://d.android.com/r/tools/dependency-metadata

library {
  maven_library {
    artifactId: "OVRPlugin"
  }
  digests {
    sha256: "\003\037\302\245Sv_m\027\253\205\206\365G\213sj\212\000HU*\027qX\367Y\206\n\031\327\250"
  }
}
library {
  digests {
    sha256: "\037r\024k\021K#\223QV\3027\235\th\030\344*\326\257\000\006\364\213\220\004\004\366\036\226*\244"
  }
}
module_dependencies {
  module_name: "base"
  dependency_index: 0
  dependency_index: 1
}
repositories {
  maven_repo {
    url: "https://dl.google.com/dl/android/maven2/"
  }
}
repositories {
  maven_repo {
    url: "https://jcenter.bintray.com/"
  }
}
