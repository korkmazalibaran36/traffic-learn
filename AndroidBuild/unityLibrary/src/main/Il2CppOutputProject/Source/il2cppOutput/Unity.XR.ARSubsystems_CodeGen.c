﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 UnityEngine.InputSystem.Controls.Vector3Control UnityEngine.XR.ARSubsystems.HandheldARInputDevice::get_devicePosition()
extern void HandheldARInputDevice_get_devicePosition_m0091E3256BC6CD9F1D17C13123CC7628EF89E707 (void);
// 0x00000002 System.Void UnityEngine.XR.ARSubsystems.HandheldARInputDevice::set_devicePosition(UnityEngine.InputSystem.Controls.Vector3Control)
extern void HandheldARInputDevice_set_devicePosition_m9A84C602F24A12A7D4BE2F30A370BD822B432902 (void);
// 0x00000003 UnityEngine.InputSystem.Controls.QuaternionControl UnityEngine.XR.ARSubsystems.HandheldARInputDevice::get_deviceRotation()
extern void HandheldARInputDevice_get_deviceRotation_m832809682CA615E9D2F315C6F38D3B5E170CC458 (void);
// 0x00000004 System.Void UnityEngine.XR.ARSubsystems.HandheldARInputDevice::set_deviceRotation(UnityEngine.InputSystem.Controls.QuaternionControl)
extern void HandheldARInputDevice_set_deviceRotation_mE17BE1068A69C53B631038C04DBB53233CFFAA9A (void);
// 0x00000005 System.Void UnityEngine.XR.ARSubsystems.HandheldARInputDevice::FinishSetup()
extern void HandheldARInputDevice_FinishSetup_m218149DF049917553E3080152EC7B1FCFB11876B (void);
// 0x00000006 System.Void UnityEngine.XR.ARSubsystems.HandheldARInputDevice::.ctor()
extern void HandheldARInputDevice__ctor_m20340276B139CF2CF798FF001DEB79D95FC433F8 (void);
static Il2CppMethodPointer s_methodPointers[6] = 
{
	HandheldARInputDevice_get_devicePosition_m0091E3256BC6CD9F1D17C13123CC7628EF89E707,
	HandheldARInputDevice_set_devicePosition_m9A84C602F24A12A7D4BE2F30A370BD822B432902,
	HandheldARInputDevice_get_deviceRotation_m832809682CA615E9D2F315C6F38D3B5E170CC458,
	HandheldARInputDevice_set_deviceRotation_mE17BE1068A69C53B631038C04DBB53233CFFAA9A,
	HandheldARInputDevice_FinishSetup_m218149DF049917553E3080152EC7B1FCFB11876B,
	HandheldARInputDevice__ctor_m20340276B139CF2CF798FF001DEB79D95FC433F8,
};
static const int32_t s_InvokerIndices[6] = 
{
	3159,
	2500,
	3159,
	2500,
	3220,
	3220,
};
extern const CustomAttributesCacheGenerator g_Unity_XR_ARSubsystems_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_Unity_XR_ARSubsystems_CodeGenModule;
const Il2CppCodeGenModule g_Unity_XR_ARSubsystems_CodeGenModule = 
{
	"Unity.XR.ARSubsystems.dll",
	6,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_Unity_XR_ARSubsystems_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
