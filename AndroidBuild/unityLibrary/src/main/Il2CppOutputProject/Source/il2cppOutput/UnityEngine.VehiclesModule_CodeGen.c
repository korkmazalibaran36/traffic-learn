﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Single UnityEngine.WheelCollider::get_radius()
extern void WheelCollider_get_radius_mDB04C14DAB0BB1D5D7A2D160C5EE3F159CE3FA26 (void);
// 0x00000002 UnityEngine.WheelFrictionCurve UnityEngine.WheelCollider::get_sidewaysFriction()
extern void WheelCollider_get_sidewaysFriction_m85AA645570CAC61DF6BC5F9B8F70409A877F7DFE (void);
// 0x00000003 System.Void UnityEngine.WheelCollider::set_sidewaysFriction(UnityEngine.WheelFrictionCurve)
extern void WheelCollider_set_sidewaysFriction_mEE0EFD1DFE53A0FA023F8D1E05840A58D69784C2 (void);
// 0x00000004 System.Single UnityEngine.WheelCollider::get_motorTorque()
extern void WheelCollider_get_motorTorque_m9FE89553A15D2CB9ED9A13074F2914557D223B18 (void);
// 0x00000005 System.Void UnityEngine.WheelCollider::set_motorTorque(System.Single)
extern void WheelCollider_set_motorTorque_mFE7962DF8003D10BA646545E56F0A6B3ED8803DA (void);
// 0x00000006 System.Void UnityEngine.WheelCollider::set_brakeTorque(System.Single)
extern void WheelCollider_set_brakeTorque_mDAFB1032B6B3AD4C8103869D64807596774C1B54 (void);
// 0x00000007 System.Single UnityEngine.WheelCollider::get_steerAngle()
extern void WheelCollider_get_steerAngle_mE954E7540E1ACDE5F5FE0D3A32388F6A22378CF4 (void);
// 0x00000008 System.Void UnityEngine.WheelCollider::set_steerAngle(System.Single)
extern void WheelCollider_set_steerAngle_m1E46602E8B77EB747B1EA44D84B5EC99F86BB968 (void);
// 0x00000009 System.Single UnityEngine.WheelCollider::get_rpm()
extern void WheelCollider_get_rpm_m5127D5BE0A627C29F38CFD8194C3F2470D8008B1 (void);
// 0x0000000A System.Void UnityEngine.WheelCollider::GetWorldPose(UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern void WheelCollider_GetWorldPose_m227D45061C7734F3ED4A43B7F89605A398BE8BB5 (void);
// 0x0000000B System.Void UnityEngine.WheelCollider::get_sidewaysFriction_Injected(UnityEngine.WheelFrictionCurve&)
extern void WheelCollider_get_sidewaysFriction_Injected_m8EAA3E001BB706E9476147E89575AE4342CD636F (void);
// 0x0000000C System.Void UnityEngine.WheelCollider::set_sidewaysFriction_Injected(UnityEngine.WheelFrictionCurve&)
extern void WheelCollider_set_sidewaysFriction_Injected_m57B33FE3F12D497BE7A050EBE8AA1059954626BA (void);
static Il2CppMethodPointer s_methodPointers[12] = 
{
	WheelCollider_get_radius_mDB04C14DAB0BB1D5D7A2D160C5EE3F159CE3FA26,
	WheelCollider_get_sidewaysFriction_m85AA645570CAC61DF6BC5F9B8F70409A877F7DFE,
	WheelCollider_set_sidewaysFriction_mEE0EFD1DFE53A0FA023F8D1E05840A58D69784C2,
	WheelCollider_get_motorTorque_m9FE89553A15D2CB9ED9A13074F2914557D223B18,
	WheelCollider_set_motorTorque_mFE7962DF8003D10BA646545E56F0A6B3ED8803DA,
	WheelCollider_set_brakeTorque_mDAFB1032B6B3AD4C8103869D64807596774C1B54,
	WheelCollider_get_steerAngle_mE954E7540E1ACDE5F5FE0D3A32388F6A22378CF4,
	WheelCollider_set_steerAngle_m1E46602E8B77EB747B1EA44D84B5EC99F86BB968,
	WheelCollider_get_rpm_m5127D5BE0A627C29F38CFD8194C3F2470D8008B1,
	WheelCollider_GetWorldPose_m227D45061C7734F3ED4A43B7F89605A398BE8BB5,
	WheelCollider_get_sidewaysFriction_Injected_m8EAA3E001BB706E9476147E89575AE4342CD636F,
	WheelCollider_set_sidewaysFriction_Injected_m57B33FE3F12D497BE7A050EBE8AA1059954626BA,
};
static const int32_t s_InvokerIndices[12] = 
{
	3192,
	3222,
	2552,
	3192,
	2525,
	2525,
	3192,
	2525,
	3192,
	1138,
	2428,
	2428,
};
extern const CustomAttributesCacheGenerator g_UnityEngine_VehiclesModule_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_UnityEngine_VehiclesModule_CodeGenModule;
const Il2CppCodeGenModule g_UnityEngine_VehiclesModule_CodeGenModule = 
{
	"UnityEngine.VehiclesModule.dll",
	12,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_UnityEngine_VehiclesModule_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
