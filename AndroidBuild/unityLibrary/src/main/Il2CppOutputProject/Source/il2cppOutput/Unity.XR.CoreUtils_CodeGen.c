﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void Microsoft.CodeAnalysis.EmbeddedAttribute::.ctor()
extern void EmbeddedAttribute__ctor_m397CF6D74070B39A844CEF4BC9846AF5DC8423B9 (void);
// 0x00000002 System.Void System.Runtime.CompilerServices.IsReadOnlyAttribute::.ctor()
extern void IsReadOnlyAttribute__ctor_m40764A40A4145B9996BC9AECBAD2C3BCE27FA65D (void);
// 0x00000003 Unity.XR.CoreUtils.XROrigin Unity.XR.CoreUtils.ARTrackablesParentTransformChangedEventArgs::get_Origin()
extern void ARTrackablesParentTransformChangedEventArgs_get_Origin_m635FF91C20A6D6C0514EABDDED14289D01BDC0A5 (void);
// 0x00000004 UnityEngine.Transform Unity.XR.CoreUtils.ARTrackablesParentTransformChangedEventArgs::get_TrackablesParent()
extern void ARTrackablesParentTransformChangedEventArgs_get_TrackablesParent_mC93ACA08A0A5E3F00A58F63EF932BD6AA768BA43 (void);
// 0x00000005 System.Void Unity.XR.CoreUtils.ARTrackablesParentTransformChangedEventArgs::.ctor(Unity.XR.CoreUtils.XROrigin,UnityEngine.Transform)
extern void ARTrackablesParentTransformChangedEventArgs__ctor_m90A574F56EAF0228BD1D7CBFDAF682B8E58F7F9D (void);
// 0x00000006 System.Boolean Unity.XR.CoreUtils.ARTrackablesParentTransformChangedEventArgs::Equals(Unity.XR.CoreUtils.ARTrackablesParentTransformChangedEventArgs)
extern void ARTrackablesParentTransformChangedEventArgs_Equals_mDEA555E578C8B4ED1C2EB86EC70E289D6A542DEE (void);
// 0x00000007 System.Boolean Unity.XR.CoreUtils.ARTrackablesParentTransformChangedEventArgs::Equals(System.Object)
extern void ARTrackablesParentTransformChangedEventArgs_Equals_mD1E41855005B1E2CF7834216ECA32621B6229D53 (void);
// 0x00000008 System.Int32 Unity.XR.CoreUtils.ARTrackablesParentTransformChangedEventArgs::GetHashCode()
extern void ARTrackablesParentTransformChangedEventArgs_GetHashCode_mDBE0D34111420DBF1A446062AD324FCDE522B51A (void);
// 0x00000009 System.Boolean Unity.XR.CoreUtils.ARTrackablesParentTransformChangedEventArgs::op_Equality(Unity.XR.CoreUtils.ARTrackablesParentTransformChangedEventArgs,Unity.XR.CoreUtils.ARTrackablesParentTransformChangedEventArgs)
extern void ARTrackablesParentTransformChangedEventArgs_op_Equality_m1B1D17C67E606DCEC56538D3DDD56BCD79B34FEC (void);
// 0x0000000A System.Boolean Unity.XR.CoreUtils.ARTrackablesParentTransformChangedEventArgs::op_Inequality(Unity.XR.CoreUtils.ARTrackablesParentTransformChangedEventArgs,Unity.XR.CoreUtils.ARTrackablesParentTransformChangedEventArgs)
extern void ARTrackablesParentTransformChangedEventArgs_op_Inequality_mA180733C851949CFBD6ACE2AB80F9D9BAA2FD96F (void);
// 0x0000000B System.Void Unity.XR.CoreUtils.ReadOnlyAttribute::.ctor()
extern void ReadOnlyAttribute__ctor_mA7EDE36B78291002E0121E5FBA2903A4F725F50F (void);
// 0x0000000C System.String Unity.XR.CoreUtils.ScriptableSettingsPathAttribute::get_Path()
extern void ScriptableSettingsPathAttribute_get_Path_m76F5FE1AB4A5E0CE163CF1C7F0F7140A2B63B078 (void);
// 0x0000000D System.Void Unity.XR.CoreUtils.ScriptableSettingsPathAttribute::.ctor(System.String)
extern void ScriptableSettingsPathAttribute__ctor_m46C282885F5C0DB52545F16B4D191701AAC56EC3 (void);
// 0x0000000E UnityEngine.Bounds Unity.XR.CoreUtils.BoundsUtils::GetBounds(System.Collections.Generic.List`1<UnityEngine.GameObject>)
extern void BoundsUtils_GetBounds_mD3B6DFDC18760F87DE0ED175D55B104AC2BB232C (void);
// 0x0000000F UnityEngine.Bounds Unity.XR.CoreUtils.BoundsUtils::GetBounds(UnityEngine.Transform[])
extern void BoundsUtils_GetBounds_m5EEDAB2D057C0B8FCF9DE74755ADCAD7418E157F (void);
// 0x00000010 UnityEngine.Bounds Unity.XR.CoreUtils.BoundsUtils::GetBounds(UnityEngine.Transform)
extern void BoundsUtils_GetBounds_m540953796CFC376F9C3AF2239FFF137F4858039A (void);
// 0x00000011 UnityEngine.Bounds Unity.XR.CoreUtils.BoundsUtils::GetBounds(System.Collections.Generic.List`1<UnityEngine.Renderer>)
extern void BoundsUtils_GetBounds_m9BD787B76BE78CAF17781487A0EDFB9DB6CC8E9F (void);
// 0x00000012 UnityEngine.Bounds Unity.XR.CoreUtils.BoundsUtils::GetBounds(System.Collections.Generic.List`1<T>)
// 0x00000013 UnityEngine.Bounds Unity.XR.CoreUtils.BoundsUtils::GetBounds(System.Collections.Generic.List`1<UnityEngine.Vector3>)
extern void BoundsUtils_GetBounds_m6C8E00F314F0FA1162C9AFF1F07799406F815FEC (void);
// 0x00000014 System.Void Unity.XR.CoreUtils.BoundsUtils::.cctor()
extern void BoundsUtils__cctor_mAD7FB5706D882279D0DA048C2045536875B5140A (void);
// 0x00000015 THostType[] Unity.XR.CoreUtils.IComponentHost`1::get_HostedComponents()
// 0x00000016 System.Void Unity.XR.CoreUtils.CachedComponentFilter`2::.ctor(TRootType,Unity.XR.CoreUtils.CachedSearchType,System.Boolean)
// 0x00000017 System.Void Unity.XR.CoreUtils.CachedComponentFilter`2::.ctor(TFilterType[],System.Boolean)
// 0x00000018 System.Void Unity.XR.CoreUtils.CachedComponentFilter`2::StoreMatchingComponents(System.Collections.Generic.List`1<TChildType>)
// 0x00000019 TChildType[] Unity.XR.CoreUtils.CachedComponentFilter`2::GetMatchingComponents()
// 0x0000001A System.Void Unity.XR.CoreUtils.CachedComponentFilter`2::FilteredCopyToMaster(System.Boolean)
// 0x0000001B System.Void Unity.XR.CoreUtils.CachedComponentFilter`2::FilteredCopyToMaster(System.Boolean,TRootType)
// 0x0000001C System.Void Unity.XR.CoreUtils.CachedComponentFilter`2::Dispose(System.Boolean)
// 0x0000001D System.Void Unity.XR.CoreUtils.CachedComponentFilter`2::Dispose()
// 0x0000001E System.Void Unity.XR.CoreUtils.CachedComponentFilter`2::.cctor()
// 0x0000001F TCollection Unity.XR.CoreUtils.CollectionPool`2::GetCollection()
// 0x00000020 System.Void Unity.XR.CoreUtils.CollectionPool`2::RecycleCollection(TCollection)
// 0x00000021 System.Void Unity.XR.CoreUtils.CollectionPool`2::.cctor()
// 0x00000022 T Unity.XR.CoreUtils.ComponentUtils`1::GetComponent(UnityEngine.GameObject)
// 0x00000023 T Unity.XR.CoreUtils.ComponentUtils`1::GetComponentInChildren(UnityEngine.GameObject)
// 0x00000024 System.Void Unity.XR.CoreUtils.ComponentUtils`1::.cctor()
// 0x00000025 T Unity.XR.CoreUtils.ComponentUtils::GetOrAddIf(UnityEngine.GameObject,System.Boolean)
// 0x00000026 System.Void Unity.XR.CoreUtils.EnumValues`1::.cctor()
// 0x00000027 System.Boolean Unity.XR.CoreUtils.BoundsExtensions::ContainsCompletely(UnityEngine.Bounds,UnityEngine.Bounds)
extern void BoundsExtensions_ContainsCompletely_mEA22CADCF3881A4A4219DDE687D9A2F8F07DD847 (void);
// 0x00000028 System.Single Unity.XR.CoreUtils.CameraExtensions::GetVerticalFieldOfView(UnityEngine.Camera,System.Single)
extern void CameraExtensions_GetVerticalFieldOfView_m101064D5B729B433C1FCDF9F6FE3117E8ADD3F53 (void);
// 0x00000029 System.Single Unity.XR.CoreUtils.CameraExtensions::GetHorizontalFieldOfView(UnityEngine.Camera)
extern void CameraExtensions_GetHorizontalFieldOfView_m49C48EFE352CB2A8E0E6E7E6506552634A8C7378 (void);
// 0x0000002A System.Single Unity.XR.CoreUtils.CameraExtensions::GetVerticalOrthographicSize(UnityEngine.Camera,System.Single)
extern void CameraExtensions_GetVerticalOrthographicSize_mE059E1B5A380CCE22C1BE172540820B473DC0653 (void);
// 0x0000002B System.String Unity.XR.CoreUtils.CollectionExtensions::Stringify(System.Collections.Generic.ICollection`1<T>)
// 0x0000002C System.Void Unity.XR.CoreUtils.CollectionExtensions::.cctor()
extern void CollectionExtensions__cctor_m1AC43AE23FEEA7AF9A7FBBBE0473FD73BCBD2FD9 (void);
// 0x0000002D System.Collections.Generic.KeyValuePair`2<TKey,TValue> Unity.XR.CoreUtils.DictionaryExtensions::First(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// 0x0000002E System.Void Unity.XR.CoreUtils.GameObjectExtensions::SetHideFlagsRecursively(UnityEngine.GameObject,UnityEngine.HideFlags)
extern void GameObjectExtensions_SetHideFlagsRecursively_m6F00615C8F62D206E47AFCA0FB5C5FA5B24469E4 (void);
// 0x0000002F System.Void Unity.XR.CoreUtils.GameObjectExtensions::AddToHideFlagsRecursively(UnityEngine.GameObject,UnityEngine.HideFlags)
extern void GameObjectExtensions_AddToHideFlagsRecursively_m09365FE52EFEFC1785D48F8737DE51E210A2C02E (void);
// 0x00000030 System.Void Unity.XR.CoreUtils.GameObjectExtensions::SetLayerRecursively(UnityEngine.GameObject,System.Int32)
extern void GameObjectExtensions_SetLayerRecursively_mDF83662C1D71E162BEA49C042C3F6AA6BF62B781 (void);
// 0x00000031 System.Void Unity.XR.CoreUtils.GameObjectExtensions::SetLayerAndAddToHideFlagsRecursively(UnityEngine.GameObject,System.Int32,UnityEngine.HideFlags)
extern void GameObjectExtensions_SetLayerAndAddToHideFlagsRecursively_m9203559713DAD72AA9445A94BF1061434B25CFFB (void);
// 0x00000032 System.Void Unity.XR.CoreUtils.GameObjectExtensions::SetLayerAndHideFlagsRecursively(UnityEngine.GameObject,System.Int32,UnityEngine.HideFlags)
extern void GameObjectExtensions_SetLayerAndHideFlagsRecursively_mBED51B647E27A9A86E48FC5958D782D2B9A709C3 (void);
// 0x00000033 System.Void Unity.XR.CoreUtils.GameObjectExtensions::SetRunInEditModeRecursively(UnityEngine.GameObject,System.Boolean)
extern void GameObjectExtensions_SetRunInEditModeRecursively_m04A08BEF612D58DE53C4DE3F8928764BFD747BF9 (void);
// 0x00000034 System.Void Unity.XR.CoreUtils.GuidExtensions::Decompose(System.Guid,System.UInt64&,System.UInt64&)
extern void GuidExtensions_Decompose_m852CCE666BC2C41B30B344DE8ACCE9F562056E3F (void);
// 0x00000035 System.Void Unity.XR.CoreUtils.HashSetExtensions::ExceptWithNonAlloc(System.Collections.Generic.HashSet`1<T>,System.Collections.Generic.HashSet`1<T>)
// 0x00000036 T Unity.XR.CoreUtils.HashSetExtensions::First(System.Collections.Generic.HashSet`1<T>)
// 0x00000037 System.Int32 Unity.XR.CoreUtils.LayerMaskExtensions::GetFirstLayerIndex(UnityEngine.LayerMask)
extern void LayerMaskExtensions_GetFirstLayerIndex_m59E2969145DDC43B4795653FA9343E3B0AB22DBE (void);
// 0x00000038 System.Boolean Unity.XR.CoreUtils.LayerMaskExtensions::Contains(UnityEngine.LayerMask,System.Int32)
extern void LayerMaskExtensions_Contains_mD73CA4A5FA715E24E4E5149BD99554C533895E36 (void);
// 0x00000039 System.Collections.Generic.List`1<T> Unity.XR.CoreUtils.ListExtensions::Fill(System.Collections.Generic.List`1<T>,System.Int32)
// 0x0000003A System.Void Unity.XR.CoreUtils.ListExtensions::EnsureCapacity(System.Collections.Generic.List`1<T>,System.Int32)
// 0x0000003B System.Void Unity.XR.CoreUtils.ListExtensions::SwapAtIndices(System.Collections.Generic.List`1<T>,System.Int32,System.Int32)
// 0x0000003C UnityEngine.Pose Unity.XR.CoreUtils.PoseExtensions::ApplyOffsetTo(UnityEngine.Pose,UnityEngine.Pose)
extern void PoseExtensions_ApplyOffsetTo_m8DC685DEE61A5BF96281BEEC786624046BF20187 (void);
// 0x0000003D UnityEngine.Vector3 Unity.XR.CoreUtils.PoseExtensions::ApplyOffsetTo(UnityEngine.Pose,UnityEngine.Vector3)
extern void PoseExtensions_ApplyOffsetTo_m74E9139469883F2C36F4F0F9C0BC249EE87D929F (void);
// 0x0000003E UnityEngine.Vector3 Unity.XR.CoreUtils.PoseExtensions::ApplyInverseOffsetTo(UnityEngine.Pose,UnityEngine.Vector3)
extern void PoseExtensions_ApplyInverseOffsetTo_m90414AC45AA5E572388BA8CC6A36B3D591ABACFF (void);
// 0x0000003F UnityEngine.Quaternion Unity.XR.CoreUtils.QuaternionExtensions::ConstrainYaw(UnityEngine.Quaternion)
extern void QuaternionExtensions_ConstrainYaw_m38F5AC58FD533B1A546DDF12F92805DC29D2E4F7 (void);
// 0x00000040 UnityEngine.Quaternion Unity.XR.CoreUtils.QuaternionExtensions::ConstrainYawNormalized(UnityEngine.Quaternion)
extern void QuaternionExtensions_ConstrainYawNormalized_mAA17E485823DBFF827A210FED067BA1D312A4818 (void);
// 0x00000041 UnityEngine.Quaternion Unity.XR.CoreUtils.QuaternionExtensions::ConstrainYawPitchNormalized(UnityEngine.Quaternion)
extern void QuaternionExtensions_ConstrainYawPitchNormalized_m8FBC20086FA9EA65AC223835873868C01A7F8D3F (void);
// 0x00000042 System.Void Unity.XR.CoreUtils.StopwatchExtensions::Restart(System.Diagnostics.Stopwatch)
extern void StopwatchExtensions_Restart_m0AE76CC92E6248DE3B1184F1517B9D0DF933985B (void);
// 0x00000043 System.String Unity.XR.CoreUtils.StringExtensions::FirstToUpper(System.String)
extern void StringExtensions_FirstToUpper_m071E8AAD165D7ED39F24053F16F8472E03AB63B8 (void);
// 0x00000044 System.String Unity.XR.CoreUtils.StringExtensions::InsertSpacesBetweenWords(System.String)
extern void StringExtensions_InsertSpacesBetweenWords_m26A802473A54EB1EEDDCD45BD26DCE9125FF113A (void);
// 0x00000045 System.Void Unity.XR.CoreUtils.StringExtensions::.cctor()
extern void StringExtensions__cctor_m16B58C6C89089FDD3B263B05FDBE532A45CE9FBF (void);
// 0x00000046 UnityEngine.Pose Unity.XR.CoreUtils.TransformExtensions::GetLocalPose(UnityEngine.Transform)
extern void TransformExtensions_GetLocalPose_mAC44DDBB372B5426A776ADF219FA89277DFD87EC (void);
// 0x00000047 UnityEngine.Pose Unity.XR.CoreUtils.TransformExtensions::GetWorldPose(UnityEngine.Transform)
extern void TransformExtensions_GetWorldPose_m33A1A882B998C9949D1C6424846E60D55B5AEF45 (void);
// 0x00000048 System.Void Unity.XR.CoreUtils.TransformExtensions::SetLocalPose(UnityEngine.Transform,UnityEngine.Pose)
extern void TransformExtensions_SetLocalPose_mB910345A162B1D213157129920771121086411EB (void);
// 0x00000049 System.Void Unity.XR.CoreUtils.TransformExtensions::SetWorldPose(UnityEngine.Transform,UnityEngine.Pose)
extern void TransformExtensions_SetWorldPose_mA1EDD07D363DBEB6E9F282ECBACBFAD7407E7CF2 (void);
// 0x0000004A UnityEngine.Pose Unity.XR.CoreUtils.TransformExtensions::TransformPose(UnityEngine.Transform,UnityEngine.Pose)
extern void TransformExtensions_TransformPose_mEB3D81D8D439A0CB8C08B791A180B78542978AAA (void);
// 0x0000004B UnityEngine.Pose Unity.XR.CoreUtils.TransformExtensions::InverseTransformPose(UnityEngine.Transform,UnityEngine.Pose)
extern void TransformExtensions_InverseTransformPose_m4032C88611F08379DFA1AD41EBC09B0578B70BB4 (void);
// 0x0000004C UnityEngine.Ray Unity.XR.CoreUtils.TransformExtensions::InverseTransformRay(UnityEngine.Transform,UnityEngine.Ray)
extern void TransformExtensions_InverseTransformRay_m8C41EB5E94CB9B39339B90E8B8A69435AC944939 (void);
// 0x0000004D System.Void Unity.XR.CoreUtils.TypeExtensions::GetAssignableTypes(System.Type,System.Collections.Generic.List`1<System.Type>,System.Func`2<System.Type,System.Boolean>)
extern void TypeExtensions_GetAssignableTypes_m0F6C4B24986632C28BCA0C533B95D20E9DB2BE24 (void);
// 0x0000004E System.Void Unity.XR.CoreUtils.TypeExtensions::GetImplementationsOfInterface(System.Type,System.Collections.Generic.List`1<System.Type>)
extern void TypeExtensions_GetImplementationsOfInterface_mB129015D4AC213C5B417DB294CB35EC606F98D8D (void);
// 0x0000004F System.Void Unity.XR.CoreUtils.TypeExtensions::GetExtensionsOfClass(System.Type,System.Collections.Generic.List`1<System.Type>)
extern void TypeExtensions_GetExtensionsOfClass_m15E6CD161BC60995D99459F2E18794AD175CE34F (void);
// 0x00000050 System.Void Unity.XR.CoreUtils.TypeExtensions::GetGenericInterfaces(System.Type,System.Type,System.Collections.Generic.List`1<System.Type>)
extern void TypeExtensions_GetGenericInterfaces_m7CF723B51E93B293EF201CC8F239F3B205273E49 (void);
// 0x00000051 System.Reflection.PropertyInfo Unity.XR.CoreUtils.TypeExtensions::GetPropertyRecursively(System.Type,System.String,System.Reflection.BindingFlags)
extern void TypeExtensions_GetPropertyRecursively_m00E9156D1FD3407A6047A7069E4BB8BC17C10447 (void);
// 0x00000052 System.Reflection.FieldInfo Unity.XR.CoreUtils.TypeExtensions::GetFieldRecursively(System.Type,System.String,System.Reflection.BindingFlags)
extern void TypeExtensions_GetFieldRecursively_m4B31CBD9AB4D628DC336F68FF8E270510B13BD93 (void);
// 0x00000053 System.Void Unity.XR.CoreUtils.TypeExtensions::GetFieldsRecursively(System.Type,System.Collections.Generic.List`1<System.Reflection.FieldInfo>,System.Reflection.BindingFlags)
extern void TypeExtensions_GetFieldsRecursively_m7C09A508D81F407687C61B96AB8264CD5E8BD1B3 (void);
// 0x00000054 System.Void Unity.XR.CoreUtils.TypeExtensions::GetPropertiesRecursively(System.Type,System.Collections.Generic.List`1<System.Reflection.PropertyInfo>,System.Reflection.BindingFlags)
extern void TypeExtensions_GetPropertiesRecursively_mD708E69AA5F2D0DB41EB62BEFFD76E0DCCA64256 (void);
// 0x00000055 System.Void Unity.XR.CoreUtils.TypeExtensions::GetInterfaceFieldsFromClasses(System.Collections.Generic.IEnumerable`1<System.Type>,System.Collections.Generic.List`1<System.Reflection.FieldInfo>,System.Collections.Generic.List`1<System.Type>,System.Reflection.BindingFlags)
extern void TypeExtensions_GetInterfaceFieldsFromClasses_mDA187CB5312C43811477DF73AE680DEE9096823B (void);
// 0x00000056 TAttribute Unity.XR.CoreUtils.TypeExtensions::GetAttribute(System.Type,System.Boolean)
// 0x00000057 System.Void Unity.XR.CoreUtils.TypeExtensions::IsDefinedGetInheritedTypes(System.Type,System.Collections.Generic.List`1<System.Type>)
// 0x00000058 System.Reflection.FieldInfo Unity.XR.CoreUtils.TypeExtensions::GetFieldInTypeOrBaseType(System.Type,System.String)
extern void TypeExtensions_GetFieldInTypeOrBaseType_m17260C2C93FED7BC94A5AF9258C874F6A614700C (void);
// 0x00000059 System.String Unity.XR.CoreUtils.TypeExtensions::GetNameWithGenericArguments(System.Type)
extern void TypeExtensions_GetNameWithGenericArguments_m6425CC5113B3AECAD3D0E6C3C23BB59FFD4B143C (void);
// 0x0000005A System.String Unity.XR.CoreUtils.TypeExtensions::GetNameWithFullGenericArguments(System.Type)
extern void TypeExtensions_GetNameWithFullGenericArguments_m177F7832D37E769718F04E6D70DD2ED89305D09E (void);
// 0x0000005B System.String Unity.XR.CoreUtils.TypeExtensions::GetFullNameWithGenericArguments(System.Type)
extern void TypeExtensions_GetFullNameWithGenericArguments_mC6EA793A18285C19FAE8F69E85BF885AF9837300 (void);
// 0x0000005C System.String Unity.XR.CoreUtils.TypeExtensions::GetFullNameWithGenericArgumentsInternal(System.Type)
extern void TypeExtensions_GetFullNameWithGenericArgumentsInternal_mC78738287FA05D0DA4C39B57B300F11056265446 (void);
// 0x0000005D System.Boolean Unity.XR.CoreUtils.TypeExtensions::IsAssignableFromOrSubclassOf(System.Type,System.Type)
extern void TypeExtensions_IsAssignableFromOrSubclassOf_m722DDEA1BEDB8728AF59697EAEDC7EE3DA050607 (void);
// 0x0000005E System.Reflection.MethodInfo Unity.XR.CoreUtils.TypeExtensions::GetMethodRecursively(System.Type,System.String,System.Reflection.BindingFlags)
extern void TypeExtensions_GetMethodRecursively_m64969B3357EFF5CDF81C10905850A4C00B4AEBC8 (void);
// 0x0000005F System.Void Unity.XR.CoreUtils.TypeExtensions::.cctor()
extern void TypeExtensions__cctor_m0298F7D1F1670D44319F47BC8DF0E98D2DAD6543 (void);
// 0x00000060 System.Void Unity.XR.CoreUtils.TypeExtensions/<>c__DisplayClass2_0::.ctor()
extern void U3CU3Ec__DisplayClass2_0__ctor_mEE54F4ED54E08027FBC67E38C0A725E59B7341E2 (void);
// 0x00000061 System.Void Unity.XR.CoreUtils.TypeExtensions/<>c__DisplayClass2_0::<GetAssignableTypes>b__0(System.Type)
extern void U3CU3Ec__DisplayClass2_0_U3CGetAssignableTypesU3Eb__0_m311247BB0DB29ED4B54C8BCA00318804CD9DE58C (void);
// 0x00000062 System.Void Unity.XR.CoreUtils.BoolUnityEvent::.ctor()
extern void BoolUnityEvent__ctor_mB03F96F7A6B3A4C7B9644CE82D5CE1771B77CBE5 (void);
// 0x00000063 System.Void Unity.XR.CoreUtils.FloatUnityEvent::.ctor()
extern void FloatUnityEvent__ctor_m3C80AE317A469CC5F7C0920889BBB1F9D86195F5 (void);
// 0x00000064 System.Void Unity.XR.CoreUtils.Vector2UnityEvent::.ctor()
extern void Vector2UnityEvent__ctor_m5030F8916B653DFD26113971AC99AFC58C5D5C94 (void);
// 0x00000065 System.Void Unity.XR.CoreUtils.Vector3UnityEvent::.ctor()
extern void Vector3UnityEvent__ctor_m4C6C0F78526D7E9651F1BC2DE40F9A6B7795B3BB (void);
// 0x00000066 System.Void Unity.XR.CoreUtils.Vector4UnityEvent::.ctor()
extern void Vector4UnityEvent__ctor_m2C9E326EE573F0E05D04A57312872BD2C89CDF27 (void);
// 0x00000067 System.Void Unity.XR.CoreUtils.QuaternionUnityEvent::.ctor()
extern void QuaternionUnityEvent__ctor_m7B6198DF5266C35E7980BE275344236913C5D789 (void);
// 0x00000068 System.Void Unity.XR.CoreUtils.IntUnityEvent::.ctor()
extern void IntUnityEvent__ctor_m5F376D569539760621F5AC2CB87CABA0940C01B4 (void);
// 0x00000069 System.Void Unity.XR.CoreUtils.ColorUnityEvent::.ctor()
extern void ColorUnityEvent__ctor_m718169C7152A67F2CCC6954EDDE9599A722AC92A (void);
// 0x0000006A System.Void Unity.XR.CoreUtils.StringUnityEvent::.ctor()
extern void StringUnityEvent__ctor_m9346FB75AB6EF113D8898CF6B75DAB7BA567E17F (void);
// 0x0000006B UnityEngine.Vector2 Unity.XR.CoreUtils.Vector2Extensions::Inverse(UnityEngine.Vector2)
extern void Vector2Extensions_Inverse_mFEF0AA73E9531EA3E86C7F8D8E0D816B6184555F (void);
// 0x0000006C System.Single Unity.XR.CoreUtils.Vector2Extensions::MinComponent(UnityEngine.Vector2)
extern void Vector2Extensions_MinComponent_m8BD462FC3DEB793F88F1FE99D4CE0207A8A1B9A2 (void);
// 0x0000006D System.Single Unity.XR.CoreUtils.Vector2Extensions::MaxComponent(UnityEngine.Vector2)
extern void Vector2Extensions_MaxComponent_m915C8AE68D7E86256A7A8F2FD53DA4AF505E4DE9 (void);
// 0x0000006E UnityEngine.Vector2 Unity.XR.CoreUtils.Vector2Extensions::Abs(UnityEngine.Vector2)
extern void Vector2Extensions_Abs_m068F59620786B075C2AEF455C95A84CEF55C8610 (void);
// 0x0000006F UnityEngine.Vector3 Unity.XR.CoreUtils.Vector3Extensions::Inverse(UnityEngine.Vector3)
extern void Vector3Extensions_Inverse_mA872E848E3D77786E662AAE17A30F22E791ADDBE (void);
// 0x00000070 System.Single Unity.XR.CoreUtils.Vector3Extensions::MinComponent(UnityEngine.Vector3)
extern void Vector3Extensions_MinComponent_m8DF203A23A50AECA3F149F6BA68F99F3F4B4DD9A (void);
// 0x00000071 System.Single Unity.XR.CoreUtils.Vector3Extensions::MaxComponent(UnityEngine.Vector3)
extern void Vector3Extensions_MaxComponent_m19DF9C78F0799BD2D1F3E5E87565C600D40CFD4D (void);
// 0x00000072 UnityEngine.Vector3 Unity.XR.CoreUtils.Vector3Extensions::Abs(UnityEngine.Vector3)
extern void Vector3Extensions_Abs_mC1EF677BA8E710983CD33810107EFE4BDF48F81B (void);
// 0x00000073 UnityEngine.Vector3 Unity.XR.CoreUtils.Vector3Extensions::Multiply(UnityEngine.Vector3,UnityEngine.Vector3)
extern void Vector3Extensions_Multiply_m7B8B8D6D08861C9401D99F12DA04FE1CB8438141 (void);
// 0x00000074 UnityEngine.Vector3 Unity.XR.CoreUtils.Vector3Extensions::Divide(UnityEngine.Vector3,UnityEngine.Vector3)
extern void Vector3Extensions_Divide_m6BEABD7E540B8C17B3738253D908EC1C09D81DED (void);
// 0x00000075 UnityEngine.Vector3 Unity.XR.CoreUtils.Vector3Extensions::SafeDivide(UnityEngine.Vector3,UnityEngine.Vector3)
extern void Vector3Extensions_SafeDivide_mF92CFB05888CDF874CE8BBA9485A498C5EE7025B (void);
// 0x00000076 System.Void Unity.XR.CoreUtils.GameObjectUtils::add_GameObjectInstantiated(System.Action`1<UnityEngine.GameObject>)
extern void GameObjectUtils_add_GameObjectInstantiated_m93853F1B21AF5E5CD011DB177725734E4B314180 (void);
// 0x00000077 System.Void Unity.XR.CoreUtils.GameObjectUtils::remove_GameObjectInstantiated(System.Action`1<UnityEngine.GameObject>)
extern void GameObjectUtils_remove_GameObjectInstantiated_mB0FD128984A862C6507846AF56EBAD1B718E9481 (void);
// 0x00000078 UnityEngine.GameObject Unity.XR.CoreUtils.GameObjectUtils::Create()
extern void GameObjectUtils_Create_m832539D579201C8BD34A2EC844D104BAB0E3AEB9 (void);
// 0x00000079 UnityEngine.GameObject Unity.XR.CoreUtils.GameObjectUtils::Create(System.String)
extern void GameObjectUtils_Create_mDCB0E89C6F3F249CB7DFDE56C42292F669B7FCB9 (void);
// 0x0000007A UnityEngine.GameObject Unity.XR.CoreUtils.GameObjectUtils::Instantiate(UnityEngine.GameObject,UnityEngine.Transform,System.Boolean)
extern void GameObjectUtils_Instantiate_m57A3CA6B34794DFEDD135571D70E9231D2CA069B (void);
// 0x0000007B UnityEngine.GameObject Unity.XR.CoreUtils.GameObjectUtils::Instantiate(UnityEngine.GameObject,UnityEngine.Vector3,UnityEngine.Quaternion)
extern void GameObjectUtils_Instantiate_m2C08AA6D3276595FFEEF2D240FD27EFBBE30C309 (void);
// 0x0000007C UnityEngine.GameObject Unity.XR.CoreUtils.GameObjectUtils::Instantiate(UnityEngine.GameObject,UnityEngine.Transform,UnityEngine.Vector3,UnityEngine.Quaternion)
extern void GameObjectUtils_Instantiate_mAC531EFA2FDECF242ECA468B102E466EAD37E748 (void);
// 0x0000007D UnityEngine.GameObject Unity.XR.CoreUtils.GameObjectUtils::CloneWithHideFlags(UnityEngine.GameObject,UnityEngine.Transform)
extern void GameObjectUtils_CloneWithHideFlags_mBADABD1F968287AA786A04514E9E148681E1D16F (void);
// 0x0000007E System.Void Unity.XR.CoreUtils.GameObjectUtils::CopyHideFlagsRecursively(UnityEngine.GameObject,UnityEngine.GameObject)
extern void GameObjectUtils_CopyHideFlagsRecursively_m63790345EA383CA679A863D100CBD4E2010A3226 (void);
// 0x0000007F T Unity.XR.CoreUtils.GameObjectUtils::ExhaustiveComponentSearch(UnityEngine.GameObject)
// 0x00000080 T Unity.XR.CoreUtils.GameObjectUtils::ExhaustiveTaggedComponentSearch(UnityEngine.GameObject,System.String)
// 0x00000081 T Unity.XR.CoreUtils.GameObjectUtils::GetComponentInScene(UnityEngine.SceneManagement.Scene)
// 0x00000082 System.Void Unity.XR.CoreUtils.GameObjectUtils::GetComponentsInScene(UnityEngine.SceneManagement.Scene,System.Collections.Generic.List`1<T>,System.Boolean)
// 0x00000083 T Unity.XR.CoreUtils.GameObjectUtils::GetComponentInActiveScene()
// 0x00000084 System.Void Unity.XR.CoreUtils.GameObjectUtils::GetComponentsInActiveScene(System.Collections.Generic.List`1<T>,System.Boolean)
// 0x00000085 System.Void Unity.XR.CoreUtils.GameObjectUtils::GetComponentsInAllScenes(System.Collections.Generic.List`1<T>,System.Boolean)
// 0x00000086 System.Void Unity.XR.CoreUtils.GameObjectUtils::GetChildGameObjects(UnityEngine.GameObject,System.Collections.Generic.List`1<UnityEngine.GameObject>)
extern void GameObjectUtils_GetChildGameObjects_m52BA016172732BCAC11DE996D643AF8AAA313628 (void);
// 0x00000087 UnityEngine.GameObject Unity.XR.CoreUtils.GameObjectUtils::GetNamedChild(UnityEngine.GameObject,System.String)
extern void GameObjectUtils_GetNamedChild_mE7FE7FD7ABC71E16175C1E15BDCEE677B208554B (void);
// 0x00000088 System.Void Unity.XR.CoreUtils.GameObjectUtils::.cctor()
extern void GameObjectUtils__cctor_m1FEAE3648834C6C946B410BD93219F922B7F2F22 (void);
// 0x00000089 System.Void Unity.XR.CoreUtils.GameObjectUtils/<>c__DisplayClass20_0::.ctor()
extern void U3CU3Ec__DisplayClass20_0__ctor_m98CD90059928EB9EF34901953B33AECC294062E6 (void);
// 0x0000008A System.Boolean Unity.XR.CoreUtils.GameObjectUtils/<>c__DisplayClass20_0::<GetNamedChild>b__0(UnityEngine.Transform)
extern void U3CU3Ec__DisplayClass20_0_U3CGetNamedChildU3Eb__0_mE1129E70184CDE5CF58BD2E8AD241D291BCC2D3F (void);
// 0x0000008B System.Boolean Unity.XR.CoreUtils.GeometryUtils::FindClosestEdge(System.Collections.Generic.List`1<UnityEngine.Vector3>,UnityEngine.Vector3,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern void GeometryUtils_FindClosestEdge_m426FBC9568D7EFBBA456A215FB7C79038A637568 (void);
// 0x0000008C UnityEngine.Vector3 Unity.XR.CoreUtils.GeometryUtils::PointOnOppositeSideOfPolygon(System.Collections.Generic.List`1<UnityEngine.Vector3>,UnityEngine.Vector3)
extern void GeometryUtils_PointOnOppositeSideOfPolygon_m7F482E06A4DB8FD3953CE47CA5021FF71DE072FC (void);
// 0x0000008D System.Void Unity.XR.CoreUtils.GeometryUtils::TriangulatePolygon(System.Collections.Generic.List`1<System.Int32>,System.Int32,System.Boolean)
extern void GeometryUtils_TriangulatePolygon_m27A91F0BF325DAA31E573215A39CD83025B37A31 (void);
// 0x0000008E System.Boolean Unity.XR.CoreUtils.GeometryUtils::ClosestTimesOnTwoLines(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,System.Single&,System.Single&,System.Double)
extern void GeometryUtils_ClosestTimesOnTwoLines_m5D6AA2427827E9FDFA5E984A5C733D740184EA78 (void);
// 0x0000008F System.Boolean Unity.XR.CoreUtils.GeometryUtils::ClosestTimesOnTwoLinesXZ(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,System.Single&,System.Single&,System.Double)
extern void GeometryUtils_ClosestTimesOnTwoLinesXZ_mDFA433A59F2BAF536DE11A66DA42165A88155297 (void);
// 0x00000090 System.Boolean Unity.XR.CoreUtils.GeometryUtils::ClosestPointsOnTwoLineSegments(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3&,UnityEngine.Vector3&,System.Double)
extern void GeometryUtils_ClosestPointsOnTwoLineSegments_mF5D955079E3387006797DCB2EB56CEE03B191255 (void);
// 0x00000091 UnityEngine.Vector3 Unity.XR.CoreUtils.GeometryUtils::ClosestPointOnLineSegment(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern void GeometryUtils_ClosestPointOnLineSegment_mCA9F47EA2C19A1C7857FBED5FDC79826F6F08179 (void);
// 0x00000092 System.Void Unity.XR.CoreUtils.GeometryUtils::ClosestPolygonApproach(System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<UnityEngine.Vector3>,UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single)
extern void GeometryUtils_ClosestPolygonApproach_m8951215E3C90D196AFA4D59FB91E2953EC3BC5E2 (void);
// 0x00000093 System.Boolean Unity.XR.CoreUtils.GeometryUtils::PointInPolygon(UnityEngine.Vector3,System.Collections.Generic.List`1<UnityEngine.Vector3>)
extern void GeometryUtils_PointInPolygon_mE28D000C5D318B632509EA4D50E8060B02F55BCA (void);
// 0x00000094 System.Boolean Unity.XR.CoreUtils.GeometryUtils::PointInPolygon3D(UnityEngine.Vector3,System.Collections.Generic.List`1<UnityEngine.Vector3>)
extern void GeometryUtils_PointInPolygon3D_mAF27769B99E59E5801F348E66BA1354AB0C66477 (void);
// 0x00000095 UnityEngine.Vector3 Unity.XR.CoreUtils.GeometryUtils::ProjectPointOnPlane(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern void GeometryUtils_ProjectPointOnPlane_m8A9CEE68B75CAFE4474E6B1A90708E749A7DBD11 (void);
// 0x00000096 System.Boolean Unity.XR.CoreUtils.GeometryUtils::ConvexHull2D(System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<UnityEngine.Vector3>)
extern void GeometryUtils_ConvexHull2D_m0F7EAB7219457A5C2505697D9F94C522B8B283E2 (void);
// 0x00000097 UnityEngine.Vector3 Unity.XR.CoreUtils.GeometryUtils::PolygonCentroid2D(System.Collections.Generic.List`1<UnityEngine.Vector3>)
extern void GeometryUtils_PolygonCentroid2D_m2683A9761702450C75A7465FBFD2EB98A76286FD (void);
// 0x00000098 UnityEngine.Vector2 Unity.XR.CoreUtils.GeometryUtils::OrientedMinimumBoundingBox2D(System.Collections.Generic.List`1<UnityEngine.Vector3>,UnityEngine.Vector3[])
extern void GeometryUtils_OrientedMinimumBoundingBox2D_m731023F67EF506D36DE8B9972640879C353E071F (void);
// 0x00000099 System.Void Unity.XR.CoreUtils.GeometryUtils::RotateCalipers(UnityEngine.Vector3,System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Int32&,System.Int32&,System.Int32&,System.Int32&,UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern void GeometryUtils_RotateCalipers_m8148C3D0A9A8B07345AAB968293C0F603993F032 (void);
// 0x0000009A UnityEngine.Quaternion Unity.XR.CoreUtils.GeometryUtils::RotationForBox(UnityEngine.Vector3[])
extern void GeometryUtils_RotationForBox_m0FD340CB5AC92B2109C7FDD12D4A788624259583 (void);
// 0x0000009B System.Single Unity.XR.CoreUtils.GeometryUtils::ConvexPolygonArea(System.Collections.Generic.List`1<UnityEngine.Vector3>)
extern void GeometryUtils_ConvexPolygonArea_mAEE1A79D4E09ABD574EC5F45D0773E67D3B459BE (void);
// 0x0000009C System.Boolean Unity.XR.CoreUtils.GeometryUtils::PolygonInPolygon(System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<UnityEngine.Vector3>)
extern void GeometryUtils_PolygonInPolygon_m35CAB26692841DCB11AC10E8A54DF8D1D47F8A14 (void);
// 0x0000009D System.Boolean Unity.XR.CoreUtils.GeometryUtils::PolygonsWithinRange(System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Single)
extern void GeometryUtils_PolygonsWithinRange_mF033B4054CA9A5A548386526E84D431DB4E80F39 (void);
// 0x0000009E System.Boolean Unity.XR.CoreUtils.GeometryUtils::PolygonsWithinSqRange(System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Single)
extern void GeometryUtils_PolygonsWithinSqRange_m8CE57CCF1132207BA95A03A18D037DC231567A38 (void);
// 0x0000009F System.Boolean Unity.XR.CoreUtils.GeometryUtils::PointOnPolygonBoundsXZ(UnityEngine.Vector3,System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Single)
extern void GeometryUtils_PointOnPolygonBoundsXZ_m1F862C4F99187506184F19A154559FE48A3D4A5A (void);
// 0x000000A0 System.Boolean Unity.XR.CoreUtils.GeometryUtils::PointOnLineSegmentXZ(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void GeometryUtils_PointOnLineSegmentXZ_m1F7ED40B5CBE46F583BC9BB8EE1BAE7F349C0465 (void);
// 0x000000A1 UnityEngine.Quaternion Unity.XR.CoreUtils.GeometryUtils::NormalizeRotationKeepingUp(UnityEngine.Quaternion)
extern void GeometryUtils_NormalizeRotationKeepingUp_m49CE54E3247404C1F25202D3529903089727AF71 (void);
// 0x000000A2 UnityEngine.Pose Unity.XR.CoreUtils.GeometryUtils::PolygonUVPoseFromPlanePose(UnityEngine.Pose)
extern void GeometryUtils_PolygonUVPoseFromPlanePose_mB2167750B8AB1B1BC3B1A8D440D3DCDED79DCD21 (void);
// 0x000000A3 UnityEngine.Vector2 Unity.XR.CoreUtils.GeometryUtils::PolygonVertexToUV(UnityEngine.Vector3,UnityEngine.Pose,UnityEngine.Pose)
extern void GeometryUtils_PolygonVertexToUV_m3CCFAE3CA861E183E88541CD96BEB51C73080564 (void);
// 0x000000A4 System.Void Unity.XR.CoreUtils.GeometryUtils::.cctor()
extern void GeometryUtils__cctor_mF1CE5786F59F051799EF179D2358F8D619E8E09D (void);
// 0x000000A5 System.Guid Unity.XR.CoreUtils.GuidUtil::Compose(System.UInt64,System.UInt64)
extern void GuidUtil_Compose_mCAE83EA22706DE7E98CAFB30DEDF102AE7922D23 (void);
// 0x000000A6 System.Int32 Unity.XR.CoreUtils.HashCodeUtil::Combine(System.Int32,System.Int32)
extern void HashCodeUtil_Combine_m5EC3008FE1451272AED445DA4B1E7A65BAAF197E (void);
// 0x000000A7 System.Int32 Unity.XR.CoreUtils.HashCodeUtil::ReferenceHash(System.Object)
extern void HashCodeUtil_ReferenceHash_m55B8BCB31F847BA215571F96A8FC190A53CE9D26 (void);
// 0x000000A8 System.Int32 Unity.XR.CoreUtils.HashCodeUtil::Combine(System.Int32,System.Int32,System.Int32)
extern void HashCodeUtil_Combine_mA6FDC6EBF7EA9CA11137B153C11B7AA893686092 (void);
// 0x000000A9 System.Int32 Unity.XR.CoreUtils.HashCodeUtil::Combine(System.Int32,System.Int32,System.Int32,System.Int32)
extern void HashCodeUtil_Combine_m3359732D408DF911409B24C5801929B253BDB0E6 (void);
// 0x000000AA System.Int32 Unity.XR.CoreUtils.HashCodeUtil::Combine(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern void HashCodeUtil_Combine_m0F6C759282B4A222D99729D2DFE43D9142863563 (void);
// 0x000000AB System.Int32 Unity.XR.CoreUtils.HashCodeUtil::Combine(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern void HashCodeUtil_Combine_mDC201C9370700E43E7298571C1EC72DBA640FEA4 (void);
// 0x000000AC System.Int32 Unity.XR.CoreUtils.HashCodeUtil::Combine(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern void HashCodeUtil_Combine_m43973BA10BC4A787707412CF407F06D39F914186 (void);
// 0x000000AD System.Int32 Unity.XR.CoreUtils.HashCodeUtil::Combine(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern void HashCodeUtil_Combine_mB8B9F21479697937FEA02822986270736B8482AF (void);
// 0x000000AE UnityEngine.Material Unity.XR.CoreUtils.MaterialUtils::GetMaterialClone(UnityEngine.Renderer)
extern void MaterialUtils_GetMaterialClone_m90471305CAA3365785C95A1509E7EB71F35F53C1 (void);
// 0x000000AF UnityEngine.Material Unity.XR.CoreUtils.MaterialUtils::GetMaterialClone(UnityEngine.UI.Graphic)
extern void MaterialUtils_GetMaterialClone_mC2E8D1C2666841C120DD6A585A2989B6095690EF (void);
// 0x000000B0 UnityEngine.Material[] Unity.XR.CoreUtils.MaterialUtils::CloneMaterials(UnityEngine.Renderer)
extern void MaterialUtils_CloneMaterials_m683E82E55D34CDC52FA8B8CF0B688A364E42ECB8 (void);
// 0x000000B1 UnityEngine.Color Unity.XR.CoreUtils.MaterialUtils::HexToColor(System.String)
extern void MaterialUtils_HexToColor_mAA7ADAFF79FEC94826E0CC9EE60B58A390720B11 (void);
// 0x000000B2 UnityEngine.Color Unity.XR.CoreUtils.MaterialUtils::HueShift(UnityEngine.Color,System.Single)
extern void MaterialUtils_HueShift_m87EFA7E2F6E7E669E5F7CBFB7D14FD0BA129CF19 (void);
// 0x000000B3 System.Void Unity.XR.CoreUtils.MaterialUtils::AddMaterial(UnityEngine.Renderer,UnityEngine.Material)
extern void MaterialUtils_AddMaterial_mA1434A2D094B315E231ADEE99785570BE8838DF4 (void);
// 0x000000B4 System.Boolean Unity.XR.CoreUtils.MathUtility::Approximately(System.Single,System.Single)
extern void MathUtility_Approximately_mC16346E0E1DC749401536FF7BAC14FE2209531B6 (void);
// 0x000000B5 System.Boolean Unity.XR.CoreUtils.MathUtility::ApproximatelyZero(System.Single)
extern void MathUtility_ApproximatelyZero_m7CC58C5F152D873E47CA0E439BF533FD8F76389F (void);
// 0x000000B6 System.Double Unity.XR.CoreUtils.MathUtility::Clamp(System.Double,System.Double,System.Double)
extern void MathUtility_Clamp_m1910D49E2E6E9F429499DBC6496DAB1805DEEC64 (void);
// 0x000000B7 System.Double Unity.XR.CoreUtils.MathUtility::ShortestAngleDistance(System.Double,System.Double,System.Double,System.Double)
extern void MathUtility_ShortestAngleDistance_m92EC6724359E8F2A5A39368A31E11B6FD7798322 (void);
// 0x000000B8 System.Single Unity.XR.CoreUtils.MathUtility::ShortestAngleDistance(System.Single,System.Single,System.Single,System.Single)
extern void MathUtility_ShortestAngleDistance_m7E85583D1BE339B2F4FF8B9FBA35E5453C9A5385 (void);
// 0x000000B9 System.Boolean Unity.XR.CoreUtils.MathUtility::IsUndefined(System.Single)
extern void MathUtility_IsUndefined_mA9A48B1D332DE89696C37DF98B48546671FC92D7 (void);
// 0x000000BA System.Boolean Unity.XR.CoreUtils.MathUtility::IsAxisAligned(UnityEngine.Vector3)
extern void MathUtility_IsAxisAligned_m2D8C03E539A7B4B22E3A8BADF0824F719F9EE466 (void);
// 0x000000BB System.Boolean Unity.XR.CoreUtils.MathUtility::IsPositivePowerOfTwo(System.Int32)
extern void MathUtility_IsPositivePowerOfTwo_m77F04D6B410A9E286031132AD9FFD99B9B858683 (void);
// 0x000000BC System.Int32 Unity.XR.CoreUtils.MathUtility::FirstActiveFlagIndex(System.Int32)
extern void MathUtility_FirstActiveFlagIndex_m52F16442BC5E44D48C2A794A40AFDDEB7CE1223C (void);
// 0x000000BD System.Void Unity.XR.CoreUtils.MathUtility::.cctor()
extern void MathUtility__cctor_m862195F28CE6C6EB2AADC31778864B97383E1DEF (void);
// 0x000000BE System.Void Unity.XR.CoreUtils.NativeArrayUtils::EnsureCapacity(Unity.Collections.NativeArray`1<T>&,System.Int32,Unity.Collections.Allocator,Unity.Collections.NativeArrayOptions)
// 0x000000BF T Unity.XR.CoreUtils.ObjectPool`1::Get()
// 0x000000C0 System.Void Unity.XR.CoreUtils.ObjectPool`1::Recycle(T)
// 0x000000C1 System.Void Unity.XR.CoreUtils.ObjectPool`1::ClearInstance(T)
// 0x000000C2 System.Void Unity.XR.CoreUtils.ObjectPool`1::.ctor()
// 0x000000C3 System.Action`1<Unity.XR.CoreUtils.OnDestroyNotifier> Unity.XR.CoreUtils.OnDestroyNotifier::get_Destroyed()
extern void OnDestroyNotifier_get_Destroyed_mC97BC83A97DE90F12FE38BE92C18B7F5C71DCEB3 (void);
// 0x000000C4 System.Void Unity.XR.CoreUtils.OnDestroyNotifier::set_Destroyed(System.Action`1<Unity.XR.CoreUtils.OnDestroyNotifier>)
extern void OnDestroyNotifier_set_Destroyed_m3BBB8995C3DD07AE5B21FE33D359335D4579C293 (void);
// 0x000000C5 System.Void Unity.XR.CoreUtils.OnDestroyNotifier::OnDestroy()
extern void OnDestroyNotifier_OnDestroy_m6A54FC8AA273DEC72D2D304C658DC4A3CBD57655 (void);
// 0x000000C6 System.Void Unity.XR.CoreUtils.OnDestroyNotifier::.ctor()
extern void OnDestroyNotifier__ctor_m5C2057FAA74A11BB09FCC79C61B3AAEC940E21A7 (void);
// 0x000000C7 System.Reflection.Assembly[] Unity.XR.CoreUtils.ReflectionUtils::GetCachedAssemblies()
extern void ReflectionUtils_GetCachedAssemblies_m66F7AC089423AAE67CACB1E42850A9DAB633F98C (void);
// 0x000000C8 System.Collections.Generic.List`1<System.Type[]> Unity.XR.CoreUtils.ReflectionUtils::GetCachedTypesPerAssembly()
extern void ReflectionUtils_GetCachedTypesPerAssembly_m33A7C1F8017478F270027E469F1C9F7245EA00CA (void);
// 0x000000C9 System.Collections.Generic.List`1<System.Collections.Generic.Dictionary`2<System.String,System.Type>> Unity.XR.CoreUtils.ReflectionUtils::GetCachedAssemblyTypeMaps()
extern void ReflectionUtils_GetCachedAssemblyTypeMaps_m013E40F27D563157E7C095328DA562D92CFFBD9A (void);
// 0x000000CA System.Void Unity.XR.CoreUtils.ReflectionUtils::PreWarmTypeCache()
extern void ReflectionUtils_PreWarmTypeCache_m107F873E4D190D4A93D34D216E3A4DD05F856B1C (void);
// 0x000000CB System.Void Unity.XR.CoreUtils.ReflectionUtils::ForEachAssembly(System.Action`1<System.Reflection.Assembly>)
extern void ReflectionUtils_ForEachAssembly_m3F2199DFBC6BB6B6B87D69A13F354DAA3E4A2AA1 (void);
// 0x000000CC System.Void Unity.XR.CoreUtils.ReflectionUtils::ForEachType(System.Action`1<System.Type>)
extern void ReflectionUtils_ForEachType_m494480789C6CAE697ABBD27025BBB86B56507747 (void);
// 0x000000CD System.Type Unity.XR.CoreUtils.ReflectionUtils::FindType(System.Func`2<System.Type,System.Boolean>)
extern void ReflectionUtils_FindType_m782AA238FC17C5BE371C8813A548FDD29A50C257 (void);
// 0x000000CE System.Type Unity.XR.CoreUtils.ReflectionUtils::FindTypeByFullName(System.String)
extern void ReflectionUtils_FindTypeByFullName_m4D38967AB5DB887F2B5BE624D782027E8CFCF98A (void);
// 0x000000CF System.Void Unity.XR.CoreUtils.ReflectionUtils::FindTypesBatch(System.Collections.Generic.List`1<System.Func`2<System.Type,System.Boolean>>,System.Collections.Generic.List`1<System.Type>)
extern void ReflectionUtils_FindTypesBatch_mF1DE4F67D23C3DB4EDE95A6B26EF5FA53B04AFC1 (void);
// 0x000000D0 System.Void Unity.XR.CoreUtils.ReflectionUtils::FindTypesByFullNameBatch(System.Collections.Generic.List`1<System.String>,System.Collections.Generic.List`1<System.Type>)
extern void ReflectionUtils_FindTypesByFullNameBatch_m04379058E9CD31296550FECBC28255442DE2CE97 (void);
// 0x000000D1 System.Type Unity.XR.CoreUtils.ReflectionUtils::FindTypeInAssemblyByFullName(System.String,System.String)
extern void ReflectionUtils_FindTypeInAssemblyByFullName_m2B87E225E054EFD5734F5BE4ADCA29DC8B8D5AFA (void);
// 0x000000D2 System.String Unity.XR.CoreUtils.ReflectionUtils::NicifyVariableName(System.String)
extern void ReflectionUtils_NicifyVariableName_mEB59FA21C726CC74758FEBF412D17C694C426A63 (void);
// 0x000000D3 T Unity.XR.CoreUtils.ScriptableSettings`1::get_Instance()
// 0x000000D4 T Unity.XR.CoreUtils.ScriptableSettings`1::CreateAndLoad()
// 0x000000D5 System.Void Unity.XR.CoreUtils.ScriptableSettings`1::.ctor()
// 0x000000D6 Unity.XR.CoreUtils.ScriptableSettingsBase Unity.XR.CoreUtils.ScriptableSettingsBase::GetInstanceByType(System.Type)
extern void ScriptableSettingsBase_GetInstanceByType_m3562E85AF76D51D5A613777BCBCDCE9552BFCA69 (void);
// 0x000000D7 System.Void Unity.XR.CoreUtils.ScriptableSettingsBase::Awake()
extern void ScriptableSettingsBase_Awake_mDCBC0A22C46F740B6D80B53A3B44F5898ADFF8DB (void);
// 0x000000D8 System.Void Unity.XR.CoreUtils.ScriptableSettingsBase::OnEnable()
extern void ScriptableSettingsBase_OnEnable_m47360EB7C37E28C785121DC715974F28F2E2EA16 (void);
// 0x000000D9 System.Void Unity.XR.CoreUtils.ScriptableSettingsBase::OnLoaded()
extern void ScriptableSettingsBase_OnLoaded_m711DBDFE8D215378B0F0076C7C74CA64C25B74D6 (void);
// 0x000000DA System.Boolean Unity.XR.CoreUtils.ScriptableSettingsBase::ValidatePath(System.String,System.String&)
extern void ScriptableSettingsBase_ValidatePath_m764AA10BED2571607C26751A417785231A1D441B (void);
// 0x000000DB System.Void Unity.XR.CoreUtils.ScriptableSettingsBase::.ctor()
extern void ScriptableSettingsBase__ctor_mEC52E58072A94518570CFFEACF7EFEE3367754BA (void);
// 0x000000DC System.Void Unity.XR.CoreUtils.ScriptableSettingsBase::.cctor()
extern void ScriptableSettingsBase__cctor_mF0722349FA498253B0D7B3CA2C8D6C7A80B8B55D (void);
// 0x000000DD System.Void Unity.XR.CoreUtils.ScriptableSettingsBase`1::.ctor()
// 0x000000DE System.Void Unity.XR.CoreUtils.ScriptableSettingsBase`1::Save(System.String)
// 0x000000DF System.String Unity.XR.CoreUtils.ScriptableSettingsBase`1::GetFilePath()
// 0x000000E0 System.Void Unity.XR.CoreUtils.ScriptableSettingsBase`1::.cctor()
// 0x000000E1 Unity.XR.CoreUtils.SerializableGuid Unity.XR.CoreUtils.SerializableGuid::get_Empty()
extern void SerializableGuid_get_Empty_m8E564E8473F52D800319FF03B5CC0AA4789F5F31 (void);
// 0x000000E2 System.Guid Unity.XR.CoreUtils.SerializableGuid::get_Guid()
extern void SerializableGuid_get_Guid_m856CA008B9F386B935D3B7F61B3051A9A617F43B (void);
// 0x000000E3 System.Void Unity.XR.CoreUtils.SerializableGuid::.ctor(System.UInt64,System.UInt64)
extern void SerializableGuid__ctor_m6BA38B84A6B818982E18562E521025FD27C37865 (void);
// 0x000000E4 System.Int32 Unity.XR.CoreUtils.SerializableGuid::GetHashCode()
extern void SerializableGuid_GetHashCode_m78B52A41CE256433C1F24D6DB86493DEEB75072F (void);
// 0x000000E5 System.Boolean Unity.XR.CoreUtils.SerializableGuid::Equals(System.Object)
extern void SerializableGuid_Equals_mA3F83D68EA7AEEF6041324D3437965F86859A744 (void);
// 0x000000E6 System.String Unity.XR.CoreUtils.SerializableGuid::ToString()
extern void SerializableGuid_ToString_m7578340F13D51160F53D0685A0884349A1A31CD8 (void);
// 0x000000E7 System.String Unity.XR.CoreUtils.SerializableGuid::ToString(System.String)
extern void SerializableGuid_ToString_mD6C5F07E58267DFAD806FE5FAA71E62574A85114 (void);
// 0x000000E8 System.String Unity.XR.CoreUtils.SerializableGuid::ToString(System.String,System.IFormatProvider)
extern void SerializableGuid_ToString_m69259E62FD545E73EC5AEAA9140154749B7A4963 (void);
// 0x000000E9 System.Boolean Unity.XR.CoreUtils.SerializableGuid::Equals(Unity.XR.CoreUtils.SerializableGuid)
extern void SerializableGuid_Equals_mD106FD276E6E41DEFCCBB364CA78E7140F04556B (void);
// 0x000000EA System.Boolean Unity.XR.CoreUtils.SerializableGuid::op_Equality(Unity.XR.CoreUtils.SerializableGuid,Unity.XR.CoreUtils.SerializableGuid)
extern void SerializableGuid_op_Equality_mD9CF69ADC96A6E1D9FFAF1201001C43856F1417D (void);
// 0x000000EB System.Boolean Unity.XR.CoreUtils.SerializableGuid::op_Inequality(Unity.XR.CoreUtils.SerializableGuid,Unity.XR.CoreUtils.SerializableGuid)
extern void SerializableGuid_op_Inequality_m68F78F2A584D9827117B83B8DC095F20B400035E (void);
// 0x000000EC System.Void Unity.XR.CoreUtils.SerializableGuid::.cctor()
extern void SerializableGuid__cctor_mF750DABEA8F1D410B4B83AB3FE110201EA3B1FB4 (void);
// 0x000000ED Unity.XR.CoreUtils.SerializableGuid Unity.XR.CoreUtils.SerializableGuidUtil::Create(System.Guid)
extern void SerializableGuidUtil_Create_mF231D85E26395ABF6DFC4C1F6401CB05D1A55230 (void);
// 0x000000EE System.Void Unity.XR.CoreUtils.TextureUtils::RenderTextureToTexture2D(UnityEngine.RenderTexture,UnityEngine.Texture2D)
extern void TextureUtils_RenderTextureToTexture2D_mD415E3056EECAF12E5FBB0167F57395EAA42724C (void);
// 0x000000EF System.Void Unity.XR.CoreUtils.UndoBlock::.ctor(System.String,System.Boolean)
extern void UndoBlock__ctor_m8D8EF58B3F23AE8430298EFD1114C0D4EF800548 (void);
// 0x000000F0 System.Void Unity.XR.CoreUtils.UndoBlock::RegisterCreatedObject(UnityEngine.Object)
extern void UndoBlock_RegisterCreatedObject_mDF025FB713FBCFDCEAEEE0D0B306F644E5B03879 (void);
// 0x000000F1 System.Void Unity.XR.CoreUtils.UndoBlock::RecordObject(UnityEngine.Object)
extern void UndoBlock_RecordObject_mAF19BA9AB219412F5A16F85A0C2427ADE5E83D2D (void);
// 0x000000F2 System.Void Unity.XR.CoreUtils.UndoBlock::SetTransformParent(UnityEngine.Transform,UnityEngine.Transform)
extern void UndoBlock_SetTransformParent_m2212E4453692E7CF8186DED59E9AFEBFFBC383B4 (void);
// 0x000000F3 T Unity.XR.CoreUtils.UndoBlock::AddComponent(UnityEngine.GameObject)
// 0x000000F4 System.Void Unity.XR.CoreUtils.UndoBlock::Dispose(System.Boolean)
extern void UndoBlock_Dispose_m339DAD7532A1F9604D6CF13DAAE1A3FA7461911A (void);
// 0x000000F5 System.Void Unity.XR.CoreUtils.UndoBlock::Dispose()
extern void UndoBlock_Dispose_m22B38E379937937C7A1BDB0D40F8C170F8E6C91A (void);
// 0x000000F6 System.Void Unity.XR.CoreUtils.UnityObjectUtils::Destroy(UnityEngine.Object,System.Boolean)
extern void UnityObjectUtils_Destroy_m8E56BEBEF22130AEE71F005D30BBEC581444B0EE (void);
// 0x000000F7 T Unity.XR.CoreUtils.UnityObjectUtils::ConvertUnityObjectToType(UnityEngine.Object)
// 0x000000F8 System.Void Unity.XR.CoreUtils.UnityObjectUtils::RemoveDestroyedObjects(System.Collections.Generic.List`1<T>)
// 0x000000F9 System.Void Unity.XR.CoreUtils.UnityObjectUtils::RemoveDestroyedKeys(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// 0x000000FA System.Void Unity.XR.CoreUtils.XRLoggingUtils::.cctor()
extern void XRLoggingUtils__cctor_m34C67D615FF5E41B0C2930A2B03E09F63E8F0EB1 (void);
// 0x000000FB System.Void Unity.XR.CoreUtils.XRLoggingUtils::Log(System.String,UnityEngine.Object)
extern void XRLoggingUtils_Log_m17C6EC7B35C7A0E5EE36C3CBF3E2A953C364CA54 (void);
// 0x000000FC System.Void Unity.XR.CoreUtils.XRLoggingUtils::LogWarning(System.String,UnityEngine.Object)
extern void XRLoggingUtils_LogWarning_m16C829B8740982FA4B0EBED606A2D518E492A5CC (void);
// 0x000000FD System.Void Unity.XR.CoreUtils.XRLoggingUtils::LogError(System.String,UnityEngine.Object)
extern void XRLoggingUtils_LogError_m65734BA6FECDC0C2A35DE1E6401886DFD6515402 (void);
// 0x000000FE System.Void Unity.XR.CoreUtils.XRLoggingUtils::LogException(System.Exception,UnityEngine.Object)
extern void XRLoggingUtils_LogException_m7AE93BFBB883746388CE34909FC8B0874B344082 (void);
// 0x000000FF UnityEngine.Camera Unity.XR.CoreUtils.XROrigin::get_Camera()
extern void XROrigin_get_Camera_mDEC1EA5E15968845DA812397BBA4506A88B0F9FF (void);
// 0x00000100 System.Void Unity.XR.CoreUtils.XROrigin::set_Camera(UnityEngine.Camera)
extern void XROrigin_set_Camera_m33ECB239BCDB92806AFD47EA8A4685E81EBDFE24 (void);
// 0x00000101 UnityEngine.Transform Unity.XR.CoreUtils.XROrigin::get_TrackablesParent()
extern void XROrigin_get_TrackablesParent_m1762CFBDDF007C80B97379835702D8607DB4F3E0 (void);
// 0x00000102 System.Void Unity.XR.CoreUtils.XROrigin::set_TrackablesParent(UnityEngine.Transform)
extern void XROrigin_set_TrackablesParent_mE069D24282C732C5164D03FEEDBA8C9FD8AF323E (void);
// 0x00000103 System.Void Unity.XR.CoreUtils.XROrigin::add_TrackablesParentTransformChanged(System.Action`1<Unity.XR.CoreUtils.ARTrackablesParentTransformChangedEventArgs>)
extern void XROrigin_add_TrackablesParentTransformChanged_mA320D83FF126A31F8139C86BC68BE280EE4BC280 (void);
// 0x00000104 System.Void Unity.XR.CoreUtils.XROrigin::remove_TrackablesParentTransformChanged(System.Action`1<Unity.XR.CoreUtils.ARTrackablesParentTransformChangedEventArgs>)
extern void XROrigin_remove_TrackablesParentTransformChanged_m4D7C48C781798E32FE484B40997BF6BFB5A8BBB0 (void);
// 0x00000105 UnityEngine.GameObject Unity.XR.CoreUtils.XROrigin::get_Origin()
extern void XROrigin_get_Origin_m791424808DC2F95B332CFCE34A260E5F53DEC2AD (void);
// 0x00000106 System.Void Unity.XR.CoreUtils.XROrigin::set_Origin(UnityEngine.GameObject)
extern void XROrigin_set_Origin_mA3665C3245F0B16C7C2AA41381FC8D57A80FD6D3 (void);
// 0x00000107 UnityEngine.GameObject Unity.XR.CoreUtils.XROrigin::get_CameraFloorOffsetObject()
extern void XROrigin_get_CameraFloorOffsetObject_mB06A006D2F1211B477E5AADF9DF6EBCB5EFCCF45 (void);
// 0x00000108 System.Void Unity.XR.CoreUtils.XROrigin::set_CameraFloorOffsetObject(UnityEngine.GameObject)
extern void XROrigin_set_CameraFloorOffsetObject_mE20DF102DDC9F456EE93F97D69A5EEF7306C1F5E (void);
// 0x00000109 Unity.XR.CoreUtils.XROrigin/TrackingOriginMode Unity.XR.CoreUtils.XROrigin::get_RequestedTrackingOriginMode()
extern void XROrigin_get_RequestedTrackingOriginMode_m4FF9E54CD679A9B3CC05B36314D66F3B1AD1688B (void);
// 0x0000010A System.Void Unity.XR.CoreUtils.XROrigin::set_RequestedTrackingOriginMode(Unity.XR.CoreUtils.XROrigin/TrackingOriginMode)
extern void XROrigin_set_RequestedTrackingOriginMode_m2D7D0AF1006FF55D8EC6D8BF6679F9867E5D496E (void);
// 0x0000010B System.Single Unity.XR.CoreUtils.XROrigin::get_CameraYOffset()
extern void XROrigin_get_CameraYOffset_mA396D70F0E27E80EA5177915433C5038BACDDF4E (void);
// 0x0000010C System.Void Unity.XR.CoreUtils.XROrigin::set_CameraYOffset(System.Single)
extern void XROrigin_set_CameraYOffset_mBA7E721A282BA9E414B1BCA15E82AE3D48C1CAA4 (void);
// 0x0000010D UnityEngine.XR.TrackingOriginModeFlags Unity.XR.CoreUtils.XROrigin::get_CurrentTrackingOriginMode()
extern void XROrigin_get_CurrentTrackingOriginMode_m90D206E17CF4EBD3DDD281F526E43AA3A1A84AFA (void);
// 0x0000010E System.Void Unity.XR.CoreUtils.XROrigin::set_CurrentTrackingOriginMode(UnityEngine.XR.TrackingOriginModeFlags)
extern void XROrigin_set_CurrentTrackingOriginMode_mE1F8C7BD93763B7503A9F1A66224B1E99AFB1680 (void);
// 0x0000010F UnityEngine.Vector3 Unity.XR.CoreUtils.XROrigin::get_OriginInCameraSpacePos()
extern void XROrigin_get_OriginInCameraSpacePos_mC934EF4E3E14C7DC72A88E058C39F7A40104174C (void);
// 0x00000110 UnityEngine.Vector3 Unity.XR.CoreUtils.XROrigin::get_CameraInOriginSpacePos()
extern void XROrigin_get_CameraInOriginSpacePos_mA25C4AECE774DCE217DF1AB838E9D82BB87BEE56 (void);
// 0x00000111 System.Single Unity.XR.CoreUtils.XROrigin::get_CameraInOriginSpaceHeight()
extern void XROrigin_get_CameraInOriginSpaceHeight_m59DC39ADD21E144299A2B7A283414591CC966FF7 (void);
// 0x00000112 System.Void Unity.XR.CoreUtils.XROrigin::MoveOffsetHeight()
extern void XROrigin_MoveOffsetHeight_mE85E434BC9ECF1D224C9A7506B814A3D37B2F3EC (void);
// 0x00000113 System.Void Unity.XR.CoreUtils.XROrigin::MoveOffsetHeight(System.Single)
extern void XROrigin_MoveOffsetHeight_m89E6241C5D67CCD0AC4A166AA4599AADF39E895F (void);
// 0x00000114 System.Void Unity.XR.CoreUtils.XROrigin::TryInitializeCamera()
extern void XROrigin_TryInitializeCamera_m8A470425D8369F6555D3C36C3335F9D8F82B40A4 (void);
// 0x00000115 System.Boolean Unity.XR.CoreUtils.XROrigin::SetupCamera()
extern void XROrigin_SetupCamera_m63ECB705B6D6398F5B8B7C2EF7B33CB37C796BC4 (void);
// 0x00000116 System.Boolean Unity.XR.CoreUtils.XROrigin::SetupCamera(UnityEngine.XR.XRInputSubsystem)
extern void XROrigin_SetupCamera_mEB39C60D1AE12C2A25647E578B353E127C406726 (void);
// 0x00000117 System.Void Unity.XR.CoreUtils.XROrigin::OnInputSubsystemTrackingOriginUpdated(UnityEngine.XR.XRInputSubsystem)
extern void XROrigin_OnInputSubsystemTrackingOriginUpdated_m132A24B0235B1E82289B45F7B5B9132A19A85719 (void);
// 0x00000118 System.Collections.IEnumerator Unity.XR.CoreUtils.XROrigin::RepeatInitializeCamera()
extern void XROrigin_RepeatInitializeCamera_mE77E233DAE27A9547E0BE478CC06ED6E51E8917A (void);
// 0x00000119 System.Boolean Unity.XR.CoreUtils.XROrigin::RotateAroundCameraUsingOriginUp(System.Single)
extern void XROrigin_RotateAroundCameraUsingOriginUp_mFBC31926C33A2A4BB0FB2CD37A5D6A2DB805C26D (void);
// 0x0000011A System.Boolean Unity.XR.CoreUtils.XROrigin::RotateAroundCameraPosition(UnityEngine.Vector3,System.Single)
extern void XROrigin_RotateAroundCameraPosition_mDFFA1FCA35ABE47B99085527C2DBF97141289EC3 (void);
// 0x0000011B System.Boolean Unity.XR.CoreUtils.XROrigin::MatchOriginUp(UnityEngine.Vector3)
extern void XROrigin_MatchOriginUp_mF2000A84EAFDFA87164B64B19BC93324AB030C26 (void);
// 0x0000011C System.Boolean Unity.XR.CoreUtils.XROrigin::MatchOriginUpCameraForward(UnityEngine.Vector3,UnityEngine.Vector3)
extern void XROrigin_MatchOriginUpCameraForward_mC4723BFAD3FF3C9ABA7659A74C4CB10800A2C50A (void);
// 0x0000011D System.Boolean Unity.XR.CoreUtils.XROrigin::MatchOriginUpOriginForward(UnityEngine.Vector3,UnityEngine.Vector3)
extern void XROrigin_MatchOriginUpOriginForward_m22ADB294CA783226D0C50DBDCEE0586A85AC9044 (void);
// 0x0000011E System.Boolean Unity.XR.CoreUtils.XROrigin::MoveCameraToWorldLocation(UnityEngine.Vector3)
extern void XROrigin_MoveCameraToWorldLocation_mBF588A0488903F9FEF0F70015E28772522794397 (void);
// 0x0000011F System.Void Unity.XR.CoreUtils.XROrigin::Awake()
extern void XROrigin_Awake_mB3295310096FFDA1017D3226E81748B301EB93FD (void);
// 0x00000120 UnityEngine.Pose Unity.XR.CoreUtils.XROrigin::GetCameraOriginPose()
extern void XROrigin_GetCameraOriginPose_m65F340AB82A12B79C1CE8A2239C49A4FCA34C3F5 (void);
// 0x00000121 System.Void Unity.XR.CoreUtils.XROrigin::OnEnable()
extern void XROrigin_OnEnable_m85878F36D9F87D746996935D8BB5A6B1335E6201 (void);
// 0x00000122 System.Void Unity.XR.CoreUtils.XROrigin::OnDisable()
extern void XROrigin_OnDisable_mCD6CC559152D9231118E30D97F253D6B5B0AE50F (void);
// 0x00000123 System.Void Unity.XR.CoreUtils.XROrigin::OnBeforeRender()
extern void XROrigin_OnBeforeRender_m20A25D128427DB95D8645B7B0C42CEC96547023E (void);
// 0x00000124 System.Void Unity.XR.CoreUtils.XROrigin::OnValidate()
extern void XROrigin_OnValidate_m107F310112D1458AD2F8E44CD064359B9476BEFE (void);
// 0x00000125 System.Void Unity.XR.CoreUtils.XROrigin::Start()
extern void XROrigin_Start_m3E862070874567EBC71CEDFDFCE96346D89872A9 (void);
// 0x00000126 System.Void Unity.XR.CoreUtils.XROrigin::OnDestroy()
extern void XROrigin_OnDestroy_m8F5C01F5CFB4BD23FCC83EA915398067189613B2 (void);
// 0x00000127 System.Void Unity.XR.CoreUtils.XROrigin::.ctor()
extern void XROrigin__ctor_mC64C36F5C94F69F20DDCC912292CB0E6614ED40B (void);
// 0x00000128 System.Void Unity.XR.CoreUtils.XROrigin::.cctor()
extern void XROrigin__cctor_mF110E2E7581DC702D61F2AAE789091C9755F244A (void);
// 0x00000129 System.Boolean Unity.XR.CoreUtils.XROrigin::<OnValidate>g__IsModeStale|60_0()
extern void XROrigin_U3COnValidateU3Eg__IsModeStaleU7C60_0_mFD6964C954EA4827D65DB27D63D003A16594CFB8 (void);
// 0x0000012A System.Void Unity.XR.CoreUtils.XROrigin/<RepeatInitializeCamera>d__48::.ctor(System.Int32)
extern void U3CRepeatInitializeCameraU3Ed__48__ctor_m38D16CB74A912FFAC7BDB663DAC73CCC66F353DB (void);
// 0x0000012B System.Void Unity.XR.CoreUtils.XROrigin/<RepeatInitializeCamera>d__48::System.IDisposable.Dispose()
extern void U3CRepeatInitializeCameraU3Ed__48_System_IDisposable_Dispose_m34D86CC2B00D547934EAB0050158EE4FC2316EB2 (void);
// 0x0000012C System.Boolean Unity.XR.CoreUtils.XROrigin/<RepeatInitializeCamera>d__48::MoveNext()
extern void U3CRepeatInitializeCameraU3Ed__48_MoveNext_m2A28BAD8A5F265425E6CED138B0ECD941C8C6D6A (void);
// 0x0000012D System.Object Unity.XR.CoreUtils.XROrigin/<RepeatInitializeCamera>d__48::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRepeatInitializeCameraU3Ed__48_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m85F41DF9153BE60931348D9DA5A5A0B0F5CB24E0 (void);
// 0x0000012E System.Void Unity.XR.CoreUtils.XROrigin/<RepeatInitializeCamera>d__48::System.Collections.IEnumerator.Reset()
extern void U3CRepeatInitializeCameraU3Ed__48_System_Collections_IEnumerator_Reset_m034DF1C46AABE33936127AE4406DAB4F078F88D4 (void);
// 0x0000012F System.Object Unity.XR.CoreUtils.XROrigin/<RepeatInitializeCamera>d__48::System.Collections.IEnumerator.get_Current()
extern void U3CRepeatInitializeCameraU3Ed__48_System_Collections_IEnumerator_get_Current_m7C83C7C74A3582ABE2F428AB355A4C5B175B9E09 (void);
// 0x00000130 System.Void Unity.XR.CoreUtils.Datums.AnimationCurveDatum::.ctor()
extern void AnimationCurveDatum__ctor_mAC74C57A2E1FA217073BC2FA9F929B60B06C0A96 (void);
// 0x00000131 System.Void Unity.XR.CoreUtils.Datums.AnimationCurveDatumProperty::.ctor(UnityEngine.AnimationCurve)
extern void AnimationCurveDatumProperty__ctor_mC666A8B6268F99B4884A55E746C61B23B4D66A78 (void);
// 0x00000132 System.Void Unity.XR.CoreUtils.Datums.AnimationCurveDatumProperty::.ctor(Unity.XR.CoreUtils.Datums.AnimationCurveDatum)
extern void AnimationCurveDatumProperty__ctor_mA03DC328AFEACBB4CDF590EBD92E96B3B6B361C7 (void);
// 0x00000133 System.String Unity.XR.CoreUtils.Datums.Datum`1::get_Comments()
// 0x00000134 System.Void Unity.XR.CoreUtils.Datums.Datum`1::set_Comments(System.String)
// 0x00000135 System.Boolean Unity.XR.CoreUtils.Datums.Datum`1::get_ReadOnly()
// 0x00000136 System.Void Unity.XR.CoreUtils.Datums.Datum`1::set_ReadOnly(System.Boolean)
// 0x00000137 Unity.XR.CoreUtils.Bindings.Variables.IReadOnlyBindableVariable`1<T> Unity.XR.CoreUtils.Datums.Datum`1::get_BindableVariableReference()
// 0x00000138 T Unity.XR.CoreUtils.Datums.Datum`1::get_Value()
// 0x00000139 System.Void Unity.XR.CoreUtils.Datums.Datum`1::set_Value(T)
// 0x0000013A System.Void Unity.XR.CoreUtils.Datums.Datum`1::OnEnable()
// 0x0000013B System.Void Unity.XR.CoreUtils.Datums.Datum`1::.ctor()
// 0x0000013C System.Void Unity.XR.CoreUtils.Datums.DatumProperty`2::.ctor()
// 0x0000013D System.Void Unity.XR.CoreUtils.Datums.DatumProperty`2::.ctor(TValue)
// 0x0000013E System.Void Unity.XR.CoreUtils.Datums.DatumProperty`2::.ctor(TDatum)
// 0x0000013F TValue Unity.XR.CoreUtils.Datums.DatumProperty`2::get_Value()
// 0x00000140 System.Void Unity.XR.CoreUtils.Datums.DatumProperty`2::set_Value(TValue)
// 0x00000141 Unity.XR.CoreUtils.Datums.Datum`1<TValue> Unity.XR.CoreUtils.Datums.DatumProperty`2::get_Datum()
// 0x00000142 TValue Unity.XR.CoreUtils.Datums.DatumProperty`2::get_ConstantValue()
// 0x00000143 TValue Unity.XR.CoreUtils.Datums.DatumProperty`2::op_Implicit(Unity.XR.CoreUtils.Datums.DatumProperty`2<TValue,TDatum>)
// 0x00000144 System.Void Unity.XR.CoreUtils.Datums.FloatDatum::.ctor()
extern void FloatDatum__ctor_mE4D7819E7E5DD9EAF2E8FAA76B3C84EDFAC484C5 (void);
// 0x00000145 System.Void Unity.XR.CoreUtils.Datums.FloatDatumProperty::.ctor(System.Single)
extern void FloatDatumProperty__ctor_m7955F8DB25DA2F305FB35CBC77F05A7D6EFA63A3 (void);
// 0x00000146 System.Void Unity.XR.CoreUtils.Datums.FloatDatumProperty::.ctor(Unity.XR.CoreUtils.Datums.FloatDatum)
extern void FloatDatumProperty__ctor_m680524511AC200E88B4BE00C84CD1D97EBDDC5AB (void);
// 0x00000147 System.Void Unity.XR.CoreUtils.Datums.IntDatum::SetValueRounded(System.Single)
extern void IntDatum_SetValueRounded_m373370FB48681B70563A6994586DE97AA240C7AA (void);
// 0x00000148 System.Void Unity.XR.CoreUtils.Datums.IntDatum::.ctor()
extern void IntDatum__ctor_mB6126495320E9F2B2AE89094D8098511EDBA2364 (void);
// 0x00000149 System.Void Unity.XR.CoreUtils.Datums.IntDatumProperty::.ctor(System.Int32)
extern void IntDatumProperty__ctor_mF952BB46846F3403A3157166A28F4554A06C236C (void);
// 0x0000014A System.Void Unity.XR.CoreUtils.Datums.IntDatumProperty::.ctor(Unity.XR.CoreUtils.Datums.IntDatum)
extern void IntDatumProperty__ctor_m2B931B4B9AB4C29DDDE025AF7CF5AA5930DC354F (void);
// 0x0000014B System.Void Unity.XR.CoreUtils.Datums.StringDatum::.ctor()
extern void StringDatum__ctor_m527034C8D2EB313C91B08ED1FF3C1D712DAA066B (void);
// 0x0000014C System.Void Unity.XR.CoreUtils.Datums.StringDatumProperty::.ctor(System.String)
extern void StringDatumProperty__ctor_m333CCD5E86C742B44863ACBAD85376E3012A5E48 (void);
// 0x0000014D System.Void Unity.XR.CoreUtils.Datums.StringDatumProperty::.ctor(Unity.XR.CoreUtils.Datums.StringDatum)
extern void StringDatumProperty__ctor_m5DA5D9F7EBC32B1DF726CE2D87069B3C7A1A22A0 (void);
// 0x0000014E System.Int32 Unity.XR.CoreUtils.Collections.HashSetList`1::get_Count()
// 0x0000014F System.Boolean Unity.XR.CoreUtils.Collections.HashSetList`1::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
// 0x00000150 T Unity.XR.CoreUtils.Collections.HashSetList`1::get_Item(System.Int32)
// 0x00000151 System.Void Unity.XR.CoreUtils.Collections.HashSetList`1::.ctor(System.Int32)
// 0x00000152 System.Collections.Generic.List`1/Enumerator<T> Unity.XR.CoreUtils.Collections.HashSetList`1::GetEnumerator()
// 0x00000153 System.Collections.Generic.IEnumerator`1<T> Unity.XR.CoreUtils.Collections.HashSetList`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x00000154 System.Collections.IEnumerator Unity.XR.CoreUtils.Collections.HashSetList`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000155 System.Void Unity.XR.CoreUtils.Collections.HashSetList`1::System.Collections.Generic.ICollection<T>.Add(T)
// 0x00000156 System.Boolean Unity.XR.CoreUtils.Collections.HashSetList`1::Add(T)
// 0x00000157 System.Boolean Unity.XR.CoreUtils.Collections.HashSetList`1::Remove(T)
// 0x00000158 System.Void Unity.XR.CoreUtils.Collections.HashSetList`1::ExceptWith(System.Collections.Generic.IEnumerable`1<T>)
// 0x00000159 System.Void Unity.XR.CoreUtils.Collections.HashSetList`1::IntersectWith(System.Collections.Generic.IEnumerable`1<T>)
// 0x0000015A System.Boolean Unity.XR.CoreUtils.Collections.HashSetList`1::IsProperSubsetOf(System.Collections.Generic.IEnumerable`1<T>)
// 0x0000015B System.Boolean Unity.XR.CoreUtils.Collections.HashSetList`1::IsProperSupersetOf(System.Collections.Generic.IEnumerable`1<T>)
// 0x0000015C System.Boolean Unity.XR.CoreUtils.Collections.HashSetList`1::IsSubsetOf(System.Collections.Generic.IEnumerable`1<T>)
// 0x0000015D System.Boolean Unity.XR.CoreUtils.Collections.HashSetList`1::IsSupersetOf(System.Collections.Generic.IEnumerable`1<T>)
// 0x0000015E System.Boolean Unity.XR.CoreUtils.Collections.HashSetList`1::Overlaps(System.Collections.Generic.IEnumerable`1<T>)
// 0x0000015F System.Boolean Unity.XR.CoreUtils.Collections.HashSetList`1::SetEquals(System.Collections.Generic.IEnumerable`1<T>)
// 0x00000160 System.Void Unity.XR.CoreUtils.Collections.HashSetList`1::SymmetricExceptWith(System.Collections.Generic.IEnumerable`1<T>)
// 0x00000161 System.Void Unity.XR.CoreUtils.Collections.HashSetList`1::UnionWith(System.Collections.Generic.IEnumerable`1<T>)
// 0x00000162 System.Void Unity.XR.CoreUtils.Collections.HashSetList`1::Clear()
// 0x00000163 System.Boolean Unity.XR.CoreUtils.Collections.HashSetList`1::Contains(T)
// 0x00000164 System.Void Unity.XR.CoreUtils.Collections.HashSetList`1::CopyTo(T[],System.Int32)
// 0x00000165 System.Void Unity.XR.CoreUtils.Collections.HashSetList`1::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x00000166 System.Void Unity.XR.CoreUtils.Collections.HashSetList`1::OnDeserialization(System.Object)
// 0x00000167 System.Void Unity.XR.CoreUtils.Collections.HashSetList`1::RefreshList()
// 0x00000168 System.Collections.Generic.IReadOnlyList`1<T> Unity.XR.CoreUtils.Collections.HashSetList`1::AsList()
// 0x00000169 System.Void Unity.XR.CoreUtils.Bindings.BindingsGroup::AddBinding(Unity.XR.CoreUtils.Bindings.IEventBinding)
extern void BindingsGroup_AddBinding_mB862CA624836F2F41878A83077484E5D4F51BBDC (void);
// 0x0000016A System.Void Unity.XR.CoreUtils.Bindings.BindingsGroup::ClearBinding(Unity.XR.CoreUtils.Bindings.IEventBinding)
extern void BindingsGroup_ClearBinding_mC21C8A6C1A0FD093CD8C1EBB4DCFE33388089154 (void);
// 0x0000016B System.Void Unity.XR.CoreUtils.Bindings.BindingsGroup::Bind()
extern void BindingsGroup_Bind_mA4F70711731E8B0918CDEDE84E0BA2285063BCDA (void);
// 0x0000016C System.Void Unity.XR.CoreUtils.Bindings.BindingsGroup::Unbind()
extern void BindingsGroup_Unbind_m5F2E23AF75E337E57CD49C17CB644A01026241E6 (void);
// 0x0000016D System.Void Unity.XR.CoreUtils.Bindings.BindingsGroup::Clear()
extern void BindingsGroup_Clear_mB28C44D8FC5CBEDCBE41D99AC52A06AD3D3C0024 (void);
// 0x0000016E System.Void Unity.XR.CoreUtils.Bindings.BindingsGroup::.ctor()
extern void BindingsGroup__ctor_m134F76F56E968839A28997A80A0B8F7272F0B745 (void);
// 0x0000016F System.Action Unity.XR.CoreUtils.Bindings.EventBinding::get_BindAction()
extern void EventBinding_get_BindAction_mC84020F3A9209984D2268B991678A608B21DBDE9 (void);
// 0x00000170 System.Void Unity.XR.CoreUtils.Bindings.EventBinding::set_BindAction(System.Action)
extern void EventBinding_set_BindAction_mBDF58F3C06D243CD7774B052BB5B0A90F5996D78 (void);
// 0x00000171 System.Action Unity.XR.CoreUtils.Bindings.EventBinding::get_UnbindAction()
extern void EventBinding_get_UnbindAction_m0F29A3DEC5CAC0716802A686F982DC6739C8EAA9 (void);
// 0x00000172 System.Void Unity.XR.CoreUtils.Bindings.EventBinding::set_UnbindAction(System.Action)
extern void EventBinding_set_UnbindAction_m0F0D30CC2B17576CA59618091D698F91C0FA28E9 (void);
// 0x00000173 System.Boolean Unity.XR.CoreUtils.Bindings.EventBinding::get_IsBound()
extern void EventBinding_get_IsBound_m1DE1DD30C013662343D545313F4F0B9FB7C5859D (void);
// 0x00000174 System.Void Unity.XR.CoreUtils.Bindings.EventBinding::.ctor(System.Action,System.Action)
extern void EventBinding__ctor_m9FBD9CE6AB00A30B99020ADDDB3642EB1D4ED55E (void);
// 0x00000175 System.Void Unity.XR.CoreUtils.Bindings.EventBinding::Bind()
extern void EventBinding_Bind_m2114894CB4EC2EC35EF0A4068F0CFAE4955C9385 (void);
// 0x00000176 System.Void Unity.XR.CoreUtils.Bindings.EventBinding::Unbind()
extern void EventBinding_Unbind_mA28A0372DAF5CF6757FF016EB2D9CA0E405A0A33 (void);
// 0x00000177 System.Void Unity.XR.CoreUtils.Bindings.EventBinding::ClearBinding()
extern void EventBinding_ClearBinding_m464FF9805BA626E2447B53C6FC273D9F79FE5BF1 (void);
// 0x00000178 System.Boolean Unity.XR.CoreUtils.Bindings.IEventBinding::get_IsBound()
// 0x00000179 System.Void Unity.XR.CoreUtils.Bindings.IEventBinding::Bind()
// 0x0000017A System.Void Unity.XR.CoreUtils.Bindings.IEventBinding::Unbind()
// 0x0000017B System.Void Unity.XR.CoreUtils.Bindings.IEventBinding::ClearBinding()
// 0x0000017C System.Void Unity.XR.CoreUtils.Bindings.Variables.BindableEnum`1::.ctor(T,System.Boolean,System.Func`3<T,T,System.Boolean>,System.Boolean)
// 0x0000017D System.Boolean Unity.XR.CoreUtils.Bindings.Variables.BindableEnum`1::ValueEquals(T)
// 0x0000017E System.Void Unity.XR.CoreUtils.Bindings.Variables.BindableVariable`1::.ctor(T,System.Boolean,System.Func`3<T,T,System.Boolean>,System.Boolean)
// 0x0000017F System.Boolean Unity.XR.CoreUtils.Bindings.Variables.BindableVariable`1::ValueEquals(T)
// 0x00000180 System.Void Unity.XR.CoreUtils.Bindings.Variables.BindableVariableAlloc`1::.ctor(T,System.Boolean,System.Func`3<T,T,System.Boolean>,System.Boolean)
// 0x00000181 System.Void Unity.XR.CoreUtils.Bindings.Variables.BindableVariableBase`1::add_valueUpdated(System.Action`1<T>)
// 0x00000182 System.Void Unity.XR.CoreUtils.Bindings.Variables.BindableVariableBase`1::remove_valueUpdated(System.Action`1<T>)
// 0x00000183 T Unity.XR.CoreUtils.Bindings.Variables.BindableVariableBase`1::get_Value()
// 0x00000184 System.Void Unity.XR.CoreUtils.Bindings.Variables.BindableVariableBase`1::set_Value(T)
// 0x00000185 System.Int32 Unity.XR.CoreUtils.Bindings.Variables.BindableVariableBase`1::get_BindingCount()
// 0x00000186 System.Boolean Unity.XR.CoreUtils.Bindings.Variables.BindableVariableBase`1::SetValueWithoutNotify(T)
// 0x00000187 Unity.XR.CoreUtils.Bindings.IEventBinding Unity.XR.CoreUtils.Bindings.Variables.BindableVariableBase`1::Subscribe(System.Action`1<T>)
// 0x00000188 Unity.XR.CoreUtils.Bindings.IEventBinding Unity.XR.CoreUtils.Bindings.Variables.BindableVariableBase`1::SubscribeAndUpdate(System.Action`1<T>)
// 0x00000189 System.Void Unity.XR.CoreUtils.Bindings.Variables.BindableVariableBase`1::Unsubscribe(System.Action`1<T>)
// 0x0000018A System.Void Unity.XR.CoreUtils.Bindings.Variables.BindableVariableBase`1::IncrementReferenceCount()
// 0x0000018B System.Void Unity.XR.CoreUtils.Bindings.Variables.BindableVariableBase`1::DecrementReferenceCount()
// 0x0000018C System.Void Unity.XR.CoreUtils.Bindings.Variables.BindableVariableBase`1::.ctor(T,System.Boolean,System.Func`3<T,T,System.Boolean>,System.Boolean)
// 0x0000018D System.Void Unity.XR.CoreUtils.Bindings.Variables.BindableVariableBase`1::BroadcastValue()
// 0x0000018E System.Threading.Tasks.Task`1<T> Unity.XR.CoreUtils.Bindings.Variables.BindableVariableBase`1::Task(System.Func`2<T,System.Boolean>,System.Threading.CancellationToken)
// 0x0000018F System.Threading.Tasks.Task`1<T> Unity.XR.CoreUtils.Bindings.Variables.BindableVariableBase`1::Task(T,System.Threading.CancellationToken)
// 0x00000190 System.Boolean Unity.XR.CoreUtils.Bindings.Variables.BindableVariableBase`1::ValueEquals(T)
// 0x00000191 System.Void Unity.XR.CoreUtils.Bindings.Variables.BindableVariableBase`1/<>c__DisplayClass14_0::.ctor()
// 0x00000192 System.Void Unity.XR.CoreUtils.Bindings.Variables.BindableVariableBase`1/<>c__DisplayClass14_0::<Subscribe>b__0()
// 0x00000193 System.Void Unity.XR.CoreUtils.Bindings.Variables.BindableVariableBase`1/<>c__DisplayClass14_0::<Subscribe>b__1()
// 0x00000194 System.Threading.Tasks.Task`1<T> Unity.XR.CoreUtils.Bindings.Variables.BindableVariableTaskPredicate`1::get_Task()
// 0x00000195 System.Void Unity.XR.CoreUtils.Bindings.Variables.BindableVariableTaskPredicate`1::.ctor(Unity.XR.CoreUtils.Bindings.Variables.IReadOnlyBindableVariable`1<T>,System.Func`2<T,System.Boolean>,System.Threading.CancellationToken)
// 0x00000196 System.Void Unity.XR.CoreUtils.Bindings.Variables.BindableVariableTaskPredicate`1::Cancelled()
// 0x00000197 System.Void Unity.XR.CoreUtils.Bindings.Variables.BindableVariableTaskPredicate`1::Await(T)
// 0x00000198 System.Threading.Tasks.Task`1<T> Unity.XR.CoreUtils.Bindings.Variables.BindableVariableTaskState`1::get_task()
// 0x00000199 System.Void Unity.XR.CoreUtils.Bindings.Variables.BindableVariableTaskState`1::.ctor(Unity.XR.CoreUtils.Bindings.Variables.IReadOnlyBindableVariable`1<T>,T,System.Threading.CancellationToken)
// 0x0000019A System.Void Unity.XR.CoreUtils.Bindings.Variables.BindableVariableTaskState`1::Cancelled()
// 0x0000019B System.Void Unity.XR.CoreUtils.Bindings.Variables.BindableVariableTaskState`1::Await(T)
// 0x0000019C Unity.XR.CoreUtils.Bindings.IEventBinding Unity.XR.CoreUtils.Bindings.Variables.IReadOnlyBindableVariable`1::Subscribe(System.Action`1<T>)
// 0x0000019D Unity.XR.CoreUtils.Bindings.IEventBinding Unity.XR.CoreUtils.Bindings.Variables.IReadOnlyBindableVariable`1::SubscribeAndUpdate(System.Action`1<T>)
// 0x0000019E System.Void Unity.XR.CoreUtils.Bindings.Variables.IReadOnlyBindableVariable`1::Unsubscribe(System.Action`1<T>)
// 0x0000019F T Unity.XR.CoreUtils.Bindings.Variables.IReadOnlyBindableVariable`1::get_Value()
// 0x000001A0 System.Int32 Unity.XR.CoreUtils.Bindings.Variables.IReadOnlyBindableVariable`1::get_BindingCount()
// 0x000001A1 System.Boolean Unity.XR.CoreUtils.Bindings.Variables.IReadOnlyBindableVariable`1::ValueEquals(T)
// 0x000001A2 System.Threading.Tasks.Task`1<T> Unity.XR.CoreUtils.Bindings.Variables.IReadOnlyBindableVariable`1::Task(System.Func`2<T,System.Boolean>,System.Threading.CancellationToken)
// 0x000001A3 System.Threading.Tasks.Task`1<T> Unity.XR.CoreUtils.Bindings.Variables.IReadOnlyBindableVariable`1::Task(T,System.Threading.CancellationToken)
// 0x000001A4 System.Void Unity.XR.CoreUtils.GUI.EnumDisplayAttribute::.ctor(System.Object[])
extern void EnumDisplayAttribute__ctor_mE08D80992FC58ED07B0232B89DDE33766AD09A9D (void);
// 0x000001A5 System.Void Unity.XR.CoreUtils.GUI.FlagsPropertyAttribute::.ctor()
extern void FlagsPropertyAttribute__ctor_mF81FEDE55981A37924F05D5EF9F6DD0AC8F6BCB2 (void);
static Il2CppMethodPointer s_methodPointers[421] = 
{
	EmbeddedAttribute__ctor_m397CF6D74070B39A844CEF4BC9846AF5DC8423B9,
	IsReadOnlyAttribute__ctor_m40764A40A4145B9996BC9AECBAD2C3BCE27FA65D,
	ARTrackablesParentTransformChangedEventArgs_get_Origin_m635FF91C20A6D6C0514EABDDED14289D01BDC0A5,
	ARTrackablesParentTransformChangedEventArgs_get_TrackablesParent_mC93ACA08A0A5E3F00A58F63EF932BD6AA768BA43,
	ARTrackablesParentTransformChangedEventArgs__ctor_m90A574F56EAF0228BD1D7CBFDAF682B8E58F7F9D,
	ARTrackablesParentTransformChangedEventArgs_Equals_mDEA555E578C8B4ED1C2EB86EC70E289D6A542DEE,
	ARTrackablesParentTransformChangedEventArgs_Equals_mD1E41855005B1E2CF7834216ECA32621B6229D53,
	ARTrackablesParentTransformChangedEventArgs_GetHashCode_mDBE0D34111420DBF1A446062AD324FCDE522B51A,
	ARTrackablesParentTransformChangedEventArgs_op_Equality_m1B1D17C67E606DCEC56538D3DDD56BCD79B34FEC,
	ARTrackablesParentTransformChangedEventArgs_op_Inequality_mA180733C851949CFBD6ACE2AB80F9D9BAA2FD96F,
	ReadOnlyAttribute__ctor_mA7EDE36B78291002E0121E5FBA2903A4F725F50F,
	ScriptableSettingsPathAttribute_get_Path_m76F5FE1AB4A5E0CE163CF1C7F0F7140A2B63B078,
	ScriptableSettingsPathAttribute__ctor_m46C282885F5C0DB52545F16B4D191701AAC56EC3,
	BoundsUtils_GetBounds_mD3B6DFDC18760F87DE0ED175D55B104AC2BB232C,
	BoundsUtils_GetBounds_m5EEDAB2D057C0B8FCF9DE74755ADCAD7418E157F,
	BoundsUtils_GetBounds_m540953796CFC376F9C3AF2239FFF137F4858039A,
	BoundsUtils_GetBounds_m9BD787B76BE78CAF17781487A0EDFB9DB6CC8E9F,
	NULL,
	BoundsUtils_GetBounds_m6C8E00F314F0FA1162C9AFF1F07799406F815FEC,
	BoundsUtils__cctor_mAD7FB5706D882279D0DA048C2045536875B5140A,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	BoundsExtensions_ContainsCompletely_mEA22CADCF3881A4A4219DDE687D9A2F8F07DD847,
	CameraExtensions_GetVerticalFieldOfView_m101064D5B729B433C1FCDF9F6FE3117E8ADD3F53,
	CameraExtensions_GetHorizontalFieldOfView_m49C48EFE352CB2A8E0E6E7E6506552634A8C7378,
	CameraExtensions_GetVerticalOrthographicSize_mE059E1B5A380CCE22C1BE172540820B473DC0653,
	NULL,
	CollectionExtensions__cctor_m1AC43AE23FEEA7AF9A7FBBBE0473FD73BCBD2FD9,
	NULL,
	GameObjectExtensions_SetHideFlagsRecursively_m6F00615C8F62D206E47AFCA0FB5C5FA5B24469E4,
	GameObjectExtensions_AddToHideFlagsRecursively_m09365FE52EFEFC1785D48F8737DE51E210A2C02E,
	GameObjectExtensions_SetLayerRecursively_mDF83662C1D71E162BEA49C042C3F6AA6BF62B781,
	GameObjectExtensions_SetLayerAndAddToHideFlagsRecursively_m9203559713DAD72AA9445A94BF1061434B25CFFB,
	GameObjectExtensions_SetLayerAndHideFlagsRecursively_mBED51B647E27A9A86E48FC5958D782D2B9A709C3,
	GameObjectExtensions_SetRunInEditModeRecursively_m04A08BEF612D58DE53C4DE3F8928764BFD747BF9,
	GuidExtensions_Decompose_m852CCE666BC2C41B30B344DE8ACCE9F562056E3F,
	NULL,
	NULL,
	LayerMaskExtensions_GetFirstLayerIndex_m59E2969145DDC43B4795653FA9343E3B0AB22DBE,
	LayerMaskExtensions_Contains_mD73CA4A5FA715E24E4E5149BD99554C533895E36,
	NULL,
	NULL,
	NULL,
	PoseExtensions_ApplyOffsetTo_m8DC685DEE61A5BF96281BEEC786624046BF20187,
	PoseExtensions_ApplyOffsetTo_m74E9139469883F2C36F4F0F9C0BC249EE87D929F,
	PoseExtensions_ApplyInverseOffsetTo_m90414AC45AA5E572388BA8CC6A36B3D591ABACFF,
	QuaternionExtensions_ConstrainYaw_m38F5AC58FD533B1A546DDF12F92805DC29D2E4F7,
	QuaternionExtensions_ConstrainYawNormalized_mAA17E485823DBFF827A210FED067BA1D312A4818,
	QuaternionExtensions_ConstrainYawPitchNormalized_m8FBC20086FA9EA65AC223835873868C01A7F8D3F,
	StopwatchExtensions_Restart_m0AE76CC92E6248DE3B1184F1517B9D0DF933985B,
	StringExtensions_FirstToUpper_m071E8AAD165D7ED39F24053F16F8472E03AB63B8,
	StringExtensions_InsertSpacesBetweenWords_m26A802473A54EB1EEDDCD45BD26DCE9125FF113A,
	StringExtensions__cctor_m16B58C6C89089FDD3B263B05FDBE532A45CE9FBF,
	TransformExtensions_GetLocalPose_mAC44DDBB372B5426A776ADF219FA89277DFD87EC,
	TransformExtensions_GetWorldPose_m33A1A882B998C9949D1C6424846E60D55B5AEF45,
	TransformExtensions_SetLocalPose_mB910345A162B1D213157129920771121086411EB,
	TransformExtensions_SetWorldPose_mA1EDD07D363DBEB6E9F282ECBACBFAD7407E7CF2,
	TransformExtensions_TransformPose_mEB3D81D8D439A0CB8C08B791A180B78542978AAA,
	TransformExtensions_InverseTransformPose_m4032C88611F08379DFA1AD41EBC09B0578B70BB4,
	TransformExtensions_InverseTransformRay_m8C41EB5E94CB9B39339B90E8B8A69435AC944939,
	TypeExtensions_GetAssignableTypes_m0F6C4B24986632C28BCA0C533B95D20E9DB2BE24,
	TypeExtensions_GetImplementationsOfInterface_mB129015D4AC213C5B417DB294CB35EC606F98D8D,
	TypeExtensions_GetExtensionsOfClass_m15E6CD161BC60995D99459F2E18794AD175CE34F,
	TypeExtensions_GetGenericInterfaces_m7CF723B51E93B293EF201CC8F239F3B205273E49,
	TypeExtensions_GetPropertyRecursively_m00E9156D1FD3407A6047A7069E4BB8BC17C10447,
	TypeExtensions_GetFieldRecursively_m4B31CBD9AB4D628DC336F68FF8E270510B13BD93,
	TypeExtensions_GetFieldsRecursively_m7C09A508D81F407687C61B96AB8264CD5E8BD1B3,
	TypeExtensions_GetPropertiesRecursively_mD708E69AA5F2D0DB41EB62BEFFD76E0DCCA64256,
	TypeExtensions_GetInterfaceFieldsFromClasses_mDA187CB5312C43811477DF73AE680DEE9096823B,
	NULL,
	NULL,
	TypeExtensions_GetFieldInTypeOrBaseType_m17260C2C93FED7BC94A5AF9258C874F6A614700C,
	TypeExtensions_GetNameWithGenericArguments_m6425CC5113B3AECAD3D0E6C3C23BB59FFD4B143C,
	TypeExtensions_GetNameWithFullGenericArguments_m177F7832D37E769718F04E6D70DD2ED89305D09E,
	TypeExtensions_GetFullNameWithGenericArguments_mC6EA793A18285C19FAE8F69E85BF885AF9837300,
	TypeExtensions_GetFullNameWithGenericArgumentsInternal_mC78738287FA05D0DA4C39B57B300F11056265446,
	TypeExtensions_IsAssignableFromOrSubclassOf_m722DDEA1BEDB8728AF59697EAEDC7EE3DA050607,
	TypeExtensions_GetMethodRecursively_m64969B3357EFF5CDF81C10905850A4C00B4AEBC8,
	TypeExtensions__cctor_m0298F7D1F1670D44319F47BC8DF0E98D2DAD6543,
	U3CU3Ec__DisplayClass2_0__ctor_mEE54F4ED54E08027FBC67E38C0A725E59B7341E2,
	U3CU3Ec__DisplayClass2_0_U3CGetAssignableTypesU3Eb__0_m311247BB0DB29ED4B54C8BCA00318804CD9DE58C,
	BoolUnityEvent__ctor_mB03F96F7A6B3A4C7B9644CE82D5CE1771B77CBE5,
	FloatUnityEvent__ctor_m3C80AE317A469CC5F7C0920889BBB1F9D86195F5,
	Vector2UnityEvent__ctor_m5030F8916B653DFD26113971AC99AFC58C5D5C94,
	Vector3UnityEvent__ctor_m4C6C0F78526D7E9651F1BC2DE40F9A6B7795B3BB,
	Vector4UnityEvent__ctor_m2C9E326EE573F0E05D04A57312872BD2C89CDF27,
	QuaternionUnityEvent__ctor_m7B6198DF5266C35E7980BE275344236913C5D789,
	IntUnityEvent__ctor_m5F376D569539760621F5AC2CB87CABA0940C01B4,
	ColorUnityEvent__ctor_m718169C7152A67F2CCC6954EDDE9599A722AC92A,
	StringUnityEvent__ctor_m9346FB75AB6EF113D8898CF6B75DAB7BA567E17F,
	Vector2Extensions_Inverse_mFEF0AA73E9531EA3E86C7F8D8E0D816B6184555F,
	Vector2Extensions_MinComponent_m8BD462FC3DEB793F88F1FE99D4CE0207A8A1B9A2,
	Vector2Extensions_MaxComponent_m915C8AE68D7E86256A7A8F2FD53DA4AF505E4DE9,
	Vector2Extensions_Abs_m068F59620786B075C2AEF455C95A84CEF55C8610,
	Vector3Extensions_Inverse_mA872E848E3D77786E662AAE17A30F22E791ADDBE,
	Vector3Extensions_MinComponent_m8DF203A23A50AECA3F149F6BA68F99F3F4B4DD9A,
	Vector3Extensions_MaxComponent_m19DF9C78F0799BD2D1F3E5E87565C600D40CFD4D,
	Vector3Extensions_Abs_mC1EF677BA8E710983CD33810107EFE4BDF48F81B,
	Vector3Extensions_Multiply_m7B8B8D6D08861C9401D99F12DA04FE1CB8438141,
	Vector3Extensions_Divide_m6BEABD7E540B8C17B3738253D908EC1C09D81DED,
	Vector3Extensions_SafeDivide_mF92CFB05888CDF874CE8BBA9485A498C5EE7025B,
	GameObjectUtils_add_GameObjectInstantiated_m93853F1B21AF5E5CD011DB177725734E4B314180,
	GameObjectUtils_remove_GameObjectInstantiated_mB0FD128984A862C6507846AF56EBAD1B718E9481,
	GameObjectUtils_Create_m832539D579201C8BD34A2EC844D104BAB0E3AEB9,
	GameObjectUtils_Create_mDCB0E89C6F3F249CB7DFDE56C42292F669B7FCB9,
	GameObjectUtils_Instantiate_m57A3CA6B34794DFEDD135571D70E9231D2CA069B,
	GameObjectUtils_Instantiate_m2C08AA6D3276595FFEEF2D240FD27EFBBE30C309,
	GameObjectUtils_Instantiate_mAC531EFA2FDECF242ECA468B102E466EAD37E748,
	GameObjectUtils_CloneWithHideFlags_mBADABD1F968287AA786A04514E9E148681E1D16F,
	GameObjectUtils_CopyHideFlagsRecursively_m63790345EA383CA679A863D100CBD4E2010A3226,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	GameObjectUtils_GetChildGameObjects_m52BA016172732BCAC11DE996D643AF8AAA313628,
	GameObjectUtils_GetNamedChild_mE7FE7FD7ABC71E16175C1E15BDCEE677B208554B,
	GameObjectUtils__cctor_m1FEAE3648834C6C946B410BD93219F922B7F2F22,
	U3CU3Ec__DisplayClass20_0__ctor_m98CD90059928EB9EF34901953B33AECC294062E6,
	U3CU3Ec__DisplayClass20_0_U3CGetNamedChildU3Eb__0_mE1129E70184CDE5CF58BD2E8AD241D291BCC2D3F,
	GeometryUtils_FindClosestEdge_m426FBC9568D7EFBBA456A215FB7C79038A637568,
	GeometryUtils_PointOnOppositeSideOfPolygon_m7F482E06A4DB8FD3953CE47CA5021FF71DE072FC,
	GeometryUtils_TriangulatePolygon_m27A91F0BF325DAA31E573215A39CD83025B37A31,
	GeometryUtils_ClosestTimesOnTwoLines_m5D6AA2427827E9FDFA5E984A5C733D740184EA78,
	GeometryUtils_ClosestTimesOnTwoLinesXZ_mDFA433A59F2BAF536DE11A66DA42165A88155297,
	GeometryUtils_ClosestPointsOnTwoLineSegments_mF5D955079E3387006797DCB2EB56CEE03B191255,
	GeometryUtils_ClosestPointOnLineSegment_mCA9F47EA2C19A1C7857FBED5FDC79826F6F08179,
	GeometryUtils_ClosestPolygonApproach_m8951215E3C90D196AFA4D59FB91E2953EC3BC5E2,
	GeometryUtils_PointInPolygon_mE28D000C5D318B632509EA4D50E8060B02F55BCA,
	GeometryUtils_PointInPolygon3D_mAF27769B99E59E5801F348E66BA1354AB0C66477,
	GeometryUtils_ProjectPointOnPlane_m8A9CEE68B75CAFE4474E6B1A90708E749A7DBD11,
	GeometryUtils_ConvexHull2D_m0F7EAB7219457A5C2505697D9F94C522B8B283E2,
	GeometryUtils_PolygonCentroid2D_m2683A9761702450C75A7465FBFD2EB98A76286FD,
	GeometryUtils_OrientedMinimumBoundingBox2D_m731023F67EF506D36DE8B9972640879C353E071F,
	GeometryUtils_RotateCalipers_m8148C3D0A9A8B07345AAB968293C0F603993F032,
	GeometryUtils_RotationForBox_m0FD340CB5AC92B2109C7FDD12D4A788624259583,
	GeometryUtils_ConvexPolygonArea_mAEE1A79D4E09ABD574EC5F45D0773E67D3B459BE,
	GeometryUtils_PolygonInPolygon_m35CAB26692841DCB11AC10E8A54DF8D1D47F8A14,
	GeometryUtils_PolygonsWithinRange_mF033B4054CA9A5A548386526E84D431DB4E80F39,
	GeometryUtils_PolygonsWithinSqRange_m8CE57CCF1132207BA95A03A18D037DC231567A38,
	GeometryUtils_PointOnPolygonBoundsXZ_m1F862C4F99187506184F19A154559FE48A3D4A5A,
	GeometryUtils_PointOnLineSegmentXZ_m1F7ED40B5CBE46F583BC9BB8EE1BAE7F349C0465,
	GeometryUtils_NormalizeRotationKeepingUp_m49CE54E3247404C1F25202D3529903089727AF71,
	GeometryUtils_PolygonUVPoseFromPlanePose_mB2167750B8AB1B1BC3B1A8D440D3DCDED79DCD21,
	GeometryUtils_PolygonVertexToUV_m3CCFAE3CA861E183E88541CD96BEB51C73080564,
	GeometryUtils__cctor_mF1CE5786F59F051799EF179D2358F8D619E8E09D,
	GuidUtil_Compose_mCAE83EA22706DE7E98CAFB30DEDF102AE7922D23,
	HashCodeUtil_Combine_m5EC3008FE1451272AED445DA4B1E7A65BAAF197E,
	HashCodeUtil_ReferenceHash_m55B8BCB31F847BA215571F96A8FC190A53CE9D26,
	HashCodeUtil_Combine_mA6FDC6EBF7EA9CA11137B153C11B7AA893686092,
	HashCodeUtil_Combine_m3359732D408DF911409B24C5801929B253BDB0E6,
	HashCodeUtil_Combine_m0F6C759282B4A222D99729D2DFE43D9142863563,
	HashCodeUtil_Combine_mDC201C9370700E43E7298571C1EC72DBA640FEA4,
	HashCodeUtil_Combine_m43973BA10BC4A787707412CF407F06D39F914186,
	HashCodeUtil_Combine_mB8B9F21479697937FEA02822986270736B8482AF,
	MaterialUtils_GetMaterialClone_m90471305CAA3365785C95A1509E7EB71F35F53C1,
	MaterialUtils_GetMaterialClone_mC2E8D1C2666841C120DD6A585A2989B6095690EF,
	MaterialUtils_CloneMaterials_m683E82E55D34CDC52FA8B8CF0B688A364E42ECB8,
	MaterialUtils_HexToColor_mAA7ADAFF79FEC94826E0CC9EE60B58A390720B11,
	MaterialUtils_HueShift_m87EFA7E2F6E7E669E5F7CBFB7D14FD0BA129CF19,
	MaterialUtils_AddMaterial_mA1434A2D094B315E231ADEE99785570BE8838DF4,
	MathUtility_Approximately_mC16346E0E1DC749401536FF7BAC14FE2209531B6,
	MathUtility_ApproximatelyZero_m7CC58C5F152D873E47CA0E439BF533FD8F76389F,
	MathUtility_Clamp_m1910D49E2E6E9F429499DBC6496DAB1805DEEC64,
	MathUtility_ShortestAngleDistance_m92EC6724359E8F2A5A39368A31E11B6FD7798322,
	MathUtility_ShortestAngleDistance_m7E85583D1BE339B2F4FF8B9FBA35E5453C9A5385,
	MathUtility_IsUndefined_mA9A48B1D332DE89696C37DF98B48546671FC92D7,
	MathUtility_IsAxisAligned_m2D8C03E539A7B4B22E3A8BADF0824F719F9EE466,
	MathUtility_IsPositivePowerOfTwo_m77F04D6B410A9E286031132AD9FFD99B9B858683,
	MathUtility_FirstActiveFlagIndex_m52F16442BC5E44D48C2A794A40AFDDEB7CE1223C,
	MathUtility__cctor_m862195F28CE6C6EB2AADC31778864B97383E1DEF,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	OnDestroyNotifier_get_Destroyed_mC97BC83A97DE90F12FE38BE92C18B7F5C71DCEB3,
	OnDestroyNotifier_set_Destroyed_m3BBB8995C3DD07AE5B21FE33D359335D4579C293,
	OnDestroyNotifier_OnDestroy_m6A54FC8AA273DEC72D2D304C658DC4A3CBD57655,
	OnDestroyNotifier__ctor_m5C2057FAA74A11BB09FCC79C61B3AAEC940E21A7,
	ReflectionUtils_GetCachedAssemblies_m66F7AC089423AAE67CACB1E42850A9DAB633F98C,
	ReflectionUtils_GetCachedTypesPerAssembly_m33A7C1F8017478F270027E469F1C9F7245EA00CA,
	ReflectionUtils_GetCachedAssemblyTypeMaps_m013E40F27D563157E7C095328DA562D92CFFBD9A,
	ReflectionUtils_PreWarmTypeCache_m107F873E4D190D4A93D34D216E3A4DD05F856B1C,
	ReflectionUtils_ForEachAssembly_m3F2199DFBC6BB6B6B87D69A13F354DAA3E4A2AA1,
	ReflectionUtils_ForEachType_m494480789C6CAE697ABBD27025BBB86B56507747,
	ReflectionUtils_FindType_m782AA238FC17C5BE371C8813A548FDD29A50C257,
	ReflectionUtils_FindTypeByFullName_m4D38967AB5DB887F2B5BE624D782027E8CFCF98A,
	ReflectionUtils_FindTypesBatch_mF1DE4F67D23C3DB4EDE95A6B26EF5FA53B04AFC1,
	ReflectionUtils_FindTypesByFullNameBatch_m04379058E9CD31296550FECBC28255442DE2CE97,
	ReflectionUtils_FindTypeInAssemblyByFullName_m2B87E225E054EFD5734F5BE4ADCA29DC8B8D5AFA,
	ReflectionUtils_NicifyVariableName_mEB59FA21C726CC74758FEBF412D17C694C426A63,
	NULL,
	NULL,
	NULL,
	ScriptableSettingsBase_GetInstanceByType_m3562E85AF76D51D5A613777BCBCDCE9552BFCA69,
	ScriptableSettingsBase_Awake_mDCBC0A22C46F740B6D80B53A3B44F5898ADFF8DB,
	ScriptableSettingsBase_OnEnable_m47360EB7C37E28C785121DC715974F28F2E2EA16,
	ScriptableSettingsBase_OnLoaded_m711DBDFE8D215378B0F0076C7C74CA64C25B74D6,
	ScriptableSettingsBase_ValidatePath_m764AA10BED2571607C26751A417785231A1D441B,
	ScriptableSettingsBase__ctor_mEC52E58072A94518570CFFEACF7EFEE3367754BA,
	ScriptableSettingsBase__cctor_mF0722349FA498253B0D7B3CA2C8D6C7A80B8B55D,
	NULL,
	NULL,
	NULL,
	NULL,
	SerializableGuid_get_Empty_m8E564E8473F52D800319FF03B5CC0AA4789F5F31,
	SerializableGuid_get_Guid_m856CA008B9F386B935D3B7F61B3051A9A617F43B,
	SerializableGuid__ctor_m6BA38B84A6B818982E18562E521025FD27C37865,
	SerializableGuid_GetHashCode_m78B52A41CE256433C1F24D6DB86493DEEB75072F,
	SerializableGuid_Equals_mA3F83D68EA7AEEF6041324D3437965F86859A744,
	SerializableGuid_ToString_m7578340F13D51160F53D0685A0884349A1A31CD8,
	SerializableGuid_ToString_mD6C5F07E58267DFAD806FE5FAA71E62574A85114,
	SerializableGuid_ToString_m69259E62FD545E73EC5AEAA9140154749B7A4963,
	SerializableGuid_Equals_mD106FD276E6E41DEFCCBB364CA78E7140F04556B,
	SerializableGuid_op_Equality_mD9CF69ADC96A6E1D9FFAF1201001C43856F1417D,
	SerializableGuid_op_Inequality_m68F78F2A584D9827117B83B8DC095F20B400035E,
	SerializableGuid__cctor_mF750DABEA8F1D410B4B83AB3FE110201EA3B1FB4,
	SerializableGuidUtil_Create_mF231D85E26395ABF6DFC4C1F6401CB05D1A55230,
	TextureUtils_RenderTextureToTexture2D_mD415E3056EECAF12E5FBB0167F57395EAA42724C,
	UndoBlock__ctor_m8D8EF58B3F23AE8430298EFD1114C0D4EF800548,
	UndoBlock_RegisterCreatedObject_mDF025FB713FBCFDCEAEEE0D0B306F644E5B03879,
	UndoBlock_RecordObject_mAF19BA9AB219412F5A16F85A0C2427ADE5E83D2D,
	UndoBlock_SetTransformParent_m2212E4453692E7CF8186DED59E9AFEBFFBC383B4,
	NULL,
	UndoBlock_Dispose_m339DAD7532A1F9604D6CF13DAAE1A3FA7461911A,
	UndoBlock_Dispose_m22B38E379937937C7A1BDB0D40F8C170F8E6C91A,
	UnityObjectUtils_Destroy_m8E56BEBEF22130AEE71F005D30BBEC581444B0EE,
	NULL,
	NULL,
	NULL,
	XRLoggingUtils__cctor_m34C67D615FF5E41B0C2930A2B03E09F63E8F0EB1,
	XRLoggingUtils_Log_m17C6EC7B35C7A0E5EE36C3CBF3E2A953C364CA54,
	XRLoggingUtils_LogWarning_m16C829B8740982FA4B0EBED606A2D518E492A5CC,
	XRLoggingUtils_LogError_m65734BA6FECDC0C2A35DE1E6401886DFD6515402,
	XRLoggingUtils_LogException_m7AE93BFBB883746388CE34909FC8B0874B344082,
	XROrigin_get_Camera_mDEC1EA5E15968845DA812397BBA4506A88B0F9FF,
	XROrigin_set_Camera_m33ECB239BCDB92806AFD47EA8A4685E81EBDFE24,
	XROrigin_get_TrackablesParent_m1762CFBDDF007C80B97379835702D8607DB4F3E0,
	XROrigin_set_TrackablesParent_mE069D24282C732C5164D03FEEDBA8C9FD8AF323E,
	XROrigin_add_TrackablesParentTransformChanged_mA320D83FF126A31F8139C86BC68BE280EE4BC280,
	XROrigin_remove_TrackablesParentTransformChanged_m4D7C48C781798E32FE484B40997BF6BFB5A8BBB0,
	XROrigin_get_Origin_m791424808DC2F95B332CFCE34A260E5F53DEC2AD,
	XROrigin_set_Origin_mA3665C3245F0B16C7C2AA41381FC8D57A80FD6D3,
	XROrigin_get_CameraFloorOffsetObject_mB06A006D2F1211B477E5AADF9DF6EBCB5EFCCF45,
	XROrigin_set_CameraFloorOffsetObject_mE20DF102DDC9F456EE93F97D69A5EEF7306C1F5E,
	XROrigin_get_RequestedTrackingOriginMode_m4FF9E54CD679A9B3CC05B36314D66F3B1AD1688B,
	XROrigin_set_RequestedTrackingOriginMode_m2D7D0AF1006FF55D8EC6D8BF6679F9867E5D496E,
	XROrigin_get_CameraYOffset_mA396D70F0E27E80EA5177915433C5038BACDDF4E,
	XROrigin_set_CameraYOffset_mBA7E721A282BA9E414B1BCA15E82AE3D48C1CAA4,
	XROrigin_get_CurrentTrackingOriginMode_m90D206E17CF4EBD3DDD281F526E43AA3A1A84AFA,
	XROrigin_set_CurrentTrackingOriginMode_mE1F8C7BD93763B7503A9F1A66224B1E99AFB1680,
	XROrigin_get_OriginInCameraSpacePos_mC934EF4E3E14C7DC72A88E058C39F7A40104174C,
	XROrigin_get_CameraInOriginSpacePos_mA25C4AECE774DCE217DF1AB838E9D82BB87BEE56,
	XROrigin_get_CameraInOriginSpaceHeight_m59DC39ADD21E144299A2B7A283414591CC966FF7,
	XROrigin_MoveOffsetHeight_mE85E434BC9ECF1D224C9A7506B814A3D37B2F3EC,
	XROrigin_MoveOffsetHeight_m89E6241C5D67CCD0AC4A166AA4599AADF39E895F,
	XROrigin_TryInitializeCamera_m8A470425D8369F6555D3C36C3335F9D8F82B40A4,
	XROrigin_SetupCamera_m63ECB705B6D6398F5B8B7C2EF7B33CB37C796BC4,
	XROrigin_SetupCamera_mEB39C60D1AE12C2A25647E578B353E127C406726,
	XROrigin_OnInputSubsystemTrackingOriginUpdated_m132A24B0235B1E82289B45F7B5B9132A19A85719,
	XROrigin_RepeatInitializeCamera_mE77E233DAE27A9547E0BE478CC06ED6E51E8917A,
	XROrigin_RotateAroundCameraUsingOriginUp_mFBC31926C33A2A4BB0FB2CD37A5D6A2DB805C26D,
	XROrigin_RotateAroundCameraPosition_mDFFA1FCA35ABE47B99085527C2DBF97141289EC3,
	XROrigin_MatchOriginUp_mF2000A84EAFDFA87164B64B19BC93324AB030C26,
	XROrigin_MatchOriginUpCameraForward_mC4723BFAD3FF3C9ABA7659A74C4CB10800A2C50A,
	XROrigin_MatchOriginUpOriginForward_m22ADB294CA783226D0C50DBDCEE0586A85AC9044,
	XROrigin_MoveCameraToWorldLocation_mBF588A0488903F9FEF0F70015E28772522794397,
	XROrigin_Awake_mB3295310096FFDA1017D3226E81748B301EB93FD,
	XROrigin_GetCameraOriginPose_m65F340AB82A12B79C1CE8A2239C49A4FCA34C3F5,
	XROrigin_OnEnable_m85878F36D9F87D746996935D8BB5A6B1335E6201,
	XROrigin_OnDisable_mCD6CC559152D9231118E30D97F253D6B5B0AE50F,
	XROrigin_OnBeforeRender_m20A25D128427DB95D8645B7B0C42CEC96547023E,
	XROrigin_OnValidate_m107F310112D1458AD2F8E44CD064359B9476BEFE,
	XROrigin_Start_m3E862070874567EBC71CEDFDFCE96346D89872A9,
	XROrigin_OnDestroy_m8F5C01F5CFB4BD23FCC83EA915398067189613B2,
	XROrigin__ctor_mC64C36F5C94F69F20DDCC912292CB0E6614ED40B,
	XROrigin__cctor_mF110E2E7581DC702D61F2AAE789091C9755F244A,
	XROrigin_U3COnValidateU3Eg__IsModeStaleU7C60_0_mFD6964C954EA4827D65DB27D63D003A16594CFB8,
	U3CRepeatInitializeCameraU3Ed__48__ctor_m38D16CB74A912FFAC7BDB663DAC73CCC66F353DB,
	U3CRepeatInitializeCameraU3Ed__48_System_IDisposable_Dispose_m34D86CC2B00D547934EAB0050158EE4FC2316EB2,
	U3CRepeatInitializeCameraU3Ed__48_MoveNext_m2A28BAD8A5F265425E6CED138B0ECD941C8C6D6A,
	U3CRepeatInitializeCameraU3Ed__48_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m85F41DF9153BE60931348D9DA5A5A0B0F5CB24E0,
	U3CRepeatInitializeCameraU3Ed__48_System_Collections_IEnumerator_Reset_m034DF1C46AABE33936127AE4406DAB4F078F88D4,
	U3CRepeatInitializeCameraU3Ed__48_System_Collections_IEnumerator_get_Current_m7C83C7C74A3582ABE2F428AB355A4C5B175B9E09,
	AnimationCurveDatum__ctor_mAC74C57A2E1FA217073BC2FA9F929B60B06C0A96,
	AnimationCurveDatumProperty__ctor_mC666A8B6268F99B4884A55E746C61B23B4D66A78,
	AnimationCurveDatumProperty__ctor_mA03DC328AFEACBB4CDF590EBD92E96B3B6B361C7,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	FloatDatum__ctor_mE4D7819E7E5DD9EAF2E8FAA76B3C84EDFAC484C5,
	FloatDatumProperty__ctor_m7955F8DB25DA2F305FB35CBC77F05A7D6EFA63A3,
	FloatDatumProperty__ctor_m680524511AC200E88B4BE00C84CD1D97EBDDC5AB,
	IntDatum_SetValueRounded_m373370FB48681B70563A6994586DE97AA240C7AA,
	IntDatum__ctor_mB6126495320E9F2B2AE89094D8098511EDBA2364,
	IntDatumProperty__ctor_mF952BB46846F3403A3157166A28F4554A06C236C,
	IntDatumProperty__ctor_m2B931B4B9AB4C29DDDE025AF7CF5AA5930DC354F,
	StringDatum__ctor_m527034C8D2EB313C91B08ED1FF3C1D712DAA066B,
	StringDatumProperty__ctor_m333CCD5E86C742B44863ACBAD85376E3012A5E48,
	StringDatumProperty__ctor_m5DA5D9F7EBC32B1DF726CE2D87069B3C7A1A22A0,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	BindingsGroup_AddBinding_mB862CA624836F2F41878A83077484E5D4F51BBDC,
	BindingsGroup_ClearBinding_mC21C8A6C1A0FD093CD8C1EBB4DCFE33388089154,
	BindingsGroup_Bind_mA4F70711731E8B0918CDEDE84E0BA2285063BCDA,
	BindingsGroup_Unbind_m5F2E23AF75E337E57CD49C17CB644A01026241E6,
	BindingsGroup_Clear_mB28C44D8FC5CBEDCBE41D99AC52A06AD3D3C0024,
	BindingsGroup__ctor_m134F76F56E968839A28997A80A0B8F7272F0B745,
	EventBinding_get_BindAction_mC84020F3A9209984D2268B991678A608B21DBDE9,
	EventBinding_set_BindAction_mBDF58F3C06D243CD7774B052BB5B0A90F5996D78,
	EventBinding_get_UnbindAction_m0F29A3DEC5CAC0716802A686F982DC6739C8EAA9,
	EventBinding_set_UnbindAction_m0F0D30CC2B17576CA59618091D698F91C0FA28E9,
	EventBinding_get_IsBound_m1DE1DD30C013662343D545313F4F0B9FB7C5859D,
	EventBinding__ctor_m9FBD9CE6AB00A30B99020ADDDB3642EB1D4ED55E,
	EventBinding_Bind_m2114894CB4EC2EC35EF0A4068F0CFAE4955C9385,
	EventBinding_Unbind_mA28A0372DAF5CF6757FF016EB2D9CA0E405A0A33,
	EventBinding_ClearBinding_m464FF9805BA626E2447B53C6FC273D9F79FE5BF1,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	EnumDisplayAttribute__ctor_mE08D80992FC58ED07B0232B89DDE33766AD09A9D,
	FlagsPropertyAttribute__ctor_mF81FEDE55981A37924F05D5EF9F6DD0AC8F6BCB2,
};
extern void ARTrackablesParentTransformChangedEventArgs_get_Origin_m635FF91C20A6D6C0514EABDDED14289D01BDC0A5_AdjustorThunk (void);
extern void ARTrackablesParentTransformChangedEventArgs_get_TrackablesParent_mC93ACA08A0A5E3F00A58F63EF932BD6AA768BA43_AdjustorThunk (void);
extern void ARTrackablesParentTransformChangedEventArgs__ctor_m90A574F56EAF0228BD1D7CBFDAF682B8E58F7F9D_AdjustorThunk (void);
extern void ARTrackablesParentTransformChangedEventArgs_Equals_mDEA555E578C8B4ED1C2EB86EC70E289D6A542DEE_AdjustorThunk (void);
extern void ARTrackablesParentTransformChangedEventArgs_Equals_mD1E41855005B1E2CF7834216ECA32621B6229D53_AdjustorThunk (void);
extern void ARTrackablesParentTransformChangedEventArgs_GetHashCode_mDBE0D34111420DBF1A446062AD324FCDE522B51A_AdjustorThunk (void);
extern void SerializableGuid_get_Guid_m856CA008B9F386B935D3B7F61B3051A9A617F43B_AdjustorThunk (void);
extern void SerializableGuid__ctor_m6BA38B84A6B818982E18562E521025FD27C37865_AdjustorThunk (void);
extern void SerializableGuid_GetHashCode_m78B52A41CE256433C1F24D6DB86493DEEB75072F_AdjustorThunk (void);
extern void SerializableGuid_Equals_mA3F83D68EA7AEEF6041324D3437965F86859A744_AdjustorThunk (void);
extern void SerializableGuid_ToString_m7578340F13D51160F53D0685A0884349A1A31CD8_AdjustorThunk (void);
extern void SerializableGuid_ToString_mD6C5F07E58267DFAD806FE5FAA71E62574A85114_AdjustorThunk (void);
extern void SerializableGuid_ToString_m69259E62FD545E73EC5AEAA9140154749B7A4963_AdjustorThunk (void);
extern void SerializableGuid_Equals_mD106FD276E6E41DEFCCBB364CA78E7140F04556B_AdjustorThunk (void);
extern void EventBinding_get_BindAction_mC84020F3A9209984D2268B991678A608B21DBDE9_AdjustorThunk (void);
extern void EventBinding_set_BindAction_mBDF58F3C06D243CD7774B052BB5B0A90F5996D78_AdjustorThunk (void);
extern void EventBinding_get_UnbindAction_m0F29A3DEC5CAC0716802A686F982DC6739C8EAA9_AdjustorThunk (void);
extern void EventBinding_set_UnbindAction_m0F0D30CC2B17576CA59618091D698F91C0FA28E9_AdjustorThunk (void);
extern void EventBinding_get_IsBound_m1DE1DD30C013662343D545313F4F0B9FB7C5859D_AdjustorThunk (void);
extern void EventBinding__ctor_m9FBD9CE6AB00A30B99020ADDDB3642EB1D4ED55E_AdjustorThunk (void);
extern void EventBinding_Bind_m2114894CB4EC2EC35EF0A4068F0CFAE4955C9385_AdjustorThunk (void);
extern void EventBinding_Unbind_mA28A0372DAF5CF6757FF016EB2D9CA0E405A0A33_AdjustorThunk (void);
extern void EventBinding_ClearBinding_m464FF9805BA626E2447B53C6FC273D9F79FE5BF1_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[23] = 
{
	{ 0x06000003, ARTrackablesParentTransformChangedEventArgs_get_Origin_m635FF91C20A6D6C0514EABDDED14289D01BDC0A5_AdjustorThunk },
	{ 0x06000004, ARTrackablesParentTransformChangedEventArgs_get_TrackablesParent_mC93ACA08A0A5E3F00A58F63EF932BD6AA768BA43_AdjustorThunk },
	{ 0x06000005, ARTrackablesParentTransformChangedEventArgs__ctor_m90A574F56EAF0228BD1D7CBFDAF682B8E58F7F9D_AdjustorThunk },
	{ 0x06000006, ARTrackablesParentTransformChangedEventArgs_Equals_mDEA555E578C8B4ED1C2EB86EC70E289D6A542DEE_AdjustorThunk },
	{ 0x06000007, ARTrackablesParentTransformChangedEventArgs_Equals_mD1E41855005B1E2CF7834216ECA32621B6229D53_AdjustorThunk },
	{ 0x06000008, ARTrackablesParentTransformChangedEventArgs_GetHashCode_mDBE0D34111420DBF1A446062AD324FCDE522B51A_AdjustorThunk },
	{ 0x060000E2, SerializableGuid_get_Guid_m856CA008B9F386B935D3B7F61B3051A9A617F43B_AdjustorThunk },
	{ 0x060000E3, SerializableGuid__ctor_m6BA38B84A6B818982E18562E521025FD27C37865_AdjustorThunk },
	{ 0x060000E4, SerializableGuid_GetHashCode_m78B52A41CE256433C1F24D6DB86493DEEB75072F_AdjustorThunk },
	{ 0x060000E5, SerializableGuid_Equals_mA3F83D68EA7AEEF6041324D3437965F86859A744_AdjustorThunk },
	{ 0x060000E6, SerializableGuid_ToString_m7578340F13D51160F53D0685A0884349A1A31CD8_AdjustorThunk },
	{ 0x060000E7, SerializableGuid_ToString_mD6C5F07E58267DFAD806FE5FAA71E62574A85114_AdjustorThunk },
	{ 0x060000E8, SerializableGuid_ToString_m69259E62FD545E73EC5AEAA9140154749B7A4963_AdjustorThunk },
	{ 0x060000E9, SerializableGuid_Equals_mD106FD276E6E41DEFCCBB364CA78E7140F04556B_AdjustorThunk },
	{ 0x0600016F, EventBinding_get_BindAction_mC84020F3A9209984D2268B991678A608B21DBDE9_AdjustorThunk },
	{ 0x06000170, EventBinding_set_BindAction_mBDF58F3C06D243CD7774B052BB5B0A90F5996D78_AdjustorThunk },
	{ 0x06000171, EventBinding_get_UnbindAction_m0F29A3DEC5CAC0716802A686F982DC6739C8EAA9_AdjustorThunk },
	{ 0x06000172, EventBinding_set_UnbindAction_m0F0D30CC2B17576CA59618091D698F91C0FA28E9_AdjustorThunk },
	{ 0x06000173, EventBinding_get_IsBound_m1DE1DD30C013662343D545313F4F0B9FB7C5859D_AdjustorThunk },
	{ 0x06000174, EventBinding__ctor_m9FBD9CE6AB00A30B99020ADDDB3642EB1D4ED55E_AdjustorThunk },
	{ 0x06000175, EventBinding_Bind_m2114894CB4EC2EC35EF0A4068F0CFAE4955C9385_AdjustorThunk },
	{ 0x06000176, EventBinding_Unbind_mA28A0372DAF5CF6757FF016EB2D9CA0E405A0A33_AdjustorThunk },
	{ 0x06000177, EventBinding_ClearBinding_m464FF9805BA626E2447B53C6FC273D9F79FE5BF1_AdjustorThunk },
};
static const int32_t s_InvokerIndices[421] = 
{
	3220,
	3220,
	3159,
	3159,
	1409,
	2041,
	2125,
	3136,
	4463,
	4463,
	3220,
	3159,
	2500,
	4790,
	4790,
	4790,
	4790,
	-1,
	4790,
	5183,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	4465,
	4561,
	5021,
	4561,
	-1,
	5183,
	-1,
	4657,
	4657,
	4657,
	4221,
	4221,
	4665,
	4183,
	-1,
	-1,
	4854,
	4507,
	-1,
	-1,
	-1,
	4436,
	4597,
	4597,
	4979,
	4979,
	4979,
	5067,
	4932,
	4932,
	5183,
	4968,
	4968,
	4662,
	4662,
	4435,
	4435,
	4440,
	4233,
	4661,
	4661,
	4233,
	4047,
	4047,
	4231,
	4231,
	3907,
	-1,
	-1,
	4413,
	4932,
	4932,
	4932,
	4932,
	4519,
	4047,
	5183,
	3220,
	2500,
	3220,
	3220,
	3220,
	3220,
	3220,
	3220,
	3220,
	3220,
	3220,
	5038,
	5025,
	5025,
	5038,
	5046,
	5026,
	5026,
	5046,
	4601,
	4601,
	4601,
	5067,
	5067,
	5154,
	4932,
	4050,
	4057,
	3797,
	4413,
	4661,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	4661,
	4413,
	5183,
	3220,
	2125,
	3843,
	4596,
	4223,
	3373,
	3373,
	3373,
	4148,
	3622,
	4544,
	4544,
	4148,
	4519,
	5043,
	4588,
	3329,
	4978,
	5021,
	4519,
	4110,
	4110,
	4121,
	3853,
	4979,
	4969,
	4144,
	5183,
	4309,
	4341,
	4855,
	3981,
	3676,
	3455,
	3385,
	3359,
	3344,
	4932,
	4932,
	4932,
	4792,
	4283,
	4661,
	4534,
	5003,
	3965,
	3644,
	3858,
	5003,
	5007,
	4994,
	4850,
	5183,
	-1,
	-1,
	-1,
	-1,
	-1,
	3159,
	2500,
	3220,
	3220,
	5154,
	5154,
	5154,
	5183,
	5067,
	5067,
	4932,
	4932,
	4661,
	4661,
	4413,
	4932,
	-1,
	-1,
	-1,
	4932,
	3220,
	3220,
	3220,
	4513,
	3220,
	5183,
	-1,
	-1,
	-1,
	-1,
	5176,
	3118,
	1380,
	3136,
	2125,
	3159,
	1896,
	935,
	2153,
	4533,
	4533,
	5183,
	5012,
	4661,
	1413,
	2500,
	2500,
	1409,
	-1,
	2522,
	3220,
	4665,
	-1,
	-1,
	-1,
	5183,
	4661,
	4661,
	4661,
	4661,
	3159,
	2500,
	3159,
	2500,
	2500,
	2500,
	3159,
	2500,
	3159,
	2500,
	3136,
	2480,
	3192,
	2525,
	3136,
	2480,
	3216,
	3216,
	3192,
	3220,
	2525,
	3220,
	3188,
	2125,
	2500,
	3159,
	2155,
	1063,
	2181,
	1064,
	1064,
	2181,
	3220,
	3170,
	3220,
	3220,
	3220,
	3220,
	3220,
	3220,
	3220,
	5183,
	3188,
	2480,
	3220,
	3188,
	3159,
	3220,
	3159,
	3220,
	2500,
	2500,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	3220,
	2525,
	2500,
	2525,
	3220,
	2480,
	2500,
	3220,
	2500,
	2500,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	2500,
	2500,
	3220,
	3220,
	3220,
	3220,
	3159,
	2500,
	3159,
	2500,
	3188,
	1409,
	3220,
	3220,
	3220,
	3188,
	3220,
	3220,
	3220,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	2500,
	3220,
};
static const Il2CppTokenRangePair s_rgctxIndices[42] = 
{
	{ 0x0200000A, { 7, 32 } },
	{ 0x0200000B, { 43, 10 } },
	{ 0x0200000C, { 53, 7 } },
	{ 0x0200000E, { 63, 3 } },
	{ 0x02000033, { 111, 7 } },
	{ 0x02000036, { 118, 8 } },
	{ 0x02000038, { 126, 3 } },
	{ 0x02000044, { 158, 4 } },
	{ 0x02000045, { 162, 5 } },
	{ 0x0200004C, { 167, 31 } },
	{ 0x02000050, { 198, 4 } },
	{ 0x02000051, { 202, 6 } },
	{ 0x02000052, { 208, 2 } },
	{ 0x02000053, { 210, 23 } },
	{ 0x02000054, { 233, 4 } },
	{ 0x02000055, { 237, 11 } },
	{ 0x02000056, { 248, 10 } },
	{ 0x06000012, { 0, 7 } },
	{ 0x06000018, { 39, 2 } },
	{ 0x06000019, { 41, 2 } },
	{ 0x06000025, { 60, 3 } },
	{ 0x0600002B, { 66, 4 } },
	{ 0x0600002D, { 70, 4 } },
	{ 0x06000035, { 74, 5 } },
	{ 0x06000036, { 79, 4 } },
	{ 0x06000039, { 83, 2 } },
	{ 0x0600003A, { 85, 2 } },
	{ 0x0600003B, { 87, 2 } },
	{ 0x06000056, { 89, 2 } },
	{ 0x06000057, { 91, 1 } },
	{ 0x0600007F, { 92, 3 } },
	{ 0x06000080, { 95, 4 } },
	{ 0x06000081, { 99, 2 } },
	{ 0x06000082, { 101, 2 } },
	{ 0x06000083, { 103, 1 } },
	{ 0x06000084, { 104, 1 } },
	{ 0x06000085, { 105, 1 } },
	{ 0x060000BE, { 106, 5 } },
	{ 0x060000F3, { 129, 1 } },
	{ 0x060000F7, { 130, 3 } },
	{ 0x060000F8, { 133, 10 } },
	{ 0x060000F9, { 143, 15 } },
};
extern const uint32_t g_rgctx_List_1_get_Count_m2CCC5537A021D23F6774955747FCCA073E199332;
extern const uint32_t g_rgctx_List_1_get_Item_m47CE5430D357E1D535051A05EAEDE996799F31C8;
extern const uint32_t g_rgctx_T_t782956A42D23A5A08D0B3CE547BEEBB53FCD81FB;
extern const uint32_t g_rgctx_List_1_GetEnumerator_m8D1CBD1FD71F1858CC93311AF44B65276BC90C13;
extern const uint32_t g_rgctx_Enumerator_get_Current_m93926E2DB28A76F4CCF0C478BDCCB2DD1176EA3E;
extern const uint32_t g_rgctx_Enumerator_MoveNext_mBE5F4D1E525C3FDF3F97262DE90B514E5F5F6996;
extern const uint32_t g_rgctx_Enumerator_tC280924931E67C964F6E8319C1DC0A50BB998A86;
extern const uint32_t g_rgctx_CollectionPool_2_GetCollection_mCBA9657585DAA516C5EE9BA90067E6CED2D0135A;
extern const uint32_t g_rgctx_CollectionPool_2_t5A55155F82289E0E878825DB4AE7A4DFE564E471;
extern const uint32_t g_rgctx_CachedComponentFilter_2_tDC5240BD513EFE9CBB73B40E639C10307754F5AC;
extern const uint32_t g_rgctx_List_1_Clear_mF9EFC55C1F4479E7DE943E57424C0D38D576BDD7;
extern const uint32_t g_rgctx_List_1_Clear_m13BF16A78A8F0BDDEB7C9E84AD3AFF5B41D61FF8;
extern const uint32_t g_rgctx_TRootType_t6656F32142172AF3FBF210D4D798F3E59C20F1A7;
extern const uint32_t g_rgctx_Component_GetComponents_TisTFilterType_t4020824B9F4769153A771DB6FE0F3608376A6245_m552F8CEDC916FF828558B7DC32B818EDF110A908;
extern const uint32_t g_rgctx_Component_GetComponents_TisIComponentHost_1_tCDBF1749CA1E45C01C9A9B414A91D8A6F0980157_mFCDFB6E0E15154A6E244C8B15FBA94E048ECAC34;
extern const uint32_t g_rgctx_CachedComponentFilter_2_FilteredCopyToMaster_mCCD9C6D9B1D1CB27AAB7B442E1C7B2AFC58856BF;
extern const uint32_t g_rgctx_Component_GetComponent_TisTRootType_t6656F32142172AF3FBF210D4D798F3E59C20F1A7_mF6C764D681E387E30742825AA5D519BE908B11BA;
extern const uint32_t g_rgctx_Component_GetComponentsInChildren_TisTFilterType_t4020824B9F4769153A771DB6FE0F3608376A6245_mC111AF621E1148CA73A6CABF74529F135D754CDA;
extern const uint32_t g_rgctx_Component_GetComponentsInChildren_TisIComponentHost_1_tCDBF1749CA1E45C01C9A9B414A91D8A6F0980157_mF6A0C148F2AA0AC0147B78BDF43DC1E46254C6CC;
extern const uint32_t g_rgctx_CachedComponentFilter_2_FilteredCopyToMaster_mBE29938FF6CBEBC19C3A9B77BE3AF6B4731D9F32;
extern const uint32_t g_rgctx_List_1_AddRange_mA4D4E0AF3DD92C0F81C1F7937E76EAA45A58E3A5;
extern const uint32_t g_rgctx_List_1_GetEnumerator_mF28337D45533EA47F60E4560F1E085FE4F4D63D5;
extern const uint32_t g_rgctx_Enumerator_get_Current_m6F5ACEB62C30631268825B90988C8DC5FFFA4BE5;
extern const uint32_t g_rgctx_TFilterType_t4020824B9F4769153A771DB6FE0F3608376A6245;
extern const uint32_t g_rgctx_Enumerator_MoveNext_m121939A1F6929BFC799A3CEE28E1950972887B5C;
extern const uint32_t g_rgctx_Enumerator_t4C2CBCA1F5C6504050164DEAE6FF0052A1B29403;
extern const uint32_t g_rgctx_List_1_GetEnumerator_m982A5A739534396E07DE9372DDF63AE90A7C4468;
extern const uint32_t g_rgctx_Enumerator_get_Current_m1229D9E8B456ACAE671B76ADC7DBA0A6B9B808CE;
extern const uint32_t g_rgctx_IComponentHost_1_tCDBF1749CA1E45C01C9A9B414A91D8A6F0980157;
extern const uint32_t g_rgctx_Enumerator_MoveNext_mFF2550624367A85D99B3146839D5385583CBE0D2;
extern const uint32_t g_rgctx_Enumerator_tFEC8C0DBE551369CA15A42DE4AAD94E77864649E;
extern const uint32_t g_rgctx_List_1_Add_mEC658B99382EC7C83C28E8B1392D4329743B1C07;
extern const uint32_t g_rgctx_Component_GetComponentInParent_TisTRootType_t6656F32142172AF3FBF210D4D798F3E59C20F1A7_mE164CEF3EEA281E389C25AC6C01A3509CAAAE695;
extern const uint32_t g_rgctx_CollectionPool_2_RecycleCollection_mCA94D2B40969E852BFBBD83A9AD10A1B3B57B19C;
extern const uint32_t g_rgctx_CachedComponentFilter_2_Dispose_m7DEE83C1346ED00A4A2CDF76CE4DC6441C83C250;
extern const uint32_t g_rgctx_List_1_t89605CE7D994E40B86B3DDA74C2C6951056E512A;
extern const uint32_t g_rgctx_List_1__ctor_m4EB253D2F835CA3868B4168F0D4DDF525F228320;
extern const uint32_t g_rgctx_List_1_tAD2AAD1FA2C184DC8F6255140BEE49904D55920D;
extern const uint32_t g_rgctx_List_1__ctor_m2792B841B87932597AD04B348DDF3596252635B9;
extern const uint32_t g_rgctx_TChildType_t8DAB93F970F5445B8A4E606BA37668F50E83FE6A;
extern const uint32_t g_rgctx_List_1_Add_mC5EE430ECBFEF0E682A2A64F3B6958F88D9DD3E6;
extern const uint32_t g_rgctx_TChildType_t1B56FCFFADE69A0388F9C8EB404AEE2E48DFD969;
extern const uint32_t g_rgctx_TChildTypeU5BU5D_t0EE6AB8DD22CC1262155977E7AC61E3C47BDF6CC;
extern const uint32_t g_rgctx_CollectionPool_2_tB2DA9F111A37B71DA8FD9DFCA08CD2C0CBFED5A6;
extern const uint32_t g_rgctx_Queue_1_get_Count_m7328DBD135918795F0AC56D1936B62370E7E3237;
extern const uint32_t g_rgctx_Activator_CreateInstance_TisTCollection_tAE554EA8BF8423716D6221246968276B78325351_mBBC3B4EF8B02E011DA33E6ECDDEE91FE561F9A1D;
extern const uint32_t g_rgctx_Queue_1_Dequeue_mD9749228621D69DE6711384ED2BE472D82242C4A;
extern const uint32_t g_rgctx_TCollection_tAE554EA8BF8423716D6221246968276B78325351;
extern const uint32_t g_rgctx_ICollection_1_tF401A0AF5A7BB4FCA94734411495A89B8A6A59DA;
extern const uint32_t g_rgctx_ICollection_1_Clear_m002383D7C3E48F574E6589D14B9F9EE8730EC7DC;
extern const uint32_t g_rgctx_Queue_1_Enqueue_m9944CA7F3580A82B40B2F76A3A4718121CB8C670;
extern const uint32_t g_rgctx_Queue_1_t03A1F96D63D9B83614AD2EB8BF49D076E7DAC076;
extern const uint32_t g_rgctx_Queue_1__ctor_mCDACD0616420281A6C55E5D0DC7FD72308D1B2EC;
extern const uint32_t g_rgctx_ComponentUtils_1_tEE8092F19444C84D2F710096BEE7E66CC76DC340;
extern const uint32_t g_rgctx_GameObject_GetComponents_TisT_t3BC2741207FBF68D9CABFD84B5E105DEB153CCDB_mAC5A3B9066A81F520C7E294540E650ED1A301DBA;
extern const uint32_t g_rgctx_List_1_get_Count_m65D432487265A3178458C5F9A8680B711651221B;
extern const uint32_t g_rgctx_List_1_get_Item_m56370F8FA1FCFFDF649E27D9BDE632645FB72BC8;
extern const uint32_t g_rgctx_GameObject_GetComponentsInChildren_TisT_t3BC2741207FBF68D9CABFD84B5E105DEB153CCDB_mE5C8E2394DAF135B3923BC8C767978D3948B3AD2;
extern const uint32_t g_rgctx_List_1_t247C5B3353A3AF610B8BACFB6B6D861379FCB842;
extern const uint32_t g_rgctx_List_1__ctor_mDC6D6CBCF088572358214BFCBF4C1B78E0249094;
extern const uint32_t g_rgctx_GameObject_GetComponent_TisT_tACA6A29EB564FBC587FD27AFE3C19D0439CC2902_m58B150AED00E59094DEF90D38746FD71A76593EA;
extern const uint32_t g_rgctx_T_tACA6A29EB564FBC587FD27AFE3C19D0439CC2902;
extern const uint32_t g_rgctx_GameObject_AddComponent_TisT_tACA6A29EB564FBC587FD27AFE3C19D0439CC2902_m6B3350977DBC4450D16C29E6228F9909E058A1C0;
extern const uint32_t g_rgctx_T_t646DFD494905AA196AB6F594EBADAE13D73F2CEB;
extern const uint32_t g_rgctx_TU5BU5D_tC0999A7E241E9C472ED88D6BCE7605D8A1C7E271;
extern const uint32_t g_rgctx_EnumValues_1_t758610FB7FA57F5DDCCE1E1B71FED5B85E4FB7FE;
extern const uint32_t g_rgctx_ICollection_1_t5827F10DAFB40C87E4504E0A7D4D0AB3F50298C3;
extern const uint32_t g_rgctx_IEnumerable_1_t67C0E088A7F797511A53E369410C99BE9ED47528;
extern const uint32_t g_rgctx_IEnumerator_1_t9419F4E0518227781B373CC3FDD3107B73AEAF0C;
extern const uint32_t g_rgctx_T_tF2E3000B412DB27DDFD26EF6E74D77CEE4C5EBB0;
extern const uint32_t g_rgctx_Dictionary_2_GetEnumerator_mC30F80A06C7B51E718317583674B808069780E42;
extern const uint32_t g_rgctx_Enumerator_MoveNext_m70F4EE52E96AFBE8DCFCB75AD42EBCD14755BC4D;
extern const uint32_t g_rgctx_Enumerator_get_Current_m0A229C7F750024DACE576F82A5FFB9863B143134;
extern const uint32_t g_rgctx_Enumerator_Dispose_mB39D4A02092A3EC8733F45D32D9E914F5BF0B940;
extern const uint32_t g_rgctx_HashSet_1_GetEnumerator_m6D724C0B15E2AE2A5F3C9AE8223C0FEB0DA9536A;
extern const uint32_t g_rgctx_Enumerator_get_Current_mBC9F1719C2C69D9AE2D1A877BA38EA0569A05695;
extern const uint32_t g_rgctx_HashSet_1_Remove_m306CCE5A1B475CA8C53D78142D39B61E22971CD2;
extern const uint32_t g_rgctx_Enumerator_MoveNext_mFABD7F409302E13C16457688A303C38C431D8287;
extern const uint32_t g_rgctx_Enumerator_t07FB3027090258095B02DE40B4B8421162174051;
extern const uint32_t g_rgctx_HashSet_1_GetEnumerator_m2675CC256DB8B9EC43DBB9B099B204EBF011D4F8;
extern const uint32_t g_rgctx_Enumerator_MoveNext_m803EB5407CD56F0CC03308A15A63758E8C4B5375;
extern const uint32_t g_rgctx_Enumerator_get_Current_m519EC456F44BC51B187E441D2BD802958024FFB0;
extern const uint32_t g_rgctx_Enumerator_Dispose_m18672D3E12A34E2CC447E785D491507C64F738AB;
extern const uint32_t g_rgctx_Activator_CreateInstance_TisT_t6124A8764A5700536AEF7572A7B38185EDACE31C_mDF3C3C059B4EA9E73A55509C585108A9CDC34C9C;
extern const uint32_t g_rgctx_List_1_Add_m92A96A7614E67195701CC7E86B87BE29C5AC1ABB;
extern const uint32_t g_rgctx_List_1_get_Capacity_mEE27ECE7F5F89DD69FED69810CD027B257F03139;
extern const uint32_t g_rgctx_List_1_set_Capacity_m7820211AEE1858C33AB3FF17952B055DFA5872A4;
extern const uint32_t g_rgctx_List_1_get_Item_mE2D1CE9CDA13E3D5D54AFB47D35B317DA2789D63;
extern const uint32_t g_rgctx_List_1_set_Item_m25F15EA57C3081BA64E0B14794F7799CDB802E90;
extern const uint32_t g_rgctx_TAttribute_t13961EE5C179450545E4DEC49759903786FB0DF0;
extern const uint32_t g_rgctx_TAttribute_t13961EE5C179450545E4DEC49759903786FB0DF0;
extern const uint32_t g_rgctx_TAttribute_tF6DD1A15775EACE2E725BC14AD65EB75F5D4D02C;
extern const uint32_t g_rgctx_GameObject_GetComponentInChildren_TisT_tC16606CA819499AAFE98B96291393D9A85F509CE_m34E0FF15A75BC18ED6400972415ECE2F5F953283;
extern const uint32_t g_rgctx_T_tC16606CA819499AAFE98B96291393D9A85F509CE;
extern const uint32_t g_rgctx_Object_FindObjectOfType_TisT_tC16606CA819499AAFE98B96291393D9A85F509CE_m99F20A7A7D7EEFEA17CD18E8B6D49BDB7EE66207;
extern const uint32_t g_rgctx_GameObject_GetComponentsInChildren_TisT_t1B49459DB8A2A8CA0FD665939BFC2D8E9D68DEA8_mAD4E76D6CBF56B1064ECB28B4606BBA5E354A585;
extern const uint32_t g_rgctx_T_t1B49459DB8A2A8CA0FD665939BFC2D8E9D68DEA8;
extern const uint32_t g_rgctx_GameObject_GetComponent_TisT_t1B49459DB8A2A8CA0FD665939BFC2D8E9D68DEA8_m50C5491180DFD399D1C3C1324B6DC8ABDEC168F6;
extern const uint32_t g_rgctx_Object_FindObjectOfType_TisT_t1B49459DB8A2A8CA0FD665939BFC2D8E9D68DEA8_m9EEF83D1970396E77131B1BCDC932A09A8601F45;
extern const uint32_t g_rgctx_GameObject_GetComponentInChildren_TisT_t894153C1C6AC3871A14691F27FC3FB7990E06F5C_m2418C83D746F66DF95B249AF5A2A4B3FA7273A8F;
extern const uint32_t g_rgctx_T_t894153C1C6AC3871A14691F27FC3FB7990E06F5C;
extern const uint32_t g_rgctx_GameObject_GetComponentsInChildren_TisT_t744FAD3FA707CBFC5058262C7116F5D3FB6DCD29_m47A0614856F69595569136AC8AEC6DBD6629A55D;
extern const uint32_t g_rgctx_List_1_AddRange_m25477A9D2A12225E948AE80328C8A98AE170CC48;
extern const uint32_t g_rgctx_GameObjectUtils_GetComponentInScene_TisT_t55CDCD2D64F1F3CB91AB093B6C4323BD166BAF2F_mDA60ECC99E7F5647BF050A26324B6DE1135E755B;
extern const uint32_t g_rgctx_GameObjectUtils_GetComponentsInScene_TisT_t02A31073F4CD1FB58597A8FCBB793DDA99E3B80E_m20509CE9025228A9CDFE8D05657A59C0AD679F77;
extern const uint32_t g_rgctx_GameObjectUtils_GetComponentsInScene_TisT_t9AE149219F45540D75D0BAFE2AC5282D9A63B5B4_m53EEEEE4FF90B3140ED44F1511FAA5CE2A83CA81;
extern const uint32_t g_rgctx_NativeArray_1_get_Length_mF274D091287F4E165CAA572890E524DF8803363C;
extern const uint32_t g_rgctx_NativeArray_1_get_IsCreated_m92C39F5ACA8D5F55026D94CA48850FE3DCD07643;
extern const uint32_t g_rgctx_NativeArray_1_Dispose_m333D2E710D8561B766B21A2F2ABD20B494502776;
extern const uint32_t g_rgctx_NativeArray_1_t8C470801D8F552B595557F2A2BDECF99F1BD8AD1;
extern const uint32_t g_rgctx_NativeArray_1__ctor_m7CFC8C958DEA5D1B2DDAF5176226A78D1E58DC19;
extern const uint32_t g_rgctx_Queue_1_get_Count_m53ACAD1137EF982835E9AE3975029456B79FD2B9;
extern const uint32_t g_rgctx_Queue_1_Dequeue_m3CA7E1103C56CF32459AB5890A031D9AFB1C19D0;
extern const uint32_t g_rgctx_Activator_CreateInstance_TisT_t41BB3F5E25CEA261101561FBC07E94FBC901EC5F_m511B8565D91B4FD59E4F3CC1517D0F35B51CEF84;
extern const uint32_t g_rgctx_ObjectPool_1_ClearInstance_m00115ECE4E1FB7561804357BC4FFAD54BAF53550;
extern const uint32_t g_rgctx_Queue_1_Enqueue_m3C181A7B38334597526803EB4C7AAB49FD790A50;
extern const uint32_t g_rgctx_Queue_1_t0278564DCEFAD0C9951B1C8B8B213337E5E84EE1;
extern const uint32_t g_rgctx_Queue_1__ctor_m830296A356E3CEEBC6719B39805579927D74EA81;
extern const uint32_t g_rgctx_ScriptableSettingsBase_1_t19D5BC2591DC4416951A4EF3183AA2DC68E739FF;
extern const uint32_t g_rgctx_T_tB2B6DD5AF29D3AE6E4835AC2359A392295B24FC7;
extern const uint32_t g_rgctx_ScriptableSettings_1_CreateAndLoad_m2ED16A96514EE5D20CDB7EF736B77E3F3FC32493;
extern const uint32_t g_rgctx_ScriptableSettings_1_tD56CF73029CC1C1C770E893FB0DAD0D94123809E;
extern const uint32_t g_rgctx_ScriptableSettingsBase_1_GetFilePath_m95681C5B0E5545DB3BA9E11A560083366073DA4D;
extern const uint32_t g_rgctx_ScriptableObject_CreateInstance_TisT_tB2B6DD5AF29D3AE6E4835AC2359A392295B24FC7_m758198DDA81B241BC062C2517DFF935817A42504;
extern const uint32_t g_rgctx_ScriptableSettingsBase_1_Save_mEE06AD1D750DD8B133CE01FBD64C851BC735179F;
extern const uint32_t g_rgctx_ScriptableSettingsBase_1__ctor_m2EC67AC0F2AF5B95B904918335B1DC45847BF667;
extern const uint32_t g_rgctx_ScriptableSettingsBase_1_t0049D77678ABE672AA85F7A15DDEAC4EC8E129D0;
extern const uint32_t g_rgctx_T_t3CCF25A3981B154E1F510C46B9DB662D6F90D27C;
extern const uint32_t g_rgctx_T_t3CCF25A3981B154E1F510C46B9DB662D6F90D27C;
extern const uint32_t g_rgctx_GameObject_AddComponent_TisT_t17F1FEAE24D5C4C43E0699DD8420C9CF1D27382A_m2A079ADF83CCB064D1F427C4E28F66360F38513C;
extern const uint32_t g_rgctx_T_t2599377CB501F3CAF9013461AF3E56AF2968545D;
extern const uint32_t g_rgctx_GameObject_GetComponent_TisT_t2599377CB501F3CAF9013461AF3E56AF2968545D_m7C19B4FA78D47F6A6428E2B01F30B87349CB4E2D;
extern const uint32_t g_rgctx_Component_GetComponent_TisT_t2599377CB501F3CAF9013461AF3E56AF2968545D_m207DAD31E9CCDD5BBD3C79E99CF465B4D4736D77;
extern const uint32_t g_rgctx_CollectionPool_2_GetCollection_mFB10EB6253F5D3440DE7CB6CD603CCEEED2F16C5;
extern const uint32_t g_rgctx_CollectionPool_2_t8315E65048291BDC44BBC021093C926F1E3B13DA;
extern const uint32_t g_rgctx_List_1_GetEnumerator_m0FE139CDAA83F2F018E50E4FB0679273BBE18BAD;
extern const uint32_t g_rgctx_Enumerator_get_Current_m674D1F6B4C73AA6C02F509CA3411C9DFD9D93435;
extern const uint32_t g_rgctx_T_tA4711439D2D7024848E50CC76205CE721F819CF8;
extern const uint32_t g_rgctx_List_1_Add_mA11C84B8C39EC3B62E4B52930FB107C387F33582;
extern const uint32_t g_rgctx_Enumerator_MoveNext_mA3ECE48FE18A3015E8116EEF73415185C3A87C0A;
extern const uint32_t g_rgctx_Enumerator_tBCB903DACDB9F855AF33B905AEA3C1ACDD25A617;
extern const uint32_t g_rgctx_List_1_Remove_mE937D22F8BBAB1C094FE386FC043701FC1219E9D;
extern const uint32_t g_rgctx_CollectionPool_2_RecycleCollection_m95132B1869321A7092F76232B4DCDC6661671E2A;
extern const uint32_t g_rgctx_CollectionPool_2_GetCollection_m50DE3378B8341438BD7A9F3A7EC88E609DDEE4D0;
extern const uint32_t g_rgctx_CollectionPool_2_tDCF50DCF3AD42655B3B9C35F396EE791475F5DF8;
extern const uint32_t g_rgctx_Dictionary_2_GetEnumerator_mE9CD7C7B0CB4FA7DF98C68B93BC98EBCE7D37BE1;
extern const uint32_t g_rgctx_Enumerator_get_Current_m2BC45B84B089EAE8EB9DFC53D2E17CA04E87D368;
extern const uint32_t g_rgctx_KeyValuePair_2_get_Key_m4B024CAF0CA5B1B80C524186C75A1CC511C59A88;
extern const uint32_t g_rgctx_TKey_t0A55C1B1B4FCC085A17AB71751FCA408CD75636B;
extern const uint32_t g_rgctx_List_1_Add_m782362DEC749352FA4092147B985A267E33F7550;
extern const uint32_t g_rgctx_Enumerator_MoveNext_m890FB782710FD23E70DD136D366906AEA16EF36C;
extern const uint32_t g_rgctx_Enumerator_t963411FADC1637E07AF617B170046039A998FF1A;
extern const uint32_t g_rgctx_List_1_GetEnumerator_mDBED224269C3C6BA67F323BCDCD86531DB0006FF;
extern const uint32_t g_rgctx_Enumerator_get_Current_m50C7BD2C028D33DEB585F47F631AEC9383548930;
extern const uint32_t g_rgctx_Dictionary_2_Remove_m3A2F3BBCE29AEAB9E78126E23482AF3218096432;
extern const uint32_t g_rgctx_Enumerator_MoveNext_m1B58A181C9B1F82112A2F681D39E4A299288ED96;
extern const uint32_t g_rgctx_Enumerator_t0CAF376A220E2EA710897E3DDA38A67332DE6E34;
extern const uint32_t g_rgctx_CollectionPool_2_RecycleCollection_m2BF3853A507E1CCB4F6F02B2C4BA2CDC9E281294;
extern const uint32_t g_rgctx_BindableVariableBase_1_set_Value_m029795FEE26AF0B58156E674E491B90F7595B432;
extern const uint32_t g_rgctx_Datum_1_get_Value_m1FB0C24D3ED2336FB53BEB633B985486E10C2A72;
extern const uint32_t g_rgctx_BindableVariableAlloc_1_t6A03104F00BAF66E31BE7756D62AF174717F5E48;
extern const uint32_t g_rgctx_BindableVariableAlloc_1__ctor_m9D361F95027C40704AF5B352F035D0A9999C5438;
extern const uint32_t g_rgctx_DatumProperty_2_get_Datum_mACEC0398AB78928B20DEB664A16C549ED8A66E99;
extern const uint32_t g_rgctx_Datum_1_get_Value_m78860C0789271ADC756D70D4463B4E453A987CB0;
extern const uint32_t g_rgctx_Datum_1_set_Value_m9008BD44CB5AEF59AB6F046885C74267BFD2E434;
extern const uint32_t g_rgctx_TDatum_t90F9F37692C5EC9CEDA1C77A7927DFF14250420F;
extern const uint32_t g_rgctx_DatumProperty_2_get_Value_m575F461C75A13449A917DB3EA6D87034460E36FA;
extern const uint32_t g_rgctx_List_1_get_Count_m55D917EB595E9A1635D9D40D4636E8542357FF6F;
extern const uint32_t g_rgctx_List_1_get_Item_m21980E154C27DFB56F19E13AA943E69AD3079589;
extern const uint32_t g_rgctx_List_1_tCCDE1AF1ABD7B611055AB762100520301A3C27AB;
extern const uint32_t g_rgctx_List_1__ctor_mB606AF0ABA966A4B4CE55178AE2817789A395B31;
extern const uint32_t g_rgctx_HashSet_1_t73E23041C7D0998CAA2E7C96A329F4636BF20FB7;
extern const uint32_t g_rgctx_HashSet_1__ctor_m75306FA985920467399D5A5D1D3C6379901DB796;
extern const uint32_t g_rgctx_List_1_GetEnumerator_mA1C87B4DA5EA93C5801B8B4E79ABEF6697B34A6B;
extern const uint32_t g_rgctx_Enumerator_t124C6038B5DC0C1917883A7FD85E0C6300E48C8A;
extern const uint32_t g_rgctx_HashSetList_1_GetEnumerator_mA5B5B1D0609C6A5344D55E09B9530CD55154FCFD;
extern const uint32_t g_rgctx_HashSet_1_Add_mDA8FB09F651136C174E9D7751536F50A11E857A0;
extern const uint32_t g_rgctx_List_1_Add_m9875E6EE1DF8D356689C757CA6C5704A3FA62937;
extern const uint32_t g_rgctx_HashSet_1_Remove_mEF85FED2B9244A5F8005A382D1852DBE2D24F4B9;
extern const uint32_t g_rgctx_List_1_Remove_m382C2EA8189B4DB7DF31E93DD8AF08F40344A1D0;
extern const uint32_t g_rgctx_HashSet_1_ExceptWith_m7E99CD766E1556CF18282E7D98E3DD8D4F7C50B4;
extern const uint32_t g_rgctx_HashSetList_1_RefreshList_mFDFEC159741A1D758845D03865BEFA53D5BECF37;
extern const uint32_t g_rgctx_HashSet_1_IntersectWith_mFAEE32DB92892ED1C211C8AF2DD779AE13D097BC;
extern const uint32_t g_rgctx_HashSet_1_IsProperSubsetOf_m5F74281A6C5BB5A43374C565E80161D91A20D959;
extern const uint32_t g_rgctx_HashSet_1_IsProperSupersetOf_m8F555421D50318580235F57181836B6726E5B167;
extern const uint32_t g_rgctx_HashSet_1_IsSubsetOf_m5762AA768F922CB0F4DE3534EE6A38F3970B545C;
extern const uint32_t g_rgctx_HashSet_1_IsSupersetOf_mF279B51E78ADB6AF6F47A8ABC33BFB12A0E15497;
extern const uint32_t g_rgctx_HashSet_1_Overlaps_mCD7366EC36E930DC72D3B9547EEA6866C988CAFB;
extern const uint32_t g_rgctx_HashSet_1_SetEquals_m05542A5A3B8041AE5DE2A01F2C15076DA4DBDA8E;
extern const uint32_t g_rgctx_HashSet_1_SymmetricExceptWith_mEA2C5E4F049BDF9D2CE2408C3A01247D2743FF8A;
extern const uint32_t g_rgctx_HashSet_1_UnionWith_mCAF070662C3E5B2CE3542BFA5CBF99719481E13E;
extern const uint32_t g_rgctx_HashSet_1_Clear_mF2DD229C8D2ECDEE25AAF8C7F3825F2F3C133838;
extern const uint32_t g_rgctx_List_1_Clear_mE9AA42A64F266656D157770B1599014996E01F73;
extern const uint32_t g_rgctx_HashSet_1_Contains_m8F7C110B7EFCA4E23DBD7D5A069E6AAA65AF3BD3;
extern const uint32_t g_rgctx_List_1_CopyTo_mB7DA8EF865DF3A686838D0F833CBA7A16C714E6A;
extern const uint32_t g_rgctx_HashSet_1_GetObjectData_m3994F1D04BC68923EFC05F17C85FF8ED45F9BF90;
extern const uint32_t g_rgctx_HashSet_1_OnDeserialization_mC5B39BF7A2836645C3E90AAA7C340F7D0423CAAA;
extern const uint32_t g_rgctx_List_1_AddRange_mA8B60B8F2382857D082FC5385FE1643F43BA2424;
extern const uint32_t g_rgctx_BindableVariableBase_1__ctor_mF9028A888B7E7D4E89554007AF85613B7AA61414;
extern const uint32_t g_rgctx_BindableVariableBase_1_t6E921659D92A989CE9181B5737B8FE6EE87C1A83;
extern const uint32_t g_rgctx_BindableVariableBase_1_get_Value_m3E059BD12CD7EDE6B15E8E991350A0DAACB94CAC;
extern const uint32_t g_rgctx_T_t734E1F14D7B4F8603A5895AD77881F20B6F538D5;
extern const uint32_t g_rgctx_BindableVariableBase_1__ctor_m868B37BB39E1D0009D0ADC47286FC3066CEA301E;
extern const uint32_t g_rgctx_BindableVariableBase_1_tC05D81844C0D3F353E70813AB98DFFC2F189FA39;
extern const uint32_t g_rgctx_BindableVariableBase_1_get_Value_m765C71BF29A2C4A63F90DF4E6EC4155658773ED1;
extern const uint32_t g_rgctx_T_t49CF87E90F14157847644D7209171BE4C4A5BA87;
extern const uint32_t g_rgctx_IEquatable_1_t285F2E8B18D483B4D4368257F2B0DE61D13D8ED5;
extern const uint32_t g_rgctx_IEquatable_1_Equals_m5A65715F1F5115D44DBFCAD8D6A0010B8F3E80E2;
extern const uint32_t g_rgctx_BindableVariableBase_1__ctor_m11ADFD330A41B9B5FCED7FFCA6D963D21173AED0;
extern const uint32_t g_rgctx_BindableVariableBase_1_tC6C9DB888A3297629069FFF37F2E20B1D40D8ACE;
extern const uint32_t g_rgctx_Action_1_tCBA045BB59B25DF8938351DA86F96233440E3A1C;
extern const uint32_t g_rgctx_Interlocked_CompareExchange_TisAction_1_tCBA045BB59B25DF8938351DA86F96233440E3A1C_mFE120A5873696AE2D5B6A30C8B1D72DC1B8C2E48;
extern const uint32_t g_rgctx_BindableVariableBase_1_SetValueWithoutNotify_mAEFCD0BA8B7B802E3D1B4D909214F74F0D7051ED;
extern const uint32_t g_rgctx_BindableVariableBase_1_BroadcastValue_m7ABE7CF89E40CA2BD5A958971A5E043D0BE33691;
extern const uint32_t g_rgctx_BindableVariableBase_1_ValueEquals_mA2F4943DA008682D5DBEA4B132B5D124AAA51A4B;
extern const uint32_t g_rgctx_Func_3_Invoke_m477C53F9A114DCB6393ACF92904840D2A0DD1461;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass14_0_t20D7A23F34463982AFAF6F3E5ED4F64D92C4F41B;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass14_0__ctor_m72C5561FB93AB50640C3D2FB32F29A0167A0694F;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass14_0_U3CSubscribeU3Eb__0_m26176BAAFA45CD3D872B7C5A1AA25E332F998CB5;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass14_0_U3CSubscribeU3Eb__1_m90B12D6B5920016876ADA3D41FA4B4AA83F9A649;
extern const uint32_t g_rgctx_Action_1_Invoke_m86F05C1B33B67DF8DC199A612E397AB7A40A389A;
extern const uint32_t g_rgctx_BindableVariableBase_1_Subscribe_m895649B4EF10AB4CF6C3F0BCEAD1F2E021E92582;
extern const uint32_t g_rgctx_BindableVariableBase_1_remove_valueUpdated_m196308837F3D48D3D056E4E240991AFD589E5183;
extern const uint32_t g_rgctx_BindableVariableBase_1_DecrementReferenceCount_mA8CA6C9C0D9551F6879BF0236C3784E8D9D96C44;
extern const uint32_t g_rgctx_Func_2_Invoke_m42285643533F4FCFDE51E5B0AC9A9FFBA15C4C0F;
extern const uint32_t g_rgctx_Task_FromResult_TisT_t2699F9BF043BB8399D5EEE48B9172148C35073CC_m2A3D803963156014BC08117FA63C5151FF7E8C38;
extern const uint32_t g_rgctx_BindableVariableTaskPredicate_1_t587670E8DD55E503F3528C77EAEAF849CE17A867;
extern const uint32_t g_rgctx_BindableVariableTaskPredicate_1__ctor_m76E1FEB395BD87138E68221709F2C28C880ECAC5;
extern const uint32_t g_rgctx_BindableVariableTaskPredicate_1_get_Task_mCBF506671D237A254F2267C15AC5AFFC323B8B43;
extern const uint32_t g_rgctx_BindableVariableTaskState_1_tFE82E670A437E00F0BB573C4358B9044F75C992F;
extern const uint32_t g_rgctx_BindableVariableTaskState_1__ctor_mAB38742B97645C58258EB88EA50F47A40281E74B;
extern const uint32_t g_rgctx_BindableVariableTaskState_1_get_task_m1EB3D99221A2C6F2D58297A417EB2982AC9988E1;
extern const uint32_t g_rgctx_T_t2699F9BF043BB8399D5EEE48B9172148C35073CC;
extern const uint32_t g_rgctx_BindableVariableBase_1_add_valueUpdated_m26F56E9B82F4345188542D93F0686176946179D2;
extern const uint32_t g_rgctx_BindableVariableBase_1_IncrementReferenceCount_mF433963765E2A9A67BD239881AE84001DEB10A6A;
extern const uint32_t g_rgctx_BindableVariableBase_1_remove_valueUpdated_m35FA98BC46DB2CF28D3CAB2004E74B074AFF0C49;
extern const uint32_t g_rgctx_BindableVariableBase_1_DecrementReferenceCount_mF6AE77A008F9FCC962605741BF3F7630EFAE059F;
extern const uint32_t g_rgctx_TaskCompletionSource_1_get_Task_m31071A44BD7D436EA055BE04EE9EBE55ABF2C292;
extern const uint32_t g_rgctx_TaskCompletionSource_1_tAC9C0DD3FB991E2BA5E967AD3140337B91E2EF75;
extern const uint32_t g_rgctx_TaskCompletionSource_1__ctor_m67A9CECFA5F64B498F02C7B91753BAC0DDC486C9;
extern const uint32_t g_rgctx_IReadOnlyBindableVariable_1_t94727DD47495C95FD3C4C230387BC6A65C50FFA7;
extern const uint32_t g_rgctx_Func_2_Invoke_m67B53451DE4A387411C4AF1CA40DD8942ADB01BA;
extern const uint32_t g_rgctx_TaskCompletionSource_1_SetResult_m64597B655EA1E48AC14377298D4A4164CECAC216;
extern const uint32_t g_rgctx_BindableVariableTaskPredicate_1_tEA4AA92AA7165B53665D7216383A6684D400EC66;
extern const uint32_t g_rgctx_BindableVariableTaskPredicate_1_Cancelled_m600C3857C22589B6E46B66429568A98CEA0FC14F;
extern const uint32_t g_rgctx_BindableVariableTaskPredicate_1_Await_mFF80DBADFF795D5CADA059CB32804F06414EABCE;
extern const uint32_t g_rgctx_Action_1_tF65BA719AB78043F434300FF17E9E9B784C76375;
extern const uint32_t g_rgctx_Action_1__ctor_mE15EF43460C58A52586DEC5F818E9F0739622398;
extern const uint32_t g_rgctx_TaskCompletionSource_1_get_Task_m68BBFADBCB1872E772BC9CD87B984117874E2E42;
extern const uint32_t g_rgctx_TaskCompletionSource_1_t87F8B8DA8977272375C6340B4DC276F8B1D594A4;
extern const uint32_t g_rgctx_TaskCompletionSource_1__ctor_mFA793FFCDA5BC9CB6E40AFCDBB22E04D76353306;
extern const uint32_t g_rgctx_IReadOnlyBindableVariable_1_t9F4338B8A486FBC963F37EAF4F47A8019DA18391;
extern const uint32_t g_rgctx_TaskCompletionSource_1_SetResult_m57BB2EA369DECDB6D4658278FA6225E37160E65B;
extern const uint32_t g_rgctx_BindableVariableTaskState_1_tA770267DC0758D7C8D3E5FB085066F14B527514E;
extern const uint32_t g_rgctx_BindableVariableTaskState_1_Cancelled_m6AE102D6DD56A6494A4E1F95E4FF33BF759C235C;
extern const uint32_t g_rgctx_BindableVariableTaskState_1_Await_m83A5BFE95DF737975A13F7D51A655A79562DF331;
extern const uint32_t g_rgctx_Action_1_t2D17B13521B6C9E4B5E82EB4CE6D0054FC22ED13;
extern const uint32_t g_rgctx_Action_1__ctor_m2282A462013D49AEFC8166246D7639E5A7BC71DF;
static const Il2CppRGCTXDefinition s_rgctxValues[258] = 
{
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_List_1_get_Count_m2CCC5537A021D23F6774955747FCCA073E199332 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_List_1_get_Item_m47CE5430D357E1D535051A05EAEDE996799F31C8 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_T_t782956A42D23A5A08D0B3CE547BEEBB53FCD81FB },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_List_1_GetEnumerator_m8D1CBD1FD71F1858CC93311AF44B65276BC90C13 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Enumerator_get_Current_m93926E2DB28A76F4CCF0C478BDCCB2DD1176EA3E },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Enumerator_MoveNext_mBE5F4D1E525C3FDF3F97262DE90B514E5F5F6996 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Enumerator_tC280924931E67C964F6E8319C1DC0A50BB998A86 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_CollectionPool_2_GetCollection_mCBA9657585DAA516C5EE9BA90067E6CED2D0135A },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_CollectionPool_2_t5A55155F82289E0E878825DB4AE7A4DFE564E471 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_CachedComponentFilter_2_tDC5240BD513EFE9CBB73B40E639C10307754F5AC },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_List_1_Clear_mF9EFC55C1F4479E7DE943E57424C0D38D576BDD7 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_List_1_Clear_m13BF16A78A8F0BDDEB7C9E84AD3AFF5B41D61FF8 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_TRootType_t6656F32142172AF3FBF210D4D798F3E59C20F1A7 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Component_GetComponents_TisTFilterType_t4020824B9F4769153A771DB6FE0F3608376A6245_m552F8CEDC916FF828558B7DC32B818EDF110A908 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Component_GetComponents_TisIComponentHost_1_tCDBF1749CA1E45C01C9A9B414A91D8A6F0980157_mFCDFB6E0E15154A6E244C8B15FBA94E048ECAC34 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_CachedComponentFilter_2_FilteredCopyToMaster_mCCD9C6D9B1D1CB27AAB7B442E1C7B2AFC58856BF },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Component_GetComponent_TisTRootType_t6656F32142172AF3FBF210D4D798F3E59C20F1A7_mF6C764D681E387E30742825AA5D519BE908B11BA },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Component_GetComponentsInChildren_TisTFilterType_t4020824B9F4769153A771DB6FE0F3608376A6245_mC111AF621E1148CA73A6CABF74529F135D754CDA },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Component_GetComponentsInChildren_TisIComponentHost_1_tCDBF1749CA1E45C01C9A9B414A91D8A6F0980157_mF6A0C148F2AA0AC0147B78BDF43DC1E46254C6CC },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_CachedComponentFilter_2_FilteredCopyToMaster_mBE29938FF6CBEBC19C3A9B77BE3AF6B4731D9F32 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_List_1_AddRange_mA4D4E0AF3DD92C0F81C1F7937E76EAA45A58E3A5 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_List_1_GetEnumerator_mF28337D45533EA47F60E4560F1E085FE4F4D63D5 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Enumerator_get_Current_m6F5ACEB62C30631268825B90988C8DC5FFFA4BE5 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_TFilterType_t4020824B9F4769153A771DB6FE0F3608376A6245 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Enumerator_MoveNext_m121939A1F6929BFC799A3CEE28E1950972887B5C },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Enumerator_t4C2CBCA1F5C6504050164DEAE6FF0052A1B29403 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_List_1_GetEnumerator_m982A5A739534396E07DE9372DDF63AE90A7C4468 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Enumerator_get_Current_m1229D9E8B456ACAE671B76ADC7DBA0A6B9B808CE },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_IComponentHost_1_tCDBF1749CA1E45C01C9A9B414A91D8A6F0980157 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Enumerator_MoveNext_mFF2550624367A85D99B3146839D5385583CBE0D2 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Enumerator_tFEC8C0DBE551369CA15A42DE4AAD94E77864649E },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_List_1_Add_mEC658B99382EC7C83C28E8B1392D4329743B1C07 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Component_GetComponentInParent_TisTRootType_t6656F32142172AF3FBF210D4D798F3E59C20F1A7_mE164CEF3EEA281E389C25AC6C01A3509CAAAE695 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_CollectionPool_2_RecycleCollection_mCA94D2B40969E852BFBBD83A9AD10A1B3B57B19C },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_CachedComponentFilter_2_Dispose_m7DEE83C1346ED00A4A2CDF76CE4DC6441C83C250 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_List_1_t89605CE7D994E40B86B3DDA74C2C6951056E512A },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_List_1__ctor_m4EB253D2F835CA3868B4168F0D4DDF525F228320 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_List_1_tAD2AAD1FA2C184DC8F6255140BEE49904D55920D },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_List_1__ctor_m2792B841B87932597AD04B348DDF3596252635B9 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_TChildType_t8DAB93F970F5445B8A4E606BA37668F50E83FE6A },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_List_1_Add_mC5EE430ECBFEF0E682A2A64F3B6958F88D9DD3E6 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_TChildType_t1B56FCFFADE69A0388F9C8EB404AEE2E48DFD969 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_TChildTypeU5BU5D_t0EE6AB8DD22CC1262155977E7AC61E3C47BDF6CC },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_CollectionPool_2_tB2DA9F111A37B71DA8FD9DFCA08CD2C0CBFED5A6 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Queue_1_get_Count_m7328DBD135918795F0AC56D1936B62370E7E3237 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Activator_CreateInstance_TisTCollection_tAE554EA8BF8423716D6221246968276B78325351_mBBC3B4EF8B02E011DA33E6ECDDEE91FE561F9A1D },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Queue_1_Dequeue_mD9749228621D69DE6711384ED2BE472D82242C4A },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_TCollection_tAE554EA8BF8423716D6221246968276B78325351 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ICollection_1_tF401A0AF5A7BB4FCA94734411495A89B8A6A59DA },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ICollection_1_Clear_m002383D7C3E48F574E6589D14B9F9EE8730EC7DC },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Queue_1_Enqueue_m9944CA7F3580A82B40B2F76A3A4718121CB8C670 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Queue_1_t03A1F96D63D9B83614AD2EB8BF49D076E7DAC076 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Queue_1__ctor_mCDACD0616420281A6C55E5D0DC7FD72308D1B2EC },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ComponentUtils_1_tEE8092F19444C84D2F710096BEE7E66CC76DC340 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_GameObject_GetComponents_TisT_t3BC2741207FBF68D9CABFD84B5E105DEB153CCDB_mAC5A3B9066A81F520C7E294540E650ED1A301DBA },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_List_1_get_Count_m65D432487265A3178458C5F9A8680B711651221B },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_List_1_get_Item_m56370F8FA1FCFFDF649E27D9BDE632645FB72BC8 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_GameObject_GetComponentsInChildren_TisT_t3BC2741207FBF68D9CABFD84B5E105DEB153CCDB_mE5C8E2394DAF135B3923BC8C767978D3948B3AD2 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_List_1_t247C5B3353A3AF610B8BACFB6B6D861379FCB842 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_List_1__ctor_mDC6D6CBCF088572358214BFCBF4C1B78E0249094 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_GameObject_GetComponent_TisT_tACA6A29EB564FBC587FD27AFE3C19D0439CC2902_m58B150AED00E59094DEF90D38746FD71A76593EA },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_T_tACA6A29EB564FBC587FD27AFE3C19D0439CC2902 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_GameObject_AddComponent_TisT_tACA6A29EB564FBC587FD27AFE3C19D0439CC2902_m6B3350977DBC4450D16C29E6228F9909E058A1C0 },
	{ (Il2CppRGCTXDataType)1, (const Il2CppRGCTXDefinitionData *)&g_rgctx_T_t646DFD494905AA196AB6F594EBADAE13D73F2CEB },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_TU5BU5D_tC0999A7E241E9C472ED88D6BCE7605D8A1C7E271 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_EnumValues_1_t758610FB7FA57F5DDCCE1E1B71FED5B85E4FB7FE },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ICollection_1_t5827F10DAFB40C87E4504E0A7D4D0AB3F50298C3 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_IEnumerable_1_t67C0E088A7F797511A53E369410C99BE9ED47528 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_IEnumerator_1_t9419F4E0518227781B373CC3FDD3107B73AEAF0C },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_T_tF2E3000B412DB27DDFD26EF6E74D77CEE4C5EBB0 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Dictionary_2_GetEnumerator_mC30F80A06C7B51E718317583674B808069780E42 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Enumerator_MoveNext_m70F4EE52E96AFBE8DCFCB75AD42EBCD14755BC4D },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Enumerator_get_Current_m0A229C7F750024DACE576F82A5FFB9863B143134 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Enumerator_Dispose_mB39D4A02092A3EC8733F45D32D9E914F5BF0B940 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_HashSet_1_GetEnumerator_m6D724C0B15E2AE2A5F3C9AE8223C0FEB0DA9536A },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Enumerator_get_Current_mBC9F1719C2C69D9AE2D1A877BA38EA0569A05695 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_HashSet_1_Remove_m306CCE5A1B475CA8C53D78142D39B61E22971CD2 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Enumerator_MoveNext_mFABD7F409302E13C16457688A303C38C431D8287 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Enumerator_t07FB3027090258095B02DE40B4B8421162174051 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_HashSet_1_GetEnumerator_m2675CC256DB8B9EC43DBB9B099B204EBF011D4F8 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Enumerator_MoveNext_m803EB5407CD56F0CC03308A15A63758E8C4B5375 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Enumerator_get_Current_m519EC456F44BC51B187E441D2BD802958024FFB0 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Enumerator_Dispose_m18672D3E12A34E2CC447E785D491507C64F738AB },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Activator_CreateInstance_TisT_t6124A8764A5700536AEF7572A7B38185EDACE31C_mDF3C3C059B4EA9E73A55509C585108A9CDC34C9C },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_List_1_Add_m92A96A7614E67195701CC7E86B87BE29C5AC1ABB },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_List_1_get_Capacity_mEE27ECE7F5F89DD69FED69810CD027B257F03139 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_List_1_set_Capacity_m7820211AEE1858C33AB3FF17952B055DFA5872A4 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_List_1_get_Item_mE2D1CE9CDA13E3D5D54AFB47D35B317DA2789D63 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_List_1_set_Item_m25F15EA57C3081BA64E0B14794F7799CDB802E90 },
	{ (Il2CppRGCTXDataType)1, (const Il2CppRGCTXDefinitionData *)&g_rgctx_TAttribute_t13961EE5C179450545E4DEC49759903786FB0DF0 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_TAttribute_t13961EE5C179450545E4DEC49759903786FB0DF0 },
	{ (Il2CppRGCTXDataType)1, (const Il2CppRGCTXDefinitionData *)&g_rgctx_TAttribute_tF6DD1A15775EACE2E725BC14AD65EB75F5D4D02C },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_GameObject_GetComponentInChildren_TisT_tC16606CA819499AAFE98B96291393D9A85F509CE_m34E0FF15A75BC18ED6400972415ECE2F5F953283 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_T_tC16606CA819499AAFE98B96291393D9A85F509CE },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Object_FindObjectOfType_TisT_tC16606CA819499AAFE98B96291393D9A85F509CE_m99F20A7A7D7EEFEA17CD18E8B6D49BDB7EE66207 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_GameObject_GetComponentsInChildren_TisT_t1B49459DB8A2A8CA0FD665939BFC2D8E9D68DEA8_mAD4E76D6CBF56B1064ECB28B4606BBA5E354A585 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_T_t1B49459DB8A2A8CA0FD665939BFC2D8E9D68DEA8 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_GameObject_GetComponent_TisT_t1B49459DB8A2A8CA0FD665939BFC2D8E9D68DEA8_m50C5491180DFD399D1C3C1324B6DC8ABDEC168F6 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Object_FindObjectOfType_TisT_t1B49459DB8A2A8CA0FD665939BFC2D8E9D68DEA8_m9EEF83D1970396E77131B1BCDC932A09A8601F45 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_GameObject_GetComponentInChildren_TisT_t894153C1C6AC3871A14691F27FC3FB7990E06F5C_m2418C83D746F66DF95B249AF5A2A4B3FA7273A8F },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_T_t894153C1C6AC3871A14691F27FC3FB7990E06F5C },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_GameObject_GetComponentsInChildren_TisT_t744FAD3FA707CBFC5058262C7116F5D3FB6DCD29_m47A0614856F69595569136AC8AEC6DBD6629A55D },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_List_1_AddRange_m25477A9D2A12225E948AE80328C8A98AE170CC48 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_GameObjectUtils_GetComponentInScene_TisT_t55CDCD2D64F1F3CB91AB093B6C4323BD166BAF2F_mDA60ECC99E7F5647BF050A26324B6DE1135E755B },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_GameObjectUtils_GetComponentsInScene_TisT_t02A31073F4CD1FB58597A8FCBB793DDA99E3B80E_m20509CE9025228A9CDFE8D05657A59C0AD679F77 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_GameObjectUtils_GetComponentsInScene_TisT_t9AE149219F45540D75D0BAFE2AC5282D9A63B5B4_m53EEEEE4FF90B3140ED44F1511FAA5CE2A83CA81 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_NativeArray_1_get_Length_mF274D091287F4E165CAA572890E524DF8803363C },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_NativeArray_1_get_IsCreated_m92C39F5ACA8D5F55026D94CA48850FE3DCD07643 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_NativeArray_1_Dispose_m333D2E710D8561B766B21A2F2ABD20B494502776 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_NativeArray_1_t8C470801D8F552B595557F2A2BDECF99F1BD8AD1 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_NativeArray_1__ctor_m7CFC8C958DEA5D1B2DDAF5176226A78D1E58DC19 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Queue_1_get_Count_m53ACAD1137EF982835E9AE3975029456B79FD2B9 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Queue_1_Dequeue_m3CA7E1103C56CF32459AB5890A031D9AFB1C19D0 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Activator_CreateInstance_TisT_t41BB3F5E25CEA261101561FBC07E94FBC901EC5F_m511B8565D91B4FD59E4F3CC1517D0F35B51CEF84 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ObjectPool_1_ClearInstance_m00115ECE4E1FB7561804357BC4FFAD54BAF53550 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Queue_1_Enqueue_m3C181A7B38334597526803EB4C7AAB49FD790A50 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Queue_1_t0278564DCEFAD0C9951B1C8B8B213337E5E84EE1 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Queue_1__ctor_m830296A356E3CEEBC6719B39805579927D74EA81 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ScriptableSettingsBase_1_t19D5BC2591DC4416951A4EF3183AA2DC68E739FF },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_T_tB2B6DD5AF29D3AE6E4835AC2359A392295B24FC7 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ScriptableSettings_1_CreateAndLoad_m2ED16A96514EE5D20CDB7EF736B77E3F3FC32493 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ScriptableSettings_1_tD56CF73029CC1C1C770E893FB0DAD0D94123809E },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ScriptableSettingsBase_1_GetFilePath_m95681C5B0E5545DB3BA9E11A560083366073DA4D },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ScriptableObject_CreateInstance_TisT_tB2B6DD5AF29D3AE6E4835AC2359A392295B24FC7_m758198DDA81B241BC062C2517DFF935817A42504 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ScriptableSettingsBase_1_Save_mEE06AD1D750DD8B133CE01FBD64C851BC735179F },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ScriptableSettingsBase_1__ctor_m2EC67AC0F2AF5B95B904918335B1DC45847BF667 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_ScriptableSettingsBase_1_t0049D77678ABE672AA85F7A15DDEAC4EC8E129D0 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_T_t3CCF25A3981B154E1F510C46B9DB662D6F90D27C },
	{ (Il2CppRGCTXDataType)1, (const Il2CppRGCTXDefinitionData *)&g_rgctx_T_t3CCF25A3981B154E1F510C46B9DB662D6F90D27C },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_GameObject_AddComponent_TisT_t17F1FEAE24D5C4C43E0699DD8420C9CF1D27382A_m2A079ADF83CCB064D1F427C4E28F66360F38513C },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_T_t2599377CB501F3CAF9013461AF3E56AF2968545D },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_GameObject_GetComponent_TisT_t2599377CB501F3CAF9013461AF3E56AF2968545D_m7C19B4FA78D47F6A6428E2B01F30B87349CB4E2D },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Component_GetComponent_TisT_t2599377CB501F3CAF9013461AF3E56AF2968545D_m207DAD31E9CCDD5BBD3C79E99CF465B4D4736D77 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_CollectionPool_2_GetCollection_mFB10EB6253F5D3440DE7CB6CD603CCEEED2F16C5 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_CollectionPool_2_t8315E65048291BDC44BBC021093C926F1E3B13DA },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_List_1_GetEnumerator_m0FE139CDAA83F2F018E50E4FB0679273BBE18BAD },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Enumerator_get_Current_m674D1F6B4C73AA6C02F509CA3411C9DFD9D93435 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_T_tA4711439D2D7024848E50CC76205CE721F819CF8 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_List_1_Add_mA11C84B8C39EC3B62E4B52930FB107C387F33582 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Enumerator_MoveNext_mA3ECE48FE18A3015E8116EEF73415185C3A87C0A },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Enumerator_tBCB903DACDB9F855AF33B905AEA3C1ACDD25A617 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_List_1_Remove_mE937D22F8BBAB1C094FE386FC043701FC1219E9D },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_CollectionPool_2_RecycleCollection_m95132B1869321A7092F76232B4DCDC6661671E2A },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_CollectionPool_2_GetCollection_m50DE3378B8341438BD7A9F3A7EC88E609DDEE4D0 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_CollectionPool_2_tDCF50DCF3AD42655B3B9C35F396EE791475F5DF8 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Dictionary_2_GetEnumerator_mE9CD7C7B0CB4FA7DF98C68B93BC98EBCE7D37BE1 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Enumerator_get_Current_m2BC45B84B089EAE8EB9DFC53D2E17CA04E87D368 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_KeyValuePair_2_get_Key_m4B024CAF0CA5B1B80C524186C75A1CC511C59A88 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_TKey_t0A55C1B1B4FCC085A17AB71751FCA408CD75636B },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_List_1_Add_m782362DEC749352FA4092147B985A267E33F7550 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Enumerator_MoveNext_m890FB782710FD23E70DD136D366906AEA16EF36C },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Enumerator_t963411FADC1637E07AF617B170046039A998FF1A },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_List_1_GetEnumerator_mDBED224269C3C6BA67F323BCDCD86531DB0006FF },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Enumerator_get_Current_m50C7BD2C028D33DEB585F47F631AEC9383548930 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Dictionary_2_Remove_m3A2F3BBCE29AEAB9E78126E23482AF3218096432 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Enumerator_MoveNext_m1B58A181C9B1F82112A2F681D39E4A299288ED96 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Enumerator_t0CAF376A220E2EA710897E3DDA38A67332DE6E34 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_CollectionPool_2_RecycleCollection_m2BF3853A507E1CCB4F6F02B2C4BA2CDC9E281294 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_BindableVariableBase_1_set_Value_m029795FEE26AF0B58156E674E491B90F7595B432 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Datum_1_get_Value_m1FB0C24D3ED2336FB53BEB633B985486E10C2A72 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_BindableVariableAlloc_1_t6A03104F00BAF66E31BE7756D62AF174717F5E48 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_BindableVariableAlloc_1__ctor_m9D361F95027C40704AF5B352F035D0A9999C5438 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_DatumProperty_2_get_Datum_mACEC0398AB78928B20DEB664A16C549ED8A66E99 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Datum_1_get_Value_m78860C0789271ADC756D70D4463B4E453A987CB0 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Datum_1_set_Value_m9008BD44CB5AEF59AB6F046885C74267BFD2E434 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_TDatum_t90F9F37692C5EC9CEDA1C77A7927DFF14250420F },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_DatumProperty_2_get_Value_m575F461C75A13449A917DB3EA6D87034460E36FA },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_List_1_get_Count_m55D917EB595E9A1635D9D40D4636E8542357FF6F },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_List_1_get_Item_m21980E154C27DFB56F19E13AA943E69AD3079589 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_List_1_tCCDE1AF1ABD7B611055AB762100520301A3C27AB },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_List_1__ctor_mB606AF0ABA966A4B4CE55178AE2817789A395B31 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_HashSet_1_t73E23041C7D0998CAA2E7C96A329F4636BF20FB7 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_HashSet_1__ctor_m75306FA985920467399D5A5D1D3C6379901DB796 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_List_1_GetEnumerator_mA1C87B4DA5EA93C5801B8B4E79ABEF6697B34A6B },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Enumerator_t124C6038B5DC0C1917883A7FD85E0C6300E48C8A },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_HashSetList_1_GetEnumerator_mA5B5B1D0609C6A5344D55E09B9530CD55154FCFD },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_HashSet_1_Add_mDA8FB09F651136C174E9D7751536F50A11E857A0 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_List_1_Add_m9875E6EE1DF8D356689C757CA6C5704A3FA62937 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_HashSet_1_Remove_mEF85FED2B9244A5F8005A382D1852DBE2D24F4B9 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_List_1_Remove_m382C2EA8189B4DB7DF31E93DD8AF08F40344A1D0 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_HashSet_1_ExceptWith_m7E99CD766E1556CF18282E7D98E3DD8D4F7C50B4 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_HashSetList_1_RefreshList_mFDFEC159741A1D758845D03865BEFA53D5BECF37 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_HashSet_1_IntersectWith_mFAEE32DB92892ED1C211C8AF2DD779AE13D097BC },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_HashSet_1_IsProperSubsetOf_m5F74281A6C5BB5A43374C565E80161D91A20D959 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_HashSet_1_IsProperSupersetOf_m8F555421D50318580235F57181836B6726E5B167 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_HashSet_1_IsSubsetOf_m5762AA768F922CB0F4DE3534EE6A38F3970B545C },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_HashSet_1_IsSupersetOf_mF279B51E78ADB6AF6F47A8ABC33BFB12A0E15497 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_HashSet_1_Overlaps_mCD7366EC36E930DC72D3B9547EEA6866C988CAFB },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_HashSet_1_SetEquals_m05542A5A3B8041AE5DE2A01F2C15076DA4DBDA8E },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_HashSet_1_SymmetricExceptWith_mEA2C5E4F049BDF9D2CE2408C3A01247D2743FF8A },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_HashSet_1_UnionWith_mCAF070662C3E5B2CE3542BFA5CBF99719481E13E },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_HashSet_1_Clear_mF2DD229C8D2ECDEE25AAF8C7F3825F2F3C133838 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_List_1_Clear_mE9AA42A64F266656D157770B1599014996E01F73 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_HashSet_1_Contains_m8F7C110B7EFCA4E23DBD7D5A069E6AAA65AF3BD3 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_List_1_CopyTo_mB7DA8EF865DF3A686838D0F833CBA7A16C714E6A },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_HashSet_1_GetObjectData_m3994F1D04BC68923EFC05F17C85FF8ED45F9BF90 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_HashSet_1_OnDeserialization_mC5B39BF7A2836645C3E90AAA7C340F7D0423CAAA },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_List_1_AddRange_mA8B60B8F2382857D082FC5385FE1643F43BA2424 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_BindableVariableBase_1__ctor_mF9028A888B7E7D4E89554007AF85613B7AA61414 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_BindableVariableBase_1_t6E921659D92A989CE9181B5737B8FE6EE87C1A83 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_BindableVariableBase_1_get_Value_m3E059BD12CD7EDE6B15E8E991350A0DAACB94CAC },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_T_t734E1F14D7B4F8603A5895AD77881F20B6F538D5 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_BindableVariableBase_1__ctor_m868B37BB39E1D0009D0ADC47286FC3066CEA301E },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_BindableVariableBase_1_tC05D81844C0D3F353E70813AB98DFFC2F189FA39 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_BindableVariableBase_1_get_Value_m765C71BF29A2C4A63F90DF4E6EC4155658773ED1 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_T_t49CF87E90F14157847644D7209171BE4C4A5BA87 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_IEquatable_1_t285F2E8B18D483B4D4368257F2B0DE61D13D8ED5 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_IEquatable_1_Equals_m5A65715F1F5115D44DBFCAD8D6A0010B8F3E80E2 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_BindableVariableBase_1__ctor_m11ADFD330A41B9B5FCED7FFCA6D963D21173AED0 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_BindableVariableBase_1_tC6C9DB888A3297629069FFF37F2E20B1D40D8ACE },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Action_1_tCBA045BB59B25DF8938351DA86F96233440E3A1C },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Interlocked_CompareExchange_TisAction_1_tCBA045BB59B25DF8938351DA86F96233440E3A1C_mFE120A5873696AE2D5B6A30C8B1D72DC1B8C2E48 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_BindableVariableBase_1_SetValueWithoutNotify_mAEFCD0BA8B7B802E3D1B4D909214F74F0D7051ED },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_BindableVariableBase_1_BroadcastValue_m7ABE7CF89E40CA2BD5A958971A5E043D0BE33691 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_BindableVariableBase_1_ValueEquals_mA2F4943DA008682D5DBEA4B132B5D124AAA51A4B },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Func_3_Invoke_m477C53F9A114DCB6393ACF92904840D2A0DD1461 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_U3CU3Ec__DisplayClass14_0_t20D7A23F34463982AFAF6F3E5ED4F64D92C4F41B },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_U3CU3Ec__DisplayClass14_0__ctor_m72C5561FB93AB50640C3D2FB32F29A0167A0694F },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_U3CU3Ec__DisplayClass14_0_U3CSubscribeU3Eb__0_m26176BAAFA45CD3D872B7C5A1AA25E332F998CB5 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_U3CU3Ec__DisplayClass14_0_U3CSubscribeU3Eb__1_m90B12D6B5920016876ADA3D41FA4B4AA83F9A649 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Action_1_Invoke_m86F05C1B33B67DF8DC199A612E397AB7A40A389A },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_BindableVariableBase_1_Subscribe_m895649B4EF10AB4CF6C3F0BCEAD1F2E021E92582 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_BindableVariableBase_1_remove_valueUpdated_m196308837F3D48D3D056E4E240991AFD589E5183 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_BindableVariableBase_1_DecrementReferenceCount_mA8CA6C9C0D9551F6879BF0236C3784E8D9D96C44 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Func_2_Invoke_m42285643533F4FCFDE51E5B0AC9A9FFBA15C4C0F },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Task_FromResult_TisT_t2699F9BF043BB8399D5EEE48B9172148C35073CC_m2A3D803963156014BC08117FA63C5151FF7E8C38 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_BindableVariableTaskPredicate_1_t587670E8DD55E503F3528C77EAEAF849CE17A867 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_BindableVariableTaskPredicate_1__ctor_m76E1FEB395BD87138E68221709F2C28C880ECAC5 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_BindableVariableTaskPredicate_1_get_Task_mCBF506671D237A254F2267C15AC5AFFC323B8B43 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_BindableVariableTaskState_1_tFE82E670A437E00F0BB573C4358B9044F75C992F },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_BindableVariableTaskState_1__ctor_mAB38742B97645C58258EB88EA50F47A40281E74B },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_BindableVariableTaskState_1_get_task_m1EB3D99221A2C6F2D58297A417EB2982AC9988E1 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_T_t2699F9BF043BB8399D5EEE48B9172148C35073CC },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_BindableVariableBase_1_add_valueUpdated_m26F56E9B82F4345188542D93F0686176946179D2 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_BindableVariableBase_1_IncrementReferenceCount_mF433963765E2A9A67BD239881AE84001DEB10A6A },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_BindableVariableBase_1_remove_valueUpdated_m35FA98BC46DB2CF28D3CAB2004E74B074AFF0C49 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_BindableVariableBase_1_DecrementReferenceCount_mF6AE77A008F9FCC962605741BF3F7630EFAE059F },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_TaskCompletionSource_1_get_Task_m31071A44BD7D436EA055BE04EE9EBE55ABF2C292 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_TaskCompletionSource_1_tAC9C0DD3FB991E2BA5E967AD3140337B91E2EF75 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_TaskCompletionSource_1__ctor_m67A9CECFA5F64B498F02C7B91753BAC0DDC486C9 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_IReadOnlyBindableVariable_1_t94727DD47495C95FD3C4C230387BC6A65C50FFA7 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Func_2_Invoke_m67B53451DE4A387411C4AF1CA40DD8942ADB01BA },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_TaskCompletionSource_1_SetResult_m64597B655EA1E48AC14377298D4A4164CECAC216 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_BindableVariableTaskPredicate_1_tEA4AA92AA7165B53665D7216383A6684D400EC66 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_BindableVariableTaskPredicate_1_Cancelled_m600C3857C22589B6E46B66429568A98CEA0FC14F },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_BindableVariableTaskPredicate_1_Await_mFF80DBADFF795D5CADA059CB32804F06414EABCE },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Action_1_tF65BA719AB78043F434300FF17E9E9B784C76375 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Action_1__ctor_mE15EF43460C58A52586DEC5F818E9F0739622398 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_TaskCompletionSource_1_get_Task_m68BBFADBCB1872E772BC9CD87B984117874E2E42 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_TaskCompletionSource_1_t87F8B8DA8977272375C6340B4DC276F8B1D594A4 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_TaskCompletionSource_1__ctor_mFA793FFCDA5BC9CB6E40AFCDBB22E04D76353306 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_IReadOnlyBindableVariable_1_t9F4338B8A486FBC963F37EAF4F47A8019DA18391 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_TaskCompletionSource_1_SetResult_m57BB2EA369DECDB6D4658278FA6225E37160E65B },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_BindableVariableTaskState_1_tA770267DC0758D7C8D3E5FB085066F14B527514E },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_BindableVariableTaskState_1_Cancelled_m6AE102D6DD56A6494A4E1F95E4FF33BF759C235C },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_BindableVariableTaskState_1_Await_m83A5BFE95DF737975A13F7D51A655A79562DF331 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Action_1_t2D17B13521B6C9E4B5E82EB4CE6D0054FC22ED13 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Action_1__ctor_m2282A462013D49AEFC8166246D7639E5A7BC71DF },
};
extern const CustomAttributesCacheGenerator g_Unity_XR_CoreUtils_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_Unity_XR_CoreUtils_CodeGenModule;
const Il2CppCodeGenModule g_Unity_XR_CoreUtils_CodeGenModule = 
{
	"Unity.XR.CoreUtils.dll",
	421,
	s_methodPointers,
	23,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	42,
	s_rgctxIndices,
	258,
	s_rgctxValues,
	NULL,
	g_Unity_XR_CoreUtils_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
