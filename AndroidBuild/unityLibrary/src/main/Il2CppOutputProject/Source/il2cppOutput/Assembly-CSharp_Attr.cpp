﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// UnityEngine.AddComponentMenu
struct AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100;
// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF;
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C;
// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B;
// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88;
// UnityEngine.ExecuteInEditMode
struct ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173;
// UnityEngine.HeaderAttribute
struct HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB;
// UnityEngine.HideInInspector
struct HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA;
// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// UnityEngine.RangeAttribute
struct RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5;
// UnityEngine.RequireComponent
struct RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80;
// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25;
// UnityEngine.SpaceAttribute
struct SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8;
// System.String
struct String_t;
// UnityEngine.TooltipAttribute
struct TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B;
// System.Type
struct Type_t;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;

IL2CPP_EXTERN_C const RuntimeType* CharacterController_tCCF68621C784CCB3391E0C66FE134F6F93DD6C2E_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* PlayerInput_tCB9A1A84F447F3392889C457C9BE413891FE848A_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CSimulateKeyPressU3Ed__8_tAB6F8BC46272BF48D20F34C4D00E6A15FCEF04C6_0_0_0_var;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Attribute
struct  Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct  ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// UnityEngine.AddComponentMenu
struct  AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String UnityEngine.AddComponentMenu::m_AddComponentMenu
	String_t* ___m_AddComponentMenu_0;
	// System.Int32 UnityEngine.AddComponentMenu::m_Ordering
	int32_t ___m_Ordering_1;

public:
	inline static int32_t get_offset_of_m_AddComponentMenu_0() { return static_cast<int32_t>(offsetof(AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100, ___m_AddComponentMenu_0)); }
	inline String_t* get_m_AddComponentMenu_0() const { return ___m_AddComponentMenu_0; }
	inline String_t** get_address_of_m_AddComponentMenu_0() { return &___m_AddComponentMenu_0; }
	inline void set_m_AddComponentMenu_0(String_t* value)
	{
		___m_AddComponentMenu_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_AddComponentMenu_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Ordering_1() { return static_cast<int32_t>(offsetof(AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100, ___m_Ordering_1)); }
	inline int32_t get_m_Ordering_1() const { return ___m_Ordering_1; }
	inline int32_t* get_address_of_m_Ordering_1() { return &___m_Ordering_1; }
	inline void set_m_Ordering_1(int32_t value)
	{
		___m_Ordering_1 = value;
	}
};


// System.Boolean
struct  Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct  CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 System.Runtime.CompilerServices.CompilationRelaxationsAttribute::m_relaxations
	int32_t ___m_relaxations_0;

public:
	inline static int32_t get_offset_of_m_relaxations_0() { return static_cast<int32_t>(offsetof(CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF, ___m_relaxations_0)); }
	inline int32_t get_m_relaxations_0() const { return ___m_relaxations_0; }
	inline int32_t* get_address_of_m_relaxations_0() { return &___m_relaxations_0; }
	inline void set_m_relaxations_0(int32_t value)
	{
		___m_relaxations_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct  CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Diagnostics.DebuggerHiddenAttribute
struct  DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Enum
struct  Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// UnityEngine.ExecuteInEditMode
struct  ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.HideInInspector
struct  HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// UnityEngine.PropertyAttribute
struct  PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.RequireComponent
struct  RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type UnityEngine.RequireComponent::m_Type0
	Type_t * ___m_Type0_0;
	// System.Type UnityEngine.RequireComponent::m_Type1
	Type_t * ___m_Type1_1;
	// System.Type UnityEngine.RequireComponent::m_Type2
	Type_t * ___m_Type2_2;

public:
	inline static int32_t get_offset_of_m_Type0_0() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type0_0)); }
	inline Type_t * get_m_Type0_0() const { return ___m_Type0_0; }
	inline Type_t ** get_address_of_m_Type0_0() { return &___m_Type0_0; }
	inline void set_m_Type0_0(Type_t * value)
	{
		___m_Type0_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type0_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type1_1() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type1_1)); }
	inline Type_t * get_m_Type1_1() const { return ___m_Type1_1; }
	inline Type_t ** get_address_of_m_Type1_1() { return &___m_Type1_1; }
	inline void set_m_Type1_1(Type_t * value)
	{
		___m_Type1_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type1_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type2_2() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type2_2)); }
	inline Type_t * get_m_Type2_2() const { return ___m_Type2_2; }
	inline Type_t ** get_address_of_m_Type2_2() { return &___m_Type2_2; }
	inline void set_m_Type2_2(Type_t * value)
	{
		___m_Type2_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type2_2), (void*)value);
	}
};


// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct  RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::m_wrapNonExceptionThrows
	bool ___m_wrapNonExceptionThrows_0;

public:
	inline static int32_t get_offset_of_m_wrapNonExceptionThrows_0() { return static_cast<int32_t>(offsetof(RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80, ___m_wrapNonExceptionThrows_0)); }
	inline bool get_m_wrapNonExceptionThrows_0() const { return ___m_wrapNonExceptionThrows_0; }
	inline bool* get_address_of_m_wrapNonExceptionThrows_0() { return &___m_wrapNonExceptionThrows_0; }
	inline void set_m_wrapNonExceptionThrows_0(bool value)
	{
		___m_wrapNonExceptionThrows_0 = value;
	}
};


// UnityEngine.SerializeField
struct  SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.StateMachineAttribute
struct  StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type System.Runtime.CompilerServices.StateMachineAttribute::<StateMachineType>k__BackingField
	Type_t * ___U3CStateMachineTypeU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CStateMachineTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3, ___U3CStateMachineTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CStateMachineTypeU3Ek__BackingField_0() const { return ___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CStateMachineTypeU3Ek__BackingField_0() { return &___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline void set_U3CStateMachineTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CStateMachineTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CStateMachineTypeU3Ek__BackingField_0), (void*)value);
	}
};


// System.Void
struct  Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.Reflection.BindingFlags
struct  BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.HeaderAttribute
struct  HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String UnityEngine.HeaderAttribute::header
	String_t* ___header_0;

public:
	inline static int32_t get_offset_of_header_0() { return static_cast<int32_t>(offsetof(HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB, ___header_0)); }
	inline String_t* get_header_0() const { return ___header_0; }
	inline String_t** get_address_of_header_0() { return &___header_0; }
	inline void set_header_0(String_t* value)
	{
		___header_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___header_0), (void*)value);
	}
};


// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct  IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830  : public StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3
{
public:

public:
};


// UnityEngine.RangeAttribute
struct  RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.Single UnityEngine.RangeAttribute::min
	float ___min_0;
	// System.Single UnityEngine.RangeAttribute::max
	float ___max_1;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5, ___min_0)); }
	inline float get_min_0() const { return ___min_0; }
	inline float* get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(float value)
	{
		___min_0 = value;
	}

	inline static int32_t get_offset_of_max_1() { return static_cast<int32_t>(offsetof(RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5, ___max_1)); }
	inline float get_max_1() const { return ___max_1; }
	inline float* get_address_of_max_1() { return &___max_1; }
	inline void set_max_1(float value)
	{
		___max_1 = value;
	}
};


// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// UnityEngine.SpaceAttribute
struct  SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.Single UnityEngine.SpaceAttribute::height
	float ___height_0;

public:
	inline static int32_t get_offset_of_height_0() { return static_cast<int32_t>(offsetof(SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8, ___height_0)); }
	inline float get_height_0() const { return ___height_0; }
	inline float* get_address_of_height_0() { return &___height_0; }
	inline void set_height_0(float value)
	{
		___height_0 = value;
	}
};


// UnityEngine.TooltipAttribute
struct  TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String UnityEngine.TooltipAttribute::tooltip
	String_t* ___tooltip_0;

public:
	inline static int32_t get_offset_of_tooltip_0() { return static_cast<int32_t>(offsetof(TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B, ___tooltip_0)); }
	inline String_t* get_tooltip_0() const { return ___tooltip_0; }
	inline String_t** get_address_of_tooltip_0() { return &___tooltip_0; }
	inline void set_tooltip_0(String_t* value)
	{
		___tooltip_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tooltip_0), (void*)value);
	}
};


// System.Diagnostics.DebuggableAttribute/DebuggingModes
struct  DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8 
{
public:
	// System.Int32 System.Diagnostics.DebuggableAttribute/DebuggingModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Diagnostics.DebuggableAttribute
struct  DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggableAttribute/DebuggingModes System.Diagnostics.DebuggableAttribute::m_debuggingModes
	int32_t ___m_debuggingModes_0;

public:
	inline static int32_t get_offset_of_m_debuggingModes_0() { return static_cast<int32_t>(offsetof(DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B, ___m_debuggingModes_0)); }
	inline int32_t get_m_debuggingModes_0() const { return ___m_debuggingModes_0; }
	inline int32_t* get_address_of_m_debuggingModes_0() { return &___m_debuggingModes_0; }
	inline void set_m_debuggingModes_0(int32_t value)
	{
		___m_debuggingModes_0 = value;
	}
};


// System.Type
struct  Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif



// System.Void System.Diagnostics.DebuggableAttribute::.ctor(System.Diagnostics.DebuggableAttribute/DebuggingModes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550 (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * __this, int32_t ___modes0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilationRelaxationsAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * __this, int32_t ___relaxations0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::set_WrapNonExceptionThrows(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.RangeAttribute::.ctor(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000 (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * __this, float ___min0, float ___max1, const RuntimeMethod* method);
// System.Void UnityEngine.SpaceAttribute::.ctor(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SpaceAttribute__ctor_m765D137779D8FB95279BCE4A90BAB4EA409C9C44 (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * __this, float ___height0, const RuntimeMethod* method);
// System.Void UnityEngine.HideInInspector::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9 (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * __this, const RuntimeMethod* method);
// System.Void UnityEngine.AddComponentMenu::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549 (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * __this, String_t* ___menuName0, const RuntimeMethod* method);
// System.Void UnityEngine.ExecuteInEditMode::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilerGeneratedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35 (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * __this, const RuntimeMethod* method);
// System.Void UnityEngine.SerializeField::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3 (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.IteratorStateMachineAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481 (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * __this, Type_t * ___stateMachineType0, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggerHiddenAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3 (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.HeaderAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * __this, String_t* ___header0, const RuntimeMethod* method);
// System.Void UnityEngine.RequireComponent::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4 (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * __this, Type_t * ___requiredComponent0, const RuntimeMethod* method);
// System.Void UnityEngine.TooltipAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042 (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * __this, String_t* ___tooltip0, const RuntimeMethod* method);
static void AssemblyU2DCSharp_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * tmp = (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B *)cache->attributes[0];
		DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550(tmp, 2LL, NULL);
	}
	{
		CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * tmp = (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF *)cache->attributes[1];
		CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B(tmp, 8LL, NULL);
	}
	{
		RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * tmp = (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 *)cache->attributes[2];
		RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline(tmp, true, NULL);
	}
}
static void CameraFollow_tC9B62E254DA1376073E7B793597F9D6CD2A82DF8_CustomAttributesCacheGenerator_followSpeed(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 1.0f, 10.0f, NULL);
	}
}
static void CameraFollow_tC9B62E254DA1376073E7B793597F9D6CD2A82DF8_CustomAttributesCacheGenerator_lookSpeed(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 1.0f, 10.0f, NULL);
	}
}
static void PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E_CustomAttributesCacheGenerator_maxSpeed(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 20.0f, 190.0f, NULL);
	}
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[1];
		SpaceAttribute__ctor_m765D137779D8FB95279BCE4A90BAB4EA409C9C44(tmp, 10.0f, NULL);
	}
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[2];
		SpaceAttribute__ctor_m765D137779D8FB95279BCE4A90BAB4EA409C9C44(tmp, 20.0f, NULL);
	}
}
static void PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E_CustomAttributesCacheGenerator_maxReverseSpeed(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 10.0f, 120.0f, NULL);
	}
}
static void PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E_CustomAttributesCacheGenerator_accelerationMultiplier(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 1.0f, 10.0f, NULL);
	}
}
static void PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E_CustomAttributesCacheGenerator_maxSteeringAngle(CustomAttributesCache* cache)
{
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m765D137779D8FB95279BCE4A90BAB4EA409C9C44(tmp, 10.0f, NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 10.0f, 45.0f, NULL);
	}
}
static void PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E_CustomAttributesCacheGenerator_steeringSpeed(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.100000001f, 1.0f, NULL);
	}
}
static void PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E_CustomAttributesCacheGenerator_brakeForce(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 100.0f, 600.0f, NULL);
	}
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[1];
		SpaceAttribute__ctor_m765D137779D8FB95279BCE4A90BAB4EA409C9C44(tmp, 10.0f, NULL);
	}
}
static void PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E_CustomAttributesCacheGenerator_decelerationMultiplier(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 1.0f, 10.0f, NULL);
	}
}
static void PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E_CustomAttributesCacheGenerator_handbrakeDriftMultiplier(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 1.0f, 10.0f, NULL);
	}
}
static void PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E_CustomAttributesCacheGenerator_bodyMassCenter(CustomAttributesCache* cache)
{
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m765D137779D8FB95279BCE4A90BAB4EA409C9C44(tmp, 10.0f, NULL);
	}
}
static void PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E_CustomAttributesCacheGenerator_frontRightMesh(CustomAttributesCache* cache)
{
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m765D137779D8FB95279BCE4A90BAB4EA409C9C44(tmp, 10.0f, NULL);
	}
}
static void PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E_CustomAttributesCacheGenerator_rearLeftMesh(CustomAttributesCache* cache)
{
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m765D137779D8FB95279BCE4A90BAB4EA409C9C44(tmp, 10.0f, NULL);
	}
}
static void PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E_CustomAttributesCacheGenerator_rearRightMesh(CustomAttributesCache* cache)
{
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m765D137779D8FB95279BCE4A90BAB4EA409C9C44(tmp, 10.0f, NULL);
	}
}
static void PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E_CustomAttributesCacheGenerator_useEffects(CustomAttributesCache* cache)
{
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m765D137779D8FB95279BCE4A90BAB4EA409C9C44(tmp, 20.0f, NULL);
	}
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[1];
		SpaceAttribute__ctor_m765D137779D8FB95279BCE4A90BAB4EA409C9C44(tmp, 10.0f, NULL);
	}
}
static void PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E_CustomAttributesCacheGenerator_RLWTireSkid(CustomAttributesCache* cache)
{
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m765D137779D8FB95279BCE4A90BAB4EA409C9C44(tmp, 10.0f, NULL);
	}
}
static void PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E_CustomAttributesCacheGenerator_useUI(CustomAttributesCache* cache)
{
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m765D137779D8FB95279BCE4A90BAB4EA409C9C44(tmp, 10.0f, NULL);
	}
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[1];
		SpaceAttribute__ctor_m765D137779D8FB95279BCE4A90BAB4EA409C9C44(tmp, 20.0f, NULL);
	}
}
static void PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E_CustomAttributesCacheGenerator_useSounds(CustomAttributesCache* cache)
{
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m765D137779D8FB95279BCE4A90BAB4EA409C9C44(tmp, 10.0f, NULL);
	}
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[1];
		SpaceAttribute__ctor_m765D137779D8FB95279BCE4A90BAB4EA409C9C44(tmp, 20.0f, NULL);
	}
}
static void PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E_CustomAttributesCacheGenerator_useTouchControls(CustomAttributesCache* cache)
{
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m765D137779D8FB95279BCE4A90BAB4EA409C9C44(tmp, 20.0f, NULL);
	}
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[1];
		SpaceAttribute__ctor_m765D137779D8FB95279BCE4A90BAB4EA409C9C44(tmp, 10.0f, NULL);
	}
}
static void PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E_CustomAttributesCacheGenerator_carSpeed(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E_CustomAttributesCacheGenerator_isDrifting(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E_CustomAttributesCacheGenerator_isTractionLocked(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void PrometeoTouchInput_t1C5A9B19512A75AFEAD2E916C6F53F597ADF73CF_CustomAttributesCacheGenerator_buttonPressed(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void MouseLook_t1E1EE79B13022184F3EF9AC8FE8C6771053890DA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x43\x61\x6D\x65\x72\x61\x2D\x43\x6F\x6E\x74\x72\x6F\x6C\x2F\x4D\x6F\x75\x73\x65\x20\x4C\x6F\x6F\x6B"), NULL);
	}
}
static void Water_t49D731BAF5A7CB66C04A942899CCE2391A114F4E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[0];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
}
static void PlayerInputActions_t375171A1A870F2F98685801D864FC76FD516F22C_CustomAttributesCacheGenerator_U3CassetU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PlayerInputActions_t375171A1A870F2F98685801D864FC76FD516F22C_CustomAttributesCacheGenerator_PlayerInputActions_get_asset_m3998FFE48C0D23FA068FF8995F9958EF79384CEF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AIController_t79B964E91146AC59C27CD306018BA3E368831F34_CustomAttributesCacheGenerator_vector1(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void AIController_t79B964E91146AC59C27CD306018BA3E368831F34_CustomAttributesCacheGenerator_vector2(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void AIController_t79B964E91146AC59C27CD306018BA3E368831F34_CustomAttributesCacheGenerator_rot(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void AIController_t79B964E91146AC59C27CD306018BA3E368831F34_CustomAttributesCacheGenerator_carTrans(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void AIController_t79B964E91146AC59C27CD306018BA3E368831F34_CustomAttributesCacheGenerator_cars(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void AIController_t79B964E91146AC59C27CD306018BA3E368831F34_CustomAttributesCacheGenerator_isFarEnough(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void AIController_t79B964E91146AC59C27CD306018BA3E368831F34_CustomAttributesCacheGenerator_onlyFirstVector(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void AIController_t79B964E91146AC59C27CD306018BA3E368831F34_CustomAttributesCacheGenerator_distance(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void AIController_t79B964E91146AC59C27CD306018BA3E368831F34_CustomAttributesCacheGenerator_maxDistance(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void AIController_t79B964E91146AC59C27CD306018BA3E368831F34_CustomAttributesCacheGenerator_timer(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CarAI_t69CE78AFFF0A7EFA2FFFDF485C42E5A279C8494A_CustomAttributesCacheGenerator_distanceWithLight(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CarAI_t69CE78AFFF0A7EFA2FFFDF485C42E5A279C8494A_CustomAttributesCacheGenerator_distanceWithCharacter(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CarAI_t69CE78AFFF0A7EFA2FFFDF485C42E5A279C8494A_CustomAttributesCacheGenerator_distanceWithCLosestCar(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CarAI_t69CE78AFFF0A7EFA2FFFDF485C42E5A279C8494A_CustomAttributesCacheGenerator_maxDistanceL(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CarAI_t69CE78AFFF0A7EFA2FFFDF485C42E5A279C8494A_CustomAttributesCacheGenerator_maxDistanceC(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CarAI_t69CE78AFFF0A7EFA2FFFDF485C42E5A279C8494A_CustomAttributesCacheGenerator_maxDistanceCars(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CarAI_t69CE78AFFF0A7EFA2FFFDF485C42E5A279C8494A_CustomAttributesCacheGenerator_speed(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CarAI_t69CE78AFFF0A7EFA2FFFDF485C42E5A279C8494A_CustomAttributesCacheGenerator_isCloseToCars(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CarController_tB945E8CD4EBA16BA6F4C8B4346292786701C9311_CustomAttributesCacheGenerator_isBreaking(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CarController_tB945E8CD4EBA16BA6F4C8B4346292786701C9311_CustomAttributesCacheGenerator_speed(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CarController_tB945E8CD4EBA16BA6F4C8B4346292786701C9311_CustomAttributesCacheGenerator_motorForce(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CarController_tB945E8CD4EBA16BA6F4C8B4346292786701C9311_CustomAttributesCacheGenerator_breakForce(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CarController_tB945E8CD4EBA16BA6F4C8B4346292786701C9311_CustomAttributesCacheGenerator_maxSteerAngle(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CarController_tB945E8CD4EBA16BA6F4C8B4346292786701C9311_CustomAttributesCacheGenerator_frontLeftWheelCollider(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CarController_tB945E8CD4EBA16BA6F4C8B4346292786701C9311_CustomAttributesCacheGenerator_frontRightWheelCollider(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CarController_tB945E8CD4EBA16BA6F4C8B4346292786701C9311_CustomAttributesCacheGenerator_rearLeftWheelCollider(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CarController_tB945E8CD4EBA16BA6F4C8B4346292786701C9311_CustomAttributesCacheGenerator_rearRightWheelCollider(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CarController_tB945E8CD4EBA16BA6F4C8B4346292786701C9311_CustomAttributesCacheGenerator_frontLeftWheelTransform(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CarController_tB945E8CD4EBA16BA6F4C8B4346292786701C9311_CustomAttributesCacheGenerator_frontRightWheeTransform(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CarController_tB945E8CD4EBA16BA6F4C8B4346292786701C9311_CustomAttributesCacheGenerator_rearLeftWheelTransform(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CarController_tB945E8CD4EBA16BA6F4C8B4346292786701C9311_CustomAttributesCacheGenerator_rearRightWheelTransform(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameEndController_t22B3012DEB58F882ECF6BE83EB24B4997F177C12_CustomAttributesCacheGenerator_wonPanel(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameEndController_t22B3012DEB58F882ECF6BE83EB24B4997F177C12_CustomAttributesCacheGenerator_wonPanelText(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameEndController_t22B3012DEB58F882ECF6BE83EB24B4997F177C12_CustomAttributesCacheGenerator_wonPanelSound(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameEndController_t22B3012DEB58F882ECF6BE83EB24B4997F177C12_CustomAttributesCacheGenerator_failPanel(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameEndController_t22B3012DEB58F882ECF6BE83EB24B4997F177C12_CustomAttributesCacheGenerator_failPanelText(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameEndController_t22B3012DEB58F882ECF6BE83EB24B4997F177C12_CustomAttributesCacheGenerator_failPanelSound(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void RoadDecider_tCF6285CFDF778A9348BC78EF830371F1855A9E61_CustomAttributesCacheGenerator_GoTo(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SignalsCar_t381616BF85EDCCF005FD247DC7E72DB6EAD4DAB0_CustomAttributesCacheGenerator_hazardActive(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SignalsCar_t381616BF85EDCCF005FD247DC7E72DB6EAD4DAB0_CustomAttributesCacheGenerator_rightActive(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SignalsCar_t381616BF85EDCCF005FD247DC7E72DB6EAD4DAB0_CustomAttributesCacheGenerator_leftActive(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SignalsCar_t381616BF85EDCCF005FD247DC7E72DB6EAD4DAB0_CustomAttributesCacheGenerator_timer(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SignalsCar_t381616BF85EDCCF005FD247DC7E72DB6EAD4DAB0_CustomAttributesCacheGenerator_rightLight(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SignalsCar_t381616BF85EDCCF005FD247DC7E72DB6EAD4DAB0_CustomAttributesCacheGenerator_leftLight(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SignalsCar_t381616BF85EDCCF005FD247DC7E72DB6EAD4DAB0_CustomAttributesCacheGenerator_hazardLight(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SignalsCar_t381616BF85EDCCF005FD247DC7E72DB6EAD4DAB0_CustomAttributesCacheGenerator_vrCam(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SignalsCar_t381616BF85EDCCF005FD247DC7E72DB6EAD4DAB0_CustomAttributesCacheGenerator_vrComponent(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SignalsCar_t381616BF85EDCCF005FD247DC7E72DB6EAD4DAB0_CustomAttributesCacheGenerator_normalCam(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SignalsCar_t381616BF85EDCCF005FD247DC7E72DB6EAD4DAB0_CustomAttributesCacheGenerator_vrEnabled(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SignalsCar_t381616BF85EDCCF005FD247DC7E72DB6EAD4DAB0_CustomAttributesCacheGenerator_LeftPointLight(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SignalsCar_t381616BF85EDCCF005FD247DC7E72DB6EAD4DAB0_CustomAttributesCacheGenerator_RightPointLight(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SignalsCar_t381616BF85EDCCF005FD247DC7E72DB6EAD4DAB0_CustomAttributesCacheGenerator_signalColor(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SignalsCar_t381616BF85EDCCF005FD247DC7E72DB6EAD4DAB0_CustomAttributesCacheGenerator_hazardColor(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SignalsCar_t381616BF85EDCCF005FD247DC7E72DB6EAD4DAB0_CustomAttributesCacheGenerator_turnSignalSound(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SignalsCar_t381616BF85EDCCF005FD247DC7E72DB6EAD4DAB0_CustomAttributesCacheGenerator_hazardSound(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TrafficLights_t6127A0CC6322E5899984953DA144B63D24333A73_CustomAttributesCacheGenerator_lightCount(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TrafficLights_t6127A0CC6322E5899984953DA144B63D24333A73_CustomAttributesCacheGenerator_trafficLights(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TrafficLights_t6127A0CC6322E5899984953DA144B63D24333A73_CustomAttributesCacheGenerator_isProcessing(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TrafficLights_t6127A0CC6322E5899984953DA144B63D24333A73_CustomAttributesCacheGenerator_howManySecs(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TrafficLights_t6127A0CC6322E5899984953DA144B63D24333A73_CustomAttributesCacheGenerator_timer(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TrafficRulesController_Car_tB16F6F9B85938EDD1604F77EF7DC0A1BDB472A60_CustomAttributesCacheGenerator_speed(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TrafficRulesController_Car_tB16F6F9B85938EDD1604F77EF7DC0A1BDB472A60_CustomAttributesCacheGenerator_speedLimit(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TrafficRulesController_Car_tB16F6F9B85938EDD1604F77EF7DC0A1BDB472A60_CustomAttributesCacheGenerator_State(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TrafficRulesController_Car_tB16F6F9B85938EDD1604F77EF7DC0A1BDB472A60_CustomAttributesCacheGenerator_direction(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TrafficRulesController_Car_tB16F6F9B85938EDD1604F77EF7DC0A1BDB472A60_CustomAttributesCacheGenerator_WalkerWay(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TrafficRulesController_Car_tB16F6F9B85938EDD1604F77EF7DC0A1BDB472A60_CustomAttributesCacheGenerator_signalController(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TrafficRulesController_Car_tB16F6F9B85938EDD1604F77EF7DC0A1BDB472A60_CustomAttributesCacheGenerator_Decider(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TrafficRulesController_Car_tB16F6F9B85938EDD1604F77EF7DC0A1BDB472A60_CustomAttributesCacheGenerator_RoadStart(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TrafficRulesController_Car_tB16F6F9B85938EDD1604F77EF7DC0A1BDB472A60_CustomAttributesCacheGenerator_Directions(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TrafficRulesController_Car_tB16F6F9B85938EDD1604F77EF7DC0A1BDB472A60_CustomAttributesCacheGenerator_Rules(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TrafficRulesController_Car_tB16F6F9B85938EDD1604F77EF7DC0A1BDB472A60_CustomAttributesCacheGenerator_Controller(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TrafficRulesController_Car_tB16F6F9B85938EDD1604F77EF7DC0A1BDB472A60_CustomAttributesCacheGenerator_shouldBe(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void VRControls_tEAE1C9E737B679D74AD0DA693794E297010CFA77_CustomAttributesCacheGenerator_VRControls_SimulateKeyPress_m42F3AD2DAACED561F00377A6F9732B1A3AE1DC51(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CSimulateKeyPressU3Ed__8_tAB6F8BC46272BF48D20F34C4D00E6A15FCEF04C6_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CSimulateKeyPressU3Ed__8_tAB6F8BC46272BF48D20F34C4D00E6A15FCEF04C6_0_0_0_var), NULL);
	}
}
static void U3CSimulateKeyPressU3Ed__8_tAB6F8BC46272BF48D20F34C4D00E6A15FCEF04C6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CSimulateKeyPressU3Ed__8_tAB6F8BC46272BF48D20F34C4D00E6A15FCEF04C6_CustomAttributesCacheGenerator_U3CSimulateKeyPressU3Ed__8__ctor_mB333C0FA66B2395C19496CC79C6100503A0633BF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSimulateKeyPressU3Ed__8_tAB6F8BC46272BF48D20F34C4D00E6A15FCEF04C6_CustomAttributesCacheGenerator_U3CSimulateKeyPressU3Ed__8_System_IDisposable_Dispose_m6B6AA45F2BFEA8845A8F47384DAECB7A55DC4FC9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSimulateKeyPressU3Ed__8_tAB6F8BC46272BF48D20F34C4D00E6A15FCEF04C6_CustomAttributesCacheGenerator_U3CSimulateKeyPressU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFB7AE2BA8C02D3857C2599A9D8B6389319488E07(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSimulateKeyPressU3Ed__8_tAB6F8BC46272BF48D20F34C4D00E6A15FCEF04C6_CustomAttributesCacheGenerator_U3CSimulateKeyPressU3Ed__8_System_Collections_IEnumerator_Reset_m5A0CAFC39F155EEFA816F434F43DEBC933C7FD10(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSimulateKeyPressU3Ed__8_tAB6F8BC46272BF48D20F34C4D00E6A15FCEF04C6_CustomAttributesCacheGenerator_U3CSimulateKeyPressU3Ed__8_System_Collections_IEnumerator_get_Current_m8D210BFBFCCE65F373299DC960D8D4FE87D5C0EE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void WalkerWay_tE1DBB9C531DC74D2654F2AB74C9D6E1304A418DD_CustomAttributesCacheGenerator_stopColor(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void WalkerWay_tE1DBB9C531DC74D2654F2AB74C9D6E1304A418DD_CustomAttributesCacheGenerator_passColor(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void WalkerWay_tE1DBB9C531DC74D2654F2AB74C9D6E1304A418DD_CustomAttributesCacheGenerator_timer(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void WalkerWay_tE1DBB9C531DC74D2654F2AB74C9D6E1304A418DD_CustomAttributesCacheGenerator_speed(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MobileDisableAutoSwitchControls_tFFBB1531EDDED48A8D620DF56060164FE3482D4F_CustomAttributesCacheGenerator_playerInput(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x54\x61\x72\x67\x65\x74"), NULL);
	}
}
static void UIVirtualButton_tA1A6B564C5E71E1B61B240E28958DEB8F79BABBE_CustomAttributesCacheGenerator_buttonStateOutputEvent(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x4F\x75\x74\x70\x75\x74"), NULL);
	}
}
static void UIVirtualJoystick_tEF0053DAA02EDBA00713D7B29C34394EA7574C2D_CustomAttributesCacheGenerator_containerRect(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x63\x74\x20\x52\x65\x66\x65\x72\x65\x6E\x63\x65\x73"), NULL);
	}
}
static void UIVirtualJoystick_tEF0053DAA02EDBA00713D7B29C34394EA7574C2D_CustomAttributesCacheGenerator_joystickRange(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x53\x65\x74\x74\x69\x6E\x67\x73"), NULL);
	}
}
static void UIVirtualJoystick_tEF0053DAA02EDBA00713D7B29C34394EA7574C2D_CustomAttributesCacheGenerator_joystickOutputEvent(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x4F\x75\x74\x70\x75\x74"), NULL);
	}
}
static void UIVirtualTouchZone_t2EB72E6BED3964232BD92D6723DEA7BA013BED23_CustomAttributesCacheGenerator_containerRect(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x63\x74\x20\x52\x65\x66\x65\x72\x65\x6E\x63\x65\x73"), NULL);
	}
}
static void UIVirtualTouchZone_t2EB72E6BED3964232BD92D6723DEA7BA013BED23_CustomAttributesCacheGenerator_clampToMagnitude(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x53\x65\x74\x74\x69\x6E\x67\x73"), NULL);
	}
}
static void UIVirtualTouchZone_t2EB72E6BED3964232BD92D6723DEA7BA013BED23_CustomAttributesCacheGenerator_touchZoneOutputEvent(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x4F\x75\x74\x70\x75\x74"), NULL);
	}
}
static void BasicRigidBodyPush_t9E4C23843BA4437AF06C87D968F0A0BE0B41C714_CustomAttributesCacheGenerator_strength(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.5f, 5.0f, NULL);
	}
}
static void StarterAssetsInputs_tE7DDADB7E0E59B93F86B819E96C2523BC28F16DC_CustomAttributesCacheGenerator_move(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x43\x68\x61\x72\x61\x63\x74\x65\x72\x20\x49\x6E\x70\x75\x74\x20\x56\x61\x6C\x75\x65\x73"), NULL);
	}
}
static void StarterAssetsInputs_tE7DDADB7E0E59B93F86B819E96C2523BC28F16DC_CustomAttributesCacheGenerator_analogMovement(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x6F\x76\x65\x6D\x65\x6E\x74\x20\x53\x65\x74\x74\x69\x6E\x67\x73"), NULL);
	}
}
static void StarterAssetsInputs_tE7DDADB7E0E59B93F86B819E96C2523BC28F16DC_CustomAttributesCacheGenerator_cursorLocked(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x6F\x75\x73\x65\x20\x43\x75\x72\x73\x6F\x72\x20\x53\x65\x74\x74\x69\x6E\x67\x73"), NULL);
	}
}
static void UICanvasControllerInput_t18C69AA8A6225F0EDEC9D31926AFCA88FC8F8EA1_CustomAttributesCacheGenerator_starterAssetsInputs(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x4F\x75\x74\x70\x75\x74"), NULL);
	}
}
static void ThirdPersonController_tCA99A63A4BE57B6A9950FFA8CFFABE8F82AD0903_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CharacterController_tCCF68621C784CCB3391E0C66FE134F6F93DD6C2E_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PlayerInput_tCB9A1A84F447F3392889C457C9BE413891FE848A_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(CharacterController_tCCF68621C784CCB3391E0C66FE134F6F93DD6C2E_0_0_0_var), NULL);
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[1];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(PlayerInput_tCB9A1A84F447F3392889C457C9BE413891FE848A_0_0_0_var), NULL);
	}
}
static void ThirdPersonController_tCA99A63A4BE57B6A9950FFA8CFFABE8F82AD0903_CustomAttributesCacheGenerator_MoveSpeed(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x50\x6C\x61\x79\x65\x72"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x6F\x76\x65\x20\x73\x70\x65\x65\x64\x20\x6F\x66\x20\x74\x68\x65\x20\x63\x68\x61\x72\x61\x63\x74\x65\x72\x20\x69\x6E\x20\x6D\x2F\x73"), NULL);
	}
}
static void ThirdPersonController_tCA99A63A4BE57B6A9950FFA8CFFABE8F82AD0903_CustomAttributesCacheGenerator_SprintSpeed(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x70\x72\x69\x6E\x74\x20\x73\x70\x65\x65\x64\x20\x6F\x66\x20\x74\x68\x65\x20\x63\x68\x61\x72\x61\x63\x74\x65\x72\x20\x69\x6E\x20\x6D\x2F\x73"), NULL);
	}
}
static void ThirdPersonController_tCA99A63A4BE57B6A9950FFA8CFFABE8F82AD0903_CustomAttributesCacheGenerator_RotationSmoothTime(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x48\x6F\x77\x20\x66\x61\x73\x74\x20\x74\x68\x65\x20\x63\x68\x61\x72\x61\x63\x74\x65\x72\x20\x74\x75\x72\x6E\x73\x20\x74\x6F\x20\x66\x61\x63\x65\x20\x6D\x6F\x76\x65\x6D\x65\x6E\x74\x20\x64\x69\x72\x65\x63\x74\x69\x6F\x6E"), NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 0.300000012f, NULL);
	}
}
static void ThirdPersonController_tCA99A63A4BE57B6A9950FFA8CFFABE8F82AD0903_CustomAttributesCacheGenerator_SpeedChangeRate(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x41\x63\x63\x65\x6C\x65\x72\x61\x74\x69\x6F\x6E\x20\x61\x6E\x64\x20\x64\x65\x63\x65\x6C\x65\x72\x61\x74\x69\x6F\x6E"), NULL);
	}
}
static void ThirdPersonController_tCA99A63A4BE57B6A9950FFA8CFFABE8F82AD0903_CustomAttributesCacheGenerator_FootstepAudioVolume(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void ThirdPersonController_tCA99A63A4BE57B6A9950FFA8CFFABE8F82AD0903_CustomAttributesCacheGenerator_JumpHeight(CustomAttributesCache* cache)
{
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m765D137779D8FB95279BCE4A90BAB4EA409C9C44(tmp, 10.0f, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x68\x65\x69\x67\x68\x74\x20\x74\x68\x65\x20\x70\x6C\x61\x79\x65\x72\x20\x63\x61\x6E\x20\x6A\x75\x6D\x70"), NULL);
	}
}
static void ThirdPersonController_tCA99A63A4BE57B6A9950FFA8CFFABE8F82AD0903_CustomAttributesCacheGenerator_Gravity(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x63\x68\x61\x72\x61\x63\x74\x65\x72\x20\x75\x73\x65\x73\x20\x69\x74\x73\x20\x6F\x77\x6E\x20\x67\x72\x61\x76\x69\x74\x79\x20\x76\x61\x6C\x75\x65\x2E\x20\x54\x68\x65\x20\x65\x6E\x67\x69\x6E\x65\x20\x64\x65\x66\x61\x75\x6C\x74\x20\x69\x73\x20\x2D\x39\x2E\x38\x31\x66"), NULL);
	}
}
static void ThirdPersonController_tCA99A63A4BE57B6A9950FFA8CFFABE8F82AD0903_CustomAttributesCacheGenerator_JumpTimeout(CustomAttributesCache* cache)
{
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m765D137779D8FB95279BCE4A90BAB4EA409C9C44(tmp, 10.0f, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x69\x6D\x65\x20\x72\x65\x71\x75\x69\x72\x65\x64\x20\x74\x6F\x20\x70\x61\x73\x73\x20\x62\x65\x66\x6F\x72\x65\x20\x62\x65\x69\x6E\x67\x20\x61\x62\x6C\x65\x20\x74\x6F\x20\x6A\x75\x6D\x70\x20\x61\x67\x61\x69\x6E\x2E\x20\x53\x65\x74\x20\x74\x6F\x20\x30\x66\x20\x74\x6F\x20\x69\x6E\x73\x74\x61\x6E\x74\x6C\x79\x20\x6A\x75\x6D\x70\x20\x61\x67\x61\x69\x6E"), NULL);
	}
}
static void ThirdPersonController_tCA99A63A4BE57B6A9950FFA8CFFABE8F82AD0903_CustomAttributesCacheGenerator_FallTimeout(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x69\x6D\x65\x20\x72\x65\x71\x75\x69\x72\x65\x64\x20\x74\x6F\x20\x70\x61\x73\x73\x20\x62\x65\x66\x6F\x72\x65\x20\x65\x6E\x74\x65\x72\x69\x6E\x67\x20\x74\x68\x65\x20\x66\x61\x6C\x6C\x20\x73\x74\x61\x74\x65\x2E\x20\x55\x73\x65\x66\x75\x6C\x20\x66\x6F\x72\x20\x77\x61\x6C\x6B\x69\x6E\x67\x20\x64\x6F\x77\x6E\x20\x73\x74\x61\x69\x72\x73"), NULL);
	}
}
static void ThirdPersonController_tCA99A63A4BE57B6A9950FFA8CFFABE8F82AD0903_CustomAttributesCacheGenerator_Grounded(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x49\x66\x20\x74\x68\x65\x20\x63\x68\x61\x72\x61\x63\x74\x65\x72\x20\x69\x73\x20\x67\x72\x6F\x75\x6E\x64\x65\x64\x20\x6F\x72\x20\x6E\x6F\x74\x2E\x20\x4E\x6F\x74\x20\x70\x61\x72\x74\x20\x6F\x66\x20\x74\x68\x65\x20\x43\x68\x61\x72\x61\x63\x74\x65\x72\x43\x6F\x6E\x74\x72\x6F\x6C\x6C\x65\x72\x20\x62\x75\x69\x6C\x74\x20\x69\x6E\x20\x67\x72\x6F\x75\x6E\x64\x65\x64\x20\x63\x68\x65\x63\x6B"), NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x50\x6C\x61\x79\x65\x72\x20\x47\x72\x6F\x75\x6E\x64\x65\x64"), NULL);
	}
}
static void ThirdPersonController_tCA99A63A4BE57B6A9950FFA8CFFABE8F82AD0903_CustomAttributesCacheGenerator_GroundedOffset(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x55\x73\x65\x66\x75\x6C\x20\x66\x6F\x72\x20\x72\x6F\x75\x67\x68\x20\x67\x72\x6F\x75\x6E\x64"), NULL);
	}
}
static void ThirdPersonController_tCA99A63A4BE57B6A9950FFA8CFFABE8F82AD0903_CustomAttributesCacheGenerator_GroundedRadius(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x72\x61\x64\x69\x75\x73\x20\x6F\x66\x20\x74\x68\x65\x20\x67\x72\x6F\x75\x6E\x64\x65\x64\x20\x63\x68\x65\x63\x6B\x2E\x20\x53\x68\x6F\x75\x6C\x64\x20\x6D\x61\x74\x63\x68\x20\x74\x68\x65\x20\x72\x61\x64\x69\x75\x73\x20\x6F\x66\x20\x74\x68\x65\x20\x43\x68\x61\x72\x61\x63\x74\x65\x72\x43\x6F\x6E\x74\x72\x6F\x6C\x6C\x65\x72"), NULL);
	}
}
static void ThirdPersonController_tCA99A63A4BE57B6A9950FFA8CFFABE8F82AD0903_CustomAttributesCacheGenerator_GroundLayers(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x57\x68\x61\x74\x20\x6C\x61\x79\x65\x72\x73\x20\x74\x68\x65\x20\x63\x68\x61\x72\x61\x63\x74\x65\x72\x20\x75\x73\x65\x73\x20\x61\x73\x20\x67\x72\x6F\x75\x6E\x64"), NULL);
	}
}
static void ThirdPersonController_tCA99A63A4BE57B6A9950FFA8CFFABE8F82AD0903_CustomAttributesCacheGenerator_CinemachineCameraTarget(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x66\x6F\x6C\x6C\x6F\x77\x20\x74\x61\x72\x67\x65\x74\x20\x73\x65\x74\x20\x69\x6E\x20\x74\x68\x65\x20\x43\x69\x6E\x65\x6D\x61\x63\x68\x69\x6E\x65\x20\x56\x69\x72\x74\x75\x61\x6C\x20\x43\x61\x6D\x65\x72\x61\x20\x74\x68\x61\x74\x20\x74\x68\x65\x20\x63\x61\x6D\x65\x72\x61\x20\x77\x69\x6C\x6C\x20\x66\x6F\x6C\x6C\x6F\x77"), NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x43\x69\x6E\x65\x6D\x61\x63\x68\x69\x6E\x65"), NULL);
	}
}
static void ThirdPersonController_tCA99A63A4BE57B6A9950FFA8CFFABE8F82AD0903_CustomAttributesCacheGenerator_TopClamp(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x48\x6F\x77\x20\x66\x61\x72\x20\x69\x6E\x20\x64\x65\x67\x72\x65\x65\x73\x20\x63\x61\x6E\x20\x79\x6F\x75\x20\x6D\x6F\x76\x65\x20\x74\x68\x65\x20\x63\x61\x6D\x65\x72\x61\x20\x75\x70"), NULL);
	}
}
static void ThirdPersonController_tCA99A63A4BE57B6A9950FFA8CFFABE8F82AD0903_CustomAttributesCacheGenerator_BottomClamp(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x48\x6F\x77\x20\x66\x61\x72\x20\x69\x6E\x20\x64\x65\x67\x72\x65\x65\x73\x20\x63\x61\x6E\x20\x79\x6F\x75\x20\x6D\x6F\x76\x65\x20\x74\x68\x65\x20\x63\x61\x6D\x65\x72\x61\x20\x64\x6F\x77\x6E"), NULL);
	}
}
static void ThirdPersonController_tCA99A63A4BE57B6A9950FFA8CFFABE8F82AD0903_CustomAttributesCacheGenerator_CameraAngleOverride(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x41\x64\x64\x69\x74\x69\x6F\x6E\x61\x6C\x20\x64\x65\x67\x72\x65\x73\x73\x20\x74\x6F\x20\x6F\x76\x65\x72\x72\x69\x64\x65\x20\x74\x68\x65\x20\x63\x61\x6D\x65\x72\x61\x2E\x20\x55\x73\x65\x66\x75\x6C\x20\x66\x6F\x72\x20\x66\x69\x6E\x65\x20\x74\x75\x6E\x69\x6E\x67\x20\x63\x61\x6D\x65\x72\x61\x20\x70\x6F\x73\x69\x74\x69\x6F\x6E\x20\x77\x68\x65\x6E\x20\x6C\x6F\x63\x6B\x65\x64"), NULL);
	}
}
static void ThirdPersonController_tCA99A63A4BE57B6A9950FFA8CFFABE8F82AD0903_CustomAttributesCacheGenerator_LockCameraPosition(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x46\x6F\x72\x20\x6C\x6F\x63\x6B\x69\x6E\x67\x20\x74\x68\x65\x20\x63\x61\x6D\x65\x72\x61\x20\x70\x6F\x73\x69\x74\x69\x6F\x6E\x20\x6F\x6E\x20\x61\x6C\x6C\x20\x61\x78\x69\x73"), NULL);
	}
}
IL2CPP_EXTERN_C const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[143] = 
{
	MouseLook_t1E1EE79B13022184F3EF9AC8FE8C6771053890DA_CustomAttributesCacheGenerator,
	Water_t49D731BAF5A7CB66C04A942899CCE2391A114F4E_CustomAttributesCacheGenerator,
	U3CSimulateKeyPressU3Ed__8_tAB6F8BC46272BF48D20F34C4D00E6A15FCEF04C6_CustomAttributesCacheGenerator,
	ThirdPersonController_tCA99A63A4BE57B6A9950FFA8CFFABE8F82AD0903_CustomAttributesCacheGenerator,
	CameraFollow_tC9B62E254DA1376073E7B793597F9D6CD2A82DF8_CustomAttributesCacheGenerator_followSpeed,
	CameraFollow_tC9B62E254DA1376073E7B793597F9D6CD2A82DF8_CustomAttributesCacheGenerator_lookSpeed,
	PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E_CustomAttributesCacheGenerator_maxSpeed,
	PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E_CustomAttributesCacheGenerator_maxReverseSpeed,
	PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E_CustomAttributesCacheGenerator_accelerationMultiplier,
	PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E_CustomAttributesCacheGenerator_maxSteeringAngle,
	PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E_CustomAttributesCacheGenerator_steeringSpeed,
	PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E_CustomAttributesCacheGenerator_brakeForce,
	PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E_CustomAttributesCacheGenerator_decelerationMultiplier,
	PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E_CustomAttributesCacheGenerator_handbrakeDriftMultiplier,
	PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E_CustomAttributesCacheGenerator_bodyMassCenter,
	PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E_CustomAttributesCacheGenerator_frontRightMesh,
	PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E_CustomAttributesCacheGenerator_rearLeftMesh,
	PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E_CustomAttributesCacheGenerator_rearRightMesh,
	PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E_CustomAttributesCacheGenerator_useEffects,
	PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E_CustomAttributesCacheGenerator_RLWTireSkid,
	PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E_CustomAttributesCacheGenerator_useUI,
	PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E_CustomAttributesCacheGenerator_useSounds,
	PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E_CustomAttributesCacheGenerator_useTouchControls,
	PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E_CustomAttributesCacheGenerator_carSpeed,
	PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E_CustomAttributesCacheGenerator_isDrifting,
	PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E_CustomAttributesCacheGenerator_isTractionLocked,
	PrometeoTouchInput_t1C5A9B19512A75AFEAD2E916C6F53F597ADF73CF_CustomAttributesCacheGenerator_buttonPressed,
	PlayerInputActions_t375171A1A870F2F98685801D864FC76FD516F22C_CustomAttributesCacheGenerator_U3CassetU3Ek__BackingField,
	AIController_t79B964E91146AC59C27CD306018BA3E368831F34_CustomAttributesCacheGenerator_vector1,
	AIController_t79B964E91146AC59C27CD306018BA3E368831F34_CustomAttributesCacheGenerator_vector2,
	AIController_t79B964E91146AC59C27CD306018BA3E368831F34_CustomAttributesCacheGenerator_rot,
	AIController_t79B964E91146AC59C27CD306018BA3E368831F34_CustomAttributesCacheGenerator_carTrans,
	AIController_t79B964E91146AC59C27CD306018BA3E368831F34_CustomAttributesCacheGenerator_cars,
	AIController_t79B964E91146AC59C27CD306018BA3E368831F34_CustomAttributesCacheGenerator_isFarEnough,
	AIController_t79B964E91146AC59C27CD306018BA3E368831F34_CustomAttributesCacheGenerator_onlyFirstVector,
	AIController_t79B964E91146AC59C27CD306018BA3E368831F34_CustomAttributesCacheGenerator_distance,
	AIController_t79B964E91146AC59C27CD306018BA3E368831F34_CustomAttributesCacheGenerator_maxDistance,
	AIController_t79B964E91146AC59C27CD306018BA3E368831F34_CustomAttributesCacheGenerator_timer,
	CarAI_t69CE78AFFF0A7EFA2FFFDF485C42E5A279C8494A_CustomAttributesCacheGenerator_distanceWithLight,
	CarAI_t69CE78AFFF0A7EFA2FFFDF485C42E5A279C8494A_CustomAttributesCacheGenerator_distanceWithCharacter,
	CarAI_t69CE78AFFF0A7EFA2FFFDF485C42E5A279C8494A_CustomAttributesCacheGenerator_distanceWithCLosestCar,
	CarAI_t69CE78AFFF0A7EFA2FFFDF485C42E5A279C8494A_CustomAttributesCacheGenerator_maxDistanceL,
	CarAI_t69CE78AFFF0A7EFA2FFFDF485C42E5A279C8494A_CustomAttributesCacheGenerator_maxDistanceC,
	CarAI_t69CE78AFFF0A7EFA2FFFDF485C42E5A279C8494A_CustomAttributesCacheGenerator_maxDistanceCars,
	CarAI_t69CE78AFFF0A7EFA2FFFDF485C42E5A279C8494A_CustomAttributesCacheGenerator_speed,
	CarAI_t69CE78AFFF0A7EFA2FFFDF485C42E5A279C8494A_CustomAttributesCacheGenerator_isCloseToCars,
	CarController_tB945E8CD4EBA16BA6F4C8B4346292786701C9311_CustomAttributesCacheGenerator_isBreaking,
	CarController_tB945E8CD4EBA16BA6F4C8B4346292786701C9311_CustomAttributesCacheGenerator_speed,
	CarController_tB945E8CD4EBA16BA6F4C8B4346292786701C9311_CustomAttributesCacheGenerator_motorForce,
	CarController_tB945E8CD4EBA16BA6F4C8B4346292786701C9311_CustomAttributesCacheGenerator_breakForce,
	CarController_tB945E8CD4EBA16BA6F4C8B4346292786701C9311_CustomAttributesCacheGenerator_maxSteerAngle,
	CarController_tB945E8CD4EBA16BA6F4C8B4346292786701C9311_CustomAttributesCacheGenerator_frontLeftWheelCollider,
	CarController_tB945E8CD4EBA16BA6F4C8B4346292786701C9311_CustomAttributesCacheGenerator_frontRightWheelCollider,
	CarController_tB945E8CD4EBA16BA6F4C8B4346292786701C9311_CustomAttributesCacheGenerator_rearLeftWheelCollider,
	CarController_tB945E8CD4EBA16BA6F4C8B4346292786701C9311_CustomAttributesCacheGenerator_rearRightWheelCollider,
	CarController_tB945E8CD4EBA16BA6F4C8B4346292786701C9311_CustomAttributesCacheGenerator_frontLeftWheelTransform,
	CarController_tB945E8CD4EBA16BA6F4C8B4346292786701C9311_CustomAttributesCacheGenerator_frontRightWheeTransform,
	CarController_tB945E8CD4EBA16BA6F4C8B4346292786701C9311_CustomAttributesCacheGenerator_rearLeftWheelTransform,
	CarController_tB945E8CD4EBA16BA6F4C8B4346292786701C9311_CustomAttributesCacheGenerator_rearRightWheelTransform,
	GameEndController_t22B3012DEB58F882ECF6BE83EB24B4997F177C12_CustomAttributesCacheGenerator_wonPanel,
	GameEndController_t22B3012DEB58F882ECF6BE83EB24B4997F177C12_CustomAttributesCacheGenerator_wonPanelText,
	GameEndController_t22B3012DEB58F882ECF6BE83EB24B4997F177C12_CustomAttributesCacheGenerator_wonPanelSound,
	GameEndController_t22B3012DEB58F882ECF6BE83EB24B4997F177C12_CustomAttributesCacheGenerator_failPanel,
	GameEndController_t22B3012DEB58F882ECF6BE83EB24B4997F177C12_CustomAttributesCacheGenerator_failPanelText,
	GameEndController_t22B3012DEB58F882ECF6BE83EB24B4997F177C12_CustomAttributesCacheGenerator_failPanelSound,
	RoadDecider_tCF6285CFDF778A9348BC78EF830371F1855A9E61_CustomAttributesCacheGenerator_GoTo,
	SignalsCar_t381616BF85EDCCF005FD247DC7E72DB6EAD4DAB0_CustomAttributesCacheGenerator_hazardActive,
	SignalsCar_t381616BF85EDCCF005FD247DC7E72DB6EAD4DAB0_CustomAttributesCacheGenerator_rightActive,
	SignalsCar_t381616BF85EDCCF005FD247DC7E72DB6EAD4DAB0_CustomAttributesCacheGenerator_leftActive,
	SignalsCar_t381616BF85EDCCF005FD247DC7E72DB6EAD4DAB0_CustomAttributesCacheGenerator_timer,
	SignalsCar_t381616BF85EDCCF005FD247DC7E72DB6EAD4DAB0_CustomAttributesCacheGenerator_rightLight,
	SignalsCar_t381616BF85EDCCF005FD247DC7E72DB6EAD4DAB0_CustomAttributesCacheGenerator_leftLight,
	SignalsCar_t381616BF85EDCCF005FD247DC7E72DB6EAD4DAB0_CustomAttributesCacheGenerator_hazardLight,
	SignalsCar_t381616BF85EDCCF005FD247DC7E72DB6EAD4DAB0_CustomAttributesCacheGenerator_vrCam,
	SignalsCar_t381616BF85EDCCF005FD247DC7E72DB6EAD4DAB0_CustomAttributesCacheGenerator_vrComponent,
	SignalsCar_t381616BF85EDCCF005FD247DC7E72DB6EAD4DAB0_CustomAttributesCacheGenerator_normalCam,
	SignalsCar_t381616BF85EDCCF005FD247DC7E72DB6EAD4DAB0_CustomAttributesCacheGenerator_vrEnabled,
	SignalsCar_t381616BF85EDCCF005FD247DC7E72DB6EAD4DAB0_CustomAttributesCacheGenerator_LeftPointLight,
	SignalsCar_t381616BF85EDCCF005FD247DC7E72DB6EAD4DAB0_CustomAttributesCacheGenerator_RightPointLight,
	SignalsCar_t381616BF85EDCCF005FD247DC7E72DB6EAD4DAB0_CustomAttributesCacheGenerator_signalColor,
	SignalsCar_t381616BF85EDCCF005FD247DC7E72DB6EAD4DAB0_CustomAttributesCacheGenerator_hazardColor,
	SignalsCar_t381616BF85EDCCF005FD247DC7E72DB6EAD4DAB0_CustomAttributesCacheGenerator_turnSignalSound,
	SignalsCar_t381616BF85EDCCF005FD247DC7E72DB6EAD4DAB0_CustomAttributesCacheGenerator_hazardSound,
	TrafficLights_t6127A0CC6322E5899984953DA144B63D24333A73_CustomAttributesCacheGenerator_lightCount,
	TrafficLights_t6127A0CC6322E5899984953DA144B63D24333A73_CustomAttributesCacheGenerator_trafficLights,
	TrafficLights_t6127A0CC6322E5899984953DA144B63D24333A73_CustomAttributesCacheGenerator_isProcessing,
	TrafficLights_t6127A0CC6322E5899984953DA144B63D24333A73_CustomAttributesCacheGenerator_howManySecs,
	TrafficLights_t6127A0CC6322E5899984953DA144B63D24333A73_CustomAttributesCacheGenerator_timer,
	TrafficRulesController_Car_tB16F6F9B85938EDD1604F77EF7DC0A1BDB472A60_CustomAttributesCacheGenerator_speed,
	TrafficRulesController_Car_tB16F6F9B85938EDD1604F77EF7DC0A1BDB472A60_CustomAttributesCacheGenerator_speedLimit,
	TrafficRulesController_Car_tB16F6F9B85938EDD1604F77EF7DC0A1BDB472A60_CustomAttributesCacheGenerator_State,
	TrafficRulesController_Car_tB16F6F9B85938EDD1604F77EF7DC0A1BDB472A60_CustomAttributesCacheGenerator_direction,
	TrafficRulesController_Car_tB16F6F9B85938EDD1604F77EF7DC0A1BDB472A60_CustomAttributesCacheGenerator_WalkerWay,
	TrafficRulesController_Car_tB16F6F9B85938EDD1604F77EF7DC0A1BDB472A60_CustomAttributesCacheGenerator_signalController,
	TrafficRulesController_Car_tB16F6F9B85938EDD1604F77EF7DC0A1BDB472A60_CustomAttributesCacheGenerator_Decider,
	TrafficRulesController_Car_tB16F6F9B85938EDD1604F77EF7DC0A1BDB472A60_CustomAttributesCacheGenerator_RoadStart,
	TrafficRulesController_Car_tB16F6F9B85938EDD1604F77EF7DC0A1BDB472A60_CustomAttributesCacheGenerator_Directions,
	TrafficRulesController_Car_tB16F6F9B85938EDD1604F77EF7DC0A1BDB472A60_CustomAttributesCacheGenerator_Rules,
	TrafficRulesController_Car_tB16F6F9B85938EDD1604F77EF7DC0A1BDB472A60_CustomAttributesCacheGenerator_Controller,
	TrafficRulesController_Car_tB16F6F9B85938EDD1604F77EF7DC0A1BDB472A60_CustomAttributesCacheGenerator_shouldBe,
	WalkerWay_tE1DBB9C531DC74D2654F2AB74C9D6E1304A418DD_CustomAttributesCacheGenerator_stopColor,
	WalkerWay_tE1DBB9C531DC74D2654F2AB74C9D6E1304A418DD_CustomAttributesCacheGenerator_passColor,
	WalkerWay_tE1DBB9C531DC74D2654F2AB74C9D6E1304A418DD_CustomAttributesCacheGenerator_timer,
	WalkerWay_tE1DBB9C531DC74D2654F2AB74C9D6E1304A418DD_CustomAttributesCacheGenerator_speed,
	MobileDisableAutoSwitchControls_tFFBB1531EDDED48A8D620DF56060164FE3482D4F_CustomAttributesCacheGenerator_playerInput,
	UIVirtualButton_tA1A6B564C5E71E1B61B240E28958DEB8F79BABBE_CustomAttributesCacheGenerator_buttonStateOutputEvent,
	UIVirtualJoystick_tEF0053DAA02EDBA00713D7B29C34394EA7574C2D_CustomAttributesCacheGenerator_containerRect,
	UIVirtualJoystick_tEF0053DAA02EDBA00713D7B29C34394EA7574C2D_CustomAttributesCacheGenerator_joystickRange,
	UIVirtualJoystick_tEF0053DAA02EDBA00713D7B29C34394EA7574C2D_CustomAttributesCacheGenerator_joystickOutputEvent,
	UIVirtualTouchZone_t2EB72E6BED3964232BD92D6723DEA7BA013BED23_CustomAttributesCacheGenerator_containerRect,
	UIVirtualTouchZone_t2EB72E6BED3964232BD92D6723DEA7BA013BED23_CustomAttributesCacheGenerator_clampToMagnitude,
	UIVirtualTouchZone_t2EB72E6BED3964232BD92D6723DEA7BA013BED23_CustomAttributesCacheGenerator_touchZoneOutputEvent,
	BasicRigidBodyPush_t9E4C23843BA4437AF06C87D968F0A0BE0B41C714_CustomAttributesCacheGenerator_strength,
	StarterAssetsInputs_tE7DDADB7E0E59B93F86B819E96C2523BC28F16DC_CustomAttributesCacheGenerator_move,
	StarterAssetsInputs_tE7DDADB7E0E59B93F86B819E96C2523BC28F16DC_CustomAttributesCacheGenerator_analogMovement,
	StarterAssetsInputs_tE7DDADB7E0E59B93F86B819E96C2523BC28F16DC_CustomAttributesCacheGenerator_cursorLocked,
	UICanvasControllerInput_t18C69AA8A6225F0EDEC9D31926AFCA88FC8F8EA1_CustomAttributesCacheGenerator_starterAssetsInputs,
	ThirdPersonController_tCA99A63A4BE57B6A9950FFA8CFFABE8F82AD0903_CustomAttributesCacheGenerator_MoveSpeed,
	ThirdPersonController_tCA99A63A4BE57B6A9950FFA8CFFABE8F82AD0903_CustomAttributesCacheGenerator_SprintSpeed,
	ThirdPersonController_tCA99A63A4BE57B6A9950FFA8CFFABE8F82AD0903_CustomAttributesCacheGenerator_RotationSmoothTime,
	ThirdPersonController_tCA99A63A4BE57B6A9950FFA8CFFABE8F82AD0903_CustomAttributesCacheGenerator_SpeedChangeRate,
	ThirdPersonController_tCA99A63A4BE57B6A9950FFA8CFFABE8F82AD0903_CustomAttributesCacheGenerator_FootstepAudioVolume,
	ThirdPersonController_tCA99A63A4BE57B6A9950FFA8CFFABE8F82AD0903_CustomAttributesCacheGenerator_JumpHeight,
	ThirdPersonController_tCA99A63A4BE57B6A9950FFA8CFFABE8F82AD0903_CustomAttributesCacheGenerator_Gravity,
	ThirdPersonController_tCA99A63A4BE57B6A9950FFA8CFFABE8F82AD0903_CustomAttributesCacheGenerator_JumpTimeout,
	ThirdPersonController_tCA99A63A4BE57B6A9950FFA8CFFABE8F82AD0903_CustomAttributesCacheGenerator_FallTimeout,
	ThirdPersonController_tCA99A63A4BE57B6A9950FFA8CFFABE8F82AD0903_CustomAttributesCacheGenerator_Grounded,
	ThirdPersonController_tCA99A63A4BE57B6A9950FFA8CFFABE8F82AD0903_CustomAttributesCacheGenerator_GroundedOffset,
	ThirdPersonController_tCA99A63A4BE57B6A9950FFA8CFFABE8F82AD0903_CustomAttributesCacheGenerator_GroundedRadius,
	ThirdPersonController_tCA99A63A4BE57B6A9950FFA8CFFABE8F82AD0903_CustomAttributesCacheGenerator_GroundLayers,
	ThirdPersonController_tCA99A63A4BE57B6A9950FFA8CFFABE8F82AD0903_CustomAttributesCacheGenerator_CinemachineCameraTarget,
	ThirdPersonController_tCA99A63A4BE57B6A9950FFA8CFFABE8F82AD0903_CustomAttributesCacheGenerator_TopClamp,
	ThirdPersonController_tCA99A63A4BE57B6A9950FFA8CFFABE8F82AD0903_CustomAttributesCacheGenerator_BottomClamp,
	ThirdPersonController_tCA99A63A4BE57B6A9950FFA8CFFABE8F82AD0903_CustomAttributesCacheGenerator_CameraAngleOverride,
	ThirdPersonController_tCA99A63A4BE57B6A9950FFA8CFFABE8F82AD0903_CustomAttributesCacheGenerator_LockCameraPosition,
	PlayerInputActions_t375171A1A870F2F98685801D864FC76FD516F22C_CustomAttributesCacheGenerator_PlayerInputActions_get_asset_m3998FFE48C0D23FA068FF8995F9958EF79384CEF,
	VRControls_tEAE1C9E737B679D74AD0DA693794E297010CFA77_CustomAttributesCacheGenerator_VRControls_SimulateKeyPress_m42F3AD2DAACED561F00377A6F9732B1A3AE1DC51,
	U3CSimulateKeyPressU3Ed__8_tAB6F8BC46272BF48D20F34C4D00E6A15FCEF04C6_CustomAttributesCacheGenerator_U3CSimulateKeyPressU3Ed__8__ctor_mB333C0FA66B2395C19496CC79C6100503A0633BF,
	U3CSimulateKeyPressU3Ed__8_tAB6F8BC46272BF48D20F34C4D00E6A15FCEF04C6_CustomAttributesCacheGenerator_U3CSimulateKeyPressU3Ed__8_System_IDisposable_Dispose_m6B6AA45F2BFEA8845A8F47384DAECB7A55DC4FC9,
	U3CSimulateKeyPressU3Ed__8_tAB6F8BC46272BF48D20F34C4D00E6A15FCEF04C6_CustomAttributesCacheGenerator_U3CSimulateKeyPressU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFB7AE2BA8C02D3857C2599A9D8B6389319488E07,
	U3CSimulateKeyPressU3Ed__8_tAB6F8BC46272BF48D20F34C4D00E6A15FCEF04C6_CustomAttributesCacheGenerator_U3CSimulateKeyPressU3Ed__8_System_Collections_IEnumerator_Reset_m5A0CAFC39F155EEFA816F434F43DEBC933C7FD10,
	U3CSimulateKeyPressU3Ed__8_tAB6F8BC46272BF48D20F34C4D00E6A15FCEF04C6_CustomAttributesCacheGenerator_U3CSimulateKeyPressU3Ed__8_System_Collections_IEnumerator_get_Current_m8D210BFBFCCE65F373299DC960D8D4FE87D5C0EE,
	AssemblyU2DCSharp_CustomAttributesCacheGenerator,
};
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_wrapNonExceptionThrows_0(L_0);
		return;
	}
}
