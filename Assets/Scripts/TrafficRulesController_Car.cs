using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrafficRulesController_Car : MonoBehaviour
{

    [SerializeField] private float speed;
    [SerializeField] private float speedLimit;
    [SerializeField] private TrafficRules.States State;
    [SerializeField] private RoadDecider.Directions direction;
    [SerializeField] private WalkerWay WalkerWay;
    [SerializeField] private SignalsCar signalController;

    [SerializeField] private GameObject Decider;
    [SerializeField] private GameObject RoadStart;
    [SerializeField] private GameObject[] Directions;
    [SerializeField] private GameObject[] Rules;
    [SerializeField] private GameEndController Controller;

    [SerializeField] private bool shouldBe;

    void Start()
    {
        
    }


    void Update()
    {
        speed = this.GetComponent<PrometeoCarController>().carSpeed;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "TrafficWarner") 
        {
            foreach (var rule in Rules) 
            {
                rule.SetActive(false);
            }
            if (other.GetComponent<TrafficRules>().State == TrafficRules.States.Speed50)
            {
                speedLimit = 50;
                State = TrafficRules.States.Speed50;
                Rules[1].SetActive(true);
            }
            if (other.GetComponent<TrafficRules>().State == TrafficRules.States.Speed25)
            {
                speedLimit = 25;
                State = TrafficRules.States.Speed25;
                Rules[2].SetActive(true);
            }
            if (other.GetComponent<TrafficRules>().State == TrafficRules.States.WalkWay)
            {
                speedLimit = 81;
                State = TrafficRules.States.WalkWay;
                Rules[3].SetActive(true);
                WalkerWay = other.gameObject.transform.parent.gameObject.GetComponentInChildren<WalkerWay>();
                WalkerWay.didCarCome = true;
            }
            other.gameObject.SetActive(false);
        }

        //Traffic Controller
        if (other.gameObject.tag == "TrafficController") 
        {
            if (speed > speedLimit)
            {
                Controller.Fail("Cok Hizli Gidiyorsun. Yavasla Biraz!");
            }
            if (WalkerWay != null && !WalkerWay.canPass)
            {
                Controller.Fail("Yayalardan birisine �arpt�n ve kurallara uymad���n i�in hapse at�ld�n. Yaya ge�idinde yayalara yol vermelisin!");
            }
            else 
            {
                WalkerWay = null;
            }
            foreach (var rule in Rules)
            {
                rule.SetActive(false);
            }
            Rules[0].SetActive(true);
        }
        // Deciders
        if (other.gameObject.tag == "DirDecider")
        {
            RoadDecider decider = other.gameObject.GetComponent<RoadDecider>();
            if (RoadStart == null && decider.SelectNext() == null) 
            {
                Controller.Fail("Yanlis Yonde Gidiyordun ! Lutfen Yoldan Sapma");
            }
            Decider = other.gameObject;
            RoadStart = decider.SelectNext();
            shouldBe = true;
            foreach (var dir in Directions)
            {
                dir.SetActive(false);
            }
            if (decider.Direction == RoadDecider.Directions.Left)
            {
                Directions[0].SetActive(true);
            }
            else if (decider.Direction == RoadDecider.Directions.Right)
            {
                Directions[1].SetActive(true);
            }
            else if (decider.Direction == RoadDecider.Directions.Straight)
            {
                Directions[2].SetActive(true);
            }
        }
        //RoadStarts and Signals
        if (other.gameObject.tag == "DirStart")
        {
            if (RoadStart == null && shouldBe|| other.gameObject != RoadStart && shouldBe)
            {
                Controller.Fail("Yanlis Yonde Gidiyordun ! Lutfen Yoldan Sapma");
                return;
            }
            if (Decider != null && Decider.GetComponent<RoadDecider>().Direction != signalController.direction) 
            {
                Controller.Fail("Sinyal Vermedin Veya Yanlis Verdin. Lutfen Sinyallere Dikkat Et");
                return;
            }
            signalController.DisableAll();
            Decider = null;
            foreach (var dir in Directions)
            {
                dir.SetActive(false);
            }
            Directions[2].SetActive(true);
            shouldBe = false;
            RoadStart = null;
        }

        //Obstacles and walls
        if (other.gameObject.tag == "Neighbour")
        {
            if (other.gameObject.GetComponent<CarAI>() == null || !other.gameObject.GetComponent<CarAI>().isStopping)
            {
                Controller.Fail("Yoldan Ciktin Veya Kaza Yapt�n. Daha Dikkatli Ol!");
            }
        }    
        
        //Traffic Lights
        if (other.gameObject.tag == "TrafficLight_Car")
        {
            if (!other.gameObject.GetComponent<TrafficLight>().isGreenToCars)
            {
                Controller.Fail("Kirmizida gectin. Yesil isik yanana kadar beklemelisin!");
            }
        }

        if (other.gameObject.name == "Finish") 
        {
            Controller.Won("Gorevleri Basariyla Tamamladin");
        }
    }
}
