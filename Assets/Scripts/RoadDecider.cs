using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoadDecider : MonoBehaviour
{
    public enum ControlTypes
    {
        Decider,
        RoadStart
    }
    public ControlTypes ControlType;

    public enum Directions
    {
        Left,
        Right,
        Straight
    }
    public Directions Direction;
    [SerializeField] private GameObject GoTo;

    public GameObject SelectNext()
    {
        if (ControlType == ControlTypes.Decider)
        {
            return GoTo;
        }
        return null;
    }

    public Directions GetDirection() 
    {
        if (Direction == Directions.Left)
        {
            return Directions.Left;
        }
        else if (Direction == Directions.Right)
        {
            return Directions.Right;
        }

        return Directions.Straight;
    }

}
