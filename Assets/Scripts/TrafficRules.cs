using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrafficRules : MonoBehaviour
{
    public enum States 
    {
        None,
        Speed50,
        Speed25,
        WalkWay,
    }

    public States State;
    public float Value;

    void Start()
    {
        if (State == States.Speed50)
        {
            Value = 50;
        }
        else if (State == States.Speed25)
        {
            Value = 25;
        }
        else if (State == States.WalkWay)
        {
            Value = 0;
        }
    }

    void Update()
    {
        
    }
}
