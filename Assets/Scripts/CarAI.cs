using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarAI : MonoBehaviour
{
    public Transform character;
    public Transform carFront;
    public bool isStopping;

    [SerializeField] private float distanceWithLight;
    [SerializeField] private float distanceWithCharacter;
    [SerializeField] private float distanceWithCLosestCar;
    [SerializeField] private float maxDistanceL;
    [SerializeField] private float maxDistanceC;
    [SerializeField] private float maxDistanceCars;
    [SerializeField] private float speed;
    [SerializeField] private bool isCloseToCars;

    private TrafficLight trafficLight;


    void Start()
    {
        
    }

    void Update()
    {
        if (trafficLight != null)
        {
            distanceWithLight = Vector3.Distance(trafficLight.gameObject.transform.position, this.transform.position);
            distanceWithCharacter = Vector3.Distance(character.position, this.transform.position);
            if (distanceWithCharacter > maxDistanceC && trafficLight.isGreenToCars && !isCloseToCars)
            {
                transform.Translate(speed, 0, 0);
            }
            else if (distanceWithLight > maxDistanceL && !trafficLight.isGreenToCars && !isCloseToCars)
            {
                transform.Translate(speed, 0, 0);
            }
        }
        else if(!isCloseToCars)
        {
            transform.Translate(speed, 0, 0);
            isStopping = false;
        }

        if (carFront != null) 
        {
            float distance = Vector3.Distance(this.transform.position, carFront.position);
            if (distance > 20f) 
            {
                isCloseToCars = false;
            }
        }

        var heading = character.position - this.transform.position;
        float dot = Vector3.Dot(heading, this.transform.forward);
        if (distanceWithCharacter < maxDistanceC) 
        {
            if (dot < 0.1f && dot > -0.1f) 
            {
                isCloseToCars = true;
                carFront = character.transform;
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "TrafficSelector")
        {
            if (other.GetComponent<AISelector>().isLast) 
            {
                Destroy(this.gameObject);
                return;
            }
            trafficLight = other.GetComponent<AISelector>().trafficLight;
        }
        if (other.gameObject.name == "CarStopper")
        {
            isStopping = true;
            if (other.transform.parent != this.transform)
            {
                isCloseToCars = true;
                carFront = other.transform;
            }
        }        
    }
}
