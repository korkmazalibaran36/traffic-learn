using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameEndController : MonoBehaviour
{
    [SerializeField] private GameObject wonPanel;
    [SerializeField] private Text wonPanelText;
    [SerializeField] private AudioSource wonPanelSound;

    [SerializeField] private GameObject failPanel;
    [SerializeField] private Text failPanelText;
    [SerializeField] private AudioSource failPanelSound;

    public void Won(string message)
    {
        Time.timeScale = 0;
        wonPanel.SetActive(true);
        wonPanelText.text = message;
        wonPanelSound.Play();
    }

    public void Fail(string message)
    {
        Time.timeScale = 0;
        failPanel.SetActive(true);
        failPanelText.text = message;
        failPanelSound.Play();
    }
}
