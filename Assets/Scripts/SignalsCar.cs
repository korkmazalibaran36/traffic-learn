using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SignalsCar : MonoBehaviour
{
    private const int timeDefault = 50;
    [SerializeField] private bool hazardActive;
    [SerializeField] private bool rightActive;
    [SerializeField] private bool leftActive;
    [SerializeField] private int timer;

    [SerializeField] private GameObject rightLight;
    [SerializeField] private GameObject leftLight;
    [SerializeField] private GameObject hazardLight;

    [SerializeField] private GameObject vrCam;
    [SerializeField] private GameObject vrComponent;
    [SerializeField] private GameObject normalCam;
    [SerializeField] private bool vrEnabled;

    [SerializeField] private GameObject LeftPointLight;
    [SerializeField] private GameObject RightPointLight;

    [SerializeField] private Color signalColor;
    [SerializeField] private Color hazardColor;

    [SerializeField] private AudioSource turnSignalSound;
    [SerializeField] private AudioSource hazardSound;

    public RoadDecider.Directions direction;

    void Start()
    {
        
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            SetLeft();
            Debug.Log("Left");
        }
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            SetRight();
            Debug.Log("Right");
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            SetHazard();
            Debug.Log("Hazard");
        }
    }
    void FixedUpdate()
    {


        if (leftActive)
        {
            direction = RoadDecider.Directions.Left;
            if (timer == timeDefault)
            {
                leftLight.GetComponent<Image>().color = signalColor;
                LeftPointLight.gameObject.SetActive(true);
            }
            else if (timer == timeDefault / 2)
            {
                LeftPointLight.gameObject.SetActive(false);
                leftLight.GetComponent<Image>().color = Color.white;
            }
            timer--;

            if (timer == 0)
            {
                timer = timeDefault;
            }
        }
        else if (rightActive)
        {
            direction = RoadDecider.Directions.Right;
            if (timer == timeDefault)
            {
                rightLight.GetComponent<Image>().color = signalColor;
                RightPointLight.gameObject.SetActive(true);
            }
            else if (timer == timeDefault / 2)
            {
                rightLight.GetComponent<Image>().color = Color.white;
                RightPointLight.gameObject.SetActive(false);
            }
            timer--;
            if (timer == 0)
            {
                timer = timeDefault;
            }
        }
        else if (hazardActive)
        {
            direction = RoadDecider.Directions.Straight;
            if (timer == timeDefault)
            {
                rightLight.GetComponent<Image>().color = signalColor;
                leftLight.GetComponent<Image>().color = signalColor;
                hazardLight.GetComponent<Image>().color = hazardColor;
                RightPointLight.gameObject.SetActive(true);
                LeftPointLight.gameObject.SetActive(true);
            }
            else if (timer == timeDefault / 2)
            {
                rightLight.GetComponent<Image>().color = Color.white;
                leftLight.GetComponent<Image>().color = Color.white;
                hazardLight.GetComponent<Image>().color = Color.white;
                RightPointLight.gameObject.SetActive(false);
                LeftPointLight.gameObject.SetActive(false);
            }
            timer--;
            if (timer == 0)
            {
                timer = timeDefault;
            }
        }
        else 
        {
            direction = RoadDecider.Directions.Straight;
        }

    }

    public void SetLeft()
    {
        leftActive = !leftActive;
        if (leftActive)
        {
            rightActive = false;
            hazardActive = false;
            rightLight.GetComponent<Image>().color = Color.white;
            hazardLight.GetComponent<Image>().color = Color.white;
            turnSignalSound.Play();
            hazardSound.Stop();
        }
        else 
        {
            leftLight.GetComponent<Image>().color = Color.white;
            turnSignalSound.Stop();
        }
        timer = timeDefault;
    }
    public void SetRight()
    {
        rightActive = !rightActive;
        if (rightActive)
        {
            leftActive = false;
            hazardActive = false;
            leftLight.GetComponent<Image>().color = Color.white;
            hazardLight.GetComponent<Image>().color = Color.white;
            turnSignalSound.Play();
            hazardSound.Stop();
        }
        else 
        {
            rightLight.GetComponent<Image>().color = Color.white;
            turnSignalSound.Stop();
        }
        timer = timeDefault;
    }
    public void SetHazard()
    {
        hazardActive = !hazardActive;
        if (hazardActive)
        {
            leftActive = false;
            rightActive = false;
            turnSignalSound.Stop();
            hazardSound.Play();
        }
        else 
        {
            rightLight.GetComponent<Image>().color = Color.white;
            leftLight.GetComponent<Image>().color = Color.white;
            hazardLight.GetComponent<Image>().color = Color.white;
            hazardSound.Stop();
        }
        timer = timeDefault;
    }
    public void DisableAll() 
    {
        rightActive = false;
        leftActive = false;
        rightLight.GetComponent<Image>().color = Color.white;
        leftLight.GetComponent<Image>().color = Color.white;
        hazardLight.GetComponent<Image>().color = Color.white;
        turnSignalSound.Stop();
        hazardSound.Stop();
    }

    public void ChangeCamera()
    {
        vrEnabled = !vrEnabled;
        vrCam.gameObject.SetActive(vrEnabled);
        vrComponent.gameObject.SetActive(vrEnabled);
        normalCam.gameObject.SetActive(!vrEnabled);
    }
}
