using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WalkerWay : MonoBehaviour
{
    public GameObject[] walkers;
    public bool canPass;
    public bool didCarCome;

    [SerializeField] private Color stopColor;
    [SerializeField] private Color passColor;
    [SerializeField] private int timer;
    [SerializeField] private float speed;
    void Start()
    {
        this.GetComponent<MeshRenderer>().material.color = stopColor;
    }

    // Update is called once per frame
    void Update()
    {
        if (didCarCome)
        {
            if (timer > 0)
            {
                if (timer < 120)
                {
                    walkers[0].SetActive(true);
                    walkers[0].transform.Translate(0, 0, speed);
                }
                if (timer == 30)
                {
                    this.GetComponent<MeshRenderer>().material.color = passColor;
                    canPass = true;
                }

                timer--;
            }
            if (timer == 0)
            {
                walkers[2].SetActive(false);
            }
        }
    }
}
