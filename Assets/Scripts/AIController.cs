using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIController : MonoBehaviour
{
    [SerializeField] private Vector3 vector1;
    [SerializeField] private Vector3 vector2;
    [SerializeField] private Quaternion rot;
    [SerializeField] private Transform carTrans;
    [SerializeField] private GameObject[] cars;
    [SerializeField] private bool isFarEnough;
    [SerializeField] private bool onlyFirstVector;
    [SerializeField] private float distance;
    [SerializeField] private float maxDistance;
    [SerializeField] private int timer;


    void Update()
    {
        distance = Vector3.Distance(this.transform.position, carTrans.position);
        if (distance > maxDistance)
        {
            isFarEnough = true;
        }
        else 
        {
            isFarEnough = false;
        }
        if (isFarEnough) 
        {
            if (timer == 0)
            {
                if (!onlyFirstVector)
                {
                    int random = Random.Range(0, cars.Length);
                    int random2 = Random.Range(0, 2);
                    Vector3 randomVector = Vector3.zero;

                    if (random2 == 0) randomVector = vector1;
                    else randomVector = vector2;

                    GameObject car = GameObject.Instantiate(cars[random], randomVector, rot);
                    car.GetComponent<CarAI>().character = carTrans;
                    timer = 360;
                }
                else 
                {
                    int random = Random.Range(0, cars.Length);
                    GameObject car = GameObject.Instantiate(cars[random], vector1, rot);
                    car.GetComponent<CarAI>().character = carTrans;
                    timer = 360;
                }
            }
            timer--;
        }
    }
}
