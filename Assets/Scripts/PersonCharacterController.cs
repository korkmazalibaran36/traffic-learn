using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersonCharacterController : MonoBehaviour
{

    public GameObject walkerStart;
    public GameObject walkerMiddle;
    public GameObject walkerFinish;
    public GameObject overpassStart;
    public GameObject overpassUp;
    public GameObject overpassFinish;
    public GameObject[] rules;
    public GameObject[] directions;

    public TrafficLight firstLight;
    public TrafficLight secondLight;

    public Material red;
    public Material green;

    public GameEndController Controller;

    void Start()
    {
        
    }

    void Update()
    {
        if (firstLight.isGreenToWalkers)
        {
            walkerStart.GetComponent<Renderer>().material = green;
        }
        else 
        {
            walkerStart.GetComponent<Renderer>().material = red;
        }
        if (secondLight.isGreenToWalkers)
        {
            walkerMiddle.GetComponent<Renderer>().material = green;
        }
        else 
        {
            walkerMiddle.GetComponent<Renderer>().material = red;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == walkerStart) 
        {
            if (!firstLight.isGreenToWalkers) 
            {
                Controller.Fail("Yayalara Kirmizi Yanarken Beklemelisin. Yoksa Araba Carpabilir!");
            }
        }
        if (other.gameObject == walkerMiddle) 
        {
            if (!secondLight.isGreenToWalkers) 
            {
                Controller.Fail("Yayalara Kirmizi Yanarken Beklemelisin. Yoksa Araba Carpabilir!");
            }
        }
        if (other.gameObject.tag == "DirDecider") 
        {
            directions[1].SetActive(true);
            directions[0].SetActive(false);
        }
        if (other.gameObject.tag == "Neighbour")
        {
            CarAI ai = other.GetComponent<CarAI>();
            if (ai != null)
            {
                Controller.Fail("Yolun Ortasinda Durdugun Icin Araba Carpti. Daha Dikkatli Ol!");
            }
        }
        if (other.gameObject.name == "RoadCollider")
        {

            Controller.Fail("Yolun Yayalar Icin Olan Kismi Haric Yola Cikmamalisin");

        }
        if (other.gameObject == walkerFinish) 
        {
            rules[0].SetActive(false);
            rules[1].SetActive(true);
            directions[1].SetActive(false);
            directions[0].SetActive(true);
        }
        if (other.gameObject == overpassStart)
        {
            rules[1].SetActive(false);
            rules[2].SetActive(true);
            this.transform.position = overpassUp.transform.position;
        }
        if (other.gameObject == overpassFinish)
        {
            Controller.Won("Gorevleri Tamamladin Ve Oyunu Bitirdin");
        }
    }
}
