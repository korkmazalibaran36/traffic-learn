using System.Collections;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.OnScreen;
using UnityEngine.UI;
using UnityEngine.XR;
using UnityEngine.XR.Interaction.Toolkit;

public class VRControls : MonoBehaviour
{
    public PlayerInput playerInput;
    public PrometeoCarController carController;
    public SignalsCar signal;
    Button a_button;


    private void Start()
    {
        playerInput = GetComponent<PlayerInput>();
        PlayerInputActions inputActions = new PlayerInputActions();
        inputActions.Player.Enable();
        //inputActions.Player.GoForward.performed += goForward;
        //inputActions.Player.GoBack.performed += goBack;
        //inputActions.Player.GoLeft.performed += turnLeft;
        //inputActions.Player.GoRight.performed += turnRight;
        //inputActions.Player.RightSignal.performed += rightSignal;
        //inputActions.Player.LeftSignal.performed += leftSignal;

    }
    void Update()
    {

    }
    void goForward(InputAction.CallbackContext context)
    {
    }
    public void go()
    {
        StartCoroutine(SimulateKeyPress());
    }

    private IEnumerator SimulateKeyPress()
    {
        // Set the 'W' key state to 'down'
        Input.GetKeyDown(KeyCode.W);

        // Wait for a short delay
        yield return new WaitForSeconds(0.1f);

        // Set the 'W' key state back to 'up'
        Input.GetKeyUp(KeyCode.W);
    }
    void goBack(InputAction.CallbackContext context)
    {
        //carController.brake();
    }
    void turnLeft(InputAction.CallbackContext context)
    {
        //carController.turnLeft();
    }
    void turnRight(InputAction.CallbackContext context)
    {
        //carController.turnRight();
    }
    void rightSignal(InputAction.CallbackContext context)
    {
        signal.SetRight();
    }
    void leftSignal(InputAction.CallbackContext context)
    {
        signal.SetLeft();
    }

}