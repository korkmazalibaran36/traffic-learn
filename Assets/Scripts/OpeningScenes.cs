using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpeningScenes : MonoBehaviour
{
    public void Open(int level)
    {
        Time.timeScale = 1;
        Application.LoadLevel(level);
    }
}
