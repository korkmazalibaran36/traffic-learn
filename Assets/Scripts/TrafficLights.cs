using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrafficLights : MonoBehaviour
{
    public enum LightCount 
    {
        None,
        Three,
        Four
    }
    [SerializeField] LightCount lightCount;
    [SerializeField] private TrafficLight[] trafficLights;
    [SerializeField] private bool isProcessing;
    [SerializeField] private int howManySecs;
    [SerializeField] private int timer;

    void Start()
    {
        timer = howManySecs;
    }

    // Update is called once per frame
    void Update()
    {
        if (lightCount == LightCount.Three)
        {
            if (timer == 3600)
            {
                trafficLights[1].AllowWalkers();
                trafficLights[2].AllowWalkers();
                trafficLights[0].AllowCars();
            }
            else if (timer == 2400)
            {
                trafficLights[0].AllowWalkers();
                trafficLights[2].AllowWalkers();
                trafficLights[1].AllowCars();
            }
            else if (timer == 1200)
            {
                trafficLights[0].AllowWalkers();
                trafficLights[1].AllowWalkers();
                trafficLights[2].AllowCars();
            }
            else if (timer == 0)
            {
                timer = 3601;
            }
        }
        else if (lightCount == LightCount.Four)
        {
            if (timer == 4800)
            {
                trafficLights[1].AllowWalkers();
                trafficLights[2].AllowWalkers();
                trafficLights[3].AllowWalkers();
                trafficLights[0].AllowCars();
            }
            else if (timer == 3600)
            {
                trafficLights[0].AllowWalkers();
                trafficLights[2].AllowWalkers();
                trafficLights[3].AllowWalkers();
                trafficLights[1].AllowCars();
            }
            else if (timer == 2400)
            {
                trafficLights[0].AllowWalkers();
                trafficLights[1].AllowWalkers();
                trafficLights[3].AllowWalkers();
                trafficLights[2].AllowCars();
            }
            else if (timer == 1200)
            {
                trafficLights[0].AllowWalkers();
                trafficLights[2].AllowWalkers();
                trafficLights[1].AllowWalkers();
                trafficLights[3].AllowCars();
            }
            else if (timer == 0)
            {
                timer = 4801;
            }
        }
        else
        {

        }

        timer--;
    }
}
