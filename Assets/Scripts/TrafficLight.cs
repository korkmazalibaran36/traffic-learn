using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

public class TrafficLight : MonoBehaviour
{

    public bool isGreenToCars;
    public bool isGreenToWalkers;
    public bool lastWasThis;
    public GameObject[] carLights;
    public GameObject[] walkLights;
    int timer;
    public TrafficLight[] lightsThatClosing;
    public TrafficLight[] lightsThatOpening;

    void Start()
    {
        timer = 180;
        lastWasThis = true;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        timer--;
        if (isGreenToCars)
        {
            setOtherLights();
            if (timer == 179)
            {
                Walker_GreenToRed();
            }
            if (timer == 90)
            {
                Car_RedToYellow();
            }
            if (timer == 0)
            {
                Car_YellowToGreen();
            }
        }
        else 
        {
            if (lastWasThis)
            {
                if (timer == 179)
                {
                    Car_GreenToYellow();
                }
                if (timer == 90)
                {
                    Car_YellowToRed();
                    lastWasThis = false;
                }
            }
            if (timer == 90)
            {
                Walker_RedToGreen();
            }
        }
    }


    public void AllowCars()
    {
        isGreenToCars = true;
        timer = 180;
        lastWasThis = true;
    }

    public void AllowWalkers()
    {
        isGreenToCars = false;
        timer = 180;
    }

    void setOtherLights() 
    {
        foreach (var light in lightsThatClosing)
        {
            if (light.gameObject.tag == "WalkerLight") 
            {
                light.GetComponent<TrafficLight>().isGreenToWalkers = false;
            }
            light.Walker_GreenToRed();
        }
        foreach (var light in lightsThatOpening)
        {
            if (light.gameObject.tag == "WalkerLight")
            {
                light.GetComponent<TrafficLight>().isGreenToWalkers = true;
            }
            light.Walker_RedToGreen();
        }
    }

    #region Methods
    public void Car_RedToYellow()
    {
        carLights[0].SetActive(false);
        carLights[1].SetActive(true);
    }
    public void Car_YellowToGreen()
    {
        carLights[1].SetActive(false);
        carLights[2].SetActive(true);
    }
    public void Car_GreenToYellow()
    {
        carLights[2].SetActive(false);
        carLights[1].SetActive(true);
    }
    public void Car_YellowToRed()
    {
        carLights[1].SetActive(false);
        carLights[0].SetActive(true);
    }
    public void Walker_GreenToRed()
    {
        walkLights[1].SetActive(false);
        walkLights[0].SetActive(true);
    }
    public void Walker_RedToGreen()
    {
        walkLights[0].SetActive(false);
        walkLights[1].SetActive(true);
    }
    #endregion
}
