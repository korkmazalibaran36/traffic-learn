﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <stdint.h>
#include <limits>



// System.Collections.Generic.Dictionary`2<System.Type,UnityEngine.ISubsystem>
struct Dictionary_2_t4F3B5B526335E16355EDBC766052AEAB07B1777B;
// System.Collections.Generic.List`1<UnityEngine.XR.ARSubsystems.XRAnchorSubsystemDescriptor>
struct List_1_tDED98C236097B36F9015B396398179A6F8A62E50;
// System.Collections.Generic.List`1<UnityEngine.XR.XRDisplaySubsystemDescriptor>
struct List_1_t1BC024192EE6F54EADD3239A60DB2A4A0B4B5048;
// System.Collections.Generic.List`1<UnityEngine.XR.InteractionSubsystems.XRGestureSubsystemDescriptor>
struct List_1_tFDF63FE53408489D6A2F16814E1594D8AF3B4942;
// System.Collections.Generic.List`1<UnityEngine.XR.XRInputSubsystemDescriptor>
struct List_1_t885BD663DFFEB6C32E74934BE1CE00D566657BA0;
// System.Collections.Generic.List`1<UnityEngine.XR.XRMeshSubsystemDescriptor>
struct List_1_tA4CB3CC063D44B52D336C5DDA258EF7CE9B98A94;
// System.Collections.Generic.List`1<UnityEngine.XR.ARSubsystems.XRSessionSubsystemDescriptor>
struct List_1_tDA25459A722F5B00FFE84463D9952660BC3F6673;
// UnityEngine.AudioClip[]
struct AudioClipU5BU5D_t9BA3E7C4B62164BA2E054F3A82CD6CE6EB273CBE;
// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642;
// UnityEngine.InputSystem.InputControl[]
struct InputControlU5BU5D_tB874FECA56E2B08D3280F4174B988EA155E99680;
// UnityEngine.InputSystem.Utilities.InternedString[]
struct InternedStringU5BU5D_t1EAE8433C8EF7C76894673801EE0EE9F6833E9A4;
// System.Object[]
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE;
// TrafficLight[]
struct TrafficLightU5BU5D_tD9EAAB468C6B67BB3FB40A4EB89B90754BC540A3;
// System.UInt32[]
struct UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF;
// Readme/Section[]
struct SectionU5BU5D_t013C5C59593F9D61D69A33E91B8C1450419ED659;
// UnityEngine.XR.WindowsMR.WindowsMRExtensions/WMRPlane[]
struct WMRPlaneU5BU5D_tC28080B40113E442BC0C9D74D326973B2C2E136F;
// UnityEngine.Animator
struct Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149;
// UnityEngine.AudioClip
struct AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE;
// UnityEngine.AudioSource
struct AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B;
// UnityEngine.InputSystem.Controls.AxisControl
struct AxisControl_tA997FF52442F7B08C7E13B7028393B4117248680;
// UnityEngine.UI.Button
struct Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D;
// UnityEngine.InputSystem.Controls.ButtonControl
struct ButtonControl_t3918D56D674854AF9DFEB8CCF130919AAD9FCA68;
// UnityEngine.CharacterController
struct CharacterController_tCCF68621C784CCB3391E0C66FE134F6F93DD6C2E;
// GameEndController
struct GameEndController_t22B3012DEB58F882ECF6BE83EB24B4997F177C12;
// UnityEngine.GameObject
struct GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319;
// System.Collections.Hashtable
struct Hashtable_t7565AB92A12227AD5BADD6911F10D87EE52509AC;
// UnityEngine.InputSystem.InputAction
struct InputAction_t9AC3F79A5911EDC5CEB0BCD81640FDACA8DB0B0B;
// UnityEngine.InputSystem.InputActionAsset
struct InputActionAsset_t528ACD0ABA45255A4FD391F0C3D26B1B50B82B18;
// UnityEngine.InputSystem.InputActionMap
struct InputActionMap_t588F4834F020838C4E8AE148F8A42CC84FA75B44;
// UnityEngine.InputSystem.InputDevice
struct InputDevice_t64BD575C54DB522A280D29E47F96E52B79B22D87;
// UnityEngine.InputSystem.Controls.IntegerControl
struct IntegerControl_t68534DE7D89FBFC2952062997BB54680FE7DCCA4;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9;
// UnityEngine.Material
struct Material_t8927C00353A72755313F046D0CE85178AE8218EE;
// UnityEngine.ParticleSystem
struct ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC;
// UnityEngine.InputSystem.PlayerInput
struct PlayerInput_tCB9A1A84F447F3392889C457C9BE413891FE848A;
// PlayerInputActions
struct PlayerInputActions_t375171A1A870F2F98685801D864FC76FD516F22C;
// PrometeoCarController
struct PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E;
// PrometeoTouchInput
struct PrometeoTouchInput_t1C5A9B19512A75AFEAD2E916C6F53F597ADF73CF;
// UnityEngine.InputSystem.Controls.QuaternionControl
struct QuaternionControl_t7BEFE3CFC03CC5CE3F779DDCB5AA2C011F5C7A48;
// UnityEngine.RectTransform
struct RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072;
// UnityEngine.RenderTexture
struct RenderTexture_t5FE7A5B47EF962A0E8D7BEBA05E9FC87D49A1849;
// UnityEngine.Rigidbody
struct Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A;
// SignalsCar
struct SignalsCar_t381616BF85EDCCF005FD247DC7E72DB6EAD4DAB0;
// StarterAssets.StarterAssetsInputs
struct StarterAssetsInputs_tE7DDADB7E0E59B93F86B819E96C2523BC28F16DC;
// System.String
struct String_t;
// UnityEngine.UI.Text
struct Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1;
// UnityEngine.Texture2D
struct Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF;
// TrafficLight
struct TrafficLight_tD55F68E16E18C0A5F32B506715F5BE0989084ABD;
// UnityEngine.TrailRenderer
struct TrailRenderer_t219A9B1F6C4B984AE4BEEC40F90665D122056A01;
// UnityEngine.Transform
struct Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1;
// UnityEngine.InputSystem.Controls.Vector2Control
struct Vector2Control_t271CA458D56BCA875642853132733D774B009A96;
// UnityEngine.InputSystem.Controls.Vector3Control
struct Vector3Control_t915A3DA3C3E367B6F5587853E96EAD5215D328AE;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;
// WalkerWay
struct WalkerWay_tE1DBB9C531DC74D2654F2AB74C9D6E1304A418DD;
// UnityEngine.WheelCollider
struct WheelCollider_t57B08104FE16DFC3BF72826F7A3CCB8477C01779;
// PlayerInputActions/IPlayerActions
struct IPlayerActions_t03264FBE977E75A2BF0D10CDB68F207C3F74D429;
// UIVirtualButton/BoolEvent
struct BoolEvent_tD84A3E3A4245DFD9FA5D608A5ADA77DBDFB6BD56;
// UIVirtualButton/Event
struct Event_tB8168EB885996D80674A82913E2B33B4915A9E23;
// UIVirtualJoystick/Event
struct Event_t5D63277B4BC67CE95DE0316A422A69420D3A368D;
// UIVirtualTouchZone/Event
struct Event_t1E1104A6087ED46DBF720AC59C7A8E63B974639D;



IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_tFDCAFCBB4B3431CFF2DC4D3E03FBFDF54EFF7E9A 
{
public:

public:
};


// <Module>
struct  U3CModuleU3E_tE7281C0E269ACF224AB82F00FE8D46ED8C621032 
{
public:

public:
};


// System.Object


// UnityEngine.XR.WindowsMR.InputLayoutLoader
struct  InputLayoutLoader_t8194F7E176BF352FBBD40A342873F2A6933A65CA  : public RuntimeObject
{
public:

public:
};


// UnityEngine.XR.WindowsMR.Native
struct  Native_t558849F3C2B4DC5969DEE6CCB9AC2945D7F93965  : public RuntimeObject
{
public:

public:
};


// UnityEngine.XR.WindowsMR.NativeTypes
struct  NativeTypes_t2BBAC7306EFF0C4FA66164F944E5A02589C6470E  : public RuntimeObject
{
public:

public:
};


// PlayerInputActions
struct  PlayerInputActions_t375171A1A870F2F98685801D864FC76FD516F22C  : public RuntimeObject
{
public:
	// UnityEngine.InputSystem.InputActionAsset PlayerInputActions::<asset>k__BackingField
	InputActionAsset_t528ACD0ABA45255A4FD391F0C3D26B1B50B82B18 * ___U3CassetU3Ek__BackingField_0;
	// UnityEngine.InputSystem.InputActionMap PlayerInputActions::m_Player
	InputActionMap_t588F4834F020838C4E8AE148F8A42CC84FA75B44 * ___m_Player_1;
	// PlayerInputActions/IPlayerActions PlayerInputActions::m_PlayerActionsCallbackInterface
	RuntimeObject* ___m_PlayerActionsCallbackInterface_2;
	// UnityEngine.InputSystem.InputAction PlayerInputActions::m_Player_Movement
	InputAction_t9AC3F79A5911EDC5CEB0BCD81640FDACA8DB0B0B * ___m_Player_Movement_3;
	// UnityEngine.InputSystem.InputAction PlayerInputActions::m_Player_LeftSignal
	InputAction_t9AC3F79A5911EDC5CEB0BCD81640FDACA8DB0B0B * ___m_Player_LeftSignal_4;
	// UnityEngine.InputSystem.InputAction PlayerInputActions::m_Player_RightSignal
	InputAction_t9AC3F79A5911EDC5CEB0BCD81640FDACA8DB0B0B * ___m_Player_RightSignal_5;
	// UnityEngine.InputSystem.InputAction PlayerInputActions::m_Player_Hazard
	InputAction_t9AC3F79A5911EDC5CEB0BCD81640FDACA8DB0B0B * ___m_Player_Hazard_6;
	// UnityEngine.InputSystem.InputAction PlayerInputActions::m_Player_GoForward
	InputAction_t9AC3F79A5911EDC5CEB0BCD81640FDACA8DB0B0B * ___m_Player_GoForward_7;
	// UnityEngine.InputSystem.InputAction PlayerInputActions::m_Player_GoBack
	InputAction_t9AC3F79A5911EDC5CEB0BCD81640FDACA8DB0B0B * ___m_Player_GoBack_8;
	// UnityEngine.InputSystem.InputAction PlayerInputActions::m_Player_GoLeft
	InputAction_t9AC3F79A5911EDC5CEB0BCD81640FDACA8DB0B0B * ___m_Player_GoLeft_9;
	// UnityEngine.InputSystem.InputAction PlayerInputActions::m_Player_GoRight
	InputAction_t9AC3F79A5911EDC5CEB0BCD81640FDACA8DB0B0B * ___m_Player_GoRight_10;
	// System.Int32 PlayerInputActions::m_KeyboardSchemeIndex
	int32_t ___m_KeyboardSchemeIndex_11;
	// System.Int32 PlayerInputActions::m_VRRemoteSchemeIndex
	int32_t ___m_VRRemoteSchemeIndex_12;

public:
	inline static int32_t get_offset_of_U3CassetU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(PlayerInputActions_t375171A1A870F2F98685801D864FC76FD516F22C, ___U3CassetU3Ek__BackingField_0)); }
	inline InputActionAsset_t528ACD0ABA45255A4FD391F0C3D26B1B50B82B18 * get_U3CassetU3Ek__BackingField_0() const { return ___U3CassetU3Ek__BackingField_0; }
	inline InputActionAsset_t528ACD0ABA45255A4FD391F0C3D26B1B50B82B18 ** get_address_of_U3CassetU3Ek__BackingField_0() { return &___U3CassetU3Ek__BackingField_0; }
	inline void set_U3CassetU3Ek__BackingField_0(InputActionAsset_t528ACD0ABA45255A4FD391F0C3D26B1B50B82B18 * value)
	{
		___U3CassetU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CassetU3Ek__BackingField_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Player_1() { return static_cast<int32_t>(offsetof(PlayerInputActions_t375171A1A870F2F98685801D864FC76FD516F22C, ___m_Player_1)); }
	inline InputActionMap_t588F4834F020838C4E8AE148F8A42CC84FA75B44 * get_m_Player_1() const { return ___m_Player_1; }
	inline InputActionMap_t588F4834F020838C4E8AE148F8A42CC84FA75B44 ** get_address_of_m_Player_1() { return &___m_Player_1; }
	inline void set_m_Player_1(InputActionMap_t588F4834F020838C4E8AE148F8A42CC84FA75B44 * value)
	{
		___m_Player_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Player_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_PlayerActionsCallbackInterface_2() { return static_cast<int32_t>(offsetof(PlayerInputActions_t375171A1A870F2F98685801D864FC76FD516F22C, ___m_PlayerActionsCallbackInterface_2)); }
	inline RuntimeObject* get_m_PlayerActionsCallbackInterface_2() const { return ___m_PlayerActionsCallbackInterface_2; }
	inline RuntimeObject** get_address_of_m_PlayerActionsCallbackInterface_2() { return &___m_PlayerActionsCallbackInterface_2; }
	inline void set_m_PlayerActionsCallbackInterface_2(RuntimeObject* value)
	{
		___m_PlayerActionsCallbackInterface_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PlayerActionsCallbackInterface_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_Player_Movement_3() { return static_cast<int32_t>(offsetof(PlayerInputActions_t375171A1A870F2F98685801D864FC76FD516F22C, ___m_Player_Movement_3)); }
	inline InputAction_t9AC3F79A5911EDC5CEB0BCD81640FDACA8DB0B0B * get_m_Player_Movement_3() const { return ___m_Player_Movement_3; }
	inline InputAction_t9AC3F79A5911EDC5CEB0BCD81640FDACA8DB0B0B ** get_address_of_m_Player_Movement_3() { return &___m_Player_Movement_3; }
	inline void set_m_Player_Movement_3(InputAction_t9AC3F79A5911EDC5CEB0BCD81640FDACA8DB0B0B * value)
	{
		___m_Player_Movement_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Player_Movement_3), (void*)value);
	}

	inline static int32_t get_offset_of_m_Player_LeftSignal_4() { return static_cast<int32_t>(offsetof(PlayerInputActions_t375171A1A870F2F98685801D864FC76FD516F22C, ___m_Player_LeftSignal_4)); }
	inline InputAction_t9AC3F79A5911EDC5CEB0BCD81640FDACA8DB0B0B * get_m_Player_LeftSignal_4() const { return ___m_Player_LeftSignal_4; }
	inline InputAction_t9AC3F79A5911EDC5CEB0BCD81640FDACA8DB0B0B ** get_address_of_m_Player_LeftSignal_4() { return &___m_Player_LeftSignal_4; }
	inline void set_m_Player_LeftSignal_4(InputAction_t9AC3F79A5911EDC5CEB0BCD81640FDACA8DB0B0B * value)
	{
		___m_Player_LeftSignal_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Player_LeftSignal_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_Player_RightSignal_5() { return static_cast<int32_t>(offsetof(PlayerInputActions_t375171A1A870F2F98685801D864FC76FD516F22C, ___m_Player_RightSignal_5)); }
	inline InputAction_t9AC3F79A5911EDC5CEB0BCD81640FDACA8DB0B0B * get_m_Player_RightSignal_5() const { return ___m_Player_RightSignal_5; }
	inline InputAction_t9AC3F79A5911EDC5CEB0BCD81640FDACA8DB0B0B ** get_address_of_m_Player_RightSignal_5() { return &___m_Player_RightSignal_5; }
	inline void set_m_Player_RightSignal_5(InputAction_t9AC3F79A5911EDC5CEB0BCD81640FDACA8DB0B0B * value)
	{
		___m_Player_RightSignal_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Player_RightSignal_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_Player_Hazard_6() { return static_cast<int32_t>(offsetof(PlayerInputActions_t375171A1A870F2F98685801D864FC76FD516F22C, ___m_Player_Hazard_6)); }
	inline InputAction_t9AC3F79A5911EDC5CEB0BCD81640FDACA8DB0B0B * get_m_Player_Hazard_6() const { return ___m_Player_Hazard_6; }
	inline InputAction_t9AC3F79A5911EDC5CEB0BCD81640FDACA8DB0B0B ** get_address_of_m_Player_Hazard_6() { return &___m_Player_Hazard_6; }
	inline void set_m_Player_Hazard_6(InputAction_t9AC3F79A5911EDC5CEB0BCD81640FDACA8DB0B0B * value)
	{
		___m_Player_Hazard_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Player_Hazard_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_Player_GoForward_7() { return static_cast<int32_t>(offsetof(PlayerInputActions_t375171A1A870F2F98685801D864FC76FD516F22C, ___m_Player_GoForward_7)); }
	inline InputAction_t9AC3F79A5911EDC5CEB0BCD81640FDACA8DB0B0B * get_m_Player_GoForward_7() const { return ___m_Player_GoForward_7; }
	inline InputAction_t9AC3F79A5911EDC5CEB0BCD81640FDACA8DB0B0B ** get_address_of_m_Player_GoForward_7() { return &___m_Player_GoForward_7; }
	inline void set_m_Player_GoForward_7(InputAction_t9AC3F79A5911EDC5CEB0BCD81640FDACA8DB0B0B * value)
	{
		___m_Player_GoForward_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Player_GoForward_7), (void*)value);
	}

	inline static int32_t get_offset_of_m_Player_GoBack_8() { return static_cast<int32_t>(offsetof(PlayerInputActions_t375171A1A870F2F98685801D864FC76FD516F22C, ___m_Player_GoBack_8)); }
	inline InputAction_t9AC3F79A5911EDC5CEB0BCD81640FDACA8DB0B0B * get_m_Player_GoBack_8() const { return ___m_Player_GoBack_8; }
	inline InputAction_t9AC3F79A5911EDC5CEB0BCD81640FDACA8DB0B0B ** get_address_of_m_Player_GoBack_8() { return &___m_Player_GoBack_8; }
	inline void set_m_Player_GoBack_8(InputAction_t9AC3F79A5911EDC5CEB0BCD81640FDACA8DB0B0B * value)
	{
		___m_Player_GoBack_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Player_GoBack_8), (void*)value);
	}

	inline static int32_t get_offset_of_m_Player_GoLeft_9() { return static_cast<int32_t>(offsetof(PlayerInputActions_t375171A1A870F2F98685801D864FC76FD516F22C, ___m_Player_GoLeft_9)); }
	inline InputAction_t9AC3F79A5911EDC5CEB0BCD81640FDACA8DB0B0B * get_m_Player_GoLeft_9() const { return ___m_Player_GoLeft_9; }
	inline InputAction_t9AC3F79A5911EDC5CEB0BCD81640FDACA8DB0B0B ** get_address_of_m_Player_GoLeft_9() { return &___m_Player_GoLeft_9; }
	inline void set_m_Player_GoLeft_9(InputAction_t9AC3F79A5911EDC5CEB0BCD81640FDACA8DB0B0B * value)
	{
		___m_Player_GoLeft_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Player_GoLeft_9), (void*)value);
	}

	inline static int32_t get_offset_of_m_Player_GoRight_10() { return static_cast<int32_t>(offsetof(PlayerInputActions_t375171A1A870F2F98685801D864FC76FD516F22C, ___m_Player_GoRight_10)); }
	inline InputAction_t9AC3F79A5911EDC5CEB0BCD81640FDACA8DB0B0B * get_m_Player_GoRight_10() const { return ___m_Player_GoRight_10; }
	inline InputAction_t9AC3F79A5911EDC5CEB0BCD81640FDACA8DB0B0B ** get_address_of_m_Player_GoRight_10() { return &___m_Player_GoRight_10; }
	inline void set_m_Player_GoRight_10(InputAction_t9AC3F79A5911EDC5CEB0BCD81640FDACA8DB0B0B * value)
	{
		___m_Player_GoRight_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Player_GoRight_10), (void*)value);
	}

	inline static int32_t get_offset_of_m_KeyboardSchemeIndex_11() { return static_cast<int32_t>(offsetof(PlayerInputActions_t375171A1A870F2F98685801D864FC76FD516F22C, ___m_KeyboardSchemeIndex_11)); }
	inline int32_t get_m_KeyboardSchemeIndex_11() const { return ___m_KeyboardSchemeIndex_11; }
	inline int32_t* get_address_of_m_KeyboardSchemeIndex_11() { return &___m_KeyboardSchemeIndex_11; }
	inline void set_m_KeyboardSchemeIndex_11(int32_t value)
	{
		___m_KeyboardSchemeIndex_11 = value;
	}

	inline static int32_t get_offset_of_m_VRRemoteSchemeIndex_12() { return static_cast<int32_t>(offsetof(PlayerInputActions_t375171A1A870F2F98685801D864FC76FD516F22C, ___m_VRRemoteSchemeIndex_12)); }
	inline int32_t get_m_VRRemoteSchemeIndex_12() const { return ___m_VRRemoteSchemeIndex_12; }
	inline int32_t* get_address_of_m_VRRemoteSchemeIndex_12() { return &___m_VRRemoteSchemeIndex_12; }
	inline void set_m_VRRemoteSchemeIndex_12(int32_t value)
	{
		___m_VRRemoteSchemeIndex_12 = value;
	}
};


// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9 * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC * ___m_PersistentCalls_1;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_2;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB, ___m_Calls_0)); }
	inline InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9 * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9 ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9 * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Calls_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PersistentCalls_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_2() { return static_cast<int32_t>(offsetof(UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB, ___m_CallsDirty_2)); }
	inline bool get_m_CallsDirty_2() const { return ___m_CallsDirty_2; }
	inline bool* get_address_of_m_CallsDirty_2() { return &___m_CallsDirty_2; }
	inline void set_m_CallsDirty_2(bool value)
	{
		___m_CallsDirty_2 = value;
	}
};


// System.ValueType
struct  ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// UnityEngine.XR.WindowsMR.WindowsMRExtensions
struct  WindowsMRExtensions_t03F9C7C197F339DE39B5DCB2D3C60968A0A39EC1  : public RuntimeObject
{
public:

public:
};

struct WindowsMRExtensions_t03F9C7C197F339DE39B5DCB2D3C60968A0A39EC1_StaticFields
{
public:
	// UnityEngine.XR.WindowsMR.WindowsMRExtensions/WMRPlane[] UnityEngine.XR.WindowsMR.WindowsMRExtensions::wmrp
	WMRPlaneU5BU5D_tC28080B40113E442BC0C9D74D326973B2C2E136F* ___wmrp_0;

public:
	inline static int32_t get_offset_of_wmrp_0() { return static_cast<int32_t>(offsetof(WindowsMRExtensions_t03F9C7C197F339DE39B5DCB2D3C60968A0A39EC1_StaticFields, ___wmrp_0)); }
	inline WMRPlaneU5BU5D_tC28080B40113E442BC0C9D74D326973B2C2E136F* get_wmrp_0() const { return ___wmrp_0; }
	inline WMRPlaneU5BU5D_tC28080B40113E442BC0C9D74D326973B2C2E136F** get_address_of_wmrp_0() { return &___wmrp_0; }
	inline void set_wmrp_0(WMRPlaneU5BU5D_tC28080B40113E442BC0C9D74D326973B2C2E136F* value)
	{
		___wmrp_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___wmrp_0), (void*)value);
	}
};


// UnityEngine.XR.WindowsMR.WindowsMRInput
struct  WindowsMRInput_t5EDB8246AE09F91332639AB2D8D0D962C79734C7  : public RuntimeObject
{
public:

public:
};


// Readme/Section
struct  Section_t9F25FADC74C202674AFDB11AE2AC4D332DE6CA1D  : public RuntimeObject
{
public:
	// System.String Readme/Section::heading
	String_t* ___heading_0;
	// System.String Readme/Section::text
	String_t* ___text_1;
	// System.String Readme/Section::linkText
	String_t* ___linkText_2;
	// System.String Readme/Section::url
	String_t* ___url_3;

public:
	inline static int32_t get_offset_of_heading_0() { return static_cast<int32_t>(offsetof(Section_t9F25FADC74C202674AFDB11AE2AC4D332DE6CA1D, ___heading_0)); }
	inline String_t* get_heading_0() const { return ___heading_0; }
	inline String_t** get_address_of_heading_0() { return &___heading_0; }
	inline void set_heading_0(String_t* value)
	{
		___heading_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___heading_0), (void*)value);
	}

	inline static int32_t get_offset_of_text_1() { return static_cast<int32_t>(offsetof(Section_t9F25FADC74C202674AFDB11AE2AC4D332DE6CA1D, ___text_1)); }
	inline String_t* get_text_1() const { return ___text_1; }
	inline String_t** get_address_of_text_1() { return &___text_1; }
	inline void set_text_1(String_t* value)
	{
		___text_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___text_1), (void*)value);
	}

	inline static int32_t get_offset_of_linkText_2() { return static_cast<int32_t>(offsetof(Section_t9F25FADC74C202674AFDB11AE2AC4D332DE6CA1D, ___linkText_2)); }
	inline String_t* get_linkText_2() const { return ___linkText_2; }
	inline String_t** get_address_of_linkText_2() { return &___linkText_2; }
	inline void set_linkText_2(String_t* value)
	{
		___linkText_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___linkText_2), (void*)value);
	}

	inline static int32_t get_offset_of_url_3() { return static_cast<int32_t>(offsetof(Section_t9F25FADC74C202674AFDB11AE2AC4D332DE6CA1D, ___url_3)); }
	inline String_t* get_url_3() const { return ___url_3; }
	inline String_t** get_address_of_url_3() { return &___url_3; }
	inline void set_url_3(String_t* value)
	{
		___url_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___url_3), (void*)value);
	}
};


// VRControls/<SimulateKeyPress>d__8
struct  U3CSimulateKeyPressU3Ed__8_tAB6F8BC46272BF48D20F34C4D00E6A15FCEF04C6  : public RuntimeObject
{
public:
	// System.Int32 VRControls/<SimulateKeyPress>d__8::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object VRControls/<SimulateKeyPress>d__8::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CSimulateKeyPressU3Ed__8_tAB6F8BC46272BF48D20F34C4D00E6A15FCEF04C6, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CSimulateKeyPressU3Ed__8_tAB6F8BC46272BF48D20F34C4D00E6A15FCEF04C6, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}
};


// UnityEngine.XR.WindowsMR.WindowsMRExtensions/NativeApi
struct  NativeApi_t8FD1DE10BA51F3BEFE275F05940B1BEA29A71621  : public RuntimeObject
{
public:

public:
};


// UnityEngine.XR.WindowsMR.WindowsMRInput/NativeApi
struct  NativeApi_t5F77BA8AF1BBDB28AE8B5329CDDBCB21D6574EF2  : public RuntimeObject
{
public:

public:
};


// UnityEngine.Events.UnityEvent`1<System.Boolean>
struct  UnityEvent_1_t10C429A2DAF73A4517568E494115F7503F9E17EB  : public UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_1_t10C429A2DAF73A4517568E494115F7503F9E17EB, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InvokeArray_3), (void*)value);
	}
};


// UnityEngine.Events.UnityEvent`1<UnityEngine.Vector2>
struct  UnityEvent_1_t3E6599546F71BCEFF271ED16D5DF9646BD868D7C  : public UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_1_t3E6599546F71BCEFF271ED16D5DF9646BD868D7C, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InvokeArray_3), (void*)value);
	}
};


// UnityEngine.Color
struct  Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};


// System.Enum
struct  Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// UnityEngine.InputSystem.Utilities.FourCC
struct  FourCC_t8D38CDCEAD02FE53A6C2E9AF01354E4E5AB400A9 
{
public:
	// System.Int32 UnityEngine.InputSystem.Utilities.FourCC::m_Code
	int32_t ___m_Code_0;

public:
	inline static int32_t get_offset_of_m_Code_0() { return static_cast<int32_t>(offsetof(FourCC_t8D38CDCEAD02FE53A6C2E9AF01354E4E5AB400A9, ___m_Code_0)); }
	inline int32_t get_m_Code_0() const { return ___m_Code_0; }
	inline int32_t* get_address_of_m_Code_0() { return &___m_Code_0; }
	inline void set_m_Code_0(int32_t value)
	{
		___m_Code_0 = value;
	}
};


// UnityEngine.InputSystem.Layouts.InputDeviceDescription
struct  InputDeviceDescription_tBE78411023CFED0A42110A1357D65BD6E5EDBB58 
{
public:
	// System.String UnityEngine.InputSystem.Layouts.InputDeviceDescription::m_InterfaceName
	String_t* ___m_InterfaceName_0;
	// System.String UnityEngine.InputSystem.Layouts.InputDeviceDescription::m_DeviceClass
	String_t* ___m_DeviceClass_1;
	// System.String UnityEngine.InputSystem.Layouts.InputDeviceDescription::m_Manufacturer
	String_t* ___m_Manufacturer_2;
	// System.String UnityEngine.InputSystem.Layouts.InputDeviceDescription::m_Product
	String_t* ___m_Product_3;
	// System.String UnityEngine.InputSystem.Layouts.InputDeviceDescription::m_Serial
	String_t* ___m_Serial_4;
	// System.String UnityEngine.InputSystem.Layouts.InputDeviceDescription::m_Version
	String_t* ___m_Version_5;
	// System.String UnityEngine.InputSystem.Layouts.InputDeviceDescription::m_Capabilities
	String_t* ___m_Capabilities_6;

public:
	inline static int32_t get_offset_of_m_InterfaceName_0() { return static_cast<int32_t>(offsetof(InputDeviceDescription_tBE78411023CFED0A42110A1357D65BD6E5EDBB58, ___m_InterfaceName_0)); }
	inline String_t* get_m_InterfaceName_0() const { return ___m_InterfaceName_0; }
	inline String_t** get_address_of_m_InterfaceName_0() { return &___m_InterfaceName_0; }
	inline void set_m_InterfaceName_0(String_t* value)
	{
		___m_InterfaceName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InterfaceName_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_DeviceClass_1() { return static_cast<int32_t>(offsetof(InputDeviceDescription_tBE78411023CFED0A42110A1357D65BD6E5EDBB58, ___m_DeviceClass_1)); }
	inline String_t* get_m_DeviceClass_1() const { return ___m_DeviceClass_1; }
	inline String_t** get_address_of_m_DeviceClass_1() { return &___m_DeviceClass_1; }
	inline void set_m_DeviceClass_1(String_t* value)
	{
		___m_DeviceClass_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_DeviceClass_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_Manufacturer_2() { return static_cast<int32_t>(offsetof(InputDeviceDescription_tBE78411023CFED0A42110A1357D65BD6E5EDBB58, ___m_Manufacturer_2)); }
	inline String_t* get_m_Manufacturer_2() const { return ___m_Manufacturer_2; }
	inline String_t** get_address_of_m_Manufacturer_2() { return &___m_Manufacturer_2; }
	inline void set_m_Manufacturer_2(String_t* value)
	{
		___m_Manufacturer_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Manufacturer_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_Product_3() { return static_cast<int32_t>(offsetof(InputDeviceDescription_tBE78411023CFED0A42110A1357D65BD6E5EDBB58, ___m_Product_3)); }
	inline String_t* get_m_Product_3() const { return ___m_Product_3; }
	inline String_t** get_address_of_m_Product_3() { return &___m_Product_3; }
	inline void set_m_Product_3(String_t* value)
	{
		___m_Product_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Product_3), (void*)value);
	}

	inline static int32_t get_offset_of_m_Serial_4() { return static_cast<int32_t>(offsetof(InputDeviceDescription_tBE78411023CFED0A42110A1357D65BD6E5EDBB58, ___m_Serial_4)); }
	inline String_t* get_m_Serial_4() const { return ___m_Serial_4; }
	inline String_t** get_address_of_m_Serial_4() { return &___m_Serial_4; }
	inline void set_m_Serial_4(String_t* value)
	{
		___m_Serial_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Serial_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_Version_5() { return static_cast<int32_t>(offsetof(InputDeviceDescription_tBE78411023CFED0A42110A1357D65BD6E5EDBB58, ___m_Version_5)); }
	inline String_t* get_m_Version_5() const { return ___m_Version_5; }
	inline String_t** get_address_of_m_Version_5() { return &___m_Version_5; }
	inline void set_m_Version_5(String_t* value)
	{
		___m_Version_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Version_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_Capabilities_6() { return static_cast<int32_t>(offsetof(InputDeviceDescription_tBE78411023CFED0A42110A1357D65BD6E5EDBB58, ___m_Capabilities_6)); }
	inline String_t* get_m_Capabilities_6() const { return ___m_Capabilities_6; }
	inline String_t** get_address_of_m_Capabilities_6() { return &___m_Capabilities_6; }
	inline void set_m_Capabilities_6(String_t* value)
	{
		___m_Capabilities_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Capabilities_6), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.InputSystem.Layouts.InputDeviceDescription
struct InputDeviceDescription_tBE78411023CFED0A42110A1357D65BD6E5EDBB58_marshaled_pinvoke
{
	char* ___m_InterfaceName_0;
	char* ___m_DeviceClass_1;
	char* ___m_Manufacturer_2;
	char* ___m_Product_3;
	char* ___m_Serial_4;
	char* ___m_Version_5;
	char* ___m_Capabilities_6;
};
// Native definition for COM marshalling of UnityEngine.InputSystem.Layouts.InputDeviceDescription
struct InputDeviceDescription_tBE78411023CFED0A42110A1357D65BD6E5EDBB58_marshaled_com
{
	Il2CppChar* ___m_InterfaceName_0;
	Il2CppChar* ___m_DeviceClass_1;
	Il2CppChar* ___m_Manufacturer_2;
	Il2CppChar* ___m_Product_3;
	Il2CppChar* ___m_Serial_4;
	Il2CppChar* ___m_Version_5;
	Il2CppChar* ___m_Capabilities_6;
};

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// UnityEngine.InputSystem.Utilities.InternedString
struct  InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED 
{
public:
	// System.String UnityEngine.InputSystem.Utilities.InternedString::m_StringOriginalCase
	String_t* ___m_StringOriginalCase_0;
	// System.String UnityEngine.InputSystem.Utilities.InternedString::m_StringLowerCase
	String_t* ___m_StringLowerCase_1;

public:
	inline static int32_t get_offset_of_m_StringOriginalCase_0() { return static_cast<int32_t>(offsetof(InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED, ___m_StringOriginalCase_0)); }
	inline String_t* get_m_StringOriginalCase_0() const { return ___m_StringOriginalCase_0; }
	inline String_t** get_address_of_m_StringOriginalCase_0() { return &___m_StringOriginalCase_0; }
	inline void set_m_StringOriginalCase_0(String_t* value)
	{
		___m_StringOriginalCase_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_StringOriginalCase_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_StringLowerCase_1() { return static_cast<int32_t>(offsetof(InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED, ___m_StringLowerCase_1)); }
	inline String_t* get_m_StringLowerCase_1() const { return ___m_StringLowerCase_1; }
	inline String_t** get_address_of_m_StringLowerCase_1() { return &___m_StringLowerCase_1; }
	inline void set_m_StringLowerCase_1(String_t* value)
	{
		___m_StringLowerCase_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_StringLowerCase_1), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.InputSystem.Utilities.InternedString
struct InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED_marshaled_pinvoke
{
	char* ___m_StringOriginalCase_0;
	char* ___m_StringLowerCase_1;
};
// Native definition for COM marshalling of UnityEngine.InputSystem.Utilities.InternedString
struct InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED_marshaled_com
{
	Il2CppChar* ___m_StringOriginalCase_0;
	Il2CppChar* ___m_StringLowerCase_1;
};

// UnityEngine.LayerMask
struct  LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8 
{
public:
	// System.Int32 UnityEngine.LayerMask::m_Mask
	int32_t ___m_Mask_0;

public:
	inline static int32_t get_offset_of_m_Mask_0() { return static_cast<int32_t>(offsetof(LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8, ___m_Mask_0)); }
	inline int32_t get_m_Mask_0() const { return ___m_Mask_0; }
	inline int32_t* get_address_of_m_Mask_0() { return &___m_Mask_0; }
	inline void set_m_Mask_0(int32_t value)
	{
		___m_Mask_0 = value;
	}
};


// UnityEngine.Quaternion
struct  Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  value)
	{
		___identityQuaternion_4 = value;
	}
};


// UnityEngine.Events.UnityEvent
struct  UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4  : public UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent::m_InvokeArray
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InvokeArray_3), (void*)value);
	}
};


// UnityEngine.Vector2
struct  Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___zeroVector_2)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___oneVector_3)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___upVector_4)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___downVector_5)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___leftVector_6)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___rightVector_7)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___negativeInfinityVector_9 = value;
	}
};


// UnityEngine.Vector3
struct  Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___zeroVector_5)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___oneVector_6)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___upVector_7)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___downVector_8)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___leftVector_9)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___rightVector_10)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___forwardVector_11)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___backVector_12)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___negativeInfinityVector_14 = value;
	}
};


// UnityEngine.WheelFrictionCurve
struct  WheelFrictionCurve_t96AB1E3B37FC1E7369962A2E712ECD9A2AFE723D 
{
public:
	// System.Single UnityEngine.WheelFrictionCurve::m_ExtremumSlip
	float ___m_ExtremumSlip_0;
	// System.Single UnityEngine.WheelFrictionCurve::m_ExtremumValue
	float ___m_ExtremumValue_1;
	// System.Single UnityEngine.WheelFrictionCurve::m_AsymptoteSlip
	float ___m_AsymptoteSlip_2;
	// System.Single UnityEngine.WheelFrictionCurve::m_AsymptoteValue
	float ___m_AsymptoteValue_3;
	// System.Single UnityEngine.WheelFrictionCurve::m_Stiffness
	float ___m_Stiffness_4;

public:
	inline static int32_t get_offset_of_m_ExtremumSlip_0() { return static_cast<int32_t>(offsetof(WheelFrictionCurve_t96AB1E3B37FC1E7369962A2E712ECD9A2AFE723D, ___m_ExtremumSlip_0)); }
	inline float get_m_ExtremumSlip_0() const { return ___m_ExtremumSlip_0; }
	inline float* get_address_of_m_ExtremumSlip_0() { return &___m_ExtremumSlip_0; }
	inline void set_m_ExtremumSlip_0(float value)
	{
		___m_ExtremumSlip_0 = value;
	}

	inline static int32_t get_offset_of_m_ExtremumValue_1() { return static_cast<int32_t>(offsetof(WheelFrictionCurve_t96AB1E3B37FC1E7369962A2E712ECD9A2AFE723D, ___m_ExtremumValue_1)); }
	inline float get_m_ExtremumValue_1() const { return ___m_ExtremumValue_1; }
	inline float* get_address_of_m_ExtremumValue_1() { return &___m_ExtremumValue_1; }
	inline void set_m_ExtremumValue_1(float value)
	{
		___m_ExtremumValue_1 = value;
	}

	inline static int32_t get_offset_of_m_AsymptoteSlip_2() { return static_cast<int32_t>(offsetof(WheelFrictionCurve_t96AB1E3B37FC1E7369962A2E712ECD9A2AFE723D, ___m_AsymptoteSlip_2)); }
	inline float get_m_AsymptoteSlip_2() const { return ___m_AsymptoteSlip_2; }
	inline float* get_address_of_m_AsymptoteSlip_2() { return &___m_AsymptoteSlip_2; }
	inline void set_m_AsymptoteSlip_2(float value)
	{
		___m_AsymptoteSlip_2 = value;
	}

	inline static int32_t get_offset_of_m_AsymptoteValue_3() { return static_cast<int32_t>(offsetof(WheelFrictionCurve_t96AB1E3B37FC1E7369962A2E712ECD9A2AFE723D, ___m_AsymptoteValue_3)); }
	inline float get_m_AsymptoteValue_3() const { return ___m_AsymptoteValue_3; }
	inline float* get_address_of_m_AsymptoteValue_3() { return &___m_AsymptoteValue_3; }
	inline void set_m_AsymptoteValue_3(float value)
	{
		___m_AsymptoteValue_3 = value;
	}

	inline static int32_t get_offset_of_m_Stiffness_4() { return static_cast<int32_t>(offsetof(WheelFrictionCurve_t96AB1E3B37FC1E7369962A2E712ECD9A2AFE723D, ___m_Stiffness_4)); }
	inline float get_m_Stiffness_4() const { return ___m_Stiffness_4; }
	inline float* get_address_of_m_Stiffness_4() { return &___m_Stiffness_4; }
	inline void set_m_Stiffness_4(float value)
	{
		___m_Stiffness_4 = value;
	}
};


// UnityEngine.XR.WindowsMR.Native/UserDefinedSettings
struct  UserDefinedSettings_t192979420533CDACE82D93D78FCD17264639368D 
{
public:
	// System.UInt16 UnityEngine.XR.WindowsMR.Native/UserDefinedSettings::depthBufferType
	uint16_t ___depthBufferType_0;
	// System.UInt16 UnityEngine.XR.WindowsMR.Native/UserDefinedSettings::sharedDepthBuffer
	uint16_t ___sharedDepthBuffer_1;

public:
	inline static int32_t get_offset_of_depthBufferType_0() { return static_cast<int32_t>(offsetof(UserDefinedSettings_t192979420533CDACE82D93D78FCD17264639368D, ___depthBufferType_0)); }
	inline uint16_t get_depthBufferType_0() const { return ___depthBufferType_0; }
	inline uint16_t* get_address_of_depthBufferType_0() { return &___depthBufferType_0; }
	inline void set_depthBufferType_0(uint16_t value)
	{
		___depthBufferType_0 = value;
	}

	inline static int32_t get_offset_of_sharedDepthBuffer_1() { return static_cast<int32_t>(offsetof(UserDefinedSettings_t192979420533CDACE82D93D78FCD17264639368D, ___sharedDepthBuffer_1)); }
	inline uint16_t get_sharedDepthBuffer_1() const { return ___sharedDepthBuffer_1; }
	inline uint16_t* get_address_of_sharedDepthBuffer_1() { return &___sharedDepthBuffer_1; }
	inline void set_sharedDepthBuffer_1(uint16_t value)
	{
		___sharedDepthBuffer_1 = value;
	}
};


// PlayerInputActions/PlayerActions
struct  PlayerActions_t5ED1C532DD140605FE691CA882B2478B7F7FDE23 
{
public:
	// PlayerInputActions PlayerInputActions/PlayerActions::m_Wrapper
	PlayerInputActions_t375171A1A870F2F98685801D864FC76FD516F22C * ___m_Wrapper_0;

public:
	inline static int32_t get_offset_of_m_Wrapper_0() { return static_cast<int32_t>(offsetof(PlayerActions_t5ED1C532DD140605FE691CA882B2478B7F7FDE23, ___m_Wrapper_0)); }
	inline PlayerInputActions_t375171A1A870F2F98685801D864FC76FD516F22C * get_m_Wrapper_0() const { return ___m_Wrapper_0; }
	inline PlayerInputActions_t375171A1A870F2F98685801D864FC76FD516F22C ** get_address_of_m_Wrapper_0() { return &___m_Wrapper_0; }
	inline void set_m_Wrapper_0(PlayerInputActions_t375171A1A870F2F98685801D864FC76FD516F22C * value)
	{
		___m_Wrapper_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Wrapper_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of PlayerInputActions/PlayerActions
struct PlayerActions_t5ED1C532DD140605FE691CA882B2478B7F7FDE23_marshaled_pinvoke
{
	PlayerInputActions_t375171A1A870F2F98685801D864FC76FD516F22C * ___m_Wrapper_0;
};
// Native definition for COM marshalling of PlayerInputActions/PlayerActions
struct PlayerActions_t5ED1C532DD140605FE691CA882B2478B7F7FDE23_marshaled_com
{
	PlayerInputActions_t375171A1A870F2F98685801D864FC76FD516F22C * ___m_Wrapper_0;
};

// UnityEngine.XR.WindowsMR.WindowsMRExtensions/MeshingData
struct  MeshingData_tC880B07EE51E038020C40CC1B6A61863D78F8703 
{
public:
	// System.Int32 UnityEngine.XR.WindowsMR.WindowsMRExtensions/MeshingData::version
	int32_t ___version_0;
	// System.Object UnityEngine.XR.WindowsMR.WindowsMRExtensions/MeshingData::surfaceInfo
	RuntimeObject * ___surfaceInfo_1;
	// System.Object UnityEngine.XR.WindowsMR.WindowsMRExtensions/MeshingData::surfaceMesh
	RuntimeObject * ___surfaceMesh_2;

public:
	inline static int32_t get_offset_of_version_0() { return static_cast<int32_t>(offsetof(MeshingData_tC880B07EE51E038020C40CC1B6A61863D78F8703, ___version_0)); }
	inline int32_t get_version_0() const { return ___version_0; }
	inline int32_t* get_address_of_version_0() { return &___version_0; }
	inline void set_version_0(int32_t value)
	{
		___version_0 = value;
	}

	inline static int32_t get_offset_of_surfaceInfo_1() { return static_cast<int32_t>(offsetof(MeshingData_tC880B07EE51E038020C40CC1B6A61863D78F8703, ___surfaceInfo_1)); }
	inline RuntimeObject * get_surfaceInfo_1() const { return ___surfaceInfo_1; }
	inline RuntimeObject ** get_address_of_surfaceInfo_1() { return &___surfaceInfo_1; }
	inline void set_surfaceInfo_1(RuntimeObject * value)
	{
		___surfaceInfo_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___surfaceInfo_1), (void*)value);
	}

	inline static int32_t get_offset_of_surfaceMesh_2() { return static_cast<int32_t>(offsetof(MeshingData_tC880B07EE51E038020C40CC1B6A61863D78F8703, ___surfaceMesh_2)); }
	inline RuntimeObject * get_surfaceMesh_2() const { return ___surfaceMesh_2; }
	inline RuntimeObject ** get_address_of_surfaceMesh_2() { return &___surfaceMesh_2; }
	inline void set_surfaceMesh_2(RuntimeObject * value)
	{
		___surfaceMesh_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___surfaceMesh_2), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.XR.WindowsMR.WindowsMRExtensions/MeshingData
struct MeshingData_tC880B07EE51E038020C40CC1B6A61863D78F8703_marshaled_pinvoke
{
	int32_t ___version_0;
	Il2CppIUnknown* ___surfaceInfo_1;
	Il2CppIUnknown* ___surfaceMesh_2;
};
// Native definition for COM marshalling of UnityEngine.XR.WindowsMR.WindowsMRExtensions/MeshingData
struct MeshingData_tC880B07EE51E038020C40CC1B6A61863D78F8703_marshaled_com
{
	int32_t ___version_0;
	Il2CppIUnknown* ___surfaceInfo_1;
	Il2CppIUnknown* ___surfaceMesh_2;
};

// UnityEngine.XR.WindowsMR.WindowsMRExtensions/WMROrientedBox
struct  WMROrientedBox_t89B9CFE40A5BDF457D8CE1D54F09F73446D68454 
{
public:
	// System.Single UnityEngine.XR.WindowsMR.WindowsMRExtensions/WMROrientedBox::cx
	float ___cx_0;
	// System.Single UnityEngine.XR.WindowsMR.WindowsMRExtensions/WMROrientedBox::cy
	float ___cy_1;
	// System.Single UnityEngine.XR.WindowsMR.WindowsMRExtensions/WMROrientedBox::cz
	float ___cz_2;
	// System.Single UnityEngine.XR.WindowsMR.WindowsMRExtensions/WMROrientedBox::ex
	float ___ex_3;
	// System.Single UnityEngine.XR.WindowsMR.WindowsMRExtensions/WMROrientedBox::ey
	float ___ey_4;
	// System.Single UnityEngine.XR.WindowsMR.WindowsMRExtensions/WMROrientedBox::ez
	float ___ez_5;
	// System.Single UnityEngine.XR.WindowsMR.WindowsMRExtensions/WMROrientedBox::ox
	float ___ox_6;
	// System.Single UnityEngine.XR.WindowsMR.WindowsMRExtensions/WMROrientedBox::oy
	float ___oy_7;
	// System.Single UnityEngine.XR.WindowsMR.WindowsMRExtensions/WMROrientedBox::oz
	float ___oz_8;
	// System.Single UnityEngine.XR.WindowsMR.WindowsMRExtensions/WMROrientedBox::ow
	float ___ow_9;

public:
	inline static int32_t get_offset_of_cx_0() { return static_cast<int32_t>(offsetof(WMROrientedBox_t89B9CFE40A5BDF457D8CE1D54F09F73446D68454, ___cx_0)); }
	inline float get_cx_0() const { return ___cx_0; }
	inline float* get_address_of_cx_0() { return &___cx_0; }
	inline void set_cx_0(float value)
	{
		___cx_0 = value;
	}

	inline static int32_t get_offset_of_cy_1() { return static_cast<int32_t>(offsetof(WMROrientedBox_t89B9CFE40A5BDF457D8CE1D54F09F73446D68454, ___cy_1)); }
	inline float get_cy_1() const { return ___cy_1; }
	inline float* get_address_of_cy_1() { return &___cy_1; }
	inline void set_cy_1(float value)
	{
		___cy_1 = value;
	}

	inline static int32_t get_offset_of_cz_2() { return static_cast<int32_t>(offsetof(WMROrientedBox_t89B9CFE40A5BDF457D8CE1D54F09F73446D68454, ___cz_2)); }
	inline float get_cz_2() const { return ___cz_2; }
	inline float* get_address_of_cz_2() { return &___cz_2; }
	inline void set_cz_2(float value)
	{
		___cz_2 = value;
	}

	inline static int32_t get_offset_of_ex_3() { return static_cast<int32_t>(offsetof(WMROrientedBox_t89B9CFE40A5BDF457D8CE1D54F09F73446D68454, ___ex_3)); }
	inline float get_ex_3() const { return ___ex_3; }
	inline float* get_address_of_ex_3() { return &___ex_3; }
	inline void set_ex_3(float value)
	{
		___ex_3 = value;
	}

	inline static int32_t get_offset_of_ey_4() { return static_cast<int32_t>(offsetof(WMROrientedBox_t89B9CFE40A5BDF457D8CE1D54F09F73446D68454, ___ey_4)); }
	inline float get_ey_4() const { return ___ey_4; }
	inline float* get_address_of_ey_4() { return &___ey_4; }
	inline void set_ey_4(float value)
	{
		___ey_4 = value;
	}

	inline static int32_t get_offset_of_ez_5() { return static_cast<int32_t>(offsetof(WMROrientedBox_t89B9CFE40A5BDF457D8CE1D54F09F73446D68454, ___ez_5)); }
	inline float get_ez_5() const { return ___ez_5; }
	inline float* get_address_of_ez_5() { return &___ez_5; }
	inline void set_ez_5(float value)
	{
		___ez_5 = value;
	}

	inline static int32_t get_offset_of_ox_6() { return static_cast<int32_t>(offsetof(WMROrientedBox_t89B9CFE40A5BDF457D8CE1D54F09F73446D68454, ___ox_6)); }
	inline float get_ox_6() const { return ___ox_6; }
	inline float* get_address_of_ox_6() { return &___ox_6; }
	inline void set_ox_6(float value)
	{
		___ox_6 = value;
	}

	inline static int32_t get_offset_of_oy_7() { return static_cast<int32_t>(offsetof(WMROrientedBox_t89B9CFE40A5BDF457D8CE1D54F09F73446D68454, ___oy_7)); }
	inline float get_oy_7() const { return ___oy_7; }
	inline float* get_address_of_oy_7() { return &___oy_7; }
	inline void set_oy_7(float value)
	{
		___oy_7 = value;
	}

	inline static int32_t get_offset_of_oz_8() { return static_cast<int32_t>(offsetof(WMROrientedBox_t89B9CFE40A5BDF457D8CE1D54F09F73446D68454, ___oz_8)); }
	inline float get_oz_8() const { return ___oz_8; }
	inline float* get_address_of_oz_8() { return &___oz_8; }
	inline void set_oz_8(float value)
	{
		___oz_8 = value;
	}

	inline static int32_t get_offset_of_ow_9() { return static_cast<int32_t>(offsetof(WMROrientedBox_t89B9CFE40A5BDF457D8CE1D54F09F73446D68454, ___ow_9)); }
	inline float get_ow_9() const { return ___ow_9; }
	inline float* get_address_of_ow_9() { return &___ow_9; }
	inline void set_ow_9(float value)
	{
		___ow_9 = value;
	}
};


// UnityEngine.XR.WindowsMR.WindowsMRExtensions/WMRPlane
struct  WMRPlane_t145DDAE58819337C56015603B976FAE8AE1A577F 
{
public:
	// System.Single UnityEngine.XR.WindowsMR.WindowsMRExtensions/WMRPlane::d
	float ___d_0;
	// System.Single UnityEngine.XR.WindowsMR.WindowsMRExtensions/WMRPlane::nx
	float ___nx_1;
	// System.Single UnityEngine.XR.WindowsMR.WindowsMRExtensions/WMRPlane::ny
	float ___ny_2;
	// System.Single UnityEngine.XR.WindowsMR.WindowsMRExtensions/WMRPlane::nz
	float ___nz_3;

public:
	inline static int32_t get_offset_of_d_0() { return static_cast<int32_t>(offsetof(WMRPlane_t145DDAE58819337C56015603B976FAE8AE1A577F, ___d_0)); }
	inline float get_d_0() const { return ___d_0; }
	inline float* get_address_of_d_0() { return &___d_0; }
	inline void set_d_0(float value)
	{
		___d_0 = value;
	}

	inline static int32_t get_offset_of_nx_1() { return static_cast<int32_t>(offsetof(WMRPlane_t145DDAE58819337C56015603B976FAE8AE1A577F, ___nx_1)); }
	inline float get_nx_1() const { return ___nx_1; }
	inline float* get_address_of_nx_1() { return &___nx_1; }
	inline void set_nx_1(float value)
	{
		___nx_1 = value;
	}

	inline static int32_t get_offset_of_ny_2() { return static_cast<int32_t>(offsetof(WMRPlane_t145DDAE58819337C56015603B976FAE8AE1A577F, ___ny_2)); }
	inline float get_ny_2() const { return ___ny_2; }
	inline float* get_address_of_ny_2() { return &___ny_2; }
	inline void set_ny_2(float value)
	{
		___ny_2 = value;
	}

	inline static int32_t get_offset_of_nz_3() { return static_cast<int32_t>(offsetof(WMRPlane_t145DDAE58819337C56015603B976FAE8AE1A577F, ___nz_3)); }
	inline float get_nz_3() const { return ___nz_3; }
	inline float* get_address_of_nz_3() { return &___nz_3; }
	inline void set_nz_3(float value)
	{
		___nz_3 = value;
	}
};


// UnityEngine.XR.WindowsMR.WindowsMRExtensions/WMRSphere
struct  WMRSphere_t8587086CFF4EB326D562ECD887085F5D5CEB3BB4 
{
public:
	// System.Single UnityEngine.XR.WindowsMR.WindowsMRExtensions/WMRSphere::cx
	float ___cx_0;
	// System.Single UnityEngine.XR.WindowsMR.WindowsMRExtensions/WMRSphere::cy
	float ___cy_1;
	// System.Single UnityEngine.XR.WindowsMR.WindowsMRExtensions/WMRSphere::cz
	float ___cz_2;
	// System.Single UnityEngine.XR.WindowsMR.WindowsMRExtensions/WMRSphere::r
	float ___r_3;

public:
	inline static int32_t get_offset_of_cx_0() { return static_cast<int32_t>(offsetof(WMRSphere_t8587086CFF4EB326D562ECD887085F5D5CEB3BB4, ___cx_0)); }
	inline float get_cx_0() const { return ___cx_0; }
	inline float* get_address_of_cx_0() { return &___cx_0; }
	inline void set_cx_0(float value)
	{
		___cx_0 = value;
	}

	inline static int32_t get_offset_of_cy_1() { return static_cast<int32_t>(offsetof(WMRSphere_t8587086CFF4EB326D562ECD887085F5D5CEB3BB4, ___cy_1)); }
	inline float get_cy_1() const { return ___cy_1; }
	inline float* get_address_of_cy_1() { return &___cy_1; }
	inline void set_cy_1(float value)
	{
		___cy_1 = value;
	}

	inline static int32_t get_offset_of_cz_2() { return static_cast<int32_t>(offsetof(WMRSphere_t8587086CFF4EB326D562ECD887085F5D5CEB3BB4, ___cz_2)); }
	inline float get_cz_2() const { return ___cz_2; }
	inline float* get_address_of_cz_2() { return &___cz_2; }
	inline void set_cz_2(float value)
	{
		___cz_2 = value;
	}

	inline static int32_t get_offset_of_r_3() { return static_cast<int32_t>(offsetof(WMRSphere_t8587086CFF4EB326D562ECD887085F5D5CEB3BB4, ___r_3)); }
	inline float get_r_3() const { return ___r_3; }
	inline float* get_address_of_r_3() { return &___r_3; }
	inline void set_r_3(float value)
	{
		___r_3 = value;
	}
};


// UnityEngine.XR.WindowsMR.WindowsMRLoader/UserDefinedSettings
struct  UserDefinedSettings_t13075A9CE5747FC83A7F4F3EC5BFA53E0F73402F 
{
public:
	// System.UInt16 UnityEngine.XR.WindowsMR.WindowsMRLoader/UserDefinedSettings::depthBufferType
	uint16_t ___depthBufferType_0;
	// System.UInt16 UnityEngine.XR.WindowsMR.WindowsMRLoader/UserDefinedSettings::sharedDepthBuffer
	uint16_t ___sharedDepthBuffer_1;

public:
	inline static int32_t get_offset_of_depthBufferType_0() { return static_cast<int32_t>(offsetof(UserDefinedSettings_t13075A9CE5747FC83A7F4F3EC5BFA53E0F73402F, ___depthBufferType_0)); }
	inline uint16_t get_depthBufferType_0() const { return ___depthBufferType_0; }
	inline uint16_t* get_address_of_depthBufferType_0() { return &___depthBufferType_0; }
	inline void set_depthBufferType_0(uint16_t value)
	{
		___depthBufferType_0 = value;
	}

	inline static int32_t get_offset_of_sharedDepthBuffer_1() { return static_cast<int32_t>(offsetof(UserDefinedSettings_t13075A9CE5747FC83A7F4F3EC5BFA53E0F73402F, ___sharedDepthBuffer_1)); }
	inline uint16_t get_sharedDepthBuffer_1() const { return ___sharedDepthBuffer_1; }
	inline uint16_t* get_address_of_sharedDepthBuffer_1() { return &___sharedDepthBuffer_1; }
	inline void set_sharedDepthBuffer_1(uint16_t value)
	{
		___sharedDepthBuffer_1 = value;
	}
};


// UnityEngine.InputSystem.LowLevel.InputStateBlock
struct  InputStateBlock_tB9FCBE0F27DDBB11B04B0EC517712C29CC126A0B 
{
public:
	// UnityEngine.InputSystem.Utilities.FourCC UnityEngine.InputSystem.LowLevel.InputStateBlock::<format>k__BackingField
	FourCC_t8D38CDCEAD02FE53A6C2E9AF01354E4E5AB400A9  ___U3CformatU3Ek__BackingField_33;
	// System.UInt32 UnityEngine.InputSystem.LowLevel.InputStateBlock::<byteOffset>k__BackingField
	uint32_t ___U3CbyteOffsetU3Ek__BackingField_34;
	// System.UInt32 UnityEngine.InputSystem.LowLevel.InputStateBlock::<bitOffset>k__BackingField
	uint32_t ___U3CbitOffsetU3Ek__BackingField_35;
	// System.UInt32 UnityEngine.InputSystem.LowLevel.InputStateBlock::<sizeInBits>k__BackingField
	uint32_t ___U3CsizeInBitsU3Ek__BackingField_36;

public:
	inline static int32_t get_offset_of_U3CformatU3Ek__BackingField_33() { return static_cast<int32_t>(offsetof(InputStateBlock_tB9FCBE0F27DDBB11B04B0EC517712C29CC126A0B, ___U3CformatU3Ek__BackingField_33)); }
	inline FourCC_t8D38CDCEAD02FE53A6C2E9AF01354E4E5AB400A9  get_U3CformatU3Ek__BackingField_33() const { return ___U3CformatU3Ek__BackingField_33; }
	inline FourCC_t8D38CDCEAD02FE53A6C2E9AF01354E4E5AB400A9 * get_address_of_U3CformatU3Ek__BackingField_33() { return &___U3CformatU3Ek__BackingField_33; }
	inline void set_U3CformatU3Ek__BackingField_33(FourCC_t8D38CDCEAD02FE53A6C2E9AF01354E4E5AB400A9  value)
	{
		___U3CformatU3Ek__BackingField_33 = value;
	}

	inline static int32_t get_offset_of_U3CbyteOffsetU3Ek__BackingField_34() { return static_cast<int32_t>(offsetof(InputStateBlock_tB9FCBE0F27DDBB11B04B0EC517712C29CC126A0B, ___U3CbyteOffsetU3Ek__BackingField_34)); }
	inline uint32_t get_U3CbyteOffsetU3Ek__BackingField_34() const { return ___U3CbyteOffsetU3Ek__BackingField_34; }
	inline uint32_t* get_address_of_U3CbyteOffsetU3Ek__BackingField_34() { return &___U3CbyteOffsetU3Ek__BackingField_34; }
	inline void set_U3CbyteOffsetU3Ek__BackingField_34(uint32_t value)
	{
		___U3CbyteOffsetU3Ek__BackingField_34 = value;
	}

	inline static int32_t get_offset_of_U3CbitOffsetU3Ek__BackingField_35() { return static_cast<int32_t>(offsetof(InputStateBlock_tB9FCBE0F27DDBB11B04B0EC517712C29CC126A0B, ___U3CbitOffsetU3Ek__BackingField_35)); }
	inline uint32_t get_U3CbitOffsetU3Ek__BackingField_35() const { return ___U3CbitOffsetU3Ek__BackingField_35; }
	inline uint32_t* get_address_of_U3CbitOffsetU3Ek__BackingField_35() { return &___U3CbitOffsetU3Ek__BackingField_35; }
	inline void set_U3CbitOffsetU3Ek__BackingField_35(uint32_t value)
	{
		___U3CbitOffsetU3Ek__BackingField_35 = value;
	}

	inline static int32_t get_offset_of_U3CsizeInBitsU3Ek__BackingField_36() { return static_cast<int32_t>(offsetof(InputStateBlock_tB9FCBE0F27DDBB11B04B0EC517712C29CC126A0B, ___U3CsizeInBitsU3Ek__BackingField_36)); }
	inline uint32_t get_U3CsizeInBitsU3Ek__BackingField_36() const { return ___U3CsizeInBitsU3Ek__BackingField_36; }
	inline uint32_t* get_address_of_U3CsizeInBitsU3Ek__BackingField_36() { return &___U3CsizeInBitsU3Ek__BackingField_36; }
	inline void set_U3CsizeInBitsU3Ek__BackingField_36(uint32_t value)
	{
		___U3CsizeInBitsU3Ek__BackingField_36 = value;
	}
};

struct InputStateBlock_tB9FCBE0F27DDBB11B04B0EC517712C29CC126A0B_StaticFields
{
public:
	// UnityEngine.InputSystem.Utilities.FourCC UnityEngine.InputSystem.LowLevel.InputStateBlock::FormatBit
	FourCC_t8D38CDCEAD02FE53A6C2E9AF01354E4E5AB400A9  ___FormatBit_2;
	// UnityEngine.InputSystem.Utilities.FourCC UnityEngine.InputSystem.LowLevel.InputStateBlock::FormatSBit
	FourCC_t8D38CDCEAD02FE53A6C2E9AF01354E4E5AB400A9  ___FormatSBit_4;
	// UnityEngine.InputSystem.Utilities.FourCC UnityEngine.InputSystem.LowLevel.InputStateBlock::FormatInt
	FourCC_t8D38CDCEAD02FE53A6C2E9AF01354E4E5AB400A9  ___FormatInt_6;
	// UnityEngine.InputSystem.Utilities.FourCC UnityEngine.InputSystem.LowLevel.InputStateBlock::FormatUInt
	FourCC_t8D38CDCEAD02FE53A6C2E9AF01354E4E5AB400A9  ___FormatUInt_8;
	// UnityEngine.InputSystem.Utilities.FourCC UnityEngine.InputSystem.LowLevel.InputStateBlock::FormatShort
	FourCC_t8D38CDCEAD02FE53A6C2E9AF01354E4E5AB400A9  ___FormatShort_10;
	// UnityEngine.InputSystem.Utilities.FourCC UnityEngine.InputSystem.LowLevel.InputStateBlock::FormatUShort
	FourCC_t8D38CDCEAD02FE53A6C2E9AF01354E4E5AB400A9  ___FormatUShort_12;
	// UnityEngine.InputSystem.Utilities.FourCC UnityEngine.InputSystem.LowLevel.InputStateBlock::FormatByte
	FourCC_t8D38CDCEAD02FE53A6C2E9AF01354E4E5AB400A9  ___FormatByte_14;
	// UnityEngine.InputSystem.Utilities.FourCC UnityEngine.InputSystem.LowLevel.InputStateBlock::FormatSByte
	FourCC_t8D38CDCEAD02FE53A6C2E9AF01354E4E5AB400A9  ___FormatSByte_16;
	// UnityEngine.InputSystem.Utilities.FourCC UnityEngine.InputSystem.LowLevel.InputStateBlock::FormatLong
	FourCC_t8D38CDCEAD02FE53A6C2E9AF01354E4E5AB400A9  ___FormatLong_18;
	// UnityEngine.InputSystem.Utilities.FourCC UnityEngine.InputSystem.LowLevel.InputStateBlock::FormatULong
	FourCC_t8D38CDCEAD02FE53A6C2E9AF01354E4E5AB400A9  ___FormatULong_20;
	// UnityEngine.InputSystem.Utilities.FourCC UnityEngine.InputSystem.LowLevel.InputStateBlock::FormatFloat
	FourCC_t8D38CDCEAD02FE53A6C2E9AF01354E4E5AB400A9  ___FormatFloat_22;
	// UnityEngine.InputSystem.Utilities.FourCC UnityEngine.InputSystem.LowLevel.InputStateBlock::FormatDouble
	FourCC_t8D38CDCEAD02FE53A6C2E9AF01354E4E5AB400A9  ___FormatDouble_24;
	// UnityEngine.InputSystem.Utilities.FourCC UnityEngine.InputSystem.LowLevel.InputStateBlock::FormatVector2
	FourCC_t8D38CDCEAD02FE53A6C2E9AF01354E4E5AB400A9  ___FormatVector2_26;
	// UnityEngine.InputSystem.Utilities.FourCC UnityEngine.InputSystem.LowLevel.InputStateBlock::FormatVector3
	FourCC_t8D38CDCEAD02FE53A6C2E9AF01354E4E5AB400A9  ___FormatVector3_27;
	// UnityEngine.InputSystem.Utilities.FourCC UnityEngine.InputSystem.LowLevel.InputStateBlock::FormatQuaternion
	FourCC_t8D38CDCEAD02FE53A6C2E9AF01354E4E5AB400A9  ___FormatQuaternion_28;
	// UnityEngine.InputSystem.Utilities.FourCC UnityEngine.InputSystem.LowLevel.InputStateBlock::FormatVector2Short
	FourCC_t8D38CDCEAD02FE53A6C2E9AF01354E4E5AB400A9  ___FormatVector2Short_29;
	// UnityEngine.InputSystem.Utilities.FourCC UnityEngine.InputSystem.LowLevel.InputStateBlock::FormatVector3Short
	FourCC_t8D38CDCEAD02FE53A6C2E9AF01354E4E5AB400A9  ___FormatVector3Short_30;
	// UnityEngine.InputSystem.Utilities.FourCC UnityEngine.InputSystem.LowLevel.InputStateBlock::FormatVector2Byte
	FourCC_t8D38CDCEAD02FE53A6C2E9AF01354E4E5AB400A9  ___FormatVector2Byte_31;
	// UnityEngine.InputSystem.Utilities.FourCC UnityEngine.InputSystem.LowLevel.InputStateBlock::FormatVector3Byte
	FourCC_t8D38CDCEAD02FE53A6C2E9AF01354E4E5AB400A9  ___FormatVector3Byte_32;

public:
	inline static int32_t get_offset_of_FormatBit_2() { return static_cast<int32_t>(offsetof(InputStateBlock_tB9FCBE0F27DDBB11B04B0EC517712C29CC126A0B_StaticFields, ___FormatBit_2)); }
	inline FourCC_t8D38CDCEAD02FE53A6C2E9AF01354E4E5AB400A9  get_FormatBit_2() const { return ___FormatBit_2; }
	inline FourCC_t8D38CDCEAD02FE53A6C2E9AF01354E4E5AB400A9 * get_address_of_FormatBit_2() { return &___FormatBit_2; }
	inline void set_FormatBit_2(FourCC_t8D38CDCEAD02FE53A6C2E9AF01354E4E5AB400A9  value)
	{
		___FormatBit_2 = value;
	}

	inline static int32_t get_offset_of_FormatSBit_4() { return static_cast<int32_t>(offsetof(InputStateBlock_tB9FCBE0F27DDBB11B04B0EC517712C29CC126A0B_StaticFields, ___FormatSBit_4)); }
	inline FourCC_t8D38CDCEAD02FE53A6C2E9AF01354E4E5AB400A9  get_FormatSBit_4() const { return ___FormatSBit_4; }
	inline FourCC_t8D38CDCEAD02FE53A6C2E9AF01354E4E5AB400A9 * get_address_of_FormatSBit_4() { return &___FormatSBit_4; }
	inline void set_FormatSBit_4(FourCC_t8D38CDCEAD02FE53A6C2E9AF01354E4E5AB400A9  value)
	{
		___FormatSBit_4 = value;
	}

	inline static int32_t get_offset_of_FormatInt_6() { return static_cast<int32_t>(offsetof(InputStateBlock_tB9FCBE0F27DDBB11B04B0EC517712C29CC126A0B_StaticFields, ___FormatInt_6)); }
	inline FourCC_t8D38CDCEAD02FE53A6C2E9AF01354E4E5AB400A9  get_FormatInt_6() const { return ___FormatInt_6; }
	inline FourCC_t8D38CDCEAD02FE53A6C2E9AF01354E4E5AB400A9 * get_address_of_FormatInt_6() { return &___FormatInt_6; }
	inline void set_FormatInt_6(FourCC_t8D38CDCEAD02FE53A6C2E9AF01354E4E5AB400A9  value)
	{
		___FormatInt_6 = value;
	}

	inline static int32_t get_offset_of_FormatUInt_8() { return static_cast<int32_t>(offsetof(InputStateBlock_tB9FCBE0F27DDBB11B04B0EC517712C29CC126A0B_StaticFields, ___FormatUInt_8)); }
	inline FourCC_t8D38CDCEAD02FE53A6C2E9AF01354E4E5AB400A9  get_FormatUInt_8() const { return ___FormatUInt_8; }
	inline FourCC_t8D38CDCEAD02FE53A6C2E9AF01354E4E5AB400A9 * get_address_of_FormatUInt_8() { return &___FormatUInt_8; }
	inline void set_FormatUInt_8(FourCC_t8D38CDCEAD02FE53A6C2E9AF01354E4E5AB400A9  value)
	{
		___FormatUInt_8 = value;
	}

	inline static int32_t get_offset_of_FormatShort_10() { return static_cast<int32_t>(offsetof(InputStateBlock_tB9FCBE0F27DDBB11B04B0EC517712C29CC126A0B_StaticFields, ___FormatShort_10)); }
	inline FourCC_t8D38CDCEAD02FE53A6C2E9AF01354E4E5AB400A9  get_FormatShort_10() const { return ___FormatShort_10; }
	inline FourCC_t8D38CDCEAD02FE53A6C2E9AF01354E4E5AB400A9 * get_address_of_FormatShort_10() { return &___FormatShort_10; }
	inline void set_FormatShort_10(FourCC_t8D38CDCEAD02FE53A6C2E9AF01354E4E5AB400A9  value)
	{
		___FormatShort_10 = value;
	}

	inline static int32_t get_offset_of_FormatUShort_12() { return static_cast<int32_t>(offsetof(InputStateBlock_tB9FCBE0F27DDBB11B04B0EC517712C29CC126A0B_StaticFields, ___FormatUShort_12)); }
	inline FourCC_t8D38CDCEAD02FE53A6C2E9AF01354E4E5AB400A9  get_FormatUShort_12() const { return ___FormatUShort_12; }
	inline FourCC_t8D38CDCEAD02FE53A6C2E9AF01354E4E5AB400A9 * get_address_of_FormatUShort_12() { return &___FormatUShort_12; }
	inline void set_FormatUShort_12(FourCC_t8D38CDCEAD02FE53A6C2E9AF01354E4E5AB400A9  value)
	{
		___FormatUShort_12 = value;
	}

	inline static int32_t get_offset_of_FormatByte_14() { return static_cast<int32_t>(offsetof(InputStateBlock_tB9FCBE0F27DDBB11B04B0EC517712C29CC126A0B_StaticFields, ___FormatByte_14)); }
	inline FourCC_t8D38CDCEAD02FE53A6C2E9AF01354E4E5AB400A9  get_FormatByte_14() const { return ___FormatByte_14; }
	inline FourCC_t8D38CDCEAD02FE53A6C2E9AF01354E4E5AB400A9 * get_address_of_FormatByte_14() { return &___FormatByte_14; }
	inline void set_FormatByte_14(FourCC_t8D38CDCEAD02FE53A6C2E9AF01354E4E5AB400A9  value)
	{
		___FormatByte_14 = value;
	}

	inline static int32_t get_offset_of_FormatSByte_16() { return static_cast<int32_t>(offsetof(InputStateBlock_tB9FCBE0F27DDBB11B04B0EC517712C29CC126A0B_StaticFields, ___FormatSByte_16)); }
	inline FourCC_t8D38CDCEAD02FE53A6C2E9AF01354E4E5AB400A9  get_FormatSByte_16() const { return ___FormatSByte_16; }
	inline FourCC_t8D38CDCEAD02FE53A6C2E9AF01354E4E5AB400A9 * get_address_of_FormatSByte_16() { return &___FormatSByte_16; }
	inline void set_FormatSByte_16(FourCC_t8D38CDCEAD02FE53A6C2E9AF01354E4E5AB400A9  value)
	{
		___FormatSByte_16 = value;
	}

	inline static int32_t get_offset_of_FormatLong_18() { return static_cast<int32_t>(offsetof(InputStateBlock_tB9FCBE0F27DDBB11B04B0EC517712C29CC126A0B_StaticFields, ___FormatLong_18)); }
	inline FourCC_t8D38CDCEAD02FE53A6C2E9AF01354E4E5AB400A9  get_FormatLong_18() const { return ___FormatLong_18; }
	inline FourCC_t8D38CDCEAD02FE53A6C2E9AF01354E4E5AB400A9 * get_address_of_FormatLong_18() { return &___FormatLong_18; }
	inline void set_FormatLong_18(FourCC_t8D38CDCEAD02FE53A6C2E9AF01354E4E5AB400A9  value)
	{
		___FormatLong_18 = value;
	}

	inline static int32_t get_offset_of_FormatULong_20() { return static_cast<int32_t>(offsetof(InputStateBlock_tB9FCBE0F27DDBB11B04B0EC517712C29CC126A0B_StaticFields, ___FormatULong_20)); }
	inline FourCC_t8D38CDCEAD02FE53A6C2E9AF01354E4E5AB400A9  get_FormatULong_20() const { return ___FormatULong_20; }
	inline FourCC_t8D38CDCEAD02FE53A6C2E9AF01354E4E5AB400A9 * get_address_of_FormatULong_20() { return &___FormatULong_20; }
	inline void set_FormatULong_20(FourCC_t8D38CDCEAD02FE53A6C2E9AF01354E4E5AB400A9  value)
	{
		___FormatULong_20 = value;
	}

	inline static int32_t get_offset_of_FormatFloat_22() { return static_cast<int32_t>(offsetof(InputStateBlock_tB9FCBE0F27DDBB11B04B0EC517712C29CC126A0B_StaticFields, ___FormatFloat_22)); }
	inline FourCC_t8D38CDCEAD02FE53A6C2E9AF01354E4E5AB400A9  get_FormatFloat_22() const { return ___FormatFloat_22; }
	inline FourCC_t8D38CDCEAD02FE53A6C2E9AF01354E4E5AB400A9 * get_address_of_FormatFloat_22() { return &___FormatFloat_22; }
	inline void set_FormatFloat_22(FourCC_t8D38CDCEAD02FE53A6C2E9AF01354E4E5AB400A9  value)
	{
		___FormatFloat_22 = value;
	}

	inline static int32_t get_offset_of_FormatDouble_24() { return static_cast<int32_t>(offsetof(InputStateBlock_tB9FCBE0F27DDBB11B04B0EC517712C29CC126A0B_StaticFields, ___FormatDouble_24)); }
	inline FourCC_t8D38CDCEAD02FE53A6C2E9AF01354E4E5AB400A9  get_FormatDouble_24() const { return ___FormatDouble_24; }
	inline FourCC_t8D38CDCEAD02FE53A6C2E9AF01354E4E5AB400A9 * get_address_of_FormatDouble_24() { return &___FormatDouble_24; }
	inline void set_FormatDouble_24(FourCC_t8D38CDCEAD02FE53A6C2E9AF01354E4E5AB400A9  value)
	{
		___FormatDouble_24 = value;
	}

	inline static int32_t get_offset_of_FormatVector2_26() { return static_cast<int32_t>(offsetof(InputStateBlock_tB9FCBE0F27DDBB11B04B0EC517712C29CC126A0B_StaticFields, ___FormatVector2_26)); }
	inline FourCC_t8D38CDCEAD02FE53A6C2E9AF01354E4E5AB400A9  get_FormatVector2_26() const { return ___FormatVector2_26; }
	inline FourCC_t8D38CDCEAD02FE53A6C2E9AF01354E4E5AB400A9 * get_address_of_FormatVector2_26() { return &___FormatVector2_26; }
	inline void set_FormatVector2_26(FourCC_t8D38CDCEAD02FE53A6C2E9AF01354E4E5AB400A9  value)
	{
		___FormatVector2_26 = value;
	}

	inline static int32_t get_offset_of_FormatVector3_27() { return static_cast<int32_t>(offsetof(InputStateBlock_tB9FCBE0F27DDBB11B04B0EC517712C29CC126A0B_StaticFields, ___FormatVector3_27)); }
	inline FourCC_t8D38CDCEAD02FE53A6C2E9AF01354E4E5AB400A9  get_FormatVector3_27() const { return ___FormatVector3_27; }
	inline FourCC_t8D38CDCEAD02FE53A6C2E9AF01354E4E5AB400A9 * get_address_of_FormatVector3_27() { return &___FormatVector3_27; }
	inline void set_FormatVector3_27(FourCC_t8D38CDCEAD02FE53A6C2E9AF01354E4E5AB400A9  value)
	{
		___FormatVector3_27 = value;
	}

	inline static int32_t get_offset_of_FormatQuaternion_28() { return static_cast<int32_t>(offsetof(InputStateBlock_tB9FCBE0F27DDBB11B04B0EC517712C29CC126A0B_StaticFields, ___FormatQuaternion_28)); }
	inline FourCC_t8D38CDCEAD02FE53A6C2E9AF01354E4E5AB400A9  get_FormatQuaternion_28() const { return ___FormatQuaternion_28; }
	inline FourCC_t8D38CDCEAD02FE53A6C2E9AF01354E4E5AB400A9 * get_address_of_FormatQuaternion_28() { return &___FormatQuaternion_28; }
	inline void set_FormatQuaternion_28(FourCC_t8D38CDCEAD02FE53A6C2E9AF01354E4E5AB400A9  value)
	{
		___FormatQuaternion_28 = value;
	}

	inline static int32_t get_offset_of_FormatVector2Short_29() { return static_cast<int32_t>(offsetof(InputStateBlock_tB9FCBE0F27DDBB11B04B0EC517712C29CC126A0B_StaticFields, ___FormatVector2Short_29)); }
	inline FourCC_t8D38CDCEAD02FE53A6C2E9AF01354E4E5AB400A9  get_FormatVector2Short_29() const { return ___FormatVector2Short_29; }
	inline FourCC_t8D38CDCEAD02FE53A6C2E9AF01354E4E5AB400A9 * get_address_of_FormatVector2Short_29() { return &___FormatVector2Short_29; }
	inline void set_FormatVector2Short_29(FourCC_t8D38CDCEAD02FE53A6C2E9AF01354E4E5AB400A9  value)
	{
		___FormatVector2Short_29 = value;
	}

	inline static int32_t get_offset_of_FormatVector3Short_30() { return static_cast<int32_t>(offsetof(InputStateBlock_tB9FCBE0F27DDBB11B04B0EC517712C29CC126A0B_StaticFields, ___FormatVector3Short_30)); }
	inline FourCC_t8D38CDCEAD02FE53A6C2E9AF01354E4E5AB400A9  get_FormatVector3Short_30() const { return ___FormatVector3Short_30; }
	inline FourCC_t8D38CDCEAD02FE53A6C2E9AF01354E4E5AB400A9 * get_address_of_FormatVector3Short_30() { return &___FormatVector3Short_30; }
	inline void set_FormatVector3Short_30(FourCC_t8D38CDCEAD02FE53A6C2E9AF01354E4E5AB400A9  value)
	{
		___FormatVector3Short_30 = value;
	}

	inline static int32_t get_offset_of_FormatVector2Byte_31() { return static_cast<int32_t>(offsetof(InputStateBlock_tB9FCBE0F27DDBB11B04B0EC517712C29CC126A0B_StaticFields, ___FormatVector2Byte_31)); }
	inline FourCC_t8D38CDCEAD02FE53A6C2E9AF01354E4E5AB400A9  get_FormatVector2Byte_31() const { return ___FormatVector2Byte_31; }
	inline FourCC_t8D38CDCEAD02FE53A6C2E9AF01354E4E5AB400A9 * get_address_of_FormatVector2Byte_31() { return &___FormatVector2Byte_31; }
	inline void set_FormatVector2Byte_31(FourCC_t8D38CDCEAD02FE53A6C2E9AF01354E4E5AB400A9  value)
	{
		___FormatVector2Byte_31 = value;
	}

	inline static int32_t get_offset_of_FormatVector3Byte_32() { return static_cast<int32_t>(offsetof(InputStateBlock_tB9FCBE0F27DDBB11B04B0EC517712C29CC126A0B_StaticFields, ___FormatVector3Byte_32)); }
	inline FourCC_t8D38CDCEAD02FE53A6C2E9AF01354E4E5AB400A9  get_FormatVector3Byte_32() const { return ___FormatVector3Byte_32; }
	inline FourCC_t8D38CDCEAD02FE53A6C2E9AF01354E4E5AB400A9 * get_address_of_FormatVector3Byte_32() { return &___FormatVector3Byte_32; }
	inline void set_FormatVector3Byte_32(FourCC_t8D38CDCEAD02FE53A6C2E9AF01354E4E5AB400A9  value)
	{
		___FormatVector3Byte_32 = value;
	}
};


// UnityEngine.Object
struct  Object_tF2F3778131EFF286AF62B7B013A170F95A91571A  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// System.TypeCode
struct  TypeCode_tCB39BAB5CFB7A1E0BCB521413E3C46B81C31AA7C 
{
public:
	// System.Int32 System.TypeCode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TypeCode_tCB39BAB5CFB7A1E0BCB521413E3C46B81C31AA7C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.InputSystem.InputControl/ControlFlags
struct  ControlFlags_t43CF0138618503E94B4811FD6CF66F13F0186787 
{
public:
	// System.Int32 UnityEngine.InputSystem.InputControl/ControlFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ControlFlags_t43CF0138618503E94B4811FD6CF66F13F0186787, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.InputSystem.InputDevice/DeviceFlags
struct  DeviceFlags_tF2567D99F286E930FC6B04C6A3CCB2E65B1D762D 
{
public:
	// System.Int32 UnityEngine.InputSystem.InputDevice/DeviceFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DeviceFlags_tF2567D99F286E930FC6B04C6A3CCB2E65B1D762D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// MouseLook/RotationAxes
struct  RotationAxes_t4ECFECEB53ADD83AF4841AF02D59C1D562CC7EC0 
{
public:
	// System.Int32 MouseLook/RotationAxes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RotationAxes_t4ECFECEB53ADD83AF4841AF02D59C1D562CC7EC0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.XR.WindowsMR.Native/UnitySubsystemErrorCode
struct  UnitySubsystemErrorCode_tAA5F8843DC35694D214A6CE631CD3CD5382AD846 
{
public:
	// System.Int32 UnityEngine.XR.WindowsMR.Native/UnitySubsystemErrorCode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UnitySubsystemErrorCode_tAA5F8843DC35694D214A6CE631CD3CD5382AD846, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.XR.WindowsMR.NativeTypes/SpatialLocatability
struct  SpatialLocatability_t8CF515124A8D73BEDCFA7133166CF5DAB300B2DD 
{
public:
	// System.Int32 UnityEngine.XR.WindowsMR.NativeTypes/SpatialLocatability::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SpatialLocatability_t8CF515124A8D73BEDCFA7133166CF5DAB300B2DD, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// RoadDecider/ControlTypes
struct  ControlTypes_tA3908D80A567868B8F2C84147F045D36A051B4C6 
{
public:
	// System.Int32 RoadDecider/ControlTypes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ControlTypes_tA3908D80A567868B8F2C84147F045D36A051B4C6, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// RoadDecider/Directions
struct  Directions_t960CFDDDEA9EDA98F5C8E72E7A9682F230821891 
{
public:
	// System.Int32 RoadDecider/Directions::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Directions_t960CFDDDEA9EDA98F5C8E72E7A9682F230821891, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// TrafficLights/LightCount
struct  LightCount_t3D030F443416E83983AB89A4AD529CF0A4CF8F73 
{
public:
	// System.Int32 TrafficLights/LightCount::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LightCount_t3D030F443416E83983AB89A4AD529CF0A4CF8F73, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// TrafficRules/States
struct  States_t96903228AD196C0C3ACDB1C778496FF9DA9E5D2C 
{
public:
	// System.Int32 TrafficRules/States::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(States_t96903228AD196C0C3ACDB1C778496FF9DA9E5D2C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UIVirtualButton/BoolEvent
struct  BoolEvent_tD84A3E3A4245DFD9FA5D608A5ADA77DBDFB6BD56  : public UnityEvent_1_t10C429A2DAF73A4517568E494115F7503F9E17EB
{
public:

public:
};


// UIVirtualButton/Event
struct  Event_tB8168EB885996D80674A82913E2B33B4915A9E23  : public UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4
{
public:

public:
};


// UIVirtualJoystick/Event
struct  Event_t5D63277B4BC67CE95DE0316A422A69420D3A368D  : public UnityEvent_1_t3E6599546F71BCEFF271ED16D5DF9646BD868D7C
{
public:

public:
};


// UIVirtualTouchZone/Event
struct  Event_t1E1104A6087ED46DBF720AC59C7A8E63B974639D  : public UnityEvent_1_t3E6599546F71BCEFF271ED16D5DF9646BD868D7C
{
public:

public:
};


// Water/WaterMode
struct  WaterMode_tC644407B60E63E8184AD7B9A01762A385325D86C 
{
public:
	// System.Int32 Water/WaterMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(WaterMode_tC644407B60E63E8184AD7B9A01762A385325D86C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.XR.WindowsMR.WindowsMRSettings/DepthBufferOption
struct  DepthBufferOption_tAC9E22B0DD4E1FE942C6E0D77525A118FF7B6462 
{
public:
	// System.Int32 UnityEngine.XR.WindowsMR.WindowsMRSettings/DepthBufferOption::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DepthBufferOption_tAC9E22B0DD4E1FE942C6E0D77525A118FF7B6462, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Component
struct  Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// UnityEngine.InputSystem.Utilities.PrimitiveValue
struct  PrimitiveValue_t70D8365A1CB2217C466D27D6820362EF4331A0C8 
{
public:
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.TypeCode UnityEngine.InputSystem.Utilities.PrimitiveValue::m_Type
			int32_t ___m_Type_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___m_Type_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_BoolValue_1_OffsetPadding[4];
			// System.Boolean UnityEngine.InputSystem.Utilities.PrimitiveValue::m_BoolValue
			bool ___m_BoolValue_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_BoolValue_1_OffsetPadding_forAlignmentOnly[4];
			bool ___m_BoolValue_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_CharValue_2_OffsetPadding[4];
			// System.Char UnityEngine.InputSystem.Utilities.PrimitiveValue::m_CharValue
			Il2CppChar ___m_CharValue_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_CharValue_2_OffsetPadding_forAlignmentOnly[4];
			Il2CppChar ___m_CharValue_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_ByteValue_3_OffsetPadding[4];
			// System.Byte UnityEngine.InputSystem.Utilities.PrimitiveValue::m_ByteValue
			uint8_t ___m_ByteValue_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_ByteValue_3_OffsetPadding_forAlignmentOnly[4];
			uint8_t ___m_ByteValue_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_SByteValue_4_OffsetPadding[4];
			// System.SByte UnityEngine.InputSystem.Utilities.PrimitiveValue::m_SByteValue
			int8_t ___m_SByteValue_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_SByteValue_4_OffsetPadding_forAlignmentOnly[4];
			int8_t ___m_SByteValue_4_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_ShortValue_5_OffsetPadding[4];
			// System.Int16 UnityEngine.InputSystem.Utilities.PrimitiveValue::m_ShortValue
			int16_t ___m_ShortValue_5;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_ShortValue_5_OffsetPadding_forAlignmentOnly[4];
			int16_t ___m_ShortValue_5_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_UShortValue_6_OffsetPadding[4];
			// System.UInt16 UnityEngine.InputSystem.Utilities.PrimitiveValue::m_UShortValue
			uint16_t ___m_UShortValue_6;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_UShortValue_6_OffsetPadding_forAlignmentOnly[4];
			uint16_t ___m_UShortValue_6_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_IntValue_7_OffsetPadding[4];
			// System.Int32 UnityEngine.InputSystem.Utilities.PrimitiveValue::m_IntValue
			int32_t ___m_IntValue_7;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_IntValue_7_OffsetPadding_forAlignmentOnly[4];
			int32_t ___m_IntValue_7_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_UIntValue_8_OffsetPadding[4];
			// System.UInt32 UnityEngine.InputSystem.Utilities.PrimitiveValue::m_UIntValue
			uint32_t ___m_UIntValue_8;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_UIntValue_8_OffsetPadding_forAlignmentOnly[4];
			uint32_t ___m_UIntValue_8_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_LongValue_9_OffsetPadding[4];
			// System.Int64 UnityEngine.InputSystem.Utilities.PrimitiveValue::m_LongValue
			int64_t ___m_LongValue_9;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_LongValue_9_OffsetPadding_forAlignmentOnly[4];
			int64_t ___m_LongValue_9_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_ULongValue_10_OffsetPadding[4];
			// System.UInt64 UnityEngine.InputSystem.Utilities.PrimitiveValue::m_ULongValue
			uint64_t ___m_ULongValue_10;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_ULongValue_10_OffsetPadding_forAlignmentOnly[4];
			uint64_t ___m_ULongValue_10_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_FloatValue_11_OffsetPadding[4];
			// System.Single UnityEngine.InputSystem.Utilities.PrimitiveValue::m_FloatValue
			float ___m_FloatValue_11;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_FloatValue_11_OffsetPadding_forAlignmentOnly[4];
			float ___m_FloatValue_11_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_DoubleValue_12_OffsetPadding[4];
			// System.Double UnityEngine.InputSystem.Utilities.PrimitiveValue::m_DoubleValue
			double ___m_DoubleValue_12;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_DoubleValue_12_OffsetPadding_forAlignmentOnly[4];
			double ___m_DoubleValue_12_forAlignmentOnly;
		};
	};

public:
	inline static int32_t get_offset_of_m_Type_0() { return static_cast<int32_t>(offsetof(PrimitiveValue_t70D8365A1CB2217C466D27D6820362EF4331A0C8, ___m_Type_0)); }
	inline int32_t get_m_Type_0() const { return ___m_Type_0; }
	inline int32_t* get_address_of_m_Type_0() { return &___m_Type_0; }
	inline void set_m_Type_0(int32_t value)
	{
		___m_Type_0 = value;
	}

	inline static int32_t get_offset_of_m_BoolValue_1() { return static_cast<int32_t>(offsetof(PrimitiveValue_t70D8365A1CB2217C466D27D6820362EF4331A0C8, ___m_BoolValue_1)); }
	inline bool get_m_BoolValue_1() const { return ___m_BoolValue_1; }
	inline bool* get_address_of_m_BoolValue_1() { return &___m_BoolValue_1; }
	inline void set_m_BoolValue_1(bool value)
	{
		___m_BoolValue_1 = value;
	}

	inline static int32_t get_offset_of_m_CharValue_2() { return static_cast<int32_t>(offsetof(PrimitiveValue_t70D8365A1CB2217C466D27D6820362EF4331A0C8, ___m_CharValue_2)); }
	inline Il2CppChar get_m_CharValue_2() const { return ___m_CharValue_2; }
	inline Il2CppChar* get_address_of_m_CharValue_2() { return &___m_CharValue_2; }
	inline void set_m_CharValue_2(Il2CppChar value)
	{
		___m_CharValue_2 = value;
	}

	inline static int32_t get_offset_of_m_ByteValue_3() { return static_cast<int32_t>(offsetof(PrimitiveValue_t70D8365A1CB2217C466D27D6820362EF4331A0C8, ___m_ByteValue_3)); }
	inline uint8_t get_m_ByteValue_3() const { return ___m_ByteValue_3; }
	inline uint8_t* get_address_of_m_ByteValue_3() { return &___m_ByteValue_3; }
	inline void set_m_ByteValue_3(uint8_t value)
	{
		___m_ByteValue_3 = value;
	}

	inline static int32_t get_offset_of_m_SByteValue_4() { return static_cast<int32_t>(offsetof(PrimitiveValue_t70D8365A1CB2217C466D27D6820362EF4331A0C8, ___m_SByteValue_4)); }
	inline int8_t get_m_SByteValue_4() const { return ___m_SByteValue_4; }
	inline int8_t* get_address_of_m_SByteValue_4() { return &___m_SByteValue_4; }
	inline void set_m_SByteValue_4(int8_t value)
	{
		___m_SByteValue_4 = value;
	}

	inline static int32_t get_offset_of_m_ShortValue_5() { return static_cast<int32_t>(offsetof(PrimitiveValue_t70D8365A1CB2217C466D27D6820362EF4331A0C8, ___m_ShortValue_5)); }
	inline int16_t get_m_ShortValue_5() const { return ___m_ShortValue_5; }
	inline int16_t* get_address_of_m_ShortValue_5() { return &___m_ShortValue_5; }
	inline void set_m_ShortValue_5(int16_t value)
	{
		___m_ShortValue_5 = value;
	}

	inline static int32_t get_offset_of_m_UShortValue_6() { return static_cast<int32_t>(offsetof(PrimitiveValue_t70D8365A1CB2217C466D27D6820362EF4331A0C8, ___m_UShortValue_6)); }
	inline uint16_t get_m_UShortValue_6() const { return ___m_UShortValue_6; }
	inline uint16_t* get_address_of_m_UShortValue_6() { return &___m_UShortValue_6; }
	inline void set_m_UShortValue_6(uint16_t value)
	{
		___m_UShortValue_6 = value;
	}

	inline static int32_t get_offset_of_m_IntValue_7() { return static_cast<int32_t>(offsetof(PrimitiveValue_t70D8365A1CB2217C466D27D6820362EF4331A0C8, ___m_IntValue_7)); }
	inline int32_t get_m_IntValue_7() const { return ___m_IntValue_7; }
	inline int32_t* get_address_of_m_IntValue_7() { return &___m_IntValue_7; }
	inline void set_m_IntValue_7(int32_t value)
	{
		___m_IntValue_7 = value;
	}

	inline static int32_t get_offset_of_m_UIntValue_8() { return static_cast<int32_t>(offsetof(PrimitiveValue_t70D8365A1CB2217C466D27D6820362EF4331A0C8, ___m_UIntValue_8)); }
	inline uint32_t get_m_UIntValue_8() const { return ___m_UIntValue_8; }
	inline uint32_t* get_address_of_m_UIntValue_8() { return &___m_UIntValue_8; }
	inline void set_m_UIntValue_8(uint32_t value)
	{
		___m_UIntValue_8 = value;
	}

	inline static int32_t get_offset_of_m_LongValue_9() { return static_cast<int32_t>(offsetof(PrimitiveValue_t70D8365A1CB2217C466D27D6820362EF4331A0C8, ___m_LongValue_9)); }
	inline int64_t get_m_LongValue_9() const { return ___m_LongValue_9; }
	inline int64_t* get_address_of_m_LongValue_9() { return &___m_LongValue_9; }
	inline void set_m_LongValue_9(int64_t value)
	{
		___m_LongValue_9 = value;
	}

	inline static int32_t get_offset_of_m_ULongValue_10() { return static_cast<int32_t>(offsetof(PrimitiveValue_t70D8365A1CB2217C466D27D6820362EF4331A0C8, ___m_ULongValue_10)); }
	inline uint64_t get_m_ULongValue_10() const { return ___m_ULongValue_10; }
	inline uint64_t* get_address_of_m_ULongValue_10() { return &___m_ULongValue_10; }
	inline void set_m_ULongValue_10(uint64_t value)
	{
		___m_ULongValue_10 = value;
	}

	inline static int32_t get_offset_of_m_FloatValue_11() { return static_cast<int32_t>(offsetof(PrimitiveValue_t70D8365A1CB2217C466D27D6820362EF4331A0C8, ___m_FloatValue_11)); }
	inline float get_m_FloatValue_11() const { return ___m_FloatValue_11; }
	inline float* get_address_of_m_FloatValue_11() { return &___m_FloatValue_11; }
	inline void set_m_FloatValue_11(float value)
	{
		___m_FloatValue_11 = value;
	}

	inline static int32_t get_offset_of_m_DoubleValue_12() { return static_cast<int32_t>(offsetof(PrimitiveValue_t70D8365A1CB2217C466D27D6820362EF4331A0C8, ___m_DoubleValue_12)); }
	inline double get_m_DoubleValue_12() const { return ___m_DoubleValue_12; }
	inline double* get_address_of_m_DoubleValue_12() { return &___m_DoubleValue_12; }
	inline void set_m_DoubleValue_12(double value)
	{
		___m_DoubleValue_12 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.InputSystem.Utilities.PrimitiveValue
struct PrimitiveValue_t70D8365A1CB2217C466D27D6820362EF4331A0C8_marshaled_pinvoke
{
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			int32_t ___m_Type_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___m_Type_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_BoolValue_1_OffsetPadding[4];
			int32_t ___m_BoolValue_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_BoolValue_1_OffsetPadding_forAlignmentOnly[4];
			int32_t ___m_BoolValue_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_CharValue_2_OffsetPadding[4];
			uint8_t ___m_CharValue_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_CharValue_2_OffsetPadding_forAlignmentOnly[4];
			uint8_t ___m_CharValue_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_ByteValue_3_OffsetPadding[4];
			uint8_t ___m_ByteValue_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_ByteValue_3_OffsetPadding_forAlignmentOnly[4];
			uint8_t ___m_ByteValue_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_SByteValue_4_OffsetPadding[4];
			int8_t ___m_SByteValue_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_SByteValue_4_OffsetPadding_forAlignmentOnly[4];
			int8_t ___m_SByteValue_4_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_ShortValue_5_OffsetPadding[4];
			int16_t ___m_ShortValue_5;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_ShortValue_5_OffsetPadding_forAlignmentOnly[4];
			int16_t ___m_ShortValue_5_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_UShortValue_6_OffsetPadding[4];
			uint16_t ___m_UShortValue_6;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_UShortValue_6_OffsetPadding_forAlignmentOnly[4];
			uint16_t ___m_UShortValue_6_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_IntValue_7_OffsetPadding[4];
			int32_t ___m_IntValue_7;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_IntValue_7_OffsetPadding_forAlignmentOnly[4];
			int32_t ___m_IntValue_7_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_UIntValue_8_OffsetPadding[4];
			uint32_t ___m_UIntValue_8;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_UIntValue_8_OffsetPadding_forAlignmentOnly[4];
			uint32_t ___m_UIntValue_8_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_LongValue_9_OffsetPadding[4];
			int64_t ___m_LongValue_9;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_LongValue_9_OffsetPadding_forAlignmentOnly[4];
			int64_t ___m_LongValue_9_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_ULongValue_10_OffsetPadding[4];
			uint64_t ___m_ULongValue_10;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_ULongValue_10_OffsetPadding_forAlignmentOnly[4];
			uint64_t ___m_ULongValue_10_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_FloatValue_11_OffsetPadding[4];
			float ___m_FloatValue_11;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_FloatValue_11_OffsetPadding_forAlignmentOnly[4];
			float ___m_FloatValue_11_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_DoubleValue_12_OffsetPadding[4];
			double ___m_DoubleValue_12;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_DoubleValue_12_OffsetPadding_forAlignmentOnly[4];
			double ___m_DoubleValue_12_forAlignmentOnly;
		};
	};
};
// Native definition for COM marshalling of UnityEngine.InputSystem.Utilities.PrimitiveValue
struct PrimitiveValue_t70D8365A1CB2217C466D27D6820362EF4331A0C8_marshaled_com
{
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			int32_t ___m_Type_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___m_Type_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_BoolValue_1_OffsetPadding[4];
			int32_t ___m_BoolValue_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_BoolValue_1_OffsetPadding_forAlignmentOnly[4];
			int32_t ___m_BoolValue_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_CharValue_2_OffsetPadding[4];
			uint8_t ___m_CharValue_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_CharValue_2_OffsetPadding_forAlignmentOnly[4];
			uint8_t ___m_CharValue_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_ByteValue_3_OffsetPadding[4];
			uint8_t ___m_ByteValue_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_ByteValue_3_OffsetPadding_forAlignmentOnly[4];
			uint8_t ___m_ByteValue_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_SByteValue_4_OffsetPadding[4];
			int8_t ___m_SByteValue_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_SByteValue_4_OffsetPadding_forAlignmentOnly[4];
			int8_t ___m_SByteValue_4_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_ShortValue_5_OffsetPadding[4];
			int16_t ___m_ShortValue_5;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_ShortValue_5_OffsetPadding_forAlignmentOnly[4];
			int16_t ___m_ShortValue_5_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_UShortValue_6_OffsetPadding[4];
			uint16_t ___m_UShortValue_6;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_UShortValue_6_OffsetPadding_forAlignmentOnly[4];
			uint16_t ___m_UShortValue_6_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_IntValue_7_OffsetPadding[4];
			int32_t ___m_IntValue_7;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_IntValue_7_OffsetPadding_forAlignmentOnly[4];
			int32_t ___m_IntValue_7_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_UIntValue_8_OffsetPadding[4];
			uint32_t ___m_UIntValue_8;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_UIntValue_8_OffsetPadding_forAlignmentOnly[4];
			uint32_t ___m_UIntValue_8_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_LongValue_9_OffsetPadding[4];
			int64_t ___m_LongValue_9;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_LongValue_9_OffsetPadding_forAlignmentOnly[4];
			int64_t ___m_LongValue_9_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_ULongValue_10_OffsetPadding[4];
			uint64_t ___m_ULongValue_10;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_ULongValue_10_OffsetPadding_forAlignmentOnly[4];
			uint64_t ___m_ULongValue_10_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_FloatValue_11_OffsetPadding[4];
			float ___m_FloatValue_11;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_FloatValue_11_OffsetPadding_forAlignmentOnly[4];
			float ___m_FloatValue_11_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_DoubleValue_12_OffsetPadding[4];
			double ___m_DoubleValue_12;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_DoubleValue_12_OffsetPadding_forAlignmentOnly[4];
			double ___m_DoubleValue_12_forAlignmentOnly;
		};
	};
};

// UnityEngine.ScriptableObject
struct  ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};

// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A_marshaled_pinvoke : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A_marshaled_com : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_com
{
};

// UnityEngine.Behaviour
struct  Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.InputSystem.InputControl
struct  InputControl_tF71EDA53B03EA6D600257755523682E10F94DDC1  : public RuntimeObject
{
public:
	// UnityEngine.InputSystem.LowLevel.InputStateBlock UnityEngine.InputSystem.InputControl::m_StateBlock
	InputStateBlock_tB9FCBE0F27DDBB11B04B0EC517712C29CC126A0B  ___m_StateBlock_0;
	// UnityEngine.InputSystem.Utilities.InternedString UnityEngine.InputSystem.InputControl::m_Name
	InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  ___m_Name_1;
	// System.String UnityEngine.InputSystem.InputControl::m_Path
	String_t* ___m_Path_2;
	// System.String UnityEngine.InputSystem.InputControl::m_DisplayName
	String_t* ___m_DisplayName_3;
	// System.String UnityEngine.InputSystem.InputControl::m_DisplayNameFromLayout
	String_t* ___m_DisplayNameFromLayout_4;
	// System.String UnityEngine.InputSystem.InputControl::m_ShortDisplayName
	String_t* ___m_ShortDisplayName_5;
	// System.String UnityEngine.InputSystem.InputControl::m_ShortDisplayNameFromLayout
	String_t* ___m_ShortDisplayNameFromLayout_6;
	// UnityEngine.InputSystem.Utilities.InternedString UnityEngine.InputSystem.InputControl::m_Layout
	InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  ___m_Layout_7;
	// UnityEngine.InputSystem.Utilities.InternedString UnityEngine.InputSystem.InputControl::m_Variants
	InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  ___m_Variants_8;
	// UnityEngine.InputSystem.InputDevice UnityEngine.InputSystem.InputControl::m_Device
	InputDevice_t64BD575C54DB522A280D29E47F96E52B79B22D87 * ___m_Device_9;
	// UnityEngine.InputSystem.InputControl UnityEngine.InputSystem.InputControl::m_Parent
	InputControl_tF71EDA53B03EA6D600257755523682E10F94DDC1 * ___m_Parent_10;
	// System.Int32 UnityEngine.InputSystem.InputControl::m_UsageCount
	int32_t ___m_UsageCount_11;
	// System.Int32 UnityEngine.InputSystem.InputControl::m_UsageStartIndex
	int32_t ___m_UsageStartIndex_12;
	// System.Int32 UnityEngine.InputSystem.InputControl::m_AliasCount
	int32_t ___m_AliasCount_13;
	// System.Int32 UnityEngine.InputSystem.InputControl::m_AliasStartIndex
	int32_t ___m_AliasStartIndex_14;
	// System.Int32 UnityEngine.InputSystem.InputControl::m_ChildCount
	int32_t ___m_ChildCount_15;
	// System.Int32 UnityEngine.InputSystem.InputControl::m_ChildStartIndex
	int32_t ___m_ChildStartIndex_16;
	// UnityEngine.InputSystem.InputControl/ControlFlags UnityEngine.InputSystem.InputControl::m_ControlFlags
	int32_t ___m_ControlFlags_17;
	// UnityEngine.InputSystem.Utilities.PrimitiveValue UnityEngine.InputSystem.InputControl::m_DefaultState
	PrimitiveValue_t70D8365A1CB2217C466D27D6820362EF4331A0C8  ___m_DefaultState_18;
	// UnityEngine.InputSystem.Utilities.PrimitiveValue UnityEngine.InputSystem.InputControl::m_MinValue
	PrimitiveValue_t70D8365A1CB2217C466D27D6820362EF4331A0C8  ___m_MinValue_19;
	// UnityEngine.InputSystem.Utilities.PrimitiveValue UnityEngine.InputSystem.InputControl::m_MaxValue
	PrimitiveValue_t70D8365A1CB2217C466D27D6820362EF4331A0C8  ___m_MaxValue_20;

public:
	inline static int32_t get_offset_of_m_StateBlock_0() { return static_cast<int32_t>(offsetof(InputControl_tF71EDA53B03EA6D600257755523682E10F94DDC1, ___m_StateBlock_0)); }
	inline InputStateBlock_tB9FCBE0F27DDBB11B04B0EC517712C29CC126A0B  get_m_StateBlock_0() const { return ___m_StateBlock_0; }
	inline InputStateBlock_tB9FCBE0F27DDBB11B04B0EC517712C29CC126A0B * get_address_of_m_StateBlock_0() { return &___m_StateBlock_0; }
	inline void set_m_StateBlock_0(InputStateBlock_tB9FCBE0F27DDBB11B04B0EC517712C29CC126A0B  value)
	{
		___m_StateBlock_0 = value;
	}

	inline static int32_t get_offset_of_m_Name_1() { return static_cast<int32_t>(offsetof(InputControl_tF71EDA53B03EA6D600257755523682E10F94DDC1, ___m_Name_1)); }
	inline InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  get_m_Name_1() const { return ___m_Name_1; }
	inline InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED * get_address_of_m_Name_1() { return &___m_Name_1; }
	inline void set_m_Name_1(InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  value)
	{
		___m_Name_1 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Name_1))->___m_StringOriginalCase_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Name_1))->___m_StringLowerCase_1), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_Path_2() { return static_cast<int32_t>(offsetof(InputControl_tF71EDA53B03EA6D600257755523682E10F94DDC1, ___m_Path_2)); }
	inline String_t* get_m_Path_2() const { return ___m_Path_2; }
	inline String_t** get_address_of_m_Path_2() { return &___m_Path_2; }
	inline void set_m_Path_2(String_t* value)
	{
		___m_Path_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Path_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_DisplayName_3() { return static_cast<int32_t>(offsetof(InputControl_tF71EDA53B03EA6D600257755523682E10F94DDC1, ___m_DisplayName_3)); }
	inline String_t* get_m_DisplayName_3() const { return ___m_DisplayName_3; }
	inline String_t** get_address_of_m_DisplayName_3() { return &___m_DisplayName_3; }
	inline void set_m_DisplayName_3(String_t* value)
	{
		___m_DisplayName_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_DisplayName_3), (void*)value);
	}

	inline static int32_t get_offset_of_m_DisplayNameFromLayout_4() { return static_cast<int32_t>(offsetof(InputControl_tF71EDA53B03EA6D600257755523682E10F94DDC1, ___m_DisplayNameFromLayout_4)); }
	inline String_t* get_m_DisplayNameFromLayout_4() const { return ___m_DisplayNameFromLayout_4; }
	inline String_t** get_address_of_m_DisplayNameFromLayout_4() { return &___m_DisplayNameFromLayout_4; }
	inline void set_m_DisplayNameFromLayout_4(String_t* value)
	{
		___m_DisplayNameFromLayout_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_DisplayNameFromLayout_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_ShortDisplayName_5() { return static_cast<int32_t>(offsetof(InputControl_tF71EDA53B03EA6D600257755523682E10F94DDC1, ___m_ShortDisplayName_5)); }
	inline String_t* get_m_ShortDisplayName_5() const { return ___m_ShortDisplayName_5; }
	inline String_t** get_address_of_m_ShortDisplayName_5() { return &___m_ShortDisplayName_5; }
	inline void set_m_ShortDisplayName_5(String_t* value)
	{
		___m_ShortDisplayName_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ShortDisplayName_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_ShortDisplayNameFromLayout_6() { return static_cast<int32_t>(offsetof(InputControl_tF71EDA53B03EA6D600257755523682E10F94DDC1, ___m_ShortDisplayNameFromLayout_6)); }
	inline String_t* get_m_ShortDisplayNameFromLayout_6() const { return ___m_ShortDisplayNameFromLayout_6; }
	inline String_t** get_address_of_m_ShortDisplayNameFromLayout_6() { return &___m_ShortDisplayNameFromLayout_6; }
	inline void set_m_ShortDisplayNameFromLayout_6(String_t* value)
	{
		___m_ShortDisplayNameFromLayout_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ShortDisplayNameFromLayout_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_Layout_7() { return static_cast<int32_t>(offsetof(InputControl_tF71EDA53B03EA6D600257755523682E10F94DDC1, ___m_Layout_7)); }
	inline InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  get_m_Layout_7() const { return ___m_Layout_7; }
	inline InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED * get_address_of_m_Layout_7() { return &___m_Layout_7; }
	inline void set_m_Layout_7(InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  value)
	{
		___m_Layout_7 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Layout_7))->___m_StringOriginalCase_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Layout_7))->___m_StringLowerCase_1), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_Variants_8() { return static_cast<int32_t>(offsetof(InputControl_tF71EDA53B03EA6D600257755523682E10F94DDC1, ___m_Variants_8)); }
	inline InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  get_m_Variants_8() const { return ___m_Variants_8; }
	inline InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED * get_address_of_m_Variants_8() { return &___m_Variants_8; }
	inline void set_m_Variants_8(InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  value)
	{
		___m_Variants_8 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Variants_8))->___m_StringOriginalCase_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Variants_8))->___m_StringLowerCase_1), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_Device_9() { return static_cast<int32_t>(offsetof(InputControl_tF71EDA53B03EA6D600257755523682E10F94DDC1, ___m_Device_9)); }
	inline InputDevice_t64BD575C54DB522A280D29E47F96E52B79B22D87 * get_m_Device_9() const { return ___m_Device_9; }
	inline InputDevice_t64BD575C54DB522A280D29E47F96E52B79B22D87 ** get_address_of_m_Device_9() { return &___m_Device_9; }
	inline void set_m_Device_9(InputDevice_t64BD575C54DB522A280D29E47F96E52B79B22D87 * value)
	{
		___m_Device_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Device_9), (void*)value);
	}

	inline static int32_t get_offset_of_m_Parent_10() { return static_cast<int32_t>(offsetof(InputControl_tF71EDA53B03EA6D600257755523682E10F94DDC1, ___m_Parent_10)); }
	inline InputControl_tF71EDA53B03EA6D600257755523682E10F94DDC1 * get_m_Parent_10() const { return ___m_Parent_10; }
	inline InputControl_tF71EDA53B03EA6D600257755523682E10F94DDC1 ** get_address_of_m_Parent_10() { return &___m_Parent_10; }
	inline void set_m_Parent_10(InputControl_tF71EDA53B03EA6D600257755523682E10F94DDC1 * value)
	{
		___m_Parent_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Parent_10), (void*)value);
	}

	inline static int32_t get_offset_of_m_UsageCount_11() { return static_cast<int32_t>(offsetof(InputControl_tF71EDA53B03EA6D600257755523682E10F94DDC1, ___m_UsageCount_11)); }
	inline int32_t get_m_UsageCount_11() const { return ___m_UsageCount_11; }
	inline int32_t* get_address_of_m_UsageCount_11() { return &___m_UsageCount_11; }
	inline void set_m_UsageCount_11(int32_t value)
	{
		___m_UsageCount_11 = value;
	}

	inline static int32_t get_offset_of_m_UsageStartIndex_12() { return static_cast<int32_t>(offsetof(InputControl_tF71EDA53B03EA6D600257755523682E10F94DDC1, ___m_UsageStartIndex_12)); }
	inline int32_t get_m_UsageStartIndex_12() const { return ___m_UsageStartIndex_12; }
	inline int32_t* get_address_of_m_UsageStartIndex_12() { return &___m_UsageStartIndex_12; }
	inline void set_m_UsageStartIndex_12(int32_t value)
	{
		___m_UsageStartIndex_12 = value;
	}

	inline static int32_t get_offset_of_m_AliasCount_13() { return static_cast<int32_t>(offsetof(InputControl_tF71EDA53B03EA6D600257755523682E10F94DDC1, ___m_AliasCount_13)); }
	inline int32_t get_m_AliasCount_13() const { return ___m_AliasCount_13; }
	inline int32_t* get_address_of_m_AliasCount_13() { return &___m_AliasCount_13; }
	inline void set_m_AliasCount_13(int32_t value)
	{
		___m_AliasCount_13 = value;
	}

	inline static int32_t get_offset_of_m_AliasStartIndex_14() { return static_cast<int32_t>(offsetof(InputControl_tF71EDA53B03EA6D600257755523682E10F94DDC1, ___m_AliasStartIndex_14)); }
	inline int32_t get_m_AliasStartIndex_14() const { return ___m_AliasStartIndex_14; }
	inline int32_t* get_address_of_m_AliasStartIndex_14() { return &___m_AliasStartIndex_14; }
	inline void set_m_AliasStartIndex_14(int32_t value)
	{
		___m_AliasStartIndex_14 = value;
	}

	inline static int32_t get_offset_of_m_ChildCount_15() { return static_cast<int32_t>(offsetof(InputControl_tF71EDA53B03EA6D600257755523682E10F94DDC1, ___m_ChildCount_15)); }
	inline int32_t get_m_ChildCount_15() const { return ___m_ChildCount_15; }
	inline int32_t* get_address_of_m_ChildCount_15() { return &___m_ChildCount_15; }
	inline void set_m_ChildCount_15(int32_t value)
	{
		___m_ChildCount_15 = value;
	}

	inline static int32_t get_offset_of_m_ChildStartIndex_16() { return static_cast<int32_t>(offsetof(InputControl_tF71EDA53B03EA6D600257755523682E10F94DDC1, ___m_ChildStartIndex_16)); }
	inline int32_t get_m_ChildStartIndex_16() const { return ___m_ChildStartIndex_16; }
	inline int32_t* get_address_of_m_ChildStartIndex_16() { return &___m_ChildStartIndex_16; }
	inline void set_m_ChildStartIndex_16(int32_t value)
	{
		___m_ChildStartIndex_16 = value;
	}

	inline static int32_t get_offset_of_m_ControlFlags_17() { return static_cast<int32_t>(offsetof(InputControl_tF71EDA53B03EA6D600257755523682E10F94DDC1, ___m_ControlFlags_17)); }
	inline int32_t get_m_ControlFlags_17() const { return ___m_ControlFlags_17; }
	inline int32_t* get_address_of_m_ControlFlags_17() { return &___m_ControlFlags_17; }
	inline void set_m_ControlFlags_17(int32_t value)
	{
		___m_ControlFlags_17 = value;
	}

	inline static int32_t get_offset_of_m_DefaultState_18() { return static_cast<int32_t>(offsetof(InputControl_tF71EDA53B03EA6D600257755523682E10F94DDC1, ___m_DefaultState_18)); }
	inline PrimitiveValue_t70D8365A1CB2217C466D27D6820362EF4331A0C8  get_m_DefaultState_18() const { return ___m_DefaultState_18; }
	inline PrimitiveValue_t70D8365A1CB2217C466D27D6820362EF4331A0C8 * get_address_of_m_DefaultState_18() { return &___m_DefaultState_18; }
	inline void set_m_DefaultState_18(PrimitiveValue_t70D8365A1CB2217C466D27D6820362EF4331A0C8  value)
	{
		___m_DefaultState_18 = value;
	}

	inline static int32_t get_offset_of_m_MinValue_19() { return static_cast<int32_t>(offsetof(InputControl_tF71EDA53B03EA6D600257755523682E10F94DDC1, ___m_MinValue_19)); }
	inline PrimitiveValue_t70D8365A1CB2217C466D27D6820362EF4331A0C8  get_m_MinValue_19() const { return ___m_MinValue_19; }
	inline PrimitiveValue_t70D8365A1CB2217C466D27D6820362EF4331A0C8 * get_address_of_m_MinValue_19() { return &___m_MinValue_19; }
	inline void set_m_MinValue_19(PrimitiveValue_t70D8365A1CB2217C466D27D6820362EF4331A0C8  value)
	{
		___m_MinValue_19 = value;
	}

	inline static int32_t get_offset_of_m_MaxValue_20() { return static_cast<int32_t>(offsetof(InputControl_tF71EDA53B03EA6D600257755523682E10F94DDC1, ___m_MaxValue_20)); }
	inline PrimitiveValue_t70D8365A1CB2217C466D27D6820362EF4331A0C8  get_m_MaxValue_20() const { return ___m_MaxValue_20; }
	inline PrimitiveValue_t70D8365A1CB2217C466D27D6820362EF4331A0C8 * get_address_of_m_MaxValue_20() { return &___m_MaxValue_20; }
	inline void set_m_MaxValue_20(PrimitiveValue_t70D8365A1CB2217C466D27D6820362EF4331A0C8  value)
	{
		___m_MaxValue_20 = value;
	}
};


// Readme
struct  Readme_tD58D22679F01C9AD66EA323B2DE68BFF6DBDCEE7  : public ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A
{
public:
	// UnityEngine.Texture2D Readme::icon
	Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * ___icon_4;
	// System.String Readme::title
	String_t* ___title_5;
	// Readme/Section[] Readme::sections
	SectionU5BU5D_t013C5C59593F9D61D69A33E91B8C1450419ED659* ___sections_6;
	// System.Boolean Readme::loadedLayout
	bool ___loadedLayout_7;

public:
	inline static int32_t get_offset_of_icon_4() { return static_cast<int32_t>(offsetof(Readme_tD58D22679F01C9AD66EA323B2DE68BFF6DBDCEE7, ___icon_4)); }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * get_icon_4() const { return ___icon_4; }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF ** get_address_of_icon_4() { return &___icon_4; }
	inline void set_icon_4(Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * value)
	{
		___icon_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___icon_4), (void*)value);
	}

	inline static int32_t get_offset_of_title_5() { return static_cast<int32_t>(offsetof(Readme_tD58D22679F01C9AD66EA323B2DE68BFF6DBDCEE7, ___title_5)); }
	inline String_t* get_title_5() const { return ___title_5; }
	inline String_t** get_address_of_title_5() { return &___title_5; }
	inline void set_title_5(String_t* value)
	{
		___title_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___title_5), (void*)value);
	}

	inline static int32_t get_offset_of_sections_6() { return static_cast<int32_t>(offsetof(Readme_tD58D22679F01C9AD66EA323B2DE68BFF6DBDCEE7, ___sections_6)); }
	inline SectionU5BU5D_t013C5C59593F9D61D69A33E91B8C1450419ED659* get_sections_6() const { return ___sections_6; }
	inline SectionU5BU5D_t013C5C59593F9D61D69A33E91B8C1450419ED659** get_address_of_sections_6() { return &___sections_6; }
	inline void set_sections_6(SectionU5BU5D_t013C5C59593F9D61D69A33E91B8C1450419ED659* value)
	{
		___sections_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___sections_6), (void*)value);
	}

	inline static int32_t get_offset_of_loadedLayout_7() { return static_cast<int32_t>(offsetof(Readme_tD58D22679F01C9AD66EA323B2DE68BFF6DBDCEE7, ___loadedLayout_7)); }
	inline bool get_loadedLayout_7() const { return ___loadedLayout_7; }
	inline bool* get_address_of_loadedLayout_7() { return &___loadedLayout_7; }
	inline void set_loadedLayout_7(bool value)
	{
		___loadedLayout_7 = value;
	}
};


// UnityEngine.XR.WindowsMR.WindowsMRSettings
struct  WindowsMRSettings_tD6F827D24F45274A0B5B38D88702B85BC400DA81  : public ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A
{
public:
	// UnityEngine.XR.WindowsMR.WindowsMRSettings/DepthBufferOption UnityEngine.XR.WindowsMR.WindowsMRSettings::DepthBufferFormat
	int32_t ___DepthBufferFormat_4;
	// System.Boolean UnityEngine.XR.WindowsMR.WindowsMRSettings::UseSharedDepthBuffer
	bool ___UseSharedDepthBuffer_5;

public:
	inline static int32_t get_offset_of_DepthBufferFormat_4() { return static_cast<int32_t>(offsetof(WindowsMRSettings_tD6F827D24F45274A0B5B38D88702B85BC400DA81, ___DepthBufferFormat_4)); }
	inline int32_t get_DepthBufferFormat_4() const { return ___DepthBufferFormat_4; }
	inline int32_t* get_address_of_DepthBufferFormat_4() { return &___DepthBufferFormat_4; }
	inline void set_DepthBufferFormat_4(int32_t value)
	{
		___DepthBufferFormat_4 = value;
	}

	inline static int32_t get_offset_of_UseSharedDepthBuffer_5() { return static_cast<int32_t>(offsetof(WindowsMRSettings_tD6F827D24F45274A0B5B38D88702B85BC400DA81, ___UseSharedDepthBuffer_5)); }
	inline bool get_UseSharedDepthBuffer_5() const { return ___UseSharedDepthBuffer_5; }
	inline bool* get_address_of_UseSharedDepthBuffer_5() { return &___UseSharedDepthBuffer_5; }
	inline void set_UseSharedDepthBuffer_5(bool value)
	{
		___UseSharedDepthBuffer_5 = value;
	}
};

struct WindowsMRSettings_tD6F827D24F45274A0B5B38D88702B85BC400DA81_StaticFields
{
public:
	// UnityEngine.XR.WindowsMR.WindowsMRSettings UnityEngine.XR.WindowsMR.WindowsMRSettings::s_Settings
	WindowsMRSettings_tD6F827D24F45274A0B5B38D88702B85BC400DA81 * ___s_Settings_6;

public:
	inline static int32_t get_offset_of_s_Settings_6() { return static_cast<int32_t>(offsetof(WindowsMRSettings_tD6F827D24F45274A0B5B38D88702B85BC400DA81_StaticFields, ___s_Settings_6)); }
	inline WindowsMRSettings_tD6F827D24F45274A0B5B38D88702B85BC400DA81 * get_s_Settings_6() const { return ___s_Settings_6; }
	inline WindowsMRSettings_tD6F827D24F45274A0B5B38D88702B85BC400DA81 ** get_address_of_s_Settings_6() { return &___s_Settings_6; }
	inline void set_s_Settings_6(WindowsMRSettings_tD6F827D24F45274A0B5B38D88702B85BC400DA81 * value)
	{
		___s_Settings_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Settings_6), (void*)value);
	}
};


// UnityEngine.XR.Management.XRLoader
struct  XRLoader_tE37B92C6B9CDD944DDF7AFF5704E9EB342D62F6B  : public ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A
{
public:

public:
};


// UnityEngine.InputSystem.InputDevice
struct  InputDevice_t64BD575C54DB522A280D29E47F96E52B79B22D87  : public InputControl_tF71EDA53B03EA6D600257755523682E10F94DDC1
{
public:
	// UnityEngine.InputSystem.InputDevice/DeviceFlags UnityEngine.InputSystem.InputDevice::m_DeviceFlags
	int32_t ___m_DeviceFlags_24;
	// System.Int32 UnityEngine.InputSystem.InputDevice::m_DeviceId
	int32_t ___m_DeviceId_25;
	// System.Int32 UnityEngine.InputSystem.InputDevice::m_ParticipantId
	int32_t ___m_ParticipantId_26;
	// System.Int32 UnityEngine.InputSystem.InputDevice::m_DeviceIndex
	int32_t ___m_DeviceIndex_27;
	// UnityEngine.InputSystem.Layouts.InputDeviceDescription UnityEngine.InputSystem.InputDevice::m_Description
	InputDeviceDescription_tBE78411023CFED0A42110A1357D65BD6E5EDBB58  ___m_Description_28;
	// System.Double UnityEngine.InputSystem.InputDevice::m_LastUpdateTimeInternal
	double ___m_LastUpdateTimeInternal_29;
	// System.UInt32 UnityEngine.InputSystem.InputDevice::m_CurrentUpdateStepCount
	uint32_t ___m_CurrentUpdateStepCount_30;
	// UnityEngine.InputSystem.Utilities.InternedString[] UnityEngine.InputSystem.InputDevice::m_AliasesForEachControl
	InternedStringU5BU5D_t1EAE8433C8EF7C76894673801EE0EE9F6833E9A4* ___m_AliasesForEachControl_31;
	// UnityEngine.InputSystem.Utilities.InternedString[] UnityEngine.InputSystem.InputDevice::m_UsagesForEachControl
	InternedStringU5BU5D_t1EAE8433C8EF7C76894673801EE0EE9F6833E9A4* ___m_UsagesForEachControl_32;
	// UnityEngine.InputSystem.InputControl[] UnityEngine.InputSystem.InputDevice::m_UsageToControl
	InputControlU5BU5D_tB874FECA56E2B08D3280F4174B988EA155E99680* ___m_UsageToControl_33;
	// UnityEngine.InputSystem.InputControl[] UnityEngine.InputSystem.InputDevice::m_ChildrenForEachControl
	InputControlU5BU5D_tB874FECA56E2B08D3280F4174B988EA155E99680* ___m_ChildrenForEachControl_34;
	// System.UInt32[] UnityEngine.InputSystem.InputDevice::m_StateOffsetToControlMap
	UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF* ___m_StateOffsetToControlMap_35;

public:
	inline static int32_t get_offset_of_m_DeviceFlags_24() { return static_cast<int32_t>(offsetof(InputDevice_t64BD575C54DB522A280D29E47F96E52B79B22D87, ___m_DeviceFlags_24)); }
	inline int32_t get_m_DeviceFlags_24() const { return ___m_DeviceFlags_24; }
	inline int32_t* get_address_of_m_DeviceFlags_24() { return &___m_DeviceFlags_24; }
	inline void set_m_DeviceFlags_24(int32_t value)
	{
		___m_DeviceFlags_24 = value;
	}

	inline static int32_t get_offset_of_m_DeviceId_25() { return static_cast<int32_t>(offsetof(InputDevice_t64BD575C54DB522A280D29E47F96E52B79B22D87, ___m_DeviceId_25)); }
	inline int32_t get_m_DeviceId_25() const { return ___m_DeviceId_25; }
	inline int32_t* get_address_of_m_DeviceId_25() { return &___m_DeviceId_25; }
	inline void set_m_DeviceId_25(int32_t value)
	{
		___m_DeviceId_25 = value;
	}

	inline static int32_t get_offset_of_m_ParticipantId_26() { return static_cast<int32_t>(offsetof(InputDevice_t64BD575C54DB522A280D29E47F96E52B79B22D87, ___m_ParticipantId_26)); }
	inline int32_t get_m_ParticipantId_26() const { return ___m_ParticipantId_26; }
	inline int32_t* get_address_of_m_ParticipantId_26() { return &___m_ParticipantId_26; }
	inline void set_m_ParticipantId_26(int32_t value)
	{
		___m_ParticipantId_26 = value;
	}

	inline static int32_t get_offset_of_m_DeviceIndex_27() { return static_cast<int32_t>(offsetof(InputDevice_t64BD575C54DB522A280D29E47F96E52B79B22D87, ___m_DeviceIndex_27)); }
	inline int32_t get_m_DeviceIndex_27() const { return ___m_DeviceIndex_27; }
	inline int32_t* get_address_of_m_DeviceIndex_27() { return &___m_DeviceIndex_27; }
	inline void set_m_DeviceIndex_27(int32_t value)
	{
		___m_DeviceIndex_27 = value;
	}

	inline static int32_t get_offset_of_m_Description_28() { return static_cast<int32_t>(offsetof(InputDevice_t64BD575C54DB522A280D29E47F96E52B79B22D87, ___m_Description_28)); }
	inline InputDeviceDescription_tBE78411023CFED0A42110A1357D65BD6E5EDBB58  get_m_Description_28() const { return ___m_Description_28; }
	inline InputDeviceDescription_tBE78411023CFED0A42110A1357D65BD6E5EDBB58 * get_address_of_m_Description_28() { return &___m_Description_28; }
	inline void set_m_Description_28(InputDeviceDescription_tBE78411023CFED0A42110A1357D65BD6E5EDBB58  value)
	{
		___m_Description_28 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Description_28))->___m_InterfaceName_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Description_28))->___m_DeviceClass_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Description_28))->___m_Manufacturer_2), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Description_28))->___m_Product_3), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Description_28))->___m_Serial_4), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Description_28))->___m_Version_5), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Description_28))->___m_Capabilities_6), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_LastUpdateTimeInternal_29() { return static_cast<int32_t>(offsetof(InputDevice_t64BD575C54DB522A280D29E47F96E52B79B22D87, ___m_LastUpdateTimeInternal_29)); }
	inline double get_m_LastUpdateTimeInternal_29() const { return ___m_LastUpdateTimeInternal_29; }
	inline double* get_address_of_m_LastUpdateTimeInternal_29() { return &___m_LastUpdateTimeInternal_29; }
	inline void set_m_LastUpdateTimeInternal_29(double value)
	{
		___m_LastUpdateTimeInternal_29 = value;
	}

	inline static int32_t get_offset_of_m_CurrentUpdateStepCount_30() { return static_cast<int32_t>(offsetof(InputDevice_t64BD575C54DB522A280D29E47F96E52B79B22D87, ___m_CurrentUpdateStepCount_30)); }
	inline uint32_t get_m_CurrentUpdateStepCount_30() const { return ___m_CurrentUpdateStepCount_30; }
	inline uint32_t* get_address_of_m_CurrentUpdateStepCount_30() { return &___m_CurrentUpdateStepCount_30; }
	inline void set_m_CurrentUpdateStepCount_30(uint32_t value)
	{
		___m_CurrentUpdateStepCount_30 = value;
	}

	inline static int32_t get_offset_of_m_AliasesForEachControl_31() { return static_cast<int32_t>(offsetof(InputDevice_t64BD575C54DB522A280D29E47F96E52B79B22D87, ___m_AliasesForEachControl_31)); }
	inline InternedStringU5BU5D_t1EAE8433C8EF7C76894673801EE0EE9F6833E9A4* get_m_AliasesForEachControl_31() const { return ___m_AliasesForEachControl_31; }
	inline InternedStringU5BU5D_t1EAE8433C8EF7C76894673801EE0EE9F6833E9A4** get_address_of_m_AliasesForEachControl_31() { return &___m_AliasesForEachControl_31; }
	inline void set_m_AliasesForEachControl_31(InternedStringU5BU5D_t1EAE8433C8EF7C76894673801EE0EE9F6833E9A4* value)
	{
		___m_AliasesForEachControl_31 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_AliasesForEachControl_31), (void*)value);
	}

	inline static int32_t get_offset_of_m_UsagesForEachControl_32() { return static_cast<int32_t>(offsetof(InputDevice_t64BD575C54DB522A280D29E47F96E52B79B22D87, ___m_UsagesForEachControl_32)); }
	inline InternedStringU5BU5D_t1EAE8433C8EF7C76894673801EE0EE9F6833E9A4* get_m_UsagesForEachControl_32() const { return ___m_UsagesForEachControl_32; }
	inline InternedStringU5BU5D_t1EAE8433C8EF7C76894673801EE0EE9F6833E9A4** get_address_of_m_UsagesForEachControl_32() { return &___m_UsagesForEachControl_32; }
	inline void set_m_UsagesForEachControl_32(InternedStringU5BU5D_t1EAE8433C8EF7C76894673801EE0EE9F6833E9A4* value)
	{
		___m_UsagesForEachControl_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_UsagesForEachControl_32), (void*)value);
	}

	inline static int32_t get_offset_of_m_UsageToControl_33() { return static_cast<int32_t>(offsetof(InputDevice_t64BD575C54DB522A280D29E47F96E52B79B22D87, ___m_UsageToControl_33)); }
	inline InputControlU5BU5D_tB874FECA56E2B08D3280F4174B988EA155E99680* get_m_UsageToControl_33() const { return ___m_UsageToControl_33; }
	inline InputControlU5BU5D_tB874FECA56E2B08D3280F4174B988EA155E99680** get_address_of_m_UsageToControl_33() { return &___m_UsageToControl_33; }
	inline void set_m_UsageToControl_33(InputControlU5BU5D_tB874FECA56E2B08D3280F4174B988EA155E99680* value)
	{
		___m_UsageToControl_33 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_UsageToControl_33), (void*)value);
	}

	inline static int32_t get_offset_of_m_ChildrenForEachControl_34() { return static_cast<int32_t>(offsetof(InputDevice_t64BD575C54DB522A280D29E47F96E52B79B22D87, ___m_ChildrenForEachControl_34)); }
	inline InputControlU5BU5D_tB874FECA56E2B08D3280F4174B988EA155E99680* get_m_ChildrenForEachControl_34() const { return ___m_ChildrenForEachControl_34; }
	inline InputControlU5BU5D_tB874FECA56E2B08D3280F4174B988EA155E99680** get_address_of_m_ChildrenForEachControl_34() { return &___m_ChildrenForEachControl_34; }
	inline void set_m_ChildrenForEachControl_34(InputControlU5BU5D_tB874FECA56E2B08D3280F4174B988EA155E99680* value)
	{
		___m_ChildrenForEachControl_34 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ChildrenForEachControl_34), (void*)value);
	}

	inline static int32_t get_offset_of_m_StateOffsetToControlMap_35() { return static_cast<int32_t>(offsetof(InputDevice_t64BD575C54DB522A280D29E47F96E52B79B22D87, ___m_StateOffsetToControlMap_35)); }
	inline UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF* get_m_StateOffsetToControlMap_35() const { return ___m_StateOffsetToControlMap_35; }
	inline UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF** get_address_of_m_StateOffsetToControlMap_35() { return &___m_StateOffsetToControlMap_35; }
	inline void set_m_StateOffsetToControlMap_35(UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF* value)
	{
		___m_StateOffsetToControlMap_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_StateOffsetToControlMap_35), (void*)value);
	}
};


// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};


// UnityEngine.XR.Management.XRLoaderHelper
struct  XRLoaderHelper_t37A55C343AC31D25BB3EBD203DABFA357F51C013  : public XRLoader_tE37B92C6B9CDD944DDF7AFF5704E9EB342D62F6B
{
public:
	// System.Collections.Generic.Dictionary`2<System.Type,UnityEngine.ISubsystem> UnityEngine.XR.Management.XRLoaderHelper::m_SubsystemInstanceMap
	Dictionary_2_t4F3B5B526335E16355EDBC766052AEAB07B1777B * ___m_SubsystemInstanceMap_4;

public:
	inline static int32_t get_offset_of_m_SubsystemInstanceMap_4() { return static_cast<int32_t>(offsetof(XRLoaderHelper_t37A55C343AC31D25BB3EBD203DABFA357F51C013, ___m_SubsystemInstanceMap_4)); }
	inline Dictionary_2_t4F3B5B526335E16355EDBC766052AEAB07B1777B * get_m_SubsystemInstanceMap_4() const { return ___m_SubsystemInstanceMap_4; }
	inline Dictionary_2_t4F3B5B526335E16355EDBC766052AEAB07B1777B ** get_address_of_m_SubsystemInstanceMap_4() { return &___m_SubsystemInstanceMap_4; }
	inline void set_m_SubsystemInstanceMap_4(Dictionary_2_t4F3B5B526335E16355EDBC766052AEAB07B1777B * value)
	{
		___m_SubsystemInstanceMap_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SubsystemInstanceMap_4), (void*)value);
	}
};


// AIController
struct  AIController_t79B964E91146AC59C27CD306018BA3E368831F34  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Vector3 AIController::vector1
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___vector1_4;
	// UnityEngine.Vector3 AIController::vector2
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___vector2_5;
	// UnityEngine.Quaternion AIController::rot
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___rot_6;
	// UnityEngine.Transform AIController::carTrans
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___carTrans_7;
	// UnityEngine.GameObject[] AIController::cars
	GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* ___cars_8;
	// System.Boolean AIController::isFarEnough
	bool ___isFarEnough_9;
	// System.Boolean AIController::onlyFirstVector
	bool ___onlyFirstVector_10;
	// System.Single AIController::distance
	float ___distance_11;
	// System.Single AIController::maxDistance
	float ___maxDistance_12;
	// System.Int32 AIController::timer
	int32_t ___timer_13;

public:
	inline static int32_t get_offset_of_vector1_4() { return static_cast<int32_t>(offsetof(AIController_t79B964E91146AC59C27CD306018BA3E368831F34, ___vector1_4)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_vector1_4() const { return ___vector1_4; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_vector1_4() { return &___vector1_4; }
	inline void set_vector1_4(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___vector1_4 = value;
	}

	inline static int32_t get_offset_of_vector2_5() { return static_cast<int32_t>(offsetof(AIController_t79B964E91146AC59C27CD306018BA3E368831F34, ___vector2_5)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_vector2_5() const { return ___vector2_5; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_vector2_5() { return &___vector2_5; }
	inline void set_vector2_5(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___vector2_5 = value;
	}

	inline static int32_t get_offset_of_rot_6() { return static_cast<int32_t>(offsetof(AIController_t79B964E91146AC59C27CD306018BA3E368831F34, ___rot_6)); }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  get_rot_6() const { return ___rot_6; }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * get_address_of_rot_6() { return &___rot_6; }
	inline void set_rot_6(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  value)
	{
		___rot_6 = value;
	}

	inline static int32_t get_offset_of_carTrans_7() { return static_cast<int32_t>(offsetof(AIController_t79B964E91146AC59C27CD306018BA3E368831F34, ___carTrans_7)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_carTrans_7() const { return ___carTrans_7; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_carTrans_7() { return &___carTrans_7; }
	inline void set_carTrans_7(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___carTrans_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___carTrans_7), (void*)value);
	}

	inline static int32_t get_offset_of_cars_8() { return static_cast<int32_t>(offsetof(AIController_t79B964E91146AC59C27CD306018BA3E368831F34, ___cars_8)); }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* get_cars_8() const { return ___cars_8; }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642** get_address_of_cars_8() { return &___cars_8; }
	inline void set_cars_8(GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* value)
	{
		___cars_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cars_8), (void*)value);
	}

	inline static int32_t get_offset_of_isFarEnough_9() { return static_cast<int32_t>(offsetof(AIController_t79B964E91146AC59C27CD306018BA3E368831F34, ___isFarEnough_9)); }
	inline bool get_isFarEnough_9() const { return ___isFarEnough_9; }
	inline bool* get_address_of_isFarEnough_9() { return &___isFarEnough_9; }
	inline void set_isFarEnough_9(bool value)
	{
		___isFarEnough_9 = value;
	}

	inline static int32_t get_offset_of_onlyFirstVector_10() { return static_cast<int32_t>(offsetof(AIController_t79B964E91146AC59C27CD306018BA3E368831F34, ___onlyFirstVector_10)); }
	inline bool get_onlyFirstVector_10() const { return ___onlyFirstVector_10; }
	inline bool* get_address_of_onlyFirstVector_10() { return &___onlyFirstVector_10; }
	inline void set_onlyFirstVector_10(bool value)
	{
		___onlyFirstVector_10 = value;
	}

	inline static int32_t get_offset_of_distance_11() { return static_cast<int32_t>(offsetof(AIController_t79B964E91146AC59C27CD306018BA3E368831F34, ___distance_11)); }
	inline float get_distance_11() const { return ___distance_11; }
	inline float* get_address_of_distance_11() { return &___distance_11; }
	inline void set_distance_11(float value)
	{
		___distance_11 = value;
	}

	inline static int32_t get_offset_of_maxDistance_12() { return static_cast<int32_t>(offsetof(AIController_t79B964E91146AC59C27CD306018BA3E368831F34, ___maxDistance_12)); }
	inline float get_maxDistance_12() const { return ___maxDistance_12; }
	inline float* get_address_of_maxDistance_12() { return &___maxDistance_12; }
	inline void set_maxDistance_12(float value)
	{
		___maxDistance_12 = value;
	}

	inline static int32_t get_offset_of_timer_13() { return static_cast<int32_t>(offsetof(AIController_t79B964E91146AC59C27CD306018BA3E368831F34, ___timer_13)); }
	inline int32_t get_timer_13() const { return ___timer_13; }
	inline int32_t* get_address_of_timer_13() { return &___timer_13; }
	inline void set_timer_13(int32_t value)
	{
		___timer_13 = value;
	}
};


// AISelector
struct  AISelector_t0C5CD94B22748E54AC7860C6E3AE2EB3EAC362A6  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// TrafficLight AISelector::trafficLight
	TrafficLight_tD55F68E16E18C0A5F32B506715F5BE0989084ABD * ___trafficLight_4;
	// System.Boolean AISelector::isLast
	bool ___isLast_5;

public:
	inline static int32_t get_offset_of_trafficLight_4() { return static_cast<int32_t>(offsetof(AISelector_t0C5CD94B22748E54AC7860C6E3AE2EB3EAC362A6, ___trafficLight_4)); }
	inline TrafficLight_tD55F68E16E18C0A5F32B506715F5BE0989084ABD * get_trafficLight_4() const { return ___trafficLight_4; }
	inline TrafficLight_tD55F68E16E18C0A5F32B506715F5BE0989084ABD ** get_address_of_trafficLight_4() { return &___trafficLight_4; }
	inline void set_trafficLight_4(TrafficLight_tD55F68E16E18C0A5F32B506715F5BE0989084ABD * value)
	{
		___trafficLight_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___trafficLight_4), (void*)value);
	}

	inline static int32_t get_offset_of_isLast_5() { return static_cast<int32_t>(offsetof(AISelector_t0C5CD94B22748E54AC7860C6E3AE2EB3EAC362A6, ___isLast_5)); }
	inline bool get_isLast_5() const { return ___isLast_5; }
	inline bool* get_address_of_isLast_5() { return &___isLast_5; }
	inline void set_isLast_5(bool value)
	{
		___isLast_5 = value;
	}
};


// BasicRigidBodyPush
struct  BasicRigidBodyPush_t9E4C23843BA4437AF06C87D968F0A0BE0B41C714  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.LayerMask BasicRigidBodyPush::pushLayers
	LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  ___pushLayers_4;
	// System.Boolean BasicRigidBodyPush::canPush
	bool ___canPush_5;
	// System.Single BasicRigidBodyPush::strength
	float ___strength_6;

public:
	inline static int32_t get_offset_of_pushLayers_4() { return static_cast<int32_t>(offsetof(BasicRigidBodyPush_t9E4C23843BA4437AF06C87D968F0A0BE0B41C714, ___pushLayers_4)); }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  get_pushLayers_4() const { return ___pushLayers_4; }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8 * get_address_of_pushLayers_4() { return &___pushLayers_4; }
	inline void set_pushLayers_4(LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  value)
	{
		___pushLayers_4 = value;
	}

	inline static int32_t get_offset_of_canPush_5() { return static_cast<int32_t>(offsetof(BasicRigidBodyPush_t9E4C23843BA4437AF06C87D968F0A0BE0B41C714, ___canPush_5)); }
	inline bool get_canPush_5() const { return ___canPush_5; }
	inline bool* get_address_of_canPush_5() { return &___canPush_5; }
	inline void set_canPush_5(bool value)
	{
		___canPush_5 = value;
	}

	inline static int32_t get_offset_of_strength_6() { return static_cast<int32_t>(offsetof(BasicRigidBodyPush_t9E4C23843BA4437AF06C87D968F0A0BE0B41C714, ___strength_6)); }
	inline float get_strength_6() const { return ___strength_6; }
	inline float* get_address_of_strength_6() { return &___strength_6; }
	inline void set_strength_6(float value)
	{
		___strength_6 = value;
	}
};


// CameraFollow
struct  CameraFollow_tC9B62E254DA1376073E7B793597F9D6CD2A82DF8  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Transform CameraFollow::carTransform
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___carTransform_4;
	// System.Single CameraFollow::followSpeed
	float ___followSpeed_5;
	// System.Single CameraFollow::lookSpeed
	float ___lookSpeed_6;
	// UnityEngine.Vector3 CameraFollow::initialCameraPosition
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___initialCameraPosition_7;
	// UnityEngine.Vector3 CameraFollow::initialCarPosition
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___initialCarPosition_8;
	// UnityEngine.Vector3 CameraFollow::absoluteInitCameraPosition
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___absoluteInitCameraPosition_9;

public:
	inline static int32_t get_offset_of_carTransform_4() { return static_cast<int32_t>(offsetof(CameraFollow_tC9B62E254DA1376073E7B793597F9D6CD2A82DF8, ___carTransform_4)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_carTransform_4() const { return ___carTransform_4; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_carTransform_4() { return &___carTransform_4; }
	inline void set_carTransform_4(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___carTransform_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___carTransform_4), (void*)value);
	}

	inline static int32_t get_offset_of_followSpeed_5() { return static_cast<int32_t>(offsetof(CameraFollow_tC9B62E254DA1376073E7B793597F9D6CD2A82DF8, ___followSpeed_5)); }
	inline float get_followSpeed_5() const { return ___followSpeed_5; }
	inline float* get_address_of_followSpeed_5() { return &___followSpeed_5; }
	inline void set_followSpeed_5(float value)
	{
		___followSpeed_5 = value;
	}

	inline static int32_t get_offset_of_lookSpeed_6() { return static_cast<int32_t>(offsetof(CameraFollow_tC9B62E254DA1376073E7B793597F9D6CD2A82DF8, ___lookSpeed_6)); }
	inline float get_lookSpeed_6() const { return ___lookSpeed_6; }
	inline float* get_address_of_lookSpeed_6() { return &___lookSpeed_6; }
	inline void set_lookSpeed_6(float value)
	{
		___lookSpeed_6 = value;
	}

	inline static int32_t get_offset_of_initialCameraPosition_7() { return static_cast<int32_t>(offsetof(CameraFollow_tC9B62E254DA1376073E7B793597F9D6CD2A82DF8, ___initialCameraPosition_7)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_initialCameraPosition_7() const { return ___initialCameraPosition_7; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_initialCameraPosition_7() { return &___initialCameraPosition_7; }
	inline void set_initialCameraPosition_7(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___initialCameraPosition_7 = value;
	}

	inline static int32_t get_offset_of_initialCarPosition_8() { return static_cast<int32_t>(offsetof(CameraFollow_tC9B62E254DA1376073E7B793597F9D6CD2A82DF8, ___initialCarPosition_8)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_initialCarPosition_8() const { return ___initialCarPosition_8; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_initialCarPosition_8() { return &___initialCarPosition_8; }
	inline void set_initialCarPosition_8(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___initialCarPosition_8 = value;
	}

	inline static int32_t get_offset_of_absoluteInitCameraPosition_9() { return static_cast<int32_t>(offsetof(CameraFollow_tC9B62E254DA1376073E7B793597F9D6CD2A82DF8, ___absoluteInitCameraPosition_9)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_absoluteInitCameraPosition_9() const { return ___absoluteInitCameraPosition_9; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_absoluteInitCameraPosition_9() { return &___absoluteInitCameraPosition_9; }
	inline void set_absoluteInitCameraPosition_9(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___absoluteInitCameraPosition_9 = value;
	}
};


// CarAI
struct  CarAI_t69CE78AFFF0A7EFA2FFFDF485C42E5A279C8494A  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Transform CarAI::character
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___character_4;
	// UnityEngine.Transform CarAI::carFront
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___carFront_5;
	// System.Boolean CarAI::isStopping
	bool ___isStopping_6;
	// System.Single CarAI::distanceWithLight
	float ___distanceWithLight_7;
	// System.Single CarAI::distanceWithCharacter
	float ___distanceWithCharacter_8;
	// System.Single CarAI::distanceWithCLosestCar
	float ___distanceWithCLosestCar_9;
	// System.Single CarAI::maxDistanceL
	float ___maxDistanceL_10;
	// System.Single CarAI::maxDistanceC
	float ___maxDistanceC_11;
	// System.Single CarAI::maxDistanceCars
	float ___maxDistanceCars_12;
	// System.Single CarAI::speed
	float ___speed_13;
	// System.Boolean CarAI::isCloseToCars
	bool ___isCloseToCars_14;
	// TrafficLight CarAI::trafficLight
	TrafficLight_tD55F68E16E18C0A5F32B506715F5BE0989084ABD * ___trafficLight_15;

public:
	inline static int32_t get_offset_of_character_4() { return static_cast<int32_t>(offsetof(CarAI_t69CE78AFFF0A7EFA2FFFDF485C42E5A279C8494A, ___character_4)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_character_4() const { return ___character_4; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_character_4() { return &___character_4; }
	inline void set_character_4(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___character_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___character_4), (void*)value);
	}

	inline static int32_t get_offset_of_carFront_5() { return static_cast<int32_t>(offsetof(CarAI_t69CE78AFFF0A7EFA2FFFDF485C42E5A279C8494A, ___carFront_5)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_carFront_5() const { return ___carFront_5; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_carFront_5() { return &___carFront_5; }
	inline void set_carFront_5(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___carFront_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___carFront_5), (void*)value);
	}

	inline static int32_t get_offset_of_isStopping_6() { return static_cast<int32_t>(offsetof(CarAI_t69CE78AFFF0A7EFA2FFFDF485C42E5A279C8494A, ___isStopping_6)); }
	inline bool get_isStopping_6() const { return ___isStopping_6; }
	inline bool* get_address_of_isStopping_6() { return &___isStopping_6; }
	inline void set_isStopping_6(bool value)
	{
		___isStopping_6 = value;
	}

	inline static int32_t get_offset_of_distanceWithLight_7() { return static_cast<int32_t>(offsetof(CarAI_t69CE78AFFF0A7EFA2FFFDF485C42E5A279C8494A, ___distanceWithLight_7)); }
	inline float get_distanceWithLight_7() const { return ___distanceWithLight_7; }
	inline float* get_address_of_distanceWithLight_7() { return &___distanceWithLight_7; }
	inline void set_distanceWithLight_7(float value)
	{
		___distanceWithLight_7 = value;
	}

	inline static int32_t get_offset_of_distanceWithCharacter_8() { return static_cast<int32_t>(offsetof(CarAI_t69CE78AFFF0A7EFA2FFFDF485C42E5A279C8494A, ___distanceWithCharacter_8)); }
	inline float get_distanceWithCharacter_8() const { return ___distanceWithCharacter_8; }
	inline float* get_address_of_distanceWithCharacter_8() { return &___distanceWithCharacter_8; }
	inline void set_distanceWithCharacter_8(float value)
	{
		___distanceWithCharacter_8 = value;
	}

	inline static int32_t get_offset_of_distanceWithCLosestCar_9() { return static_cast<int32_t>(offsetof(CarAI_t69CE78AFFF0A7EFA2FFFDF485C42E5A279C8494A, ___distanceWithCLosestCar_9)); }
	inline float get_distanceWithCLosestCar_9() const { return ___distanceWithCLosestCar_9; }
	inline float* get_address_of_distanceWithCLosestCar_9() { return &___distanceWithCLosestCar_9; }
	inline void set_distanceWithCLosestCar_9(float value)
	{
		___distanceWithCLosestCar_9 = value;
	}

	inline static int32_t get_offset_of_maxDistanceL_10() { return static_cast<int32_t>(offsetof(CarAI_t69CE78AFFF0A7EFA2FFFDF485C42E5A279C8494A, ___maxDistanceL_10)); }
	inline float get_maxDistanceL_10() const { return ___maxDistanceL_10; }
	inline float* get_address_of_maxDistanceL_10() { return &___maxDistanceL_10; }
	inline void set_maxDistanceL_10(float value)
	{
		___maxDistanceL_10 = value;
	}

	inline static int32_t get_offset_of_maxDistanceC_11() { return static_cast<int32_t>(offsetof(CarAI_t69CE78AFFF0A7EFA2FFFDF485C42E5A279C8494A, ___maxDistanceC_11)); }
	inline float get_maxDistanceC_11() const { return ___maxDistanceC_11; }
	inline float* get_address_of_maxDistanceC_11() { return &___maxDistanceC_11; }
	inline void set_maxDistanceC_11(float value)
	{
		___maxDistanceC_11 = value;
	}

	inline static int32_t get_offset_of_maxDistanceCars_12() { return static_cast<int32_t>(offsetof(CarAI_t69CE78AFFF0A7EFA2FFFDF485C42E5A279C8494A, ___maxDistanceCars_12)); }
	inline float get_maxDistanceCars_12() const { return ___maxDistanceCars_12; }
	inline float* get_address_of_maxDistanceCars_12() { return &___maxDistanceCars_12; }
	inline void set_maxDistanceCars_12(float value)
	{
		___maxDistanceCars_12 = value;
	}

	inline static int32_t get_offset_of_speed_13() { return static_cast<int32_t>(offsetof(CarAI_t69CE78AFFF0A7EFA2FFFDF485C42E5A279C8494A, ___speed_13)); }
	inline float get_speed_13() const { return ___speed_13; }
	inline float* get_address_of_speed_13() { return &___speed_13; }
	inline void set_speed_13(float value)
	{
		___speed_13 = value;
	}

	inline static int32_t get_offset_of_isCloseToCars_14() { return static_cast<int32_t>(offsetof(CarAI_t69CE78AFFF0A7EFA2FFFDF485C42E5A279C8494A, ___isCloseToCars_14)); }
	inline bool get_isCloseToCars_14() const { return ___isCloseToCars_14; }
	inline bool* get_address_of_isCloseToCars_14() { return &___isCloseToCars_14; }
	inline void set_isCloseToCars_14(bool value)
	{
		___isCloseToCars_14 = value;
	}

	inline static int32_t get_offset_of_trafficLight_15() { return static_cast<int32_t>(offsetof(CarAI_t69CE78AFFF0A7EFA2FFFDF485C42E5A279C8494A, ___trafficLight_15)); }
	inline TrafficLight_tD55F68E16E18C0A5F32B506715F5BE0989084ABD * get_trafficLight_15() const { return ___trafficLight_15; }
	inline TrafficLight_tD55F68E16E18C0A5F32B506715F5BE0989084ABD ** get_address_of_trafficLight_15() { return &___trafficLight_15; }
	inline void set_trafficLight_15(TrafficLight_tD55F68E16E18C0A5F32B506715F5BE0989084ABD * value)
	{
		___trafficLight_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___trafficLight_15), (void*)value);
	}
};


// CarController
struct  CarController_tB945E8CD4EBA16BA6F4C8B4346292786701C9311  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Single CarController::horizontalInput
	float ___horizontalInput_6;
	// System.Single CarController::verticalInput
	float ___verticalInput_7;
	// System.Single CarController::currentSteerAngle
	float ___currentSteerAngle_8;
	// System.Single CarController::currentbreakForce
	float ___currentbreakForce_9;
	// System.Boolean CarController::isBreaking
	bool ___isBreaking_10;
	// System.Single CarController::speed
	float ___speed_11;
	// System.Single CarController::motorForce
	float ___motorForce_12;
	// System.Single CarController::breakForce
	float ___breakForce_13;
	// System.Single CarController::maxSteerAngle
	float ___maxSteerAngle_14;
	// UnityEngine.WheelCollider CarController::frontLeftWheelCollider
	WheelCollider_t57B08104FE16DFC3BF72826F7A3CCB8477C01779 * ___frontLeftWheelCollider_15;
	// UnityEngine.WheelCollider CarController::frontRightWheelCollider
	WheelCollider_t57B08104FE16DFC3BF72826F7A3CCB8477C01779 * ___frontRightWheelCollider_16;
	// UnityEngine.WheelCollider CarController::rearLeftWheelCollider
	WheelCollider_t57B08104FE16DFC3BF72826F7A3CCB8477C01779 * ___rearLeftWheelCollider_17;
	// UnityEngine.WheelCollider CarController::rearRightWheelCollider
	WheelCollider_t57B08104FE16DFC3BF72826F7A3CCB8477C01779 * ___rearRightWheelCollider_18;
	// UnityEngine.Transform CarController::frontLeftWheelTransform
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___frontLeftWheelTransform_19;
	// UnityEngine.Transform CarController::frontRightWheeTransform
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___frontRightWheeTransform_20;
	// UnityEngine.Transform CarController::rearLeftWheelTransform
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___rearLeftWheelTransform_21;
	// UnityEngine.Transform CarController::rearRightWheelTransform
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___rearRightWheelTransform_22;

public:
	inline static int32_t get_offset_of_horizontalInput_6() { return static_cast<int32_t>(offsetof(CarController_tB945E8CD4EBA16BA6F4C8B4346292786701C9311, ___horizontalInput_6)); }
	inline float get_horizontalInput_6() const { return ___horizontalInput_6; }
	inline float* get_address_of_horizontalInput_6() { return &___horizontalInput_6; }
	inline void set_horizontalInput_6(float value)
	{
		___horizontalInput_6 = value;
	}

	inline static int32_t get_offset_of_verticalInput_7() { return static_cast<int32_t>(offsetof(CarController_tB945E8CD4EBA16BA6F4C8B4346292786701C9311, ___verticalInput_7)); }
	inline float get_verticalInput_7() const { return ___verticalInput_7; }
	inline float* get_address_of_verticalInput_7() { return &___verticalInput_7; }
	inline void set_verticalInput_7(float value)
	{
		___verticalInput_7 = value;
	}

	inline static int32_t get_offset_of_currentSteerAngle_8() { return static_cast<int32_t>(offsetof(CarController_tB945E8CD4EBA16BA6F4C8B4346292786701C9311, ___currentSteerAngle_8)); }
	inline float get_currentSteerAngle_8() const { return ___currentSteerAngle_8; }
	inline float* get_address_of_currentSteerAngle_8() { return &___currentSteerAngle_8; }
	inline void set_currentSteerAngle_8(float value)
	{
		___currentSteerAngle_8 = value;
	}

	inline static int32_t get_offset_of_currentbreakForce_9() { return static_cast<int32_t>(offsetof(CarController_tB945E8CD4EBA16BA6F4C8B4346292786701C9311, ___currentbreakForce_9)); }
	inline float get_currentbreakForce_9() const { return ___currentbreakForce_9; }
	inline float* get_address_of_currentbreakForce_9() { return &___currentbreakForce_9; }
	inline void set_currentbreakForce_9(float value)
	{
		___currentbreakForce_9 = value;
	}

	inline static int32_t get_offset_of_isBreaking_10() { return static_cast<int32_t>(offsetof(CarController_tB945E8CD4EBA16BA6F4C8B4346292786701C9311, ___isBreaking_10)); }
	inline bool get_isBreaking_10() const { return ___isBreaking_10; }
	inline bool* get_address_of_isBreaking_10() { return &___isBreaking_10; }
	inline void set_isBreaking_10(bool value)
	{
		___isBreaking_10 = value;
	}

	inline static int32_t get_offset_of_speed_11() { return static_cast<int32_t>(offsetof(CarController_tB945E8CD4EBA16BA6F4C8B4346292786701C9311, ___speed_11)); }
	inline float get_speed_11() const { return ___speed_11; }
	inline float* get_address_of_speed_11() { return &___speed_11; }
	inline void set_speed_11(float value)
	{
		___speed_11 = value;
	}

	inline static int32_t get_offset_of_motorForce_12() { return static_cast<int32_t>(offsetof(CarController_tB945E8CD4EBA16BA6F4C8B4346292786701C9311, ___motorForce_12)); }
	inline float get_motorForce_12() const { return ___motorForce_12; }
	inline float* get_address_of_motorForce_12() { return &___motorForce_12; }
	inline void set_motorForce_12(float value)
	{
		___motorForce_12 = value;
	}

	inline static int32_t get_offset_of_breakForce_13() { return static_cast<int32_t>(offsetof(CarController_tB945E8CD4EBA16BA6F4C8B4346292786701C9311, ___breakForce_13)); }
	inline float get_breakForce_13() const { return ___breakForce_13; }
	inline float* get_address_of_breakForce_13() { return &___breakForce_13; }
	inline void set_breakForce_13(float value)
	{
		___breakForce_13 = value;
	}

	inline static int32_t get_offset_of_maxSteerAngle_14() { return static_cast<int32_t>(offsetof(CarController_tB945E8CD4EBA16BA6F4C8B4346292786701C9311, ___maxSteerAngle_14)); }
	inline float get_maxSteerAngle_14() const { return ___maxSteerAngle_14; }
	inline float* get_address_of_maxSteerAngle_14() { return &___maxSteerAngle_14; }
	inline void set_maxSteerAngle_14(float value)
	{
		___maxSteerAngle_14 = value;
	}

	inline static int32_t get_offset_of_frontLeftWheelCollider_15() { return static_cast<int32_t>(offsetof(CarController_tB945E8CD4EBA16BA6F4C8B4346292786701C9311, ___frontLeftWheelCollider_15)); }
	inline WheelCollider_t57B08104FE16DFC3BF72826F7A3CCB8477C01779 * get_frontLeftWheelCollider_15() const { return ___frontLeftWheelCollider_15; }
	inline WheelCollider_t57B08104FE16DFC3BF72826F7A3CCB8477C01779 ** get_address_of_frontLeftWheelCollider_15() { return &___frontLeftWheelCollider_15; }
	inline void set_frontLeftWheelCollider_15(WheelCollider_t57B08104FE16DFC3BF72826F7A3CCB8477C01779 * value)
	{
		___frontLeftWheelCollider_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___frontLeftWheelCollider_15), (void*)value);
	}

	inline static int32_t get_offset_of_frontRightWheelCollider_16() { return static_cast<int32_t>(offsetof(CarController_tB945E8CD4EBA16BA6F4C8B4346292786701C9311, ___frontRightWheelCollider_16)); }
	inline WheelCollider_t57B08104FE16DFC3BF72826F7A3CCB8477C01779 * get_frontRightWheelCollider_16() const { return ___frontRightWheelCollider_16; }
	inline WheelCollider_t57B08104FE16DFC3BF72826F7A3CCB8477C01779 ** get_address_of_frontRightWheelCollider_16() { return &___frontRightWheelCollider_16; }
	inline void set_frontRightWheelCollider_16(WheelCollider_t57B08104FE16DFC3BF72826F7A3CCB8477C01779 * value)
	{
		___frontRightWheelCollider_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___frontRightWheelCollider_16), (void*)value);
	}

	inline static int32_t get_offset_of_rearLeftWheelCollider_17() { return static_cast<int32_t>(offsetof(CarController_tB945E8CD4EBA16BA6F4C8B4346292786701C9311, ___rearLeftWheelCollider_17)); }
	inline WheelCollider_t57B08104FE16DFC3BF72826F7A3CCB8477C01779 * get_rearLeftWheelCollider_17() const { return ___rearLeftWheelCollider_17; }
	inline WheelCollider_t57B08104FE16DFC3BF72826F7A3CCB8477C01779 ** get_address_of_rearLeftWheelCollider_17() { return &___rearLeftWheelCollider_17; }
	inline void set_rearLeftWheelCollider_17(WheelCollider_t57B08104FE16DFC3BF72826F7A3CCB8477C01779 * value)
	{
		___rearLeftWheelCollider_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___rearLeftWheelCollider_17), (void*)value);
	}

	inline static int32_t get_offset_of_rearRightWheelCollider_18() { return static_cast<int32_t>(offsetof(CarController_tB945E8CD4EBA16BA6F4C8B4346292786701C9311, ___rearRightWheelCollider_18)); }
	inline WheelCollider_t57B08104FE16DFC3BF72826F7A3CCB8477C01779 * get_rearRightWheelCollider_18() const { return ___rearRightWheelCollider_18; }
	inline WheelCollider_t57B08104FE16DFC3BF72826F7A3CCB8477C01779 ** get_address_of_rearRightWheelCollider_18() { return &___rearRightWheelCollider_18; }
	inline void set_rearRightWheelCollider_18(WheelCollider_t57B08104FE16DFC3BF72826F7A3CCB8477C01779 * value)
	{
		___rearRightWheelCollider_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___rearRightWheelCollider_18), (void*)value);
	}

	inline static int32_t get_offset_of_frontLeftWheelTransform_19() { return static_cast<int32_t>(offsetof(CarController_tB945E8CD4EBA16BA6F4C8B4346292786701C9311, ___frontLeftWheelTransform_19)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_frontLeftWheelTransform_19() const { return ___frontLeftWheelTransform_19; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_frontLeftWheelTransform_19() { return &___frontLeftWheelTransform_19; }
	inline void set_frontLeftWheelTransform_19(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___frontLeftWheelTransform_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___frontLeftWheelTransform_19), (void*)value);
	}

	inline static int32_t get_offset_of_frontRightWheeTransform_20() { return static_cast<int32_t>(offsetof(CarController_tB945E8CD4EBA16BA6F4C8B4346292786701C9311, ___frontRightWheeTransform_20)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_frontRightWheeTransform_20() const { return ___frontRightWheeTransform_20; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_frontRightWheeTransform_20() { return &___frontRightWheeTransform_20; }
	inline void set_frontRightWheeTransform_20(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___frontRightWheeTransform_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___frontRightWheeTransform_20), (void*)value);
	}

	inline static int32_t get_offset_of_rearLeftWheelTransform_21() { return static_cast<int32_t>(offsetof(CarController_tB945E8CD4EBA16BA6F4C8B4346292786701C9311, ___rearLeftWheelTransform_21)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_rearLeftWheelTransform_21() const { return ___rearLeftWheelTransform_21; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_rearLeftWheelTransform_21() { return &___rearLeftWheelTransform_21; }
	inline void set_rearLeftWheelTransform_21(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___rearLeftWheelTransform_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___rearLeftWheelTransform_21), (void*)value);
	}

	inline static int32_t get_offset_of_rearRightWheelTransform_22() { return static_cast<int32_t>(offsetof(CarController_tB945E8CD4EBA16BA6F4C8B4346292786701C9311, ___rearRightWheelTransform_22)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_rearRightWheelTransform_22() const { return ___rearRightWheelTransform_22; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_rearRightWheelTransform_22() { return &___rearRightWheelTransform_22; }
	inline void set_rearRightWheelTransform_22(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___rearRightWheelTransform_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___rearRightWheelTransform_22), (void*)value);
	}
};


// GameEndController
struct  GameEndController_t22B3012DEB58F882ECF6BE83EB24B4997F177C12  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.GameObject GameEndController::wonPanel
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___wonPanel_4;
	// UnityEngine.UI.Text GameEndController::wonPanelText
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___wonPanelText_5;
	// UnityEngine.AudioSource GameEndController::wonPanelSound
	AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * ___wonPanelSound_6;
	// UnityEngine.GameObject GameEndController::failPanel
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___failPanel_7;
	// UnityEngine.UI.Text GameEndController::failPanelText
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___failPanelText_8;
	// UnityEngine.AudioSource GameEndController::failPanelSound
	AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * ___failPanelSound_9;

public:
	inline static int32_t get_offset_of_wonPanel_4() { return static_cast<int32_t>(offsetof(GameEndController_t22B3012DEB58F882ECF6BE83EB24B4997F177C12, ___wonPanel_4)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_wonPanel_4() const { return ___wonPanel_4; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_wonPanel_4() { return &___wonPanel_4; }
	inline void set_wonPanel_4(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___wonPanel_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___wonPanel_4), (void*)value);
	}

	inline static int32_t get_offset_of_wonPanelText_5() { return static_cast<int32_t>(offsetof(GameEndController_t22B3012DEB58F882ECF6BE83EB24B4997F177C12, ___wonPanelText_5)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_wonPanelText_5() const { return ___wonPanelText_5; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_wonPanelText_5() { return &___wonPanelText_5; }
	inline void set_wonPanelText_5(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___wonPanelText_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___wonPanelText_5), (void*)value);
	}

	inline static int32_t get_offset_of_wonPanelSound_6() { return static_cast<int32_t>(offsetof(GameEndController_t22B3012DEB58F882ECF6BE83EB24B4997F177C12, ___wonPanelSound_6)); }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * get_wonPanelSound_6() const { return ___wonPanelSound_6; }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B ** get_address_of_wonPanelSound_6() { return &___wonPanelSound_6; }
	inline void set_wonPanelSound_6(AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * value)
	{
		___wonPanelSound_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___wonPanelSound_6), (void*)value);
	}

	inline static int32_t get_offset_of_failPanel_7() { return static_cast<int32_t>(offsetof(GameEndController_t22B3012DEB58F882ECF6BE83EB24B4997F177C12, ___failPanel_7)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_failPanel_7() const { return ___failPanel_7; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_failPanel_7() { return &___failPanel_7; }
	inline void set_failPanel_7(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___failPanel_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___failPanel_7), (void*)value);
	}

	inline static int32_t get_offset_of_failPanelText_8() { return static_cast<int32_t>(offsetof(GameEndController_t22B3012DEB58F882ECF6BE83EB24B4997F177C12, ___failPanelText_8)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_failPanelText_8() const { return ___failPanelText_8; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_failPanelText_8() { return &___failPanelText_8; }
	inline void set_failPanelText_8(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___failPanelText_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___failPanelText_8), (void*)value);
	}

	inline static int32_t get_offset_of_failPanelSound_9() { return static_cast<int32_t>(offsetof(GameEndController_t22B3012DEB58F882ECF6BE83EB24B4997F177C12, ___failPanelSound_9)); }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * get_failPanelSound_9() const { return ___failPanelSound_9; }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B ** get_address_of_failPanelSound_9() { return &___failPanelSound_9; }
	inline void set_failPanelSound_9(AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * value)
	{
		___failPanelSound_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___failPanelSound_9), (void*)value);
	}
};


// MobileDisableAutoSwitchControls
struct  MobileDisableAutoSwitchControls_tFFBB1531EDDED48A8D620DF56060164FE3482D4F  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};


// MouseLook
struct  MouseLook_t1E1EE79B13022184F3EF9AC8FE8C6771053890DA  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// MouseLook/RotationAxes MouseLook::axes
	int32_t ___axes_4;
	// System.Single MouseLook::sensitivityX
	float ___sensitivityX_5;
	// System.Single MouseLook::sensitivityY
	float ___sensitivityY_6;
	// System.Single MouseLook::minimumX
	float ___minimumX_7;
	// System.Single MouseLook::maximumX
	float ___maximumX_8;
	// System.Single MouseLook::minimumY
	float ___minimumY_9;
	// System.Single MouseLook::maximumY
	float ___maximumY_10;
	// System.Single MouseLook::rotationY
	float ___rotationY_11;

public:
	inline static int32_t get_offset_of_axes_4() { return static_cast<int32_t>(offsetof(MouseLook_t1E1EE79B13022184F3EF9AC8FE8C6771053890DA, ___axes_4)); }
	inline int32_t get_axes_4() const { return ___axes_4; }
	inline int32_t* get_address_of_axes_4() { return &___axes_4; }
	inline void set_axes_4(int32_t value)
	{
		___axes_4 = value;
	}

	inline static int32_t get_offset_of_sensitivityX_5() { return static_cast<int32_t>(offsetof(MouseLook_t1E1EE79B13022184F3EF9AC8FE8C6771053890DA, ___sensitivityX_5)); }
	inline float get_sensitivityX_5() const { return ___sensitivityX_5; }
	inline float* get_address_of_sensitivityX_5() { return &___sensitivityX_5; }
	inline void set_sensitivityX_5(float value)
	{
		___sensitivityX_5 = value;
	}

	inline static int32_t get_offset_of_sensitivityY_6() { return static_cast<int32_t>(offsetof(MouseLook_t1E1EE79B13022184F3EF9AC8FE8C6771053890DA, ___sensitivityY_6)); }
	inline float get_sensitivityY_6() const { return ___sensitivityY_6; }
	inline float* get_address_of_sensitivityY_6() { return &___sensitivityY_6; }
	inline void set_sensitivityY_6(float value)
	{
		___sensitivityY_6 = value;
	}

	inline static int32_t get_offset_of_minimumX_7() { return static_cast<int32_t>(offsetof(MouseLook_t1E1EE79B13022184F3EF9AC8FE8C6771053890DA, ___minimumX_7)); }
	inline float get_minimumX_7() const { return ___minimumX_7; }
	inline float* get_address_of_minimumX_7() { return &___minimumX_7; }
	inline void set_minimumX_7(float value)
	{
		___minimumX_7 = value;
	}

	inline static int32_t get_offset_of_maximumX_8() { return static_cast<int32_t>(offsetof(MouseLook_t1E1EE79B13022184F3EF9AC8FE8C6771053890DA, ___maximumX_8)); }
	inline float get_maximumX_8() const { return ___maximumX_8; }
	inline float* get_address_of_maximumX_8() { return &___maximumX_8; }
	inline void set_maximumX_8(float value)
	{
		___maximumX_8 = value;
	}

	inline static int32_t get_offset_of_minimumY_9() { return static_cast<int32_t>(offsetof(MouseLook_t1E1EE79B13022184F3EF9AC8FE8C6771053890DA, ___minimumY_9)); }
	inline float get_minimumY_9() const { return ___minimumY_9; }
	inline float* get_address_of_minimumY_9() { return &___minimumY_9; }
	inline void set_minimumY_9(float value)
	{
		___minimumY_9 = value;
	}

	inline static int32_t get_offset_of_maximumY_10() { return static_cast<int32_t>(offsetof(MouseLook_t1E1EE79B13022184F3EF9AC8FE8C6771053890DA, ___maximumY_10)); }
	inline float get_maximumY_10() const { return ___maximumY_10; }
	inline float* get_address_of_maximumY_10() { return &___maximumY_10; }
	inline void set_maximumY_10(float value)
	{
		___maximumY_10 = value;
	}

	inline static int32_t get_offset_of_rotationY_11() { return static_cast<int32_t>(offsetof(MouseLook_t1E1EE79B13022184F3EF9AC8FE8C6771053890DA, ___rotationY_11)); }
	inline float get_rotationY_11() const { return ___rotationY_11; }
	inline float* get_address_of_rotationY_11() { return &___rotationY_11; }
	inline void set_rotationY_11(float value)
	{
		___rotationY_11 = value;
	}
};


// OpeningScenes
struct  OpeningScenes_t7E22C921E28D22764559359937031A9C87AE65CD  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};


// PersonCharacterController
struct  PersonCharacterController_t8CEAA5B6ABD8B66DB06CDDE5CA6104ECEDC0538B  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.GameObject PersonCharacterController::walkerStart
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___walkerStart_4;
	// UnityEngine.GameObject PersonCharacterController::walkerMiddle
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___walkerMiddle_5;
	// UnityEngine.GameObject PersonCharacterController::walkerFinish
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___walkerFinish_6;
	// UnityEngine.GameObject PersonCharacterController::overpassStart
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___overpassStart_7;
	// UnityEngine.GameObject PersonCharacterController::overpassUp
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___overpassUp_8;
	// UnityEngine.GameObject PersonCharacterController::overpassFinish
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___overpassFinish_9;
	// UnityEngine.GameObject[] PersonCharacterController::rules
	GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* ___rules_10;
	// UnityEngine.GameObject[] PersonCharacterController::directions
	GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* ___directions_11;
	// TrafficLight PersonCharacterController::firstLight
	TrafficLight_tD55F68E16E18C0A5F32B506715F5BE0989084ABD * ___firstLight_12;
	// TrafficLight PersonCharacterController::secondLight
	TrafficLight_tD55F68E16E18C0A5F32B506715F5BE0989084ABD * ___secondLight_13;
	// UnityEngine.Material PersonCharacterController::red
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___red_14;
	// UnityEngine.Material PersonCharacterController::green
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___green_15;
	// GameEndController PersonCharacterController::Controller
	GameEndController_t22B3012DEB58F882ECF6BE83EB24B4997F177C12 * ___Controller_16;

public:
	inline static int32_t get_offset_of_walkerStart_4() { return static_cast<int32_t>(offsetof(PersonCharacterController_t8CEAA5B6ABD8B66DB06CDDE5CA6104ECEDC0538B, ___walkerStart_4)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_walkerStart_4() const { return ___walkerStart_4; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_walkerStart_4() { return &___walkerStart_4; }
	inline void set_walkerStart_4(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___walkerStart_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___walkerStart_4), (void*)value);
	}

	inline static int32_t get_offset_of_walkerMiddle_5() { return static_cast<int32_t>(offsetof(PersonCharacterController_t8CEAA5B6ABD8B66DB06CDDE5CA6104ECEDC0538B, ___walkerMiddle_5)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_walkerMiddle_5() const { return ___walkerMiddle_5; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_walkerMiddle_5() { return &___walkerMiddle_5; }
	inline void set_walkerMiddle_5(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___walkerMiddle_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___walkerMiddle_5), (void*)value);
	}

	inline static int32_t get_offset_of_walkerFinish_6() { return static_cast<int32_t>(offsetof(PersonCharacterController_t8CEAA5B6ABD8B66DB06CDDE5CA6104ECEDC0538B, ___walkerFinish_6)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_walkerFinish_6() const { return ___walkerFinish_6; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_walkerFinish_6() { return &___walkerFinish_6; }
	inline void set_walkerFinish_6(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___walkerFinish_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___walkerFinish_6), (void*)value);
	}

	inline static int32_t get_offset_of_overpassStart_7() { return static_cast<int32_t>(offsetof(PersonCharacterController_t8CEAA5B6ABD8B66DB06CDDE5CA6104ECEDC0538B, ___overpassStart_7)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_overpassStart_7() const { return ___overpassStart_7; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_overpassStart_7() { return &___overpassStart_7; }
	inline void set_overpassStart_7(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___overpassStart_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___overpassStart_7), (void*)value);
	}

	inline static int32_t get_offset_of_overpassUp_8() { return static_cast<int32_t>(offsetof(PersonCharacterController_t8CEAA5B6ABD8B66DB06CDDE5CA6104ECEDC0538B, ___overpassUp_8)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_overpassUp_8() const { return ___overpassUp_8; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_overpassUp_8() { return &___overpassUp_8; }
	inline void set_overpassUp_8(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___overpassUp_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___overpassUp_8), (void*)value);
	}

	inline static int32_t get_offset_of_overpassFinish_9() { return static_cast<int32_t>(offsetof(PersonCharacterController_t8CEAA5B6ABD8B66DB06CDDE5CA6104ECEDC0538B, ___overpassFinish_9)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_overpassFinish_9() const { return ___overpassFinish_9; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_overpassFinish_9() { return &___overpassFinish_9; }
	inline void set_overpassFinish_9(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___overpassFinish_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___overpassFinish_9), (void*)value);
	}

	inline static int32_t get_offset_of_rules_10() { return static_cast<int32_t>(offsetof(PersonCharacterController_t8CEAA5B6ABD8B66DB06CDDE5CA6104ECEDC0538B, ___rules_10)); }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* get_rules_10() const { return ___rules_10; }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642** get_address_of_rules_10() { return &___rules_10; }
	inline void set_rules_10(GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* value)
	{
		___rules_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___rules_10), (void*)value);
	}

	inline static int32_t get_offset_of_directions_11() { return static_cast<int32_t>(offsetof(PersonCharacterController_t8CEAA5B6ABD8B66DB06CDDE5CA6104ECEDC0538B, ___directions_11)); }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* get_directions_11() const { return ___directions_11; }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642** get_address_of_directions_11() { return &___directions_11; }
	inline void set_directions_11(GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* value)
	{
		___directions_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___directions_11), (void*)value);
	}

	inline static int32_t get_offset_of_firstLight_12() { return static_cast<int32_t>(offsetof(PersonCharacterController_t8CEAA5B6ABD8B66DB06CDDE5CA6104ECEDC0538B, ___firstLight_12)); }
	inline TrafficLight_tD55F68E16E18C0A5F32B506715F5BE0989084ABD * get_firstLight_12() const { return ___firstLight_12; }
	inline TrafficLight_tD55F68E16E18C0A5F32B506715F5BE0989084ABD ** get_address_of_firstLight_12() { return &___firstLight_12; }
	inline void set_firstLight_12(TrafficLight_tD55F68E16E18C0A5F32B506715F5BE0989084ABD * value)
	{
		___firstLight_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___firstLight_12), (void*)value);
	}

	inline static int32_t get_offset_of_secondLight_13() { return static_cast<int32_t>(offsetof(PersonCharacterController_t8CEAA5B6ABD8B66DB06CDDE5CA6104ECEDC0538B, ___secondLight_13)); }
	inline TrafficLight_tD55F68E16E18C0A5F32B506715F5BE0989084ABD * get_secondLight_13() const { return ___secondLight_13; }
	inline TrafficLight_tD55F68E16E18C0A5F32B506715F5BE0989084ABD ** get_address_of_secondLight_13() { return &___secondLight_13; }
	inline void set_secondLight_13(TrafficLight_tD55F68E16E18C0A5F32B506715F5BE0989084ABD * value)
	{
		___secondLight_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___secondLight_13), (void*)value);
	}

	inline static int32_t get_offset_of_red_14() { return static_cast<int32_t>(offsetof(PersonCharacterController_t8CEAA5B6ABD8B66DB06CDDE5CA6104ECEDC0538B, ___red_14)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_red_14() const { return ___red_14; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_red_14() { return &___red_14; }
	inline void set_red_14(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___red_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___red_14), (void*)value);
	}

	inline static int32_t get_offset_of_green_15() { return static_cast<int32_t>(offsetof(PersonCharacterController_t8CEAA5B6ABD8B66DB06CDDE5CA6104ECEDC0538B, ___green_15)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_green_15() const { return ___green_15; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_green_15() { return &___green_15; }
	inline void set_green_15(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___green_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___green_15), (void*)value);
	}

	inline static int32_t get_offset_of_Controller_16() { return static_cast<int32_t>(offsetof(PersonCharacterController_t8CEAA5B6ABD8B66DB06CDDE5CA6104ECEDC0538B, ___Controller_16)); }
	inline GameEndController_t22B3012DEB58F882ECF6BE83EB24B4997F177C12 * get_Controller_16() const { return ___Controller_16; }
	inline GameEndController_t22B3012DEB58F882ECF6BE83EB24B4997F177C12 ** get_address_of_Controller_16() { return &___Controller_16; }
	inline void set_Controller_16(GameEndController_t22B3012DEB58F882ECF6BE83EB24B4997F177C12 * value)
	{
		___Controller_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Controller_16), (void*)value);
	}
};


// PrometeoCarController
struct  PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Int32 PrometeoCarController::maxSpeed
	int32_t ___maxSpeed_4;
	// System.Int32 PrometeoCarController::maxReverseSpeed
	int32_t ___maxReverseSpeed_5;
	// System.Int32 PrometeoCarController::accelerationMultiplier
	int32_t ___accelerationMultiplier_6;
	// System.Int32 PrometeoCarController::maxSteeringAngle
	int32_t ___maxSteeringAngle_7;
	// System.Single PrometeoCarController::steeringSpeed
	float ___steeringSpeed_8;
	// System.Int32 PrometeoCarController::brakeForce
	int32_t ___brakeForce_9;
	// System.Int32 PrometeoCarController::decelerationMultiplier
	int32_t ___decelerationMultiplier_10;
	// System.Int32 PrometeoCarController::handbrakeDriftMultiplier
	int32_t ___handbrakeDriftMultiplier_11;
	// UnityEngine.Vector3 PrometeoCarController::bodyMassCenter
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___bodyMassCenter_12;
	// UnityEngine.GameObject PrometeoCarController::frontLeftMesh
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___frontLeftMesh_13;
	// UnityEngine.WheelCollider PrometeoCarController::frontLeftCollider
	WheelCollider_t57B08104FE16DFC3BF72826F7A3CCB8477C01779 * ___frontLeftCollider_14;
	// UnityEngine.GameObject PrometeoCarController::frontRightMesh
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___frontRightMesh_15;
	// UnityEngine.WheelCollider PrometeoCarController::frontRightCollider
	WheelCollider_t57B08104FE16DFC3BF72826F7A3CCB8477C01779 * ___frontRightCollider_16;
	// UnityEngine.GameObject PrometeoCarController::rearLeftMesh
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___rearLeftMesh_17;
	// UnityEngine.WheelCollider PrometeoCarController::rearLeftCollider
	WheelCollider_t57B08104FE16DFC3BF72826F7A3CCB8477C01779 * ___rearLeftCollider_18;
	// UnityEngine.GameObject PrometeoCarController::rearRightMesh
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___rearRightMesh_19;
	// UnityEngine.WheelCollider PrometeoCarController::rearRightCollider
	WheelCollider_t57B08104FE16DFC3BF72826F7A3CCB8477C01779 * ___rearRightCollider_20;
	// System.Boolean PrometeoCarController::useEffects
	bool ___useEffects_21;
	// UnityEngine.ParticleSystem PrometeoCarController::RLWParticleSystem
	ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___RLWParticleSystem_22;
	// UnityEngine.ParticleSystem PrometeoCarController::RRWParticleSystem
	ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___RRWParticleSystem_23;
	// UnityEngine.TrailRenderer PrometeoCarController::RLWTireSkid
	TrailRenderer_t219A9B1F6C4B984AE4BEEC40F90665D122056A01 * ___RLWTireSkid_24;
	// UnityEngine.TrailRenderer PrometeoCarController::RRWTireSkid
	TrailRenderer_t219A9B1F6C4B984AE4BEEC40F90665D122056A01 * ___RRWTireSkid_25;
	// System.Boolean PrometeoCarController::useUI
	bool ___useUI_26;
	// UnityEngine.UI.Text PrometeoCarController::carSpeedText
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___carSpeedText_27;
	// System.Boolean PrometeoCarController::useSounds
	bool ___useSounds_28;
	// UnityEngine.AudioSource PrometeoCarController::carEngineSound
	AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * ___carEngineSound_29;
	// UnityEngine.AudioSource PrometeoCarController::tireScreechSound
	AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * ___tireScreechSound_30;
	// System.Single PrometeoCarController::initialCarEngineSoundPitch
	float ___initialCarEngineSoundPitch_31;
	// System.Boolean PrometeoCarController::useTouchControls
	bool ___useTouchControls_32;
	// UnityEngine.GameObject PrometeoCarController::throttleButton
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___throttleButton_33;
	// PrometeoTouchInput PrometeoCarController::throttlePTI
	PrometeoTouchInput_t1C5A9B19512A75AFEAD2E916C6F53F597ADF73CF * ___throttlePTI_34;
	// UnityEngine.GameObject PrometeoCarController::reverseButton
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___reverseButton_35;
	// PrometeoTouchInput PrometeoCarController::reversePTI
	PrometeoTouchInput_t1C5A9B19512A75AFEAD2E916C6F53F597ADF73CF * ___reversePTI_36;
	// UnityEngine.GameObject PrometeoCarController::turnRightButton
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___turnRightButton_37;
	// PrometeoTouchInput PrometeoCarController::turnRightPTI
	PrometeoTouchInput_t1C5A9B19512A75AFEAD2E916C6F53F597ADF73CF * ___turnRightPTI_38;
	// UnityEngine.GameObject PrometeoCarController::turnLeftButton
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___turnLeftButton_39;
	// PrometeoTouchInput PrometeoCarController::turnLeftPTI
	PrometeoTouchInput_t1C5A9B19512A75AFEAD2E916C6F53F597ADF73CF * ___turnLeftPTI_40;
	// UnityEngine.GameObject PrometeoCarController::handbrakeButton
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___handbrakeButton_41;
	// PrometeoTouchInput PrometeoCarController::handbrakePTI
	PrometeoTouchInput_t1C5A9B19512A75AFEAD2E916C6F53F597ADF73CF * ___handbrakePTI_42;
	// System.Single PrometeoCarController::carSpeed
	float ___carSpeed_43;
	// System.Boolean PrometeoCarController::isDrifting
	bool ___isDrifting_44;
	// System.Boolean PrometeoCarController::isTractionLocked
	bool ___isTractionLocked_45;
	// UnityEngine.Rigidbody PrometeoCarController::carRigidbody
	Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * ___carRigidbody_46;
	// System.Single PrometeoCarController::steeringAxis
	float ___steeringAxis_47;
	// System.Single PrometeoCarController::throttleAxis
	float ___throttleAxis_48;
	// System.Single PrometeoCarController::driftingAxis
	float ___driftingAxis_49;
	// System.Single PrometeoCarController::localVelocityZ
	float ___localVelocityZ_50;
	// System.Single PrometeoCarController::localVelocityX
	float ___localVelocityX_51;
	// System.Boolean PrometeoCarController::deceleratingCar
	bool ___deceleratingCar_52;
	// System.Boolean PrometeoCarController::touchControlsSetup
	bool ___touchControlsSetup_53;
	// UnityEngine.WheelFrictionCurve PrometeoCarController::FLwheelFriction
	WheelFrictionCurve_t96AB1E3B37FC1E7369962A2E712ECD9A2AFE723D  ___FLwheelFriction_54;
	// System.Single PrometeoCarController::FLWextremumSlip
	float ___FLWextremumSlip_55;
	// UnityEngine.WheelFrictionCurve PrometeoCarController::FRwheelFriction
	WheelFrictionCurve_t96AB1E3B37FC1E7369962A2E712ECD9A2AFE723D  ___FRwheelFriction_56;
	// System.Single PrometeoCarController::FRWextremumSlip
	float ___FRWextremumSlip_57;
	// UnityEngine.WheelFrictionCurve PrometeoCarController::RLwheelFriction
	WheelFrictionCurve_t96AB1E3B37FC1E7369962A2E712ECD9A2AFE723D  ___RLwheelFriction_58;
	// System.Single PrometeoCarController::RLWextremumSlip
	float ___RLWextremumSlip_59;
	// UnityEngine.WheelFrictionCurve PrometeoCarController::RRwheelFriction
	WheelFrictionCurve_t96AB1E3B37FC1E7369962A2E712ECD9A2AFE723D  ___RRwheelFriction_60;
	// System.Single PrometeoCarController::RRWextremumSlip
	float ___RRWextremumSlip_61;

public:
	inline static int32_t get_offset_of_maxSpeed_4() { return static_cast<int32_t>(offsetof(PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E, ___maxSpeed_4)); }
	inline int32_t get_maxSpeed_4() const { return ___maxSpeed_4; }
	inline int32_t* get_address_of_maxSpeed_4() { return &___maxSpeed_4; }
	inline void set_maxSpeed_4(int32_t value)
	{
		___maxSpeed_4 = value;
	}

	inline static int32_t get_offset_of_maxReverseSpeed_5() { return static_cast<int32_t>(offsetof(PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E, ___maxReverseSpeed_5)); }
	inline int32_t get_maxReverseSpeed_5() const { return ___maxReverseSpeed_5; }
	inline int32_t* get_address_of_maxReverseSpeed_5() { return &___maxReverseSpeed_5; }
	inline void set_maxReverseSpeed_5(int32_t value)
	{
		___maxReverseSpeed_5 = value;
	}

	inline static int32_t get_offset_of_accelerationMultiplier_6() { return static_cast<int32_t>(offsetof(PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E, ___accelerationMultiplier_6)); }
	inline int32_t get_accelerationMultiplier_6() const { return ___accelerationMultiplier_6; }
	inline int32_t* get_address_of_accelerationMultiplier_6() { return &___accelerationMultiplier_6; }
	inline void set_accelerationMultiplier_6(int32_t value)
	{
		___accelerationMultiplier_6 = value;
	}

	inline static int32_t get_offset_of_maxSteeringAngle_7() { return static_cast<int32_t>(offsetof(PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E, ___maxSteeringAngle_7)); }
	inline int32_t get_maxSteeringAngle_7() const { return ___maxSteeringAngle_7; }
	inline int32_t* get_address_of_maxSteeringAngle_7() { return &___maxSteeringAngle_7; }
	inline void set_maxSteeringAngle_7(int32_t value)
	{
		___maxSteeringAngle_7 = value;
	}

	inline static int32_t get_offset_of_steeringSpeed_8() { return static_cast<int32_t>(offsetof(PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E, ___steeringSpeed_8)); }
	inline float get_steeringSpeed_8() const { return ___steeringSpeed_8; }
	inline float* get_address_of_steeringSpeed_8() { return &___steeringSpeed_8; }
	inline void set_steeringSpeed_8(float value)
	{
		___steeringSpeed_8 = value;
	}

	inline static int32_t get_offset_of_brakeForce_9() { return static_cast<int32_t>(offsetof(PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E, ___brakeForce_9)); }
	inline int32_t get_brakeForce_9() const { return ___brakeForce_9; }
	inline int32_t* get_address_of_brakeForce_9() { return &___brakeForce_9; }
	inline void set_brakeForce_9(int32_t value)
	{
		___brakeForce_9 = value;
	}

	inline static int32_t get_offset_of_decelerationMultiplier_10() { return static_cast<int32_t>(offsetof(PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E, ___decelerationMultiplier_10)); }
	inline int32_t get_decelerationMultiplier_10() const { return ___decelerationMultiplier_10; }
	inline int32_t* get_address_of_decelerationMultiplier_10() { return &___decelerationMultiplier_10; }
	inline void set_decelerationMultiplier_10(int32_t value)
	{
		___decelerationMultiplier_10 = value;
	}

	inline static int32_t get_offset_of_handbrakeDriftMultiplier_11() { return static_cast<int32_t>(offsetof(PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E, ___handbrakeDriftMultiplier_11)); }
	inline int32_t get_handbrakeDriftMultiplier_11() const { return ___handbrakeDriftMultiplier_11; }
	inline int32_t* get_address_of_handbrakeDriftMultiplier_11() { return &___handbrakeDriftMultiplier_11; }
	inline void set_handbrakeDriftMultiplier_11(int32_t value)
	{
		___handbrakeDriftMultiplier_11 = value;
	}

	inline static int32_t get_offset_of_bodyMassCenter_12() { return static_cast<int32_t>(offsetof(PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E, ___bodyMassCenter_12)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_bodyMassCenter_12() const { return ___bodyMassCenter_12; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_bodyMassCenter_12() { return &___bodyMassCenter_12; }
	inline void set_bodyMassCenter_12(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___bodyMassCenter_12 = value;
	}

	inline static int32_t get_offset_of_frontLeftMesh_13() { return static_cast<int32_t>(offsetof(PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E, ___frontLeftMesh_13)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_frontLeftMesh_13() const { return ___frontLeftMesh_13; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_frontLeftMesh_13() { return &___frontLeftMesh_13; }
	inline void set_frontLeftMesh_13(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___frontLeftMesh_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___frontLeftMesh_13), (void*)value);
	}

	inline static int32_t get_offset_of_frontLeftCollider_14() { return static_cast<int32_t>(offsetof(PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E, ___frontLeftCollider_14)); }
	inline WheelCollider_t57B08104FE16DFC3BF72826F7A3CCB8477C01779 * get_frontLeftCollider_14() const { return ___frontLeftCollider_14; }
	inline WheelCollider_t57B08104FE16DFC3BF72826F7A3CCB8477C01779 ** get_address_of_frontLeftCollider_14() { return &___frontLeftCollider_14; }
	inline void set_frontLeftCollider_14(WheelCollider_t57B08104FE16DFC3BF72826F7A3CCB8477C01779 * value)
	{
		___frontLeftCollider_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___frontLeftCollider_14), (void*)value);
	}

	inline static int32_t get_offset_of_frontRightMesh_15() { return static_cast<int32_t>(offsetof(PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E, ___frontRightMesh_15)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_frontRightMesh_15() const { return ___frontRightMesh_15; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_frontRightMesh_15() { return &___frontRightMesh_15; }
	inline void set_frontRightMesh_15(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___frontRightMesh_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___frontRightMesh_15), (void*)value);
	}

	inline static int32_t get_offset_of_frontRightCollider_16() { return static_cast<int32_t>(offsetof(PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E, ___frontRightCollider_16)); }
	inline WheelCollider_t57B08104FE16DFC3BF72826F7A3CCB8477C01779 * get_frontRightCollider_16() const { return ___frontRightCollider_16; }
	inline WheelCollider_t57B08104FE16DFC3BF72826F7A3CCB8477C01779 ** get_address_of_frontRightCollider_16() { return &___frontRightCollider_16; }
	inline void set_frontRightCollider_16(WheelCollider_t57B08104FE16DFC3BF72826F7A3CCB8477C01779 * value)
	{
		___frontRightCollider_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___frontRightCollider_16), (void*)value);
	}

	inline static int32_t get_offset_of_rearLeftMesh_17() { return static_cast<int32_t>(offsetof(PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E, ___rearLeftMesh_17)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_rearLeftMesh_17() const { return ___rearLeftMesh_17; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_rearLeftMesh_17() { return &___rearLeftMesh_17; }
	inline void set_rearLeftMesh_17(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___rearLeftMesh_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___rearLeftMesh_17), (void*)value);
	}

	inline static int32_t get_offset_of_rearLeftCollider_18() { return static_cast<int32_t>(offsetof(PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E, ___rearLeftCollider_18)); }
	inline WheelCollider_t57B08104FE16DFC3BF72826F7A3CCB8477C01779 * get_rearLeftCollider_18() const { return ___rearLeftCollider_18; }
	inline WheelCollider_t57B08104FE16DFC3BF72826F7A3CCB8477C01779 ** get_address_of_rearLeftCollider_18() { return &___rearLeftCollider_18; }
	inline void set_rearLeftCollider_18(WheelCollider_t57B08104FE16DFC3BF72826F7A3CCB8477C01779 * value)
	{
		___rearLeftCollider_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___rearLeftCollider_18), (void*)value);
	}

	inline static int32_t get_offset_of_rearRightMesh_19() { return static_cast<int32_t>(offsetof(PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E, ___rearRightMesh_19)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_rearRightMesh_19() const { return ___rearRightMesh_19; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_rearRightMesh_19() { return &___rearRightMesh_19; }
	inline void set_rearRightMesh_19(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___rearRightMesh_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___rearRightMesh_19), (void*)value);
	}

	inline static int32_t get_offset_of_rearRightCollider_20() { return static_cast<int32_t>(offsetof(PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E, ___rearRightCollider_20)); }
	inline WheelCollider_t57B08104FE16DFC3BF72826F7A3CCB8477C01779 * get_rearRightCollider_20() const { return ___rearRightCollider_20; }
	inline WheelCollider_t57B08104FE16DFC3BF72826F7A3CCB8477C01779 ** get_address_of_rearRightCollider_20() { return &___rearRightCollider_20; }
	inline void set_rearRightCollider_20(WheelCollider_t57B08104FE16DFC3BF72826F7A3CCB8477C01779 * value)
	{
		___rearRightCollider_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___rearRightCollider_20), (void*)value);
	}

	inline static int32_t get_offset_of_useEffects_21() { return static_cast<int32_t>(offsetof(PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E, ___useEffects_21)); }
	inline bool get_useEffects_21() const { return ___useEffects_21; }
	inline bool* get_address_of_useEffects_21() { return &___useEffects_21; }
	inline void set_useEffects_21(bool value)
	{
		___useEffects_21 = value;
	}

	inline static int32_t get_offset_of_RLWParticleSystem_22() { return static_cast<int32_t>(offsetof(PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E, ___RLWParticleSystem_22)); }
	inline ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * get_RLWParticleSystem_22() const { return ___RLWParticleSystem_22; }
	inline ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E ** get_address_of_RLWParticleSystem_22() { return &___RLWParticleSystem_22; }
	inline void set_RLWParticleSystem_22(ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * value)
	{
		___RLWParticleSystem_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___RLWParticleSystem_22), (void*)value);
	}

	inline static int32_t get_offset_of_RRWParticleSystem_23() { return static_cast<int32_t>(offsetof(PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E, ___RRWParticleSystem_23)); }
	inline ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * get_RRWParticleSystem_23() const { return ___RRWParticleSystem_23; }
	inline ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E ** get_address_of_RRWParticleSystem_23() { return &___RRWParticleSystem_23; }
	inline void set_RRWParticleSystem_23(ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * value)
	{
		___RRWParticleSystem_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___RRWParticleSystem_23), (void*)value);
	}

	inline static int32_t get_offset_of_RLWTireSkid_24() { return static_cast<int32_t>(offsetof(PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E, ___RLWTireSkid_24)); }
	inline TrailRenderer_t219A9B1F6C4B984AE4BEEC40F90665D122056A01 * get_RLWTireSkid_24() const { return ___RLWTireSkid_24; }
	inline TrailRenderer_t219A9B1F6C4B984AE4BEEC40F90665D122056A01 ** get_address_of_RLWTireSkid_24() { return &___RLWTireSkid_24; }
	inline void set_RLWTireSkid_24(TrailRenderer_t219A9B1F6C4B984AE4BEEC40F90665D122056A01 * value)
	{
		___RLWTireSkid_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___RLWTireSkid_24), (void*)value);
	}

	inline static int32_t get_offset_of_RRWTireSkid_25() { return static_cast<int32_t>(offsetof(PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E, ___RRWTireSkid_25)); }
	inline TrailRenderer_t219A9B1F6C4B984AE4BEEC40F90665D122056A01 * get_RRWTireSkid_25() const { return ___RRWTireSkid_25; }
	inline TrailRenderer_t219A9B1F6C4B984AE4BEEC40F90665D122056A01 ** get_address_of_RRWTireSkid_25() { return &___RRWTireSkid_25; }
	inline void set_RRWTireSkid_25(TrailRenderer_t219A9B1F6C4B984AE4BEEC40F90665D122056A01 * value)
	{
		___RRWTireSkid_25 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___RRWTireSkid_25), (void*)value);
	}

	inline static int32_t get_offset_of_useUI_26() { return static_cast<int32_t>(offsetof(PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E, ___useUI_26)); }
	inline bool get_useUI_26() const { return ___useUI_26; }
	inline bool* get_address_of_useUI_26() { return &___useUI_26; }
	inline void set_useUI_26(bool value)
	{
		___useUI_26 = value;
	}

	inline static int32_t get_offset_of_carSpeedText_27() { return static_cast<int32_t>(offsetof(PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E, ___carSpeedText_27)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_carSpeedText_27() const { return ___carSpeedText_27; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_carSpeedText_27() { return &___carSpeedText_27; }
	inline void set_carSpeedText_27(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___carSpeedText_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___carSpeedText_27), (void*)value);
	}

	inline static int32_t get_offset_of_useSounds_28() { return static_cast<int32_t>(offsetof(PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E, ___useSounds_28)); }
	inline bool get_useSounds_28() const { return ___useSounds_28; }
	inline bool* get_address_of_useSounds_28() { return &___useSounds_28; }
	inline void set_useSounds_28(bool value)
	{
		___useSounds_28 = value;
	}

	inline static int32_t get_offset_of_carEngineSound_29() { return static_cast<int32_t>(offsetof(PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E, ___carEngineSound_29)); }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * get_carEngineSound_29() const { return ___carEngineSound_29; }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B ** get_address_of_carEngineSound_29() { return &___carEngineSound_29; }
	inline void set_carEngineSound_29(AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * value)
	{
		___carEngineSound_29 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___carEngineSound_29), (void*)value);
	}

	inline static int32_t get_offset_of_tireScreechSound_30() { return static_cast<int32_t>(offsetof(PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E, ___tireScreechSound_30)); }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * get_tireScreechSound_30() const { return ___tireScreechSound_30; }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B ** get_address_of_tireScreechSound_30() { return &___tireScreechSound_30; }
	inline void set_tireScreechSound_30(AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * value)
	{
		___tireScreechSound_30 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tireScreechSound_30), (void*)value);
	}

	inline static int32_t get_offset_of_initialCarEngineSoundPitch_31() { return static_cast<int32_t>(offsetof(PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E, ___initialCarEngineSoundPitch_31)); }
	inline float get_initialCarEngineSoundPitch_31() const { return ___initialCarEngineSoundPitch_31; }
	inline float* get_address_of_initialCarEngineSoundPitch_31() { return &___initialCarEngineSoundPitch_31; }
	inline void set_initialCarEngineSoundPitch_31(float value)
	{
		___initialCarEngineSoundPitch_31 = value;
	}

	inline static int32_t get_offset_of_useTouchControls_32() { return static_cast<int32_t>(offsetof(PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E, ___useTouchControls_32)); }
	inline bool get_useTouchControls_32() const { return ___useTouchControls_32; }
	inline bool* get_address_of_useTouchControls_32() { return &___useTouchControls_32; }
	inline void set_useTouchControls_32(bool value)
	{
		___useTouchControls_32 = value;
	}

	inline static int32_t get_offset_of_throttleButton_33() { return static_cast<int32_t>(offsetof(PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E, ___throttleButton_33)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_throttleButton_33() const { return ___throttleButton_33; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_throttleButton_33() { return &___throttleButton_33; }
	inline void set_throttleButton_33(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___throttleButton_33 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___throttleButton_33), (void*)value);
	}

	inline static int32_t get_offset_of_throttlePTI_34() { return static_cast<int32_t>(offsetof(PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E, ___throttlePTI_34)); }
	inline PrometeoTouchInput_t1C5A9B19512A75AFEAD2E916C6F53F597ADF73CF * get_throttlePTI_34() const { return ___throttlePTI_34; }
	inline PrometeoTouchInput_t1C5A9B19512A75AFEAD2E916C6F53F597ADF73CF ** get_address_of_throttlePTI_34() { return &___throttlePTI_34; }
	inline void set_throttlePTI_34(PrometeoTouchInput_t1C5A9B19512A75AFEAD2E916C6F53F597ADF73CF * value)
	{
		___throttlePTI_34 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___throttlePTI_34), (void*)value);
	}

	inline static int32_t get_offset_of_reverseButton_35() { return static_cast<int32_t>(offsetof(PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E, ___reverseButton_35)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_reverseButton_35() const { return ___reverseButton_35; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_reverseButton_35() { return &___reverseButton_35; }
	inline void set_reverseButton_35(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___reverseButton_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___reverseButton_35), (void*)value);
	}

	inline static int32_t get_offset_of_reversePTI_36() { return static_cast<int32_t>(offsetof(PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E, ___reversePTI_36)); }
	inline PrometeoTouchInput_t1C5A9B19512A75AFEAD2E916C6F53F597ADF73CF * get_reversePTI_36() const { return ___reversePTI_36; }
	inline PrometeoTouchInput_t1C5A9B19512A75AFEAD2E916C6F53F597ADF73CF ** get_address_of_reversePTI_36() { return &___reversePTI_36; }
	inline void set_reversePTI_36(PrometeoTouchInput_t1C5A9B19512A75AFEAD2E916C6F53F597ADF73CF * value)
	{
		___reversePTI_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___reversePTI_36), (void*)value);
	}

	inline static int32_t get_offset_of_turnRightButton_37() { return static_cast<int32_t>(offsetof(PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E, ___turnRightButton_37)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_turnRightButton_37() const { return ___turnRightButton_37; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_turnRightButton_37() { return &___turnRightButton_37; }
	inline void set_turnRightButton_37(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___turnRightButton_37 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___turnRightButton_37), (void*)value);
	}

	inline static int32_t get_offset_of_turnRightPTI_38() { return static_cast<int32_t>(offsetof(PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E, ___turnRightPTI_38)); }
	inline PrometeoTouchInput_t1C5A9B19512A75AFEAD2E916C6F53F597ADF73CF * get_turnRightPTI_38() const { return ___turnRightPTI_38; }
	inline PrometeoTouchInput_t1C5A9B19512A75AFEAD2E916C6F53F597ADF73CF ** get_address_of_turnRightPTI_38() { return &___turnRightPTI_38; }
	inline void set_turnRightPTI_38(PrometeoTouchInput_t1C5A9B19512A75AFEAD2E916C6F53F597ADF73CF * value)
	{
		___turnRightPTI_38 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___turnRightPTI_38), (void*)value);
	}

	inline static int32_t get_offset_of_turnLeftButton_39() { return static_cast<int32_t>(offsetof(PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E, ___turnLeftButton_39)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_turnLeftButton_39() const { return ___turnLeftButton_39; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_turnLeftButton_39() { return &___turnLeftButton_39; }
	inline void set_turnLeftButton_39(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___turnLeftButton_39 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___turnLeftButton_39), (void*)value);
	}

	inline static int32_t get_offset_of_turnLeftPTI_40() { return static_cast<int32_t>(offsetof(PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E, ___turnLeftPTI_40)); }
	inline PrometeoTouchInput_t1C5A9B19512A75AFEAD2E916C6F53F597ADF73CF * get_turnLeftPTI_40() const { return ___turnLeftPTI_40; }
	inline PrometeoTouchInput_t1C5A9B19512A75AFEAD2E916C6F53F597ADF73CF ** get_address_of_turnLeftPTI_40() { return &___turnLeftPTI_40; }
	inline void set_turnLeftPTI_40(PrometeoTouchInput_t1C5A9B19512A75AFEAD2E916C6F53F597ADF73CF * value)
	{
		___turnLeftPTI_40 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___turnLeftPTI_40), (void*)value);
	}

	inline static int32_t get_offset_of_handbrakeButton_41() { return static_cast<int32_t>(offsetof(PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E, ___handbrakeButton_41)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_handbrakeButton_41() const { return ___handbrakeButton_41; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_handbrakeButton_41() { return &___handbrakeButton_41; }
	inline void set_handbrakeButton_41(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___handbrakeButton_41 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___handbrakeButton_41), (void*)value);
	}

	inline static int32_t get_offset_of_handbrakePTI_42() { return static_cast<int32_t>(offsetof(PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E, ___handbrakePTI_42)); }
	inline PrometeoTouchInput_t1C5A9B19512A75AFEAD2E916C6F53F597ADF73CF * get_handbrakePTI_42() const { return ___handbrakePTI_42; }
	inline PrometeoTouchInput_t1C5A9B19512A75AFEAD2E916C6F53F597ADF73CF ** get_address_of_handbrakePTI_42() { return &___handbrakePTI_42; }
	inline void set_handbrakePTI_42(PrometeoTouchInput_t1C5A9B19512A75AFEAD2E916C6F53F597ADF73CF * value)
	{
		___handbrakePTI_42 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___handbrakePTI_42), (void*)value);
	}

	inline static int32_t get_offset_of_carSpeed_43() { return static_cast<int32_t>(offsetof(PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E, ___carSpeed_43)); }
	inline float get_carSpeed_43() const { return ___carSpeed_43; }
	inline float* get_address_of_carSpeed_43() { return &___carSpeed_43; }
	inline void set_carSpeed_43(float value)
	{
		___carSpeed_43 = value;
	}

	inline static int32_t get_offset_of_isDrifting_44() { return static_cast<int32_t>(offsetof(PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E, ___isDrifting_44)); }
	inline bool get_isDrifting_44() const { return ___isDrifting_44; }
	inline bool* get_address_of_isDrifting_44() { return &___isDrifting_44; }
	inline void set_isDrifting_44(bool value)
	{
		___isDrifting_44 = value;
	}

	inline static int32_t get_offset_of_isTractionLocked_45() { return static_cast<int32_t>(offsetof(PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E, ___isTractionLocked_45)); }
	inline bool get_isTractionLocked_45() const { return ___isTractionLocked_45; }
	inline bool* get_address_of_isTractionLocked_45() { return &___isTractionLocked_45; }
	inline void set_isTractionLocked_45(bool value)
	{
		___isTractionLocked_45 = value;
	}

	inline static int32_t get_offset_of_carRigidbody_46() { return static_cast<int32_t>(offsetof(PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E, ___carRigidbody_46)); }
	inline Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * get_carRigidbody_46() const { return ___carRigidbody_46; }
	inline Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A ** get_address_of_carRigidbody_46() { return &___carRigidbody_46; }
	inline void set_carRigidbody_46(Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * value)
	{
		___carRigidbody_46 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___carRigidbody_46), (void*)value);
	}

	inline static int32_t get_offset_of_steeringAxis_47() { return static_cast<int32_t>(offsetof(PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E, ___steeringAxis_47)); }
	inline float get_steeringAxis_47() const { return ___steeringAxis_47; }
	inline float* get_address_of_steeringAxis_47() { return &___steeringAxis_47; }
	inline void set_steeringAxis_47(float value)
	{
		___steeringAxis_47 = value;
	}

	inline static int32_t get_offset_of_throttleAxis_48() { return static_cast<int32_t>(offsetof(PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E, ___throttleAxis_48)); }
	inline float get_throttleAxis_48() const { return ___throttleAxis_48; }
	inline float* get_address_of_throttleAxis_48() { return &___throttleAxis_48; }
	inline void set_throttleAxis_48(float value)
	{
		___throttleAxis_48 = value;
	}

	inline static int32_t get_offset_of_driftingAxis_49() { return static_cast<int32_t>(offsetof(PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E, ___driftingAxis_49)); }
	inline float get_driftingAxis_49() const { return ___driftingAxis_49; }
	inline float* get_address_of_driftingAxis_49() { return &___driftingAxis_49; }
	inline void set_driftingAxis_49(float value)
	{
		___driftingAxis_49 = value;
	}

	inline static int32_t get_offset_of_localVelocityZ_50() { return static_cast<int32_t>(offsetof(PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E, ___localVelocityZ_50)); }
	inline float get_localVelocityZ_50() const { return ___localVelocityZ_50; }
	inline float* get_address_of_localVelocityZ_50() { return &___localVelocityZ_50; }
	inline void set_localVelocityZ_50(float value)
	{
		___localVelocityZ_50 = value;
	}

	inline static int32_t get_offset_of_localVelocityX_51() { return static_cast<int32_t>(offsetof(PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E, ___localVelocityX_51)); }
	inline float get_localVelocityX_51() const { return ___localVelocityX_51; }
	inline float* get_address_of_localVelocityX_51() { return &___localVelocityX_51; }
	inline void set_localVelocityX_51(float value)
	{
		___localVelocityX_51 = value;
	}

	inline static int32_t get_offset_of_deceleratingCar_52() { return static_cast<int32_t>(offsetof(PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E, ___deceleratingCar_52)); }
	inline bool get_deceleratingCar_52() const { return ___deceleratingCar_52; }
	inline bool* get_address_of_deceleratingCar_52() { return &___deceleratingCar_52; }
	inline void set_deceleratingCar_52(bool value)
	{
		___deceleratingCar_52 = value;
	}

	inline static int32_t get_offset_of_touchControlsSetup_53() { return static_cast<int32_t>(offsetof(PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E, ___touchControlsSetup_53)); }
	inline bool get_touchControlsSetup_53() const { return ___touchControlsSetup_53; }
	inline bool* get_address_of_touchControlsSetup_53() { return &___touchControlsSetup_53; }
	inline void set_touchControlsSetup_53(bool value)
	{
		___touchControlsSetup_53 = value;
	}

	inline static int32_t get_offset_of_FLwheelFriction_54() { return static_cast<int32_t>(offsetof(PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E, ___FLwheelFriction_54)); }
	inline WheelFrictionCurve_t96AB1E3B37FC1E7369962A2E712ECD9A2AFE723D  get_FLwheelFriction_54() const { return ___FLwheelFriction_54; }
	inline WheelFrictionCurve_t96AB1E3B37FC1E7369962A2E712ECD9A2AFE723D * get_address_of_FLwheelFriction_54() { return &___FLwheelFriction_54; }
	inline void set_FLwheelFriction_54(WheelFrictionCurve_t96AB1E3B37FC1E7369962A2E712ECD9A2AFE723D  value)
	{
		___FLwheelFriction_54 = value;
	}

	inline static int32_t get_offset_of_FLWextremumSlip_55() { return static_cast<int32_t>(offsetof(PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E, ___FLWextremumSlip_55)); }
	inline float get_FLWextremumSlip_55() const { return ___FLWextremumSlip_55; }
	inline float* get_address_of_FLWextremumSlip_55() { return &___FLWextremumSlip_55; }
	inline void set_FLWextremumSlip_55(float value)
	{
		___FLWextremumSlip_55 = value;
	}

	inline static int32_t get_offset_of_FRwheelFriction_56() { return static_cast<int32_t>(offsetof(PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E, ___FRwheelFriction_56)); }
	inline WheelFrictionCurve_t96AB1E3B37FC1E7369962A2E712ECD9A2AFE723D  get_FRwheelFriction_56() const { return ___FRwheelFriction_56; }
	inline WheelFrictionCurve_t96AB1E3B37FC1E7369962A2E712ECD9A2AFE723D * get_address_of_FRwheelFriction_56() { return &___FRwheelFriction_56; }
	inline void set_FRwheelFriction_56(WheelFrictionCurve_t96AB1E3B37FC1E7369962A2E712ECD9A2AFE723D  value)
	{
		___FRwheelFriction_56 = value;
	}

	inline static int32_t get_offset_of_FRWextremumSlip_57() { return static_cast<int32_t>(offsetof(PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E, ___FRWextremumSlip_57)); }
	inline float get_FRWextremumSlip_57() const { return ___FRWextremumSlip_57; }
	inline float* get_address_of_FRWextremumSlip_57() { return &___FRWextremumSlip_57; }
	inline void set_FRWextremumSlip_57(float value)
	{
		___FRWextremumSlip_57 = value;
	}

	inline static int32_t get_offset_of_RLwheelFriction_58() { return static_cast<int32_t>(offsetof(PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E, ___RLwheelFriction_58)); }
	inline WheelFrictionCurve_t96AB1E3B37FC1E7369962A2E712ECD9A2AFE723D  get_RLwheelFriction_58() const { return ___RLwheelFriction_58; }
	inline WheelFrictionCurve_t96AB1E3B37FC1E7369962A2E712ECD9A2AFE723D * get_address_of_RLwheelFriction_58() { return &___RLwheelFriction_58; }
	inline void set_RLwheelFriction_58(WheelFrictionCurve_t96AB1E3B37FC1E7369962A2E712ECD9A2AFE723D  value)
	{
		___RLwheelFriction_58 = value;
	}

	inline static int32_t get_offset_of_RLWextremumSlip_59() { return static_cast<int32_t>(offsetof(PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E, ___RLWextremumSlip_59)); }
	inline float get_RLWextremumSlip_59() const { return ___RLWextremumSlip_59; }
	inline float* get_address_of_RLWextremumSlip_59() { return &___RLWextremumSlip_59; }
	inline void set_RLWextremumSlip_59(float value)
	{
		___RLWextremumSlip_59 = value;
	}

	inline static int32_t get_offset_of_RRwheelFriction_60() { return static_cast<int32_t>(offsetof(PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E, ___RRwheelFriction_60)); }
	inline WheelFrictionCurve_t96AB1E3B37FC1E7369962A2E712ECD9A2AFE723D  get_RRwheelFriction_60() const { return ___RRwheelFriction_60; }
	inline WheelFrictionCurve_t96AB1E3B37FC1E7369962A2E712ECD9A2AFE723D * get_address_of_RRwheelFriction_60() { return &___RRwheelFriction_60; }
	inline void set_RRwheelFriction_60(WheelFrictionCurve_t96AB1E3B37FC1E7369962A2E712ECD9A2AFE723D  value)
	{
		___RRwheelFriction_60 = value;
	}

	inline static int32_t get_offset_of_RRWextremumSlip_61() { return static_cast<int32_t>(offsetof(PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E, ___RRWextremumSlip_61)); }
	inline float get_RRWextremumSlip_61() const { return ___RRWextremumSlip_61; }
	inline float* get_address_of_RRWextremumSlip_61() { return &___RRWextremumSlip_61; }
	inline void set_RRWextremumSlip_61(float value)
	{
		___RRWextremumSlip_61 = value;
	}
};


// PrometeoTouchInput
struct  PrometeoTouchInput_t1C5A9B19512A75AFEAD2E916C6F53F597ADF73CF  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Boolean PrometeoTouchInput::changeScaleOnPressed
	bool ___changeScaleOnPressed_4;
	// System.Boolean PrometeoTouchInput::buttonPressed
	bool ___buttonPressed_5;
	// UnityEngine.RectTransform PrometeoTouchInput::rectTransform
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___rectTransform_6;
	// UnityEngine.Vector3 PrometeoTouchInput::initialScale
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___initialScale_7;
	// System.Single PrometeoTouchInput::scaleDownMultiplier
	float ___scaleDownMultiplier_8;

public:
	inline static int32_t get_offset_of_changeScaleOnPressed_4() { return static_cast<int32_t>(offsetof(PrometeoTouchInput_t1C5A9B19512A75AFEAD2E916C6F53F597ADF73CF, ___changeScaleOnPressed_4)); }
	inline bool get_changeScaleOnPressed_4() const { return ___changeScaleOnPressed_4; }
	inline bool* get_address_of_changeScaleOnPressed_4() { return &___changeScaleOnPressed_4; }
	inline void set_changeScaleOnPressed_4(bool value)
	{
		___changeScaleOnPressed_4 = value;
	}

	inline static int32_t get_offset_of_buttonPressed_5() { return static_cast<int32_t>(offsetof(PrometeoTouchInput_t1C5A9B19512A75AFEAD2E916C6F53F597ADF73CF, ___buttonPressed_5)); }
	inline bool get_buttonPressed_5() const { return ___buttonPressed_5; }
	inline bool* get_address_of_buttonPressed_5() { return &___buttonPressed_5; }
	inline void set_buttonPressed_5(bool value)
	{
		___buttonPressed_5 = value;
	}

	inline static int32_t get_offset_of_rectTransform_6() { return static_cast<int32_t>(offsetof(PrometeoTouchInput_t1C5A9B19512A75AFEAD2E916C6F53F597ADF73CF, ___rectTransform_6)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_rectTransform_6() const { return ___rectTransform_6; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_rectTransform_6() { return &___rectTransform_6; }
	inline void set_rectTransform_6(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___rectTransform_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___rectTransform_6), (void*)value);
	}

	inline static int32_t get_offset_of_initialScale_7() { return static_cast<int32_t>(offsetof(PrometeoTouchInput_t1C5A9B19512A75AFEAD2E916C6F53F597ADF73CF, ___initialScale_7)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_initialScale_7() const { return ___initialScale_7; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_initialScale_7() { return &___initialScale_7; }
	inline void set_initialScale_7(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___initialScale_7 = value;
	}

	inline static int32_t get_offset_of_scaleDownMultiplier_8() { return static_cast<int32_t>(offsetof(PrometeoTouchInput_t1C5A9B19512A75AFEAD2E916C6F53F597ADF73CF, ___scaleDownMultiplier_8)); }
	inline float get_scaleDownMultiplier_8() const { return ___scaleDownMultiplier_8; }
	inline float* get_address_of_scaleDownMultiplier_8() { return &___scaleDownMultiplier_8; }
	inline void set_scaleDownMultiplier_8(float value)
	{
		___scaleDownMultiplier_8 = value;
	}
};


// RoadDecider
struct  RoadDecider_tCF6285CFDF778A9348BC78EF830371F1855A9E61  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// RoadDecider/ControlTypes RoadDecider::ControlType
	int32_t ___ControlType_4;
	// RoadDecider/Directions RoadDecider::Direction
	int32_t ___Direction_5;
	// UnityEngine.GameObject RoadDecider::GoTo
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___GoTo_6;

public:
	inline static int32_t get_offset_of_ControlType_4() { return static_cast<int32_t>(offsetof(RoadDecider_tCF6285CFDF778A9348BC78EF830371F1855A9E61, ___ControlType_4)); }
	inline int32_t get_ControlType_4() const { return ___ControlType_4; }
	inline int32_t* get_address_of_ControlType_4() { return &___ControlType_4; }
	inline void set_ControlType_4(int32_t value)
	{
		___ControlType_4 = value;
	}

	inline static int32_t get_offset_of_Direction_5() { return static_cast<int32_t>(offsetof(RoadDecider_tCF6285CFDF778A9348BC78EF830371F1855A9E61, ___Direction_5)); }
	inline int32_t get_Direction_5() const { return ___Direction_5; }
	inline int32_t* get_address_of_Direction_5() { return &___Direction_5; }
	inline void set_Direction_5(int32_t value)
	{
		___Direction_5 = value;
	}

	inline static int32_t get_offset_of_GoTo_6() { return static_cast<int32_t>(offsetof(RoadDecider_tCF6285CFDF778A9348BC78EF830371F1855A9E61, ___GoTo_6)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_GoTo_6() const { return ___GoTo_6; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_GoTo_6() { return &___GoTo_6; }
	inline void set_GoTo_6(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___GoTo_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___GoTo_6), (void*)value);
	}
};


// SignalsCar
struct  SignalsCar_t381616BF85EDCCF005FD247DC7E72DB6EAD4DAB0  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Boolean SignalsCar::hazardActive
	bool ___hazardActive_5;
	// System.Boolean SignalsCar::rightActive
	bool ___rightActive_6;
	// System.Boolean SignalsCar::leftActive
	bool ___leftActive_7;
	// System.Int32 SignalsCar::timer
	int32_t ___timer_8;
	// UnityEngine.GameObject SignalsCar::rightLight
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___rightLight_9;
	// UnityEngine.GameObject SignalsCar::leftLight
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___leftLight_10;
	// UnityEngine.GameObject SignalsCar::hazardLight
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___hazardLight_11;
	// UnityEngine.GameObject SignalsCar::vrCam
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___vrCam_12;
	// UnityEngine.GameObject SignalsCar::vrComponent
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___vrComponent_13;
	// UnityEngine.GameObject SignalsCar::normalCam
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___normalCam_14;
	// System.Boolean SignalsCar::vrEnabled
	bool ___vrEnabled_15;
	// UnityEngine.GameObject SignalsCar::LeftPointLight
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___LeftPointLight_16;
	// UnityEngine.GameObject SignalsCar::RightPointLight
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___RightPointLight_17;
	// UnityEngine.Color SignalsCar::signalColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___signalColor_18;
	// UnityEngine.Color SignalsCar::hazardColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___hazardColor_19;
	// UnityEngine.AudioSource SignalsCar::turnSignalSound
	AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * ___turnSignalSound_20;
	// UnityEngine.AudioSource SignalsCar::hazardSound
	AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * ___hazardSound_21;
	// RoadDecider/Directions SignalsCar::direction
	int32_t ___direction_22;

public:
	inline static int32_t get_offset_of_hazardActive_5() { return static_cast<int32_t>(offsetof(SignalsCar_t381616BF85EDCCF005FD247DC7E72DB6EAD4DAB0, ___hazardActive_5)); }
	inline bool get_hazardActive_5() const { return ___hazardActive_5; }
	inline bool* get_address_of_hazardActive_5() { return &___hazardActive_5; }
	inline void set_hazardActive_5(bool value)
	{
		___hazardActive_5 = value;
	}

	inline static int32_t get_offset_of_rightActive_6() { return static_cast<int32_t>(offsetof(SignalsCar_t381616BF85EDCCF005FD247DC7E72DB6EAD4DAB0, ___rightActive_6)); }
	inline bool get_rightActive_6() const { return ___rightActive_6; }
	inline bool* get_address_of_rightActive_6() { return &___rightActive_6; }
	inline void set_rightActive_6(bool value)
	{
		___rightActive_6 = value;
	}

	inline static int32_t get_offset_of_leftActive_7() { return static_cast<int32_t>(offsetof(SignalsCar_t381616BF85EDCCF005FD247DC7E72DB6EAD4DAB0, ___leftActive_7)); }
	inline bool get_leftActive_7() const { return ___leftActive_7; }
	inline bool* get_address_of_leftActive_7() { return &___leftActive_7; }
	inline void set_leftActive_7(bool value)
	{
		___leftActive_7 = value;
	}

	inline static int32_t get_offset_of_timer_8() { return static_cast<int32_t>(offsetof(SignalsCar_t381616BF85EDCCF005FD247DC7E72DB6EAD4DAB0, ___timer_8)); }
	inline int32_t get_timer_8() const { return ___timer_8; }
	inline int32_t* get_address_of_timer_8() { return &___timer_8; }
	inline void set_timer_8(int32_t value)
	{
		___timer_8 = value;
	}

	inline static int32_t get_offset_of_rightLight_9() { return static_cast<int32_t>(offsetof(SignalsCar_t381616BF85EDCCF005FD247DC7E72DB6EAD4DAB0, ___rightLight_9)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_rightLight_9() const { return ___rightLight_9; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_rightLight_9() { return &___rightLight_9; }
	inline void set_rightLight_9(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___rightLight_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___rightLight_9), (void*)value);
	}

	inline static int32_t get_offset_of_leftLight_10() { return static_cast<int32_t>(offsetof(SignalsCar_t381616BF85EDCCF005FD247DC7E72DB6EAD4DAB0, ___leftLight_10)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_leftLight_10() const { return ___leftLight_10; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_leftLight_10() { return &___leftLight_10; }
	inline void set_leftLight_10(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___leftLight_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___leftLight_10), (void*)value);
	}

	inline static int32_t get_offset_of_hazardLight_11() { return static_cast<int32_t>(offsetof(SignalsCar_t381616BF85EDCCF005FD247DC7E72DB6EAD4DAB0, ___hazardLight_11)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_hazardLight_11() const { return ___hazardLight_11; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_hazardLight_11() { return &___hazardLight_11; }
	inline void set_hazardLight_11(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___hazardLight_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___hazardLight_11), (void*)value);
	}

	inline static int32_t get_offset_of_vrCam_12() { return static_cast<int32_t>(offsetof(SignalsCar_t381616BF85EDCCF005FD247DC7E72DB6EAD4DAB0, ___vrCam_12)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_vrCam_12() const { return ___vrCam_12; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_vrCam_12() { return &___vrCam_12; }
	inline void set_vrCam_12(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___vrCam_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___vrCam_12), (void*)value);
	}

	inline static int32_t get_offset_of_vrComponent_13() { return static_cast<int32_t>(offsetof(SignalsCar_t381616BF85EDCCF005FD247DC7E72DB6EAD4DAB0, ___vrComponent_13)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_vrComponent_13() const { return ___vrComponent_13; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_vrComponent_13() { return &___vrComponent_13; }
	inline void set_vrComponent_13(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___vrComponent_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___vrComponent_13), (void*)value);
	}

	inline static int32_t get_offset_of_normalCam_14() { return static_cast<int32_t>(offsetof(SignalsCar_t381616BF85EDCCF005FD247DC7E72DB6EAD4DAB0, ___normalCam_14)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_normalCam_14() const { return ___normalCam_14; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_normalCam_14() { return &___normalCam_14; }
	inline void set_normalCam_14(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___normalCam_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___normalCam_14), (void*)value);
	}

	inline static int32_t get_offset_of_vrEnabled_15() { return static_cast<int32_t>(offsetof(SignalsCar_t381616BF85EDCCF005FD247DC7E72DB6EAD4DAB0, ___vrEnabled_15)); }
	inline bool get_vrEnabled_15() const { return ___vrEnabled_15; }
	inline bool* get_address_of_vrEnabled_15() { return &___vrEnabled_15; }
	inline void set_vrEnabled_15(bool value)
	{
		___vrEnabled_15 = value;
	}

	inline static int32_t get_offset_of_LeftPointLight_16() { return static_cast<int32_t>(offsetof(SignalsCar_t381616BF85EDCCF005FD247DC7E72DB6EAD4DAB0, ___LeftPointLight_16)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_LeftPointLight_16() const { return ___LeftPointLight_16; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_LeftPointLight_16() { return &___LeftPointLight_16; }
	inline void set_LeftPointLight_16(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___LeftPointLight_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___LeftPointLight_16), (void*)value);
	}

	inline static int32_t get_offset_of_RightPointLight_17() { return static_cast<int32_t>(offsetof(SignalsCar_t381616BF85EDCCF005FD247DC7E72DB6EAD4DAB0, ___RightPointLight_17)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_RightPointLight_17() const { return ___RightPointLight_17; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_RightPointLight_17() { return &___RightPointLight_17; }
	inline void set_RightPointLight_17(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___RightPointLight_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___RightPointLight_17), (void*)value);
	}

	inline static int32_t get_offset_of_signalColor_18() { return static_cast<int32_t>(offsetof(SignalsCar_t381616BF85EDCCF005FD247DC7E72DB6EAD4DAB0, ___signalColor_18)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_signalColor_18() const { return ___signalColor_18; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_signalColor_18() { return &___signalColor_18; }
	inline void set_signalColor_18(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___signalColor_18 = value;
	}

	inline static int32_t get_offset_of_hazardColor_19() { return static_cast<int32_t>(offsetof(SignalsCar_t381616BF85EDCCF005FD247DC7E72DB6EAD4DAB0, ___hazardColor_19)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_hazardColor_19() const { return ___hazardColor_19; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_hazardColor_19() { return &___hazardColor_19; }
	inline void set_hazardColor_19(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___hazardColor_19 = value;
	}

	inline static int32_t get_offset_of_turnSignalSound_20() { return static_cast<int32_t>(offsetof(SignalsCar_t381616BF85EDCCF005FD247DC7E72DB6EAD4DAB0, ___turnSignalSound_20)); }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * get_turnSignalSound_20() const { return ___turnSignalSound_20; }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B ** get_address_of_turnSignalSound_20() { return &___turnSignalSound_20; }
	inline void set_turnSignalSound_20(AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * value)
	{
		___turnSignalSound_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___turnSignalSound_20), (void*)value);
	}

	inline static int32_t get_offset_of_hazardSound_21() { return static_cast<int32_t>(offsetof(SignalsCar_t381616BF85EDCCF005FD247DC7E72DB6EAD4DAB0, ___hazardSound_21)); }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * get_hazardSound_21() const { return ___hazardSound_21; }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B ** get_address_of_hazardSound_21() { return &___hazardSound_21; }
	inline void set_hazardSound_21(AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * value)
	{
		___hazardSound_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___hazardSound_21), (void*)value);
	}

	inline static int32_t get_offset_of_direction_22() { return static_cast<int32_t>(offsetof(SignalsCar_t381616BF85EDCCF005FD247DC7E72DB6EAD4DAB0, ___direction_22)); }
	inline int32_t get_direction_22() const { return ___direction_22; }
	inline int32_t* get_address_of_direction_22() { return &___direction_22; }
	inline void set_direction_22(int32_t value)
	{
		___direction_22 = value;
	}
};


// StarterAssets.StarterAssetsInputs
struct  StarterAssetsInputs_tE7DDADB7E0E59B93F86B819E96C2523BC28F16DC  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Vector2 StarterAssets.StarterAssetsInputs::move
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___move_4;
	// UnityEngine.Vector2 StarterAssets.StarterAssetsInputs::look
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___look_5;
	// System.Boolean StarterAssets.StarterAssetsInputs::jump
	bool ___jump_6;
	// System.Boolean StarterAssets.StarterAssetsInputs::sprint
	bool ___sprint_7;
	// System.Boolean StarterAssets.StarterAssetsInputs::analogMovement
	bool ___analogMovement_8;
	// System.Boolean StarterAssets.StarterAssetsInputs::cursorLocked
	bool ___cursorLocked_9;
	// System.Boolean StarterAssets.StarterAssetsInputs::cursorInputForLook
	bool ___cursorInputForLook_10;

public:
	inline static int32_t get_offset_of_move_4() { return static_cast<int32_t>(offsetof(StarterAssetsInputs_tE7DDADB7E0E59B93F86B819E96C2523BC28F16DC, ___move_4)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_move_4() const { return ___move_4; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_move_4() { return &___move_4; }
	inline void set_move_4(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___move_4 = value;
	}

	inline static int32_t get_offset_of_look_5() { return static_cast<int32_t>(offsetof(StarterAssetsInputs_tE7DDADB7E0E59B93F86B819E96C2523BC28F16DC, ___look_5)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_look_5() const { return ___look_5; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_look_5() { return &___look_5; }
	inline void set_look_5(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___look_5 = value;
	}

	inline static int32_t get_offset_of_jump_6() { return static_cast<int32_t>(offsetof(StarterAssetsInputs_tE7DDADB7E0E59B93F86B819E96C2523BC28F16DC, ___jump_6)); }
	inline bool get_jump_6() const { return ___jump_6; }
	inline bool* get_address_of_jump_6() { return &___jump_6; }
	inline void set_jump_6(bool value)
	{
		___jump_6 = value;
	}

	inline static int32_t get_offset_of_sprint_7() { return static_cast<int32_t>(offsetof(StarterAssetsInputs_tE7DDADB7E0E59B93F86B819E96C2523BC28F16DC, ___sprint_7)); }
	inline bool get_sprint_7() const { return ___sprint_7; }
	inline bool* get_address_of_sprint_7() { return &___sprint_7; }
	inline void set_sprint_7(bool value)
	{
		___sprint_7 = value;
	}

	inline static int32_t get_offset_of_analogMovement_8() { return static_cast<int32_t>(offsetof(StarterAssetsInputs_tE7DDADB7E0E59B93F86B819E96C2523BC28F16DC, ___analogMovement_8)); }
	inline bool get_analogMovement_8() const { return ___analogMovement_8; }
	inline bool* get_address_of_analogMovement_8() { return &___analogMovement_8; }
	inline void set_analogMovement_8(bool value)
	{
		___analogMovement_8 = value;
	}

	inline static int32_t get_offset_of_cursorLocked_9() { return static_cast<int32_t>(offsetof(StarterAssetsInputs_tE7DDADB7E0E59B93F86B819E96C2523BC28F16DC, ___cursorLocked_9)); }
	inline bool get_cursorLocked_9() const { return ___cursorLocked_9; }
	inline bool* get_address_of_cursorLocked_9() { return &___cursorLocked_9; }
	inline void set_cursorLocked_9(bool value)
	{
		___cursorLocked_9 = value;
	}

	inline static int32_t get_offset_of_cursorInputForLook_10() { return static_cast<int32_t>(offsetof(StarterAssetsInputs_tE7DDADB7E0E59B93F86B819E96C2523BC28F16DC, ___cursorInputForLook_10)); }
	inline bool get_cursorInputForLook_10() const { return ___cursorInputForLook_10; }
	inline bool* get_address_of_cursorInputForLook_10() { return &___cursorInputForLook_10; }
	inline void set_cursorInputForLook_10(bool value)
	{
		___cursorInputForLook_10 = value;
	}
};


// StarterAssets.ThirdPersonController
struct  ThirdPersonController_tCA99A63A4BE57B6A9950FFA8CFFABE8F82AD0903  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Single StarterAssets.ThirdPersonController::MoveSpeed
	float ___MoveSpeed_4;
	// System.Single StarterAssets.ThirdPersonController::SprintSpeed
	float ___SprintSpeed_5;
	// System.Single StarterAssets.ThirdPersonController::RotationSmoothTime
	float ___RotationSmoothTime_6;
	// System.Single StarterAssets.ThirdPersonController::SpeedChangeRate
	float ___SpeedChangeRate_7;
	// UnityEngine.AudioClip StarterAssets.ThirdPersonController::LandingAudioClip
	AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * ___LandingAudioClip_8;
	// UnityEngine.AudioClip[] StarterAssets.ThirdPersonController::FootstepAudioClips
	AudioClipU5BU5D_t9BA3E7C4B62164BA2E054F3A82CD6CE6EB273CBE* ___FootstepAudioClips_9;
	// System.Single StarterAssets.ThirdPersonController::FootstepAudioVolume
	float ___FootstepAudioVolume_10;
	// System.Single StarterAssets.ThirdPersonController::JumpHeight
	float ___JumpHeight_11;
	// System.Single StarterAssets.ThirdPersonController::Gravity
	float ___Gravity_12;
	// System.Single StarterAssets.ThirdPersonController::JumpTimeout
	float ___JumpTimeout_13;
	// System.Single StarterAssets.ThirdPersonController::FallTimeout
	float ___FallTimeout_14;
	// System.Boolean StarterAssets.ThirdPersonController::Grounded
	bool ___Grounded_15;
	// System.Single StarterAssets.ThirdPersonController::GroundedOffset
	float ___GroundedOffset_16;
	// System.Single StarterAssets.ThirdPersonController::GroundedRadius
	float ___GroundedRadius_17;
	// UnityEngine.LayerMask StarterAssets.ThirdPersonController::GroundLayers
	LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  ___GroundLayers_18;
	// UnityEngine.GameObject StarterAssets.ThirdPersonController::CinemachineCameraTarget
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___CinemachineCameraTarget_19;
	// System.Single StarterAssets.ThirdPersonController::TopClamp
	float ___TopClamp_20;
	// System.Single StarterAssets.ThirdPersonController::BottomClamp
	float ___BottomClamp_21;
	// System.Single StarterAssets.ThirdPersonController::CameraAngleOverride
	float ___CameraAngleOverride_22;
	// System.Boolean StarterAssets.ThirdPersonController::LockCameraPosition
	bool ___LockCameraPosition_23;
	// System.Single StarterAssets.ThirdPersonController::_cinemachineTargetYaw
	float ____cinemachineTargetYaw_24;
	// System.Single StarterAssets.ThirdPersonController::_cinemachineTargetPitch
	float ____cinemachineTargetPitch_25;
	// System.Single StarterAssets.ThirdPersonController::_speed
	float ____speed_26;
	// System.Single StarterAssets.ThirdPersonController::_animationBlend
	float ____animationBlend_27;
	// System.Single StarterAssets.ThirdPersonController::_targetRotation
	float ____targetRotation_28;
	// System.Single StarterAssets.ThirdPersonController::_rotationVelocity
	float ____rotationVelocity_29;
	// System.Single StarterAssets.ThirdPersonController::_verticalVelocity
	float ____verticalVelocity_30;
	// System.Single StarterAssets.ThirdPersonController::_terminalVelocity
	float ____terminalVelocity_31;
	// System.Single StarterAssets.ThirdPersonController::_jumpTimeoutDelta
	float ____jumpTimeoutDelta_32;
	// System.Single StarterAssets.ThirdPersonController::_fallTimeoutDelta
	float ____fallTimeoutDelta_33;
	// System.Int32 StarterAssets.ThirdPersonController::_animIDSpeed
	int32_t ____animIDSpeed_34;
	// System.Int32 StarterAssets.ThirdPersonController::_animIDGrounded
	int32_t ____animIDGrounded_35;
	// System.Int32 StarterAssets.ThirdPersonController::_animIDJump
	int32_t ____animIDJump_36;
	// System.Int32 StarterAssets.ThirdPersonController::_animIDFreeFall
	int32_t ____animIDFreeFall_37;
	// System.Int32 StarterAssets.ThirdPersonController::_animIDMotionSpeed
	int32_t ____animIDMotionSpeed_38;
	// UnityEngine.InputSystem.PlayerInput StarterAssets.ThirdPersonController::_playerInput
	PlayerInput_tCB9A1A84F447F3392889C457C9BE413891FE848A * ____playerInput_39;
	// UnityEngine.Animator StarterAssets.ThirdPersonController::_animator
	Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * ____animator_40;
	// UnityEngine.CharacterController StarterAssets.ThirdPersonController::_controller
	CharacterController_tCCF68621C784CCB3391E0C66FE134F6F93DD6C2E * ____controller_41;
	// StarterAssets.StarterAssetsInputs StarterAssets.ThirdPersonController::_input
	StarterAssetsInputs_tE7DDADB7E0E59B93F86B819E96C2523BC28F16DC * ____input_42;
	// UnityEngine.GameObject StarterAssets.ThirdPersonController::_mainCamera
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ____mainCamera_43;
	// System.Boolean StarterAssets.ThirdPersonController::_hasAnimator
	bool ____hasAnimator_45;

public:
	inline static int32_t get_offset_of_MoveSpeed_4() { return static_cast<int32_t>(offsetof(ThirdPersonController_tCA99A63A4BE57B6A9950FFA8CFFABE8F82AD0903, ___MoveSpeed_4)); }
	inline float get_MoveSpeed_4() const { return ___MoveSpeed_4; }
	inline float* get_address_of_MoveSpeed_4() { return &___MoveSpeed_4; }
	inline void set_MoveSpeed_4(float value)
	{
		___MoveSpeed_4 = value;
	}

	inline static int32_t get_offset_of_SprintSpeed_5() { return static_cast<int32_t>(offsetof(ThirdPersonController_tCA99A63A4BE57B6A9950FFA8CFFABE8F82AD0903, ___SprintSpeed_5)); }
	inline float get_SprintSpeed_5() const { return ___SprintSpeed_5; }
	inline float* get_address_of_SprintSpeed_5() { return &___SprintSpeed_5; }
	inline void set_SprintSpeed_5(float value)
	{
		___SprintSpeed_5 = value;
	}

	inline static int32_t get_offset_of_RotationSmoothTime_6() { return static_cast<int32_t>(offsetof(ThirdPersonController_tCA99A63A4BE57B6A9950FFA8CFFABE8F82AD0903, ___RotationSmoothTime_6)); }
	inline float get_RotationSmoothTime_6() const { return ___RotationSmoothTime_6; }
	inline float* get_address_of_RotationSmoothTime_6() { return &___RotationSmoothTime_6; }
	inline void set_RotationSmoothTime_6(float value)
	{
		___RotationSmoothTime_6 = value;
	}

	inline static int32_t get_offset_of_SpeedChangeRate_7() { return static_cast<int32_t>(offsetof(ThirdPersonController_tCA99A63A4BE57B6A9950FFA8CFFABE8F82AD0903, ___SpeedChangeRate_7)); }
	inline float get_SpeedChangeRate_7() const { return ___SpeedChangeRate_7; }
	inline float* get_address_of_SpeedChangeRate_7() { return &___SpeedChangeRate_7; }
	inline void set_SpeedChangeRate_7(float value)
	{
		___SpeedChangeRate_7 = value;
	}

	inline static int32_t get_offset_of_LandingAudioClip_8() { return static_cast<int32_t>(offsetof(ThirdPersonController_tCA99A63A4BE57B6A9950FFA8CFFABE8F82AD0903, ___LandingAudioClip_8)); }
	inline AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * get_LandingAudioClip_8() const { return ___LandingAudioClip_8; }
	inline AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE ** get_address_of_LandingAudioClip_8() { return &___LandingAudioClip_8; }
	inline void set_LandingAudioClip_8(AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * value)
	{
		___LandingAudioClip_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___LandingAudioClip_8), (void*)value);
	}

	inline static int32_t get_offset_of_FootstepAudioClips_9() { return static_cast<int32_t>(offsetof(ThirdPersonController_tCA99A63A4BE57B6A9950FFA8CFFABE8F82AD0903, ___FootstepAudioClips_9)); }
	inline AudioClipU5BU5D_t9BA3E7C4B62164BA2E054F3A82CD6CE6EB273CBE* get_FootstepAudioClips_9() const { return ___FootstepAudioClips_9; }
	inline AudioClipU5BU5D_t9BA3E7C4B62164BA2E054F3A82CD6CE6EB273CBE** get_address_of_FootstepAudioClips_9() { return &___FootstepAudioClips_9; }
	inline void set_FootstepAudioClips_9(AudioClipU5BU5D_t9BA3E7C4B62164BA2E054F3A82CD6CE6EB273CBE* value)
	{
		___FootstepAudioClips_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FootstepAudioClips_9), (void*)value);
	}

	inline static int32_t get_offset_of_FootstepAudioVolume_10() { return static_cast<int32_t>(offsetof(ThirdPersonController_tCA99A63A4BE57B6A9950FFA8CFFABE8F82AD0903, ___FootstepAudioVolume_10)); }
	inline float get_FootstepAudioVolume_10() const { return ___FootstepAudioVolume_10; }
	inline float* get_address_of_FootstepAudioVolume_10() { return &___FootstepAudioVolume_10; }
	inline void set_FootstepAudioVolume_10(float value)
	{
		___FootstepAudioVolume_10 = value;
	}

	inline static int32_t get_offset_of_JumpHeight_11() { return static_cast<int32_t>(offsetof(ThirdPersonController_tCA99A63A4BE57B6A9950FFA8CFFABE8F82AD0903, ___JumpHeight_11)); }
	inline float get_JumpHeight_11() const { return ___JumpHeight_11; }
	inline float* get_address_of_JumpHeight_11() { return &___JumpHeight_11; }
	inline void set_JumpHeight_11(float value)
	{
		___JumpHeight_11 = value;
	}

	inline static int32_t get_offset_of_Gravity_12() { return static_cast<int32_t>(offsetof(ThirdPersonController_tCA99A63A4BE57B6A9950FFA8CFFABE8F82AD0903, ___Gravity_12)); }
	inline float get_Gravity_12() const { return ___Gravity_12; }
	inline float* get_address_of_Gravity_12() { return &___Gravity_12; }
	inline void set_Gravity_12(float value)
	{
		___Gravity_12 = value;
	}

	inline static int32_t get_offset_of_JumpTimeout_13() { return static_cast<int32_t>(offsetof(ThirdPersonController_tCA99A63A4BE57B6A9950FFA8CFFABE8F82AD0903, ___JumpTimeout_13)); }
	inline float get_JumpTimeout_13() const { return ___JumpTimeout_13; }
	inline float* get_address_of_JumpTimeout_13() { return &___JumpTimeout_13; }
	inline void set_JumpTimeout_13(float value)
	{
		___JumpTimeout_13 = value;
	}

	inline static int32_t get_offset_of_FallTimeout_14() { return static_cast<int32_t>(offsetof(ThirdPersonController_tCA99A63A4BE57B6A9950FFA8CFFABE8F82AD0903, ___FallTimeout_14)); }
	inline float get_FallTimeout_14() const { return ___FallTimeout_14; }
	inline float* get_address_of_FallTimeout_14() { return &___FallTimeout_14; }
	inline void set_FallTimeout_14(float value)
	{
		___FallTimeout_14 = value;
	}

	inline static int32_t get_offset_of_Grounded_15() { return static_cast<int32_t>(offsetof(ThirdPersonController_tCA99A63A4BE57B6A9950FFA8CFFABE8F82AD0903, ___Grounded_15)); }
	inline bool get_Grounded_15() const { return ___Grounded_15; }
	inline bool* get_address_of_Grounded_15() { return &___Grounded_15; }
	inline void set_Grounded_15(bool value)
	{
		___Grounded_15 = value;
	}

	inline static int32_t get_offset_of_GroundedOffset_16() { return static_cast<int32_t>(offsetof(ThirdPersonController_tCA99A63A4BE57B6A9950FFA8CFFABE8F82AD0903, ___GroundedOffset_16)); }
	inline float get_GroundedOffset_16() const { return ___GroundedOffset_16; }
	inline float* get_address_of_GroundedOffset_16() { return &___GroundedOffset_16; }
	inline void set_GroundedOffset_16(float value)
	{
		___GroundedOffset_16 = value;
	}

	inline static int32_t get_offset_of_GroundedRadius_17() { return static_cast<int32_t>(offsetof(ThirdPersonController_tCA99A63A4BE57B6A9950FFA8CFFABE8F82AD0903, ___GroundedRadius_17)); }
	inline float get_GroundedRadius_17() const { return ___GroundedRadius_17; }
	inline float* get_address_of_GroundedRadius_17() { return &___GroundedRadius_17; }
	inline void set_GroundedRadius_17(float value)
	{
		___GroundedRadius_17 = value;
	}

	inline static int32_t get_offset_of_GroundLayers_18() { return static_cast<int32_t>(offsetof(ThirdPersonController_tCA99A63A4BE57B6A9950FFA8CFFABE8F82AD0903, ___GroundLayers_18)); }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  get_GroundLayers_18() const { return ___GroundLayers_18; }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8 * get_address_of_GroundLayers_18() { return &___GroundLayers_18; }
	inline void set_GroundLayers_18(LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  value)
	{
		___GroundLayers_18 = value;
	}

	inline static int32_t get_offset_of_CinemachineCameraTarget_19() { return static_cast<int32_t>(offsetof(ThirdPersonController_tCA99A63A4BE57B6A9950FFA8CFFABE8F82AD0903, ___CinemachineCameraTarget_19)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_CinemachineCameraTarget_19() const { return ___CinemachineCameraTarget_19; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_CinemachineCameraTarget_19() { return &___CinemachineCameraTarget_19; }
	inline void set_CinemachineCameraTarget_19(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___CinemachineCameraTarget_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___CinemachineCameraTarget_19), (void*)value);
	}

	inline static int32_t get_offset_of_TopClamp_20() { return static_cast<int32_t>(offsetof(ThirdPersonController_tCA99A63A4BE57B6A9950FFA8CFFABE8F82AD0903, ___TopClamp_20)); }
	inline float get_TopClamp_20() const { return ___TopClamp_20; }
	inline float* get_address_of_TopClamp_20() { return &___TopClamp_20; }
	inline void set_TopClamp_20(float value)
	{
		___TopClamp_20 = value;
	}

	inline static int32_t get_offset_of_BottomClamp_21() { return static_cast<int32_t>(offsetof(ThirdPersonController_tCA99A63A4BE57B6A9950FFA8CFFABE8F82AD0903, ___BottomClamp_21)); }
	inline float get_BottomClamp_21() const { return ___BottomClamp_21; }
	inline float* get_address_of_BottomClamp_21() { return &___BottomClamp_21; }
	inline void set_BottomClamp_21(float value)
	{
		___BottomClamp_21 = value;
	}

	inline static int32_t get_offset_of_CameraAngleOverride_22() { return static_cast<int32_t>(offsetof(ThirdPersonController_tCA99A63A4BE57B6A9950FFA8CFFABE8F82AD0903, ___CameraAngleOverride_22)); }
	inline float get_CameraAngleOverride_22() const { return ___CameraAngleOverride_22; }
	inline float* get_address_of_CameraAngleOverride_22() { return &___CameraAngleOverride_22; }
	inline void set_CameraAngleOverride_22(float value)
	{
		___CameraAngleOverride_22 = value;
	}

	inline static int32_t get_offset_of_LockCameraPosition_23() { return static_cast<int32_t>(offsetof(ThirdPersonController_tCA99A63A4BE57B6A9950FFA8CFFABE8F82AD0903, ___LockCameraPosition_23)); }
	inline bool get_LockCameraPosition_23() const { return ___LockCameraPosition_23; }
	inline bool* get_address_of_LockCameraPosition_23() { return &___LockCameraPosition_23; }
	inline void set_LockCameraPosition_23(bool value)
	{
		___LockCameraPosition_23 = value;
	}

	inline static int32_t get_offset_of__cinemachineTargetYaw_24() { return static_cast<int32_t>(offsetof(ThirdPersonController_tCA99A63A4BE57B6A9950FFA8CFFABE8F82AD0903, ____cinemachineTargetYaw_24)); }
	inline float get__cinemachineTargetYaw_24() const { return ____cinemachineTargetYaw_24; }
	inline float* get_address_of__cinemachineTargetYaw_24() { return &____cinemachineTargetYaw_24; }
	inline void set__cinemachineTargetYaw_24(float value)
	{
		____cinemachineTargetYaw_24 = value;
	}

	inline static int32_t get_offset_of__cinemachineTargetPitch_25() { return static_cast<int32_t>(offsetof(ThirdPersonController_tCA99A63A4BE57B6A9950FFA8CFFABE8F82AD0903, ____cinemachineTargetPitch_25)); }
	inline float get__cinemachineTargetPitch_25() const { return ____cinemachineTargetPitch_25; }
	inline float* get_address_of__cinemachineTargetPitch_25() { return &____cinemachineTargetPitch_25; }
	inline void set__cinemachineTargetPitch_25(float value)
	{
		____cinemachineTargetPitch_25 = value;
	}

	inline static int32_t get_offset_of__speed_26() { return static_cast<int32_t>(offsetof(ThirdPersonController_tCA99A63A4BE57B6A9950FFA8CFFABE8F82AD0903, ____speed_26)); }
	inline float get__speed_26() const { return ____speed_26; }
	inline float* get_address_of__speed_26() { return &____speed_26; }
	inline void set__speed_26(float value)
	{
		____speed_26 = value;
	}

	inline static int32_t get_offset_of__animationBlend_27() { return static_cast<int32_t>(offsetof(ThirdPersonController_tCA99A63A4BE57B6A9950FFA8CFFABE8F82AD0903, ____animationBlend_27)); }
	inline float get__animationBlend_27() const { return ____animationBlend_27; }
	inline float* get_address_of__animationBlend_27() { return &____animationBlend_27; }
	inline void set__animationBlend_27(float value)
	{
		____animationBlend_27 = value;
	}

	inline static int32_t get_offset_of__targetRotation_28() { return static_cast<int32_t>(offsetof(ThirdPersonController_tCA99A63A4BE57B6A9950FFA8CFFABE8F82AD0903, ____targetRotation_28)); }
	inline float get__targetRotation_28() const { return ____targetRotation_28; }
	inline float* get_address_of__targetRotation_28() { return &____targetRotation_28; }
	inline void set__targetRotation_28(float value)
	{
		____targetRotation_28 = value;
	}

	inline static int32_t get_offset_of__rotationVelocity_29() { return static_cast<int32_t>(offsetof(ThirdPersonController_tCA99A63A4BE57B6A9950FFA8CFFABE8F82AD0903, ____rotationVelocity_29)); }
	inline float get__rotationVelocity_29() const { return ____rotationVelocity_29; }
	inline float* get_address_of__rotationVelocity_29() { return &____rotationVelocity_29; }
	inline void set__rotationVelocity_29(float value)
	{
		____rotationVelocity_29 = value;
	}

	inline static int32_t get_offset_of__verticalVelocity_30() { return static_cast<int32_t>(offsetof(ThirdPersonController_tCA99A63A4BE57B6A9950FFA8CFFABE8F82AD0903, ____verticalVelocity_30)); }
	inline float get__verticalVelocity_30() const { return ____verticalVelocity_30; }
	inline float* get_address_of__verticalVelocity_30() { return &____verticalVelocity_30; }
	inline void set__verticalVelocity_30(float value)
	{
		____verticalVelocity_30 = value;
	}

	inline static int32_t get_offset_of__terminalVelocity_31() { return static_cast<int32_t>(offsetof(ThirdPersonController_tCA99A63A4BE57B6A9950FFA8CFFABE8F82AD0903, ____terminalVelocity_31)); }
	inline float get__terminalVelocity_31() const { return ____terminalVelocity_31; }
	inline float* get_address_of__terminalVelocity_31() { return &____terminalVelocity_31; }
	inline void set__terminalVelocity_31(float value)
	{
		____terminalVelocity_31 = value;
	}

	inline static int32_t get_offset_of__jumpTimeoutDelta_32() { return static_cast<int32_t>(offsetof(ThirdPersonController_tCA99A63A4BE57B6A9950FFA8CFFABE8F82AD0903, ____jumpTimeoutDelta_32)); }
	inline float get__jumpTimeoutDelta_32() const { return ____jumpTimeoutDelta_32; }
	inline float* get_address_of__jumpTimeoutDelta_32() { return &____jumpTimeoutDelta_32; }
	inline void set__jumpTimeoutDelta_32(float value)
	{
		____jumpTimeoutDelta_32 = value;
	}

	inline static int32_t get_offset_of__fallTimeoutDelta_33() { return static_cast<int32_t>(offsetof(ThirdPersonController_tCA99A63A4BE57B6A9950FFA8CFFABE8F82AD0903, ____fallTimeoutDelta_33)); }
	inline float get__fallTimeoutDelta_33() const { return ____fallTimeoutDelta_33; }
	inline float* get_address_of__fallTimeoutDelta_33() { return &____fallTimeoutDelta_33; }
	inline void set__fallTimeoutDelta_33(float value)
	{
		____fallTimeoutDelta_33 = value;
	}

	inline static int32_t get_offset_of__animIDSpeed_34() { return static_cast<int32_t>(offsetof(ThirdPersonController_tCA99A63A4BE57B6A9950FFA8CFFABE8F82AD0903, ____animIDSpeed_34)); }
	inline int32_t get__animIDSpeed_34() const { return ____animIDSpeed_34; }
	inline int32_t* get_address_of__animIDSpeed_34() { return &____animIDSpeed_34; }
	inline void set__animIDSpeed_34(int32_t value)
	{
		____animIDSpeed_34 = value;
	}

	inline static int32_t get_offset_of__animIDGrounded_35() { return static_cast<int32_t>(offsetof(ThirdPersonController_tCA99A63A4BE57B6A9950FFA8CFFABE8F82AD0903, ____animIDGrounded_35)); }
	inline int32_t get__animIDGrounded_35() const { return ____animIDGrounded_35; }
	inline int32_t* get_address_of__animIDGrounded_35() { return &____animIDGrounded_35; }
	inline void set__animIDGrounded_35(int32_t value)
	{
		____animIDGrounded_35 = value;
	}

	inline static int32_t get_offset_of__animIDJump_36() { return static_cast<int32_t>(offsetof(ThirdPersonController_tCA99A63A4BE57B6A9950FFA8CFFABE8F82AD0903, ____animIDJump_36)); }
	inline int32_t get__animIDJump_36() const { return ____animIDJump_36; }
	inline int32_t* get_address_of__animIDJump_36() { return &____animIDJump_36; }
	inline void set__animIDJump_36(int32_t value)
	{
		____animIDJump_36 = value;
	}

	inline static int32_t get_offset_of__animIDFreeFall_37() { return static_cast<int32_t>(offsetof(ThirdPersonController_tCA99A63A4BE57B6A9950FFA8CFFABE8F82AD0903, ____animIDFreeFall_37)); }
	inline int32_t get__animIDFreeFall_37() const { return ____animIDFreeFall_37; }
	inline int32_t* get_address_of__animIDFreeFall_37() { return &____animIDFreeFall_37; }
	inline void set__animIDFreeFall_37(int32_t value)
	{
		____animIDFreeFall_37 = value;
	}

	inline static int32_t get_offset_of__animIDMotionSpeed_38() { return static_cast<int32_t>(offsetof(ThirdPersonController_tCA99A63A4BE57B6A9950FFA8CFFABE8F82AD0903, ____animIDMotionSpeed_38)); }
	inline int32_t get__animIDMotionSpeed_38() const { return ____animIDMotionSpeed_38; }
	inline int32_t* get_address_of__animIDMotionSpeed_38() { return &____animIDMotionSpeed_38; }
	inline void set__animIDMotionSpeed_38(int32_t value)
	{
		____animIDMotionSpeed_38 = value;
	}

	inline static int32_t get_offset_of__playerInput_39() { return static_cast<int32_t>(offsetof(ThirdPersonController_tCA99A63A4BE57B6A9950FFA8CFFABE8F82AD0903, ____playerInput_39)); }
	inline PlayerInput_tCB9A1A84F447F3392889C457C9BE413891FE848A * get__playerInput_39() const { return ____playerInput_39; }
	inline PlayerInput_tCB9A1A84F447F3392889C457C9BE413891FE848A ** get_address_of__playerInput_39() { return &____playerInput_39; }
	inline void set__playerInput_39(PlayerInput_tCB9A1A84F447F3392889C457C9BE413891FE848A * value)
	{
		____playerInput_39 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____playerInput_39), (void*)value);
	}

	inline static int32_t get_offset_of__animator_40() { return static_cast<int32_t>(offsetof(ThirdPersonController_tCA99A63A4BE57B6A9950FFA8CFFABE8F82AD0903, ____animator_40)); }
	inline Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * get__animator_40() const { return ____animator_40; }
	inline Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 ** get_address_of__animator_40() { return &____animator_40; }
	inline void set__animator_40(Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * value)
	{
		____animator_40 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____animator_40), (void*)value);
	}

	inline static int32_t get_offset_of__controller_41() { return static_cast<int32_t>(offsetof(ThirdPersonController_tCA99A63A4BE57B6A9950FFA8CFFABE8F82AD0903, ____controller_41)); }
	inline CharacterController_tCCF68621C784CCB3391E0C66FE134F6F93DD6C2E * get__controller_41() const { return ____controller_41; }
	inline CharacterController_tCCF68621C784CCB3391E0C66FE134F6F93DD6C2E ** get_address_of__controller_41() { return &____controller_41; }
	inline void set__controller_41(CharacterController_tCCF68621C784CCB3391E0C66FE134F6F93DD6C2E * value)
	{
		____controller_41 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____controller_41), (void*)value);
	}

	inline static int32_t get_offset_of__input_42() { return static_cast<int32_t>(offsetof(ThirdPersonController_tCA99A63A4BE57B6A9950FFA8CFFABE8F82AD0903, ____input_42)); }
	inline StarterAssetsInputs_tE7DDADB7E0E59B93F86B819E96C2523BC28F16DC * get__input_42() const { return ____input_42; }
	inline StarterAssetsInputs_tE7DDADB7E0E59B93F86B819E96C2523BC28F16DC ** get_address_of__input_42() { return &____input_42; }
	inline void set__input_42(StarterAssetsInputs_tE7DDADB7E0E59B93F86B819E96C2523BC28F16DC * value)
	{
		____input_42 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____input_42), (void*)value);
	}

	inline static int32_t get_offset_of__mainCamera_43() { return static_cast<int32_t>(offsetof(ThirdPersonController_tCA99A63A4BE57B6A9950FFA8CFFABE8F82AD0903, ____mainCamera_43)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get__mainCamera_43() const { return ____mainCamera_43; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of__mainCamera_43() { return &____mainCamera_43; }
	inline void set__mainCamera_43(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		____mainCamera_43 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____mainCamera_43), (void*)value);
	}

	inline static int32_t get_offset_of__hasAnimator_45() { return static_cast<int32_t>(offsetof(ThirdPersonController_tCA99A63A4BE57B6A9950FFA8CFFABE8F82AD0903, ____hasAnimator_45)); }
	inline bool get__hasAnimator_45() const { return ____hasAnimator_45; }
	inline bool* get_address_of__hasAnimator_45() { return &____hasAnimator_45; }
	inline void set__hasAnimator_45(bool value)
	{
		____hasAnimator_45 = value;
	}
};


// UnityEngine.InputSystem.TrackedDevice
struct  TrackedDevice_tDF5E09C055D0CA92DF8930A7B45EDA3BBA14ED71  : public InputDevice_t64BD575C54DB522A280D29E47F96E52B79B22D87
{
public:
	// UnityEngine.InputSystem.Controls.IntegerControl UnityEngine.InputSystem.TrackedDevice::<trackingState>k__BackingField
	IntegerControl_t68534DE7D89FBFC2952062997BB54680FE7DCCA4 * ___U3CtrackingStateU3Ek__BackingField_39;
	// UnityEngine.InputSystem.Controls.ButtonControl UnityEngine.InputSystem.TrackedDevice::<isTracked>k__BackingField
	ButtonControl_t3918D56D674854AF9DFEB8CCF130919AAD9FCA68 * ___U3CisTrackedU3Ek__BackingField_40;
	// UnityEngine.InputSystem.Controls.Vector3Control UnityEngine.InputSystem.TrackedDevice::<devicePosition>k__BackingField
	Vector3Control_t915A3DA3C3E367B6F5587853E96EAD5215D328AE * ___U3CdevicePositionU3Ek__BackingField_41;
	// UnityEngine.InputSystem.Controls.QuaternionControl UnityEngine.InputSystem.TrackedDevice::<deviceRotation>k__BackingField
	QuaternionControl_t7BEFE3CFC03CC5CE3F779DDCB5AA2C011F5C7A48 * ___U3CdeviceRotationU3Ek__BackingField_42;

public:
	inline static int32_t get_offset_of_U3CtrackingStateU3Ek__BackingField_39() { return static_cast<int32_t>(offsetof(TrackedDevice_tDF5E09C055D0CA92DF8930A7B45EDA3BBA14ED71, ___U3CtrackingStateU3Ek__BackingField_39)); }
	inline IntegerControl_t68534DE7D89FBFC2952062997BB54680FE7DCCA4 * get_U3CtrackingStateU3Ek__BackingField_39() const { return ___U3CtrackingStateU3Ek__BackingField_39; }
	inline IntegerControl_t68534DE7D89FBFC2952062997BB54680FE7DCCA4 ** get_address_of_U3CtrackingStateU3Ek__BackingField_39() { return &___U3CtrackingStateU3Ek__BackingField_39; }
	inline void set_U3CtrackingStateU3Ek__BackingField_39(IntegerControl_t68534DE7D89FBFC2952062997BB54680FE7DCCA4 * value)
	{
		___U3CtrackingStateU3Ek__BackingField_39 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CtrackingStateU3Ek__BackingField_39), (void*)value);
	}

	inline static int32_t get_offset_of_U3CisTrackedU3Ek__BackingField_40() { return static_cast<int32_t>(offsetof(TrackedDevice_tDF5E09C055D0CA92DF8930A7B45EDA3BBA14ED71, ___U3CisTrackedU3Ek__BackingField_40)); }
	inline ButtonControl_t3918D56D674854AF9DFEB8CCF130919AAD9FCA68 * get_U3CisTrackedU3Ek__BackingField_40() const { return ___U3CisTrackedU3Ek__BackingField_40; }
	inline ButtonControl_t3918D56D674854AF9DFEB8CCF130919AAD9FCA68 ** get_address_of_U3CisTrackedU3Ek__BackingField_40() { return &___U3CisTrackedU3Ek__BackingField_40; }
	inline void set_U3CisTrackedU3Ek__BackingField_40(ButtonControl_t3918D56D674854AF9DFEB8CCF130919AAD9FCA68 * value)
	{
		___U3CisTrackedU3Ek__BackingField_40 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CisTrackedU3Ek__BackingField_40), (void*)value);
	}

	inline static int32_t get_offset_of_U3CdevicePositionU3Ek__BackingField_41() { return static_cast<int32_t>(offsetof(TrackedDevice_tDF5E09C055D0CA92DF8930A7B45EDA3BBA14ED71, ___U3CdevicePositionU3Ek__BackingField_41)); }
	inline Vector3Control_t915A3DA3C3E367B6F5587853E96EAD5215D328AE * get_U3CdevicePositionU3Ek__BackingField_41() const { return ___U3CdevicePositionU3Ek__BackingField_41; }
	inline Vector3Control_t915A3DA3C3E367B6F5587853E96EAD5215D328AE ** get_address_of_U3CdevicePositionU3Ek__BackingField_41() { return &___U3CdevicePositionU3Ek__BackingField_41; }
	inline void set_U3CdevicePositionU3Ek__BackingField_41(Vector3Control_t915A3DA3C3E367B6F5587853E96EAD5215D328AE * value)
	{
		___U3CdevicePositionU3Ek__BackingField_41 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CdevicePositionU3Ek__BackingField_41), (void*)value);
	}

	inline static int32_t get_offset_of_U3CdeviceRotationU3Ek__BackingField_42() { return static_cast<int32_t>(offsetof(TrackedDevice_tDF5E09C055D0CA92DF8930A7B45EDA3BBA14ED71, ___U3CdeviceRotationU3Ek__BackingField_42)); }
	inline QuaternionControl_t7BEFE3CFC03CC5CE3F779DDCB5AA2C011F5C7A48 * get_U3CdeviceRotationU3Ek__BackingField_42() const { return ___U3CdeviceRotationU3Ek__BackingField_42; }
	inline QuaternionControl_t7BEFE3CFC03CC5CE3F779DDCB5AA2C011F5C7A48 ** get_address_of_U3CdeviceRotationU3Ek__BackingField_42() { return &___U3CdeviceRotationU3Ek__BackingField_42; }
	inline void set_U3CdeviceRotationU3Ek__BackingField_42(QuaternionControl_t7BEFE3CFC03CC5CE3F779DDCB5AA2C011F5C7A48 * value)
	{
		___U3CdeviceRotationU3Ek__BackingField_42 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CdeviceRotationU3Ek__BackingField_42), (void*)value);
	}
};


// TrafficLight
struct  TrafficLight_tD55F68E16E18C0A5F32B506715F5BE0989084ABD  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Boolean TrafficLight::isGreenToCars
	bool ___isGreenToCars_4;
	// System.Boolean TrafficLight::isGreenToWalkers
	bool ___isGreenToWalkers_5;
	// System.Boolean TrafficLight::lastWasThis
	bool ___lastWasThis_6;
	// UnityEngine.GameObject[] TrafficLight::carLights
	GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* ___carLights_7;
	// UnityEngine.GameObject[] TrafficLight::walkLights
	GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* ___walkLights_8;
	// System.Int32 TrafficLight::timer
	int32_t ___timer_9;
	// TrafficLight[] TrafficLight::lightsThatClosing
	TrafficLightU5BU5D_tD9EAAB468C6B67BB3FB40A4EB89B90754BC540A3* ___lightsThatClosing_10;
	// TrafficLight[] TrafficLight::lightsThatOpening
	TrafficLightU5BU5D_tD9EAAB468C6B67BB3FB40A4EB89B90754BC540A3* ___lightsThatOpening_11;

public:
	inline static int32_t get_offset_of_isGreenToCars_4() { return static_cast<int32_t>(offsetof(TrafficLight_tD55F68E16E18C0A5F32B506715F5BE0989084ABD, ___isGreenToCars_4)); }
	inline bool get_isGreenToCars_4() const { return ___isGreenToCars_4; }
	inline bool* get_address_of_isGreenToCars_4() { return &___isGreenToCars_4; }
	inline void set_isGreenToCars_4(bool value)
	{
		___isGreenToCars_4 = value;
	}

	inline static int32_t get_offset_of_isGreenToWalkers_5() { return static_cast<int32_t>(offsetof(TrafficLight_tD55F68E16E18C0A5F32B506715F5BE0989084ABD, ___isGreenToWalkers_5)); }
	inline bool get_isGreenToWalkers_5() const { return ___isGreenToWalkers_5; }
	inline bool* get_address_of_isGreenToWalkers_5() { return &___isGreenToWalkers_5; }
	inline void set_isGreenToWalkers_5(bool value)
	{
		___isGreenToWalkers_5 = value;
	}

	inline static int32_t get_offset_of_lastWasThis_6() { return static_cast<int32_t>(offsetof(TrafficLight_tD55F68E16E18C0A5F32B506715F5BE0989084ABD, ___lastWasThis_6)); }
	inline bool get_lastWasThis_6() const { return ___lastWasThis_6; }
	inline bool* get_address_of_lastWasThis_6() { return &___lastWasThis_6; }
	inline void set_lastWasThis_6(bool value)
	{
		___lastWasThis_6 = value;
	}

	inline static int32_t get_offset_of_carLights_7() { return static_cast<int32_t>(offsetof(TrafficLight_tD55F68E16E18C0A5F32B506715F5BE0989084ABD, ___carLights_7)); }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* get_carLights_7() const { return ___carLights_7; }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642** get_address_of_carLights_7() { return &___carLights_7; }
	inline void set_carLights_7(GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* value)
	{
		___carLights_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___carLights_7), (void*)value);
	}

	inline static int32_t get_offset_of_walkLights_8() { return static_cast<int32_t>(offsetof(TrafficLight_tD55F68E16E18C0A5F32B506715F5BE0989084ABD, ___walkLights_8)); }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* get_walkLights_8() const { return ___walkLights_8; }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642** get_address_of_walkLights_8() { return &___walkLights_8; }
	inline void set_walkLights_8(GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* value)
	{
		___walkLights_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___walkLights_8), (void*)value);
	}

	inline static int32_t get_offset_of_timer_9() { return static_cast<int32_t>(offsetof(TrafficLight_tD55F68E16E18C0A5F32B506715F5BE0989084ABD, ___timer_9)); }
	inline int32_t get_timer_9() const { return ___timer_9; }
	inline int32_t* get_address_of_timer_9() { return &___timer_9; }
	inline void set_timer_9(int32_t value)
	{
		___timer_9 = value;
	}

	inline static int32_t get_offset_of_lightsThatClosing_10() { return static_cast<int32_t>(offsetof(TrafficLight_tD55F68E16E18C0A5F32B506715F5BE0989084ABD, ___lightsThatClosing_10)); }
	inline TrafficLightU5BU5D_tD9EAAB468C6B67BB3FB40A4EB89B90754BC540A3* get_lightsThatClosing_10() const { return ___lightsThatClosing_10; }
	inline TrafficLightU5BU5D_tD9EAAB468C6B67BB3FB40A4EB89B90754BC540A3** get_address_of_lightsThatClosing_10() { return &___lightsThatClosing_10; }
	inline void set_lightsThatClosing_10(TrafficLightU5BU5D_tD9EAAB468C6B67BB3FB40A4EB89B90754BC540A3* value)
	{
		___lightsThatClosing_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___lightsThatClosing_10), (void*)value);
	}

	inline static int32_t get_offset_of_lightsThatOpening_11() { return static_cast<int32_t>(offsetof(TrafficLight_tD55F68E16E18C0A5F32B506715F5BE0989084ABD, ___lightsThatOpening_11)); }
	inline TrafficLightU5BU5D_tD9EAAB468C6B67BB3FB40A4EB89B90754BC540A3* get_lightsThatOpening_11() const { return ___lightsThatOpening_11; }
	inline TrafficLightU5BU5D_tD9EAAB468C6B67BB3FB40A4EB89B90754BC540A3** get_address_of_lightsThatOpening_11() { return &___lightsThatOpening_11; }
	inline void set_lightsThatOpening_11(TrafficLightU5BU5D_tD9EAAB468C6B67BB3FB40A4EB89B90754BC540A3* value)
	{
		___lightsThatOpening_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___lightsThatOpening_11), (void*)value);
	}
};


// TrafficLights
struct  TrafficLights_t6127A0CC6322E5899984953DA144B63D24333A73  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// TrafficLights/LightCount TrafficLights::lightCount
	int32_t ___lightCount_4;
	// TrafficLight[] TrafficLights::trafficLights
	TrafficLightU5BU5D_tD9EAAB468C6B67BB3FB40A4EB89B90754BC540A3* ___trafficLights_5;
	// System.Boolean TrafficLights::isProcessing
	bool ___isProcessing_6;
	// System.Int32 TrafficLights::howManySecs
	int32_t ___howManySecs_7;
	// System.Int32 TrafficLights::timer
	int32_t ___timer_8;

public:
	inline static int32_t get_offset_of_lightCount_4() { return static_cast<int32_t>(offsetof(TrafficLights_t6127A0CC6322E5899984953DA144B63D24333A73, ___lightCount_4)); }
	inline int32_t get_lightCount_4() const { return ___lightCount_4; }
	inline int32_t* get_address_of_lightCount_4() { return &___lightCount_4; }
	inline void set_lightCount_4(int32_t value)
	{
		___lightCount_4 = value;
	}

	inline static int32_t get_offset_of_trafficLights_5() { return static_cast<int32_t>(offsetof(TrafficLights_t6127A0CC6322E5899984953DA144B63D24333A73, ___trafficLights_5)); }
	inline TrafficLightU5BU5D_tD9EAAB468C6B67BB3FB40A4EB89B90754BC540A3* get_trafficLights_5() const { return ___trafficLights_5; }
	inline TrafficLightU5BU5D_tD9EAAB468C6B67BB3FB40A4EB89B90754BC540A3** get_address_of_trafficLights_5() { return &___trafficLights_5; }
	inline void set_trafficLights_5(TrafficLightU5BU5D_tD9EAAB468C6B67BB3FB40A4EB89B90754BC540A3* value)
	{
		___trafficLights_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___trafficLights_5), (void*)value);
	}

	inline static int32_t get_offset_of_isProcessing_6() { return static_cast<int32_t>(offsetof(TrafficLights_t6127A0CC6322E5899984953DA144B63D24333A73, ___isProcessing_6)); }
	inline bool get_isProcessing_6() const { return ___isProcessing_6; }
	inline bool* get_address_of_isProcessing_6() { return &___isProcessing_6; }
	inline void set_isProcessing_6(bool value)
	{
		___isProcessing_6 = value;
	}

	inline static int32_t get_offset_of_howManySecs_7() { return static_cast<int32_t>(offsetof(TrafficLights_t6127A0CC6322E5899984953DA144B63D24333A73, ___howManySecs_7)); }
	inline int32_t get_howManySecs_7() const { return ___howManySecs_7; }
	inline int32_t* get_address_of_howManySecs_7() { return &___howManySecs_7; }
	inline void set_howManySecs_7(int32_t value)
	{
		___howManySecs_7 = value;
	}

	inline static int32_t get_offset_of_timer_8() { return static_cast<int32_t>(offsetof(TrafficLights_t6127A0CC6322E5899984953DA144B63D24333A73, ___timer_8)); }
	inline int32_t get_timer_8() const { return ___timer_8; }
	inline int32_t* get_address_of_timer_8() { return &___timer_8; }
	inline void set_timer_8(int32_t value)
	{
		___timer_8 = value;
	}
};


// TrafficRules
struct  TrafficRules_tA4734D51E40A038ED9DEED23A4311BCD821226A0  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// TrafficRules/States TrafficRules::State
	int32_t ___State_4;
	// System.Single TrafficRules::Value
	float ___Value_5;

public:
	inline static int32_t get_offset_of_State_4() { return static_cast<int32_t>(offsetof(TrafficRules_tA4734D51E40A038ED9DEED23A4311BCD821226A0, ___State_4)); }
	inline int32_t get_State_4() const { return ___State_4; }
	inline int32_t* get_address_of_State_4() { return &___State_4; }
	inline void set_State_4(int32_t value)
	{
		___State_4 = value;
	}

	inline static int32_t get_offset_of_Value_5() { return static_cast<int32_t>(offsetof(TrafficRules_tA4734D51E40A038ED9DEED23A4311BCD821226A0, ___Value_5)); }
	inline float get_Value_5() const { return ___Value_5; }
	inline float* get_address_of_Value_5() { return &___Value_5; }
	inline void set_Value_5(float value)
	{
		___Value_5 = value;
	}
};


// TrafficRulesController_Car
struct  TrafficRulesController_Car_tB16F6F9B85938EDD1604F77EF7DC0A1BDB472A60  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Single TrafficRulesController_Car::speed
	float ___speed_4;
	// System.Single TrafficRulesController_Car::speedLimit
	float ___speedLimit_5;
	// TrafficRules/States TrafficRulesController_Car::State
	int32_t ___State_6;
	// RoadDecider/Directions TrafficRulesController_Car::direction
	int32_t ___direction_7;
	// WalkerWay TrafficRulesController_Car::WalkerWay
	WalkerWay_tE1DBB9C531DC74D2654F2AB74C9D6E1304A418DD * ___WalkerWay_8;
	// SignalsCar TrafficRulesController_Car::signalController
	SignalsCar_t381616BF85EDCCF005FD247DC7E72DB6EAD4DAB0 * ___signalController_9;
	// UnityEngine.GameObject TrafficRulesController_Car::Decider
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___Decider_10;
	// UnityEngine.GameObject TrafficRulesController_Car::RoadStart
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___RoadStart_11;
	// UnityEngine.GameObject[] TrafficRulesController_Car::Directions
	GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* ___Directions_12;
	// UnityEngine.GameObject[] TrafficRulesController_Car::Rules
	GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* ___Rules_13;
	// GameEndController TrafficRulesController_Car::Controller
	GameEndController_t22B3012DEB58F882ECF6BE83EB24B4997F177C12 * ___Controller_14;
	// System.Boolean TrafficRulesController_Car::shouldBe
	bool ___shouldBe_15;

public:
	inline static int32_t get_offset_of_speed_4() { return static_cast<int32_t>(offsetof(TrafficRulesController_Car_tB16F6F9B85938EDD1604F77EF7DC0A1BDB472A60, ___speed_4)); }
	inline float get_speed_4() const { return ___speed_4; }
	inline float* get_address_of_speed_4() { return &___speed_4; }
	inline void set_speed_4(float value)
	{
		___speed_4 = value;
	}

	inline static int32_t get_offset_of_speedLimit_5() { return static_cast<int32_t>(offsetof(TrafficRulesController_Car_tB16F6F9B85938EDD1604F77EF7DC0A1BDB472A60, ___speedLimit_5)); }
	inline float get_speedLimit_5() const { return ___speedLimit_5; }
	inline float* get_address_of_speedLimit_5() { return &___speedLimit_5; }
	inline void set_speedLimit_5(float value)
	{
		___speedLimit_5 = value;
	}

	inline static int32_t get_offset_of_State_6() { return static_cast<int32_t>(offsetof(TrafficRulesController_Car_tB16F6F9B85938EDD1604F77EF7DC0A1BDB472A60, ___State_6)); }
	inline int32_t get_State_6() const { return ___State_6; }
	inline int32_t* get_address_of_State_6() { return &___State_6; }
	inline void set_State_6(int32_t value)
	{
		___State_6 = value;
	}

	inline static int32_t get_offset_of_direction_7() { return static_cast<int32_t>(offsetof(TrafficRulesController_Car_tB16F6F9B85938EDD1604F77EF7DC0A1BDB472A60, ___direction_7)); }
	inline int32_t get_direction_7() const { return ___direction_7; }
	inline int32_t* get_address_of_direction_7() { return &___direction_7; }
	inline void set_direction_7(int32_t value)
	{
		___direction_7 = value;
	}

	inline static int32_t get_offset_of_WalkerWay_8() { return static_cast<int32_t>(offsetof(TrafficRulesController_Car_tB16F6F9B85938EDD1604F77EF7DC0A1BDB472A60, ___WalkerWay_8)); }
	inline WalkerWay_tE1DBB9C531DC74D2654F2AB74C9D6E1304A418DD * get_WalkerWay_8() const { return ___WalkerWay_8; }
	inline WalkerWay_tE1DBB9C531DC74D2654F2AB74C9D6E1304A418DD ** get_address_of_WalkerWay_8() { return &___WalkerWay_8; }
	inline void set_WalkerWay_8(WalkerWay_tE1DBB9C531DC74D2654F2AB74C9D6E1304A418DD * value)
	{
		___WalkerWay_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___WalkerWay_8), (void*)value);
	}

	inline static int32_t get_offset_of_signalController_9() { return static_cast<int32_t>(offsetof(TrafficRulesController_Car_tB16F6F9B85938EDD1604F77EF7DC0A1BDB472A60, ___signalController_9)); }
	inline SignalsCar_t381616BF85EDCCF005FD247DC7E72DB6EAD4DAB0 * get_signalController_9() const { return ___signalController_9; }
	inline SignalsCar_t381616BF85EDCCF005FD247DC7E72DB6EAD4DAB0 ** get_address_of_signalController_9() { return &___signalController_9; }
	inline void set_signalController_9(SignalsCar_t381616BF85EDCCF005FD247DC7E72DB6EAD4DAB0 * value)
	{
		___signalController_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___signalController_9), (void*)value);
	}

	inline static int32_t get_offset_of_Decider_10() { return static_cast<int32_t>(offsetof(TrafficRulesController_Car_tB16F6F9B85938EDD1604F77EF7DC0A1BDB472A60, ___Decider_10)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_Decider_10() const { return ___Decider_10; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_Decider_10() { return &___Decider_10; }
	inline void set_Decider_10(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___Decider_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Decider_10), (void*)value);
	}

	inline static int32_t get_offset_of_RoadStart_11() { return static_cast<int32_t>(offsetof(TrafficRulesController_Car_tB16F6F9B85938EDD1604F77EF7DC0A1BDB472A60, ___RoadStart_11)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_RoadStart_11() const { return ___RoadStart_11; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_RoadStart_11() { return &___RoadStart_11; }
	inline void set_RoadStart_11(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___RoadStart_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___RoadStart_11), (void*)value);
	}

	inline static int32_t get_offset_of_Directions_12() { return static_cast<int32_t>(offsetof(TrafficRulesController_Car_tB16F6F9B85938EDD1604F77EF7DC0A1BDB472A60, ___Directions_12)); }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* get_Directions_12() const { return ___Directions_12; }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642** get_address_of_Directions_12() { return &___Directions_12; }
	inline void set_Directions_12(GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* value)
	{
		___Directions_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Directions_12), (void*)value);
	}

	inline static int32_t get_offset_of_Rules_13() { return static_cast<int32_t>(offsetof(TrafficRulesController_Car_tB16F6F9B85938EDD1604F77EF7DC0A1BDB472A60, ___Rules_13)); }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* get_Rules_13() const { return ___Rules_13; }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642** get_address_of_Rules_13() { return &___Rules_13; }
	inline void set_Rules_13(GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* value)
	{
		___Rules_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Rules_13), (void*)value);
	}

	inline static int32_t get_offset_of_Controller_14() { return static_cast<int32_t>(offsetof(TrafficRulesController_Car_tB16F6F9B85938EDD1604F77EF7DC0A1BDB472A60, ___Controller_14)); }
	inline GameEndController_t22B3012DEB58F882ECF6BE83EB24B4997F177C12 * get_Controller_14() const { return ___Controller_14; }
	inline GameEndController_t22B3012DEB58F882ECF6BE83EB24B4997F177C12 ** get_address_of_Controller_14() { return &___Controller_14; }
	inline void set_Controller_14(GameEndController_t22B3012DEB58F882ECF6BE83EB24B4997F177C12 * value)
	{
		___Controller_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Controller_14), (void*)value);
	}

	inline static int32_t get_offset_of_shouldBe_15() { return static_cast<int32_t>(offsetof(TrafficRulesController_Car_tB16F6F9B85938EDD1604F77EF7DC0A1BDB472A60, ___shouldBe_15)); }
	inline bool get_shouldBe_15() const { return ___shouldBe_15; }
	inline bool* get_address_of_shouldBe_15() { return &___shouldBe_15; }
	inline void set_shouldBe_15(bool value)
	{
		___shouldBe_15 = value;
	}
};


// StarterAssets.UICanvasControllerInput
struct  UICanvasControllerInput_t18C69AA8A6225F0EDEC9D31926AFCA88FC8F8EA1  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// StarterAssets.StarterAssetsInputs StarterAssets.UICanvasControllerInput::starterAssetsInputs
	StarterAssetsInputs_tE7DDADB7E0E59B93F86B819E96C2523BC28F16DC * ___starterAssetsInputs_4;

public:
	inline static int32_t get_offset_of_starterAssetsInputs_4() { return static_cast<int32_t>(offsetof(UICanvasControllerInput_t18C69AA8A6225F0EDEC9D31926AFCA88FC8F8EA1, ___starterAssetsInputs_4)); }
	inline StarterAssetsInputs_tE7DDADB7E0E59B93F86B819E96C2523BC28F16DC * get_starterAssetsInputs_4() const { return ___starterAssetsInputs_4; }
	inline StarterAssetsInputs_tE7DDADB7E0E59B93F86B819E96C2523BC28F16DC ** get_address_of_starterAssetsInputs_4() { return &___starterAssetsInputs_4; }
	inline void set_starterAssetsInputs_4(StarterAssetsInputs_tE7DDADB7E0E59B93F86B819E96C2523BC28F16DC * value)
	{
		___starterAssetsInputs_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___starterAssetsInputs_4), (void*)value);
	}
};


// UIVirtualButton
struct  UIVirtualButton_tA1A6B564C5E71E1B61B240E28958DEB8F79BABBE  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UIVirtualButton/BoolEvent UIVirtualButton::buttonStateOutputEvent
	BoolEvent_tD84A3E3A4245DFD9FA5D608A5ADA77DBDFB6BD56 * ___buttonStateOutputEvent_4;
	// UIVirtualButton/Event UIVirtualButton::buttonClickOutputEvent
	Event_tB8168EB885996D80674A82913E2B33B4915A9E23 * ___buttonClickOutputEvent_5;

public:
	inline static int32_t get_offset_of_buttonStateOutputEvent_4() { return static_cast<int32_t>(offsetof(UIVirtualButton_tA1A6B564C5E71E1B61B240E28958DEB8F79BABBE, ___buttonStateOutputEvent_4)); }
	inline BoolEvent_tD84A3E3A4245DFD9FA5D608A5ADA77DBDFB6BD56 * get_buttonStateOutputEvent_4() const { return ___buttonStateOutputEvent_4; }
	inline BoolEvent_tD84A3E3A4245DFD9FA5D608A5ADA77DBDFB6BD56 ** get_address_of_buttonStateOutputEvent_4() { return &___buttonStateOutputEvent_4; }
	inline void set_buttonStateOutputEvent_4(BoolEvent_tD84A3E3A4245DFD9FA5D608A5ADA77DBDFB6BD56 * value)
	{
		___buttonStateOutputEvent_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___buttonStateOutputEvent_4), (void*)value);
	}

	inline static int32_t get_offset_of_buttonClickOutputEvent_5() { return static_cast<int32_t>(offsetof(UIVirtualButton_tA1A6B564C5E71E1B61B240E28958DEB8F79BABBE, ___buttonClickOutputEvent_5)); }
	inline Event_tB8168EB885996D80674A82913E2B33B4915A9E23 * get_buttonClickOutputEvent_5() const { return ___buttonClickOutputEvent_5; }
	inline Event_tB8168EB885996D80674A82913E2B33B4915A9E23 ** get_address_of_buttonClickOutputEvent_5() { return &___buttonClickOutputEvent_5; }
	inline void set_buttonClickOutputEvent_5(Event_tB8168EB885996D80674A82913E2B33B4915A9E23 * value)
	{
		___buttonClickOutputEvent_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___buttonClickOutputEvent_5), (void*)value);
	}
};


// UIVirtualJoystick
struct  UIVirtualJoystick_tEF0053DAA02EDBA00713D7B29C34394EA7574C2D  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.RectTransform UIVirtualJoystick::containerRect
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___containerRect_4;
	// UnityEngine.RectTransform UIVirtualJoystick::handleRect
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___handleRect_5;
	// System.Single UIVirtualJoystick::joystickRange
	float ___joystickRange_6;
	// System.Single UIVirtualJoystick::magnitudeMultiplier
	float ___magnitudeMultiplier_7;
	// System.Boolean UIVirtualJoystick::invertXOutputValue
	bool ___invertXOutputValue_8;
	// System.Boolean UIVirtualJoystick::invertYOutputValue
	bool ___invertYOutputValue_9;
	// UIVirtualJoystick/Event UIVirtualJoystick::joystickOutputEvent
	Event_t5D63277B4BC67CE95DE0316A422A69420D3A368D * ___joystickOutputEvent_10;

public:
	inline static int32_t get_offset_of_containerRect_4() { return static_cast<int32_t>(offsetof(UIVirtualJoystick_tEF0053DAA02EDBA00713D7B29C34394EA7574C2D, ___containerRect_4)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_containerRect_4() const { return ___containerRect_4; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_containerRect_4() { return &___containerRect_4; }
	inline void set_containerRect_4(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___containerRect_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___containerRect_4), (void*)value);
	}

	inline static int32_t get_offset_of_handleRect_5() { return static_cast<int32_t>(offsetof(UIVirtualJoystick_tEF0053DAA02EDBA00713D7B29C34394EA7574C2D, ___handleRect_5)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_handleRect_5() const { return ___handleRect_5; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_handleRect_5() { return &___handleRect_5; }
	inline void set_handleRect_5(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___handleRect_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___handleRect_5), (void*)value);
	}

	inline static int32_t get_offset_of_joystickRange_6() { return static_cast<int32_t>(offsetof(UIVirtualJoystick_tEF0053DAA02EDBA00713D7B29C34394EA7574C2D, ___joystickRange_6)); }
	inline float get_joystickRange_6() const { return ___joystickRange_6; }
	inline float* get_address_of_joystickRange_6() { return &___joystickRange_6; }
	inline void set_joystickRange_6(float value)
	{
		___joystickRange_6 = value;
	}

	inline static int32_t get_offset_of_magnitudeMultiplier_7() { return static_cast<int32_t>(offsetof(UIVirtualJoystick_tEF0053DAA02EDBA00713D7B29C34394EA7574C2D, ___magnitudeMultiplier_7)); }
	inline float get_magnitudeMultiplier_7() const { return ___magnitudeMultiplier_7; }
	inline float* get_address_of_magnitudeMultiplier_7() { return &___magnitudeMultiplier_7; }
	inline void set_magnitudeMultiplier_7(float value)
	{
		___magnitudeMultiplier_7 = value;
	}

	inline static int32_t get_offset_of_invertXOutputValue_8() { return static_cast<int32_t>(offsetof(UIVirtualJoystick_tEF0053DAA02EDBA00713D7B29C34394EA7574C2D, ___invertXOutputValue_8)); }
	inline bool get_invertXOutputValue_8() const { return ___invertXOutputValue_8; }
	inline bool* get_address_of_invertXOutputValue_8() { return &___invertXOutputValue_8; }
	inline void set_invertXOutputValue_8(bool value)
	{
		___invertXOutputValue_8 = value;
	}

	inline static int32_t get_offset_of_invertYOutputValue_9() { return static_cast<int32_t>(offsetof(UIVirtualJoystick_tEF0053DAA02EDBA00713D7B29C34394EA7574C2D, ___invertYOutputValue_9)); }
	inline bool get_invertYOutputValue_9() const { return ___invertYOutputValue_9; }
	inline bool* get_address_of_invertYOutputValue_9() { return &___invertYOutputValue_9; }
	inline void set_invertYOutputValue_9(bool value)
	{
		___invertYOutputValue_9 = value;
	}

	inline static int32_t get_offset_of_joystickOutputEvent_10() { return static_cast<int32_t>(offsetof(UIVirtualJoystick_tEF0053DAA02EDBA00713D7B29C34394EA7574C2D, ___joystickOutputEvent_10)); }
	inline Event_t5D63277B4BC67CE95DE0316A422A69420D3A368D * get_joystickOutputEvent_10() const { return ___joystickOutputEvent_10; }
	inline Event_t5D63277B4BC67CE95DE0316A422A69420D3A368D ** get_address_of_joystickOutputEvent_10() { return &___joystickOutputEvent_10; }
	inline void set_joystickOutputEvent_10(Event_t5D63277B4BC67CE95DE0316A422A69420D3A368D * value)
	{
		___joystickOutputEvent_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___joystickOutputEvent_10), (void*)value);
	}
};


// UIVirtualTouchZone
struct  UIVirtualTouchZone_t2EB72E6BED3964232BD92D6723DEA7BA013BED23  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.RectTransform UIVirtualTouchZone::containerRect
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___containerRect_4;
	// UnityEngine.RectTransform UIVirtualTouchZone::handleRect
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___handleRect_5;
	// System.Boolean UIVirtualTouchZone::clampToMagnitude
	bool ___clampToMagnitude_6;
	// System.Single UIVirtualTouchZone::magnitudeMultiplier
	float ___magnitudeMultiplier_7;
	// System.Boolean UIVirtualTouchZone::invertXOutputValue
	bool ___invertXOutputValue_8;
	// System.Boolean UIVirtualTouchZone::invertYOutputValue
	bool ___invertYOutputValue_9;
	// UnityEngine.Vector2 UIVirtualTouchZone::pointerDownPosition
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___pointerDownPosition_10;
	// UnityEngine.Vector2 UIVirtualTouchZone::currentPointerPosition
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___currentPointerPosition_11;
	// UIVirtualTouchZone/Event UIVirtualTouchZone::touchZoneOutputEvent
	Event_t1E1104A6087ED46DBF720AC59C7A8E63B974639D * ___touchZoneOutputEvent_12;

public:
	inline static int32_t get_offset_of_containerRect_4() { return static_cast<int32_t>(offsetof(UIVirtualTouchZone_t2EB72E6BED3964232BD92D6723DEA7BA013BED23, ___containerRect_4)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_containerRect_4() const { return ___containerRect_4; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_containerRect_4() { return &___containerRect_4; }
	inline void set_containerRect_4(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___containerRect_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___containerRect_4), (void*)value);
	}

	inline static int32_t get_offset_of_handleRect_5() { return static_cast<int32_t>(offsetof(UIVirtualTouchZone_t2EB72E6BED3964232BD92D6723DEA7BA013BED23, ___handleRect_5)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_handleRect_5() const { return ___handleRect_5; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_handleRect_5() { return &___handleRect_5; }
	inline void set_handleRect_5(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___handleRect_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___handleRect_5), (void*)value);
	}

	inline static int32_t get_offset_of_clampToMagnitude_6() { return static_cast<int32_t>(offsetof(UIVirtualTouchZone_t2EB72E6BED3964232BD92D6723DEA7BA013BED23, ___clampToMagnitude_6)); }
	inline bool get_clampToMagnitude_6() const { return ___clampToMagnitude_6; }
	inline bool* get_address_of_clampToMagnitude_6() { return &___clampToMagnitude_6; }
	inline void set_clampToMagnitude_6(bool value)
	{
		___clampToMagnitude_6 = value;
	}

	inline static int32_t get_offset_of_magnitudeMultiplier_7() { return static_cast<int32_t>(offsetof(UIVirtualTouchZone_t2EB72E6BED3964232BD92D6723DEA7BA013BED23, ___magnitudeMultiplier_7)); }
	inline float get_magnitudeMultiplier_7() const { return ___magnitudeMultiplier_7; }
	inline float* get_address_of_magnitudeMultiplier_7() { return &___magnitudeMultiplier_7; }
	inline void set_magnitudeMultiplier_7(float value)
	{
		___magnitudeMultiplier_7 = value;
	}

	inline static int32_t get_offset_of_invertXOutputValue_8() { return static_cast<int32_t>(offsetof(UIVirtualTouchZone_t2EB72E6BED3964232BD92D6723DEA7BA013BED23, ___invertXOutputValue_8)); }
	inline bool get_invertXOutputValue_8() const { return ___invertXOutputValue_8; }
	inline bool* get_address_of_invertXOutputValue_8() { return &___invertXOutputValue_8; }
	inline void set_invertXOutputValue_8(bool value)
	{
		___invertXOutputValue_8 = value;
	}

	inline static int32_t get_offset_of_invertYOutputValue_9() { return static_cast<int32_t>(offsetof(UIVirtualTouchZone_t2EB72E6BED3964232BD92D6723DEA7BA013BED23, ___invertYOutputValue_9)); }
	inline bool get_invertYOutputValue_9() const { return ___invertYOutputValue_9; }
	inline bool* get_address_of_invertYOutputValue_9() { return &___invertYOutputValue_9; }
	inline void set_invertYOutputValue_9(bool value)
	{
		___invertYOutputValue_9 = value;
	}

	inline static int32_t get_offset_of_pointerDownPosition_10() { return static_cast<int32_t>(offsetof(UIVirtualTouchZone_t2EB72E6BED3964232BD92D6723DEA7BA013BED23, ___pointerDownPosition_10)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_pointerDownPosition_10() const { return ___pointerDownPosition_10; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_pointerDownPosition_10() { return &___pointerDownPosition_10; }
	inline void set_pointerDownPosition_10(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___pointerDownPosition_10 = value;
	}

	inline static int32_t get_offset_of_currentPointerPosition_11() { return static_cast<int32_t>(offsetof(UIVirtualTouchZone_t2EB72E6BED3964232BD92D6723DEA7BA013BED23, ___currentPointerPosition_11)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_currentPointerPosition_11() const { return ___currentPointerPosition_11; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_currentPointerPosition_11() { return &___currentPointerPosition_11; }
	inline void set_currentPointerPosition_11(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___currentPointerPosition_11 = value;
	}

	inline static int32_t get_offset_of_touchZoneOutputEvent_12() { return static_cast<int32_t>(offsetof(UIVirtualTouchZone_t2EB72E6BED3964232BD92D6723DEA7BA013BED23, ___touchZoneOutputEvent_12)); }
	inline Event_t1E1104A6087ED46DBF720AC59C7A8E63B974639D * get_touchZoneOutputEvent_12() const { return ___touchZoneOutputEvent_12; }
	inline Event_t1E1104A6087ED46DBF720AC59C7A8E63B974639D ** get_address_of_touchZoneOutputEvent_12() { return &___touchZoneOutputEvent_12; }
	inline void set_touchZoneOutputEvent_12(Event_t1E1104A6087ED46DBF720AC59C7A8E63B974639D * value)
	{
		___touchZoneOutputEvent_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___touchZoneOutputEvent_12), (void*)value);
	}
};


// VRControls
struct  VRControls_tEAE1C9E737B679D74AD0DA693794E297010CFA77  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.InputSystem.PlayerInput VRControls::playerInput
	PlayerInput_tCB9A1A84F447F3392889C457C9BE413891FE848A * ___playerInput_4;
	// PrometeoCarController VRControls::carController
	PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E * ___carController_5;
	// SignalsCar VRControls::signal
	SignalsCar_t381616BF85EDCCF005FD247DC7E72DB6EAD4DAB0 * ___signal_6;
	// UnityEngine.UI.Button VRControls::a_button
	Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * ___a_button_7;

public:
	inline static int32_t get_offset_of_playerInput_4() { return static_cast<int32_t>(offsetof(VRControls_tEAE1C9E737B679D74AD0DA693794E297010CFA77, ___playerInput_4)); }
	inline PlayerInput_tCB9A1A84F447F3392889C457C9BE413891FE848A * get_playerInput_4() const { return ___playerInput_4; }
	inline PlayerInput_tCB9A1A84F447F3392889C457C9BE413891FE848A ** get_address_of_playerInput_4() { return &___playerInput_4; }
	inline void set_playerInput_4(PlayerInput_tCB9A1A84F447F3392889C457C9BE413891FE848A * value)
	{
		___playerInput_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___playerInput_4), (void*)value);
	}

	inline static int32_t get_offset_of_carController_5() { return static_cast<int32_t>(offsetof(VRControls_tEAE1C9E737B679D74AD0DA693794E297010CFA77, ___carController_5)); }
	inline PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E * get_carController_5() const { return ___carController_5; }
	inline PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E ** get_address_of_carController_5() { return &___carController_5; }
	inline void set_carController_5(PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E * value)
	{
		___carController_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___carController_5), (void*)value);
	}

	inline static int32_t get_offset_of_signal_6() { return static_cast<int32_t>(offsetof(VRControls_tEAE1C9E737B679D74AD0DA693794E297010CFA77, ___signal_6)); }
	inline SignalsCar_t381616BF85EDCCF005FD247DC7E72DB6EAD4DAB0 * get_signal_6() const { return ___signal_6; }
	inline SignalsCar_t381616BF85EDCCF005FD247DC7E72DB6EAD4DAB0 ** get_address_of_signal_6() { return &___signal_6; }
	inline void set_signal_6(SignalsCar_t381616BF85EDCCF005FD247DC7E72DB6EAD4DAB0 * value)
	{
		___signal_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___signal_6), (void*)value);
	}

	inline static int32_t get_offset_of_a_button_7() { return static_cast<int32_t>(offsetof(VRControls_tEAE1C9E737B679D74AD0DA693794E297010CFA77, ___a_button_7)); }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * get_a_button_7() const { return ___a_button_7; }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D ** get_address_of_a_button_7() { return &___a_button_7; }
	inline void set_a_button_7(Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * value)
	{
		___a_button_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___a_button_7), (void*)value);
	}
};


// WalkerWay
struct  WalkerWay_tE1DBB9C531DC74D2654F2AB74C9D6E1304A418DD  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.GameObject[] WalkerWay::walkers
	GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* ___walkers_4;
	// System.Boolean WalkerWay::canPass
	bool ___canPass_5;
	// System.Boolean WalkerWay::didCarCome
	bool ___didCarCome_6;
	// UnityEngine.Color WalkerWay::stopColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___stopColor_7;
	// UnityEngine.Color WalkerWay::passColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___passColor_8;
	// System.Int32 WalkerWay::timer
	int32_t ___timer_9;
	// System.Single WalkerWay::speed
	float ___speed_10;

public:
	inline static int32_t get_offset_of_walkers_4() { return static_cast<int32_t>(offsetof(WalkerWay_tE1DBB9C531DC74D2654F2AB74C9D6E1304A418DD, ___walkers_4)); }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* get_walkers_4() const { return ___walkers_4; }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642** get_address_of_walkers_4() { return &___walkers_4; }
	inline void set_walkers_4(GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* value)
	{
		___walkers_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___walkers_4), (void*)value);
	}

	inline static int32_t get_offset_of_canPass_5() { return static_cast<int32_t>(offsetof(WalkerWay_tE1DBB9C531DC74D2654F2AB74C9D6E1304A418DD, ___canPass_5)); }
	inline bool get_canPass_5() const { return ___canPass_5; }
	inline bool* get_address_of_canPass_5() { return &___canPass_5; }
	inline void set_canPass_5(bool value)
	{
		___canPass_5 = value;
	}

	inline static int32_t get_offset_of_didCarCome_6() { return static_cast<int32_t>(offsetof(WalkerWay_tE1DBB9C531DC74D2654F2AB74C9D6E1304A418DD, ___didCarCome_6)); }
	inline bool get_didCarCome_6() const { return ___didCarCome_6; }
	inline bool* get_address_of_didCarCome_6() { return &___didCarCome_6; }
	inline void set_didCarCome_6(bool value)
	{
		___didCarCome_6 = value;
	}

	inline static int32_t get_offset_of_stopColor_7() { return static_cast<int32_t>(offsetof(WalkerWay_tE1DBB9C531DC74D2654F2AB74C9D6E1304A418DD, ___stopColor_7)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_stopColor_7() const { return ___stopColor_7; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_stopColor_7() { return &___stopColor_7; }
	inline void set_stopColor_7(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___stopColor_7 = value;
	}

	inline static int32_t get_offset_of_passColor_8() { return static_cast<int32_t>(offsetof(WalkerWay_tE1DBB9C531DC74D2654F2AB74C9D6E1304A418DD, ___passColor_8)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_passColor_8() const { return ___passColor_8; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_passColor_8() { return &___passColor_8; }
	inline void set_passColor_8(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___passColor_8 = value;
	}

	inline static int32_t get_offset_of_timer_9() { return static_cast<int32_t>(offsetof(WalkerWay_tE1DBB9C531DC74D2654F2AB74C9D6E1304A418DD, ___timer_9)); }
	inline int32_t get_timer_9() const { return ___timer_9; }
	inline int32_t* get_address_of_timer_9() { return &___timer_9; }
	inline void set_timer_9(int32_t value)
	{
		___timer_9 = value;
	}

	inline static int32_t get_offset_of_speed_10() { return static_cast<int32_t>(offsetof(WalkerWay_tE1DBB9C531DC74D2654F2AB74C9D6E1304A418DD, ___speed_10)); }
	inline float get_speed_10() const { return ___speed_10; }
	inline float* get_address_of_speed_10() { return &___speed_10; }
	inline void set_speed_10(float value)
	{
		___speed_10 = value;
	}
};


// Water
struct  Water_t49D731BAF5A7CB66C04A942899CCE2391A114F4E  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// Water/WaterMode Water::m_WaterMode
	int32_t ___m_WaterMode_4;
	// System.Boolean Water::m_DisablePixelLights
	bool ___m_DisablePixelLights_5;
	// System.Int32 Water::m_TextureSize
	int32_t ___m_TextureSize_6;
	// System.Single Water::m_ClipPlaneOffset
	float ___m_ClipPlaneOffset_7;
	// UnityEngine.LayerMask Water::m_ReflectLayers
	LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  ___m_ReflectLayers_8;
	// UnityEngine.LayerMask Water::m_RefractLayers
	LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  ___m_RefractLayers_9;
	// System.Collections.Hashtable Water::m_ReflectionCameras
	Hashtable_t7565AB92A12227AD5BADD6911F10D87EE52509AC * ___m_ReflectionCameras_10;
	// System.Collections.Hashtable Water::m_RefractionCameras
	Hashtable_t7565AB92A12227AD5BADD6911F10D87EE52509AC * ___m_RefractionCameras_11;
	// UnityEngine.RenderTexture Water::m_ReflectionTexture
	RenderTexture_t5FE7A5B47EF962A0E8D7BEBA05E9FC87D49A1849 * ___m_ReflectionTexture_12;
	// UnityEngine.RenderTexture Water::m_RefractionTexture
	RenderTexture_t5FE7A5B47EF962A0E8D7BEBA05E9FC87D49A1849 * ___m_RefractionTexture_13;
	// Water/WaterMode Water::m_HardwareWaterSupport
	int32_t ___m_HardwareWaterSupport_14;
	// System.Int32 Water::m_OldReflectionTextureSize
	int32_t ___m_OldReflectionTextureSize_15;
	// System.Int32 Water::m_OldRefractionTextureSize
	int32_t ___m_OldRefractionTextureSize_16;

public:
	inline static int32_t get_offset_of_m_WaterMode_4() { return static_cast<int32_t>(offsetof(Water_t49D731BAF5A7CB66C04A942899CCE2391A114F4E, ___m_WaterMode_4)); }
	inline int32_t get_m_WaterMode_4() const { return ___m_WaterMode_4; }
	inline int32_t* get_address_of_m_WaterMode_4() { return &___m_WaterMode_4; }
	inline void set_m_WaterMode_4(int32_t value)
	{
		___m_WaterMode_4 = value;
	}

	inline static int32_t get_offset_of_m_DisablePixelLights_5() { return static_cast<int32_t>(offsetof(Water_t49D731BAF5A7CB66C04A942899CCE2391A114F4E, ___m_DisablePixelLights_5)); }
	inline bool get_m_DisablePixelLights_5() const { return ___m_DisablePixelLights_5; }
	inline bool* get_address_of_m_DisablePixelLights_5() { return &___m_DisablePixelLights_5; }
	inline void set_m_DisablePixelLights_5(bool value)
	{
		___m_DisablePixelLights_5 = value;
	}

	inline static int32_t get_offset_of_m_TextureSize_6() { return static_cast<int32_t>(offsetof(Water_t49D731BAF5A7CB66C04A942899CCE2391A114F4E, ___m_TextureSize_6)); }
	inline int32_t get_m_TextureSize_6() const { return ___m_TextureSize_6; }
	inline int32_t* get_address_of_m_TextureSize_6() { return &___m_TextureSize_6; }
	inline void set_m_TextureSize_6(int32_t value)
	{
		___m_TextureSize_6 = value;
	}

	inline static int32_t get_offset_of_m_ClipPlaneOffset_7() { return static_cast<int32_t>(offsetof(Water_t49D731BAF5A7CB66C04A942899CCE2391A114F4E, ___m_ClipPlaneOffset_7)); }
	inline float get_m_ClipPlaneOffset_7() const { return ___m_ClipPlaneOffset_7; }
	inline float* get_address_of_m_ClipPlaneOffset_7() { return &___m_ClipPlaneOffset_7; }
	inline void set_m_ClipPlaneOffset_7(float value)
	{
		___m_ClipPlaneOffset_7 = value;
	}

	inline static int32_t get_offset_of_m_ReflectLayers_8() { return static_cast<int32_t>(offsetof(Water_t49D731BAF5A7CB66C04A942899CCE2391A114F4E, ___m_ReflectLayers_8)); }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  get_m_ReflectLayers_8() const { return ___m_ReflectLayers_8; }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8 * get_address_of_m_ReflectLayers_8() { return &___m_ReflectLayers_8; }
	inline void set_m_ReflectLayers_8(LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  value)
	{
		___m_ReflectLayers_8 = value;
	}

	inline static int32_t get_offset_of_m_RefractLayers_9() { return static_cast<int32_t>(offsetof(Water_t49D731BAF5A7CB66C04A942899CCE2391A114F4E, ___m_RefractLayers_9)); }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  get_m_RefractLayers_9() const { return ___m_RefractLayers_9; }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8 * get_address_of_m_RefractLayers_9() { return &___m_RefractLayers_9; }
	inline void set_m_RefractLayers_9(LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  value)
	{
		___m_RefractLayers_9 = value;
	}

	inline static int32_t get_offset_of_m_ReflectionCameras_10() { return static_cast<int32_t>(offsetof(Water_t49D731BAF5A7CB66C04A942899CCE2391A114F4E, ___m_ReflectionCameras_10)); }
	inline Hashtable_t7565AB92A12227AD5BADD6911F10D87EE52509AC * get_m_ReflectionCameras_10() const { return ___m_ReflectionCameras_10; }
	inline Hashtable_t7565AB92A12227AD5BADD6911F10D87EE52509AC ** get_address_of_m_ReflectionCameras_10() { return &___m_ReflectionCameras_10; }
	inline void set_m_ReflectionCameras_10(Hashtable_t7565AB92A12227AD5BADD6911F10D87EE52509AC * value)
	{
		___m_ReflectionCameras_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ReflectionCameras_10), (void*)value);
	}

	inline static int32_t get_offset_of_m_RefractionCameras_11() { return static_cast<int32_t>(offsetof(Water_t49D731BAF5A7CB66C04A942899CCE2391A114F4E, ___m_RefractionCameras_11)); }
	inline Hashtable_t7565AB92A12227AD5BADD6911F10D87EE52509AC * get_m_RefractionCameras_11() const { return ___m_RefractionCameras_11; }
	inline Hashtable_t7565AB92A12227AD5BADD6911F10D87EE52509AC ** get_address_of_m_RefractionCameras_11() { return &___m_RefractionCameras_11; }
	inline void set_m_RefractionCameras_11(Hashtable_t7565AB92A12227AD5BADD6911F10D87EE52509AC * value)
	{
		___m_RefractionCameras_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_RefractionCameras_11), (void*)value);
	}

	inline static int32_t get_offset_of_m_ReflectionTexture_12() { return static_cast<int32_t>(offsetof(Water_t49D731BAF5A7CB66C04A942899CCE2391A114F4E, ___m_ReflectionTexture_12)); }
	inline RenderTexture_t5FE7A5B47EF962A0E8D7BEBA05E9FC87D49A1849 * get_m_ReflectionTexture_12() const { return ___m_ReflectionTexture_12; }
	inline RenderTexture_t5FE7A5B47EF962A0E8D7BEBA05E9FC87D49A1849 ** get_address_of_m_ReflectionTexture_12() { return &___m_ReflectionTexture_12; }
	inline void set_m_ReflectionTexture_12(RenderTexture_t5FE7A5B47EF962A0E8D7BEBA05E9FC87D49A1849 * value)
	{
		___m_ReflectionTexture_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ReflectionTexture_12), (void*)value);
	}

	inline static int32_t get_offset_of_m_RefractionTexture_13() { return static_cast<int32_t>(offsetof(Water_t49D731BAF5A7CB66C04A942899CCE2391A114F4E, ___m_RefractionTexture_13)); }
	inline RenderTexture_t5FE7A5B47EF962A0E8D7BEBA05E9FC87D49A1849 * get_m_RefractionTexture_13() const { return ___m_RefractionTexture_13; }
	inline RenderTexture_t5FE7A5B47EF962A0E8D7BEBA05E9FC87D49A1849 ** get_address_of_m_RefractionTexture_13() { return &___m_RefractionTexture_13; }
	inline void set_m_RefractionTexture_13(RenderTexture_t5FE7A5B47EF962A0E8D7BEBA05E9FC87D49A1849 * value)
	{
		___m_RefractionTexture_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_RefractionTexture_13), (void*)value);
	}

	inline static int32_t get_offset_of_m_HardwareWaterSupport_14() { return static_cast<int32_t>(offsetof(Water_t49D731BAF5A7CB66C04A942899CCE2391A114F4E, ___m_HardwareWaterSupport_14)); }
	inline int32_t get_m_HardwareWaterSupport_14() const { return ___m_HardwareWaterSupport_14; }
	inline int32_t* get_address_of_m_HardwareWaterSupport_14() { return &___m_HardwareWaterSupport_14; }
	inline void set_m_HardwareWaterSupport_14(int32_t value)
	{
		___m_HardwareWaterSupport_14 = value;
	}

	inline static int32_t get_offset_of_m_OldReflectionTextureSize_15() { return static_cast<int32_t>(offsetof(Water_t49D731BAF5A7CB66C04A942899CCE2391A114F4E, ___m_OldReflectionTextureSize_15)); }
	inline int32_t get_m_OldReflectionTextureSize_15() const { return ___m_OldReflectionTextureSize_15; }
	inline int32_t* get_address_of_m_OldReflectionTextureSize_15() { return &___m_OldReflectionTextureSize_15; }
	inline void set_m_OldReflectionTextureSize_15(int32_t value)
	{
		___m_OldReflectionTextureSize_15 = value;
	}

	inline static int32_t get_offset_of_m_OldRefractionTextureSize_16() { return static_cast<int32_t>(offsetof(Water_t49D731BAF5A7CB66C04A942899CCE2391A114F4E, ___m_OldRefractionTextureSize_16)); }
	inline int32_t get_m_OldRefractionTextureSize_16() const { return ___m_OldRefractionTextureSize_16; }
	inline int32_t* get_address_of_m_OldRefractionTextureSize_16() { return &___m_OldRefractionTextureSize_16; }
	inline void set_m_OldRefractionTextureSize_16(int32_t value)
	{
		___m_OldRefractionTextureSize_16 = value;
	}
};

struct Water_t49D731BAF5A7CB66C04A942899CCE2391A114F4E_StaticFields
{
public:
	// System.Boolean Water::s_InsideWater
	bool ___s_InsideWater_17;

public:
	inline static int32_t get_offset_of_s_InsideWater_17() { return static_cast<int32_t>(offsetof(Water_t49D731BAF5A7CB66C04A942899CCE2391A114F4E_StaticFields, ___s_InsideWater_17)); }
	inline bool get_s_InsideWater_17() const { return ___s_InsideWater_17; }
	inline bool* get_address_of_s_InsideWater_17() { return &___s_InsideWater_17; }
	inline void set_s_InsideWater_17(bool value)
	{
		___s_InsideWater_17 = value;
	}
};


// UnityEngine.XR.WindowsMR.WindowsMRLoader
struct  WindowsMRLoader_t46FCCC8CDD8C8F604D806C954A1BCF9EE817AF92  : public XRLoaderHelper_t37A55C343AC31D25BB3EBD203DABFA357F51C013
{
public:

public:
};

struct WindowsMRLoader_t46FCCC8CDD8C8F604D806C954A1BCF9EE817AF92_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.XR.ARSubsystems.XRSessionSubsystemDescriptor> UnityEngine.XR.WindowsMR.WindowsMRLoader::s_SessionSubsystemDescriptors
	List_1_tDA25459A722F5B00FFE84463D9952660BC3F6673 * ___s_SessionSubsystemDescriptors_5;
	// System.Collections.Generic.List`1<UnityEngine.XR.XRDisplaySubsystemDescriptor> UnityEngine.XR.WindowsMR.WindowsMRLoader::s_DisplaySubsystemDescriptors
	List_1_t1BC024192EE6F54EADD3239A60DB2A4A0B4B5048 * ___s_DisplaySubsystemDescriptors_6;
	// System.Collections.Generic.List`1<UnityEngine.XR.XRInputSubsystemDescriptor> UnityEngine.XR.WindowsMR.WindowsMRLoader::s_InputSubsystemDescriptors
	List_1_t885BD663DFFEB6C32E74934BE1CE00D566657BA0 * ___s_InputSubsystemDescriptors_7;
	// System.Collections.Generic.List`1<UnityEngine.XR.ARSubsystems.XRAnchorSubsystemDescriptor> UnityEngine.XR.WindowsMR.WindowsMRLoader::s_AnchorSubsystemDescriptors
	List_1_tDED98C236097B36F9015B396398179A6F8A62E50 * ___s_AnchorSubsystemDescriptors_8;
	// System.Collections.Generic.List`1<UnityEngine.XR.XRMeshSubsystemDescriptor> UnityEngine.XR.WindowsMR.WindowsMRLoader::s_MeshSubsystemDescriptors
	List_1_tA4CB3CC063D44B52D336C5DDA258EF7CE9B98A94 * ___s_MeshSubsystemDescriptors_9;
	// System.Collections.Generic.List`1<UnityEngine.XR.InteractionSubsystems.XRGestureSubsystemDescriptor> UnityEngine.XR.WindowsMR.WindowsMRLoader::s_GestureSubsystemDescriptors
	List_1_tFDF63FE53408489D6A2F16814E1594D8AF3B4942 * ___s_GestureSubsystemDescriptors_10;

public:
	inline static int32_t get_offset_of_s_SessionSubsystemDescriptors_5() { return static_cast<int32_t>(offsetof(WindowsMRLoader_t46FCCC8CDD8C8F604D806C954A1BCF9EE817AF92_StaticFields, ___s_SessionSubsystemDescriptors_5)); }
	inline List_1_tDA25459A722F5B00FFE84463D9952660BC3F6673 * get_s_SessionSubsystemDescriptors_5() const { return ___s_SessionSubsystemDescriptors_5; }
	inline List_1_tDA25459A722F5B00FFE84463D9952660BC3F6673 ** get_address_of_s_SessionSubsystemDescriptors_5() { return &___s_SessionSubsystemDescriptors_5; }
	inline void set_s_SessionSubsystemDescriptors_5(List_1_tDA25459A722F5B00FFE84463D9952660BC3F6673 * value)
	{
		___s_SessionSubsystemDescriptors_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_SessionSubsystemDescriptors_5), (void*)value);
	}

	inline static int32_t get_offset_of_s_DisplaySubsystemDescriptors_6() { return static_cast<int32_t>(offsetof(WindowsMRLoader_t46FCCC8CDD8C8F604D806C954A1BCF9EE817AF92_StaticFields, ___s_DisplaySubsystemDescriptors_6)); }
	inline List_1_t1BC024192EE6F54EADD3239A60DB2A4A0B4B5048 * get_s_DisplaySubsystemDescriptors_6() const { return ___s_DisplaySubsystemDescriptors_6; }
	inline List_1_t1BC024192EE6F54EADD3239A60DB2A4A0B4B5048 ** get_address_of_s_DisplaySubsystemDescriptors_6() { return &___s_DisplaySubsystemDescriptors_6; }
	inline void set_s_DisplaySubsystemDescriptors_6(List_1_t1BC024192EE6F54EADD3239A60DB2A4A0B4B5048 * value)
	{
		___s_DisplaySubsystemDescriptors_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_DisplaySubsystemDescriptors_6), (void*)value);
	}

	inline static int32_t get_offset_of_s_InputSubsystemDescriptors_7() { return static_cast<int32_t>(offsetof(WindowsMRLoader_t46FCCC8CDD8C8F604D806C954A1BCF9EE817AF92_StaticFields, ___s_InputSubsystemDescriptors_7)); }
	inline List_1_t885BD663DFFEB6C32E74934BE1CE00D566657BA0 * get_s_InputSubsystemDescriptors_7() const { return ___s_InputSubsystemDescriptors_7; }
	inline List_1_t885BD663DFFEB6C32E74934BE1CE00D566657BA0 ** get_address_of_s_InputSubsystemDescriptors_7() { return &___s_InputSubsystemDescriptors_7; }
	inline void set_s_InputSubsystemDescriptors_7(List_1_t885BD663DFFEB6C32E74934BE1CE00D566657BA0 * value)
	{
		___s_InputSubsystemDescriptors_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_InputSubsystemDescriptors_7), (void*)value);
	}

	inline static int32_t get_offset_of_s_AnchorSubsystemDescriptors_8() { return static_cast<int32_t>(offsetof(WindowsMRLoader_t46FCCC8CDD8C8F604D806C954A1BCF9EE817AF92_StaticFields, ___s_AnchorSubsystemDescriptors_8)); }
	inline List_1_tDED98C236097B36F9015B396398179A6F8A62E50 * get_s_AnchorSubsystemDescriptors_8() const { return ___s_AnchorSubsystemDescriptors_8; }
	inline List_1_tDED98C236097B36F9015B396398179A6F8A62E50 ** get_address_of_s_AnchorSubsystemDescriptors_8() { return &___s_AnchorSubsystemDescriptors_8; }
	inline void set_s_AnchorSubsystemDescriptors_8(List_1_tDED98C236097B36F9015B396398179A6F8A62E50 * value)
	{
		___s_AnchorSubsystemDescriptors_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_AnchorSubsystemDescriptors_8), (void*)value);
	}

	inline static int32_t get_offset_of_s_MeshSubsystemDescriptors_9() { return static_cast<int32_t>(offsetof(WindowsMRLoader_t46FCCC8CDD8C8F604D806C954A1BCF9EE817AF92_StaticFields, ___s_MeshSubsystemDescriptors_9)); }
	inline List_1_tA4CB3CC063D44B52D336C5DDA258EF7CE9B98A94 * get_s_MeshSubsystemDescriptors_9() const { return ___s_MeshSubsystemDescriptors_9; }
	inline List_1_tA4CB3CC063D44B52D336C5DDA258EF7CE9B98A94 ** get_address_of_s_MeshSubsystemDescriptors_9() { return &___s_MeshSubsystemDescriptors_9; }
	inline void set_s_MeshSubsystemDescriptors_9(List_1_tA4CB3CC063D44B52D336C5DDA258EF7CE9B98A94 * value)
	{
		___s_MeshSubsystemDescriptors_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_MeshSubsystemDescriptors_9), (void*)value);
	}

	inline static int32_t get_offset_of_s_GestureSubsystemDescriptors_10() { return static_cast<int32_t>(offsetof(WindowsMRLoader_t46FCCC8CDD8C8F604D806C954A1BCF9EE817AF92_StaticFields, ___s_GestureSubsystemDescriptors_10)); }
	inline List_1_tFDF63FE53408489D6A2F16814E1594D8AF3B4942 * get_s_GestureSubsystemDescriptors_10() const { return ___s_GestureSubsystemDescriptors_10; }
	inline List_1_tFDF63FE53408489D6A2F16814E1594D8AF3B4942 ** get_address_of_s_GestureSubsystemDescriptors_10() { return &___s_GestureSubsystemDescriptors_10; }
	inline void set_s_GestureSubsystemDescriptors_10(List_1_tFDF63FE53408489D6A2F16814E1594D8AF3B4942 * value)
	{
		___s_GestureSubsystemDescriptors_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_GestureSubsystemDescriptors_10), (void*)value);
	}
};


// UnityEngine.InputSystem.XR.XRController
struct  XRController_tB2474121816D6124DF5CF8739FED669945C52778  : public TrackedDevice_tDF5E09C055D0CA92DF8930A7B45EDA3BBA14ED71
{
public:

public:
};


// UnityEngine.InputSystem.XR.XRHMD
struct  XRHMD_tF118D29FCF741F20C24D0AEA97DC5F78F92A92BE  : public TrackedDevice_tDF5E09C055D0CA92DF8930A7B45EDA3BBA14ED71
{
public:
	// UnityEngine.InputSystem.Controls.Vector3Control UnityEngine.InputSystem.XR.XRHMD::<leftEyePosition>k__BackingField
	Vector3Control_t915A3DA3C3E367B6F5587853E96EAD5215D328AE * ___U3CleftEyePositionU3Ek__BackingField_43;
	// UnityEngine.InputSystem.Controls.QuaternionControl UnityEngine.InputSystem.XR.XRHMD::<leftEyeRotation>k__BackingField
	QuaternionControl_t7BEFE3CFC03CC5CE3F779DDCB5AA2C011F5C7A48 * ___U3CleftEyeRotationU3Ek__BackingField_44;
	// UnityEngine.InputSystem.Controls.Vector3Control UnityEngine.InputSystem.XR.XRHMD::<rightEyePosition>k__BackingField
	Vector3Control_t915A3DA3C3E367B6F5587853E96EAD5215D328AE * ___U3CrightEyePositionU3Ek__BackingField_45;
	// UnityEngine.InputSystem.Controls.QuaternionControl UnityEngine.InputSystem.XR.XRHMD::<rightEyeRotation>k__BackingField
	QuaternionControl_t7BEFE3CFC03CC5CE3F779DDCB5AA2C011F5C7A48 * ___U3CrightEyeRotationU3Ek__BackingField_46;
	// UnityEngine.InputSystem.Controls.Vector3Control UnityEngine.InputSystem.XR.XRHMD::<centerEyePosition>k__BackingField
	Vector3Control_t915A3DA3C3E367B6F5587853E96EAD5215D328AE * ___U3CcenterEyePositionU3Ek__BackingField_47;
	// UnityEngine.InputSystem.Controls.QuaternionControl UnityEngine.InputSystem.XR.XRHMD::<centerEyeRotation>k__BackingField
	QuaternionControl_t7BEFE3CFC03CC5CE3F779DDCB5AA2C011F5C7A48 * ___U3CcenterEyeRotationU3Ek__BackingField_48;

public:
	inline static int32_t get_offset_of_U3CleftEyePositionU3Ek__BackingField_43() { return static_cast<int32_t>(offsetof(XRHMD_tF118D29FCF741F20C24D0AEA97DC5F78F92A92BE, ___U3CleftEyePositionU3Ek__BackingField_43)); }
	inline Vector3Control_t915A3DA3C3E367B6F5587853E96EAD5215D328AE * get_U3CleftEyePositionU3Ek__BackingField_43() const { return ___U3CleftEyePositionU3Ek__BackingField_43; }
	inline Vector3Control_t915A3DA3C3E367B6F5587853E96EAD5215D328AE ** get_address_of_U3CleftEyePositionU3Ek__BackingField_43() { return &___U3CleftEyePositionU3Ek__BackingField_43; }
	inline void set_U3CleftEyePositionU3Ek__BackingField_43(Vector3Control_t915A3DA3C3E367B6F5587853E96EAD5215D328AE * value)
	{
		___U3CleftEyePositionU3Ek__BackingField_43 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CleftEyePositionU3Ek__BackingField_43), (void*)value);
	}

	inline static int32_t get_offset_of_U3CleftEyeRotationU3Ek__BackingField_44() { return static_cast<int32_t>(offsetof(XRHMD_tF118D29FCF741F20C24D0AEA97DC5F78F92A92BE, ___U3CleftEyeRotationU3Ek__BackingField_44)); }
	inline QuaternionControl_t7BEFE3CFC03CC5CE3F779DDCB5AA2C011F5C7A48 * get_U3CleftEyeRotationU3Ek__BackingField_44() const { return ___U3CleftEyeRotationU3Ek__BackingField_44; }
	inline QuaternionControl_t7BEFE3CFC03CC5CE3F779DDCB5AA2C011F5C7A48 ** get_address_of_U3CleftEyeRotationU3Ek__BackingField_44() { return &___U3CleftEyeRotationU3Ek__BackingField_44; }
	inline void set_U3CleftEyeRotationU3Ek__BackingField_44(QuaternionControl_t7BEFE3CFC03CC5CE3F779DDCB5AA2C011F5C7A48 * value)
	{
		___U3CleftEyeRotationU3Ek__BackingField_44 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CleftEyeRotationU3Ek__BackingField_44), (void*)value);
	}

	inline static int32_t get_offset_of_U3CrightEyePositionU3Ek__BackingField_45() { return static_cast<int32_t>(offsetof(XRHMD_tF118D29FCF741F20C24D0AEA97DC5F78F92A92BE, ___U3CrightEyePositionU3Ek__BackingField_45)); }
	inline Vector3Control_t915A3DA3C3E367B6F5587853E96EAD5215D328AE * get_U3CrightEyePositionU3Ek__BackingField_45() const { return ___U3CrightEyePositionU3Ek__BackingField_45; }
	inline Vector3Control_t915A3DA3C3E367B6F5587853E96EAD5215D328AE ** get_address_of_U3CrightEyePositionU3Ek__BackingField_45() { return &___U3CrightEyePositionU3Ek__BackingField_45; }
	inline void set_U3CrightEyePositionU3Ek__BackingField_45(Vector3Control_t915A3DA3C3E367B6F5587853E96EAD5215D328AE * value)
	{
		___U3CrightEyePositionU3Ek__BackingField_45 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CrightEyePositionU3Ek__BackingField_45), (void*)value);
	}

	inline static int32_t get_offset_of_U3CrightEyeRotationU3Ek__BackingField_46() { return static_cast<int32_t>(offsetof(XRHMD_tF118D29FCF741F20C24D0AEA97DC5F78F92A92BE, ___U3CrightEyeRotationU3Ek__BackingField_46)); }
	inline QuaternionControl_t7BEFE3CFC03CC5CE3F779DDCB5AA2C011F5C7A48 * get_U3CrightEyeRotationU3Ek__BackingField_46() const { return ___U3CrightEyeRotationU3Ek__BackingField_46; }
	inline QuaternionControl_t7BEFE3CFC03CC5CE3F779DDCB5AA2C011F5C7A48 ** get_address_of_U3CrightEyeRotationU3Ek__BackingField_46() { return &___U3CrightEyeRotationU3Ek__BackingField_46; }
	inline void set_U3CrightEyeRotationU3Ek__BackingField_46(QuaternionControl_t7BEFE3CFC03CC5CE3F779DDCB5AA2C011F5C7A48 * value)
	{
		___U3CrightEyeRotationU3Ek__BackingField_46 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CrightEyeRotationU3Ek__BackingField_46), (void*)value);
	}

	inline static int32_t get_offset_of_U3CcenterEyePositionU3Ek__BackingField_47() { return static_cast<int32_t>(offsetof(XRHMD_tF118D29FCF741F20C24D0AEA97DC5F78F92A92BE, ___U3CcenterEyePositionU3Ek__BackingField_47)); }
	inline Vector3Control_t915A3DA3C3E367B6F5587853E96EAD5215D328AE * get_U3CcenterEyePositionU3Ek__BackingField_47() const { return ___U3CcenterEyePositionU3Ek__BackingField_47; }
	inline Vector3Control_t915A3DA3C3E367B6F5587853E96EAD5215D328AE ** get_address_of_U3CcenterEyePositionU3Ek__BackingField_47() { return &___U3CcenterEyePositionU3Ek__BackingField_47; }
	inline void set_U3CcenterEyePositionU3Ek__BackingField_47(Vector3Control_t915A3DA3C3E367B6F5587853E96EAD5215D328AE * value)
	{
		___U3CcenterEyePositionU3Ek__BackingField_47 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CcenterEyePositionU3Ek__BackingField_47), (void*)value);
	}

	inline static int32_t get_offset_of_U3CcenterEyeRotationU3Ek__BackingField_48() { return static_cast<int32_t>(offsetof(XRHMD_tF118D29FCF741F20C24D0AEA97DC5F78F92A92BE, ___U3CcenterEyeRotationU3Ek__BackingField_48)); }
	inline QuaternionControl_t7BEFE3CFC03CC5CE3F779DDCB5AA2C011F5C7A48 * get_U3CcenterEyeRotationU3Ek__BackingField_48() const { return ___U3CcenterEyeRotationU3Ek__BackingField_48; }
	inline QuaternionControl_t7BEFE3CFC03CC5CE3F779DDCB5AA2C011F5C7A48 ** get_address_of_U3CcenterEyeRotationU3Ek__BackingField_48() { return &___U3CcenterEyeRotationU3Ek__BackingField_48; }
	inline void set_U3CcenterEyeRotationU3Ek__BackingField_48(QuaternionControl_t7BEFE3CFC03CC5CE3F779DDCB5AA2C011F5C7A48 * value)
	{
		___U3CcenterEyeRotationU3Ek__BackingField_48 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CcenterEyeRotationU3Ek__BackingField_48), (void*)value);
	}
};


// UnityEngine.XR.WindowsMR.Input.HololensHand
struct  HololensHand_t95681D1B89FD7FB988895CB96106B5BAE993D022  : public XRController_tB2474121816D6124DF5CF8739FED669945C52778
{
public:
	// UnityEngine.InputSystem.Controls.IntegerControl UnityEngine.XR.WindowsMR.Input.HololensHand::<trackingState>k__BackingField
	IntegerControl_t68534DE7D89FBFC2952062997BB54680FE7DCCA4 * ___U3CtrackingStateU3Ek__BackingField_43;
	// UnityEngine.InputSystem.Controls.ButtonControl UnityEngine.XR.WindowsMR.Input.HololensHand::<isTracked>k__BackingField
	ButtonControl_t3918D56D674854AF9DFEB8CCF130919AAD9FCA68 * ___U3CisTrackedU3Ek__BackingField_44;
	// UnityEngine.InputSystem.Controls.Vector3Control UnityEngine.XR.WindowsMR.Input.HololensHand::<devicePosition>k__BackingField
	Vector3Control_t915A3DA3C3E367B6F5587853E96EAD5215D328AE * ___U3CdevicePositionU3Ek__BackingField_45;
	// UnityEngine.InputSystem.Controls.QuaternionControl UnityEngine.XR.WindowsMR.Input.HololensHand::<deviceRotation>k__BackingField
	QuaternionControl_t7BEFE3CFC03CC5CE3F779DDCB5AA2C011F5C7A48 * ___U3CdeviceRotationU3Ek__BackingField_46;
	// UnityEngine.InputSystem.Controls.Vector3Control UnityEngine.XR.WindowsMR.Input.HololensHand::<deviceVelocity>k__BackingField
	Vector3Control_t915A3DA3C3E367B6F5587853E96EAD5215D328AE * ___U3CdeviceVelocityU3Ek__BackingField_47;
	// UnityEngine.InputSystem.Controls.ButtonControl UnityEngine.XR.WindowsMR.Input.HololensHand::<airTap>k__BackingField
	ButtonControl_t3918D56D674854AF9DFEB8CCF130919AAD9FCA68 * ___U3CairTapU3Ek__BackingField_48;
	// UnityEngine.InputSystem.Controls.AxisControl UnityEngine.XR.WindowsMR.Input.HololensHand::<sourceLossRisk>k__BackingField
	AxisControl_tA997FF52442F7B08C7E13B7028393B4117248680 * ___U3CsourceLossRiskU3Ek__BackingField_49;
	// UnityEngine.InputSystem.Controls.Vector3Control UnityEngine.XR.WindowsMR.Input.HololensHand::<sourceLossMitigationDirection>k__BackingField
	Vector3Control_t915A3DA3C3E367B6F5587853E96EAD5215D328AE * ___U3CsourceLossMitigationDirectionU3Ek__BackingField_50;

public:
	inline static int32_t get_offset_of_U3CtrackingStateU3Ek__BackingField_43() { return static_cast<int32_t>(offsetof(HololensHand_t95681D1B89FD7FB988895CB96106B5BAE993D022, ___U3CtrackingStateU3Ek__BackingField_43)); }
	inline IntegerControl_t68534DE7D89FBFC2952062997BB54680FE7DCCA4 * get_U3CtrackingStateU3Ek__BackingField_43() const { return ___U3CtrackingStateU3Ek__BackingField_43; }
	inline IntegerControl_t68534DE7D89FBFC2952062997BB54680FE7DCCA4 ** get_address_of_U3CtrackingStateU3Ek__BackingField_43() { return &___U3CtrackingStateU3Ek__BackingField_43; }
	inline void set_U3CtrackingStateU3Ek__BackingField_43(IntegerControl_t68534DE7D89FBFC2952062997BB54680FE7DCCA4 * value)
	{
		___U3CtrackingStateU3Ek__BackingField_43 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CtrackingStateU3Ek__BackingField_43), (void*)value);
	}

	inline static int32_t get_offset_of_U3CisTrackedU3Ek__BackingField_44() { return static_cast<int32_t>(offsetof(HololensHand_t95681D1B89FD7FB988895CB96106B5BAE993D022, ___U3CisTrackedU3Ek__BackingField_44)); }
	inline ButtonControl_t3918D56D674854AF9DFEB8CCF130919AAD9FCA68 * get_U3CisTrackedU3Ek__BackingField_44() const { return ___U3CisTrackedU3Ek__BackingField_44; }
	inline ButtonControl_t3918D56D674854AF9DFEB8CCF130919AAD9FCA68 ** get_address_of_U3CisTrackedU3Ek__BackingField_44() { return &___U3CisTrackedU3Ek__BackingField_44; }
	inline void set_U3CisTrackedU3Ek__BackingField_44(ButtonControl_t3918D56D674854AF9DFEB8CCF130919AAD9FCA68 * value)
	{
		___U3CisTrackedU3Ek__BackingField_44 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CisTrackedU3Ek__BackingField_44), (void*)value);
	}

	inline static int32_t get_offset_of_U3CdevicePositionU3Ek__BackingField_45() { return static_cast<int32_t>(offsetof(HololensHand_t95681D1B89FD7FB988895CB96106B5BAE993D022, ___U3CdevicePositionU3Ek__BackingField_45)); }
	inline Vector3Control_t915A3DA3C3E367B6F5587853E96EAD5215D328AE * get_U3CdevicePositionU3Ek__BackingField_45() const { return ___U3CdevicePositionU3Ek__BackingField_45; }
	inline Vector3Control_t915A3DA3C3E367B6F5587853E96EAD5215D328AE ** get_address_of_U3CdevicePositionU3Ek__BackingField_45() { return &___U3CdevicePositionU3Ek__BackingField_45; }
	inline void set_U3CdevicePositionU3Ek__BackingField_45(Vector3Control_t915A3DA3C3E367B6F5587853E96EAD5215D328AE * value)
	{
		___U3CdevicePositionU3Ek__BackingField_45 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CdevicePositionU3Ek__BackingField_45), (void*)value);
	}

	inline static int32_t get_offset_of_U3CdeviceRotationU3Ek__BackingField_46() { return static_cast<int32_t>(offsetof(HololensHand_t95681D1B89FD7FB988895CB96106B5BAE993D022, ___U3CdeviceRotationU3Ek__BackingField_46)); }
	inline QuaternionControl_t7BEFE3CFC03CC5CE3F779DDCB5AA2C011F5C7A48 * get_U3CdeviceRotationU3Ek__BackingField_46() const { return ___U3CdeviceRotationU3Ek__BackingField_46; }
	inline QuaternionControl_t7BEFE3CFC03CC5CE3F779DDCB5AA2C011F5C7A48 ** get_address_of_U3CdeviceRotationU3Ek__BackingField_46() { return &___U3CdeviceRotationU3Ek__BackingField_46; }
	inline void set_U3CdeviceRotationU3Ek__BackingField_46(QuaternionControl_t7BEFE3CFC03CC5CE3F779DDCB5AA2C011F5C7A48 * value)
	{
		___U3CdeviceRotationU3Ek__BackingField_46 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CdeviceRotationU3Ek__BackingField_46), (void*)value);
	}

	inline static int32_t get_offset_of_U3CdeviceVelocityU3Ek__BackingField_47() { return static_cast<int32_t>(offsetof(HololensHand_t95681D1B89FD7FB988895CB96106B5BAE993D022, ___U3CdeviceVelocityU3Ek__BackingField_47)); }
	inline Vector3Control_t915A3DA3C3E367B6F5587853E96EAD5215D328AE * get_U3CdeviceVelocityU3Ek__BackingField_47() const { return ___U3CdeviceVelocityU3Ek__BackingField_47; }
	inline Vector3Control_t915A3DA3C3E367B6F5587853E96EAD5215D328AE ** get_address_of_U3CdeviceVelocityU3Ek__BackingField_47() { return &___U3CdeviceVelocityU3Ek__BackingField_47; }
	inline void set_U3CdeviceVelocityU3Ek__BackingField_47(Vector3Control_t915A3DA3C3E367B6F5587853E96EAD5215D328AE * value)
	{
		___U3CdeviceVelocityU3Ek__BackingField_47 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CdeviceVelocityU3Ek__BackingField_47), (void*)value);
	}

	inline static int32_t get_offset_of_U3CairTapU3Ek__BackingField_48() { return static_cast<int32_t>(offsetof(HololensHand_t95681D1B89FD7FB988895CB96106B5BAE993D022, ___U3CairTapU3Ek__BackingField_48)); }
	inline ButtonControl_t3918D56D674854AF9DFEB8CCF130919AAD9FCA68 * get_U3CairTapU3Ek__BackingField_48() const { return ___U3CairTapU3Ek__BackingField_48; }
	inline ButtonControl_t3918D56D674854AF9DFEB8CCF130919AAD9FCA68 ** get_address_of_U3CairTapU3Ek__BackingField_48() { return &___U3CairTapU3Ek__BackingField_48; }
	inline void set_U3CairTapU3Ek__BackingField_48(ButtonControl_t3918D56D674854AF9DFEB8CCF130919AAD9FCA68 * value)
	{
		___U3CairTapU3Ek__BackingField_48 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CairTapU3Ek__BackingField_48), (void*)value);
	}

	inline static int32_t get_offset_of_U3CsourceLossRiskU3Ek__BackingField_49() { return static_cast<int32_t>(offsetof(HololensHand_t95681D1B89FD7FB988895CB96106B5BAE993D022, ___U3CsourceLossRiskU3Ek__BackingField_49)); }
	inline AxisControl_tA997FF52442F7B08C7E13B7028393B4117248680 * get_U3CsourceLossRiskU3Ek__BackingField_49() const { return ___U3CsourceLossRiskU3Ek__BackingField_49; }
	inline AxisControl_tA997FF52442F7B08C7E13B7028393B4117248680 ** get_address_of_U3CsourceLossRiskU3Ek__BackingField_49() { return &___U3CsourceLossRiskU3Ek__BackingField_49; }
	inline void set_U3CsourceLossRiskU3Ek__BackingField_49(AxisControl_tA997FF52442F7B08C7E13B7028393B4117248680 * value)
	{
		___U3CsourceLossRiskU3Ek__BackingField_49 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CsourceLossRiskU3Ek__BackingField_49), (void*)value);
	}

	inline static int32_t get_offset_of_U3CsourceLossMitigationDirectionU3Ek__BackingField_50() { return static_cast<int32_t>(offsetof(HololensHand_t95681D1B89FD7FB988895CB96106B5BAE993D022, ___U3CsourceLossMitigationDirectionU3Ek__BackingField_50)); }
	inline Vector3Control_t915A3DA3C3E367B6F5587853E96EAD5215D328AE * get_U3CsourceLossMitigationDirectionU3Ek__BackingField_50() const { return ___U3CsourceLossMitigationDirectionU3Ek__BackingField_50; }
	inline Vector3Control_t915A3DA3C3E367B6F5587853E96EAD5215D328AE ** get_address_of_U3CsourceLossMitigationDirectionU3Ek__BackingField_50() { return &___U3CsourceLossMitigationDirectionU3Ek__BackingField_50; }
	inline void set_U3CsourceLossMitigationDirectionU3Ek__BackingField_50(Vector3Control_t915A3DA3C3E367B6F5587853E96EAD5215D328AE * value)
	{
		___U3CsourceLossMitigationDirectionU3Ek__BackingField_50 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CsourceLossMitigationDirectionU3Ek__BackingField_50), (void*)value);
	}
};


// UnityEngine.XR.WindowsMR.Input.WMRHMD
struct  WMRHMD_t383FD84EEB4BA62C04C8023721C130C5CC05AA0C  : public XRHMD_tF118D29FCF741F20C24D0AEA97DC5F78F92A92BE
{
public:
	// UnityEngine.InputSystem.Controls.ButtonControl UnityEngine.XR.WindowsMR.Input.WMRHMD::<userPresence>k__BackingField
	ButtonControl_t3918D56D674854AF9DFEB8CCF130919AAD9FCA68 * ___U3CuserPresenceU3Ek__BackingField_49;
	// UnityEngine.InputSystem.Controls.IntegerControl UnityEngine.XR.WindowsMR.Input.WMRHMD::<trackingState>k__BackingField
	IntegerControl_t68534DE7D89FBFC2952062997BB54680FE7DCCA4 * ___U3CtrackingStateU3Ek__BackingField_50;
	// UnityEngine.InputSystem.Controls.ButtonControl UnityEngine.XR.WindowsMR.Input.WMRHMD::<isTracked>k__BackingField
	ButtonControl_t3918D56D674854AF9DFEB8CCF130919AAD9FCA68 * ___U3CisTrackedU3Ek__BackingField_51;
	// UnityEngine.InputSystem.Controls.Vector3Control UnityEngine.XR.WindowsMR.Input.WMRHMD::<devicePosition>k__BackingField
	Vector3Control_t915A3DA3C3E367B6F5587853E96EAD5215D328AE * ___U3CdevicePositionU3Ek__BackingField_52;
	// UnityEngine.InputSystem.Controls.QuaternionControl UnityEngine.XR.WindowsMR.Input.WMRHMD::<deviceRotation>k__BackingField
	QuaternionControl_t7BEFE3CFC03CC5CE3F779DDCB5AA2C011F5C7A48 * ___U3CdeviceRotationU3Ek__BackingField_53;
	// UnityEngine.InputSystem.Controls.Vector3Control UnityEngine.XR.WindowsMR.Input.WMRHMD::<leftEyePosition>k__BackingField
	Vector3Control_t915A3DA3C3E367B6F5587853E96EAD5215D328AE * ___U3CleftEyePositionU3Ek__BackingField_54;
	// UnityEngine.InputSystem.Controls.QuaternionControl UnityEngine.XR.WindowsMR.Input.WMRHMD::<leftEyeRotation>k__BackingField
	QuaternionControl_t7BEFE3CFC03CC5CE3F779DDCB5AA2C011F5C7A48 * ___U3CleftEyeRotationU3Ek__BackingField_55;
	// UnityEngine.InputSystem.Controls.Vector3Control UnityEngine.XR.WindowsMR.Input.WMRHMD::<rightEyePosition>k__BackingField
	Vector3Control_t915A3DA3C3E367B6F5587853E96EAD5215D328AE * ___U3CrightEyePositionU3Ek__BackingField_56;
	// UnityEngine.InputSystem.Controls.QuaternionControl UnityEngine.XR.WindowsMR.Input.WMRHMD::<rightEyeRotation>k__BackingField
	QuaternionControl_t7BEFE3CFC03CC5CE3F779DDCB5AA2C011F5C7A48 * ___U3CrightEyeRotationU3Ek__BackingField_57;
	// UnityEngine.InputSystem.Controls.Vector3Control UnityEngine.XR.WindowsMR.Input.WMRHMD::<centerEyePosition>k__BackingField
	Vector3Control_t915A3DA3C3E367B6F5587853E96EAD5215D328AE * ___U3CcenterEyePositionU3Ek__BackingField_58;
	// UnityEngine.InputSystem.Controls.QuaternionControl UnityEngine.XR.WindowsMR.Input.WMRHMD::<centerEyeRotation>k__BackingField
	QuaternionControl_t7BEFE3CFC03CC5CE3F779DDCB5AA2C011F5C7A48 * ___U3CcenterEyeRotationU3Ek__BackingField_59;

public:
	inline static int32_t get_offset_of_U3CuserPresenceU3Ek__BackingField_49() { return static_cast<int32_t>(offsetof(WMRHMD_t383FD84EEB4BA62C04C8023721C130C5CC05AA0C, ___U3CuserPresenceU3Ek__BackingField_49)); }
	inline ButtonControl_t3918D56D674854AF9DFEB8CCF130919AAD9FCA68 * get_U3CuserPresenceU3Ek__BackingField_49() const { return ___U3CuserPresenceU3Ek__BackingField_49; }
	inline ButtonControl_t3918D56D674854AF9DFEB8CCF130919AAD9FCA68 ** get_address_of_U3CuserPresenceU3Ek__BackingField_49() { return &___U3CuserPresenceU3Ek__BackingField_49; }
	inline void set_U3CuserPresenceU3Ek__BackingField_49(ButtonControl_t3918D56D674854AF9DFEB8CCF130919AAD9FCA68 * value)
	{
		___U3CuserPresenceU3Ek__BackingField_49 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CuserPresenceU3Ek__BackingField_49), (void*)value);
	}

	inline static int32_t get_offset_of_U3CtrackingStateU3Ek__BackingField_50() { return static_cast<int32_t>(offsetof(WMRHMD_t383FD84EEB4BA62C04C8023721C130C5CC05AA0C, ___U3CtrackingStateU3Ek__BackingField_50)); }
	inline IntegerControl_t68534DE7D89FBFC2952062997BB54680FE7DCCA4 * get_U3CtrackingStateU3Ek__BackingField_50() const { return ___U3CtrackingStateU3Ek__BackingField_50; }
	inline IntegerControl_t68534DE7D89FBFC2952062997BB54680FE7DCCA4 ** get_address_of_U3CtrackingStateU3Ek__BackingField_50() { return &___U3CtrackingStateU3Ek__BackingField_50; }
	inline void set_U3CtrackingStateU3Ek__BackingField_50(IntegerControl_t68534DE7D89FBFC2952062997BB54680FE7DCCA4 * value)
	{
		___U3CtrackingStateU3Ek__BackingField_50 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CtrackingStateU3Ek__BackingField_50), (void*)value);
	}

	inline static int32_t get_offset_of_U3CisTrackedU3Ek__BackingField_51() { return static_cast<int32_t>(offsetof(WMRHMD_t383FD84EEB4BA62C04C8023721C130C5CC05AA0C, ___U3CisTrackedU3Ek__BackingField_51)); }
	inline ButtonControl_t3918D56D674854AF9DFEB8CCF130919AAD9FCA68 * get_U3CisTrackedU3Ek__BackingField_51() const { return ___U3CisTrackedU3Ek__BackingField_51; }
	inline ButtonControl_t3918D56D674854AF9DFEB8CCF130919AAD9FCA68 ** get_address_of_U3CisTrackedU3Ek__BackingField_51() { return &___U3CisTrackedU3Ek__BackingField_51; }
	inline void set_U3CisTrackedU3Ek__BackingField_51(ButtonControl_t3918D56D674854AF9DFEB8CCF130919AAD9FCA68 * value)
	{
		___U3CisTrackedU3Ek__BackingField_51 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CisTrackedU3Ek__BackingField_51), (void*)value);
	}

	inline static int32_t get_offset_of_U3CdevicePositionU3Ek__BackingField_52() { return static_cast<int32_t>(offsetof(WMRHMD_t383FD84EEB4BA62C04C8023721C130C5CC05AA0C, ___U3CdevicePositionU3Ek__BackingField_52)); }
	inline Vector3Control_t915A3DA3C3E367B6F5587853E96EAD5215D328AE * get_U3CdevicePositionU3Ek__BackingField_52() const { return ___U3CdevicePositionU3Ek__BackingField_52; }
	inline Vector3Control_t915A3DA3C3E367B6F5587853E96EAD5215D328AE ** get_address_of_U3CdevicePositionU3Ek__BackingField_52() { return &___U3CdevicePositionU3Ek__BackingField_52; }
	inline void set_U3CdevicePositionU3Ek__BackingField_52(Vector3Control_t915A3DA3C3E367B6F5587853E96EAD5215D328AE * value)
	{
		___U3CdevicePositionU3Ek__BackingField_52 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CdevicePositionU3Ek__BackingField_52), (void*)value);
	}

	inline static int32_t get_offset_of_U3CdeviceRotationU3Ek__BackingField_53() { return static_cast<int32_t>(offsetof(WMRHMD_t383FD84EEB4BA62C04C8023721C130C5CC05AA0C, ___U3CdeviceRotationU3Ek__BackingField_53)); }
	inline QuaternionControl_t7BEFE3CFC03CC5CE3F779DDCB5AA2C011F5C7A48 * get_U3CdeviceRotationU3Ek__BackingField_53() const { return ___U3CdeviceRotationU3Ek__BackingField_53; }
	inline QuaternionControl_t7BEFE3CFC03CC5CE3F779DDCB5AA2C011F5C7A48 ** get_address_of_U3CdeviceRotationU3Ek__BackingField_53() { return &___U3CdeviceRotationU3Ek__BackingField_53; }
	inline void set_U3CdeviceRotationU3Ek__BackingField_53(QuaternionControl_t7BEFE3CFC03CC5CE3F779DDCB5AA2C011F5C7A48 * value)
	{
		___U3CdeviceRotationU3Ek__BackingField_53 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CdeviceRotationU3Ek__BackingField_53), (void*)value);
	}

	inline static int32_t get_offset_of_U3CleftEyePositionU3Ek__BackingField_54() { return static_cast<int32_t>(offsetof(WMRHMD_t383FD84EEB4BA62C04C8023721C130C5CC05AA0C, ___U3CleftEyePositionU3Ek__BackingField_54)); }
	inline Vector3Control_t915A3DA3C3E367B6F5587853E96EAD5215D328AE * get_U3CleftEyePositionU3Ek__BackingField_54() const { return ___U3CleftEyePositionU3Ek__BackingField_54; }
	inline Vector3Control_t915A3DA3C3E367B6F5587853E96EAD5215D328AE ** get_address_of_U3CleftEyePositionU3Ek__BackingField_54() { return &___U3CleftEyePositionU3Ek__BackingField_54; }
	inline void set_U3CleftEyePositionU3Ek__BackingField_54(Vector3Control_t915A3DA3C3E367B6F5587853E96EAD5215D328AE * value)
	{
		___U3CleftEyePositionU3Ek__BackingField_54 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CleftEyePositionU3Ek__BackingField_54), (void*)value);
	}

	inline static int32_t get_offset_of_U3CleftEyeRotationU3Ek__BackingField_55() { return static_cast<int32_t>(offsetof(WMRHMD_t383FD84EEB4BA62C04C8023721C130C5CC05AA0C, ___U3CleftEyeRotationU3Ek__BackingField_55)); }
	inline QuaternionControl_t7BEFE3CFC03CC5CE3F779DDCB5AA2C011F5C7A48 * get_U3CleftEyeRotationU3Ek__BackingField_55() const { return ___U3CleftEyeRotationU3Ek__BackingField_55; }
	inline QuaternionControl_t7BEFE3CFC03CC5CE3F779DDCB5AA2C011F5C7A48 ** get_address_of_U3CleftEyeRotationU3Ek__BackingField_55() { return &___U3CleftEyeRotationU3Ek__BackingField_55; }
	inline void set_U3CleftEyeRotationU3Ek__BackingField_55(QuaternionControl_t7BEFE3CFC03CC5CE3F779DDCB5AA2C011F5C7A48 * value)
	{
		___U3CleftEyeRotationU3Ek__BackingField_55 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CleftEyeRotationU3Ek__BackingField_55), (void*)value);
	}

	inline static int32_t get_offset_of_U3CrightEyePositionU3Ek__BackingField_56() { return static_cast<int32_t>(offsetof(WMRHMD_t383FD84EEB4BA62C04C8023721C130C5CC05AA0C, ___U3CrightEyePositionU3Ek__BackingField_56)); }
	inline Vector3Control_t915A3DA3C3E367B6F5587853E96EAD5215D328AE * get_U3CrightEyePositionU3Ek__BackingField_56() const { return ___U3CrightEyePositionU3Ek__BackingField_56; }
	inline Vector3Control_t915A3DA3C3E367B6F5587853E96EAD5215D328AE ** get_address_of_U3CrightEyePositionU3Ek__BackingField_56() { return &___U3CrightEyePositionU3Ek__BackingField_56; }
	inline void set_U3CrightEyePositionU3Ek__BackingField_56(Vector3Control_t915A3DA3C3E367B6F5587853E96EAD5215D328AE * value)
	{
		___U3CrightEyePositionU3Ek__BackingField_56 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CrightEyePositionU3Ek__BackingField_56), (void*)value);
	}

	inline static int32_t get_offset_of_U3CrightEyeRotationU3Ek__BackingField_57() { return static_cast<int32_t>(offsetof(WMRHMD_t383FD84EEB4BA62C04C8023721C130C5CC05AA0C, ___U3CrightEyeRotationU3Ek__BackingField_57)); }
	inline QuaternionControl_t7BEFE3CFC03CC5CE3F779DDCB5AA2C011F5C7A48 * get_U3CrightEyeRotationU3Ek__BackingField_57() const { return ___U3CrightEyeRotationU3Ek__BackingField_57; }
	inline QuaternionControl_t7BEFE3CFC03CC5CE3F779DDCB5AA2C011F5C7A48 ** get_address_of_U3CrightEyeRotationU3Ek__BackingField_57() { return &___U3CrightEyeRotationU3Ek__BackingField_57; }
	inline void set_U3CrightEyeRotationU3Ek__BackingField_57(QuaternionControl_t7BEFE3CFC03CC5CE3F779DDCB5AA2C011F5C7A48 * value)
	{
		___U3CrightEyeRotationU3Ek__BackingField_57 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CrightEyeRotationU3Ek__BackingField_57), (void*)value);
	}

	inline static int32_t get_offset_of_U3CcenterEyePositionU3Ek__BackingField_58() { return static_cast<int32_t>(offsetof(WMRHMD_t383FD84EEB4BA62C04C8023721C130C5CC05AA0C, ___U3CcenterEyePositionU3Ek__BackingField_58)); }
	inline Vector3Control_t915A3DA3C3E367B6F5587853E96EAD5215D328AE * get_U3CcenterEyePositionU3Ek__BackingField_58() const { return ___U3CcenterEyePositionU3Ek__BackingField_58; }
	inline Vector3Control_t915A3DA3C3E367B6F5587853E96EAD5215D328AE ** get_address_of_U3CcenterEyePositionU3Ek__BackingField_58() { return &___U3CcenterEyePositionU3Ek__BackingField_58; }
	inline void set_U3CcenterEyePositionU3Ek__BackingField_58(Vector3Control_t915A3DA3C3E367B6F5587853E96EAD5215D328AE * value)
	{
		___U3CcenterEyePositionU3Ek__BackingField_58 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CcenterEyePositionU3Ek__BackingField_58), (void*)value);
	}

	inline static int32_t get_offset_of_U3CcenterEyeRotationU3Ek__BackingField_59() { return static_cast<int32_t>(offsetof(WMRHMD_t383FD84EEB4BA62C04C8023721C130C5CC05AA0C, ___U3CcenterEyeRotationU3Ek__BackingField_59)); }
	inline QuaternionControl_t7BEFE3CFC03CC5CE3F779DDCB5AA2C011F5C7A48 * get_U3CcenterEyeRotationU3Ek__BackingField_59() const { return ___U3CcenterEyeRotationU3Ek__BackingField_59; }
	inline QuaternionControl_t7BEFE3CFC03CC5CE3F779DDCB5AA2C011F5C7A48 ** get_address_of_U3CcenterEyeRotationU3Ek__BackingField_59() { return &___U3CcenterEyeRotationU3Ek__BackingField_59; }
	inline void set_U3CcenterEyeRotationU3Ek__BackingField_59(QuaternionControl_t7BEFE3CFC03CC5CE3F779DDCB5AA2C011F5C7A48 * value)
	{
		___U3CcenterEyeRotationU3Ek__BackingField_59 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CcenterEyeRotationU3Ek__BackingField_59), (void*)value);
	}
};


// UnityEngine.InputSystem.XR.XRControllerWithRumble
struct  XRControllerWithRumble_tAECE062A3E616AC023DA89DA07003DB0D43EDEC2  : public XRController_tB2474121816D6124DF5CF8739FED669945C52778
{
public:

public:
};


// UnityEngine.XR.WindowsMR.Input.WMRSpatialController
struct  WMRSpatialController_t41CF610FC58697A3F5D3070E0D2A5557915901C6  : public XRControllerWithRumble_tAECE062A3E616AC023DA89DA07003DB0D43EDEC2
{
public:
	// UnityEngine.InputSystem.Controls.Vector2Control UnityEngine.XR.WindowsMR.Input.WMRSpatialController::<joystick>k__BackingField
	Vector2Control_t271CA458D56BCA875642853132733D774B009A96 * ___U3CjoystickU3Ek__BackingField_43;
	// UnityEngine.InputSystem.Controls.Vector2Control UnityEngine.XR.WindowsMR.Input.WMRSpatialController::<touchpad>k__BackingField
	Vector2Control_t271CA458D56BCA875642853132733D774B009A96 * ___U3CtouchpadU3Ek__BackingField_44;
	// UnityEngine.InputSystem.Controls.AxisControl UnityEngine.XR.WindowsMR.Input.WMRSpatialController::<grip>k__BackingField
	AxisControl_tA997FF52442F7B08C7E13B7028393B4117248680 * ___U3CgripU3Ek__BackingField_45;
	// UnityEngine.InputSystem.Controls.ButtonControl UnityEngine.XR.WindowsMR.Input.WMRSpatialController::<gripPressed>k__BackingField
	ButtonControl_t3918D56D674854AF9DFEB8CCF130919AAD9FCA68 * ___U3CgripPressedU3Ek__BackingField_46;
	// UnityEngine.InputSystem.Controls.ButtonControl UnityEngine.XR.WindowsMR.Input.WMRSpatialController::<menu>k__BackingField
	ButtonControl_t3918D56D674854AF9DFEB8CCF130919AAD9FCA68 * ___U3CmenuU3Ek__BackingField_47;
	// UnityEngine.InputSystem.Controls.AxisControl UnityEngine.XR.WindowsMR.Input.WMRSpatialController::<trigger>k__BackingField
	AxisControl_tA997FF52442F7B08C7E13B7028393B4117248680 * ___U3CtriggerU3Ek__BackingField_48;
	// UnityEngine.InputSystem.Controls.ButtonControl UnityEngine.XR.WindowsMR.Input.WMRSpatialController::<triggerPressed>k__BackingField
	ButtonControl_t3918D56D674854AF9DFEB8CCF130919AAD9FCA68 * ___U3CtriggerPressedU3Ek__BackingField_49;
	// UnityEngine.InputSystem.Controls.ButtonControl UnityEngine.XR.WindowsMR.Input.WMRSpatialController::<joystickClicked>k__BackingField
	ButtonControl_t3918D56D674854AF9DFEB8CCF130919AAD9FCA68 * ___U3CjoystickClickedU3Ek__BackingField_50;
	// UnityEngine.InputSystem.Controls.ButtonControl UnityEngine.XR.WindowsMR.Input.WMRSpatialController::<touchpadClicked>k__BackingField
	ButtonControl_t3918D56D674854AF9DFEB8CCF130919AAD9FCA68 * ___U3CtouchpadClickedU3Ek__BackingField_51;
	// UnityEngine.InputSystem.Controls.ButtonControl UnityEngine.XR.WindowsMR.Input.WMRSpatialController::<touchpadTouched>k__BackingField
	ButtonControl_t3918D56D674854AF9DFEB8CCF130919AAD9FCA68 * ___U3CtouchpadTouchedU3Ek__BackingField_52;
	// UnityEngine.InputSystem.Controls.IntegerControl UnityEngine.XR.WindowsMR.Input.WMRSpatialController::<trackingState>k__BackingField
	IntegerControl_t68534DE7D89FBFC2952062997BB54680FE7DCCA4 * ___U3CtrackingStateU3Ek__BackingField_53;
	// UnityEngine.InputSystem.Controls.ButtonControl UnityEngine.XR.WindowsMR.Input.WMRSpatialController::<isTracked>k__BackingField
	ButtonControl_t3918D56D674854AF9DFEB8CCF130919AAD9FCA68 * ___U3CisTrackedU3Ek__BackingField_54;
	// UnityEngine.InputSystem.Controls.Vector3Control UnityEngine.XR.WindowsMR.Input.WMRSpatialController::<devicePosition>k__BackingField
	Vector3Control_t915A3DA3C3E367B6F5587853E96EAD5215D328AE * ___U3CdevicePositionU3Ek__BackingField_55;
	// UnityEngine.InputSystem.Controls.QuaternionControl UnityEngine.XR.WindowsMR.Input.WMRSpatialController::<deviceRotation>k__BackingField
	QuaternionControl_t7BEFE3CFC03CC5CE3F779DDCB5AA2C011F5C7A48 * ___U3CdeviceRotationU3Ek__BackingField_56;
	// UnityEngine.InputSystem.Controls.Vector3Control UnityEngine.XR.WindowsMR.Input.WMRSpatialController::<deviceVelocity>k__BackingField
	Vector3Control_t915A3DA3C3E367B6F5587853E96EAD5215D328AE * ___U3CdeviceVelocityU3Ek__BackingField_57;
	// UnityEngine.InputSystem.Controls.Vector3Control UnityEngine.XR.WindowsMR.Input.WMRSpatialController::<deviceAngularVelocity>k__BackingField
	Vector3Control_t915A3DA3C3E367B6F5587853E96EAD5215D328AE * ___U3CdeviceAngularVelocityU3Ek__BackingField_58;
	// UnityEngine.InputSystem.Controls.AxisControl UnityEngine.XR.WindowsMR.Input.WMRSpatialController::<batteryLevel>k__BackingField
	AxisControl_tA997FF52442F7B08C7E13B7028393B4117248680 * ___U3CbatteryLevelU3Ek__BackingField_59;
	// UnityEngine.InputSystem.Controls.AxisControl UnityEngine.XR.WindowsMR.Input.WMRSpatialController::<sourceLossRisk>k__BackingField
	AxisControl_tA997FF52442F7B08C7E13B7028393B4117248680 * ___U3CsourceLossRiskU3Ek__BackingField_60;
	// UnityEngine.InputSystem.Controls.Vector3Control UnityEngine.XR.WindowsMR.Input.WMRSpatialController::<sourceLossMitigationDirection>k__BackingField
	Vector3Control_t915A3DA3C3E367B6F5587853E96EAD5215D328AE * ___U3CsourceLossMitigationDirectionU3Ek__BackingField_61;
	// UnityEngine.InputSystem.Controls.Vector3Control UnityEngine.XR.WindowsMR.Input.WMRSpatialController::<pointerPosition>k__BackingField
	Vector3Control_t915A3DA3C3E367B6F5587853E96EAD5215D328AE * ___U3CpointerPositionU3Ek__BackingField_62;
	// UnityEngine.InputSystem.Controls.QuaternionControl UnityEngine.XR.WindowsMR.Input.WMRSpatialController::<pointerRotation>k__BackingField
	QuaternionControl_t7BEFE3CFC03CC5CE3F779DDCB5AA2C011F5C7A48 * ___U3CpointerRotationU3Ek__BackingField_63;

public:
	inline static int32_t get_offset_of_U3CjoystickU3Ek__BackingField_43() { return static_cast<int32_t>(offsetof(WMRSpatialController_t41CF610FC58697A3F5D3070E0D2A5557915901C6, ___U3CjoystickU3Ek__BackingField_43)); }
	inline Vector2Control_t271CA458D56BCA875642853132733D774B009A96 * get_U3CjoystickU3Ek__BackingField_43() const { return ___U3CjoystickU3Ek__BackingField_43; }
	inline Vector2Control_t271CA458D56BCA875642853132733D774B009A96 ** get_address_of_U3CjoystickU3Ek__BackingField_43() { return &___U3CjoystickU3Ek__BackingField_43; }
	inline void set_U3CjoystickU3Ek__BackingField_43(Vector2Control_t271CA458D56BCA875642853132733D774B009A96 * value)
	{
		___U3CjoystickU3Ek__BackingField_43 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CjoystickU3Ek__BackingField_43), (void*)value);
	}

	inline static int32_t get_offset_of_U3CtouchpadU3Ek__BackingField_44() { return static_cast<int32_t>(offsetof(WMRSpatialController_t41CF610FC58697A3F5D3070E0D2A5557915901C6, ___U3CtouchpadU3Ek__BackingField_44)); }
	inline Vector2Control_t271CA458D56BCA875642853132733D774B009A96 * get_U3CtouchpadU3Ek__BackingField_44() const { return ___U3CtouchpadU3Ek__BackingField_44; }
	inline Vector2Control_t271CA458D56BCA875642853132733D774B009A96 ** get_address_of_U3CtouchpadU3Ek__BackingField_44() { return &___U3CtouchpadU3Ek__BackingField_44; }
	inline void set_U3CtouchpadU3Ek__BackingField_44(Vector2Control_t271CA458D56BCA875642853132733D774B009A96 * value)
	{
		___U3CtouchpadU3Ek__BackingField_44 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CtouchpadU3Ek__BackingField_44), (void*)value);
	}

	inline static int32_t get_offset_of_U3CgripU3Ek__BackingField_45() { return static_cast<int32_t>(offsetof(WMRSpatialController_t41CF610FC58697A3F5D3070E0D2A5557915901C6, ___U3CgripU3Ek__BackingField_45)); }
	inline AxisControl_tA997FF52442F7B08C7E13B7028393B4117248680 * get_U3CgripU3Ek__BackingField_45() const { return ___U3CgripU3Ek__BackingField_45; }
	inline AxisControl_tA997FF52442F7B08C7E13B7028393B4117248680 ** get_address_of_U3CgripU3Ek__BackingField_45() { return &___U3CgripU3Ek__BackingField_45; }
	inline void set_U3CgripU3Ek__BackingField_45(AxisControl_tA997FF52442F7B08C7E13B7028393B4117248680 * value)
	{
		___U3CgripU3Ek__BackingField_45 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CgripU3Ek__BackingField_45), (void*)value);
	}

	inline static int32_t get_offset_of_U3CgripPressedU3Ek__BackingField_46() { return static_cast<int32_t>(offsetof(WMRSpatialController_t41CF610FC58697A3F5D3070E0D2A5557915901C6, ___U3CgripPressedU3Ek__BackingField_46)); }
	inline ButtonControl_t3918D56D674854AF9DFEB8CCF130919AAD9FCA68 * get_U3CgripPressedU3Ek__BackingField_46() const { return ___U3CgripPressedU3Ek__BackingField_46; }
	inline ButtonControl_t3918D56D674854AF9DFEB8CCF130919AAD9FCA68 ** get_address_of_U3CgripPressedU3Ek__BackingField_46() { return &___U3CgripPressedU3Ek__BackingField_46; }
	inline void set_U3CgripPressedU3Ek__BackingField_46(ButtonControl_t3918D56D674854AF9DFEB8CCF130919AAD9FCA68 * value)
	{
		___U3CgripPressedU3Ek__BackingField_46 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CgripPressedU3Ek__BackingField_46), (void*)value);
	}

	inline static int32_t get_offset_of_U3CmenuU3Ek__BackingField_47() { return static_cast<int32_t>(offsetof(WMRSpatialController_t41CF610FC58697A3F5D3070E0D2A5557915901C6, ___U3CmenuU3Ek__BackingField_47)); }
	inline ButtonControl_t3918D56D674854AF9DFEB8CCF130919AAD9FCA68 * get_U3CmenuU3Ek__BackingField_47() const { return ___U3CmenuU3Ek__BackingField_47; }
	inline ButtonControl_t3918D56D674854AF9DFEB8CCF130919AAD9FCA68 ** get_address_of_U3CmenuU3Ek__BackingField_47() { return &___U3CmenuU3Ek__BackingField_47; }
	inline void set_U3CmenuU3Ek__BackingField_47(ButtonControl_t3918D56D674854AF9DFEB8CCF130919AAD9FCA68 * value)
	{
		___U3CmenuU3Ek__BackingField_47 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CmenuU3Ek__BackingField_47), (void*)value);
	}

	inline static int32_t get_offset_of_U3CtriggerU3Ek__BackingField_48() { return static_cast<int32_t>(offsetof(WMRSpatialController_t41CF610FC58697A3F5D3070E0D2A5557915901C6, ___U3CtriggerU3Ek__BackingField_48)); }
	inline AxisControl_tA997FF52442F7B08C7E13B7028393B4117248680 * get_U3CtriggerU3Ek__BackingField_48() const { return ___U3CtriggerU3Ek__BackingField_48; }
	inline AxisControl_tA997FF52442F7B08C7E13B7028393B4117248680 ** get_address_of_U3CtriggerU3Ek__BackingField_48() { return &___U3CtriggerU3Ek__BackingField_48; }
	inline void set_U3CtriggerU3Ek__BackingField_48(AxisControl_tA997FF52442F7B08C7E13B7028393B4117248680 * value)
	{
		___U3CtriggerU3Ek__BackingField_48 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CtriggerU3Ek__BackingField_48), (void*)value);
	}

	inline static int32_t get_offset_of_U3CtriggerPressedU3Ek__BackingField_49() { return static_cast<int32_t>(offsetof(WMRSpatialController_t41CF610FC58697A3F5D3070E0D2A5557915901C6, ___U3CtriggerPressedU3Ek__BackingField_49)); }
	inline ButtonControl_t3918D56D674854AF9DFEB8CCF130919AAD9FCA68 * get_U3CtriggerPressedU3Ek__BackingField_49() const { return ___U3CtriggerPressedU3Ek__BackingField_49; }
	inline ButtonControl_t3918D56D674854AF9DFEB8CCF130919AAD9FCA68 ** get_address_of_U3CtriggerPressedU3Ek__BackingField_49() { return &___U3CtriggerPressedU3Ek__BackingField_49; }
	inline void set_U3CtriggerPressedU3Ek__BackingField_49(ButtonControl_t3918D56D674854AF9DFEB8CCF130919AAD9FCA68 * value)
	{
		___U3CtriggerPressedU3Ek__BackingField_49 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CtriggerPressedU3Ek__BackingField_49), (void*)value);
	}

	inline static int32_t get_offset_of_U3CjoystickClickedU3Ek__BackingField_50() { return static_cast<int32_t>(offsetof(WMRSpatialController_t41CF610FC58697A3F5D3070E0D2A5557915901C6, ___U3CjoystickClickedU3Ek__BackingField_50)); }
	inline ButtonControl_t3918D56D674854AF9DFEB8CCF130919AAD9FCA68 * get_U3CjoystickClickedU3Ek__BackingField_50() const { return ___U3CjoystickClickedU3Ek__BackingField_50; }
	inline ButtonControl_t3918D56D674854AF9DFEB8CCF130919AAD9FCA68 ** get_address_of_U3CjoystickClickedU3Ek__BackingField_50() { return &___U3CjoystickClickedU3Ek__BackingField_50; }
	inline void set_U3CjoystickClickedU3Ek__BackingField_50(ButtonControl_t3918D56D674854AF9DFEB8CCF130919AAD9FCA68 * value)
	{
		___U3CjoystickClickedU3Ek__BackingField_50 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CjoystickClickedU3Ek__BackingField_50), (void*)value);
	}

	inline static int32_t get_offset_of_U3CtouchpadClickedU3Ek__BackingField_51() { return static_cast<int32_t>(offsetof(WMRSpatialController_t41CF610FC58697A3F5D3070E0D2A5557915901C6, ___U3CtouchpadClickedU3Ek__BackingField_51)); }
	inline ButtonControl_t3918D56D674854AF9DFEB8CCF130919AAD9FCA68 * get_U3CtouchpadClickedU3Ek__BackingField_51() const { return ___U3CtouchpadClickedU3Ek__BackingField_51; }
	inline ButtonControl_t3918D56D674854AF9DFEB8CCF130919AAD9FCA68 ** get_address_of_U3CtouchpadClickedU3Ek__BackingField_51() { return &___U3CtouchpadClickedU3Ek__BackingField_51; }
	inline void set_U3CtouchpadClickedU3Ek__BackingField_51(ButtonControl_t3918D56D674854AF9DFEB8CCF130919AAD9FCA68 * value)
	{
		___U3CtouchpadClickedU3Ek__BackingField_51 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CtouchpadClickedU3Ek__BackingField_51), (void*)value);
	}

	inline static int32_t get_offset_of_U3CtouchpadTouchedU3Ek__BackingField_52() { return static_cast<int32_t>(offsetof(WMRSpatialController_t41CF610FC58697A3F5D3070E0D2A5557915901C6, ___U3CtouchpadTouchedU3Ek__BackingField_52)); }
	inline ButtonControl_t3918D56D674854AF9DFEB8CCF130919AAD9FCA68 * get_U3CtouchpadTouchedU3Ek__BackingField_52() const { return ___U3CtouchpadTouchedU3Ek__BackingField_52; }
	inline ButtonControl_t3918D56D674854AF9DFEB8CCF130919AAD9FCA68 ** get_address_of_U3CtouchpadTouchedU3Ek__BackingField_52() { return &___U3CtouchpadTouchedU3Ek__BackingField_52; }
	inline void set_U3CtouchpadTouchedU3Ek__BackingField_52(ButtonControl_t3918D56D674854AF9DFEB8CCF130919AAD9FCA68 * value)
	{
		___U3CtouchpadTouchedU3Ek__BackingField_52 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CtouchpadTouchedU3Ek__BackingField_52), (void*)value);
	}

	inline static int32_t get_offset_of_U3CtrackingStateU3Ek__BackingField_53() { return static_cast<int32_t>(offsetof(WMRSpatialController_t41CF610FC58697A3F5D3070E0D2A5557915901C6, ___U3CtrackingStateU3Ek__BackingField_53)); }
	inline IntegerControl_t68534DE7D89FBFC2952062997BB54680FE7DCCA4 * get_U3CtrackingStateU3Ek__BackingField_53() const { return ___U3CtrackingStateU3Ek__BackingField_53; }
	inline IntegerControl_t68534DE7D89FBFC2952062997BB54680FE7DCCA4 ** get_address_of_U3CtrackingStateU3Ek__BackingField_53() { return &___U3CtrackingStateU3Ek__BackingField_53; }
	inline void set_U3CtrackingStateU3Ek__BackingField_53(IntegerControl_t68534DE7D89FBFC2952062997BB54680FE7DCCA4 * value)
	{
		___U3CtrackingStateU3Ek__BackingField_53 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CtrackingStateU3Ek__BackingField_53), (void*)value);
	}

	inline static int32_t get_offset_of_U3CisTrackedU3Ek__BackingField_54() { return static_cast<int32_t>(offsetof(WMRSpatialController_t41CF610FC58697A3F5D3070E0D2A5557915901C6, ___U3CisTrackedU3Ek__BackingField_54)); }
	inline ButtonControl_t3918D56D674854AF9DFEB8CCF130919AAD9FCA68 * get_U3CisTrackedU3Ek__BackingField_54() const { return ___U3CisTrackedU3Ek__BackingField_54; }
	inline ButtonControl_t3918D56D674854AF9DFEB8CCF130919AAD9FCA68 ** get_address_of_U3CisTrackedU3Ek__BackingField_54() { return &___U3CisTrackedU3Ek__BackingField_54; }
	inline void set_U3CisTrackedU3Ek__BackingField_54(ButtonControl_t3918D56D674854AF9DFEB8CCF130919AAD9FCA68 * value)
	{
		___U3CisTrackedU3Ek__BackingField_54 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CisTrackedU3Ek__BackingField_54), (void*)value);
	}

	inline static int32_t get_offset_of_U3CdevicePositionU3Ek__BackingField_55() { return static_cast<int32_t>(offsetof(WMRSpatialController_t41CF610FC58697A3F5D3070E0D2A5557915901C6, ___U3CdevicePositionU3Ek__BackingField_55)); }
	inline Vector3Control_t915A3DA3C3E367B6F5587853E96EAD5215D328AE * get_U3CdevicePositionU3Ek__BackingField_55() const { return ___U3CdevicePositionU3Ek__BackingField_55; }
	inline Vector3Control_t915A3DA3C3E367B6F5587853E96EAD5215D328AE ** get_address_of_U3CdevicePositionU3Ek__BackingField_55() { return &___U3CdevicePositionU3Ek__BackingField_55; }
	inline void set_U3CdevicePositionU3Ek__BackingField_55(Vector3Control_t915A3DA3C3E367B6F5587853E96EAD5215D328AE * value)
	{
		___U3CdevicePositionU3Ek__BackingField_55 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CdevicePositionU3Ek__BackingField_55), (void*)value);
	}

	inline static int32_t get_offset_of_U3CdeviceRotationU3Ek__BackingField_56() { return static_cast<int32_t>(offsetof(WMRSpatialController_t41CF610FC58697A3F5D3070E0D2A5557915901C6, ___U3CdeviceRotationU3Ek__BackingField_56)); }
	inline QuaternionControl_t7BEFE3CFC03CC5CE3F779DDCB5AA2C011F5C7A48 * get_U3CdeviceRotationU3Ek__BackingField_56() const { return ___U3CdeviceRotationU3Ek__BackingField_56; }
	inline QuaternionControl_t7BEFE3CFC03CC5CE3F779DDCB5AA2C011F5C7A48 ** get_address_of_U3CdeviceRotationU3Ek__BackingField_56() { return &___U3CdeviceRotationU3Ek__BackingField_56; }
	inline void set_U3CdeviceRotationU3Ek__BackingField_56(QuaternionControl_t7BEFE3CFC03CC5CE3F779DDCB5AA2C011F5C7A48 * value)
	{
		___U3CdeviceRotationU3Ek__BackingField_56 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CdeviceRotationU3Ek__BackingField_56), (void*)value);
	}

	inline static int32_t get_offset_of_U3CdeviceVelocityU3Ek__BackingField_57() { return static_cast<int32_t>(offsetof(WMRSpatialController_t41CF610FC58697A3F5D3070E0D2A5557915901C6, ___U3CdeviceVelocityU3Ek__BackingField_57)); }
	inline Vector3Control_t915A3DA3C3E367B6F5587853E96EAD5215D328AE * get_U3CdeviceVelocityU3Ek__BackingField_57() const { return ___U3CdeviceVelocityU3Ek__BackingField_57; }
	inline Vector3Control_t915A3DA3C3E367B6F5587853E96EAD5215D328AE ** get_address_of_U3CdeviceVelocityU3Ek__BackingField_57() { return &___U3CdeviceVelocityU3Ek__BackingField_57; }
	inline void set_U3CdeviceVelocityU3Ek__BackingField_57(Vector3Control_t915A3DA3C3E367B6F5587853E96EAD5215D328AE * value)
	{
		___U3CdeviceVelocityU3Ek__BackingField_57 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CdeviceVelocityU3Ek__BackingField_57), (void*)value);
	}

	inline static int32_t get_offset_of_U3CdeviceAngularVelocityU3Ek__BackingField_58() { return static_cast<int32_t>(offsetof(WMRSpatialController_t41CF610FC58697A3F5D3070E0D2A5557915901C6, ___U3CdeviceAngularVelocityU3Ek__BackingField_58)); }
	inline Vector3Control_t915A3DA3C3E367B6F5587853E96EAD5215D328AE * get_U3CdeviceAngularVelocityU3Ek__BackingField_58() const { return ___U3CdeviceAngularVelocityU3Ek__BackingField_58; }
	inline Vector3Control_t915A3DA3C3E367B6F5587853E96EAD5215D328AE ** get_address_of_U3CdeviceAngularVelocityU3Ek__BackingField_58() { return &___U3CdeviceAngularVelocityU3Ek__BackingField_58; }
	inline void set_U3CdeviceAngularVelocityU3Ek__BackingField_58(Vector3Control_t915A3DA3C3E367B6F5587853E96EAD5215D328AE * value)
	{
		___U3CdeviceAngularVelocityU3Ek__BackingField_58 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CdeviceAngularVelocityU3Ek__BackingField_58), (void*)value);
	}

	inline static int32_t get_offset_of_U3CbatteryLevelU3Ek__BackingField_59() { return static_cast<int32_t>(offsetof(WMRSpatialController_t41CF610FC58697A3F5D3070E0D2A5557915901C6, ___U3CbatteryLevelU3Ek__BackingField_59)); }
	inline AxisControl_tA997FF52442F7B08C7E13B7028393B4117248680 * get_U3CbatteryLevelU3Ek__BackingField_59() const { return ___U3CbatteryLevelU3Ek__BackingField_59; }
	inline AxisControl_tA997FF52442F7B08C7E13B7028393B4117248680 ** get_address_of_U3CbatteryLevelU3Ek__BackingField_59() { return &___U3CbatteryLevelU3Ek__BackingField_59; }
	inline void set_U3CbatteryLevelU3Ek__BackingField_59(AxisControl_tA997FF52442F7B08C7E13B7028393B4117248680 * value)
	{
		___U3CbatteryLevelU3Ek__BackingField_59 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CbatteryLevelU3Ek__BackingField_59), (void*)value);
	}

	inline static int32_t get_offset_of_U3CsourceLossRiskU3Ek__BackingField_60() { return static_cast<int32_t>(offsetof(WMRSpatialController_t41CF610FC58697A3F5D3070E0D2A5557915901C6, ___U3CsourceLossRiskU3Ek__BackingField_60)); }
	inline AxisControl_tA997FF52442F7B08C7E13B7028393B4117248680 * get_U3CsourceLossRiskU3Ek__BackingField_60() const { return ___U3CsourceLossRiskU3Ek__BackingField_60; }
	inline AxisControl_tA997FF52442F7B08C7E13B7028393B4117248680 ** get_address_of_U3CsourceLossRiskU3Ek__BackingField_60() { return &___U3CsourceLossRiskU3Ek__BackingField_60; }
	inline void set_U3CsourceLossRiskU3Ek__BackingField_60(AxisControl_tA997FF52442F7B08C7E13B7028393B4117248680 * value)
	{
		___U3CsourceLossRiskU3Ek__BackingField_60 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CsourceLossRiskU3Ek__BackingField_60), (void*)value);
	}

	inline static int32_t get_offset_of_U3CsourceLossMitigationDirectionU3Ek__BackingField_61() { return static_cast<int32_t>(offsetof(WMRSpatialController_t41CF610FC58697A3F5D3070E0D2A5557915901C6, ___U3CsourceLossMitigationDirectionU3Ek__BackingField_61)); }
	inline Vector3Control_t915A3DA3C3E367B6F5587853E96EAD5215D328AE * get_U3CsourceLossMitigationDirectionU3Ek__BackingField_61() const { return ___U3CsourceLossMitigationDirectionU3Ek__BackingField_61; }
	inline Vector3Control_t915A3DA3C3E367B6F5587853E96EAD5215D328AE ** get_address_of_U3CsourceLossMitigationDirectionU3Ek__BackingField_61() { return &___U3CsourceLossMitigationDirectionU3Ek__BackingField_61; }
	inline void set_U3CsourceLossMitigationDirectionU3Ek__BackingField_61(Vector3Control_t915A3DA3C3E367B6F5587853E96EAD5215D328AE * value)
	{
		___U3CsourceLossMitigationDirectionU3Ek__BackingField_61 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CsourceLossMitigationDirectionU3Ek__BackingField_61), (void*)value);
	}

	inline static int32_t get_offset_of_U3CpointerPositionU3Ek__BackingField_62() { return static_cast<int32_t>(offsetof(WMRSpatialController_t41CF610FC58697A3F5D3070E0D2A5557915901C6, ___U3CpointerPositionU3Ek__BackingField_62)); }
	inline Vector3Control_t915A3DA3C3E367B6F5587853E96EAD5215D328AE * get_U3CpointerPositionU3Ek__BackingField_62() const { return ___U3CpointerPositionU3Ek__BackingField_62; }
	inline Vector3Control_t915A3DA3C3E367B6F5587853E96EAD5215D328AE ** get_address_of_U3CpointerPositionU3Ek__BackingField_62() { return &___U3CpointerPositionU3Ek__BackingField_62; }
	inline void set_U3CpointerPositionU3Ek__BackingField_62(Vector3Control_t915A3DA3C3E367B6F5587853E96EAD5215D328AE * value)
	{
		___U3CpointerPositionU3Ek__BackingField_62 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CpointerPositionU3Ek__BackingField_62), (void*)value);
	}

	inline static int32_t get_offset_of_U3CpointerRotationU3Ek__BackingField_63() { return static_cast<int32_t>(offsetof(WMRSpatialController_t41CF610FC58697A3F5D3070E0D2A5557915901C6, ___U3CpointerRotationU3Ek__BackingField_63)); }
	inline QuaternionControl_t7BEFE3CFC03CC5CE3F779DDCB5AA2C011F5C7A48 * get_U3CpointerRotationU3Ek__BackingField_63() const { return ___U3CpointerRotationU3Ek__BackingField_63; }
	inline QuaternionControl_t7BEFE3CFC03CC5CE3F779DDCB5AA2C011F5C7A48 ** get_address_of_U3CpointerRotationU3Ek__BackingField_63() { return &___U3CpointerRotationU3Ek__BackingField_63; }
	inline void set_U3CpointerRotationU3Ek__BackingField_63(QuaternionControl_t7BEFE3CFC03CC5CE3F779DDCB5AA2C011F5C7A48 * value)
	{
		___U3CpointerRotationU3Ek__BackingField_63 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CpointerRotationU3Ek__BackingField_63), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4571;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4571 = { sizeof (NativeApi_t5F77BA8AF1BBDB28AE8B5329CDDBCB21D6574EF2), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4572;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4572 = { sizeof (WindowsMRInput_t5EDB8246AE09F91332639AB2D8D0D962C79734C7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4573;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4573 = { sizeof (InputLayoutLoader_t8194F7E176BF352FBBD40A342873F2A6933A65CA), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4574;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4574 = { sizeof (UserDefinedSettings_t13075A9CE5747FC83A7F4F3EC5BFA53E0F73402F)+ sizeof (RuntimeObject), sizeof(UserDefinedSettings_t13075A9CE5747FC83A7F4F3EC5BFA53E0F73402F ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4575;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4575 = { sizeof (WindowsMRLoader_t46FCCC8CDD8C8F604D806C954A1BCF9EE817AF92), -1, sizeof(WindowsMRLoader_t46FCCC8CDD8C8F604D806C954A1BCF9EE817AF92_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4576;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4576 = { sizeof (WMRPlane_t145DDAE58819337C56015603B976FAE8AE1A577F)+ sizeof (RuntimeObject), sizeof(WMRPlane_t145DDAE58819337C56015603B976FAE8AE1A577F ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4577;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4577 = { sizeof (WMROrientedBox_t89B9CFE40A5BDF457D8CE1D54F09F73446D68454)+ sizeof (RuntimeObject), sizeof(WMROrientedBox_t89B9CFE40A5BDF457D8CE1D54F09F73446D68454 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4578;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4578 = { sizeof (WMRSphere_t8587086CFF4EB326D562ECD887085F5D5CEB3BB4)+ sizeof (RuntimeObject), sizeof(WMRSphere_t8587086CFF4EB326D562ECD887085F5D5CEB3BB4 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4579;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4579 = { sizeof (MeshingData_tC880B07EE51E038020C40CC1B6A61863D78F8703)+ sizeof (RuntimeObject), sizeof(MeshingData_tC880B07EE51E038020C40CC1B6A61863D78F8703_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4580;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4580 = { sizeof (NativeApi_t8FD1DE10BA51F3BEFE275F05940B1BEA29A71621), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4581;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4581 = { sizeof (WindowsMRExtensions_t03F9C7C197F339DE39B5DCB2D3C60968A0A39EC1), -1, sizeof(WindowsMRExtensions_t03F9C7C197F339DE39B5DCB2D3C60968A0A39EC1_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4582;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4582 = { sizeof (SpatialLocatability_t8CF515124A8D73BEDCFA7133166CF5DAB300B2DD)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4583;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4583 = { sizeof (NativeTypes_t2BBAC7306EFF0C4FA66164F944E5A02589C6470E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4584;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4584 = { sizeof (UnitySubsystemErrorCode_tAA5F8843DC35694D214A6CE631CD3CD5382AD846)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4585;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4585 = { sizeof (UserDefinedSettings_t192979420533CDACE82D93D78FCD17264639368D)+ sizeof (RuntimeObject), sizeof(UserDefinedSettings_t192979420533CDACE82D93D78FCD17264639368D ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4586;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4586 = { sizeof (Native_t558849F3C2B4DC5969DEE6CCB9AC2945D7F93965), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4587;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4587 = { sizeof (DepthBufferOption_tAC9E22B0DD4E1FE942C6E0D77525A118FF7B6462)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4588;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4588 = { sizeof (WindowsMRSettings_tD6F827D24F45274A0B5B38D88702B85BC400DA81), -1, sizeof(WindowsMRSettings_tD6F827D24F45274A0B5B38D88702B85BC400DA81_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4589;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4589 = { sizeof (WMRHMD_t383FD84EEB4BA62C04C8023721C130C5CC05AA0C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4590;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4590 = { sizeof (HololensHand_t95681D1B89FD7FB988895CB96106B5BAE993D022), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4591;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4591 = { sizeof (WMRSpatialController_t41CF610FC58697A3F5D3070E0D2A5557915901C6), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4592;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4592 = { sizeof (U3CModuleU3E_tE7281C0E269ACF224AB82F00FE8D46ED8C621032), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4593;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4593 = { sizeof (U3CModuleU3E_tFDCAFCBB4B3431CFF2DC4D3E03FBFDF54EFF7E9A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4594;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4594 = { sizeof (CameraFollow_tC9B62E254DA1376073E7B793597F9D6CD2A82DF8), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4595;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4595 = { sizeof (PrometeoCarController_tFE973A57FA060DB2AC720E9CBB5CB5D7430DF67E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4596;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4596 = { sizeof (PrometeoTouchInput_t1C5A9B19512A75AFEAD2E916C6F53F597ADF73CF), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4597;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4597 = { sizeof (RotationAxes_t4ECFECEB53ADD83AF4841AF02D59C1D562CC7EC0)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4598;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4598 = { sizeof (MouseLook_t1E1EE79B13022184F3EF9AC8FE8C6771053890DA), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4599;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4599 = { sizeof (WaterMode_tC644407B60E63E8184AD7B9A01762A385325D86C)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4600;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4600 = { sizeof (Water_t49D731BAF5A7CB66C04A942899CCE2391A114F4E), -1, sizeof(Water_t49D731BAF5A7CB66C04A942899CCE2391A114F4E_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4601;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4601 = { sizeof (PlayerActions_t5ED1C532DD140605FE691CA882B2478B7F7FDE23)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4602;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4602 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4603;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4603 = { sizeof (PlayerInputActions_t375171A1A870F2F98685801D864FC76FD516F22C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4604;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4604 = { sizeof (AIController_t79B964E91146AC59C27CD306018BA3E368831F34), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4605;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4605 = { sizeof (AISelector_t0C5CD94B22748E54AC7860C6E3AE2EB3EAC362A6), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4606;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4606 = { sizeof (CarAI_t69CE78AFFF0A7EFA2FFFDF485C42E5A279C8494A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4607;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4607 = { sizeof (CarController_tB945E8CD4EBA16BA6F4C8B4346292786701C9311), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4608;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4608 = { sizeof (GameEndController_t22B3012DEB58F882ECF6BE83EB24B4997F177C12), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4609;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4609 = { sizeof (OpeningScenes_t7E22C921E28D22764559359937031A9C87AE65CD), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4610;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4610 = { sizeof (PersonCharacterController_t8CEAA5B6ABD8B66DB06CDDE5CA6104ECEDC0538B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4611;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4611 = { sizeof (ControlTypes_tA3908D80A567868B8F2C84147F045D36A051B4C6)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4612;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4612 = { sizeof (Directions_t960CFDDDEA9EDA98F5C8E72E7A9682F230821891)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4613;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4613 = { sizeof (RoadDecider_tCF6285CFDF778A9348BC78EF830371F1855A9E61), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4614;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4614 = { sizeof (SignalsCar_t381616BF85EDCCF005FD247DC7E72DB6EAD4DAB0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4615;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4615 = { sizeof (TrafficLight_tD55F68E16E18C0A5F32B506715F5BE0989084ABD), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4616;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4616 = { sizeof (LightCount_t3D030F443416E83983AB89A4AD529CF0A4CF8F73)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4617;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4617 = { sizeof (TrafficLights_t6127A0CC6322E5899984953DA144B63D24333A73), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4618;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4618 = { sizeof (States_t96903228AD196C0C3ACDB1C778496FF9DA9E5D2C)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4619;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4619 = { sizeof (TrafficRules_tA4734D51E40A038ED9DEED23A4311BCD821226A0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4620;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4620 = { sizeof (TrafficRulesController_Car_tB16F6F9B85938EDD1604F77EF7DC0A1BDB472A60), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4621;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4621 = { sizeof (U3CSimulateKeyPressU3Ed__8_tAB6F8BC46272BF48D20F34C4D00E6A15FCEF04C6), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4622;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4622 = { sizeof (VRControls_tEAE1C9E737B679D74AD0DA693794E297010CFA77), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4623;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4623 = { sizeof (WalkerWay_tE1DBB9C531DC74D2654F2AB74C9D6E1304A418DD), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4624;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4624 = { sizeof (MobileDisableAutoSwitchControls_tFFBB1531EDDED48A8D620DF56060164FE3482D4F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4625;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4625 = { sizeof (BoolEvent_tD84A3E3A4245DFD9FA5D608A5ADA77DBDFB6BD56), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4626;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4626 = { sizeof (Event_tB8168EB885996D80674A82913E2B33B4915A9E23), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4627;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4627 = { sizeof (UIVirtualButton_tA1A6B564C5E71E1B61B240E28958DEB8F79BABBE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4628;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4628 = { sizeof (Event_t5D63277B4BC67CE95DE0316A422A69420D3A368D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4629;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4629 = { sizeof (UIVirtualJoystick_tEF0053DAA02EDBA00713D7B29C34394EA7574C2D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4630;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4630 = { sizeof (Event_t1E1104A6087ED46DBF720AC59C7A8E63B974639D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4631;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4631 = { sizeof (UIVirtualTouchZone_t2EB72E6BED3964232BD92D6723DEA7BA013BED23), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4632;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4632 = { sizeof (BasicRigidBodyPush_t9E4C23843BA4437AF06C87D968F0A0BE0B41C714), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4633;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4633 = { sizeof (Section_t9F25FADC74C202674AFDB11AE2AC4D332DE6CA1D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4634;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4634 = { sizeof (Readme_tD58D22679F01C9AD66EA323B2DE68BFF6DBDCEE7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4635;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4635 = { sizeof (StarterAssetsInputs_tE7DDADB7E0E59B93F86B819E96C2523BC28F16DC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4636;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4636 = { sizeof (UICanvasControllerInput_t18C69AA8A6225F0EDEC9D31926AFCA88FC8F8EA1), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4637;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4637 = { sizeof (ThirdPersonController_tCA99A63A4BE57B6A9950FFA8CFFABE8F82AD0903), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
