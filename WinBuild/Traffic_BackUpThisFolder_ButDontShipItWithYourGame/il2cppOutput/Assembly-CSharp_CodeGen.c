﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void CameraFollow::Start()
extern void CameraFollow_Start_mC13DAB135378AADB3E72275F75CDEAF53343810D (void);
// 0x00000002 System.Void CameraFollow::FixedUpdate()
extern void CameraFollow_FixedUpdate_mFECF5648CF148E8FAA66C90BFB5F694E66DC9AB1 (void);
// 0x00000003 System.Void CameraFollow::.ctor()
extern void CameraFollow__ctor_m29F88CCFD2ED12A7BCC75A9BBA892CEF179C83DE (void);
// 0x00000004 System.Void PrometeoCarController::Start()
extern void PrometeoCarController_Start_m4B385B8255B0F3053E1E1DDDBEF99D5C645BB879 (void);
// 0x00000005 System.Void PrometeoCarController::Update()
extern void PrometeoCarController_Update_mCD2796A6E64CFF6241D46D542142262E6ACE7E3D (void);
// 0x00000006 System.Void PrometeoCarController::forward()
extern void PrometeoCarController_forward_m11B1C0CE20DB8732CA5DDE564B0C3830E782BA8F (void);
// 0x00000007 System.Void PrometeoCarController::brake()
extern void PrometeoCarController_brake_mB1CC1DB2EAEA8017959323E589EC910707DBE125 (void);
// 0x00000008 System.Void PrometeoCarController::turnLeft()
extern void PrometeoCarController_turnLeft_m0EDC8C68258E704756DCC09945745AD482E7FCD1 (void);
// 0x00000009 System.Void PrometeoCarController::turnRight()
extern void PrometeoCarController_turnRight_m43C293E07A1037C68ED01384FB9228CBAAD444C4 (void);
// 0x0000000A System.Void PrometeoCarController::CarSpeedUI()
extern void PrometeoCarController_CarSpeedUI_mA64B6AD29552EF133710849CC850457F27B76365 (void);
// 0x0000000B System.Void PrometeoCarController::CarSounds()
extern void PrometeoCarController_CarSounds_mF1FCE33C7863C1174D165F07B12DA0A50341BCEC (void);
// 0x0000000C System.Void PrometeoCarController::TurnLeft()
extern void PrometeoCarController_TurnLeft_mC98E6426FD14AC4DAA668B428173C5EF839F5BB2 (void);
// 0x0000000D System.Void PrometeoCarController::TurnRight()
extern void PrometeoCarController_TurnRight_m95F3887AF9AB8CC980BE6DB3CA18EE9C557EBA52 (void);
// 0x0000000E System.Void PrometeoCarController::ResetSteeringAngle()
extern void PrometeoCarController_ResetSteeringAngle_m5BDE9D564D09A964CF5AE6DE04A5AB59DAF136F9 (void);
// 0x0000000F System.Void PrometeoCarController::AnimateWheelMeshes()
extern void PrometeoCarController_AnimateWheelMeshes_mB48132BEDAADBF6B2FB5E7F1CEEBEDD8B77844AC (void);
// 0x00000010 System.Void PrometeoCarController::GoForward()
extern void PrometeoCarController_GoForward_m26B19648B60B2CF3C84B84CAA6485B9A1C59BA3F (void);
// 0x00000011 System.Void PrometeoCarController::GoReverse()
extern void PrometeoCarController_GoReverse_mE9D879CECD26A91CE67A0522ECB8A11789A0F7F3 (void);
// 0x00000012 System.Void PrometeoCarController::ThrottleOff()
extern void PrometeoCarController_ThrottleOff_mAA81499D7408C68D4CD9C4A5CD10E0B4E6259812 (void);
// 0x00000013 System.Void PrometeoCarController::DecelerateCar()
extern void PrometeoCarController_DecelerateCar_mFFB337BF6A9BE5A5C63D05E0B60DA9B503F976A7 (void);
// 0x00000014 System.Void PrometeoCarController::Brakes()
extern void PrometeoCarController_Brakes_m78ABC34B32C0D8B38797BD15E3E72CE8B5F8D5E6 (void);
// 0x00000015 System.Void PrometeoCarController::Handbrake()
extern void PrometeoCarController_Handbrake_mFCE659A8B66E0FCAF00DFB912D1507401530DADE (void);
// 0x00000016 System.Void PrometeoCarController::DriftCarPS()
extern void PrometeoCarController_DriftCarPS_mE2C4BF2459BC9EF14D73643F45AD85F012D99D16 (void);
// 0x00000017 System.Void PrometeoCarController::RecoverTraction()
extern void PrometeoCarController_RecoverTraction_m877A4AC78EB3D1BF2BAD8BB6AA307621DA2DBE42 (void);
// 0x00000018 System.Void PrometeoCarController::.ctor()
extern void PrometeoCarController__ctor_m6CCE17232A6403AA9FB84CBEE0644044940859C8 (void);
// 0x00000019 System.Void PrometeoTouchInput::Start()
extern void PrometeoTouchInput_Start_mCB5BC85F2DAF455C4AA7C153B3425FBE366D4AC6 (void);
// 0x0000001A System.Void PrometeoTouchInput::ButtonDown()
extern void PrometeoTouchInput_ButtonDown_m1E38B26A6CAF5C8FF3B579461A6CAAA350790874 (void);
// 0x0000001B System.Void PrometeoTouchInput::ButtonUp()
extern void PrometeoTouchInput_ButtonUp_m7761A2CED4BC703972C96EF3A32C9A815850EDA8 (void);
// 0x0000001C System.Void PrometeoTouchInput::.ctor()
extern void PrometeoTouchInput__ctor_m6ABC3988A02D5237BE2C4E234513034EDC158E9C (void);
// 0x0000001D System.Void MouseLook::Update()
extern void MouseLook_Update_m3D49361C94E0433BB35499708EE783B4543D83D5 (void);
// 0x0000001E System.Void MouseLook::Start()
extern void MouseLook_Start_m699B23D66C4F21B566C48A524BC40A828F5E3541 (void);
// 0x0000001F System.Void MouseLook::.ctor()
extern void MouseLook__ctor_mD12D8075DDEA2085341B59FF8BA9FB353613200B (void);
// 0x00000020 System.Void Water::OnWillRenderObject()
extern void Water_OnWillRenderObject_mCB7F1CB2E19B8E8DB0C1A3B362325DA5FBB16A23 (void);
// 0x00000021 System.Void Water::OnDisable()
extern void Water_OnDisable_m6A5E6427974DCDF44385A4DA3E32CE502552014D (void);
// 0x00000022 System.Void Water::Update()
extern void Water_Update_mF334316E3D1AE1A1BAC6C36760A56DED0C573114 (void);
// 0x00000023 System.Void Water::UpdateCameraModes(UnityEngine.Camera,UnityEngine.Camera)
extern void Water_UpdateCameraModes_m3FC403A78A9C91AE531F15BE92325791CDEE13E2 (void);
// 0x00000024 System.Void Water::CreateWaterObjects(UnityEngine.Camera,UnityEngine.Camera&,UnityEngine.Camera&)
extern void Water_CreateWaterObjects_m6CAE7D2F24568DB11382B09CA4ABB332E97A13D9 (void);
// 0x00000025 Water/WaterMode Water::GetWaterMode()
extern void Water_GetWaterMode_mEAA37F8789E1F9B3C60CBB534D223BFAFBA241CF (void);
// 0x00000026 Water/WaterMode Water::FindHardwareWaterSupport()
extern void Water_FindHardwareWaterSupport_m6216B1CE6988DD3F0A74B484DA1B0C2152027030 (void);
// 0x00000027 System.Single Water::sgn(System.Single)
extern void Water_sgn_m10FC19B5C5BF7455443D14434C40D143D5A764D5 (void);
// 0x00000028 UnityEngine.Vector4 Water::CameraSpacePlane(UnityEngine.Camera,UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void Water_CameraSpacePlane_mB693E29889CB1060E0F10ECB0DD4951748BEA75C (void);
// 0x00000029 System.Void Water::CalculateObliqueMatrix(UnityEngine.Matrix4x4&,UnityEngine.Vector4)
extern void Water_CalculateObliqueMatrix_mDFBF3E905287BAA2869D879353E87D0532B89282 (void);
// 0x0000002A System.Void Water::CalculateReflectionMatrix(UnityEngine.Matrix4x4&,UnityEngine.Vector4)
extern void Water_CalculateReflectionMatrix_m03323CD0979B59C6B867B0798453DEE196E8B15C (void);
// 0x0000002B System.Void Water::.ctor()
extern void Water__ctor_m1B8AEF45C59F481296D3E0D0EEC52F72FCA7B4A9 (void);
// 0x0000002C System.Void Water::.cctor()
extern void Water__cctor_m539EDC046A37760DB685781764FC469F3E69EA20 (void);
// 0x0000002D UnityEngine.InputSystem.InputActionAsset PlayerInputActions::get_asset()
extern void PlayerInputActions_get_asset_m3998FFE48C0D23FA068FF8995F9958EF79384CEF (void);
// 0x0000002E System.Void PlayerInputActions::.ctor()
extern void PlayerInputActions__ctor_m40ECBFAA6B9AC3C128C11DEAA490D5186DFE678F (void);
// 0x0000002F System.Void PlayerInputActions::Dispose()
extern void PlayerInputActions_Dispose_mFEB41A9D82B5006F4A423779463066A0DBC10F41 (void);
// 0x00000030 System.Nullable`1<UnityEngine.InputSystem.InputBinding> PlayerInputActions::get_bindingMask()
extern void PlayerInputActions_get_bindingMask_m6668713EA286724120805CA94B7B551F2BFE10A3 (void);
// 0x00000031 System.Void PlayerInputActions::set_bindingMask(System.Nullable`1<UnityEngine.InputSystem.InputBinding>)
extern void PlayerInputActions_set_bindingMask_mBBC2D416E6DCEDAF9464199BC97038F1353CE7E2 (void);
// 0x00000032 System.Nullable`1<UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputDevice>> PlayerInputActions::get_devices()
extern void PlayerInputActions_get_devices_mFC3182893B4F00705EEB7E09C012CE4F99AB358A (void);
// 0x00000033 System.Void PlayerInputActions::set_devices(System.Nullable`1<UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputDevice>>)
extern void PlayerInputActions_set_devices_m3DBF343A225C04F4B64F8A515D54E490D44C331F (void);
// 0x00000034 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputControlScheme> PlayerInputActions::get_controlSchemes()
extern void PlayerInputActions_get_controlSchemes_mFD865C5726108D56C0612AFA1D80EF4FC5127EB9 (void);
// 0x00000035 System.Boolean PlayerInputActions::Contains(UnityEngine.InputSystem.InputAction)
extern void PlayerInputActions_Contains_m5B1FB5D83590F093636A7D62063BB0236641204A (void);
// 0x00000036 System.Collections.Generic.IEnumerator`1<UnityEngine.InputSystem.InputAction> PlayerInputActions::GetEnumerator()
extern void PlayerInputActions_GetEnumerator_m162F155F413A2E7FC58CE8C40C127CECEC5ADB9A (void);
// 0x00000037 System.Collections.IEnumerator PlayerInputActions::System.Collections.IEnumerable.GetEnumerator()
extern void PlayerInputActions_System_Collections_IEnumerable_GetEnumerator_mA49C8C12B15C75E1FEC7C13E6C76443B2453DCD7 (void);
// 0x00000038 System.Void PlayerInputActions::Enable()
extern void PlayerInputActions_Enable_m63290BD2AF7F29D6BB28434A8B8397CCFF191B7B (void);
// 0x00000039 System.Void PlayerInputActions::Disable()
extern void PlayerInputActions_Disable_m145EF8F010BB48B35AAB7B8ACED9A091AEE1AFFE (void);
// 0x0000003A System.Collections.Generic.IEnumerable`1<UnityEngine.InputSystem.InputBinding> PlayerInputActions::get_bindings()
extern void PlayerInputActions_get_bindings_m23A1C9398C0BA879FEF39DADD2941C1CE0984580 (void);
// 0x0000003B UnityEngine.InputSystem.InputAction PlayerInputActions::FindAction(System.String,System.Boolean)
extern void PlayerInputActions_FindAction_mBDE03511C5CE4157FBE35DFAA4EE4DB0F1C693C2 (void);
// 0x0000003C System.Int32 PlayerInputActions::FindBinding(UnityEngine.InputSystem.InputBinding,UnityEngine.InputSystem.InputAction&)
extern void PlayerInputActions_FindBinding_m5A4EE085D5819B6B5F876563BEC5C3D967140F8A (void);
// 0x0000003D PlayerInputActions/PlayerActions PlayerInputActions::get_Player()
extern void PlayerInputActions_get_Player_m6AFEFAFDC04F51E55FAC8DDBB08E1BC0A9DE191C (void);
// 0x0000003E UnityEngine.InputSystem.InputControlScheme PlayerInputActions::get_KeyboardScheme()
extern void PlayerInputActions_get_KeyboardScheme_m6F5FC55D8176657A319D25714A344276180ED82F (void);
// 0x0000003F UnityEngine.InputSystem.InputControlScheme PlayerInputActions::get_VRRemoteScheme()
extern void PlayerInputActions_get_VRRemoteScheme_mA06D43460A91797E02D7B83A0AB7694029BF7554 (void);
// 0x00000040 System.Void PlayerInputActions/PlayerActions::.ctor(PlayerInputActions)
extern void PlayerActions__ctor_mA82998F5F59D711B3E77A4C33CC57DEA2E56C922 (void);
// 0x00000041 UnityEngine.InputSystem.InputAction PlayerInputActions/PlayerActions::get_Movement()
extern void PlayerActions_get_Movement_m5A62A044C81C7DB5E195C190921FEBC6BCEEE36E (void);
// 0x00000042 UnityEngine.InputSystem.InputAction PlayerInputActions/PlayerActions::get_LeftSignal()
extern void PlayerActions_get_LeftSignal_m3041015B4B70CFF200743CAD76FDBD4AA540F893 (void);
// 0x00000043 UnityEngine.InputSystem.InputAction PlayerInputActions/PlayerActions::get_RightSignal()
extern void PlayerActions_get_RightSignal_m5D1761A00CF7BD1EE259FF1CCFC34C61ECA0251C (void);
// 0x00000044 UnityEngine.InputSystem.InputAction PlayerInputActions/PlayerActions::get_Hazard()
extern void PlayerActions_get_Hazard_m01ADACC86D1AEC9015F50AE5438012A528FBF671 (void);
// 0x00000045 UnityEngine.InputSystem.InputAction PlayerInputActions/PlayerActions::get_GoForward()
extern void PlayerActions_get_GoForward_mF225072B61E4AFA792A4CCB08FA85B1D29A54E9F (void);
// 0x00000046 UnityEngine.InputSystem.InputAction PlayerInputActions/PlayerActions::get_GoBack()
extern void PlayerActions_get_GoBack_m031A35BA62DA0BC100C8934740895B92CE1A2582 (void);
// 0x00000047 UnityEngine.InputSystem.InputAction PlayerInputActions/PlayerActions::get_GoLeft()
extern void PlayerActions_get_GoLeft_m5C06165991CD3DE1E7539FFB469CF952F083E381 (void);
// 0x00000048 UnityEngine.InputSystem.InputAction PlayerInputActions/PlayerActions::get_GoRight()
extern void PlayerActions_get_GoRight_mBE49CEE3C1A6F3D5F65118699AD445672F90EE7A (void);
// 0x00000049 UnityEngine.InputSystem.InputActionMap PlayerInputActions/PlayerActions::Get()
extern void PlayerActions_Get_mB3B84313C429B36C7B320015F73D1626157CB0D2 (void);
// 0x0000004A System.Void PlayerInputActions/PlayerActions::Enable()
extern void PlayerActions_Enable_mC7ABF14577F597D6CCDFA8AFC5DD2E83D64BBDDC (void);
// 0x0000004B System.Void PlayerInputActions/PlayerActions::Disable()
extern void PlayerActions_Disable_m1CFEAAAF7D99E7F9C27E0E5753489D78D12AFC80 (void);
// 0x0000004C System.Boolean PlayerInputActions/PlayerActions::get_enabled()
extern void PlayerActions_get_enabled_m9F9A99BE205EF1D798C5915E2C74CF071F288AC6 (void);
// 0x0000004D UnityEngine.InputSystem.InputActionMap PlayerInputActions/PlayerActions::op_Implicit(PlayerInputActions/PlayerActions)
extern void PlayerActions_op_Implicit_mB73E8471CF539BAA38BD4CC528382843A1597308 (void);
// 0x0000004E System.Void PlayerInputActions/PlayerActions::SetCallbacks(PlayerInputActions/IPlayerActions)
extern void PlayerActions_SetCallbacks_m22E60CE0193714BF83FF1D2F6EDB57C98776E80E (void);
// 0x0000004F System.Void PlayerInputActions/IPlayerActions::OnMovement(UnityEngine.InputSystem.InputAction/CallbackContext)
// 0x00000050 System.Void PlayerInputActions/IPlayerActions::OnLeftSignal(UnityEngine.InputSystem.InputAction/CallbackContext)
// 0x00000051 System.Void PlayerInputActions/IPlayerActions::OnRightSignal(UnityEngine.InputSystem.InputAction/CallbackContext)
// 0x00000052 System.Void PlayerInputActions/IPlayerActions::OnHazard(UnityEngine.InputSystem.InputAction/CallbackContext)
// 0x00000053 System.Void PlayerInputActions/IPlayerActions::OnGoForward(UnityEngine.InputSystem.InputAction/CallbackContext)
// 0x00000054 System.Void PlayerInputActions/IPlayerActions::OnGoBack(UnityEngine.InputSystem.InputAction/CallbackContext)
// 0x00000055 System.Void PlayerInputActions/IPlayerActions::OnGoLeft(UnityEngine.InputSystem.InputAction/CallbackContext)
// 0x00000056 System.Void PlayerInputActions/IPlayerActions::OnGoRight(UnityEngine.InputSystem.InputAction/CallbackContext)
// 0x00000057 System.Void AIController::Update()
extern void AIController_Update_mC1B4E8DE676F4EDFD8E3296F5AE6EB8C7D6E73F7 (void);
// 0x00000058 System.Void AIController::.ctor()
extern void AIController__ctor_m28A6370E8C2F1F7BB0C5D75702AE025E843F28A5 (void);
// 0x00000059 System.Void AISelector::.ctor()
extern void AISelector__ctor_m4C193D53AA74524973E91602ED57144B5E6134E2 (void);
// 0x0000005A System.Void CarAI::Start()
extern void CarAI_Start_m96F09F072477B651ED6BFAD36A54957ED51FD624 (void);
// 0x0000005B System.Void CarAI::Update()
extern void CarAI_Update_mB66B03F1089174ECA4CEE436CAE0A8572539CE65 (void);
// 0x0000005C System.Void CarAI::OnTriggerEnter(UnityEngine.Collider)
extern void CarAI_OnTriggerEnter_mEEBE096339730AF8750578F9554403836919B145 (void);
// 0x0000005D System.Void CarAI::.ctor()
extern void CarAI__ctor_mDF45DCA2AADB83426B3C68A0FF47E2A4D46477B3 (void);
// 0x0000005E System.Void CarController::FixedUpdate()
extern void CarController_FixedUpdate_m62A2DA259A4FA667F2A78CC2701E1A4601C4A4F7 (void);
// 0x0000005F System.Void CarController::GetInput()
extern void CarController_GetInput_m13F5FC2DF8E3708ED4E9C54D475DDF692376945F (void);
// 0x00000060 System.Void CarController::HandleMotor()
extern void CarController_HandleMotor_mE8973138355B368A9239C16DA68113B806C21DC1 (void);
// 0x00000061 System.Void CarController::ApplyBreaking()
extern void CarController_ApplyBreaking_m8F4CED40A069C557B2A31EBBD1C1F8507E79F8D6 (void);
// 0x00000062 System.Void CarController::HandleSteering()
extern void CarController_HandleSteering_mF516C45FE58FA3D4FB1A7C520B3C552DF40E9099 (void);
// 0x00000063 System.Void CarController::UpdateWheels()
extern void CarController_UpdateWheels_mD848267D86551E97979325D2D2BC0B1D42B9B433 (void);
// 0x00000064 System.Void CarController::UpdateSingleWheel(UnityEngine.WheelCollider,UnityEngine.Transform)
extern void CarController_UpdateSingleWheel_mEB40B788EFEC5FC24E4821DF96EA2071C3ACEECC (void);
// 0x00000065 System.Void CarController::.ctor()
extern void CarController__ctor_m180A0B729C25B52327B05362FB3A7AA7A53A9739 (void);
// 0x00000066 System.Void GameEndController::Won(System.String)
extern void GameEndController_Won_mE7F8D4A0974C1618D69A403805CF43EC4530B8E4 (void);
// 0x00000067 System.Void GameEndController::Fail(System.String)
extern void GameEndController_Fail_mC506BDD05C982F8359ED8B69807C894AF136F0C2 (void);
// 0x00000068 System.Void GameEndController::.ctor()
extern void GameEndController__ctor_m29834B0A6D2367B154EB2E792115D5DE44D0AA98 (void);
// 0x00000069 System.Void OpeningScenes::Open(System.Int32)
extern void OpeningScenes_Open_mE5839C819BCB1E38BDF101673A7495C027B4FBAA (void);
// 0x0000006A System.Void OpeningScenes::.ctor()
extern void OpeningScenes__ctor_m7597E34401C0CF2BEED852E563F09A839EED7CB2 (void);
// 0x0000006B System.Void PersonCharacterController::Start()
extern void PersonCharacterController_Start_m542A56AEEE8D0E482D9886A5479B7D4C0F41B68D (void);
// 0x0000006C System.Void PersonCharacterController::Update()
extern void PersonCharacterController_Update_mF6F11F766BF3240BE44E27100208A7546354881A (void);
// 0x0000006D System.Void PersonCharacterController::OnTriggerEnter(UnityEngine.Collider)
extern void PersonCharacterController_OnTriggerEnter_m2AE97A0C8487DBB3C5F97592B85D0568170CEFE9 (void);
// 0x0000006E System.Void PersonCharacterController::.ctor()
extern void PersonCharacterController__ctor_m65A918E99333B6E62A6410191FC2700EE826D396 (void);
// 0x0000006F UnityEngine.GameObject RoadDecider::SelectNext()
extern void RoadDecider_SelectNext_m49FF1483B105A01F9D2AF8E0387A1854977AE024 (void);
// 0x00000070 RoadDecider/Directions RoadDecider::GetDirection()
extern void RoadDecider_GetDirection_m4BCF3158D4EBE87BAE8218C3ECCCD81C463D04EB (void);
// 0x00000071 System.Void RoadDecider::.ctor()
extern void RoadDecider__ctor_mCB62EFE2AF8CC081B247335F47521F9AF9AD57BA (void);
// 0x00000072 System.Void SignalsCar::Start()
extern void SignalsCar_Start_m0C3F63BAC47BB0B568F6D09A6A1AC7408D342DE3 (void);
// 0x00000073 System.Void SignalsCar::Update()
extern void SignalsCar_Update_m5F86490653B6972F72DB5C93151DB92E83D73A78 (void);
// 0x00000074 System.Void SignalsCar::FixedUpdate()
extern void SignalsCar_FixedUpdate_m53D3C162FCC658477FFBBD9DFF7732DC7273C1FA (void);
// 0x00000075 System.Void SignalsCar::SetLeft()
extern void SignalsCar_SetLeft_m2126FAEC51BCDBE099389C70D52003038AF0F1C1 (void);
// 0x00000076 System.Void SignalsCar::SetRight()
extern void SignalsCar_SetRight_m674EDE4006B4A9A474C425E47921FBD50EE84E88 (void);
// 0x00000077 System.Void SignalsCar::SetHazard()
extern void SignalsCar_SetHazard_mB9751516CAAB2B765CB8ACD564D1F7B8208CC1B7 (void);
// 0x00000078 System.Void SignalsCar::DisableAll()
extern void SignalsCar_DisableAll_mB701170D1C5821270CAF681762AC0C2D41888476 (void);
// 0x00000079 System.Void SignalsCar::ChangeCamera()
extern void SignalsCar_ChangeCamera_mD1BA0AEFA1D87E3B3B624A002A7AF36D5B8C58C0 (void);
// 0x0000007A System.Void SignalsCar::.ctor()
extern void SignalsCar__ctor_mF51427FDDD2C0C845256C607AC6BDCC48DD56830 (void);
// 0x0000007B System.Void TrafficLight::Start()
extern void TrafficLight_Start_m4FD84738A302301C102E99D70DAA5683F1AFA3CA (void);
// 0x0000007C System.Void TrafficLight::FixedUpdate()
extern void TrafficLight_FixedUpdate_m390DDB233E7A4E1283355F2E4250EE3A048E6C91 (void);
// 0x0000007D System.Void TrafficLight::AllowCars()
extern void TrafficLight_AllowCars_m180D31929E4469F1452F21492C1CA1848F8C5EA4 (void);
// 0x0000007E System.Void TrafficLight::AllowWalkers()
extern void TrafficLight_AllowWalkers_m8030286CDE1BF3ED95A4E732824A9BCD816207A5 (void);
// 0x0000007F System.Void TrafficLight::setOtherLights()
extern void TrafficLight_setOtherLights_mBF9A30E0D034D5546D5D7BBE2DFBE4715E51C056 (void);
// 0x00000080 System.Void TrafficLight::Car_RedToYellow()
extern void TrafficLight_Car_RedToYellow_m7D484C13BEC714E16F6CDFAE3AD5759D80FA2AFE (void);
// 0x00000081 System.Void TrafficLight::Car_YellowToGreen()
extern void TrafficLight_Car_YellowToGreen_m1D84BBF573031A16531382C92181A814258EB904 (void);
// 0x00000082 System.Void TrafficLight::Car_GreenToYellow()
extern void TrafficLight_Car_GreenToYellow_mD499CC5D3D37CBC479C55FF4D116A742BB013937 (void);
// 0x00000083 System.Void TrafficLight::Car_YellowToRed()
extern void TrafficLight_Car_YellowToRed_mE638B0892F0DF1871F86EBFB5096303ABCC8F698 (void);
// 0x00000084 System.Void TrafficLight::Walker_GreenToRed()
extern void TrafficLight_Walker_GreenToRed_mEB3409FEB0DE10F130BFEB3AF2614B17BE20B5C3 (void);
// 0x00000085 System.Void TrafficLight::Walker_RedToGreen()
extern void TrafficLight_Walker_RedToGreen_mF769D2FA12BC683857BD8B147B1D4A4C2A45C01E (void);
// 0x00000086 System.Void TrafficLight::.ctor()
extern void TrafficLight__ctor_m25FBA1B06317E68A42625CD73E906BBE288896AF (void);
// 0x00000087 System.Void TrafficLights::Start()
extern void TrafficLights_Start_mA212235A87B9A7406D3BAF66EFBC9C84F5FD6565 (void);
// 0x00000088 System.Void TrafficLights::Update()
extern void TrafficLights_Update_mA82E8D2FF73196F0906E1898E48051C4DEF2E86D (void);
// 0x00000089 System.Void TrafficLights::.ctor()
extern void TrafficLights__ctor_m59AC945FF0A2BE5A91346B658650D49DDBFB7171 (void);
// 0x0000008A System.Void TrafficRules::Start()
extern void TrafficRules_Start_m958F3995E85CCDFAE511CC388DFE026F7E2A9917 (void);
// 0x0000008B System.Void TrafficRules::Update()
extern void TrafficRules_Update_m5387BDBE3C58500D9FD50F3A9DB3762AF8B413D9 (void);
// 0x0000008C System.Void TrafficRules::.ctor()
extern void TrafficRules__ctor_mC2C98984230F9A8CDF1C08CBBCB04B6F2511E8EE (void);
// 0x0000008D System.Void TrafficRulesController_Car::Start()
extern void TrafficRulesController_Car_Start_m23E16B8E1CB041431FF804B61A5F461CAAB06F7C (void);
// 0x0000008E System.Void TrafficRulesController_Car::Update()
extern void TrafficRulesController_Car_Update_mC0C047BE67F31D6341A085E8EBB6CB0AA1374D48 (void);
// 0x0000008F System.Void TrafficRulesController_Car::OnTriggerEnter(UnityEngine.Collider)
extern void TrafficRulesController_Car_OnTriggerEnter_mC17E3FCD3BC37D6A04F22ED9B2231ABDA38A7F07 (void);
// 0x00000090 System.Void TrafficRulesController_Car::.ctor()
extern void TrafficRulesController_Car__ctor_m84E57F2020FD1F32910A5F4D2718E9DB5D8DD7BA (void);
// 0x00000091 System.Void VRControls::Start()
extern void VRControls_Start_m61878CF076A16DB281675DB573D82BFFCDC1F3A0 (void);
// 0x00000092 System.Void VRControls::Update()
extern void VRControls_Update_m2F68B0220A566B6AE6826C076A1649169D3272A7 (void);
// 0x00000093 System.Void VRControls::goForward(UnityEngine.InputSystem.InputAction/CallbackContext)
extern void VRControls_goForward_m49A5138744CCFBFEE8F0C47B051A9E7BD685E05B (void);
// 0x00000094 System.Void VRControls::go()
extern void VRControls_go_mDB1A92E9287135C174899FF40DE979D4681668EC (void);
// 0x00000095 System.Collections.IEnumerator VRControls::SimulateKeyPress()
extern void VRControls_SimulateKeyPress_m42F3AD2DAACED561F00377A6F9732B1A3AE1DC51 (void);
// 0x00000096 System.Void VRControls::goBack(UnityEngine.InputSystem.InputAction/CallbackContext)
extern void VRControls_goBack_mBD3DF537AAF81CE04C8C8981D2F43F81A203AC5E (void);
// 0x00000097 System.Void VRControls::turnLeft(UnityEngine.InputSystem.InputAction/CallbackContext)
extern void VRControls_turnLeft_m6BA11C70EF91EA2169CA87D88AEEBA28C5EF247A (void);
// 0x00000098 System.Void VRControls::turnRight(UnityEngine.InputSystem.InputAction/CallbackContext)
extern void VRControls_turnRight_mDD5DA2BD8701F3BE186DAF339FC1CFDE32DE44A6 (void);
// 0x00000099 System.Void VRControls::rightSignal(UnityEngine.InputSystem.InputAction/CallbackContext)
extern void VRControls_rightSignal_mCBA6A76D5C72DB44363E15BC22BE2A694284F66D (void);
// 0x0000009A System.Void VRControls::leftSignal(UnityEngine.InputSystem.InputAction/CallbackContext)
extern void VRControls_leftSignal_m7599CC8A2EC173EDF8C801BBEDC00343B7387391 (void);
// 0x0000009B System.Void VRControls::.ctor()
extern void VRControls__ctor_m3FE3DB24192EB9070E0C6E21AC01443E50BD669D (void);
// 0x0000009C System.Void VRControls/<SimulateKeyPress>d__8::.ctor(System.Int32)
extern void U3CSimulateKeyPressU3Ed__8__ctor_mB333C0FA66B2395C19496CC79C6100503A0633BF (void);
// 0x0000009D System.Void VRControls/<SimulateKeyPress>d__8::System.IDisposable.Dispose()
extern void U3CSimulateKeyPressU3Ed__8_System_IDisposable_Dispose_m6B6AA45F2BFEA8845A8F47384DAECB7A55DC4FC9 (void);
// 0x0000009E System.Boolean VRControls/<SimulateKeyPress>d__8::MoveNext()
extern void U3CSimulateKeyPressU3Ed__8_MoveNext_m9AFFCD72905EC5A46322EB22ECBD633B3FEF1ED5 (void);
// 0x0000009F System.Object VRControls/<SimulateKeyPress>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSimulateKeyPressU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFB7AE2BA8C02D3857C2599A9D8B6389319488E07 (void);
// 0x000000A0 System.Void VRControls/<SimulateKeyPress>d__8::System.Collections.IEnumerator.Reset()
extern void U3CSimulateKeyPressU3Ed__8_System_Collections_IEnumerator_Reset_m5A0CAFC39F155EEFA816F434F43DEBC933C7FD10 (void);
// 0x000000A1 System.Object VRControls/<SimulateKeyPress>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CSimulateKeyPressU3Ed__8_System_Collections_IEnumerator_get_Current_m8D210BFBFCCE65F373299DC960D8D4FE87D5C0EE (void);
// 0x000000A2 System.Void WalkerWay::Start()
extern void WalkerWay_Start_m09F719DB74AEF1231B80CF9F74FCCDE112814F09 (void);
// 0x000000A3 System.Void WalkerWay::Update()
extern void WalkerWay_Update_mEA24C4F9627A504746142EF6B0A78EBD639FD4B3 (void);
// 0x000000A4 System.Void WalkerWay::.ctor()
extern void WalkerWay__ctor_mC5A4AB307D8BD3A259E13A53E1BDBDF61810E402 (void);
// 0x000000A5 System.Void MobileDisableAutoSwitchControls::.ctor()
extern void MobileDisableAutoSwitchControls__ctor_m7E0E6D5F71FAC813E427F2E19ACBCAA8F0CAF94A (void);
// 0x000000A6 System.Void UIVirtualButton::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void UIVirtualButton_OnPointerDown_m9B8BBCAB64E2E6799E23311EAB29104788AF5607 (void);
// 0x000000A7 System.Void UIVirtualButton::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void UIVirtualButton_OnPointerUp_m8928883C1D8EEB4E6BD673089D61C61C15FD6E26 (void);
// 0x000000A8 System.Void UIVirtualButton::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern void UIVirtualButton_OnPointerClick_m8C77F496BC858EC51F8BDA3759219187DB267A3F (void);
// 0x000000A9 System.Void UIVirtualButton::OutputButtonStateValue(System.Boolean)
extern void UIVirtualButton_OutputButtonStateValue_m4D03DEE754B7C8716A55432137E088D08626C8A8 (void);
// 0x000000AA System.Void UIVirtualButton::OutputButtonClickEvent()
extern void UIVirtualButton_OutputButtonClickEvent_m52515C61D5E82F76620D03994ED97A1F34CBA6F5 (void);
// 0x000000AB System.Void UIVirtualButton::.ctor()
extern void UIVirtualButton__ctor_mC6B209AB41BF45B6C0B9B30278CDD72DA776983A (void);
// 0x000000AC System.Void UIVirtualButton/BoolEvent::.ctor()
extern void BoolEvent__ctor_m4D8C9EEAB13A21F9D68284FB7359E58E0EAD03E5 (void);
// 0x000000AD System.Void UIVirtualButton/Event::.ctor()
extern void Event__ctor_m8A0990FCB8628C5987E39013E0316961170DF9AC (void);
// 0x000000AE System.Void UIVirtualJoystick::Start()
extern void UIVirtualJoystick_Start_m67CF1EE6291EACAF0C3A1B0DB535BD525EA479F4 (void);
// 0x000000AF System.Void UIVirtualJoystick::SetupHandle()
extern void UIVirtualJoystick_SetupHandle_m0ACFFD29C51B77207E694FD86F4A84338B030207 (void);
// 0x000000B0 System.Void UIVirtualJoystick::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void UIVirtualJoystick_OnPointerDown_mEC473003FA53CFFFBD478CD5E1A3BA3509AE1E12 (void);
// 0x000000B1 System.Void UIVirtualJoystick::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern void UIVirtualJoystick_OnDrag_m4AB1A15DA5C6DB622E55C21E6E7ED263FED7DB77 (void);
// 0x000000B2 System.Void UIVirtualJoystick::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void UIVirtualJoystick_OnPointerUp_m0FAFE18C832E0C2A30ACA560276C017A59863614 (void);
// 0x000000B3 System.Void UIVirtualJoystick::OutputPointerEventValue(UnityEngine.Vector2)
extern void UIVirtualJoystick_OutputPointerEventValue_mD4C7421AB87B90625945F3B8B1F58F611492E64A (void);
// 0x000000B4 System.Void UIVirtualJoystick::UpdateHandleRectPosition(UnityEngine.Vector2)
extern void UIVirtualJoystick_UpdateHandleRectPosition_m316CA3567F5830F9F19C381F20FEF41084740B94 (void);
// 0x000000B5 UnityEngine.Vector2 UIVirtualJoystick::ApplySizeDelta(UnityEngine.Vector2)
extern void UIVirtualJoystick_ApplySizeDelta_m62A3D273C1B2063E387278930C27828AAAA3D706 (void);
// 0x000000B6 UnityEngine.Vector2 UIVirtualJoystick::ClampValuesToMagnitude(UnityEngine.Vector2)
extern void UIVirtualJoystick_ClampValuesToMagnitude_m8B710D2ABFEAB67917267BE7B2E253A5C572907D (void);
// 0x000000B7 UnityEngine.Vector2 UIVirtualJoystick::ApplyInversionFilter(UnityEngine.Vector2)
extern void UIVirtualJoystick_ApplyInversionFilter_mD507E46BD99810E9B18F0AD4BDD843FACF70536B (void);
// 0x000000B8 System.Single UIVirtualJoystick::InvertValue(System.Single)
extern void UIVirtualJoystick_InvertValue_m51B67357EC3DF48051879F1DA61B76F4A4BBD9F7 (void);
// 0x000000B9 System.Void UIVirtualJoystick::.ctor()
extern void UIVirtualJoystick__ctor_m3EFA0B88827F62A24F2BFE15B2A782256CFB05F1 (void);
// 0x000000BA System.Void UIVirtualJoystick/Event::.ctor()
extern void Event__ctor_m55DEBAA169467EF82E82EA7541576F59539139B7 (void);
// 0x000000BB System.Void UIVirtualTouchZone::Start()
extern void UIVirtualTouchZone_Start_mA98396B89C28AC97493CB9A80630F0459C7C9350 (void);
// 0x000000BC System.Void UIVirtualTouchZone::SetupHandle()
extern void UIVirtualTouchZone_SetupHandle_m46D4991D16E65721ED9A06C538841C45A728151D (void);
// 0x000000BD System.Void UIVirtualTouchZone::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void UIVirtualTouchZone_OnPointerDown_mCFB81700E80EAD8CB2402BC894448E8CFF7434E4 (void);
// 0x000000BE System.Void UIVirtualTouchZone::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern void UIVirtualTouchZone_OnDrag_mAA2371D1A929E0EB173141433B05D2E496433B37 (void);
// 0x000000BF System.Void UIVirtualTouchZone::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void UIVirtualTouchZone_OnPointerUp_mE65CFD342E3EFCCD3C0ED8596C845E9020042B05 (void);
// 0x000000C0 System.Void UIVirtualTouchZone::OutputPointerEventValue(UnityEngine.Vector2)
extern void UIVirtualTouchZone_OutputPointerEventValue_m1ECAF819A22591A0241D790EFC936ECB3985CE68 (void);
// 0x000000C1 System.Void UIVirtualTouchZone::UpdateHandleRectPosition(UnityEngine.Vector2)
extern void UIVirtualTouchZone_UpdateHandleRectPosition_m4D0B42E5C70893DFB5408E623076C9F5B7FE3A7A (void);
// 0x000000C2 System.Void UIVirtualTouchZone::SetObjectActiveState(UnityEngine.GameObject,System.Boolean)
extern void UIVirtualTouchZone_SetObjectActiveState_mDB5AA4C0AB194A0009D681F773145E68CB678699 (void);
// 0x000000C3 UnityEngine.Vector2 UIVirtualTouchZone::GetDeltaBetweenPositions(UnityEngine.Vector2,UnityEngine.Vector2)
extern void UIVirtualTouchZone_GetDeltaBetweenPositions_mA12C35395F0319B30B47A5791759AB8FE240186A (void);
// 0x000000C4 UnityEngine.Vector2 UIVirtualTouchZone::ClampValuesToMagnitude(UnityEngine.Vector2)
extern void UIVirtualTouchZone_ClampValuesToMagnitude_m2B1CBB4A20BE4004AD520E5EC427C664E9315EFC (void);
// 0x000000C5 UnityEngine.Vector2 UIVirtualTouchZone::ApplyInversionFilter(UnityEngine.Vector2)
extern void UIVirtualTouchZone_ApplyInversionFilter_m8FEABDE5FD0BF979E96732DCF3123203E6B755E3 (void);
// 0x000000C6 System.Single UIVirtualTouchZone::InvertValue(System.Single)
extern void UIVirtualTouchZone_InvertValue_m5B003E6987A1518CEB604D6B1796A58DE5D04E4A (void);
// 0x000000C7 System.Void UIVirtualTouchZone::.ctor()
extern void UIVirtualTouchZone__ctor_m74665B8EF96FBD3D7C35B545E662FEE10C11A2A8 (void);
// 0x000000C8 System.Void UIVirtualTouchZone/Event::.ctor()
extern void Event__ctor_mE60A25521698C4C733D32C1D85667315E23BEE77 (void);
// 0x000000C9 System.Void BasicRigidBodyPush::OnControllerColliderHit(UnityEngine.ControllerColliderHit)
extern void BasicRigidBodyPush_OnControllerColliderHit_m32CA56C895E52B8AEC6A383197F4F3B9CF52155D (void);
// 0x000000CA System.Void BasicRigidBodyPush::PushRigidBodies(UnityEngine.ControllerColliderHit)
extern void BasicRigidBodyPush_PushRigidBodies_mE4B69524BF1D9A657193F0032E95F9A5F806FF27 (void);
// 0x000000CB System.Void BasicRigidBodyPush::.ctor()
extern void BasicRigidBodyPush__ctor_mD1FBA2CCB9BB6CD5A9A5768884649B55672420E1 (void);
// 0x000000CC System.Void Readme::.ctor()
extern void Readme__ctor_mF465410C5B2E598F2685E82CFCE1F42186AFF448 (void);
// 0x000000CD System.Void Readme/Section::.ctor()
extern void Section__ctor_mBAD5262A353BC071C61B8DB462A3D4D5AB5C7C4E (void);
// 0x000000CE System.Void StarterAssets.StarterAssetsInputs::OnMove(UnityEngine.InputSystem.InputValue)
extern void StarterAssetsInputs_OnMove_mC62E022C4B95BC8A3B9932C2F8EFCE91A38B88FA (void);
// 0x000000CF System.Void StarterAssets.StarterAssetsInputs::OnLook(UnityEngine.InputSystem.InputValue)
extern void StarterAssetsInputs_OnLook_m7A5EE56C246489C21CF21A0B66AA0AA44B8CC35C (void);
// 0x000000D0 System.Void StarterAssets.StarterAssetsInputs::OnJump(UnityEngine.InputSystem.InputValue)
extern void StarterAssetsInputs_OnJump_m142391E84B470F4AFDE6741C29AB922B9EA3791B (void);
// 0x000000D1 System.Void StarterAssets.StarterAssetsInputs::OnSprint(UnityEngine.InputSystem.InputValue)
extern void StarterAssetsInputs_OnSprint_mFCCCAD7A3EAEFC6C7D71AE4EE31CFFEF1D87BEA1 (void);
// 0x000000D2 System.Void StarterAssets.StarterAssetsInputs::MoveInput(UnityEngine.Vector2)
extern void StarterAssetsInputs_MoveInput_m17B8532D1DB7D870295DE9E82CBC17735F8984F8 (void);
// 0x000000D3 System.Void StarterAssets.StarterAssetsInputs::LookInput(UnityEngine.Vector2)
extern void StarterAssetsInputs_LookInput_m3B98228935C1AD2E76356732A798A3EFD8CA4186 (void);
// 0x000000D4 System.Void StarterAssets.StarterAssetsInputs::JumpInput(System.Boolean)
extern void StarterAssetsInputs_JumpInput_mEEC05639651E0D3AD679B923D34C3B58F52EC722 (void);
// 0x000000D5 System.Void StarterAssets.StarterAssetsInputs::SprintInput(System.Boolean)
extern void StarterAssetsInputs_SprintInput_m4A4EB5B45A168786D74E233577623FC38D74E4F3 (void);
// 0x000000D6 System.Void StarterAssets.StarterAssetsInputs::OnApplicationFocus(System.Boolean)
extern void StarterAssetsInputs_OnApplicationFocus_m0B8F80F47202B1072318C9DE58C5C1D3F2EAFF28 (void);
// 0x000000D7 System.Void StarterAssets.StarterAssetsInputs::SetCursorState(System.Boolean)
extern void StarterAssetsInputs_SetCursorState_m693E19396F2F0ECB2FB051410EEB72DC1B037DD0 (void);
// 0x000000D8 System.Void StarterAssets.StarterAssetsInputs::.ctor()
extern void StarterAssetsInputs__ctor_mACF3B3A6FB6B023BDBA341E65079F409777BD67B (void);
// 0x000000D9 System.Void StarterAssets.UICanvasControllerInput::VirtualMoveInput(UnityEngine.Vector2)
extern void UICanvasControllerInput_VirtualMoveInput_m3DCA73AB02D0D9D4AF69809D2EA9A388A9DD2D20 (void);
// 0x000000DA System.Void StarterAssets.UICanvasControllerInput::VirtualLookInput(UnityEngine.Vector2)
extern void UICanvasControllerInput_VirtualLookInput_mDCD0EF95A5FD1C0351C6EF330F0E21A2BC5B3BD7 (void);
// 0x000000DB System.Void StarterAssets.UICanvasControllerInput::VirtualJumpInput(System.Boolean)
extern void UICanvasControllerInput_VirtualJumpInput_m4BF9A688729DE9CAB0368373B4508833CBAF158A (void);
// 0x000000DC System.Void StarterAssets.UICanvasControllerInput::VirtualSprintInput(System.Boolean)
extern void UICanvasControllerInput_VirtualSprintInput_mC1DD56717CE777F772956FF98DA099A0C0BFD9B3 (void);
// 0x000000DD System.Void StarterAssets.UICanvasControllerInput::.ctor()
extern void UICanvasControllerInput__ctor_mBBEE562E79321A1939A2B006E6654FA85259EDEB (void);
// 0x000000DE System.Boolean StarterAssets.ThirdPersonController::get_IsCurrentDeviceMouse()
extern void ThirdPersonController_get_IsCurrentDeviceMouse_mFD1AE87B85178F91109581E5F86C8BDB1FD9773E (void);
// 0x000000DF System.Void StarterAssets.ThirdPersonController::Awake()
extern void ThirdPersonController_Awake_m982FE30F7B3D80F71E2BB40E5E9C85266840D98C (void);
// 0x000000E0 System.Void StarterAssets.ThirdPersonController::Start()
extern void ThirdPersonController_Start_mDF1E5FFEF57D1CD6BCC3A25993F99F5ECD673C43 (void);
// 0x000000E1 System.Void StarterAssets.ThirdPersonController::Update()
extern void ThirdPersonController_Update_m6A729BE364E703138033EE4C7247074BC13C6F9E (void);
// 0x000000E2 System.Void StarterAssets.ThirdPersonController::LateUpdate()
extern void ThirdPersonController_LateUpdate_m1F8928BBA64078F5E8062FCE4A9EA6A2C504750F (void);
// 0x000000E3 System.Void StarterAssets.ThirdPersonController::AssignAnimationIDs()
extern void ThirdPersonController_AssignAnimationIDs_mB8641B310C6DDEC521D5401B691A156B730C637A (void);
// 0x000000E4 System.Void StarterAssets.ThirdPersonController::GroundedCheck()
extern void ThirdPersonController_GroundedCheck_mB9F085D761FAD5805B651EC2F3466B240D94964A (void);
// 0x000000E5 System.Void StarterAssets.ThirdPersonController::CameraRotation()
extern void ThirdPersonController_CameraRotation_m389620971F720216A6BF133DFB0845BACD4866B4 (void);
// 0x000000E6 System.Void StarterAssets.ThirdPersonController::Move()
extern void ThirdPersonController_Move_m0444969DC5E148B2093542C8F31B22DF98D95B6D (void);
// 0x000000E7 System.Void StarterAssets.ThirdPersonController::JumpAndGravity()
extern void ThirdPersonController_JumpAndGravity_mF8ADE69C505E77DFB9A7A187A2E914F4D0390514 (void);
// 0x000000E8 System.Single StarterAssets.ThirdPersonController::ClampAngle(System.Single,System.Single,System.Single)
extern void ThirdPersonController_ClampAngle_mFAEB55D93C154C7523412A3287C3A957173E7455 (void);
// 0x000000E9 System.Void StarterAssets.ThirdPersonController::OnDrawGizmosSelected()
extern void ThirdPersonController_OnDrawGizmosSelected_m23A137AF678E2C538238940AE45FBB2856E73AE2 (void);
// 0x000000EA System.Void StarterAssets.ThirdPersonController::OnFootstep(UnityEngine.AnimationEvent)
extern void ThirdPersonController_OnFootstep_m2D51C8C4A6A189DBCF516409DECF9828157A6051 (void);
// 0x000000EB System.Void StarterAssets.ThirdPersonController::OnLand(UnityEngine.AnimationEvent)
extern void ThirdPersonController_OnLand_m038EF8FE1B2F9D41B1E624B40741D1C12FEE7473 (void);
// 0x000000EC System.Void StarterAssets.ThirdPersonController::.ctor()
extern void ThirdPersonController__ctor_m1A6595F81F751D300BAFBDAD5A3F8B4B1E339391 (void);
static Il2CppMethodPointer s_methodPointers[236] = 
{
	CameraFollow_Start_mC13DAB135378AADB3E72275F75CDEAF53343810D,
	CameraFollow_FixedUpdate_mFECF5648CF148E8FAA66C90BFB5F694E66DC9AB1,
	CameraFollow__ctor_m29F88CCFD2ED12A7BCC75A9BBA892CEF179C83DE,
	PrometeoCarController_Start_m4B385B8255B0F3053E1E1DDDBEF99D5C645BB879,
	PrometeoCarController_Update_mCD2796A6E64CFF6241D46D542142262E6ACE7E3D,
	PrometeoCarController_forward_m11B1C0CE20DB8732CA5DDE564B0C3830E782BA8F,
	PrometeoCarController_brake_mB1CC1DB2EAEA8017959323E589EC910707DBE125,
	PrometeoCarController_turnLeft_m0EDC8C68258E704756DCC09945745AD482E7FCD1,
	PrometeoCarController_turnRight_m43C293E07A1037C68ED01384FB9228CBAAD444C4,
	PrometeoCarController_CarSpeedUI_mA64B6AD29552EF133710849CC850457F27B76365,
	PrometeoCarController_CarSounds_mF1FCE33C7863C1174D165F07B12DA0A50341BCEC,
	PrometeoCarController_TurnLeft_mC98E6426FD14AC4DAA668B428173C5EF839F5BB2,
	PrometeoCarController_TurnRight_m95F3887AF9AB8CC980BE6DB3CA18EE9C557EBA52,
	PrometeoCarController_ResetSteeringAngle_m5BDE9D564D09A964CF5AE6DE04A5AB59DAF136F9,
	PrometeoCarController_AnimateWheelMeshes_mB48132BEDAADBF6B2FB5E7F1CEEBEDD8B77844AC,
	PrometeoCarController_GoForward_m26B19648B60B2CF3C84B84CAA6485B9A1C59BA3F,
	PrometeoCarController_GoReverse_mE9D879CECD26A91CE67A0522ECB8A11789A0F7F3,
	PrometeoCarController_ThrottleOff_mAA81499D7408C68D4CD9C4A5CD10E0B4E6259812,
	PrometeoCarController_DecelerateCar_mFFB337BF6A9BE5A5C63D05E0B60DA9B503F976A7,
	PrometeoCarController_Brakes_m78ABC34B32C0D8B38797BD15E3E72CE8B5F8D5E6,
	PrometeoCarController_Handbrake_mFCE659A8B66E0FCAF00DFB912D1507401530DADE,
	PrometeoCarController_DriftCarPS_mE2C4BF2459BC9EF14D73643F45AD85F012D99D16,
	PrometeoCarController_RecoverTraction_m877A4AC78EB3D1BF2BAD8BB6AA307621DA2DBE42,
	PrometeoCarController__ctor_m6CCE17232A6403AA9FB84CBEE0644044940859C8,
	PrometeoTouchInput_Start_mCB5BC85F2DAF455C4AA7C153B3425FBE366D4AC6,
	PrometeoTouchInput_ButtonDown_m1E38B26A6CAF5C8FF3B579461A6CAAA350790874,
	PrometeoTouchInput_ButtonUp_m7761A2CED4BC703972C96EF3A32C9A815850EDA8,
	PrometeoTouchInput__ctor_m6ABC3988A02D5237BE2C4E234513034EDC158E9C,
	MouseLook_Update_m3D49361C94E0433BB35499708EE783B4543D83D5,
	MouseLook_Start_m699B23D66C4F21B566C48A524BC40A828F5E3541,
	MouseLook__ctor_mD12D8075DDEA2085341B59FF8BA9FB353613200B,
	Water_OnWillRenderObject_mCB7F1CB2E19B8E8DB0C1A3B362325DA5FBB16A23,
	Water_OnDisable_m6A5E6427974DCDF44385A4DA3E32CE502552014D,
	Water_Update_mF334316E3D1AE1A1BAC6C36760A56DED0C573114,
	Water_UpdateCameraModes_m3FC403A78A9C91AE531F15BE92325791CDEE13E2,
	Water_CreateWaterObjects_m6CAE7D2F24568DB11382B09CA4ABB332E97A13D9,
	Water_GetWaterMode_mEAA37F8789E1F9B3C60CBB534D223BFAFBA241CF,
	Water_FindHardwareWaterSupport_m6216B1CE6988DD3F0A74B484DA1B0C2152027030,
	Water_sgn_m10FC19B5C5BF7455443D14434C40D143D5A764D5,
	Water_CameraSpacePlane_mB693E29889CB1060E0F10ECB0DD4951748BEA75C,
	Water_CalculateObliqueMatrix_mDFBF3E905287BAA2869D879353E87D0532B89282,
	Water_CalculateReflectionMatrix_m03323CD0979B59C6B867B0798453DEE196E8B15C,
	Water__ctor_m1B8AEF45C59F481296D3E0D0EEC52F72FCA7B4A9,
	Water__cctor_m539EDC046A37760DB685781764FC469F3E69EA20,
	PlayerInputActions_get_asset_m3998FFE48C0D23FA068FF8995F9958EF79384CEF,
	PlayerInputActions__ctor_m40ECBFAA6B9AC3C128C11DEAA490D5186DFE678F,
	PlayerInputActions_Dispose_mFEB41A9D82B5006F4A423779463066A0DBC10F41,
	PlayerInputActions_get_bindingMask_m6668713EA286724120805CA94B7B551F2BFE10A3,
	PlayerInputActions_set_bindingMask_mBBC2D416E6DCEDAF9464199BC97038F1353CE7E2,
	PlayerInputActions_get_devices_mFC3182893B4F00705EEB7E09C012CE4F99AB358A,
	PlayerInputActions_set_devices_m3DBF343A225C04F4B64F8A515D54E490D44C331F,
	PlayerInputActions_get_controlSchemes_mFD865C5726108D56C0612AFA1D80EF4FC5127EB9,
	PlayerInputActions_Contains_m5B1FB5D83590F093636A7D62063BB0236641204A,
	PlayerInputActions_GetEnumerator_m162F155F413A2E7FC58CE8C40C127CECEC5ADB9A,
	PlayerInputActions_System_Collections_IEnumerable_GetEnumerator_mA49C8C12B15C75E1FEC7C13E6C76443B2453DCD7,
	PlayerInputActions_Enable_m63290BD2AF7F29D6BB28434A8B8397CCFF191B7B,
	PlayerInputActions_Disable_m145EF8F010BB48B35AAB7B8ACED9A091AEE1AFFE,
	PlayerInputActions_get_bindings_m23A1C9398C0BA879FEF39DADD2941C1CE0984580,
	PlayerInputActions_FindAction_mBDE03511C5CE4157FBE35DFAA4EE4DB0F1C693C2,
	PlayerInputActions_FindBinding_m5A4EE085D5819B6B5F876563BEC5C3D967140F8A,
	PlayerInputActions_get_Player_m6AFEFAFDC04F51E55FAC8DDBB08E1BC0A9DE191C,
	PlayerInputActions_get_KeyboardScheme_m6F5FC55D8176657A319D25714A344276180ED82F,
	PlayerInputActions_get_VRRemoteScheme_mA06D43460A91797E02D7B83A0AB7694029BF7554,
	PlayerActions__ctor_mA82998F5F59D711B3E77A4C33CC57DEA2E56C922,
	PlayerActions_get_Movement_m5A62A044C81C7DB5E195C190921FEBC6BCEEE36E,
	PlayerActions_get_LeftSignal_m3041015B4B70CFF200743CAD76FDBD4AA540F893,
	PlayerActions_get_RightSignal_m5D1761A00CF7BD1EE259FF1CCFC34C61ECA0251C,
	PlayerActions_get_Hazard_m01ADACC86D1AEC9015F50AE5438012A528FBF671,
	PlayerActions_get_GoForward_mF225072B61E4AFA792A4CCB08FA85B1D29A54E9F,
	PlayerActions_get_GoBack_m031A35BA62DA0BC100C8934740895B92CE1A2582,
	PlayerActions_get_GoLeft_m5C06165991CD3DE1E7539FFB469CF952F083E381,
	PlayerActions_get_GoRight_mBE49CEE3C1A6F3D5F65118699AD445672F90EE7A,
	PlayerActions_Get_mB3B84313C429B36C7B320015F73D1626157CB0D2,
	PlayerActions_Enable_mC7ABF14577F597D6CCDFA8AFC5DD2E83D64BBDDC,
	PlayerActions_Disable_m1CFEAAAF7D99E7F9C27E0E5753489D78D12AFC80,
	PlayerActions_get_enabled_m9F9A99BE205EF1D798C5915E2C74CF071F288AC6,
	PlayerActions_op_Implicit_mB73E8471CF539BAA38BD4CC528382843A1597308,
	PlayerActions_SetCallbacks_m22E60CE0193714BF83FF1D2F6EDB57C98776E80E,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	AIController_Update_mC1B4E8DE676F4EDFD8E3296F5AE6EB8C7D6E73F7,
	AIController__ctor_m28A6370E8C2F1F7BB0C5D75702AE025E843F28A5,
	AISelector__ctor_m4C193D53AA74524973E91602ED57144B5E6134E2,
	CarAI_Start_m96F09F072477B651ED6BFAD36A54957ED51FD624,
	CarAI_Update_mB66B03F1089174ECA4CEE436CAE0A8572539CE65,
	CarAI_OnTriggerEnter_mEEBE096339730AF8750578F9554403836919B145,
	CarAI__ctor_mDF45DCA2AADB83426B3C68A0FF47E2A4D46477B3,
	CarController_FixedUpdate_m62A2DA259A4FA667F2A78CC2701E1A4601C4A4F7,
	CarController_GetInput_m13F5FC2DF8E3708ED4E9C54D475DDF692376945F,
	CarController_HandleMotor_mE8973138355B368A9239C16DA68113B806C21DC1,
	CarController_ApplyBreaking_m8F4CED40A069C557B2A31EBBD1C1F8507E79F8D6,
	CarController_HandleSteering_mF516C45FE58FA3D4FB1A7C520B3C552DF40E9099,
	CarController_UpdateWheels_mD848267D86551E97979325D2D2BC0B1D42B9B433,
	CarController_UpdateSingleWheel_mEB40B788EFEC5FC24E4821DF96EA2071C3ACEECC,
	CarController__ctor_m180A0B729C25B52327B05362FB3A7AA7A53A9739,
	GameEndController_Won_mE7F8D4A0974C1618D69A403805CF43EC4530B8E4,
	GameEndController_Fail_mC506BDD05C982F8359ED8B69807C894AF136F0C2,
	GameEndController__ctor_m29834B0A6D2367B154EB2E792115D5DE44D0AA98,
	OpeningScenes_Open_mE5839C819BCB1E38BDF101673A7495C027B4FBAA,
	OpeningScenes__ctor_m7597E34401C0CF2BEED852E563F09A839EED7CB2,
	PersonCharacterController_Start_m542A56AEEE8D0E482D9886A5479B7D4C0F41B68D,
	PersonCharacterController_Update_mF6F11F766BF3240BE44E27100208A7546354881A,
	PersonCharacterController_OnTriggerEnter_m2AE97A0C8487DBB3C5F97592B85D0568170CEFE9,
	PersonCharacterController__ctor_m65A918E99333B6E62A6410191FC2700EE826D396,
	RoadDecider_SelectNext_m49FF1483B105A01F9D2AF8E0387A1854977AE024,
	RoadDecider_GetDirection_m4BCF3158D4EBE87BAE8218C3ECCCD81C463D04EB,
	RoadDecider__ctor_mCB62EFE2AF8CC081B247335F47521F9AF9AD57BA,
	SignalsCar_Start_m0C3F63BAC47BB0B568F6D09A6A1AC7408D342DE3,
	SignalsCar_Update_m5F86490653B6972F72DB5C93151DB92E83D73A78,
	SignalsCar_FixedUpdate_m53D3C162FCC658477FFBBD9DFF7732DC7273C1FA,
	SignalsCar_SetLeft_m2126FAEC51BCDBE099389C70D52003038AF0F1C1,
	SignalsCar_SetRight_m674EDE4006B4A9A474C425E47921FBD50EE84E88,
	SignalsCar_SetHazard_mB9751516CAAB2B765CB8ACD564D1F7B8208CC1B7,
	SignalsCar_DisableAll_mB701170D1C5821270CAF681762AC0C2D41888476,
	SignalsCar_ChangeCamera_mD1BA0AEFA1D87E3B3B624A002A7AF36D5B8C58C0,
	SignalsCar__ctor_mF51427FDDD2C0C845256C607AC6BDCC48DD56830,
	TrafficLight_Start_m4FD84738A302301C102E99D70DAA5683F1AFA3CA,
	TrafficLight_FixedUpdate_m390DDB233E7A4E1283355F2E4250EE3A048E6C91,
	TrafficLight_AllowCars_m180D31929E4469F1452F21492C1CA1848F8C5EA4,
	TrafficLight_AllowWalkers_m8030286CDE1BF3ED95A4E732824A9BCD816207A5,
	TrafficLight_setOtherLights_mBF9A30E0D034D5546D5D7BBE2DFBE4715E51C056,
	TrafficLight_Car_RedToYellow_m7D484C13BEC714E16F6CDFAE3AD5759D80FA2AFE,
	TrafficLight_Car_YellowToGreen_m1D84BBF573031A16531382C92181A814258EB904,
	TrafficLight_Car_GreenToYellow_mD499CC5D3D37CBC479C55FF4D116A742BB013937,
	TrafficLight_Car_YellowToRed_mE638B0892F0DF1871F86EBFB5096303ABCC8F698,
	TrafficLight_Walker_GreenToRed_mEB3409FEB0DE10F130BFEB3AF2614B17BE20B5C3,
	TrafficLight_Walker_RedToGreen_mF769D2FA12BC683857BD8B147B1D4A4C2A45C01E,
	TrafficLight__ctor_m25FBA1B06317E68A42625CD73E906BBE288896AF,
	TrafficLights_Start_mA212235A87B9A7406D3BAF66EFBC9C84F5FD6565,
	TrafficLights_Update_mA82E8D2FF73196F0906E1898E48051C4DEF2E86D,
	TrafficLights__ctor_m59AC945FF0A2BE5A91346B658650D49DDBFB7171,
	TrafficRules_Start_m958F3995E85CCDFAE511CC388DFE026F7E2A9917,
	TrafficRules_Update_m5387BDBE3C58500D9FD50F3A9DB3762AF8B413D9,
	TrafficRules__ctor_mC2C98984230F9A8CDF1C08CBBCB04B6F2511E8EE,
	TrafficRulesController_Car_Start_m23E16B8E1CB041431FF804B61A5F461CAAB06F7C,
	TrafficRulesController_Car_Update_mC0C047BE67F31D6341A085E8EBB6CB0AA1374D48,
	TrafficRulesController_Car_OnTriggerEnter_mC17E3FCD3BC37D6A04F22ED9B2231ABDA38A7F07,
	TrafficRulesController_Car__ctor_m84E57F2020FD1F32910A5F4D2718E9DB5D8DD7BA,
	VRControls_Start_m61878CF076A16DB281675DB573D82BFFCDC1F3A0,
	VRControls_Update_m2F68B0220A566B6AE6826C076A1649169D3272A7,
	VRControls_goForward_m49A5138744CCFBFEE8F0C47B051A9E7BD685E05B,
	VRControls_go_mDB1A92E9287135C174899FF40DE979D4681668EC,
	VRControls_SimulateKeyPress_m42F3AD2DAACED561F00377A6F9732B1A3AE1DC51,
	VRControls_goBack_mBD3DF537AAF81CE04C8C8981D2F43F81A203AC5E,
	VRControls_turnLeft_m6BA11C70EF91EA2169CA87D88AEEBA28C5EF247A,
	VRControls_turnRight_mDD5DA2BD8701F3BE186DAF339FC1CFDE32DE44A6,
	VRControls_rightSignal_mCBA6A76D5C72DB44363E15BC22BE2A694284F66D,
	VRControls_leftSignal_m7599CC8A2EC173EDF8C801BBEDC00343B7387391,
	VRControls__ctor_m3FE3DB24192EB9070E0C6E21AC01443E50BD669D,
	U3CSimulateKeyPressU3Ed__8__ctor_mB333C0FA66B2395C19496CC79C6100503A0633BF,
	U3CSimulateKeyPressU3Ed__8_System_IDisposable_Dispose_m6B6AA45F2BFEA8845A8F47384DAECB7A55DC4FC9,
	U3CSimulateKeyPressU3Ed__8_MoveNext_m9AFFCD72905EC5A46322EB22ECBD633B3FEF1ED5,
	U3CSimulateKeyPressU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFB7AE2BA8C02D3857C2599A9D8B6389319488E07,
	U3CSimulateKeyPressU3Ed__8_System_Collections_IEnumerator_Reset_m5A0CAFC39F155EEFA816F434F43DEBC933C7FD10,
	U3CSimulateKeyPressU3Ed__8_System_Collections_IEnumerator_get_Current_m8D210BFBFCCE65F373299DC960D8D4FE87D5C0EE,
	WalkerWay_Start_m09F719DB74AEF1231B80CF9F74FCCDE112814F09,
	WalkerWay_Update_mEA24C4F9627A504746142EF6B0A78EBD639FD4B3,
	WalkerWay__ctor_mC5A4AB307D8BD3A259E13A53E1BDBDF61810E402,
	MobileDisableAutoSwitchControls__ctor_m7E0E6D5F71FAC813E427F2E19ACBCAA8F0CAF94A,
	UIVirtualButton_OnPointerDown_m9B8BBCAB64E2E6799E23311EAB29104788AF5607,
	UIVirtualButton_OnPointerUp_m8928883C1D8EEB4E6BD673089D61C61C15FD6E26,
	UIVirtualButton_OnPointerClick_m8C77F496BC858EC51F8BDA3759219187DB267A3F,
	UIVirtualButton_OutputButtonStateValue_m4D03DEE754B7C8716A55432137E088D08626C8A8,
	UIVirtualButton_OutputButtonClickEvent_m52515C61D5E82F76620D03994ED97A1F34CBA6F5,
	UIVirtualButton__ctor_mC6B209AB41BF45B6C0B9B30278CDD72DA776983A,
	BoolEvent__ctor_m4D8C9EEAB13A21F9D68284FB7359E58E0EAD03E5,
	Event__ctor_m8A0990FCB8628C5987E39013E0316961170DF9AC,
	UIVirtualJoystick_Start_m67CF1EE6291EACAF0C3A1B0DB535BD525EA479F4,
	UIVirtualJoystick_SetupHandle_m0ACFFD29C51B77207E694FD86F4A84338B030207,
	UIVirtualJoystick_OnPointerDown_mEC473003FA53CFFFBD478CD5E1A3BA3509AE1E12,
	UIVirtualJoystick_OnDrag_m4AB1A15DA5C6DB622E55C21E6E7ED263FED7DB77,
	UIVirtualJoystick_OnPointerUp_m0FAFE18C832E0C2A30ACA560276C017A59863614,
	UIVirtualJoystick_OutputPointerEventValue_mD4C7421AB87B90625945F3B8B1F58F611492E64A,
	UIVirtualJoystick_UpdateHandleRectPosition_m316CA3567F5830F9F19C381F20FEF41084740B94,
	UIVirtualJoystick_ApplySizeDelta_m62A3D273C1B2063E387278930C27828AAAA3D706,
	UIVirtualJoystick_ClampValuesToMagnitude_m8B710D2ABFEAB67917267BE7B2E253A5C572907D,
	UIVirtualJoystick_ApplyInversionFilter_mD507E46BD99810E9B18F0AD4BDD843FACF70536B,
	UIVirtualJoystick_InvertValue_m51B67357EC3DF48051879F1DA61B76F4A4BBD9F7,
	UIVirtualJoystick__ctor_m3EFA0B88827F62A24F2BFE15B2A782256CFB05F1,
	Event__ctor_m55DEBAA169467EF82E82EA7541576F59539139B7,
	UIVirtualTouchZone_Start_mA98396B89C28AC97493CB9A80630F0459C7C9350,
	UIVirtualTouchZone_SetupHandle_m46D4991D16E65721ED9A06C538841C45A728151D,
	UIVirtualTouchZone_OnPointerDown_mCFB81700E80EAD8CB2402BC894448E8CFF7434E4,
	UIVirtualTouchZone_OnDrag_mAA2371D1A929E0EB173141433B05D2E496433B37,
	UIVirtualTouchZone_OnPointerUp_mE65CFD342E3EFCCD3C0ED8596C845E9020042B05,
	UIVirtualTouchZone_OutputPointerEventValue_m1ECAF819A22591A0241D790EFC936ECB3985CE68,
	UIVirtualTouchZone_UpdateHandleRectPosition_m4D0B42E5C70893DFB5408E623076C9F5B7FE3A7A,
	UIVirtualTouchZone_SetObjectActiveState_mDB5AA4C0AB194A0009D681F773145E68CB678699,
	UIVirtualTouchZone_GetDeltaBetweenPositions_mA12C35395F0319B30B47A5791759AB8FE240186A,
	UIVirtualTouchZone_ClampValuesToMagnitude_m2B1CBB4A20BE4004AD520E5EC427C664E9315EFC,
	UIVirtualTouchZone_ApplyInversionFilter_m8FEABDE5FD0BF979E96732DCF3123203E6B755E3,
	UIVirtualTouchZone_InvertValue_m5B003E6987A1518CEB604D6B1796A58DE5D04E4A,
	UIVirtualTouchZone__ctor_m74665B8EF96FBD3D7C35B545E662FEE10C11A2A8,
	Event__ctor_mE60A25521698C4C733D32C1D85667315E23BEE77,
	BasicRigidBodyPush_OnControllerColliderHit_m32CA56C895E52B8AEC6A383197F4F3B9CF52155D,
	BasicRigidBodyPush_PushRigidBodies_mE4B69524BF1D9A657193F0032E95F9A5F806FF27,
	BasicRigidBodyPush__ctor_mD1FBA2CCB9BB6CD5A9A5768884649B55672420E1,
	Readme__ctor_mF465410C5B2E598F2685E82CFCE1F42186AFF448,
	Section__ctor_mBAD5262A353BC071C61B8DB462A3D4D5AB5C7C4E,
	StarterAssetsInputs_OnMove_mC62E022C4B95BC8A3B9932C2F8EFCE91A38B88FA,
	StarterAssetsInputs_OnLook_m7A5EE56C246489C21CF21A0B66AA0AA44B8CC35C,
	StarterAssetsInputs_OnJump_m142391E84B470F4AFDE6741C29AB922B9EA3791B,
	StarterAssetsInputs_OnSprint_mFCCCAD7A3EAEFC6C7D71AE4EE31CFFEF1D87BEA1,
	StarterAssetsInputs_MoveInput_m17B8532D1DB7D870295DE9E82CBC17735F8984F8,
	StarterAssetsInputs_LookInput_m3B98228935C1AD2E76356732A798A3EFD8CA4186,
	StarterAssetsInputs_JumpInput_mEEC05639651E0D3AD679B923D34C3B58F52EC722,
	StarterAssetsInputs_SprintInput_m4A4EB5B45A168786D74E233577623FC38D74E4F3,
	StarterAssetsInputs_OnApplicationFocus_m0B8F80F47202B1072318C9DE58C5C1D3F2EAFF28,
	StarterAssetsInputs_SetCursorState_m693E19396F2F0ECB2FB051410EEB72DC1B037DD0,
	StarterAssetsInputs__ctor_mACF3B3A6FB6B023BDBA341E65079F409777BD67B,
	UICanvasControllerInput_VirtualMoveInput_m3DCA73AB02D0D9D4AF69809D2EA9A388A9DD2D20,
	UICanvasControllerInput_VirtualLookInput_mDCD0EF95A5FD1C0351C6EF330F0E21A2BC5B3BD7,
	UICanvasControllerInput_VirtualJumpInput_m4BF9A688729DE9CAB0368373B4508833CBAF158A,
	UICanvasControllerInput_VirtualSprintInput_mC1DD56717CE777F772956FF98DA099A0C0BFD9B3,
	UICanvasControllerInput__ctor_mBBEE562E79321A1939A2B006E6654FA85259EDEB,
	ThirdPersonController_get_IsCurrentDeviceMouse_mFD1AE87B85178F91109581E5F86C8BDB1FD9773E,
	ThirdPersonController_Awake_m982FE30F7B3D80F71E2BB40E5E9C85266840D98C,
	ThirdPersonController_Start_mDF1E5FFEF57D1CD6BCC3A25993F99F5ECD673C43,
	ThirdPersonController_Update_m6A729BE364E703138033EE4C7247074BC13C6F9E,
	ThirdPersonController_LateUpdate_m1F8928BBA64078F5E8062FCE4A9EA6A2C504750F,
	ThirdPersonController_AssignAnimationIDs_mB8641B310C6DDEC521D5401B691A156B730C637A,
	ThirdPersonController_GroundedCheck_mB9F085D761FAD5805B651EC2F3466B240D94964A,
	ThirdPersonController_CameraRotation_m389620971F720216A6BF133DFB0845BACD4866B4,
	ThirdPersonController_Move_m0444969DC5E148B2093542C8F31B22DF98D95B6D,
	ThirdPersonController_JumpAndGravity_mF8ADE69C505E77DFB9A7A187A2E914F4D0390514,
	ThirdPersonController_ClampAngle_mFAEB55D93C154C7523412A3287C3A957173E7455,
	ThirdPersonController_OnDrawGizmosSelected_m23A137AF678E2C538238940AE45FBB2856E73AE2,
	ThirdPersonController_OnFootstep_m2D51C8C4A6A189DBCF516409DECF9828157A6051,
	ThirdPersonController_OnLand_m038EF8FE1B2F9D41B1E624B40741D1C12FEE7473,
	ThirdPersonController__ctor_m1A6595F81F751D300BAFBDAD5A3F8B4B1E339391,
};
extern void PlayerActions__ctor_mA82998F5F59D711B3E77A4C33CC57DEA2E56C922_AdjustorThunk (void);
extern void PlayerActions_get_Movement_m5A62A044C81C7DB5E195C190921FEBC6BCEEE36E_AdjustorThunk (void);
extern void PlayerActions_get_LeftSignal_m3041015B4B70CFF200743CAD76FDBD4AA540F893_AdjustorThunk (void);
extern void PlayerActions_get_RightSignal_m5D1761A00CF7BD1EE259FF1CCFC34C61ECA0251C_AdjustorThunk (void);
extern void PlayerActions_get_Hazard_m01ADACC86D1AEC9015F50AE5438012A528FBF671_AdjustorThunk (void);
extern void PlayerActions_get_GoForward_mF225072B61E4AFA792A4CCB08FA85B1D29A54E9F_AdjustorThunk (void);
extern void PlayerActions_get_GoBack_m031A35BA62DA0BC100C8934740895B92CE1A2582_AdjustorThunk (void);
extern void PlayerActions_get_GoLeft_m5C06165991CD3DE1E7539FFB469CF952F083E381_AdjustorThunk (void);
extern void PlayerActions_get_GoRight_mBE49CEE3C1A6F3D5F65118699AD445672F90EE7A_AdjustorThunk (void);
extern void PlayerActions_Get_mB3B84313C429B36C7B320015F73D1626157CB0D2_AdjustorThunk (void);
extern void PlayerActions_Enable_mC7ABF14577F597D6CCDFA8AFC5DD2E83D64BBDDC_AdjustorThunk (void);
extern void PlayerActions_Disable_m1CFEAAAF7D99E7F9C27E0E5753489D78D12AFC80_AdjustorThunk (void);
extern void PlayerActions_get_enabled_m9F9A99BE205EF1D798C5915E2C74CF071F288AC6_AdjustorThunk (void);
extern void PlayerActions_SetCallbacks_m22E60CE0193714BF83FF1D2F6EDB57C98776E80E_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[14] = 
{
	{ 0x06000040, PlayerActions__ctor_mA82998F5F59D711B3E77A4C33CC57DEA2E56C922_AdjustorThunk },
	{ 0x06000041, PlayerActions_get_Movement_m5A62A044C81C7DB5E195C190921FEBC6BCEEE36E_AdjustorThunk },
	{ 0x06000042, PlayerActions_get_LeftSignal_m3041015B4B70CFF200743CAD76FDBD4AA540F893_AdjustorThunk },
	{ 0x06000043, PlayerActions_get_RightSignal_m5D1761A00CF7BD1EE259FF1CCFC34C61ECA0251C_AdjustorThunk },
	{ 0x06000044, PlayerActions_get_Hazard_m01ADACC86D1AEC9015F50AE5438012A528FBF671_AdjustorThunk },
	{ 0x06000045, PlayerActions_get_GoForward_mF225072B61E4AFA792A4CCB08FA85B1D29A54E9F_AdjustorThunk },
	{ 0x06000046, PlayerActions_get_GoBack_m031A35BA62DA0BC100C8934740895B92CE1A2582_AdjustorThunk },
	{ 0x06000047, PlayerActions_get_GoLeft_m5C06165991CD3DE1E7539FFB469CF952F083E381_AdjustorThunk },
	{ 0x06000048, PlayerActions_get_GoRight_mBE49CEE3C1A6F3D5F65118699AD445672F90EE7A_AdjustorThunk },
	{ 0x06000049, PlayerActions_Get_mB3B84313C429B36C7B320015F73D1626157CB0D2_AdjustorThunk },
	{ 0x0600004A, PlayerActions_Enable_mC7ABF14577F597D6CCDFA8AFC5DD2E83D64BBDDC_AdjustorThunk },
	{ 0x0600004B, PlayerActions_Disable_m1CFEAAAF7D99E7F9C27E0E5753489D78D12AFC80_AdjustorThunk },
	{ 0x0600004C, PlayerActions_get_enabled_m9F9A99BE205EF1D798C5915E2C74CF071F288AC6_AdjustorThunk },
	{ 0x0600004E, PlayerActions_SetCallbacks_m22E60CE0193714BF83FF1D2F6EDB57C98776E80E_AdjustorThunk },
};
static const int32_t s_InvokerIndices[236] = 
{
	3530,
	3530,
	3530,
	3530,
	3530,
	3530,
	3530,
	3530,
	3530,
	3530,
	3530,
	3530,
	3530,
	3530,
	3530,
	3530,
	3530,
	3530,
	3530,
	3530,
	3530,
	3530,
	3530,
	3530,
	3530,
	3530,
	3530,
	3530,
	3530,
	3530,
	3530,
	3530,
	3530,
	3530,
	1525,
	765,
	3443,
	3443,
	5440,
	429,
	5004,
	5004,
	3530,
	5616,
	3466,
	3530,
	3530,
	3339,
	2651,
	3337,
	2649,
	3349,
	2331,
	3466,
	3466,
	3530,
	3530,
	3466,
	1013,
	894,
	3619,
	3432,
	3432,
	2745,
	3466,
	3466,
	3466,
	3466,
	3466,
	3466,
	3466,
	3466,
	3466,
	3530,
	3530,
	3495,
	5358,
	2745,
	2841,
	2841,
	2841,
	2841,
	2841,
	2841,
	2841,
	2841,
	3530,
	3530,
	3530,
	3530,
	3530,
	2745,
	3530,
	3530,
	3530,
	3530,
	3530,
	3530,
	3530,
	1525,
	3530,
	2745,
	2745,
	3530,
	2725,
	3530,
	3530,
	3530,
	2745,
	3530,
	3466,
	3443,
	3530,
	3530,
	3530,
	3530,
	3530,
	3530,
	3530,
	3530,
	3530,
	3530,
	3530,
	3530,
	3530,
	3530,
	3530,
	3530,
	3530,
	3530,
	3530,
	3530,
	3530,
	3530,
	3530,
	3530,
	3530,
	3530,
	3530,
	3530,
	3530,
	3530,
	2745,
	3530,
	3530,
	3530,
	2841,
	3530,
	3466,
	2841,
	2841,
	2841,
	2841,
	2841,
	3530,
	2725,
	3530,
	3495,
	3466,
	3530,
	3466,
	3530,
	3530,
	3530,
	3530,
	2745,
	2745,
	2745,
	2768,
	3530,
	3530,
	3530,
	3530,
	3530,
	3530,
	2745,
	2745,
	2745,
	2794,
	2794,
	2552,
	2552,
	2552,
	2509,
	3530,
	3530,
	3530,
	3530,
	2745,
	2745,
	2745,
	2794,
	2794,
	1529,
	1215,
	2552,
	2552,
	2509,
	3530,
	3530,
	2745,
	2745,
	3530,
	3530,
	3530,
	2745,
	2745,
	2745,
	2745,
	2794,
	2794,
	2768,
	2768,
	2768,
	2768,
	3530,
	2794,
	2794,
	2768,
	2768,
	3530,
	3495,
	3530,
	3530,
	3530,
	3530,
	3530,
	3530,
	3530,
	3530,
	3530,
	4496,
	3530,
	2745,
	2745,
	3530,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	236,
	s_methodPointers,
	14,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
