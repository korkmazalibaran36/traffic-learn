﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>


struct VirtualActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
struct InterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct InterfaceActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R>
struct InterfaceFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R, typename T1>
struct InterfaceFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};

// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>
struct Dictionary_2_t7A371082270CA8FD6BC79B5622CCD5E1C87A6E42;
// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,System.Boolean>
struct Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590;
// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,UnityEngine.InputSystem.Utilities.InternedString>
struct Func_2_tB181F4C9A24C0634DF4A10CFBFFE6B45B3796BBC;
// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,System.Object>
struct Func_2_t711512467B454C91EB8F3FB19CD6D141AAB5B6B1;
// System.Func`2<UnityEngine.InputSystem.Utilities.InternedString,System.Boolean>
struct Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1;
// System.Func`2<UnityEngine.InputSystem.Utilities.InternedString,UnityEngine.InputSystem.Utilities.InternedString>
struct Func_2_t1AF155D27A4D78882342D697E4A6DE7CE594545B;
// System.Func`2<UnityEngine.InputSystem.Utilities.InternedString,System.Object>
struct Func_2_tA2BD34BB8D97AD282016BD4CB5B8675E4AF85B45;
// System.Func`2<UnityEngine.InputSystem.Utilities.NameAndParameters,System.Boolean>
struct Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02;
// System.Func`2<UnityEngine.InputSystem.Utilities.NameAndParameters,UnityEngine.InputSystem.Utilities.InternedString>
struct Func_2_tEA2235809AB0E1AB8425745179691D76B519B0C5;
// System.Func`2<UnityEngine.InputSystem.Utilities.NameAndParameters,System.Object>
struct Func_2_tF7304610FB007335AC870B51C8FE2092628DC0DB;
// System.Func`2<UnityEngine.InputSystem.Utilities.NamedValue,System.Boolean>
struct Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5;
// System.Func`2<UnityEngine.InputSystem.Utilities.NamedValue,UnityEngine.InputSystem.Utilities.InternedString>
struct Func_2_tA21678F1D9F9F4617D645755285D28EDE5F9AE44;
// System.Func`2<UnityEngine.InputSystem.Utilities.NamedValue,System.Object>
struct Func_2_t5B101C0B26ADAA3B1A77BC50D51CC42364E5409E;
// System.Func`2<System.Object,System.Boolean>
struct Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8;
// System.Func`2<System.Object,UnityEngine.InputSystem.Utilities.InternedString>
struct Func_2_t964229B6ADD2B008268547ED2BC3C090AF218065;
// System.Func`2<System.Object,System.Object>
struct Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436;
// System.Func`2<UnityEngine.UIElements.StyleSelectorPart,System.Boolean>
struct Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC;
// System.Func`2<UnityEngine.UIElements.StyleSelectorPart,UnityEngine.InputSystem.Utilities.InternedString>
struct Func_2_tFBBF89B72C697489D82FBE8FF822B191D2EE9A41;
// System.Func`2<UnityEngine.UIElements.StyleSelectorPart,System.Object>
struct Func_2_t43979C5A872BA9ED7A923D533A01683A98D71957;
// System.Func`2<UnityEngine.InputSystem.Utilities.Substring,System.Boolean>
struct Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9;
// System.Func`2<UnityEngine.InputSystem.Utilities.Substring,UnityEngine.InputSystem.Utilities.InternedString>
struct Func_2_t14F6F4D7FBF558A20782EDC48D80473E4800048E;
// System.Func`2<UnityEngine.InputSystem.Utilities.Substring,System.Object>
struct Func_2_t4EC2F095EDDC8079537CA0FEDC463635E599C9E4;
// System.Func`2<UnityEngine.InputSystem.Layouts.InputControlLayout/ControlItem,System.Boolean>
struct Func_2_t6D3B67831BA6BBB7266E343D7EAEFF60C17F92DF;
// System.Func`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,System.Boolean>
struct Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847;
// System.Func`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,UnityEngine.InputSystem.Utilities.InternedString>
struct Func_2_t48DD1C8E439811C492BAFE220A679DB57D7C9370;
// System.Func`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,System.Object>
struct Func_2_t1628B2E4EEC7A32D5B688E54415BC42EDA9D2899;
// System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>>
struct IEnumerable_1_t4DAB7002E0E04175D7AB0251D5A8284277618627;
// System.Collections.Generic.IEnumerable`1<UnityEngine.InputSystem.Utilities.InternedString>
struct IEnumerable_1_t8A4B908F54ED8D008831DEF6F9A949FFA981E6BB;
// System.Collections.Generic.IEnumerable`1<UnityEngine.InputSystem.Utilities.NameAndParameters>
struct IEnumerable_1_tE4E960C53664FB9663CC2261AEBCA61C5EDB579D;
// System.Collections.Generic.IEnumerable`1<UnityEngine.InputSystem.Utilities.NamedValue>
struct IEnumerable_1_t3E9A1977DB64BEFC51E9B02E141FFD28CCCAFF2B;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t52B1AC8D9E5E1ED28DF6C46A37C9A1B00B394F9D;
// System.Collections.Generic.IEnumerable`1<UnityEngine.UIElements.StyleSelectorPart>
struct IEnumerable_1_tAE15EC60A2550CD3A4D9E7B9C35B23B6CCD16593;
// System.Collections.Generic.IEnumerable`1<UnityEngine.InputSystem.Utilities.Substring>
struct IEnumerable_1_t316B1044EDC4315DB26041243EABD2C79E26C97D;
// System.Collections.Generic.IEnumerable`1<UnityEngine.InputSystem.Layouts.InputControlLayout/ControlItem>
struct IEnumerable_1_tE926868C9A8FA40AFDD7B21D280482F8418172FB;
// System.Collections.Generic.IEnumerable`1<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>
struct IEnumerable_1_tDECAFAC2EA60F65D233E35C09DC8E49592E90780;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>>
struct IEnumerator_1_tFBEC9463ED361F2291493660A5217929F0BD545B;
// System.Collections.Generic.IEnumerator`1<UnityEngine.InputSystem.Utilities.InternedString>
struct IEnumerator_1_tB0283BFB1D45052C90A9ACF8B98A8B1B22B5E19B;
// System.Collections.Generic.IEnumerator`1<UnityEngine.InputSystem.Utilities.NameAndParameters>
struct IEnumerator_1_t11A4DD8C8A897B31E9F14F85734A35486782D576;
// System.Collections.Generic.IEnumerator`1<UnityEngine.InputSystem.Utilities.NamedValue>
struct IEnumerator_1_t06B2E5EA7C5F4B43FEEC71A595B1517272B448FD;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t2DC97C7D486BF9E077C2BC2E517E434F393AA76E;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UIElements.StyleSelectorPart>
struct IEnumerator_1_t4EA59F9AA3E64DD7BA460CC086E09077A46EE5B1;
// System.Collections.Generic.IEnumerator`1<UnityEngine.InputSystem.Utilities.Substring>
struct IEnumerator_1_t89ADD2D5401AA240C4CE71F52C93E029D4B57F00;
// System.Collections.Generic.IEnumerator`1<UnityEngine.InputSystem.Layouts.InputControlLayout/ControlItem>
struct IEnumerator_1_t325A269851EE29BBCD603956D42482A3140C1BC6;
// System.Collections.Generic.IEnumerator`1<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>
struct IEnumerator_1_tDB5F036395ADFF08AD1585AC3A7BE899CF28B360;
// System.IObservable`1<System.Object>
struct IObservable_1_tD298C95FFB6061193313830292FE8E817D37B5A4;
// System.IObserver`1<System.Object>
struct IObserver_1_t9EBB98F865B275FC177A4A094F9E254B58B8A962;
// System.Linq.Enumerable/Iterator`1<UnityEngine.InputSystem.Utilities.InternedString>
struct Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5;
// System.Linq.Enumerable/Iterator`1<System.Object>
struct Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279;
// System.Linq.Enumerable/Iterator`1<UnityEngine.InputSystem.Layouts.InputControlLayout/ControlItem>
struct Iterator_1_t2711EA98CD5C8E6E87A241C66556EFDB99842473;
// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>>
struct List_1_t9B1CABCBD4B6B7781E348FF332B32B333B543BA8;
// System.Collections.Generic.List`1<UnityEngine.InputSystem.Utilities.InternedString>
struct List_1_tFD37D052760A236906215AF581C58EE13DE6A5DC;
// System.Collections.Generic.List`1<UnityEngine.InputSystem.Utilities.NameAndParameters>
struct List_1_tCBF5EB99F2E538DCB47A8C835DD4B9C024879393;
// System.Collections.Generic.List`1<UnityEngine.InputSystem.Utilities.NamedValue>
struct List_1_tDC9592305A8D24F0DBC4A23F1719292FCB2887C1;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5;
// System.Collections.Generic.List`1<UnityEngine.UIElements.StyleSelectorPart>
struct List_1_tE7DC2F192C483F9E5722779E3C2C1910490AAE59;
// System.Collections.Generic.List`1<UnityEngine.InputSystem.Utilities.Substring>
struct List_1_tEAD6E3282E028927B32F56E7892994D90D512467;
// System.Collections.Generic.List`1<UnityEngine.InputSystem.Layouts.InputControlLayout/ControlItem>
struct List_1_tB7B59A258A4A84FCB5A2CC59F934C8B230EA963F;
// System.Collections.Generic.List`1<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>
struct List_1_tEC0B302C3EF23400C3EC92ED7672A715CA602068;
// UnityEngine.InputSystem.Utilities.WhereObservable`1/Where<System.Object>
struct Where_t5326AC9C3D27C847981552AE1DD31844DAB40D4D;
// System.Linq.Enumerable/WhereArrayIterator`1<System.Object>
struct WhereArrayIterator_1_t7D84D638EB94F5CC3BE1B29D8FC781CA8CD15A86;
// System.Linq.Enumerable/WhereArrayIterator`1<UnityEngine.InputSystem.Layouts.InputControlLayout/ControlItem>
struct WhereArrayIterator_1_t27BA96004BE53807B1028D1D8BFA586A91E96030;
// System.Linq.Enumerable/WhereEnumerableIterator`1<UnityEngine.InputSystem.Utilities.InternedString>
struct WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B;
// System.Linq.Enumerable/WhereEnumerableIterator`1<System.Object>
struct WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0;
// System.Linq.Enumerable/WhereEnumerableIterator`1<UnityEngine.InputSystem.Layouts.InputControlLayout/ControlItem>
struct WhereEnumerableIterator_1_t9A9C4F1C2DF243CCF60AE823D966E68DD70188D0;
// System.Linq.Enumerable/WhereListIterator`1<System.Object>
struct WhereListIterator_1_t42618389DB998070E03A982D15FA39BCA1DB56BD;
// System.Linq.Enumerable/WhereListIterator`1<UnityEngine.InputSystem.Layouts.InputControlLayout/ControlItem>
struct WhereListIterator_1_t578D3A8880FE8DFFE9A8B7462A4D3004DEDB1FD0;
// UnityEngine.InputSystem.Utilities.WhereObservable`1<System.Object>
struct WhereObservable_1_t36D963808BB5D3A64EB26C52708AD22FCEAF8B3C;
// System.Linq.Enumerable/WhereSelectArrayIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectArrayIterator_2_tB9FDF4ABA25271C5465121B8CD088BB04D7A03F9;
// System.Linq.Enumerable/WhereSelectArrayIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,System.Object>
struct WhereSelectArrayIterator_2_tEC7D2E75D2D8A9E1AA3D6F21AA9CBCB87EBD6617;
// System.Linq.Enumerable/WhereSelectArrayIterator`2<UnityEngine.InputSystem.Utilities.InternedString,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectArrayIterator_2_t45B551CB9059F02D47B0F25BE9ECC3561DF4A23D;
// System.Linq.Enumerable/WhereSelectArrayIterator`2<UnityEngine.InputSystem.Utilities.InternedString,System.Object>
struct WhereSelectArrayIterator_2_t297D221F6AEFE060ABD891ACCBE86FD625D9F687;
// System.Linq.Enumerable/WhereSelectArrayIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectArrayIterator_2_tA56FBB916D6D4ED801C2818530D724C0C701D490;
// System.Linq.Enumerable/WhereSelectArrayIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,System.Object>
struct WhereSelectArrayIterator_2_t8E1E4D86BDBA319DD5FB2D615DE93CAE92D68506;
// System.Linq.Enumerable/WhereSelectArrayIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectArrayIterator_2_t81C380953EA0951A36339AC8CE79A9B039372941;
// System.Linq.Enumerable/WhereSelectArrayIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,System.Object>
struct WhereSelectArrayIterator_2_tFFAF318BF43576D4F2880331F0ECAB9792119697;
// System.Linq.Enumerable/WhereSelectArrayIterator`2<System.Object,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectArrayIterator_2_t2A766812BE346746DE135AD9B855143B4FF483E4;
// System.Linq.Enumerable/WhereSelectArrayIterator`2<System.Object,System.Object>
struct WhereSelectArrayIterator_2_tA706D5B1608A9A8F1BF43C6E5D9D682C901DB244;
// System.Linq.Enumerable/WhereSelectArrayIterator`2<UnityEngine.UIElements.StyleSelectorPart,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectArrayIterator_2_t0EBFFCD87F6BDF27E6B0AB944A8106A0C0ABC3AE;
// System.Linq.Enumerable/WhereSelectArrayIterator`2<UnityEngine.UIElements.StyleSelectorPart,System.Object>
struct WhereSelectArrayIterator_2_t49E50A395D6004220E3A7ACD1AFFD129393B098E;
// System.Linq.Enumerable/WhereSelectArrayIterator`2<UnityEngine.InputSystem.Utilities.Substring,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectArrayIterator_2_t17AAB090C1BEA20967939542652510CB0E77A3FB;
// System.Linq.Enumerable/WhereSelectArrayIterator`2<UnityEngine.InputSystem.Utilities.Substring,System.Object>
struct WhereSelectArrayIterator_2_tAA2E097EE3422A3A4E4652B975AA568F28CF2C90;
// System.Linq.Enumerable/WhereSelectArrayIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectArrayIterator_2_t0255B8DCC00DD4A15730FE65749D22B88FA338F1;
// System.Linq.Enumerable/WhereSelectArrayIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,System.Object>
struct WhereSelectArrayIterator_2_tED9B99EA45685E4AB08C352736644EB4CAFC9980;
// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectEnumerableIterator_2_t5171F478E1C976F4C353EA25EC64567E44C86CA0;
// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,System.Object>
struct WhereSelectEnumerableIterator_2_t0ACED5698603E2D2B4071096A021E0FA6102F459;
// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.InternedString,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectEnumerableIterator_2_t0EB79FD57DE6939EBF78FA1D92312CB62DD3F2C6;
// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.InternedString,System.Object>
struct WhereSelectEnumerableIterator_2_t02B3FDD3CA4282B70A77A6A52D6AADC70EF74E8B;
// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectEnumerableIterator_2_t3B8EA5ED092FA87DEAECF94C466A54501B692B0B;
// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,System.Object>
struct WhereSelectEnumerableIterator_2_t9C8232C608C379652355BB860B0A6074ABF36E09;
// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectEnumerableIterator_2_t382E0095C883AA01020D10E08F7F38BBA26A3EC5;
// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,System.Object>
struct WhereSelectEnumerableIterator_2_tEBF9E61EFDABBBB02113A82648E32AC170921957;
// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Object,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectEnumerableIterator_2_tE487654732A8A2F1E3A2D57412C08036EE861F38;
// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Object,System.Object>
struct WhereSelectEnumerableIterator_2_tDAA8BFBEA68F81670F3F51C6200EBD26D7A8FBAB;
// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.UIElements.StyleSelectorPart,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectEnumerableIterator_2_tCCC50B2F34EC31A45C5FDC2C7A98279D4C71C99B;
// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.UIElements.StyleSelectorPart,System.Object>
struct WhereSelectEnumerableIterator_2_tF480AE5ADD931EEE44A713040BA622E7577211AE;
// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.Substring,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectEnumerableIterator_2_tFDFC244728E14BAC93BE9869A64080C38A0B7F1D;
// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.Substring,System.Object>
struct WhereSelectEnumerableIterator_2_t5E3BB9AFBEC27ED62FC129516C2D3E9FC81641B6;
// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectEnumerableIterator_2_t6D595CFCE85C8B8D77C138F46CCF2608E20474FE;
// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,System.Object>
struct WhereSelectEnumerableIterator_2_t4DAE42F4C708266EAC938F1C6E7FB762CDF50463;
// System.Linq.Enumerable/WhereSelectListIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectListIterator_2_tF2F8745320F9D29534105CFDF2FADABD42A2D3F7;
// System.Linq.Enumerable/WhereSelectListIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,System.Object>
struct WhereSelectListIterator_2_t15BF3EF128C005C26DE8132D49E71B3C1FE6E211;
// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.InternedString,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectListIterator_2_t7BDF9DF4ED7F42E4D8139C4BA35A0F055024799E;
// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.InternedString,System.Object>
struct WhereSelectListIterator_2_t1C93CE1377887193667292BEED953C31167E2F75;
// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectListIterator_2_t97CD9F77D3626E616F5290B44E8448C47A64E44C;
// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,System.Object>
struct WhereSelectListIterator_2_tDFC8E5CA0DE13C88A58AD7BCD17F1FC274A3F95D;
// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectListIterator_2_t84216899116C33A67088960F25CA2D6E1DDC786D;
// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,System.Object>
struct WhereSelectListIterator_2_tA4D5B2B639DCB29B4C84236B3E2651F17B522430;
// System.Linq.Enumerable/WhereSelectListIterator`2<System.Object,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectListIterator_2_t4F7AA31B4289096C5D625263B8BBDA051331BE94;
// System.Linq.Enumerable/WhereSelectListIterator`2<System.Object,System.Object>
struct WhereSelectListIterator_2_t85B78DFF0573BC95A62C79D6088FA39DFEBE1AF2;
// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.UIElements.StyleSelectorPart,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectListIterator_2_tCACD1A54916845C9EA0DA248A5D1197354F1AB99;
// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.UIElements.StyleSelectorPart,System.Object>
struct WhereSelectListIterator_2_t0F9C36BA8F331B1C4C968AB639F343F642078798;
// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.Substring,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectListIterator_2_t59101674E6772D817227A6D5C222AF34A4E0BBAD;
// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.Substring,System.Object>
struct WhereSelectListIterator_2_t0CEFF5458EED043DC70F7CC9B97B5AB4D45324EB;
// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectListIterator_2_tF2D97D886F080B26C19947C7C6A56563B86B14D7;
// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,System.Object>
struct WhereSelectListIterator_2_t2F7487B4BF5A9E71CBE06091C45AF7D98CF9F1AC;
// System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>[]
struct KeyValuePair_2U5BU5D_tC4ED6A2024348F6B2CB712DC491F7E57AEF32630;
// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Delegate[]
struct DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8;
// System.IntPtr[]
struct IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6;
// UnityEngine.InputSystem.Utilities.InternedString[]
struct InternedStringU5BU5D_t1EAE8433C8EF7C76894673801EE0EE9F6833E9A4;
// UnityEngine.InputSystem.Utilities.NameAndParameters[]
struct NameAndParametersU5BU5D_t960DDB52F44DD1231B0C626E9C859F1E0B2F5EAD;
// UnityEngine.InputSystem.Utilities.NamedValue[]
struct NamedValueU5BU5D_t16F5D30B70B5FEDA89AE16CFD61FACEC7DF85391;
// System.Object[]
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971;
// UnityEngine.UIElements.StyleSelectorPart[]
struct StyleSelectorPartU5BU5D_tD8762DADE4104C7D73D7B821DF7E5BBC87417BB7;
// UnityEngine.InputSystem.Utilities.Substring[]
struct SubstringU5BU5D_tE332884993470F5CF7BAAA950414E4E24691A8D0;
// UnityEngine.InputSystem.Layouts.InputControlLayout/ControlItem[]
struct ControlItemU5BU5D_t534859CA6A52E2EC9DDDCA52FE280E8D5CD598DB;
// UnityEngine.InputSystem.Utilities.JsonParser/JsonValue[]
struct JsonValueU5BU5D_t2E1CBAA099182C024E4921379907D707DA049098;
// System.DelegateData
struct DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288;
// System.Exception
struct Exception_t;
// System.Collections.IDictionary
struct IDictionary_t99871C56B8EC2452AC5C4CF3831695E617B89D3A;
// System.IDisposable
struct IDisposable_t099785737FC6A1E3699919A94109383715A8D807;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F;
// System.String
struct String_t;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;

IL2CPP_EXTERN_C RuntimeClass* Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;

struct KeyValuePair_2U5BU5D_tC4ED6A2024348F6B2CB712DC491F7E57AEF32630;
struct InternedStringU5BU5D_t1EAE8433C8EF7C76894673801EE0EE9F6833E9A4;
struct NameAndParametersU5BU5D_t960DDB52F44DD1231B0C626E9C859F1E0B2F5EAD;
struct NamedValueU5BU5D_t16F5D30B70B5FEDA89AE16CFD61FACEC7DF85391;
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE;
struct StyleSelectorPartU5BU5D_tD8762DADE4104C7D73D7B821DF7E5BBC87417BB7;
struct SubstringU5BU5D_tE332884993470F5CF7BAAA950414E4E24691A8D0;
struct ControlItemU5BU5D_t534859CA6A52E2EC9DDDCA52FE280E8D5CD598DB;
struct JsonValueU5BU5D_t2E1CBAA099182C024E4921379907D707DA049098;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Linq.Enumerable/Iterator`1<System.Object>
struct  Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279  : public RuntimeObject
{
public:
	// System.Int32 System.Linq.Enumerable/Iterator`1::threadId
	int32_t ___threadId_0;
	// System.Int32 System.Linq.Enumerable/Iterator`1::state
	int32_t ___state_1;
	// TSource System.Linq.Enumerable/Iterator`1::current
	RuntimeObject * ___current_2;

public:
	inline static int32_t get_offset_of_threadId_0() { return static_cast<int32_t>(offsetof(Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279, ___threadId_0)); }
	inline int32_t get_threadId_0() const { return ___threadId_0; }
	inline int32_t* get_address_of_threadId_0() { return &___threadId_0; }
	inline void set_threadId_0(int32_t value)
	{
		___threadId_0 = value;
	}

	inline static int32_t get_offset_of_state_1() { return static_cast<int32_t>(offsetof(Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279, ___state_1)); }
	inline int32_t get_state_1() const { return ___state_1; }
	inline int32_t* get_address_of_state_1() { return &___state_1; }
	inline void set_state_1(int32_t value)
	{
		___state_1 = value;
	}

	inline static int32_t get_offset_of_current_2() { return static_cast<int32_t>(offsetof(Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279, ___current_2)); }
	inline RuntimeObject * get_current_2() const { return ___current_2; }
	inline RuntimeObject ** get_address_of_current_2() { return &___current_2; }
	inline void set_current_2(RuntimeObject * value)
	{
		___current_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___current_2), (void*)value);
	}
};


// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>>
struct  List_1_t9B1CABCBD4B6B7781E348FF332B32B333B543BA8  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	KeyValuePair_2U5BU5D_tC4ED6A2024348F6B2CB712DC491F7E57AEF32630* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t9B1CABCBD4B6B7781E348FF332B32B333B543BA8, ____items_1)); }
	inline KeyValuePair_2U5BU5D_tC4ED6A2024348F6B2CB712DC491F7E57AEF32630* get__items_1() const { return ____items_1; }
	inline KeyValuePair_2U5BU5D_tC4ED6A2024348F6B2CB712DC491F7E57AEF32630** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(KeyValuePair_2U5BU5D_tC4ED6A2024348F6B2CB712DC491F7E57AEF32630* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t9B1CABCBD4B6B7781E348FF332B32B333B543BA8, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t9B1CABCBD4B6B7781E348FF332B32B333B543BA8, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t9B1CABCBD4B6B7781E348FF332B32B333B543BA8, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t9B1CABCBD4B6B7781E348FF332B32B333B543BA8_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	KeyValuePair_2U5BU5D_tC4ED6A2024348F6B2CB712DC491F7E57AEF32630* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t9B1CABCBD4B6B7781E348FF332B32B333B543BA8_StaticFields, ____emptyArray_5)); }
	inline KeyValuePair_2U5BU5D_tC4ED6A2024348F6B2CB712DC491F7E57AEF32630* get__emptyArray_5() const { return ____emptyArray_5; }
	inline KeyValuePair_2U5BU5D_tC4ED6A2024348F6B2CB712DC491F7E57AEF32630** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(KeyValuePair_2U5BU5D_tC4ED6A2024348F6B2CB712DC491F7E57AEF32630* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<UnityEngine.InputSystem.Utilities.InternedString>
struct  List_1_tFD37D052760A236906215AF581C58EE13DE6A5DC  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	InternedStringU5BU5D_t1EAE8433C8EF7C76894673801EE0EE9F6833E9A4* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tFD37D052760A236906215AF581C58EE13DE6A5DC, ____items_1)); }
	inline InternedStringU5BU5D_t1EAE8433C8EF7C76894673801EE0EE9F6833E9A4* get__items_1() const { return ____items_1; }
	inline InternedStringU5BU5D_t1EAE8433C8EF7C76894673801EE0EE9F6833E9A4** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(InternedStringU5BU5D_t1EAE8433C8EF7C76894673801EE0EE9F6833E9A4* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tFD37D052760A236906215AF581C58EE13DE6A5DC, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tFD37D052760A236906215AF581C58EE13DE6A5DC, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tFD37D052760A236906215AF581C58EE13DE6A5DC, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_tFD37D052760A236906215AF581C58EE13DE6A5DC_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	InternedStringU5BU5D_t1EAE8433C8EF7C76894673801EE0EE9F6833E9A4* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tFD37D052760A236906215AF581C58EE13DE6A5DC_StaticFields, ____emptyArray_5)); }
	inline InternedStringU5BU5D_t1EAE8433C8EF7C76894673801EE0EE9F6833E9A4* get__emptyArray_5() const { return ____emptyArray_5; }
	inline InternedStringU5BU5D_t1EAE8433C8EF7C76894673801EE0EE9F6833E9A4** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(InternedStringU5BU5D_t1EAE8433C8EF7C76894673801EE0EE9F6833E9A4* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<UnityEngine.InputSystem.Utilities.NameAndParameters>
struct  List_1_tCBF5EB99F2E538DCB47A8C835DD4B9C024879393  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	NameAndParametersU5BU5D_t960DDB52F44DD1231B0C626E9C859F1E0B2F5EAD* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tCBF5EB99F2E538DCB47A8C835DD4B9C024879393, ____items_1)); }
	inline NameAndParametersU5BU5D_t960DDB52F44DD1231B0C626E9C859F1E0B2F5EAD* get__items_1() const { return ____items_1; }
	inline NameAndParametersU5BU5D_t960DDB52F44DD1231B0C626E9C859F1E0B2F5EAD** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(NameAndParametersU5BU5D_t960DDB52F44DD1231B0C626E9C859F1E0B2F5EAD* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tCBF5EB99F2E538DCB47A8C835DD4B9C024879393, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tCBF5EB99F2E538DCB47A8C835DD4B9C024879393, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tCBF5EB99F2E538DCB47A8C835DD4B9C024879393, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_tCBF5EB99F2E538DCB47A8C835DD4B9C024879393_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	NameAndParametersU5BU5D_t960DDB52F44DD1231B0C626E9C859F1E0B2F5EAD* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tCBF5EB99F2E538DCB47A8C835DD4B9C024879393_StaticFields, ____emptyArray_5)); }
	inline NameAndParametersU5BU5D_t960DDB52F44DD1231B0C626E9C859F1E0B2F5EAD* get__emptyArray_5() const { return ____emptyArray_5; }
	inline NameAndParametersU5BU5D_t960DDB52F44DD1231B0C626E9C859F1E0B2F5EAD** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(NameAndParametersU5BU5D_t960DDB52F44DD1231B0C626E9C859F1E0B2F5EAD* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<UnityEngine.InputSystem.Utilities.NamedValue>
struct  List_1_tDC9592305A8D24F0DBC4A23F1719292FCB2887C1  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	NamedValueU5BU5D_t16F5D30B70B5FEDA89AE16CFD61FACEC7DF85391* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tDC9592305A8D24F0DBC4A23F1719292FCB2887C1, ____items_1)); }
	inline NamedValueU5BU5D_t16F5D30B70B5FEDA89AE16CFD61FACEC7DF85391* get__items_1() const { return ____items_1; }
	inline NamedValueU5BU5D_t16F5D30B70B5FEDA89AE16CFD61FACEC7DF85391** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(NamedValueU5BU5D_t16F5D30B70B5FEDA89AE16CFD61FACEC7DF85391* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tDC9592305A8D24F0DBC4A23F1719292FCB2887C1, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tDC9592305A8D24F0DBC4A23F1719292FCB2887C1, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tDC9592305A8D24F0DBC4A23F1719292FCB2887C1, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_tDC9592305A8D24F0DBC4A23F1719292FCB2887C1_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	NamedValueU5BU5D_t16F5D30B70B5FEDA89AE16CFD61FACEC7DF85391* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tDC9592305A8D24F0DBC4A23F1719292FCB2887C1_StaticFields, ____emptyArray_5)); }
	inline NamedValueU5BU5D_t16F5D30B70B5FEDA89AE16CFD61FACEC7DF85391* get__emptyArray_5() const { return ____emptyArray_5; }
	inline NamedValueU5BU5D_t16F5D30B70B5FEDA89AE16CFD61FACEC7DF85391** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(NamedValueU5BU5D_t16F5D30B70B5FEDA89AE16CFD61FACEC7DF85391* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<System.Object>
struct  List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____items_1)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get__items_1() const { return ____items_1; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5_StaticFields, ____emptyArray_5)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get__emptyArray_5() const { return ____emptyArray_5; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<UnityEngine.UIElements.StyleSelectorPart>
struct  List_1_tE7DC2F192C483F9E5722779E3C2C1910490AAE59  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	StyleSelectorPartU5BU5D_tD8762DADE4104C7D73D7B821DF7E5BBC87417BB7* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tE7DC2F192C483F9E5722779E3C2C1910490AAE59, ____items_1)); }
	inline StyleSelectorPartU5BU5D_tD8762DADE4104C7D73D7B821DF7E5BBC87417BB7* get__items_1() const { return ____items_1; }
	inline StyleSelectorPartU5BU5D_tD8762DADE4104C7D73D7B821DF7E5BBC87417BB7** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(StyleSelectorPartU5BU5D_tD8762DADE4104C7D73D7B821DF7E5BBC87417BB7* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tE7DC2F192C483F9E5722779E3C2C1910490AAE59, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tE7DC2F192C483F9E5722779E3C2C1910490AAE59, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tE7DC2F192C483F9E5722779E3C2C1910490AAE59, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_tE7DC2F192C483F9E5722779E3C2C1910490AAE59_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	StyleSelectorPartU5BU5D_tD8762DADE4104C7D73D7B821DF7E5BBC87417BB7* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tE7DC2F192C483F9E5722779E3C2C1910490AAE59_StaticFields, ____emptyArray_5)); }
	inline StyleSelectorPartU5BU5D_tD8762DADE4104C7D73D7B821DF7E5BBC87417BB7* get__emptyArray_5() const { return ____emptyArray_5; }
	inline StyleSelectorPartU5BU5D_tD8762DADE4104C7D73D7B821DF7E5BBC87417BB7** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(StyleSelectorPartU5BU5D_tD8762DADE4104C7D73D7B821DF7E5BBC87417BB7* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<UnityEngine.InputSystem.Utilities.Substring>
struct  List_1_tEAD6E3282E028927B32F56E7892994D90D512467  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	SubstringU5BU5D_tE332884993470F5CF7BAAA950414E4E24691A8D0* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tEAD6E3282E028927B32F56E7892994D90D512467, ____items_1)); }
	inline SubstringU5BU5D_tE332884993470F5CF7BAAA950414E4E24691A8D0* get__items_1() const { return ____items_1; }
	inline SubstringU5BU5D_tE332884993470F5CF7BAAA950414E4E24691A8D0** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(SubstringU5BU5D_tE332884993470F5CF7BAAA950414E4E24691A8D0* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tEAD6E3282E028927B32F56E7892994D90D512467, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tEAD6E3282E028927B32F56E7892994D90D512467, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tEAD6E3282E028927B32F56E7892994D90D512467, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_tEAD6E3282E028927B32F56E7892994D90D512467_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	SubstringU5BU5D_tE332884993470F5CF7BAAA950414E4E24691A8D0* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tEAD6E3282E028927B32F56E7892994D90D512467_StaticFields, ____emptyArray_5)); }
	inline SubstringU5BU5D_tE332884993470F5CF7BAAA950414E4E24691A8D0* get__emptyArray_5() const { return ____emptyArray_5; }
	inline SubstringU5BU5D_tE332884993470F5CF7BAAA950414E4E24691A8D0** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(SubstringU5BU5D_tE332884993470F5CF7BAAA950414E4E24691A8D0* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<UnityEngine.InputSystem.Layouts.InputControlLayout/ControlItem>
struct  List_1_tB7B59A258A4A84FCB5A2CC59F934C8B230EA963F  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ControlItemU5BU5D_t534859CA6A52E2EC9DDDCA52FE280E8D5CD598DB* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tB7B59A258A4A84FCB5A2CC59F934C8B230EA963F, ____items_1)); }
	inline ControlItemU5BU5D_t534859CA6A52E2EC9DDDCA52FE280E8D5CD598DB* get__items_1() const { return ____items_1; }
	inline ControlItemU5BU5D_t534859CA6A52E2EC9DDDCA52FE280E8D5CD598DB** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ControlItemU5BU5D_t534859CA6A52E2EC9DDDCA52FE280E8D5CD598DB* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tB7B59A258A4A84FCB5A2CC59F934C8B230EA963F, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tB7B59A258A4A84FCB5A2CC59F934C8B230EA963F, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tB7B59A258A4A84FCB5A2CC59F934C8B230EA963F, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_tB7B59A258A4A84FCB5A2CC59F934C8B230EA963F_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	ControlItemU5BU5D_t534859CA6A52E2EC9DDDCA52FE280E8D5CD598DB* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tB7B59A258A4A84FCB5A2CC59F934C8B230EA963F_StaticFields, ____emptyArray_5)); }
	inline ControlItemU5BU5D_t534859CA6A52E2EC9DDDCA52FE280E8D5CD598DB* get__emptyArray_5() const { return ____emptyArray_5; }
	inline ControlItemU5BU5D_t534859CA6A52E2EC9DDDCA52FE280E8D5CD598DB** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(ControlItemU5BU5D_t534859CA6A52E2EC9DDDCA52FE280E8D5CD598DB* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>
struct  List_1_tEC0B302C3EF23400C3EC92ED7672A715CA602068  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	JsonValueU5BU5D_t2E1CBAA099182C024E4921379907D707DA049098* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tEC0B302C3EF23400C3EC92ED7672A715CA602068, ____items_1)); }
	inline JsonValueU5BU5D_t2E1CBAA099182C024E4921379907D707DA049098* get__items_1() const { return ____items_1; }
	inline JsonValueU5BU5D_t2E1CBAA099182C024E4921379907D707DA049098** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(JsonValueU5BU5D_t2E1CBAA099182C024E4921379907D707DA049098* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tEC0B302C3EF23400C3EC92ED7672A715CA602068, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tEC0B302C3EF23400C3EC92ED7672A715CA602068, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tEC0B302C3EF23400C3EC92ED7672A715CA602068, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_tEC0B302C3EF23400C3EC92ED7672A715CA602068_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	JsonValueU5BU5D_t2E1CBAA099182C024E4921379907D707DA049098* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tEC0B302C3EF23400C3EC92ED7672A715CA602068_StaticFields, ____emptyArray_5)); }
	inline JsonValueU5BU5D_t2E1CBAA099182C024E4921379907D707DA049098* get__emptyArray_5() const { return ____emptyArray_5; }
	inline JsonValueU5BU5D_t2E1CBAA099182C024E4921379907D707DA049098** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(JsonValueU5BU5D_t2E1CBAA099182C024E4921379907D707DA049098* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// UnityEngine.InputSystem.Utilities.WhereObservable`1/Where<System.Object>
struct  Where_t5326AC9C3D27C847981552AE1DD31844DAB40D4D  : public RuntimeObject
{
public:
	// UnityEngine.InputSystem.Utilities.WhereObservable`1<TValue> UnityEngine.InputSystem.Utilities.WhereObservable`1/Where::m_Observable
	WhereObservable_1_t36D963808BB5D3A64EB26C52708AD22FCEAF8B3C * ___m_Observable_0;
	// System.IObserver`1<TValue> UnityEngine.InputSystem.Utilities.WhereObservable`1/Where::m_Observer
	RuntimeObject* ___m_Observer_1;

public:
	inline static int32_t get_offset_of_m_Observable_0() { return static_cast<int32_t>(offsetof(Where_t5326AC9C3D27C847981552AE1DD31844DAB40D4D, ___m_Observable_0)); }
	inline WhereObservable_1_t36D963808BB5D3A64EB26C52708AD22FCEAF8B3C * get_m_Observable_0() const { return ___m_Observable_0; }
	inline WhereObservable_1_t36D963808BB5D3A64EB26C52708AD22FCEAF8B3C ** get_address_of_m_Observable_0() { return &___m_Observable_0; }
	inline void set_m_Observable_0(WhereObservable_1_t36D963808BB5D3A64EB26C52708AD22FCEAF8B3C * value)
	{
		___m_Observable_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Observable_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Observer_1() { return static_cast<int32_t>(offsetof(Where_t5326AC9C3D27C847981552AE1DD31844DAB40D4D, ___m_Observer_1)); }
	inline RuntimeObject* get_m_Observer_1() const { return ___m_Observer_1; }
	inline RuntimeObject** get_address_of_m_Observer_1() { return &___m_Observer_1; }
	inline void set_m_Observer_1(RuntimeObject* value)
	{
		___m_Observer_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Observer_1), (void*)value);
	}
};


// UnityEngine.InputSystem.Utilities.WhereObservable`1<System.Object>
struct  WhereObservable_1_t36D963808BB5D3A64EB26C52708AD22FCEAF8B3C  : public RuntimeObject
{
public:
	// System.IObservable`1<TValue> UnityEngine.InputSystem.Utilities.WhereObservable`1::m_Source
	RuntimeObject* ___m_Source_0;
	// System.Func`2<TValue,System.Boolean> UnityEngine.InputSystem.Utilities.WhereObservable`1::m_Predicate
	Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___m_Predicate_1;

public:
	inline static int32_t get_offset_of_m_Source_0() { return static_cast<int32_t>(offsetof(WhereObservable_1_t36D963808BB5D3A64EB26C52708AD22FCEAF8B3C, ___m_Source_0)); }
	inline RuntimeObject* get_m_Source_0() const { return ___m_Source_0; }
	inline RuntimeObject** get_address_of_m_Source_0() { return &___m_Source_0; }
	inline void set_m_Source_0(RuntimeObject* value)
	{
		___m_Source_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Source_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Predicate_1() { return static_cast<int32_t>(offsetof(WhereObservable_1_t36D963808BB5D3A64EB26C52708AD22FCEAF8B3C, ___m_Predicate_1)); }
	inline Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * get_m_Predicate_1() const { return ___m_Predicate_1; }
	inline Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 ** get_address_of_m_Predicate_1() { return &___m_Predicate_1; }
	inline void set_m_Predicate_1(Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * value)
	{
		___m_Predicate_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Predicate_1), (void*)value);
	}
};

struct Il2CppArrayBounds;

// System.Array


// System.ValueType
struct  ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// System.Collections.Generic.List`1/Enumerator<System.Object>
struct  Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	RuntimeObject * ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6, ___list_0)); }
	inline List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * get_list_0() const { return ___list_0; }
	inline List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6, ___current_3)); }
	inline RuntimeObject * get_current_3() const { return ___current_3; }
	inline RuntimeObject ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(RuntimeObject * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___current_3), (void*)value);
	}
};


// UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Utilities.InternedString>
struct  ReadOnlyArray_1_t606E37DB0F1B75FCB4C4A029A0F6C2B4AE7DED75 
{
public:
	// TValue[] UnityEngine.InputSystem.Utilities.ReadOnlyArray`1::m_Array
	InternedStringU5BU5D_t1EAE8433C8EF7C76894673801EE0EE9F6833E9A4* ___m_Array_0;
	// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1::m_StartIndex
	int32_t ___m_StartIndex_1;
	// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1::m_Length
	int32_t ___m_Length_2;

public:
	inline static int32_t get_offset_of_m_Array_0() { return static_cast<int32_t>(offsetof(ReadOnlyArray_1_t606E37DB0F1B75FCB4C4A029A0F6C2B4AE7DED75, ___m_Array_0)); }
	inline InternedStringU5BU5D_t1EAE8433C8EF7C76894673801EE0EE9F6833E9A4* get_m_Array_0() const { return ___m_Array_0; }
	inline InternedStringU5BU5D_t1EAE8433C8EF7C76894673801EE0EE9F6833E9A4** get_address_of_m_Array_0() { return &___m_Array_0; }
	inline void set_m_Array_0(InternedStringU5BU5D_t1EAE8433C8EF7C76894673801EE0EE9F6833E9A4* value)
	{
		___m_Array_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Array_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_StartIndex_1() { return static_cast<int32_t>(offsetof(ReadOnlyArray_1_t606E37DB0F1B75FCB4C4A029A0F6C2B4AE7DED75, ___m_StartIndex_1)); }
	inline int32_t get_m_StartIndex_1() const { return ___m_StartIndex_1; }
	inline int32_t* get_address_of_m_StartIndex_1() { return &___m_StartIndex_1; }
	inline void set_m_StartIndex_1(int32_t value)
	{
		___m_StartIndex_1 = value;
	}

	inline static int32_t get_offset_of_m_Length_2() { return static_cast<int32_t>(offsetof(ReadOnlyArray_1_t606E37DB0F1B75FCB4C4A029A0F6C2B4AE7DED75, ___m_Length_2)); }
	inline int32_t get_m_Length_2() const { return ___m_Length_2; }
	inline int32_t* get_address_of_m_Length_2() { return &___m_Length_2; }
	inline void set_m_Length_2(int32_t value)
	{
		___m_Length_2 = value;
	}
};


// UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Utilities.NameAndParameters>
struct  ReadOnlyArray_1_tC87F80F9582CEDF1B66B5875A22591316B710C6E 
{
public:
	// TValue[] UnityEngine.InputSystem.Utilities.ReadOnlyArray`1::m_Array
	NameAndParametersU5BU5D_t960DDB52F44DD1231B0C626E9C859F1E0B2F5EAD* ___m_Array_0;
	// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1::m_StartIndex
	int32_t ___m_StartIndex_1;
	// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1::m_Length
	int32_t ___m_Length_2;

public:
	inline static int32_t get_offset_of_m_Array_0() { return static_cast<int32_t>(offsetof(ReadOnlyArray_1_tC87F80F9582CEDF1B66B5875A22591316B710C6E, ___m_Array_0)); }
	inline NameAndParametersU5BU5D_t960DDB52F44DD1231B0C626E9C859F1E0B2F5EAD* get_m_Array_0() const { return ___m_Array_0; }
	inline NameAndParametersU5BU5D_t960DDB52F44DD1231B0C626E9C859F1E0B2F5EAD** get_address_of_m_Array_0() { return &___m_Array_0; }
	inline void set_m_Array_0(NameAndParametersU5BU5D_t960DDB52F44DD1231B0C626E9C859F1E0B2F5EAD* value)
	{
		___m_Array_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Array_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_StartIndex_1() { return static_cast<int32_t>(offsetof(ReadOnlyArray_1_tC87F80F9582CEDF1B66B5875A22591316B710C6E, ___m_StartIndex_1)); }
	inline int32_t get_m_StartIndex_1() const { return ___m_StartIndex_1; }
	inline int32_t* get_address_of_m_StartIndex_1() { return &___m_StartIndex_1; }
	inline void set_m_StartIndex_1(int32_t value)
	{
		___m_StartIndex_1 = value;
	}

	inline static int32_t get_offset_of_m_Length_2() { return static_cast<int32_t>(offsetof(ReadOnlyArray_1_tC87F80F9582CEDF1B66B5875A22591316B710C6E, ___m_Length_2)); }
	inline int32_t get_m_Length_2() const { return ___m_Length_2; }
	inline int32_t* get_address_of_m_Length_2() { return &___m_Length_2; }
	inline void set_m_Length_2(int32_t value)
	{
		___m_Length_2 = value;
	}
};


// UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Utilities.NamedValue>
struct  ReadOnlyArray_1_t644486DA5FB2B247FC64733F192E2894F46CFCF5 
{
public:
	// TValue[] UnityEngine.InputSystem.Utilities.ReadOnlyArray`1::m_Array
	NamedValueU5BU5D_t16F5D30B70B5FEDA89AE16CFD61FACEC7DF85391* ___m_Array_0;
	// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1::m_StartIndex
	int32_t ___m_StartIndex_1;
	// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1::m_Length
	int32_t ___m_Length_2;

public:
	inline static int32_t get_offset_of_m_Array_0() { return static_cast<int32_t>(offsetof(ReadOnlyArray_1_t644486DA5FB2B247FC64733F192E2894F46CFCF5, ___m_Array_0)); }
	inline NamedValueU5BU5D_t16F5D30B70B5FEDA89AE16CFD61FACEC7DF85391* get_m_Array_0() const { return ___m_Array_0; }
	inline NamedValueU5BU5D_t16F5D30B70B5FEDA89AE16CFD61FACEC7DF85391** get_address_of_m_Array_0() { return &___m_Array_0; }
	inline void set_m_Array_0(NamedValueU5BU5D_t16F5D30B70B5FEDA89AE16CFD61FACEC7DF85391* value)
	{
		___m_Array_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Array_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_StartIndex_1() { return static_cast<int32_t>(offsetof(ReadOnlyArray_1_t644486DA5FB2B247FC64733F192E2894F46CFCF5, ___m_StartIndex_1)); }
	inline int32_t get_m_StartIndex_1() const { return ___m_StartIndex_1; }
	inline int32_t* get_address_of_m_StartIndex_1() { return &___m_StartIndex_1; }
	inline void set_m_StartIndex_1(int32_t value)
	{
		___m_StartIndex_1 = value;
	}

	inline static int32_t get_offset_of_m_Length_2() { return static_cast<int32_t>(offsetof(ReadOnlyArray_1_t644486DA5FB2B247FC64733F192E2894F46CFCF5, ___m_Length_2)); }
	inline int32_t get_m_Length_2() const { return ___m_Length_2; }
	inline int32_t* get_address_of_m_Length_2() { return &___m_Length_2; }
	inline void set_m_Length_2(int32_t value)
	{
		___m_Length_2 = value;
	}
};


// System.Linq.Enumerable/WhereArrayIterator`1<System.Object>
struct  WhereArrayIterator_1_t7D84D638EB94F5CC3BE1B29D8FC781CA8CD15A86  : public Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279
{
public:
	// TSource[] System.Linq.Enumerable/WhereArrayIterator`1::source
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereArrayIterator`1::predicate
	Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate_4;
	// System.Int32 System.Linq.Enumerable/WhereArrayIterator`1::index
	int32_t ___index_5;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereArrayIterator_1_t7D84D638EB94F5CC3BE1B29D8FC781CA8CD15A86, ___source_3)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get_source_3() const { return ___source_3; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereArrayIterator_1_t7D84D638EB94F5CC3BE1B29D8FC781CA8CD15A86, ___predicate_4)); }
	inline Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_index_5() { return static_cast<int32_t>(offsetof(WhereArrayIterator_1_t7D84D638EB94F5CC3BE1B29D8FC781CA8CD15A86, ___index_5)); }
	inline int32_t get_index_5() const { return ___index_5; }
	inline int32_t* get_address_of_index_5() { return &___index_5; }
	inline void set_index_5(int32_t value)
	{
		___index_5 = value;
	}
};


// System.Linq.Enumerable/WhereEnumerableIterator`1<System.Object>
struct  WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0  : public Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereEnumerableIterator`1::source
	RuntimeObject* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereEnumerableIterator`1::predicate
	Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate_4;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/WhereEnumerableIterator`1::enumerator
	RuntimeObject* ___enumerator_5;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0, ___source_3)); }
	inline RuntimeObject* get_source_3() const { return ___source_3; }
	inline RuntimeObject** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(RuntimeObject* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0, ___predicate_4)); }
	inline Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_5() { return static_cast<int32_t>(offsetof(WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0, ___enumerator_5)); }
	inline RuntimeObject* get_enumerator_5() const { return ___enumerator_5; }
	inline RuntimeObject** get_address_of_enumerator_5() { return &___enumerator_5; }
	inline void set_enumerator_5(RuntimeObject* value)
	{
		___enumerator_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumerator_5), (void*)value);
	}
};


// System.Linq.Enumerable/WhereSelectArrayIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,System.Object>
struct  WhereSelectArrayIterator_2_tEC7D2E75D2D8A9E1AA3D6F21AA9CBCB87EBD6617  : public Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279
{
public:
	// TSource[] System.Linq.Enumerable/WhereSelectArrayIterator`2::source
	KeyValuePair_2U5BU5D_tC4ED6A2024348F6B2CB712DC491F7E57AEF32630* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectArrayIterator`2::predicate
	Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectArrayIterator`2::selector
	Func_2_t711512467B454C91EB8F3FB19CD6D141AAB5B6B1 * ___selector_5;
	// System.Int32 System.Linq.Enumerable/WhereSelectArrayIterator`2::index
	int32_t ___index_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectArrayIterator_2_tEC7D2E75D2D8A9E1AA3D6F21AA9CBCB87EBD6617, ___source_3)); }
	inline KeyValuePair_2U5BU5D_tC4ED6A2024348F6B2CB712DC491F7E57AEF32630* get_source_3() const { return ___source_3; }
	inline KeyValuePair_2U5BU5D_tC4ED6A2024348F6B2CB712DC491F7E57AEF32630** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(KeyValuePair_2U5BU5D_tC4ED6A2024348F6B2CB712DC491F7E57AEF32630* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectArrayIterator_2_tEC7D2E75D2D8A9E1AA3D6F21AA9CBCB87EBD6617, ___predicate_4)); }
	inline Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectArrayIterator_2_tEC7D2E75D2D8A9E1AA3D6F21AA9CBCB87EBD6617, ___selector_5)); }
	inline Func_2_t711512467B454C91EB8F3FB19CD6D141AAB5B6B1 * get_selector_5() const { return ___selector_5; }
	inline Func_2_t711512467B454C91EB8F3FB19CD6D141AAB5B6B1 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t711512467B454C91EB8F3FB19CD6D141AAB5B6B1 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_index_6() { return static_cast<int32_t>(offsetof(WhereSelectArrayIterator_2_tEC7D2E75D2D8A9E1AA3D6F21AA9CBCB87EBD6617, ___index_6)); }
	inline int32_t get_index_6() const { return ___index_6; }
	inline int32_t* get_address_of_index_6() { return &___index_6; }
	inline void set_index_6(int32_t value)
	{
		___index_6 = value;
	}
};


// System.Linq.Enumerable/WhereSelectArrayIterator`2<UnityEngine.InputSystem.Utilities.InternedString,System.Object>
struct  WhereSelectArrayIterator_2_t297D221F6AEFE060ABD891ACCBE86FD625D9F687  : public Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279
{
public:
	// TSource[] System.Linq.Enumerable/WhereSelectArrayIterator`2::source
	InternedStringU5BU5D_t1EAE8433C8EF7C76894673801EE0EE9F6833E9A4* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectArrayIterator`2::predicate
	Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectArrayIterator`2::selector
	Func_2_tA2BD34BB8D97AD282016BD4CB5B8675E4AF85B45 * ___selector_5;
	// System.Int32 System.Linq.Enumerable/WhereSelectArrayIterator`2::index
	int32_t ___index_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectArrayIterator_2_t297D221F6AEFE060ABD891ACCBE86FD625D9F687, ___source_3)); }
	inline InternedStringU5BU5D_t1EAE8433C8EF7C76894673801EE0EE9F6833E9A4* get_source_3() const { return ___source_3; }
	inline InternedStringU5BU5D_t1EAE8433C8EF7C76894673801EE0EE9F6833E9A4** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(InternedStringU5BU5D_t1EAE8433C8EF7C76894673801EE0EE9F6833E9A4* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectArrayIterator_2_t297D221F6AEFE060ABD891ACCBE86FD625D9F687, ___predicate_4)); }
	inline Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectArrayIterator_2_t297D221F6AEFE060ABD891ACCBE86FD625D9F687, ___selector_5)); }
	inline Func_2_tA2BD34BB8D97AD282016BD4CB5B8675E4AF85B45 * get_selector_5() const { return ___selector_5; }
	inline Func_2_tA2BD34BB8D97AD282016BD4CB5B8675E4AF85B45 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_tA2BD34BB8D97AD282016BD4CB5B8675E4AF85B45 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_index_6() { return static_cast<int32_t>(offsetof(WhereSelectArrayIterator_2_t297D221F6AEFE060ABD891ACCBE86FD625D9F687, ___index_6)); }
	inline int32_t get_index_6() const { return ___index_6; }
	inline int32_t* get_address_of_index_6() { return &___index_6; }
	inline void set_index_6(int32_t value)
	{
		___index_6 = value;
	}
};


// System.Linq.Enumerable/WhereSelectArrayIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,System.Object>
struct  WhereSelectArrayIterator_2_t8E1E4D86BDBA319DD5FB2D615DE93CAE92D68506  : public Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279
{
public:
	// TSource[] System.Linq.Enumerable/WhereSelectArrayIterator`2::source
	NameAndParametersU5BU5D_t960DDB52F44DD1231B0C626E9C859F1E0B2F5EAD* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectArrayIterator`2::predicate
	Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectArrayIterator`2::selector
	Func_2_tF7304610FB007335AC870B51C8FE2092628DC0DB * ___selector_5;
	// System.Int32 System.Linq.Enumerable/WhereSelectArrayIterator`2::index
	int32_t ___index_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectArrayIterator_2_t8E1E4D86BDBA319DD5FB2D615DE93CAE92D68506, ___source_3)); }
	inline NameAndParametersU5BU5D_t960DDB52F44DD1231B0C626E9C859F1E0B2F5EAD* get_source_3() const { return ___source_3; }
	inline NameAndParametersU5BU5D_t960DDB52F44DD1231B0C626E9C859F1E0B2F5EAD** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(NameAndParametersU5BU5D_t960DDB52F44DD1231B0C626E9C859F1E0B2F5EAD* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectArrayIterator_2_t8E1E4D86BDBA319DD5FB2D615DE93CAE92D68506, ___predicate_4)); }
	inline Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectArrayIterator_2_t8E1E4D86BDBA319DD5FB2D615DE93CAE92D68506, ___selector_5)); }
	inline Func_2_tF7304610FB007335AC870B51C8FE2092628DC0DB * get_selector_5() const { return ___selector_5; }
	inline Func_2_tF7304610FB007335AC870B51C8FE2092628DC0DB ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_tF7304610FB007335AC870B51C8FE2092628DC0DB * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_index_6() { return static_cast<int32_t>(offsetof(WhereSelectArrayIterator_2_t8E1E4D86BDBA319DD5FB2D615DE93CAE92D68506, ___index_6)); }
	inline int32_t get_index_6() const { return ___index_6; }
	inline int32_t* get_address_of_index_6() { return &___index_6; }
	inline void set_index_6(int32_t value)
	{
		___index_6 = value;
	}
};


// System.Linq.Enumerable/WhereSelectArrayIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,System.Object>
struct  WhereSelectArrayIterator_2_tFFAF318BF43576D4F2880331F0ECAB9792119697  : public Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279
{
public:
	// TSource[] System.Linq.Enumerable/WhereSelectArrayIterator`2::source
	NamedValueU5BU5D_t16F5D30B70B5FEDA89AE16CFD61FACEC7DF85391* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectArrayIterator`2::predicate
	Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectArrayIterator`2::selector
	Func_2_t5B101C0B26ADAA3B1A77BC50D51CC42364E5409E * ___selector_5;
	// System.Int32 System.Linq.Enumerable/WhereSelectArrayIterator`2::index
	int32_t ___index_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectArrayIterator_2_tFFAF318BF43576D4F2880331F0ECAB9792119697, ___source_3)); }
	inline NamedValueU5BU5D_t16F5D30B70B5FEDA89AE16CFD61FACEC7DF85391* get_source_3() const { return ___source_3; }
	inline NamedValueU5BU5D_t16F5D30B70B5FEDA89AE16CFD61FACEC7DF85391** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(NamedValueU5BU5D_t16F5D30B70B5FEDA89AE16CFD61FACEC7DF85391* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectArrayIterator_2_tFFAF318BF43576D4F2880331F0ECAB9792119697, ___predicate_4)); }
	inline Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectArrayIterator_2_tFFAF318BF43576D4F2880331F0ECAB9792119697, ___selector_5)); }
	inline Func_2_t5B101C0B26ADAA3B1A77BC50D51CC42364E5409E * get_selector_5() const { return ___selector_5; }
	inline Func_2_t5B101C0B26ADAA3B1A77BC50D51CC42364E5409E ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t5B101C0B26ADAA3B1A77BC50D51CC42364E5409E * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_index_6() { return static_cast<int32_t>(offsetof(WhereSelectArrayIterator_2_tFFAF318BF43576D4F2880331F0ECAB9792119697, ___index_6)); }
	inline int32_t get_index_6() const { return ___index_6; }
	inline int32_t* get_address_of_index_6() { return &___index_6; }
	inline void set_index_6(int32_t value)
	{
		___index_6 = value;
	}
};


// System.Linq.Enumerable/WhereSelectArrayIterator`2<System.Object,System.Object>
struct  WhereSelectArrayIterator_2_tA706D5B1608A9A8F1BF43C6E5D9D682C901DB244  : public Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279
{
public:
	// TSource[] System.Linq.Enumerable/WhereSelectArrayIterator`2::source
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectArrayIterator`2::predicate
	Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectArrayIterator`2::selector
	Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 * ___selector_5;
	// System.Int32 System.Linq.Enumerable/WhereSelectArrayIterator`2::index
	int32_t ___index_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectArrayIterator_2_tA706D5B1608A9A8F1BF43C6E5D9D682C901DB244, ___source_3)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get_source_3() const { return ___source_3; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectArrayIterator_2_tA706D5B1608A9A8F1BF43C6E5D9D682C901DB244, ___predicate_4)); }
	inline Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectArrayIterator_2_tA706D5B1608A9A8F1BF43C6E5D9D682C901DB244, ___selector_5)); }
	inline Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 * get_selector_5() const { return ___selector_5; }
	inline Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_index_6() { return static_cast<int32_t>(offsetof(WhereSelectArrayIterator_2_tA706D5B1608A9A8F1BF43C6E5D9D682C901DB244, ___index_6)); }
	inline int32_t get_index_6() const { return ___index_6; }
	inline int32_t* get_address_of_index_6() { return &___index_6; }
	inline void set_index_6(int32_t value)
	{
		___index_6 = value;
	}
};


// System.Linq.Enumerable/WhereSelectArrayIterator`2<UnityEngine.UIElements.StyleSelectorPart,System.Object>
struct  WhereSelectArrayIterator_2_t49E50A395D6004220E3A7ACD1AFFD129393B098E  : public Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279
{
public:
	// TSource[] System.Linq.Enumerable/WhereSelectArrayIterator`2::source
	StyleSelectorPartU5BU5D_tD8762DADE4104C7D73D7B821DF7E5BBC87417BB7* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectArrayIterator`2::predicate
	Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectArrayIterator`2::selector
	Func_2_t43979C5A872BA9ED7A923D533A01683A98D71957 * ___selector_5;
	// System.Int32 System.Linq.Enumerable/WhereSelectArrayIterator`2::index
	int32_t ___index_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectArrayIterator_2_t49E50A395D6004220E3A7ACD1AFFD129393B098E, ___source_3)); }
	inline StyleSelectorPartU5BU5D_tD8762DADE4104C7D73D7B821DF7E5BBC87417BB7* get_source_3() const { return ___source_3; }
	inline StyleSelectorPartU5BU5D_tD8762DADE4104C7D73D7B821DF7E5BBC87417BB7** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(StyleSelectorPartU5BU5D_tD8762DADE4104C7D73D7B821DF7E5BBC87417BB7* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectArrayIterator_2_t49E50A395D6004220E3A7ACD1AFFD129393B098E, ___predicate_4)); }
	inline Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectArrayIterator_2_t49E50A395D6004220E3A7ACD1AFFD129393B098E, ___selector_5)); }
	inline Func_2_t43979C5A872BA9ED7A923D533A01683A98D71957 * get_selector_5() const { return ___selector_5; }
	inline Func_2_t43979C5A872BA9ED7A923D533A01683A98D71957 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t43979C5A872BA9ED7A923D533A01683A98D71957 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_index_6() { return static_cast<int32_t>(offsetof(WhereSelectArrayIterator_2_t49E50A395D6004220E3A7ACD1AFFD129393B098E, ___index_6)); }
	inline int32_t get_index_6() const { return ___index_6; }
	inline int32_t* get_address_of_index_6() { return &___index_6; }
	inline void set_index_6(int32_t value)
	{
		___index_6 = value;
	}
};


// System.Linq.Enumerable/WhereSelectArrayIterator`2<UnityEngine.InputSystem.Utilities.Substring,System.Object>
struct  WhereSelectArrayIterator_2_tAA2E097EE3422A3A4E4652B975AA568F28CF2C90  : public Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279
{
public:
	// TSource[] System.Linq.Enumerable/WhereSelectArrayIterator`2::source
	SubstringU5BU5D_tE332884993470F5CF7BAAA950414E4E24691A8D0* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectArrayIterator`2::predicate
	Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectArrayIterator`2::selector
	Func_2_t4EC2F095EDDC8079537CA0FEDC463635E599C9E4 * ___selector_5;
	// System.Int32 System.Linq.Enumerable/WhereSelectArrayIterator`2::index
	int32_t ___index_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectArrayIterator_2_tAA2E097EE3422A3A4E4652B975AA568F28CF2C90, ___source_3)); }
	inline SubstringU5BU5D_tE332884993470F5CF7BAAA950414E4E24691A8D0* get_source_3() const { return ___source_3; }
	inline SubstringU5BU5D_tE332884993470F5CF7BAAA950414E4E24691A8D0** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(SubstringU5BU5D_tE332884993470F5CF7BAAA950414E4E24691A8D0* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectArrayIterator_2_tAA2E097EE3422A3A4E4652B975AA568F28CF2C90, ___predicate_4)); }
	inline Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectArrayIterator_2_tAA2E097EE3422A3A4E4652B975AA568F28CF2C90, ___selector_5)); }
	inline Func_2_t4EC2F095EDDC8079537CA0FEDC463635E599C9E4 * get_selector_5() const { return ___selector_5; }
	inline Func_2_t4EC2F095EDDC8079537CA0FEDC463635E599C9E4 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t4EC2F095EDDC8079537CA0FEDC463635E599C9E4 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_index_6() { return static_cast<int32_t>(offsetof(WhereSelectArrayIterator_2_tAA2E097EE3422A3A4E4652B975AA568F28CF2C90, ___index_6)); }
	inline int32_t get_index_6() const { return ___index_6; }
	inline int32_t* get_address_of_index_6() { return &___index_6; }
	inline void set_index_6(int32_t value)
	{
		___index_6 = value;
	}
};


// System.Linq.Enumerable/WhereSelectArrayIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,System.Object>
struct  WhereSelectArrayIterator_2_tED9B99EA45685E4AB08C352736644EB4CAFC9980  : public Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279
{
public:
	// TSource[] System.Linq.Enumerable/WhereSelectArrayIterator`2::source
	JsonValueU5BU5D_t2E1CBAA099182C024E4921379907D707DA049098* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectArrayIterator`2::predicate
	Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectArrayIterator`2::selector
	Func_2_t1628B2E4EEC7A32D5B688E54415BC42EDA9D2899 * ___selector_5;
	// System.Int32 System.Linq.Enumerable/WhereSelectArrayIterator`2::index
	int32_t ___index_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectArrayIterator_2_tED9B99EA45685E4AB08C352736644EB4CAFC9980, ___source_3)); }
	inline JsonValueU5BU5D_t2E1CBAA099182C024E4921379907D707DA049098* get_source_3() const { return ___source_3; }
	inline JsonValueU5BU5D_t2E1CBAA099182C024E4921379907D707DA049098** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(JsonValueU5BU5D_t2E1CBAA099182C024E4921379907D707DA049098* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectArrayIterator_2_tED9B99EA45685E4AB08C352736644EB4CAFC9980, ___predicate_4)); }
	inline Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectArrayIterator_2_tED9B99EA45685E4AB08C352736644EB4CAFC9980, ___selector_5)); }
	inline Func_2_t1628B2E4EEC7A32D5B688E54415BC42EDA9D2899 * get_selector_5() const { return ___selector_5; }
	inline Func_2_t1628B2E4EEC7A32D5B688E54415BC42EDA9D2899 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t1628B2E4EEC7A32D5B688E54415BC42EDA9D2899 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_index_6() { return static_cast<int32_t>(offsetof(WhereSelectArrayIterator_2_tED9B99EA45685E4AB08C352736644EB4CAFC9980, ___index_6)); }
	inline int32_t get_index_6() const { return ___index_6; }
	inline int32_t* get_address_of_index_6() { return &___index_6; }
	inline void set_index_6(int32_t value)
	{
		___index_6 = value;
	}
};


// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,System.Object>
struct  WhereSelectEnumerableIterator_2_t0ACED5698603E2D2B4071096A021E0FA6102F459  : public Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::source
	RuntimeObject* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::predicate
	Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::selector
	Func_2_t711512467B454C91EB8F3FB19CD6D141AAB5B6B1 * ___selector_5;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::enumerator
	RuntimeObject* ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t0ACED5698603E2D2B4071096A021E0FA6102F459, ___source_3)); }
	inline RuntimeObject* get_source_3() const { return ___source_3; }
	inline RuntimeObject** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(RuntimeObject* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t0ACED5698603E2D2B4071096A021E0FA6102F459, ___predicate_4)); }
	inline Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t0ACED5698603E2D2B4071096A021E0FA6102F459, ___selector_5)); }
	inline Func_2_t711512467B454C91EB8F3FB19CD6D141AAB5B6B1 * get_selector_5() const { return ___selector_5; }
	inline Func_2_t711512467B454C91EB8F3FB19CD6D141AAB5B6B1 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t711512467B454C91EB8F3FB19CD6D141AAB5B6B1 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t0ACED5698603E2D2B4071096A021E0FA6102F459, ___enumerator_6)); }
	inline RuntimeObject* get_enumerator_6() const { return ___enumerator_6; }
	inline RuntimeObject** get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(RuntimeObject* value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumerator_6), (void*)value);
	}
};


// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.InternedString,System.Object>
struct  WhereSelectEnumerableIterator_2_t02B3FDD3CA4282B70A77A6A52D6AADC70EF74E8B  : public Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::source
	RuntimeObject* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::predicate
	Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::selector
	Func_2_tA2BD34BB8D97AD282016BD4CB5B8675E4AF85B45 * ___selector_5;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::enumerator
	RuntimeObject* ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t02B3FDD3CA4282B70A77A6A52D6AADC70EF74E8B, ___source_3)); }
	inline RuntimeObject* get_source_3() const { return ___source_3; }
	inline RuntimeObject** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(RuntimeObject* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t02B3FDD3CA4282B70A77A6A52D6AADC70EF74E8B, ___predicate_4)); }
	inline Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t02B3FDD3CA4282B70A77A6A52D6AADC70EF74E8B, ___selector_5)); }
	inline Func_2_tA2BD34BB8D97AD282016BD4CB5B8675E4AF85B45 * get_selector_5() const { return ___selector_5; }
	inline Func_2_tA2BD34BB8D97AD282016BD4CB5B8675E4AF85B45 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_tA2BD34BB8D97AD282016BD4CB5B8675E4AF85B45 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t02B3FDD3CA4282B70A77A6A52D6AADC70EF74E8B, ___enumerator_6)); }
	inline RuntimeObject* get_enumerator_6() const { return ___enumerator_6; }
	inline RuntimeObject** get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(RuntimeObject* value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumerator_6), (void*)value);
	}
};


// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,System.Object>
struct  WhereSelectEnumerableIterator_2_t9C8232C608C379652355BB860B0A6074ABF36E09  : public Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::source
	RuntimeObject* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::predicate
	Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::selector
	Func_2_tF7304610FB007335AC870B51C8FE2092628DC0DB * ___selector_5;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::enumerator
	RuntimeObject* ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t9C8232C608C379652355BB860B0A6074ABF36E09, ___source_3)); }
	inline RuntimeObject* get_source_3() const { return ___source_3; }
	inline RuntimeObject** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(RuntimeObject* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t9C8232C608C379652355BB860B0A6074ABF36E09, ___predicate_4)); }
	inline Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t9C8232C608C379652355BB860B0A6074ABF36E09, ___selector_5)); }
	inline Func_2_tF7304610FB007335AC870B51C8FE2092628DC0DB * get_selector_5() const { return ___selector_5; }
	inline Func_2_tF7304610FB007335AC870B51C8FE2092628DC0DB ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_tF7304610FB007335AC870B51C8FE2092628DC0DB * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t9C8232C608C379652355BB860B0A6074ABF36E09, ___enumerator_6)); }
	inline RuntimeObject* get_enumerator_6() const { return ___enumerator_6; }
	inline RuntimeObject** get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(RuntimeObject* value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumerator_6), (void*)value);
	}
};


// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,System.Object>
struct  WhereSelectEnumerableIterator_2_tEBF9E61EFDABBBB02113A82648E32AC170921957  : public Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::source
	RuntimeObject* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::predicate
	Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::selector
	Func_2_t5B101C0B26ADAA3B1A77BC50D51CC42364E5409E * ___selector_5;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::enumerator
	RuntimeObject* ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tEBF9E61EFDABBBB02113A82648E32AC170921957, ___source_3)); }
	inline RuntimeObject* get_source_3() const { return ___source_3; }
	inline RuntimeObject** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(RuntimeObject* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tEBF9E61EFDABBBB02113A82648E32AC170921957, ___predicate_4)); }
	inline Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tEBF9E61EFDABBBB02113A82648E32AC170921957, ___selector_5)); }
	inline Func_2_t5B101C0B26ADAA3B1A77BC50D51CC42364E5409E * get_selector_5() const { return ___selector_5; }
	inline Func_2_t5B101C0B26ADAA3B1A77BC50D51CC42364E5409E ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t5B101C0B26ADAA3B1A77BC50D51CC42364E5409E * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tEBF9E61EFDABBBB02113A82648E32AC170921957, ___enumerator_6)); }
	inline RuntimeObject* get_enumerator_6() const { return ___enumerator_6; }
	inline RuntimeObject** get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(RuntimeObject* value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumerator_6), (void*)value);
	}
};


// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Object,System.Object>
struct  WhereSelectEnumerableIterator_2_tDAA8BFBEA68F81670F3F51C6200EBD26D7A8FBAB  : public Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::source
	RuntimeObject* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::predicate
	Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::selector
	Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 * ___selector_5;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::enumerator
	RuntimeObject* ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tDAA8BFBEA68F81670F3F51C6200EBD26D7A8FBAB, ___source_3)); }
	inline RuntimeObject* get_source_3() const { return ___source_3; }
	inline RuntimeObject** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(RuntimeObject* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tDAA8BFBEA68F81670F3F51C6200EBD26D7A8FBAB, ___predicate_4)); }
	inline Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tDAA8BFBEA68F81670F3F51C6200EBD26D7A8FBAB, ___selector_5)); }
	inline Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 * get_selector_5() const { return ___selector_5; }
	inline Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tDAA8BFBEA68F81670F3F51C6200EBD26D7A8FBAB, ___enumerator_6)); }
	inline RuntimeObject* get_enumerator_6() const { return ___enumerator_6; }
	inline RuntimeObject** get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(RuntimeObject* value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumerator_6), (void*)value);
	}
};


// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.UIElements.StyleSelectorPart,System.Object>
struct  WhereSelectEnumerableIterator_2_tF480AE5ADD931EEE44A713040BA622E7577211AE  : public Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::source
	RuntimeObject* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::predicate
	Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::selector
	Func_2_t43979C5A872BA9ED7A923D533A01683A98D71957 * ___selector_5;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::enumerator
	RuntimeObject* ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tF480AE5ADD931EEE44A713040BA622E7577211AE, ___source_3)); }
	inline RuntimeObject* get_source_3() const { return ___source_3; }
	inline RuntimeObject** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(RuntimeObject* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tF480AE5ADD931EEE44A713040BA622E7577211AE, ___predicate_4)); }
	inline Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tF480AE5ADD931EEE44A713040BA622E7577211AE, ___selector_5)); }
	inline Func_2_t43979C5A872BA9ED7A923D533A01683A98D71957 * get_selector_5() const { return ___selector_5; }
	inline Func_2_t43979C5A872BA9ED7A923D533A01683A98D71957 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t43979C5A872BA9ED7A923D533A01683A98D71957 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tF480AE5ADD931EEE44A713040BA622E7577211AE, ___enumerator_6)); }
	inline RuntimeObject* get_enumerator_6() const { return ___enumerator_6; }
	inline RuntimeObject** get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(RuntimeObject* value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumerator_6), (void*)value);
	}
};


// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.Substring,System.Object>
struct  WhereSelectEnumerableIterator_2_t5E3BB9AFBEC27ED62FC129516C2D3E9FC81641B6  : public Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::source
	RuntimeObject* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::predicate
	Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::selector
	Func_2_t4EC2F095EDDC8079537CA0FEDC463635E599C9E4 * ___selector_5;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::enumerator
	RuntimeObject* ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t5E3BB9AFBEC27ED62FC129516C2D3E9FC81641B6, ___source_3)); }
	inline RuntimeObject* get_source_3() const { return ___source_3; }
	inline RuntimeObject** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(RuntimeObject* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t5E3BB9AFBEC27ED62FC129516C2D3E9FC81641B6, ___predicate_4)); }
	inline Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t5E3BB9AFBEC27ED62FC129516C2D3E9FC81641B6, ___selector_5)); }
	inline Func_2_t4EC2F095EDDC8079537CA0FEDC463635E599C9E4 * get_selector_5() const { return ___selector_5; }
	inline Func_2_t4EC2F095EDDC8079537CA0FEDC463635E599C9E4 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t4EC2F095EDDC8079537CA0FEDC463635E599C9E4 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t5E3BB9AFBEC27ED62FC129516C2D3E9FC81641B6, ___enumerator_6)); }
	inline RuntimeObject* get_enumerator_6() const { return ___enumerator_6; }
	inline RuntimeObject** get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(RuntimeObject* value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumerator_6), (void*)value);
	}
};


// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,System.Object>
struct  WhereSelectEnumerableIterator_2_t4DAE42F4C708266EAC938F1C6E7FB762CDF50463  : public Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::source
	RuntimeObject* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::predicate
	Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::selector
	Func_2_t1628B2E4EEC7A32D5B688E54415BC42EDA9D2899 * ___selector_5;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::enumerator
	RuntimeObject* ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t4DAE42F4C708266EAC938F1C6E7FB762CDF50463, ___source_3)); }
	inline RuntimeObject* get_source_3() const { return ___source_3; }
	inline RuntimeObject** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(RuntimeObject* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t4DAE42F4C708266EAC938F1C6E7FB762CDF50463, ___predicate_4)); }
	inline Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t4DAE42F4C708266EAC938F1C6E7FB762CDF50463, ___selector_5)); }
	inline Func_2_t1628B2E4EEC7A32D5B688E54415BC42EDA9D2899 * get_selector_5() const { return ___selector_5; }
	inline Func_2_t1628B2E4EEC7A32D5B688E54415BC42EDA9D2899 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t1628B2E4EEC7A32D5B688E54415BC42EDA9D2899 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t4DAE42F4C708266EAC938F1C6E7FB762CDF50463, ___enumerator_6)); }
	inline RuntimeObject* get_enumerator_6() const { return ___enumerator_6; }
	inline RuntimeObject** get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(RuntimeObject* value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumerator_6), (void*)value);
	}
};


// System.Boolean
struct  Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Enum
struct  Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// UnityEngine.InputSystem.Utilities.FourCC
struct  FourCC_t8D38CDCEAD02FE53A6C2E9AF01354E4E5AB400A9 
{
public:
	// System.Int32 UnityEngine.InputSystem.Utilities.FourCC::m_Code
	int32_t ___m_Code_0;

public:
	inline static int32_t get_offset_of_m_Code_0() { return static_cast<int32_t>(offsetof(FourCC_t8D38CDCEAD02FE53A6C2E9AF01354E4E5AB400A9, ___m_Code_0)); }
	inline int32_t get_m_Code_0() const { return ___m_Code_0; }
	inline int32_t* get_address_of_m_Code_0() { return &___m_Code_0; }
	inline void set_m_Code_0(int32_t value)
	{
		___m_Code_0 = value;
	}
};


// System.Int32
struct  Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// UnityEngine.InputSystem.Utilities.InternedString
struct  InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED 
{
public:
	// System.String UnityEngine.InputSystem.Utilities.InternedString::m_StringOriginalCase
	String_t* ___m_StringOriginalCase_0;
	// System.String UnityEngine.InputSystem.Utilities.InternedString::m_StringLowerCase
	String_t* ___m_StringLowerCase_1;

public:
	inline static int32_t get_offset_of_m_StringOriginalCase_0() { return static_cast<int32_t>(offsetof(InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED, ___m_StringOriginalCase_0)); }
	inline String_t* get_m_StringOriginalCase_0() const { return ___m_StringOriginalCase_0; }
	inline String_t** get_address_of_m_StringOriginalCase_0() { return &___m_StringOriginalCase_0; }
	inline void set_m_StringOriginalCase_0(String_t* value)
	{
		___m_StringOriginalCase_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_StringOriginalCase_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_StringLowerCase_1() { return static_cast<int32_t>(offsetof(InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED, ___m_StringLowerCase_1)); }
	inline String_t* get_m_StringLowerCase_1() const { return ___m_StringLowerCase_1; }
	inline String_t** get_address_of_m_StringLowerCase_1() { return &___m_StringLowerCase_1; }
	inline void set_m_StringLowerCase_1(String_t* value)
	{
		___m_StringLowerCase_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_StringLowerCase_1), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.InputSystem.Utilities.InternedString
struct InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED_marshaled_pinvoke
{
	char* ___m_StringOriginalCase_0;
	char* ___m_StringLowerCase_1;
};
// Native definition for COM marshalling of UnityEngine.InputSystem.Utilities.InternedString
struct InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED_marshaled_com
{
	Il2CppChar* ___m_StringOriginalCase_0;
	Il2CppChar* ___m_StringLowerCase_1;
};

// UnityEngine.InputSystem.Utilities.Substring
struct  Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F 
{
public:
	// System.String UnityEngine.InputSystem.Utilities.Substring::m_String
	String_t* ___m_String_0;
	// System.Int32 UnityEngine.InputSystem.Utilities.Substring::m_Index
	int32_t ___m_Index_1;
	// System.Int32 UnityEngine.InputSystem.Utilities.Substring::m_Length
	int32_t ___m_Length_2;

public:
	inline static int32_t get_offset_of_m_String_0() { return static_cast<int32_t>(offsetof(Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F, ___m_String_0)); }
	inline String_t* get_m_String_0() const { return ___m_String_0; }
	inline String_t** get_address_of_m_String_0() { return &___m_String_0; }
	inline void set_m_String_0(String_t* value)
	{
		___m_String_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_String_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Index_1() { return static_cast<int32_t>(offsetof(Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F, ___m_Index_1)); }
	inline int32_t get_m_Index_1() const { return ___m_Index_1; }
	inline int32_t* get_address_of_m_Index_1() { return &___m_Index_1; }
	inline void set_m_Index_1(int32_t value)
	{
		___m_Index_1 = value;
	}

	inline static int32_t get_offset_of_m_Length_2() { return static_cast<int32_t>(offsetof(Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F, ___m_Length_2)); }
	inline int32_t get_m_Length_2() const { return ___m_Length_2; }
	inline int32_t* get_address_of_m_Length_2() { return &___m_Length_2; }
	inline void set_m_Length_2(int32_t value)
	{
		___m_Length_2 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.InputSystem.Utilities.Substring
struct Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F_marshaled_pinvoke
{
	char* ___m_String_0;
	int32_t ___m_Index_1;
	int32_t ___m_Length_2;
};
// Native definition for COM marshalling of UnityEngine.InputSystem.Utilities.Substring
struct Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F_marshaled_com
{
	Il2CppChar* ___m_String_0;
	int32_t ___m_Index_1;
	int32_t ___m_Length_2;
};

// System.Void
struct  Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.InternedString>
struct  Enumerator_tD1ECE103520161C1BD172331D6968D3D184B3B6F 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_tFD37D052760A236906215AF581C58EE13DE6A5DC * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_tD1ECE103520161C1BD172331D6968D3D184B3B6F, ___list_0)); }
	inline List_1_tFD37D052760A236906215AF581C58EE13DE6A5DC * get_list_0() const { return ___list_0; }
	inline List_1_tFD37D052760A236906215AF581C58EE13DE6A5DC ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_tFD37D052760A236906215AF581C58EE13DE6A5DC * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_tD1ECE103520161C1BD172331D6968D3D184B3B6F, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_tD1ECE103520161C1BD172331D6968D3D184B3B6F, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_tD1ECE103520161C1BD172331D6968D3D184B3B6F, ___current_3)); }
	inline InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  get_current_3() const { return ___current_3; }
	inline InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___current_3))->___m_StringOriginalCase_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___current_3))->___m_StringLowerCase_1), (void*)NULL);
		#endif
	}
};


// System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.Substring>
struct  Enumerator_t7C514ACB3FF8E3EFB210F5F45C9F18A715BB4F80 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_tEAD6E3282E028927B32F56E7892994D90D512467 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F  ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_t7C514ACB3FF8E3EFB210F5F45C9F18A715BB4F80, ___list_0)); }
	inline List_1_tEAD6E3282E028927B32F56E7892994D90D512467 * get_list_0() const { return ___list_0; }
	inline List_1_tEAD6E3282E028927B32F56E7892994D90D512467 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_tEAD6E3282E028927B32F56E7892994D90D512467 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_t7C514ACB3FF8E3EFB210F5F45C9F18A715BB4F80, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_t7C514ACB3FF8E3EFB210F5F45C9F18A715BB4F80, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t7C514ACB3FF8E3EFB210F5F45C9F18A715BB4F80, ___current_3)); }
	inline Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F  get_current_3() const { return ___current_3; }
	inline Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F  value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___current_3))->___m_String_0), (void*)NULL);
	}
};


// System.Linq.Enumerable/Iterator`1<UnityEngine.InputSystem.Utilities.InternedString>
struct  Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5  : public RuntimeObject
{
public:
	// System.Int32 System.Linq.Enumerable/Iterator`1::threadId
	int32_t ___threadId_0;
	// System.Int32 System.Linq.Enumerable/Iterator`1::state
	int32_t ___state_1;
	// TSource System.Linq.Enumerable/Iterator`1::current
	InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  ___current_2;

public:
	inline static int32_t get_offset_of_threadId_0() { return static_cast<int32_t>(offsetof(Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5, ___threadId_0)); }
	inline int32_t get_threadId_0() const { return ___threadId_0; }
	inline int32_t* get_address_of_threadId_0() { return &___threadId_0; }
	inline void set_threadId_0(int32_t value)
	{
		___threadId_0 = value;
	}

	inline static int32_t get_offset_of_state_1() { return static_cast<int32_t>(offsetof(Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5, ___state_1)); }
	inline int32_t get_state_1() const { return ___state_1; }
	inline int32_t* get_address_of_state_1() { return &___state_1; }
	inline void set_state_1(int32_t value)
	{
		___state_1 = value;
	}

	inline static int32_t get_offset_of_current_2() { return static_cast<int32_t>(offsetof(Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5, ___current_2)); }
	inline InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  get_current_2() const { return ___current_2; }
	inline InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED * get_address_of_current_2() { return &___current_2; }
	inline void set_current_2(InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  value)
	{
		___current_2 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___current_2))->___m_StringOriginalCase_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___current_2))->___m_StringLowerCase_1), (void*)NULL);
		#endif
	}
};


// System.Linq.Enumerable/WhereListIterator`1<System.Object>
struct  WhereListIterator_1_t42618389DB998070E03A982D15FA39BCA1DB56BD  : public Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereListIterator`1::source
	List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereListIterator`1::predicate
	Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate_4;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereListIterator`1::enumerator
	Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  ___enumerator_5;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereListIterator_1_t42618389DB998070E03A982D15FA39BCA1DB56BD, ___source_3)); }
	inline List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * get_source_3() const { return ___source_3; }
	inline List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereListIterator_1_t42618389DB998070E03A982D15FA39BCA1DB56BD, ___predicate_4)); }
	inline Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_5() { return static_cast<int32_t>(offsetof(WhereListIterator_1_t42618389DB998070E03A982D15FA39BCA1DB56BD, ___enumerator_5)); }
	inline Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  get_enumerator_5() const { return ___enumerator_5; }
	inline Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * get_address_of_enumerator_5() { return &___enumerator_5; }
	inline void set_enumerator_5(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  value)
	{
		___enumerator_5 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_5))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_5))->___current_3), (void*)NULL);
		#endif
	}
};


// System.Linq.Enumerable/WhereSelectListIterator`2<System.Object,System.Object>
struct  WhereSelectListIterator_2_t85B78DFF0573BC95A62C79D6088FA39DFEBE1AF2  : public Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t85B78DFF0573BC95A62C79D6088FA39DFEBE1AF2, ___source_3)); }
	inline List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * get_source_3() const { return ___source_3; }
	inline List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t85B78DFF0573BC95A62C79D6088FA39DFEBE1AF2, ___predicate_4)); }
	inline Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t85B78DFF0573BC95A62C79D6088FA39DFEBE1AF2, ___selector_5)); }
	inline Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 * get_selector_5() const { return ___selector_5; }
	inline Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t85B78DFF0573BC95A62C79D6088FA39DFEBE1AF2, ___enumerator_6)); }
	inline Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___current_3), (void*)NULL);
		#endif
	}
};


// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_target_2), (void*)value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___method_info_7), (void*)value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___original_method_info_8), (void*)value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * get_data_9() const { return ___data_9; }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_9), (void*)value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____className_1), (void*)value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____message_2), (void*)value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____data_3), (void*)value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____innerException_4), (void*)value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____helpURL_5), (void*)value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTrace_6), (void*)value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTraceString_7), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____remoteStackTraceString_8), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____dynamicMethods_10), (void*)value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____source_12), (void*)value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____safeSerializationManager_13), (void*)value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___captured_traces_14), (void*)value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___native_trace_ips_15), (void*)value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_EDILock_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};

// UnityEngine.InputSystem.Utilities.NameAndParameters
struct  NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA 
{
public:
	// System.String UnityEngine.InputSystem.Utilities.NameAndParameters::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_0;
	// UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Utilities.NamedValue> UnityEngine.InputSystem.Utilities.NameAndParameters::<parameters>k__BackingField
	ReadOnlyArray_1_t644486DA5FB2B247FC64733F192E2894F46CFCF5  ___U3CparametersU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA, ___U3CnameU3Ek__BackingField_0)); }
	inline String_t* get_U3CnameU3Ek__BackingField_0() const { return ___U3CnameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_0() { return &___U3CnameU3Ek__BackingField_0; }
	inline void set_U3CnameU3Ek__BackingField_0(String_t* value)
	{
		___U3CnameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CnameU3Ek__BackingField_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CparametersU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA, ___U3CparametersU3Ek__BackingField_1)); }
	inline ReadOnlyArray_1_t644486DA5FB2B247FC64733F192E2894F46CFCF5  get_U3CparametersU3Ek__BackingField_1() const { return ___U3CparametersU3Ek__BackingField_1; }
	inline ReadOnlyArray_1_t644486DA5FB2B247FC64733F192E2894F46CFCF5 * get_address_of_U3CparametersU3Ek__BackingField_1() { return &___U3CparametersU3Ek__BackingField_1; }
	inline void set_U3CparametersU3Ek__BackingField_1(ReadOnlyArray_1_t644486DA5FB2B247FC64733F192E2894F46CFCF5  value)
	{
		___U3CparametersU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CparametersU3Ek__BackingField_1))->___m_Array_0), (void*)NULL);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.InputSystem.Utilities.NameAndParameters
struct NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA_marshaled_pinvoke
{
	char* ___U3CnameU3Ek__BackingField_0;
	ReadOnlyArray_1_t644486DA5FB2B247FC64733F192E2894F46CFCF5  ___U3CparametersU3Ek__BackingField_1;
};
// Native definition for COM marshalling of UnityEngine.InputSystem.Utilities.NameAndParameters
struct NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA_marshaled_com
{
	Il2CppChar* ___U3CnameU3Ek__BackingField_0;
	ReadOnlyArray_1_t644486DA5FB2B247FC64733F192E2894F46CFCF5  ___U3CparametersU3Ek__BackingField_1;
};

// UnityEngine.UIElements.StyleSelectorType
struct  StyleSelectorType_t076854E4D0D1DE5408564915375B2D4AF5F13BD7 
{
public:
	// System.Int32 UnityEngine.UIElements.StyleSelectorType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(StyleSelectorType_t076854E4D0D1DE5408564915375B2D4AF5F13BD7, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.TypeCode
struct  TypeCode_tCB39BAB5CFB7A1E0BCB521413E3C46B81C31AA7C 
{
public:
	// System.Int32 System.TypeCode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TypeCode_tCB39BAB5CFB7A1E0BCB521413E3C46B81C31AA7C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.InputSystem.Utilities.JsonParser/JsonString
struct  JsonString_tB3A1938903F2377F780FF4C4FE7CC6EA81C0D301 
{
public:
	// UnityEngine.InputSystem.Utilities.Substring UnityEngine.InputSystem.Utilities.JsonParser/JsonString::text
	Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F  ___text_0;
	// System.Boolean UnityEngine.InputSystem.Utilities.JsonParser/JsonString::hasEscapes
	bool ___hasEscapes_1;

public:
	inline static int32_t get_offset_of_text_0() { return static_cast<int32_t>(offsetof(JsonString_tB3A1938903F2377F780FF4C4FE7CC6EA81C0D301, ___text_0)); }
	inline Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F  get_text_0() const { return ___text_0; }
	inline Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F * get_address_of_text_0() { return &___text_0; }
	inline void set_text_0(Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F  value)
	{
		___text_0 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___text_0))->___m_String_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_hasEscapes_1() { return static_cast<int32_t>(offsetof(JsonString_tB3A1938903F2377F780FF4C4FE7CC6EA81C0D301, ___hasEscapes_1)); }
	inline bool get_hasEscapes_1() const { return ___hasEscapes_1; }
	inline bool* get_address_of_hasEscapes_1() { return &___hasEscapes_1; }
	inline void set_hasEscapes_1(bool value)
	{
		___hasEscapes_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.InputSystem.Utilities.JsonParser/JsonString
struct JsonString_tB3A1938903F2377F780FF4C4FE7CC6EA81C0D301_marshaled_pinvoke
{
	Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F_marshaled_pinvoke ___text_0;
	int32_t ___hasEscapes_1;
};
// Native definition for COM marshalling of UnityEngine.InputSystem.Utilities.JsonParser/JsonString
struct JsonString_tB3A1938903F2377F780FF4C4FE7CC6EA81C0D301_marshaled_com
{
	Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F_marshaled_com ___text_0;
	int32_t ___hasEscapes_1;
};

// UnityEngine.InputSystem.Utilities.JsonParser/JsonValueType
struct  JsonValueType_t536726C35383E9602EC7AE443D8CFC95C5F35540 
{
public:
	// System.Int32 UnityEngine.InputSystem.Utilities.JsonParser/JsonValueType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(JsonValueType_t536726C35383E9602EC7AE443D8CFC95C5F35540, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.InputSystem.Layouts.InputControlLayout/ControlItem/Flags
struct  Flags_t6F7599E4924E4F24DE5613340EEA1E222928DE34 
{
public:
	// System.Int32 UnityEngine.InputSystem.Layouts.InputControlLayout/ControlItem/Flags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Flags_t6F7599E4924E4F24DE5613340EEA1E222928DE34, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.NameAndParameters>
struct  Enumerator_tA2E0F83E15EA46A2B4B08AFB750700C6E1387FC0 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_tCBF5EB99F2E538DCB47A8C835DD4B9C024879393 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA  ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_tA2E0F83E15EA46A2B4B08AFB750700C6E1387FC0, ___list_0)); }
	inline List_1_tCBF5EB99F2E538DCB47A8C835DD4B9C024879393 * get_list_0() const { return ___list_0; }
	inline List_1_tCBF5EB99F2E538DCB47A8C835DD4B9C024879393 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_tCBF5EB99F2E538DCB47A8C835DD4B9C024879393 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_tA2E0F83E15EA46A2B4B08AFB750700C6E1387FC0, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_tA2E0F83E15EA46A2B4B08AFB750700C6E1387FC0, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_tA2E0F83E15EA46A2B4B08AFB750700C6E1387FC0, ___current_3)); }
	inline NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA  get_current_3() const { return ___current_3; }
	inline NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA  value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___current_3))->___U3CnameU3Ek__BackingField_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___current_3))->___U3CparametersU3Ek__BackingField_1))->___m_Array_0), (void*)NULL);
		#endif
	}
};


// System.Linq.Enumerable/WhereEnumerableIterator`1<UnityEngine.InputSystem.Utilities.InternedString>
struct  WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B  : public Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereEnumerableIterator`1::source
	RuntimeObject* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereEnumerableIterator`1::predicate
	Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * ___predicate_4;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/WhereEnumerableIterator`1::enumerator
	RuntimeObject* ___enumerator_5;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B, ___source_3)); }
	inline RuntimeObject* get_source_3() const { return ___source_3; }
	inline RuntimeObject** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(RuntimeObject* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B, ___predicate_4)); }
	inline Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_5() { return static_cast<int32_t>(offsetof(WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B, ___enumerator_5)); }
	inline RuntimeObject* get_enumerator_5() const { return ___enumerator_5; }
	inline RuntimeObject** get_address_of_enumerator_5() { return &___enumerator_5; }
	inline void set_enumerator_5(RuntimeObject* value)
	{
		___enumerator_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumerator_5), (void*)value);
	}
};


// System.Linq.Enumerable/WhereSelectArrayIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,UnityEngine.InputSystem.Utilities.InternedString>
struct  WhereSelectArrayIterator_2_tB9FDF4ABA25271C5465121B8CD088BB04D7A03F9  : public Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5
{
public:
	// TSource[] System.Linq.Enumerable/WhereSelectArrayIterator`2::source
	KeyValuePair_2U5BU5D_tC4ED6A2024348F6B2CB712DC491F7E57AEF32630* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectArrayIterator`2::predicate
	Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectArrayIterator`2::selector
	Func_2_tB181F4C9A24C0634DF4A10CFBFFE6B45B3796BBC * ___selector_5;
	// System.Int32 System.Linq.Enumerable/WhereSelectArrayIterator`2::index
	int32_t ___index_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectArrayIterator_2_tB9FDF4ABA25271C5465121B8CD088BB04D7A03F9, ___source_3)); }
	inline KeyValuePair_2U5BU5D_tC4ED6A2024348F6B2CB712DC491F7E57AEF32630* get_source_3() const { return ___source_3; }
	inline KeyValuePair_2U5BU5D_tC4ED6A2024348F6B2CB712DC491F7E57AEF32630** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(KeyValuePair_2U5BU5D_tC4ED6A2024348F6B2CB712DC491F7E57AEF32630* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectArrayIterator_2_tB9FDF4ABA25271C5465121B8CD088BB04D7A03F9, ___predicate_4)); }
	inline Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectArrayIterator_2_tB9FDF4ABA25271C5465121B8CD088BB04D7A03F9, ___selector_5)); }
	inline Func_2_tB181F4C9A24C0634DF4A10CFBFFE6B45B3796BBC * get_selector_5() const { return ___selector_5; }
	inline Func_2_tB181F4C9A24C0634DF4A10CFBFFE6B45B3796BBC ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_tB181F4C9A24C0634DF4A10CFBFFE6B45B3796BBC * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_index_6() { return static_cast<int32_t>(offsetof(WhereSelectArrayIterator_2_tB9FDF4ABA25271C5465121B8CD088BB04D7A03F9, ___index_6)); }
	inline int32_t get_index_6() const { return ___index_6; }
	inline int32_t* get_address_of_index_6() { return &___index_6; }
	inline void set_index_6(int32_t value)
	{
		___index_6 = value;
	}
};


// System.Linq.Enumerable/WhereSelectArrayIterator`2<UnityEngine.InputSystem.Utilities.InternedString,UnityEngine.InputSystem.Utilities.InternedString>
struct  WhereSelectArrayIterator_2_t45B551CB9059F02D47B0F25BE9ECC3561DF4A23D  : public Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5
{
public:
	// TSource[] System.Linq.Enumerable/WhereSelectArrayIterator`2::source
	InternedStringU5BU5D_t1EAE8433C8EF7C76894673801EE0EE9F6833E9A4* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectArrayIterator`2::predicate
	Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectArrayIterator`2::selector
	Func_2_t1AF155D27A4D78882342D697E4A6DE7CE594545B * ___selector_5;
	// System.Int32 System.Linq.Enumerable/WhereSelectArrayIterator`2::index
	int32_t ___index_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectArrayIterator_2_t45B551CB9059F02D47B0F25BE9ECC3561DF4A23D, ___source_3)); }
	inline InternedStringU5BU5D_t1EAE8433C8EF7C76894673801EE0EE9F6833E9A4* get_source_3() const { return ___source_3; }
	inline InternedStringU5BU5D_t1EAE8433C8EF7C76894673801EE0EE9F6833E9A4** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(InternedStringU5BU5D_t1EAE8433C8EF7C76894673801EE0EE9F6833E9A4* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectArrayIterator_2_t45B551CB9059F02D47B0F25BE9ECC3561DF4A23D, ___predicate_4)); }
	inline Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectArrayIterator_2_t45B551CB9059F02D47B0F25BE9ECC3561DF4A23D, ___selector_5)); }
	inline Func_2_t1AF155D27A4D78882342D697E4A6DE7CE594545B * get_selector_5() const { return ___selector_5; }
	inline Func_2_t1AF155D27A4D78882342D697E4A6DE7CE594545B ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t1AF155D27A4D78882342D697E4A6DE7CE594545B * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_index_6() { return static_cast<int32_t>(offsetof(WhereSelectArrayIterator_2_t45B551CB9059F02D47B0F25BE9ECC3561DF4A23D, ___index_6)); }
	inline int32_t get_index_6() const { return ___index_6; }
	inline int32_t* get_address_of_index_6() { return &___index_6; }
	inline void set_index_6(int32_t value)
	{
		___index_6 = value;
	}
};


// System.Linq.Enumerable/WhereSelectArrayIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,UnityEngine.InputSystem.Utilities.InternedString>
struct  WhereSelectArrayIterator_2_tA56FBB916D6D4ED801C2818530D724C0C701D490  : public Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5
{
public:
	// TSource[] System.Linq.Enumerable/WhereSelectArrayIterator`2::source
	NameAndParametersU5BU5D_t960DDB52F44DD1231B0C626E9C859F1E0B2F5EAD* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectArrayIterator`2::predicate
	Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectArrayIterator`2::selector
	Func_2_tEA2235809AB0E1AB8425745179691D76B519B0C5 * ___selector_5;
	// System.Int32 System.Linq.Enumerable/WhereSelectArrayIterator`2::index
	int32_t ___index_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectArrayIterator_2_tA56FBB916D6D4ED801C2818530D724C0C701D490, ___source_3)); }
	inline NameAndParametersU5BU5D_t960DDB52F44DD1231B0C626E9C859F1E0B2F5EAD* get_source_3() const { return ___source_3; }
	inline NameAndParametersU5BU5D_t960DDB52F44DD1231B0C626E9C859F1E0B2F5EAD** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(NameAndParametersU5BU5D_t960DDB52F44DD1231B0C626E9C859F1E0B2F5EAD* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectArrayIterator_2_tA56FBB916D6D4ED801C2818530D724C0C701D490, ___predicate_4)); }
	inline Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectArrayIterator_2_tA56FBB916D6D4ED801C2818530D724C0C701D490, ___selector_5)); }
	inline Func_2_tEA2235809AB0E1AB8425745179691D76B519B0C5 * get_selector_5() const { return ___selector_5; }
	inline Func_2_tEA2235809AB0E1AB8425745179691D76B519B0C5 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_tEA2235809AB0E1AB8425745179691D76B519B0C5 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_index_6() { return static_cast<int32_t>(offsetof(WhereSelectArrayIterator_2_tA56FBB916D6D4ED801C2818530D724C0C701D490, ___index_6)); }
	inline int32_t get_index_6() const { return ___index_6; }
	inline int32_t* get_address_of_index_6() { return &___index_6; }
	inline void set_index_6(int32_t value)
	{
		___index_6 = value;
	}
};


// System.Linq.Enumerable/WhereSelectArrayIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,UnityEngine.InputSystem.Utilities.InternedString>
struct  WhereSelectArrayIterator_2_t81C380953EA0951A36339AC8CE79A9B039372941  : public Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5
{
public:
	// TSource[] System.Linq.Enumerable/WhereSelectArrayIterator`2::source
	NamedValueU5BU5D_t16F5D30B70B5FEDA89AE16CFD61FACEC7DF85391* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectArrayIterator`2::predicate
	Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectArrayIterator`2::selector
	Func_2_tA21678F1D9F9F4617D645755285D28EDE5F9AE44 * ___selector_5;
	// System.Int32 System.Linq.Enumerable/WhereSelectArrayIterator`2::index
	int32_t ___index_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectArrayIterator_2_t81C380953EA0951A36339AC8CE79A9B039372941, ___source_3)); }
	inline NamedValueU5BU5D_t16F5D30B70B5FEDA89AE16CFD61FACEC7DF85391* get_source_3() const { return ___source_3; }
	inline NamedValueU5BU5D_t16F5D30B70B5FEDA89AE16CFD61FACEC7DF85391** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(NamedValueU5BU5D_t16F5D30B70B5FEDA89AE16CFD61FACEC7DF85391* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectArrayIterator_2_t81C380953EA0951A36339AC8CE79A9B039372941, ___predicate_4)); }
	inline Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectArrayIterator_2_t81C380953EA0951A36339AC8CE79A9B039372941, ___selector_5)); }
	inline Func_2_tA21678F1D9F9F4617D645755285D28EDE5F9AE44 * get_selector_5() const { return ___selector_5; }
	inline Func_2_tA21678F1D9F9F4617D645755285D28EDE5F9AE44 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_tA21678F1D9F9F4617D645755285D28EDE5F9AE44 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_index_6() { return static_cast<int32_t>(offsetof(WhereSelectArrayIterator_2_t81C380953EA0951A36339AC8CE79A9B039372941, ___index_6)); }
	inline int32_t get_index_6() const { return ___index_6; }
	inline int32_t* get_address_of_index_6() { return &___index_6; }
	inline void set_index_6(int32_t value)
	{
		___index_6 = value;
	}
};


// System.Linq.Enumerable/WhereSelectArrayIterator`2<System.Object,UnityEngine.InputSystem.Utilities.InternedString>
struct  WhereSelectArrayIterator_2_t2A766812BE346746DE135AD9B855143B4FF483E4  : public Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5
{
public:
	// TSource[] System.Linq.Enumerable/WhereSelectArrayIterator`2::source
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectArrayIterator`2::predicate
	Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectArrayIterator`2::selector
	Func_2_t964229B6ADD2B008268547ED2BC3C090AF218065 * ___selector_5;
	// System.Int32 System.Linq.Enumerable/WhereSelectArrayIterator`2::index
	int32_t ___index_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectArrayIterator_2_t2A766812BE346746DE135AD9B855143B4FF483E4, ___source_3)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get_source_3() const { return ___source_3; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectArrayIterator_2_t2A766812BE346746DE135AD9B855143B4FF483E4, ___predicate_4)); }
	inline Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectArrayIterator_2_t2A766812BE346746DE135AD9B855143B4FF483E4, ___selector_5)); }
	inline Func_2_t964229B6ADD2B008268547ED2BC3C090AF218065 * get_selector_5() const { return ___selector_5; }
	inline Func_2_t964229B6ADD2B008268547ED2BC3C090AF218065 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t964229B6ADD2B008268547ED2BC3C090AF218065 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_index_6() { return static_cast<int32_t>(offsetof(WhereSelectArrayIterator_2_t2A766812BE346746DE135AD9B855143B4FF483E4, ___index_6)); }
	inline int32_t get_index_6() const { return ___index_6; }
	inline int32_t* get_address_of_index_6() { return &___index_6; }
	inline void set_index_6(int32_t value)
	{
		___index_6 = value;
	}
};


// System.Linq.Enumerable/WhereSelectArrayIterator`2<UnityEngine.UIElements.StyleSelectorPart,UnityEngine.InputSystem.Utilities.InternedString>
struct  WhereSelectArrayIterator_2_t0EBFFCD87F6BDF27E6B0AB944A8106A0C0ABC3AE  : public Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5
{
public:
	// TSource[] System.Linq.Enumerable/WhereSelectArrayIterator`2::source
	StyleSelectorPartU5BU5D_tD8762DADE4104C7D73D7B821DF7E5BBC87417BB7* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectArrayIterator`2::predicate
	Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectArrayIterator`2::selector
	Func_2_tFBBF89B72C697489D82FBE8FF822B191D2EE9A41 * ___selector_5;
	// System.Int32 System.Linq.Enumerable/WhereSelectArrayIterator`2::index
	int32_t ___index_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectArrayIterator_2_t0EBFFCD87F6BDF27E6B0AB944A8106A0C0ABC3AE, ___source_3)); }
	inline StyleSelectorPartU5BU5D_tD8762DADE4104C7D73D7B821DF7E5BBC87417BB7* get_source_3() const { return ___source_3; }
	inline StyleSelectorPartU5BU5D_tD8762DADE4104C7D73D7B821DF7E5BBC87417BB7** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(StyleSelectorPartU5BU5D_tD8762DADE4104C7D73D7B821DF7E5BBC87417BB7* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectArrayIterator_2_t0EBFFCD87F6BDF27E6B0AB944A8106A0C0ABC3AE, ___predicate_4)); }
	inline Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectArrayIterator_2_t0EBFFCD87F6BDF27E6B0AB944A8106A0C0ABC3AE, ___selector_5)); }
	inline Func_2_tFBBF89B72C697489D82FBE8FF822B191D2EE9A41 * get_selector_5() const { return ___selector_5; }
	inline Func_2_tFBBF89B72C697489D82FBE8FF822B191D2EE9A41 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_tFBBF89B72C697489D82FBE8FF822B191D2EE9A41 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_index_6() { return static_cast<int32_t>(offsetof(WhereSelectArrayIterator_2_t0EBFFCD87F6BDF27E6B0AB944A8106A0C0ABC3AE, ___index_6)); }
	inline int32_t get_index_6() const { return ___index_6; }
	inline int32_t* get_address_of_index_6() { return &___index_6; }
	inline void set_index_6(int32_t value)
	{
		___index_6 = value;
	}
};


// System.Linq.Enumerable/WhereSelectArrayIterator`2<UnityEngine.InputSystem.Utilities.Substring,UnityEngine.InputSystem.Utilities.InternedString>
struct  WhereSelectArrayIterator_2_t17AAB090C1BEA20967939542652510CB0E77A3FB  : public Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5
{
public:
	// TSource[] System.Linq.Enumerable/WhereSelectArrayIterator`2::source
	SubstringU5BU5D_tE332884993470F5CF7BAAA950414E4E24691A8D0* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectArrayIterator`2::predicate
	Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectArrayIterator`2::selector
	Func_2_t14F6F4D7FBF558A20782EDC48D80473E4800048E * ___selector_5;
	// System.Int32 System.Linq.Enumerable/WhereSelectArrayIterator`2::index
	int32_t ___index_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectArrayIterator_2_t17AAB090C1BEA20967939542652510CB0E77A3FB, ___source_3)); }
	inline SubstringU5BU5D_tE332884993470F5CF7BAAA950414E4E24691A8D0* get_source_3() const { return ___source_3; }
	inline SubstringU5BU5D_tE332884993470F5CF7BAAA950414E4E24691A8D0** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(SubstringU5BU5D_tE332884993470F5CF7BAAA950414E4E24691A8D0* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectArrayIterator_2_t17AAB090C1BEA20967939542652510CB0E77A3FB, ___predicate_4)); }
	inline Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectArrayIterator_2_t17AAB090C1BEA20967939542652510CB0E77A3FB, ___selector_5)); }
	inline Func_2_t14F6F4D7FBF558A20782EDC48D80473E4800048E * get_selector_5() const { return ___selector_5; }
	inline Func_2_t14F6F4D7FBF558A20782EDC48D80473E4800048E ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t14F6F4D7FBF558A20782EDC48D80473E4800048E * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_index_6() { return static_cast<int32_t>(offsetof(WhereSelectArrayIterator_2_t17AAB090C1BEA20967939542652510CB0E77A3FB, ___index_6)); }
	inline int32_t get_index_6() const { return ___index_6; }
	inline int32_t* get_address_of_index_6() { return &___index_6; }
	inline void set_index_6(int32_t value)
	{
		___index_6 = value;
	}
};


// System.Linq.Enumerable/WhereSelectArrayIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,UnityEngine.InputSystem.Utilities.InternedString>
struct  WhereSelectArrayIterator_2_t0255B8DCC00DD4A15730FE65749D22B88FA338F1  : public Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5
{
public:
	// TSource[] System.Linq.Enumerable/WhereSelectArrayIterator`2::source
	JsonValueU5BU5D_t2E1CBAA099182C024E4921379907D707DA049098* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectArrayIterator`2::predicate
	Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectArrayIterator`2::selector
	Func_2_t48DD1C8E439811C492BAFE220A679DB57D7C9370 * ___selector_5;
	// System.Int32 System.Linq.Enumerable/WhereSelectArrayIterator`2::index
	int32_t ___index_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectArrayIterator_2_t0255B8DCC00DD4A15730FE65749D22B88FA338F1, ___source_3)); }
	inline JsonValueU5BU5D_t2E1CBAA099182C024E4921379907D707DA049098* get_source_3() const { return ___source_3; }
	inline JsonValueU5BU5D_t2E1CBAA099182C024E4921379907D707DA049098** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(JsonValueU5BU5D_t2E1CBAA099182C024E4921379907D707DA049098* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectArrayIterator_2_t0255B8DCC00DD4A15730FE65749D22B88FA338F1, ___predicate_4)); }
	inline Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectArrayIterator_2_t0255B8DCC00DD4A15730FE65749D22B88FA338F1, ___selector_5)); }
	inline Func_2_t48DD1C8E439811C492BAFE220A679DB57D7C9370 * get_selector_5() const { return ___selector_5; }
	inline Func_2_t48DD1C8E439811C492BAFE220A679DB57D7C9370 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t48DD1C8E439811C492BAFE220A679DB57D7C9370 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_index_6() { return static_cast<int32_t>(offsetof(WhereSelectArrayIterator_2_t0255B8DCC00DD4A15730FE65749D22B88FA338F1, ___index_6)); }
	inline int32_t get_index_6() const { return ___index_6; }
	inline int32_t* get_address_of_index_6() { return &___index_6; }
	inline void set_index_6(int32_t value)
	{
		___index_6 = value;
	}
};


// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,UnityEngine.InputSystem.Utilities.InternedString>
struct  WhereSelectEnumerableIterator_2_t5171F478E1C976F4C353EA25EC64567E44C86CA0  : public Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::source
	RuntimeObject* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::predicate
	Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::selector
	Func_2_tB181F4C9A24C0634DF4A10CFBFFE6B45B3796BBC * ___selector_5;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::enumerator
	RuntimeObject* ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t5171F478E1C976F4C353EA25EC64567E44C86CA0, ___source_3)); }
	inline RuntimeObject* get_source_3() const { return ___source_3; }
	inline RuntimeObject** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(RuntimeObject* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t5171F478E1C976F4C353EA25EC64567E44C86CA0, ___predicate_4)); }
	inline Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t5171F478E1C976F4C353EA25EC64567E44C86CA0, ___selector_5)); }
	inline Func_2_tB181F4C9A24C0634DF4A10CFBFFE6B45B3796BBC * get_selector_5() const { return ___selector_5; }
	inline Func_2_tB181F4C9A24C0634DF4A10CFBFFE6B45B3796BBC ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_tB181F4C9A24C0634DF4A10CFBFFE6B45B3796BBC * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t5171F478E1C976F4C353EA25EC64567E44C86CA0, ___enumerator_6)); }
	inline RuntimeObject* get_enumerator_6() const { return ___enumerator_6; }
	inline RuntimeObject** get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(RuntimeObject* value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumerator_6), (void*)value);
	}
};


// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.InternedString,UnityEngine.InputSystem.Utilities.InternedString>
struct  WhereSelectEnumerableIterator_2_t0EB79FD57DE6939EBF78FA1D92312CB62DD3F2C6  : public Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::source
	RuntimeObject* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::predicate
	Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::selector
	Func_2_t1AF155D27A4D78882342D697E4A6DE7CE594545B * ___selector_5;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::enumerator
	RuntimeObject* ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t0EB79FD57DE6939EBF78FA1D92312CB62DD3F2C6, ___source_3)); }
	inline RuntimeObject* get_source_3() const { return ___source_3; }
	inline RuntimeObject** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(RuntimeObject* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t0EB79FD57DE6939EBF78FA1D92312CB62DD3F2C6, ___predicate_4)); }
	inline Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t0EB79FD57DE6939EBF78FA1D92312CB62DD3F2C6, ___selector_5)); }
	inline Func_2_t1AF155D27A4D78882342D697E4A6DE7CE594545B * get_selector_5() const { return ___selector_5; }
	inline Func_2_t1AF155D27A4D78882342D697E4A6DE7CE594545B ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t1AF155D27A4D78882342D697E4A6DE7CE594545B * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t0EB79FD57DE6939EBF78FA1D92312CB62DD3F2C6, ___enumerator_6)); }
	inline RuntimeObject* get_enumerator_6() const { return ___enumerator_6; }
	inline RuntimeObject** get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(RuntimeObject* value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumerator_6), (void*)value);
	}
};


// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,UnityEngine.InputSystem.Utilities.InternedString>
struct  WhereSelectEnumerableIterator_2_t3B8EA5ED092FA87DEAECF94C466A54501B692B0B  : public Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::source
	RuntimeObject* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::predicate
	Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::selector
	Func_2_tEA2235809AB0E1AB8425745179691D76B519B0C5 * ___selector_5;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::enumerator
	RuntimeObject* ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t3B8EA5ED092FA87DEAECF94C466A54501B692B0B, ___source_3)); }
	inline RuntimeObject* get_source_3() const { return ___source_3; }
	inline RuntimeObject** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(RuntimeObject* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t3B8EA5ED092FA87DEAECF94C466A54501B692B0B, ___predicate_4)); }
	inline Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t3B8EA5ED092FA87DEAECF94C466A54501B692B0B, ___selector_5)); }
	inline Func_2_tEA2235809AB0E1AB8425745179691D76B519B0C5 * get_selector_5() const { return ___selector_5; }
	inline Func_2_tEA2235809AB0E1AB8425745179691D76B519B0C5 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_tEA2235809AB0E1AB8425745179691D76B519B0C5 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t3B8EA5ED092FA87DEAECF94C466A54501B692B0B, ___enumerator_6)); }
	inline RuntimeObject* get_enumerator_6() const { return ___enumerator_6; }
	inline RuntimeObject** get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(RuntimeObject* value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumerator_6), (void*)value);
	}
};


// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,UnityEngine.InputSystem.Utilities.InternedString>
struct  WhereSelectEnumerableIterator_2_t382E0095C883AA01020D10E08F7F38BBA26A3EC5  : public Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::source
	RuntimeObject* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::predicate
	Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::selector
	Func_2_tA21678F1D9F9F4617D645755285D28EDE5F9AE44 * ___selector_5;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::enumerator
	RuntimeObject* ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t382E0095C883AA01020D10E08F7F38BBA26A3EC5, ___source_3)); }
	inline RuntimeObject* get_source_3() const { return ___source_3; }
	inline RuntimeObject** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(RuntimeObject* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t382E0095C883AA01020D10E08F7F38BBA26A3EC5, ___predicate_4)); }
	inline Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t382E0095C883AA01020D10E08F7F38BBA26A3EC5, ___selector_5)); }
	inline Func_2_tA21678F1D9F9F4617D645755285D28EDE5F9AE44 * get_selector_5() const { return ___selector_5; }
	inline Func_2_tA21678F1D9F9F4617D645755285D28EDE5F9AE44 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_tA21678F1D9F9F4617D645755285D28EDE5F9AE44 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t382E0095C883AA01020D10E08F7F38BBA26A3EC5, ___enumerator_6)); }
	inline RuntimeObject* get_enumerator_6() const { return ___enumerator_6; }
	inline RuntimeObject** get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(RuntimeObject* value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumerator_6), (void*)value);
	}
};


// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Object,UnityEngine.InputSystem.Utilities.InternedString>
struct  WhereSelectEnumerableIterator_2_tE487654732A8A2F1E3A2D57412C08036EE861F38  : public Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::source
	RuntimeObject* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::predicate
	Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::selector
	Func_2_t964229B6ADD2B008268547ED2BC3C090AF218065 * ___selector_5;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::enumerator
	RuntimeObject* ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tE487654732A8A2F1E3A2D57412C08036EE861F38, ___source_3)); }
	inline RuntimeObject* get_source_3() const { return ___source_3; }
	inline RuntimeObject** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(RuntimeObject* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tE487654732A8A2F1E3A2D57412C08036EE861F38, ___predicate_4)); }
	inline Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tE487654732A8A2F1E3A2D57412C08036EE861F38, ___selector_5)); }
	inline Func_2_t964229B6ADD2B008268547ED2BC3C090AF218065 * get_selector_5() const { return ___selector_5; }
	inline Func_2_t964229B6ADD2B008268547ED2BC3C090AF218065 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t964229B6ADD2B008268547ED2BC3C090AF218065 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tE487654732A8A2F1E3A2D57412C08036EE861F38, ___enumerator_6)); }
	inline RuntimeObject* get_enumerator_6() const { return ___enumerator_6; }
	inline RuntimeObject** get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(RuntimeObject* value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumerator_6), (void*)value);
	}
};


// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.UIElements.StyleSelectorPart,UnityEngine.InputSystem.Utilities.InternedString>
struct  WhereSelectEnumerableIterator_2_tCCC50B2F34EC31A45C5FDC2C7A98279D4C71C99B  : public Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::source
	RuntimeObject* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::predicate
	Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::selector
	Func_2_tFBBF89B72C697489D82FBE8FF822B191D2EE9A41 * ___selector_5;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::enumerator
	RuntimeObject* ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tCCC50B2F34EC31A45C5FDC2C7A98279D4C71C99B, ___source_3)); }
	inline RuntimeObject* get_source_3() const { return ___source_3; }
	inline RuntimeObject** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(RuntimeObject* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tCCC50B2F34EC31A45C5FDC2C7A98279D4C71C99B, ___predicate_4)); }
	inline Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tCCC50B2F34EC31A45C5FDC2C7A98279D4C71C99B, ___selector_5)); }
	inline Func_2_tFBBF89B72C697489D82FBE8FF822B191D2EE9A41 * get_selector_5() const { return ___selector_5; }
	inline Func_2_tFBBF89B72C697489D82FBE8FF822B191D2EE9A41 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_tFBBF89B72C697489D82FBE8FF822B191D2EE9A41 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tCCC50B2F34EC31A45C5FDC2C7A98279D4C71C99B, ___enumerator_6)); }
	inline RuntimeObject* get_enumerator_6() const { return ___enumerator_6; }
	inline RuntimeObject** get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(RuntimeObject* value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumerator_6), (void*)value);
	}
};


// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.Substring,UnityEngine.InputSystem.Utilities.InternedString>
struct  WhereSelectEnumerableIterator_2_tFDFC244728E14BAC93BE9869A64080C38A0B7F1D  : public Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::source
	RuntimeObject* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::predicate
	Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::selector
	Func_2_t14F6F4D7FBF558A20782EDC48D80473E4800048E * ___selector_5;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::enumerator
	RuntimeObject* ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tFDFC244728E14BAC93BE9869A64080C38A0B7F1D, ___source_3)); }
	inline RuntimeObject* get_source_3() const { return ___source_3; }
	inline RuntimeObject** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(RuntimeObject* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tFDFC244728E14BAC93BE9869A64080C38A0B7F1D, ___predicate_4)); }
	inline Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tFDFC244728E14BAC93BE9869A64080C38A0B7F1D, ___selector_5)); }
	inline Func_2_t14F6F4D7FBF558A20782EDC48D80473E4800048E * get_selector_5() const { return ___selector_5; }
	inline Func_2_t14F6F4D7FBF558A20782EDC48D80473E4800048E ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t14F6F4D7FBF558A20782EDC48D80473E4800048E * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tFDFC244728E14BAC93BE9869A64080C38A0B7F1D, ___enumerator_6)); }
	inline RuntimeObject* get_enumerator_6() const { return ___enumerator_6; }
	inline RuntimeObject** get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(RuntimeObject* value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumerator_6), (void*)value);
	}
};


// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,UnityEngine.InputSystem.Utilities.InternedString>
struct  WhereSelectEnumerableIterator_2_t6D595CFCE85C8B8D77C138F46CCF2608E20474FE  : public Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::source
	RuntimeObject* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::predicate
	Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::selector
	Func_2_t48DD1C8E439811C492BAFE220A679DB57D7C9370 * ___selector_5;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::enumerator
	RuntimeObject* ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t6D595CFCE85C8B8D77C138F46CCF2608E20474FE, ___source_3)); }
	inline RuntimeObject* get_source_3() const { return ___source_3; }
	inline RuntimeObject** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(RuntimeObject* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t6D595CFCE85C8B8D77C138F46CCF2608E20474FE, ___predicate_4)); }
	inline Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t6D595CFCE85C8B8D77C138F46CCF2608E20474FE, ___selector_5)); }
	inline Func_2_t48DD1C8E439811C492BAFE220A679DB57D7C9370 * get_selector_5() const { return ___selector_5; }
	inline Func_2_t48DD1C8E439811C492BAFE220A679DB57D7C9370 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t48DD1C8E439811C492BAFE220A679DB57D7C9370 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t6D595CFCE85C8B8D77C138F46CCF2608E20474FE, ___enumerator_6)); }
	inline RuntimeObject* get_enumerator_6() const { return ___enumerator_6; }
	inline RuntimeObject** get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(RuntimeObject* value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumerator_6), (void*)value);
	}
};


// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.InternedString,UnityEngine.InputSystem.Utilities.InternedString>
struct  WhereSelectListIterator_2_t7BDF9DF4ED7F42E4D8139C4BA35A0F055024799E  : public Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_tFD37D052760A236906215AF581C58EE13DE6A5DC * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_t1AF155D27A4D78882342D697E4A6DE7CE594545B * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_tD1ECE103520161C1BD172331D6968D3D184B3B6F  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t7BDF9DF4ED7F42E4D8139C4BA35A0F055024799E, ___source_3)); }
	inline List_1_tFD37D052760A236906215AF581C58EE13DE6A5DC * get_source_3() const { return ___source_3; }
	inline List_1_tFD37D052760A236906215AF581C58EE13DE6A5DC ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_tFD37D052760A236906215AF581C58EE13DE6A5DC * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t7BDF9DF4ED7F42E4D8139C4BA35A0F055024799E, ___predicate_4)); }
	inline Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t7BDF9DF4ED7F42E4D8139C4BA35A0F055024799E, ___selector_5)); }
	inline Func_2_t1AF155D27A4D78882342D697E4A6DE7CE594545B * get_selector_5() const { return ___selector_5; }
	inline Func_2_t1AF155D27A4D78882342D697E4A6DE7CE594545B ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t1AF155D27A4D78882342D697E4A6DE7CE594545B * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t7BDF9DF4ED7F42E4D8139C4BA35A0F055024799E, ___enumerator_6)); }
	inline Enumerator_tD1ECE103520161C1BD172331D6968D3D184B3B6F  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_tD1ECE103520161C1BD172331D6968D3D184B3B6F * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_tD1ECE103520161C1BD172331D6968D3D184B3B6F  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___m_StringOriginalCase_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___m_StringLowerCase_1), (void*)NULL);
		#endif
	}
};


// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.InternedString,System.Object>
struct  WhereSelectListIterator_2_t1C93CE1377887193667292BEED953C31167E2F75  : public Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_tFD37D052760A236906215AF581C58EE13DE6A5DC * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_tA2BD34BB8D97AD282016BD4CB5B8675E4AF85B45 * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_tD1ECE103520161C1BD172331D6968D3D184B3B6F  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t1C93CE1377887193667292BEED953C31167E2F75, ___source_3)); }
	inline List_1_tFD37D052760A236906215AF581C58EE13DE6A5DC * get_source_3() const { return ___source_3; }
	inline List_1_tFD37D052760A236906215AF581C58EE13DE6A5DC ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_tFD37D052760A236906215AF581C58EE13DE6A5DC * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t1C93CE1377887193667292BEED953C31167E2F75, ___predicate_4)); }
	inline Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t1C93CE1377887193667292BEED953C31167E2F75, ___selector_5)); }
	inline Func_2_tA2BD34BB8D97AD282016BD4CB5B8675E4AF85B45 * get_selector_5() const { return ___selector_5; }
	inline Func_2_tA2BD34BB8D97AD282016BD4CB5B8675E4AF85B45 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_tA2BD34BB8D97AD282016BD4CB5B8675E4AF85B45 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t1C93CE1377887193667292BEED953C31167E2F75, ___enumerator_6)); }
	inline Enumerator_tD1ECE103520161C1BD172331D6968D3D184B3B6F  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_tD1ECE103520161C1BD172331D6968D3D184B3B6F * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_tD1ECE103520161C1BD172331D6968D3D184B3B6F  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___m_StringOriginalCase_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___m_StringLowerCase_1), (void*)NULL);
		#endif
	}
};


// System.Linq.Enumerable/WhereSelectListIterator`2<System.Object,UnityEngine.InputSystem.Utilities.InternedString>
struct  WhereSelectListIterator_2_t4F7AA31B4289096C5D625263B8BBDA051331BE94  : public Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_t964229B6ADD2B008268547ED2BC3C090AF218065 * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t4F7AA31B4289096C5D625263B8BBDA051331BE94, ___source_3)); }
	inline List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * get_source_3() const { return ___source_3; }
	inline List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t4F7AA31B4289096C5D625263B8BBDA051331BE94, ___predicate_4)); }
	inline Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t4F7AA31B4289096C5D625263B8BBDA051331BE94, ___selector_5)); }
	inline Func_2_t964229B6ADD2B008268547ED2BC3C090AF218065 * get_selector_5() const { return ___selector_5; }
	inline Func_2_t964229B6ADD2B008268547ED2BC3C090AF218065 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t964229B6ADD2B008268547ED2BC3C090AF218065 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t4F7AA31B4289096C5D625263B8BBDA051331BE94, ___enumerator_6)); }
	inline Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___current_3), (void*)NULL);
		#endif
	}
};


// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.Substring,UnityEngine.InputSystem.Utilities.InternedString>
struct  WhereSelectListIterator_2_t59101674E6772D817227A6D5C222AF34A4E0BBAD  : public Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_tEAD6E3282E028927B32F56E7892994D90D512467 * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_t14F6F4D7FBF558A20782EDC48D80473E4800048E * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_t7C514ACB3FF8E3EFB210F5F45C9F18A715BB4F80  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t59101674E6772D817227A6D5C222AF34A4E0BBAD, ___source_3)); }
	inline List_1_tEAD6E3282E028927B32F56E7892994D90D512467 * get_source_3() const { return ___source_3; }
	inline List_1_tEAD6E3282E028927B32F56E7892994D90D512467 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_tEAD6E3282E028927B32F56E7892994D90D512467 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t59101674E6772D817227A6D5C222AF34A4E0BBAD, ___predicate_4)); }
	inline Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t59101674E6772D817227A6D5C222AF34A4E0BBAD, ___selector_5)); }
	inline Func_2_t14F6F4D7FBF558A20782EDC48D80473E4800048E * get_selector_5() const { return ___selector_5; }
	inline Func_2_t14F6F4D7FBF558A20782EDC48D80473E4800048E ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t14F6F4D7FBF558A20782EDC48D80473E4800048E * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t59101674E6772D817227A6D5C222AF34A4E0BBAD, ___enumerator_6)); }
	inline Enumerator_t7C514ACB3FF8E3EFB210F5F45C9F18A715BB4F80  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_t7C514ACB3FF8E3EFB210F5F45C9F18A715BB4F80 * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_t7C514ACB3FF8E3EFB210F5F45C9F18A715BB4F80  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___m_String_0), (void*)NULL);
		#endif
	}
};


// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.Substring,System.Object>
struct  WhereSelectListIterator_2_t0CEFF5458EED043DC70F7CC9B97B5AB4D45324EB  : public Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_tEAD6E3282E028927B32F56E7892994D90D512467 * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_t4EC2F095EDDC8079537CA0FEDC463635E599C9E4 * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_t7C514ACB3FF8E3EFB210F5F45C9F18A715BB4F80  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t0CEFF5458EED043DC70F7CC9B97B5AB4D45324EB, ___source_3)); }
	inline List_1_tEAD6E3282E028927B32F56E7892994D90D512467 * get_source_3() const { return ___source_3; }
	inline List_1_tEAD6E3282E028927B32F56E7892994D90D512467 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_tEAD6E3282E028927B32F56E7892994D90D512467 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t0CEFF5458EED043DC70F7CC9B97B5AB4D45324EB, ___predicate_4)); }
	inline Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t0CEFF5458EED043DC70F7CC9B97B5AB4D45324EB, ___selector_5)); }
	inline Func_2_t4EC2F095EDDC8079537CA0FEDC463635E599C9E4 * get_selector_5() const { return ___selector_5; }
	inline Func_2_t4EC2F095EDDC8079537CA0FEDC463635E599C9E4 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t4EC2F095EDDC8079537CA0FEDC463635E599C9E4 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t0CEFF5458EED043DC70F7CC9B97B5AB4D45324EB, ___enumerator_6)); }
	inline Enumerator_t7C514ACB3FF8E3EFB210F5F45C9F18A715BB4F80  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_t7C514ACB3FF8E3EFB210F5F45C9F18A715BB4F80 * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_t7C514ACB3FF8E3EFB210F5F45C9F18A715BB4F80  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___m_String_0), (void*)NULL);
		#endif
	}
};


// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___delegates_11), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};

// UnityEngine.InputSystem.Utilities.PrimitiveValue
struct  PrimitiveValue_t70D8365A1CB2217C466D27D6820362EF4331A0C8 
{
public:
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.TypeCode UnityEngine.InputSystem.Utilities.PrimitiveValue::m_Type
			int32_t ___m_Type_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___m_Type_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_BoolValue_1_OffsetPadding[4];
			// System.Boolean UnityEngine.InputSystem.Utilities.PrimitiveValue::m_BoolValue
			bool ___m_BoolValue_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_BoolValue_1_OffsetPadding_forAlignmentOnly[4];
			bool ___m_BoolValue_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_CharValue_2_OffsetPadding[4];
			// System.Char UnityEngine.InputSystem.Utilities.PrimitiveValue::m_CharValue
			Il2CppChar ___m_CharValue_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_CharValue_2_OffsetPadding_forAlignmentOnly[4];
			Il2CppChar ___m_CharValue_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_ByteValue_3_OffsetPadding[4];
			// System.Byte UnityEngine.InputSystem.Utilities.PrimitiveValue::m_ByteValue
			uint8_t ___m_ByteValue_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_ByteValue_3_OffsetPadding_forAlignmentOnly[4];
			uint8_t ___m_ByteValue_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_SByteValue_4_OffsetPadding[4];
			// System.SByte UnityEngine.InputSystem.Utilities.PrimitiveValue::m_SByteValue
			int8_t ___m_SByteValue_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_SByteValue_4_OffsetPadding_forAlignmentOnly[4];
			int8_t ___m_SByteValue_4_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_ShortValue_5_OffsetPadding[4];
			// System.Int16 UnityEngine.InputSystem.Utilities.PrimitiveValue::m_ShortValue
			int16_t ___m_ShortValue_5;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_ShortValue_5_OffsetPadding_forAlignmentOnly[4];
			int16_t ___m_ShortValue_5_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_UShortValue_6_OffsetPadding[4];
			// System.UInt16 UnityEngine.InputSystem.Utilities.PrimitiveValue::m_UShortValue
			uint16_t ___m_UShortValue_6;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_UShortValue_6_OffsetPadding_forAlignmentOnly[4];
			uint16_t ___m_UShortValue_6_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_IntValue_7_OffsetPadding[4];
			// System.Int32 UnityEngine.InputSystem.Utilities.PrimitiveValue::m_IntValue
			int32_t ___m_IntValue_7;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_IntValue_7_OffsetPadding_forAlignmentOnly[4];
			int32_t ___m_IntValue_7_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_UIntValue_8_OffsetPadding[4];
			// System.UInt32 UnityEngine.InputSystem.Utilities.PrimitiveValue::m_UIntValue
			uint32_t ___m_UIntValue_8;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_UIntValue_8_OffsetPadding_forAlignmentOnly[4];
			uint32_t ___m_UIntValue_8_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_LongValue_9_OffsetPadding[4];
			// System.Int64 UnityEngine.InputSystem.Utilities.PrimitiveValue::m_LongValue
			int64_t ___m_LongValue_9;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_LongValue_9_OffsetPadding_forAlignmentOnly[4];
			int64_t ___m_LongValue_9_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_ULongValue_10_OffsetPadding[4];
			// System.UInt64 UnityEngine.InputSystem.Utilities.PrimitiveValue::m_ULongValue
			uint64_t ___m_ULongValue_10;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_ULongValue_10_OffsetPadding_forAlignmentOnly[4];
			uint64_t ___m_ULongValue_10_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_FloatValue_11_OffsetPadding[4];
			// System.Single UnityEngine.InputSystem.Utilities.PrimitiveValue::m_FloatValue
			float ___m_FloatValue_11;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_FloatValue_11_OffsetPadding_forAlignmentOnly[4];
			float ___m_FloatValue_11_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_DoubleValue_12_OffsetPadding[4];
			// System.Double UnityEngine.InputSystem.Utilities.PrimitiveValue::m_DoubleValue
			double ___m_DoubleValue_12;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_DoubleValue_12_OffsetPadding_forAlignmentOnly[4];
			double ___m_DoubleValue_12_forAlignmentOnly;
		};
	};

public:
	inline static int32_t get_offset_of_m_Type_0() { return static_cast<int32_t>(offsetof(PrimitiveValue_t70D8365A1CB2217C466D27D6820362EF4331A0C8, ___m_Type_0)); }
	inline int32_t get_m_Type_0() const { return ___m_Type_0; }
	inline int32_t* get_address_of_m_Type_0() { return &___m_Type_0; }
	inline void set_m_Type_0(int32_t value)
	{
		___m_Type_0 = value;
	}

	inline static int32_t get_offset_of_m_BoolValue_1() { return static_cast<int32_t>(offsetof(PrimitiveValue_t70D8365A1CB2217C466D27D6820362EF4331A0C8, ___m_BoolValue_1)); }
	inline bool get_m_BoolValue_1() const { return ___m_BoolValue_1; }
	inline bool* get_address_of_m_BoolValue_1() { return &___m_BoolValue_1; }
	inline void set_m_BoolValue_1(bool value)
	{
		___m_BoolValue_1 = value;
	}

	inline static int32_t get_offset_of_m_CharValue_2() { return static_cast<int32_t>(offsetof(PrimitiveValue_t70D8365A1CB2217C466D27D6820362EF4331A0C8, ___m_CharValue_2)); }
	inline Il2CppChar get_m_CharValue_2() const { return ___m_CharValue_2; }
	inline Il2CppChar* get_address_of_m_CharValue_2() { return &___m_CharValue_2; }
	inline void set_m_CharValue_2(Il2CppChar value)
	{
		___m_CharValue_2 = value;
	}

	inline static int32_t get_offset_of_m_ByteValue_3() { return static_cast<int32_t>(offsetof(PrimitiveValue_t70D8365A1CB2217C466D27D6820362EF4331A0C8, ___m_ByteValue_3)); }
	inline uint8_t get_m_ByteValue_3() const { return ___m_ByteValue_3; }
	inline uint8_t* get_address_of_m_ByteValue_3() { return &___m_ByteValue_3; }
	inline void set_m_ByteValue_3(uint8_t value)
	{
		___m_ByteValue_3 = value;
	}

	inline static int32_t get_offset_of_m_SByteValue_4() { return static_cast<int32_t>(offsetof(PrimitiveValue_t70D8365A1CB2217C466D27D6820362EF4331A0C8, ___m_SByteValue_4)); }
	inline int8_t get_m_SByteValue_4() const { return ___m_SByteValue_4; }
	inline int8_t* get_address_of_m_SByteValue_4() { return &___m_SByteValue_4; }
	inline void set_m_SByteValue_4(int8_t value)
	{
		___m_SByteValue_4 = value;
	}

	inline static int32_t get_offset_of_m_ShortValue_5() { return static_cast<int32_t>(offsetof(PrimitiveValue_t70D8365A1CB2217C466D27D6820362EF4331A0C8, ___m_ShortValue_5)); }
	inline int16_t get_m_ShortValue_5() const { return ___m_ShortValue_5; }
	inline int16_t* get_address_of_m_ShortValue_5() { return &___m_ShortValue_5; }
	inline void set_m_ShortValue_5(int16_t value)
	{
		___m_ShortValue_5 = value;
	}

	inline static int32_t get_offset_of_m_UShortValue_6() { return static_cast<int32_t>(offsetof(PrimitiveValue_t70D8365A1CB2217C466D27D6820362EF4331A0C8, ___m_UShortValue_6)); }
	inline uint16_t get_m_UShortValue_6() const { return ___m_UShortValue_6; }
	inline uint16_t* get_address_of_m_UShortValue_6() { return &___m_UShortValue_6; }
	inline void set_m_UShortValue_6(uint16_t value)
	{
		___m_UShortValue_6 = value;
	}

	inline static int32_t get_offset_of_m_IntValue_7() { return static_cast<int32_t>(offsetof(PrimitiveValue_t70D8365A1CB2217C466D27D6820362EF4331A0C8, ___m_IntValue_7)); }
	inline int32_t get_m_IntValue_7() const { return ___m_IntValue_7; }
	inline int32_t* get_address_of_m_IntValue_7() { return &___m_IntValue_7; }
	inline void set_m_IntValue_7(int32_t value)
	{
		___m_IntValue_7 = value;
	}

	inline static int32_t get_offset_of_m_UIntValue_8() { return static_cast<int32_t>(offsetof(PrimitiveValue_t70D8365A1CB2217C466D27D6820362EF4331A0C8, ___m_UIntValue_8)); }
	inline uint32_t get_m_UIntValue_8() const { return ___m_UIntValue_8; }
	inline uint32_t* get_address_of_m_UIntValue_8() { return &___m_UIntValue_8; }
	inline void set_m_UIntValue_8(uint32_t value)
	{
		___m_UIntValue_8 = value;
	}

	inline static int32_t get_offset_of_m_LongValue_9() { return static_cast<int32_t>(offsetof(PrimitiveValue_t70D8365A1CB2217C466D27D6820362EF4331A0C8, ___m_LongValue_9)); }
	inline int64_t get_m_LongValue_9() const { return ___m_LongValue_9; }
	inline int64_t* get_address_of_m_LongValue_9() { return &___m_LongValue_9; }
	inline void set_m_LongValue_9(int64_t value)
	{
		___m_LongValue_9 = value;
	}

	inline static int32_t get_offset_of_m_ULongValue_10() { return static_cast<int32_t>(offsetof(PrimitiveValue_t70D8365A1CB2217C466D27D6820362EF4331A0C8, ___m_ULongValue_10)); }
	inline uint64_t get_m_ULongValue_10() const { return ___m_ULongValue_10; }
	inline uint64_t* get_address_of_m_ULongValue_10() { return &___m_ULongValue_10; }
	inline void set_m_ULongValue_10(uint64_t value)
	{
		___m_ULongValue_10 = value;
	}

	inline static int32_t get_offset_of_m_FloatValue_11() { return static_cast<int32_t>(offsetof(PrimitiveValue_t70D8365A1CB2217C466D27D6820362EF4331A0C8, ___m_FloatValue_11)); }
	inline float get_m_FloatValue_11() const { return ___m_FloatValue_11; }
	inline float* get_address_of_m_FloatValue_11() { return &___m_FloatValue_11; }
	inline void set_m_FloatValue_11(float value)
	{
		___m_FloatValue_11 = value;
	}

	inline static int32_t get_offset_of_m_DoubleValue_12() { return static_cast<int32_t>(offsetof(PrimitiveValue_t70D8365A1CB2217C466D27D6820362EF4331A0C8, ___m_DoubleValue_12)); }
	inline double get_m_DoubleValue_12() const { return ___m_DoubleValue_12; }
	inline double* get_address_of_m_DoubleValue_12() { return &___m_DoubleValue_12; }
	inline void set_m_DoubleValue_12(double value)
	{
		___m_DoubleValue_12 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.InputSystem.Utilities.PrimitiveValue
struct PrimitiveValue_t70D8365A1CB2217C466D27D6820362EF4331A0C8_marshaled_pinvoke
{
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			int32_t ___m_Type_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___m_Type_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_BoolValue_1_OffsetPadding[4];
			int32_t ___m_BoolValue_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_BoolValue_1_OffsetPadding_forAlignmentOnly[4];
			int32_t ___m_BoolValue_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_CharValue_2_OffsetPadding[4];
			uint8_t ___m_CharValue_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_CharValue_2_OffsetPadding_forAlignmentOnly[4];
			uint8_t ___m_CharValue_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_ByteValue_3_OffsetPadding[4];
			uint8_t ___m_ByteValue_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_ByteValue_3_OffsetPadding_forAlignmentOnly[4];
			uint8_t ___m_ByteValue_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_SByteValue_4_OffsetPadding[4];
			int8_t ___m_SByteValue_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_SByteValue_4_OffsetPadding_forAlignmentOnly[4];
			int8_t ___m_SByteValue_4_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_ShortValue_5_OffsetPadding[4];
			int16_t ___m_ShortValue_5;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_ShortValue_5_OffsetPadding_forAlignmentOnly[4];
			int16_t ___m_ShortValue_5_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_UShortValue_6_OffsetPadding[4];
			uint16_t ___m_UShortValue_6;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_UShortValue_6_OffsetPadding_forAlignmentOnly[4];
			uint16_t ___m_UShortValue_6_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_IntValue_7_OffsetPadding[4];
			int32_t ___m_IntValue_7;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_IntValue_7_OffsetPadding_forAlignmentOnly[4];
			int32_t ___m_IntValue_7_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_UIntValue_8_OffsetPadding[4];
			uint32_t ___m_UIntValue_8;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_UIntValue_8_OffsetPadding_forAlignmentOnly[4];
			uint32_t ___m_UIntValue_8_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_LongValue_9_OffsetPadding[4];
			int64_t ___m_LongValue_9;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_LongValue_9_OffsetPadding_forAlignmentOnly[4];
			int64_t ___m_LongValue_9_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_ULongValue_10_OffsetPadding[4];
			uint64_t ___m_ULongValue_10;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_ULongValue_10_OffsetPadding_forAlignmentOnly[4];
			uint64_t ___m_ULongValue_10_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_FloatValue_11_OffsetPadding[4];
			float ___m_FloatValue_11;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_FloatValue_11_OffsetPadding_forAlignmentOnly[4];
			float ___m_FloatValue_11_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_DoubleValue_12_OffsetPadding[4];
			double ___m_DoubleValue_12;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_DoubleValue_12_OffsetPadding_forAlignmentOnly[4];
			double ___m_DoubleValue_12_forAlignmentOnly;
		};
	};
};
// Native definition for COM marshalling of UnityEngine.InputSystem.Utilities.PrimitiveValue
struct PrimitiveValue_t70D8365A1CB2217C466D27D6820362EF4331A0C8_marshaled_com
{
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			int32_t ___m_Type_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___m_Type_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_BoolValue_1_OffsetPadding[4];
			int32_t ___m_BoolValue_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_BoolValue_1_OffsetPadding_forAlignmentOnly[4];
			int32_t ___m_BoolValue_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_CharValue_2_OffsetPadding[4];
			uint8_t ___m_CharValue_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_CharValue_2_OffsetPadding_forAlignmentOnly[4];
			uint8_t ___m_CharValue_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_ByteValue_3_OffsetPadding[4];
			uint8_t ___m_ByteValue_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_ByteValue_3_OffsetPadding_forAlignmentOnly[4];
			uint8_t ___m_ByteValue_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_SByteValue_4_OffsetPadding[4];
			int8_t ___m_SByteValue_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_SByteValue_4_OffsetPadding_forAlignmentOnly[4];
			int8_t ___m_SByteValue_4_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_ShortValue_5_OffsetPadding[4];
			int16_t ___m_ShortValue_5;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_ShortValue_5_OffsetPadding_forAlignmentOnly[4];
			int16_t ___m_ShortValue_5_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_UShortValue_6_OffsetPadding[4];
			uint16_t ___m_UShortValue_6;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_UShortValue_6_OffsetPadding_forAlignmentOnly[4];
			uint16_t ___m_UShortValue_6_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_IntValue_7_OffsetPadding[4];
			int32_t ___m_IntValue_7;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_IntValue_7_OffsetPadding_forAlignmentOnly[4];
			int32_t ___m_IntValue_7_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_UIntValue_8_OffsetPadding[4];
			uint32_t ___m_UIntValue_8;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_UIntValue_8_OffsetPadding_forAlignmentOnly[4];
			uint32_t ___m_UIntValue_8_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_LongValue_9_OffsetPadding[4];
			int64_t ___m_LongValue_9;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_LongValue_9_OffsetPadding_forAlignmentOnly[4];
			int64_t ___m_LongValue_9_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_ULongValue_10_OffsetPadding[4];
			uint64_t ___m_ULongValue_10;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_ULongValue_10_OffsetPadding_forAlignmentOnly[4];
			uint64_t ___m_ULongValue_10_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_FloatValue_11_OffsetPadding[4];
			float ___m_FloatValue_11;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_FloatValue_11_OffsetPadding_forAlignmentOnly[4];
			float ___m_FloatValue_11_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_DoubleValue_12_OffsetPadding[4];
			double ___m_DoubleValue_12;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_DoubleValue_12_OffsetPadding_forAlignmentOnly[4];
			double ___m_DoubleValue_12_forAlignmentOnly;
		};
	};
};

// UnityEngine.UIElements.StyleSelectorPart
struct  StyleSelectorPart_t707EDC970FC0F3E91E56DCBC178672A120426D54 
{
public:
	// System.String UnityEngine.UIElements.StyleSelectorPart::m_Value
	String_t* ___m_Value_0;
	// UnityEngine.UIElements.StyleSelectorType UnityEngine.UIElements.StyleSelectorPart::m_Type
	int32_t ___m_Type_1;
	// System.Object UnityEngine.UIElements.StyleSelectorPart::tempData
	RuntimeObject * ___tempData_2;

public:
	inline static int32_t get_offset_of_m_Value_0() { return static_cast<int32_t>(offsetof(StyleSelectorPart_t707EDC970FC0F3E91E56DCBC178672A120426D54, ___m_Value_0)); }
	inline String_t* get_m_Value_0() const { return ___m_Value_0; }
	inline String_t** get_address_of_m_Value_0() { return &___m_Value_0; }
	inline void set_m_Value_0(String_t* value)
	{
		___m_Value_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Value_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type_1() { return static_cast<int32_t>(offsetof(StyleSelectorPart_t707EDC970FC0F3E91E56DCBC178672A120426D54, ___m_Type_1)); }
	inline int32_t get_m_Type_1() const { return ___m_Type_1; }
	inline int32_t* get_address_of_m_Type_1() { return &___m_Type_1; }
	inline void set_m_Type_1(int32_t value)
	{
		___m_Type_1 = value;
	}

	inline static int32_t get_offset_of_tempData_2() { return static_cast<int32_t>(offsetof(StyleSelectorPart_t707EDC970FC0F3E91E56DCBC178672A120426D54, ___tempData_2)); }
	inline RuntimeObject * get_tempData_2() const { return ___tempData_2; }
	inline RuntimeObject ** get_address_of_tempData_2() { return &___tempData_2; }
	inline void set_tempData_2(RuntimeObject * value)
	{
		___tempData_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tempData_2), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.UIElements.StyleSelectorPart
struct StyleSelectorPart_t707EDC970FC0F3E91E56DCBC178672A120426D54_marshaled_pinvoke
{
	char* ___m_Value_0;
	int32_t ___m_Type_1;
	Il2CppIUnknown* ___tempData_2;
};
// Native definition for COM marshalling of UnityEngine.UIElements.StyleSelectorPart
struct StyleSelectorPart_t707EDC970FC0F3E91E56DCBC178672A120426D54_marshaled_com
{
	Il2CppChar* ___m_Value_0;
	int32_t ___m_Type_1;
	Il2CppIUnknown* ___tempData_2;
};

// UnityEngine.InputSystem.Utilities.JsonParser/JsonValue
struct  JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB 
{
public:
	// UnityEngine.InputSystem.Utilities.JsonParser/JsonValueType UnityEngine.InputSystem.Utilities.JsonParser/JsonValue::type
	int32_t ___type_0;
	// System.Boolean UnityEngine.InputSystem.Utilities.JsonParser/JsonValue::boolValue
	bool ___boolValue_1;
	// System.Double UnityEngine.InputSystem.Utilities.JsonParser/JsonValue::realValue
	double ___realValue_2;
	// System.Int64 UnityEngine.InputSystem.Utilities.JsonParser/JsonValue::integerValue
	int64_t ___integerValue_3;
	// UnityEngine.InputSystem.Utilities.JsonParser/JsonString UnityEngine.InputSystem.Utilities.JsonParser/JsonValue::stringValue
	JsonString_tB3A1938903F2377F780FF4C4FE7CC6EA81C0D301  ___stringValue_4;
	// System.Collections.Generic.List`1<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue> UnityEngine.InputSystem.Utilities.JsonParser/JsonValue::arrayValue
	List_1_tEC0B302C3EF23400C3EC92ED7672A715CA602068 * ___arrayValue_5;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue> UnityEngine.InputSystem.Utilities.JsonParser/JsonValue::objectValue
	Dictionary_2_t7A371082270CA8FD6BC79B5622CCD5E1C87A6E42 * ___objectValue_6;
	// System.Object UnityEngine.InputSystem.Utilities.JsonParser/JsonValue::anyValue
	RuntimeObject * ___anyValue_7;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB, ___type_0)); }
	inline int32_t get_type_0() const { return ___type_0; }
	inline int32_t* get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(int32_t value)
	{
		___type_0 = value;
	}

	inline static int32_t get_offset_of_boolValue_1() { return static_cast<int32_t>(offsetof(JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB, ___boolValue_1)); }
	inline bool get_boolValue_1() const { return ___boolValue_1; }
	inline bool* get_address_of_boolValue_1() { return &___boolValue_1; }
	inline void set_boolValue_1(bool value)
	{
		___boolValue_1 = value;
	}

	inline static int32_t get_offset_of_realValue_2() { return static_cast<int32_t>(offsetof(JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB, ___realValue_2)); }
	inline double get_realValue_2() const { return ___realValue_2; }
	inline double* get_address_of_realValue_2() { return &___realValue_2; }
	inline void set_realValue_2(double value)
	{
		___realValue_2 = value;
	}

	inline static int32_t get_offset_of_integerValue_3() { return static_cast<int32_t>(offsetof(JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB, ___integerValue_3)); }
	inline int64_t get_integerValue_3() const { return ___integerValue_3; }
	inline int64_t* get_address_of_integerValue_3() { return &___integerValue_3; }
	inline void set_integerValue_3(int64_t value)
	{
		___integerValue_3 = value;
	}

	inline static int32_t get_offset_of_stringValue_4() { return static_cast<int32_t>(offsetof(JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB, ___stringValue_4)); }
	inline JsonString_tB3A1938903F2377F780FF4C4FE7CC6EA81C0D301  get_stringValue_4() const { return ___stringValue_4; }
	inline JsonString_tB3A1938903F2377F780FF4C4FE7CC6EA81C0D301 * get_address_of_stringValue_4() { return &___stringValue_4; }
	inline void set_stringValue_4(JsonString_tB3A1938903F2377F780FF4C4FE7CC6EA81C0D301  value)
	{
		___stringValue_4 = value;
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___stringValue_4))->___text_0))->___m_String_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_arrayValue_5() { return static_cast<int32_t>(offsetof(JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB, ___arrayValue_5)); }
	inline List_1_tEC0B302C3EF23400C3EC92ED7672A715CA602068 * get_arrayValue_5() const { return ___arrayValue_5; }
	inline List_1_tEC0B302C3EF23400C3EC92ED7672A715CA602068 ** get_address_of_arrayValue_5() { return &___arrayValue_5; }
	inline void set_arrayValue_5(List_1_tEC0B302C3EF23400C3EC92ED7672A715CA602068 * value)
	{
		___arrayValue_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___arrayValue_5), (void*)value);
	}

	inline static int32_t get_offset_of_objectValue_6() { return static_cast<int32_t>(offsetof(JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB, ___objectValue_6)); }
	inline Dictionary_2_t7A371082270CA8FD6BC79B5622CCD5E1C87A6E42 * get_objectValue_6() const { return ___objectValue_6; }
	inline Dictionary_2_t7A371082270CA8FD6BC79B5622CCD5E1C87A6E42 ** get_address_of_objectValue_6() { return &___objectValue_6; }
	inline void set_objectValue_6(Dictionary_2_t7A371082270CA8FD6BC79B5622CCD5E1C87A6E42 * value)
	{
		___objectValue_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___objectValue_6), (void*)value);
	}

	inline static int32_t get_offset_of_anyValue_7() { return static_cast<int32_t>(offsetof(JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB, ___anyValue_7)); }
	inline RuntimeObject * get_anyValue_7() const { return ___anyValue_7; }
	inline RuntimeObject ** get_address_of_anyValue_7() { return &___anyValue_7; }
	inline void set_anyValue_7(RuntimeObject * value)
	{
		___anyValue_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___anyValue_7), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.InputSystem.Utilities.JsonParser/JsonValue
struct JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB_marshaled_pinvoke
{
	int32_t ___type_0;
	int32_t ___boolValue_1;
	double ___realValue_2;
	int64_t ___integerValue_3;
	JsonString_tB3A1938903F2377F780FF4C4FE7CC6EA81C0D301_marshaled_pinvoke ___stringValue_4;
	List_1_tEC0B302C3EF23400C3EC92ED7672A715CA602068 * ___arrayValue_5;
	Dictionary_2_t7A371082270CA8FD6BC79B5622CCD5E1C87A6E42 * ___objectValue_6;
	Il2CppIUnknown* ___anyValue_7;
};
// Native definition for COM marshalling of UnityEngine.InputSystem.Utilities.JsonParser/JsonValue
struct JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB_marshaled_com
{
	int32_t ___type_0;
	int32_t ___boolValue_1;
	double ___realValue_2;
	int64_t ___integerValue_3;
	JsonString_tB3A1938903F2377F780FF4C4FE7CC6EA81C0D301_marshaled_com ___stringValue_4;
	List_1_tEC0B302C3EF23400C3EC92ED7672A715CA602068 * ___arrayValue_5;
	Dictionary_2_t7A371082270CA8FD6BC79B5622CCD5E1C87A6E42 * ___objectValue_6;
	Il2CppIUnknown* ___anyValue_7;
};

// System.Collections.Generic.List`1/Enumerator<UnityEngine.UIElements.StyleSelectorPart>
struct  Enumerator_tC861559498255F6677610CED32D2161012D8D897 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_tE7DC2F192C483F9E5722779E3C2C1910490AAE59 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	StyleSelectorPart_t707EDC970FC0F3E91E56DCBC178672A120426D54  ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_tC861559498255F6677610CED32D2161012D8D897, ___list_0)); }
	inline List_1_tE7DC2F192C483F9E5722779E3C2C1910490AAE59 * get_list_0() const { return ___list_0; }
	inline List_1_tE7DC2F192C483F9E5722779E3C2C1910490AAE59 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_tE7DC2F192C483F9E5722779E3C2C1910490AAE59 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_tC861559498255F6677610CED32D2161012D8D897, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_tC861559498255F6677610CED32D2161012D8D897, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_tC861559498255F6677610CED32D2161012D8D897, ___current_3)); }
	inline StyleSelectorPart_t707EDC970FC0F3E91E56DCBC178672A120426D54  get_current_3() const { return ___current_3; }
	inline StyleSelectorPart_t707EDC970FC0F3E91E56DCBC178672A120426D54 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(StyleSelectorPart_t707EDC970FC0F3E91E56DCBC178672A120426D54  value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___current_3))->___m_Value_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___current_3))->___tempData_2), (void*)NULL);
		#endif
	}
};


// System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>
struct  Enumerator_tF2D95CC18A66F3DE682EAC9B88E865750DFAEF60 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_tEC0B302C3EF23400C3EC92ED7672A715CA602068 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB  ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_tF2D95CC18A66F3DE682EAC9B88E865750DFAEF60, ___list_0)); }
	inline List_1_tEC0B302C3EF23400C3EC92ED7672A715CA602068 * get_list_0() const { return ___list_0; }
	inline List_1_tEC0B302C3EF23400C3EC92ED7672A715CA602068 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_tEC0B302C3EF23400C3EC92ED7672A715CA602068 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_tF2D95CC18A66F3DE682EAC9B88E865750DFAEF60, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_tF2D95CC18A66F3DE682EAC9B88E865750DFAEF60, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_tF2D95CC18A66F3DE682EAC9B88E865750DFAEF60, ___current_3)); }
	inline JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB  get_current_3() const { return ___current_3; }
	inline JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB  value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___current_3))->___stringValue_4))->___text_0))->___m_String_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___current_3))->___arrayValue_5), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___current_3))->___objectValue_6), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___current_3))->___anyValue_7), (void*)NULL);
		#endif
	}
};


// System.Func`2<UnityEngine.InputSystem.Utilities.InternedString,System.Boolean>
struct  Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.InputSystem.Utilities.InternedString,UnityEngine.InputSystem.Utilities.InternedString>
struct  Func_2_t1AF155D27A4D78882342D697E4A6DE7CE594545B  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.InputSystem.Utilities.InternedString,System.Object>
struct  Func_2_tA2BD34BB8D97AD282016BD4CB5B8675E4AF85B45  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.InputSystem.Utilities.NameAndParameters,System.Boolean>
struct  Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.InputSystem.Utilities.NameAndParameters,UnityEngine.InputSystem.Utilities.InternedString>
struct  Func_2_tEA2235809AB0E1AB8425745179691D76B519B0C5  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.InputSystem.Utilities.NameAndParameters,System.Object>
struct  Func_2_tF7304610FB007335AC870B51C8FE2092628DC0DB  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<System.Object,System.Boolean>
struct  Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<System.Object,UnityEngine.InputSystem.Utilities.InternedString>
struct  Func_2_t964229B6ADD2B008268547ED2BC3C090AF218065  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<System.Object,System.Object>
struct  Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.UIElements.StyleSelectorPart,System.Boolean>
struct  Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.UIElements.StyleSelectorPart,UnityEngine.InputSystem.Utilities.InternedString>
struct  Func_2_tFBBF89B72C697489D82FBE8FF822B191D2EE9A41  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.UIElements.StyleSelectorPart,System.Object>
struct  Func_2_t43979C5A872BA9ED7A923D533A01683A98D71957  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.InputSystem.Utilities.Substring,System.Boolean>
struct  Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.InputSystem.Utilities.Substring,UnityEngine.InputSystem.Utilities.InternedString>
struct  Func_2_t14F6F4D7FBF558A20782EDC48D80473E4800048E  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.InputSystem.Utilities.Substring,System.Object>
struct  Func_2_t4EC2F095EDDC8079537CA0FEDC463635E599C9E4  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,System.Boolean>
struct  Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,UnityEngine.InputSystem.Utilities.InternedString>
struct  Func_2_t48DD1C8E439811C492BAFE220A679DB57D7C9370  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,System.Object>
struct  Func_2_t1628B2E4EEC7A32D5B688E54415BC42EDA9D2899  : public MulticastDelegate_t
{
public:

public:
};


// System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>
struct  KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	RuntimeObject * ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB  ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184, ___key_0)); }
	inline RuntimeObject * get_key_0() const { return ___key_0; }
	inline RuntimeObject ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(RuntimeObject * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___key_0), (void*)value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184, ___value_1)); }
	inline JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB  get_value_1() const { return ___value_1; }
	inline JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB * get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB  value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___value_1))->___stringValue_4))->___text_0))->___m_String_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___value_1))->___arrayValue_5), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___value_1))->___objectValue_6), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___value_1))->___anyValue_7), (void*)NULL);
		#endif
	}
};


// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,UnityEngine.InputSystem.Utilities.InternedString>
struct  WhereSelectListIterator_2_t97CD9F77D3626E616F5290B44E8448C47A64E44C  : public Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_tCBF5EB99F2E538DCB47A8C835DD4B9C024879393 * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_tEA2235809AB0E1AB8425745179691D76B519B0C5 * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_tA2E0F83E15EA46A2B4B08AFB750700C6E1387FC0  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t97CD9F77D3626E616F5290B44E8448C47A64E44C, ___source_3)); }
	inline List_1_tCBF5EB99F2E538DCB47A8C835DD4B9C024879393 * get_source_3() const { return ___source_3; }
	inline List_1_tCBF5EB99F2E538DCB47A8C835DD4B9C024879393 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_tCBF5EB99F2E538DCB47A8C835DD4B9C024879393 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t97CD9F77D3626E616F5290B44E8448C47A64E44C, ___predicate_4)); }
	inline Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t97CD9F77D3626E616F5290B44E8448C47A64E44C, ___selector_5)); }
	inline Func_2_tEA2235809AB0E1AB8425745179691D76B519B0C5 * get_selector_5() const { return ___selector_5; }
	inline Func_2_tEA2235809AB0E1AB8425745179691D76B519B0C5 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_tEA2235809AB0E1AB8425745179691D76B519B0C5 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t97CD9F77D3626E616F5290B44E8448C47A64E44C, ___enumerator_6)); }
	inline Enumerator_tA2E0F83E15EA46A2B4B08AFB750700C6E1387FC0  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_tA2E0F83E15EA46A2B4B08AFB750700C6E1387FC0 * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_tA2E0F83E15EA46A2B4B08AFB750700C6E1387FC0  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___U3CnameU3Ek__BackingField_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___enumerator_6))->___current_3))->___U3CparametersU3Ek__BackingField_1))->___m_Array_0), (void*)NULL);
		#endif
	}
};


// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,System.Object>
struct  WhereSelectListIterator_2_tDFC8E5CA0DE13C88A58AD7BCD17F1FC274A3F95D  : public Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_tCBF5EB99F2E538DCB47A8C835DD4B9C024879393 * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_tF7304610FB007335AC870B51C8FE2092628DC0DB * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_tA2E0F83E15EA46A2B4B08AFB750700C6E1387FC0  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tDFC8E5CA0DE13C88A58AD7BCD17F1FC274A3F95D, ___source_3)); }
	inline List_1_tCBF5EB99F2E538DCB47A8C835DD4B9C024879393 * get_source_3() const { return ___source_3; }
	inline List_1_tCBF5EB99F2E538DCB47A8C835DD4B9C024879393 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_tCBF5EB99F2E538DCB47A8C835DD4B9C024879393 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tDFC8E5CA0DE13C88A58AD7BCD17F1FC274A3F95D, ___predicate_4)); }
	inline Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tDFC8E5CA0DE13C88A58AD7BCD17F1FC274A3F95D, ___selector_5)); }
	inline Func_2_tF7304610FB007335AC870B51C8FE2092628DC0DB * get_selector_5() const { return ___selector_5; }
	inline Func_2_tF7304610FB007335AC870B51C8FE2092628DC0DB ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_tF7304610FB007335AC870B51C8FE2092628DC0DB * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tDFC8E5CA0DE13C88A58AD7BCD17F1FC274A3F95D, ___enumerator_6)); }
	inline Enumerator_tA2E0F83E15EA46A2B4B08AFB750700C6E1387FC0  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_tA2E0F83E15EA46A2B4B08AFB750700C6E1387FC0 * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_tA2E0F83E15EA46A2B4B08AFB750700C6E1387FC0  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___U3CnameU3Ek__BackingField_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___enumerator_6))->___current_3))->___U3CparametersU3Ek__BackingField_1))->___m_Array_0), (void*)NULL);
		#endif
	}
};


// UnityEngine.InputSystem.Utilities.NamedValue
struct  NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077 
{
public:
	// System.String UnityEngine.InputSystem.Utilities.NamedValue::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_1;
	// UnityEngine.InputSystem.Utilities.PrimitiveValue UnityEngine.InputSystem.Utilities.NamedValue::<value>k__BackingField
	PrimitiveValue_t70D8365A1CB2217C466D27D6820362EF4331A0C8  ___U3CvalueU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077, ___U3CnameU3Ek__BackingField_1)); }
	inline String_t* get_U3CnameU3Ek__BackingField_1() const { return ___U3CnameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_1() { return &___U3CnameU3Ek__BackingField_1; }
	inline void set_U3CnameU3Ek__BackingField_1(String_t* value)
	{
		___U3CnameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CnameU3Ek__BackingField_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CvalueU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077, ___U3CvalueU3Ek__BackingField_2)); }
	inline PrimitiveValue_t70D8365A1CB2217C466D27D6820362EF4331A0C8  get_U3CvalueU3Ek__BackingField_2() const { return ___U3CvalueU3Ek__BackingField_2; }
	inline PrimitiveValue_t70D8365A1CB2217C466D27D6820362EF4331A0C8 * get_address_of_U3CvalueU3Ek__BackingField_2() { return &___U3CvalueU3Ek__BackingField_2; }
	inline void set_U3CvalueU3Ek__BackingField_2(PrimitiveValue_t70D8365A1CB2217C466D27D6820362EF4331A0C8  value)
	{
		___U3CvalueU3Ek__BackingField_2 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.InputSystem.Utilities.NamedValue
struct NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077_marshaled_pinvoke
{
	char* ___U3CnameU3Ek__BackingField_1;
	PrimitiveValue_t70D8365A1CB2217C466D27D6820362EF4331A0C8_marshaled_pinvoke ___U3CvalueU3Ek__BackingField_2;
};
// Native definition for COM marshalling of UnityEngine.InputSystem.Utilities.NamedValue
struct NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077_marshaled_com
{
	Il2CppChar* ___U3CnameU3Ek__BackingField_1;
	PrimitiveValue_t70D8365A1CB2217C466D27D6820362EF4331A0C8_marshaled_com ___U3CvalueU3Ek__BackingField_2;
};

// UnityEngine.InputSystem.Layouts.InputControlLayout/ControlItem
struct  ControlItem_t88696CA6FA7E3E6EDBF31BD5A8994D959F583B15 
{
public:
	// UnityEngine.InputSystem.Utilities.InternedString UnityEngine.InputSystem.Layouts.InputControlLayout/ControlItem::<name>k__BackingField
	InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  ___U3CnameU3Ek__BackingField_0;
	// UnityEngine.InputSystem.Utilities.InternedString UnityEngine.InputSystem.Layouts.InputControlLayout/ControlItem::<layout>k__BackingField
	InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  ___U3ClayoutU3Ek__BackingField_1;
	// UnityEngine.InputSystem.Utilities.InternedString UnityEngine.InputSystem.Layouts.InputControlLayout/ControlItem::<variants>k__BackingField
	InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  ___U3CvariantsU3Ek__BackingField_2;
	// System.String UnityEngine.InputSystem.Layouts.InputControlLayout/ControlItem::<useStateFrom>k__BackingField
	String_t* ___U3CuseStateFromU3Ek__BackingField_3;
	// System.String UnityEngine.InputSystem.Layouts.InputControlLayout/ControlItem::<displayName>k__BackingField
	String_t* ___U3CdisplayNameU3Ek__BackingField_4;
	// System.String UnityEngine.InputSystem.Layouts.InputControlLayout/ControlItem::<shortDisplayName>k__BackingField
	String_t* ___U3CshortDisplayNameU3Ek__BackingField_5;
	// UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Utilities.InternedString> UnityEngine.InputSystem.Layouts.InputControlLayout/ControlItem::<usages>k__BackingField
	ReadOnlyArray_1_t606E37DB0F1B75FCB4C4A029A0F6C2B4AE7DED75  ___U3CusagesU3Ek__BackingField_6;
	// UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Utilities.InternedString> UnityEngine.InputSystem.Layouts.InputControlLayout/ControlItem::<aliases>k__BackingField
	ReadOnlyArray_1_t606E37DB0F1B75FCB4C4A029A0F6C2B4AE7DED75  ___U3CaliasesU3Ek__BackingField_7;
	// UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Utilities.NamedValue> UnityEngine.InputSystem.Layouts.InputControlLayout/ControlItem::<parameters>k__BackingField
	ReadOnlyArray_1_t644486DA5FB2B247FC64733F192E2894F46CFCF5  ___U3CparametersU3Ek__BackingField_8;
	// UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Utilities.NameAndParameters> UnityEngine.InputSystem.Layouts.InputControlLayout/ControlItem::<processors>k__BackingField
	ReadOnlyArray_1_tC87F80F9582CEDF1B66B5875A22591316B710C6E  ___U3CprocessorsU3Ek__BackingField_9;
	// System.UInt32 UnityEngine.InputSystem.Layouts.InputControlLayout/ControlItem::<offset>k__BackingField
	uint32_t ___U3CoffsetU3Ek__BackingField_10;
	// System.UInt32 UnityEngine.InputSystem.Layouts.InputControlLayout/ControlItem::<bit>k__BackingField
	uint32_t ___U3CbitU3Ek__BackingField_11;
	// System.UInt32 UnityEngine.InputSystem.Layouts.InputControlLayout/ControlItem::<sizeInBits>k__BackingField
	uint32_t ___U3CsizeInBitsU3Ek__BackingField_12;
	// UnityEngine.InputSystem.Utilities.FourCC UnityEngine.InputSystem.Layouts.InputControlLayout/ControlItem::<format>k__BackingField
	FourCC_t8D38CDCEAD02FE53A6C2E9AF01354E4E5AB400A9  ___U3CformatU3Ek__BackingField_13;
	// UnityEngine.InputSystem.Layouts.InputControlLayout/ControlItem/Flags UnityEngine.InputSystem.Layouts.InputControlLayout/ControlItem::<flags>k__BackingField
	int32_t ___U3CflagsU3Ek__BackingField_14;
	// System.Int32 UnityEngine.InputSystem.Layouts.InputControlLayout/ControlItem::<arraySize>k__BackingField
	int32_t ___U3CarraySizeU3Ek__BackingField_15;
	// UnityEngine.InputSystem.Utilities.PrimitiveValue UnityEngine.InputSystem.Layouts.InputControlLayout/ControlItem::<defaultState>k__BackingField
	PrimitiveValue_t70D8365A1CB2217C466D27D6820362EF4331A0C8  ___U3CdefaultStateU3Ek__BackingField_16;
	// UnityEngine.InputSystem.Utilities.PrimitiveValue UnityEngine.InputSystem.Layouts.InputControlLayout/ControlItem::<minValue>k__BackingField
	PrimitiveValue_t70D8365A1CB2217C466D27D6820362EF4331A0C8  ___U3CminValueU3Ek__BackingField_17;
	// UnityEngine.InputSystem.Utilities.PrimitiveValue UnityEngine.InputSystem.Layouts.InputControlLayout/ControlItem::<maxValue>k__BackingField
	PrimitiveValue_t70D8365A1CB2217C466D27D6820362EF4331A0C8  ___U3CmaxValueU3Ek__BackingField_18;

public:
	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ControlItem_t88696CA6FA7E3E6EDBF31BD5A8994D959F583B15, ___U3CnameU3Ek__BackingField_0)); }
	inline InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  get_U3CnameU3Ek__BackingField_0() const { return ___U3CnameU3Ek__BackingField_0; }
	inline InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED * get_address_of_U3CnameU3Ek__BackingField_0() { return &___U3CnameU3Ek__BackingField_0; }
	inline void set_U3CnameU3Ek__BackingField_0(InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  value)
	{
		___U3CnameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CnameU3Ek__BackingField_0))->___m_StringOriginalCase_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CnameU3Ek__BackingField_0))->___m_StringLowerCase_1), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_U3ClayoutU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ControlItem_t88696CA6FA7E3E6EDBF31BD5A8994D959F583B15, ___U3ClayoutU3Ek__BackingField_1)); }
	inline InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  get_U3ClayoutU3Ek__BackingField_1() const { return ___U3ClayoutU3Ek__BackingField_1; }
	inline InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED * get_address_of_U3ClayoutU3Ek__BackingField_1() { return &___U3ClayoutU3Ek__BackingField_1; }
	inline void set_U3ClayoutU3Ek__BackingField_1(InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  value)
	{
		___U3ClayoutU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3ClayoutU3Ek__BackingField_1))->___m_StringOriginalCase_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3ClayoutU3Ek__BackingField_1))->___m_StringLowerCase_1), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_U3CvariantsU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ControlItem_t88696CA6FA7E3E6EDBF31BD5A8994D959F583B15, ___U3CvariantsU3Ek__BackingField_2)); }
	inline InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  get_U3CvariantsU3Ek__BackingField_2() const { return ___U3CvariantsU3Ek__BackingField_2; }
	inline InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED * get_address_of_U3CvariantsU3Ek__BackingField_2() { return &___U3CvariantsU3Ek__BackingField_2; }
	inline void set_U3CvariantsU3Ek__BackingField_2(InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  value)
	{
		___U3CvariantsU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CvariantsU3Ek__BackingField_2))->___m_StringOriginalCase_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CvariantsU3Ek__BackingField_2))->___m_StringLowerCase_1), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_U3CuseStateFromU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ControlItem_t88696CA6FA7E3E6EDBF31BD5A8994D959F583B15, ___U3CuseStateFromU3Ek__BackingField_3)); }
	inline String_t* get_U3CuseStateFromU3Ek__BackingField_3() const { return ___U3CuseStateFromU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CuseStateFromU3Ek__BackingField_3() { return &___U3CuseStateFromU3Ek__BackingField_3; }
	inline void set_U3CuseStateFromU3Ek__BackingField_3(String_t* value)
	{
		___U3CuseStateFromU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CuseStateFromU3Ek__BackingField_3), (void*)value);
	}

	inline static int32_t get_offset_of_U3CdisplayNameU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(ControlItem_t88696CA6FA7E3E6EDBF31BD5A8994D959F583B15, ___U3CdisplayNameU3Ek__BackingField_4)); }
	inline String_t* get_U3CdisplayNameU3Ek__BackingField_4() const { return ___U3CdisplayNameU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CdisplayNameU3Ek__BackingField_4() { return &___U3CdisplayNameU3Ek__BackingField_4; }
	inline void set_U3CdisplayNameU3Ek__BackingField_4(String_t* value)
	{
		___U3CdisplayNameU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CdisplayNameU3Ek__BackingField_4), (void*)value);
	}

	inline static int32_t get_offset_of_U3CshortDisplayNameU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(ControlItem_t88696CA6FA7E3E6EDBF31BD5A8994D959F583B15, ___U3CshortDisplayNameU3Ek__BackingField_5)); }
	inline String_t* get_U3CshortDisplayNameU3Ek__BackingField_5() const { return ___U3CshortDisplayNameU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CshortDisplayNameU3Ek__BackingField_5() { return &___U3CshortDisplayNameU3Ek__BackingField_5; }
	inline void set_U3CshortDisplayNameU3Ek__BackingField_5(String_t* value)
	{
		___U3CshortDisplayNameU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CshortDisplayNameU3Ek__BackingField_5), (void*)value);
	}

	inline static int32_t get_offset_of_U3CusagesU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(ControlItem_t88696CA6FA7E3E6EDBF31BD5A8994D959F583B15, ___U3CusagesU3Ek__BackingField_6)); }
	inline ReadOnlyArray_1_t606E37DB0F1B75FCB4C4A029A0F6C2B4AE7DED75  get_U3CusagesU3Ek__BackingField_6() const { return ___U3CusagesU3Ek__BackingField_6; }
	inline ReadOnlyArray_1_t606E37DB0F1B75FCB4C4A029A0F6C2B4AE7DED75 * get_address_of_U3CusagesU3Ek__BackingField_6() { return &___U3CusagesU3Ek__BackingField_6; }
	inline void set_U3CusagesU3Ek__BackingField_6(ReadOnlyArray_1_t606E37DB0F1B75FCB4C4A029A0F6C2B4AE7DED75  value)
	{
		___U3CusagesU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CusagesU3Ek__BackingField_6))->___m_Array_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_U3CaliasesU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(ControlItem_t88696CA6FA7E3E6EDBF31BD5A8994D959F583B15, ___U3CaliasesU3Ek__BackingField_7)); }
	inline ReadOnlyArray_1_t606E37DB0F1B75FCB4C4A029A0F6C2B4AE7DED75  get_U3CaliasesU3Ek__BackingField_7() const { return ___U3CaliasesU3Ek__BackingField_7; }
	inline ReadOnlyArray_1_t606E37DB0F1B75FCB4C4A029A0F6C2B4AE7DED75 * get_address_of_U3CaliasesU3Ek__BackingField_7() { return &___U3CaliasesU3Ek__BackingField_7; }
	inline void set_U3CaliasesU3Ek__BackingField_7(ReadOnlyArray_1_t606E37DB0F1B75FCB4C4A029A0F6C2B4AE7DED75  value)
	{
		___U3CaliasesU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CaliasesU3Ek__BackingField_7))->___m_Array_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_U3CparametersU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(ControlItem_t88696CA6FA7E3E6EDBF31BD5A8994D959F583B15, ___U3CparametersU3Ek__BackingField_8)); }
	inline ReadOnlyArray_1_t644486DA5FB2B247FC64733F192E2894F46CFCF5  get_U3CparametersU3Ek__BackingField_8() const { return ___U3CparametersU3Ek__BackingField_8; }
	inline ReadOnlyArray_1_t644486DA5FB2B247FC64733F192E2894F46CFCF5 * get_address_of_U3CparametersU3Ek__BackingField_8() { return &___U3CparametersU3Ek__BackingField_8; }
	inline void set_U3CparametersU3Ek__BackingField_8(ReadOnlyArray_1_t644486DA5FB2B247FC64733F192E2894F46CFCF5  value)
	{
		___U3CparametersU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CparametersU3Ek__BackingField_8))->___m_Array_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_U3CprocessorsU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(ControlItem_t88696CA6FA7E3E6EDBF31BD5A8994D959F583B15, ___U3CprocessorsU3Ek__BackingField_9)); }
	inline ReadOnlyArray_1_tC87F80F9582CEDF1B66B5875A22591316B710C6E  get_U3CprocessorsU3Ek__BackingField_9() const { return ___U3CprocessorsU3Ek__BackingField_9; }
	inline ReadOnlyArray_1_tC87F80F9582CEDF1B66B5875A22591316B710C6E * get_address_of_U3CprocessorsU3Ek__BackingField_9() { return &___U3CprocessorsU3Ek__BackingField_9; }
	inline void set_U3CprocessorsU3Ek__BackingField_9(ReadOnlyArray_1_tC87F80F9582CEDF1B66B5875A22591316B710C6E  value)
	{
		___U3CprocessorsU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CprocessorsU3Ek__BackingField_9))->___m_Array_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_U3CoffsetU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(ControlItem_t88696CA6FA7E3E6EDBF31BD5A8994D959F583B15, ___U3CoffsetU3Ek__BackingField_10)); }
	inline uint32_t get_U3CoffsetU3Ek__BackingField_10() const { return ___U3CoffsetU3Ek__BackingField_10; }
	inline uint32_t* get_address_of_U3CoffsetU3Ek__BackingField_10() { return &___U3CoffsetU3Ek__BackingField_10; }
	inline void set_U3CoffsetU3Ek__BackingField_10(uint32_t value)
	{
		___U3CoffsetU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3CbitU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(ControlItem_t88696CA6FA7E3E6EDBF31BD5A8994D959F583B15, ___U3CbitU3Ek__BackingField_11)); }
	inline uint32_t get_U3CbitU3Ek__BackingField_11() const { return ___U3CbitU3Ek__BackingField_11; }
	inline uint32_t* get_address_of_U3CbitU3Ek__BackingField_11() { return &___U3CbitU3Ek__BackingField_11; }
	inline void set_U3CbitU3Ek__BackingField_11(uint32_t value)
	{
		___U3CbitU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_U3CsizeInBitsU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(ControlItem_t88696CA6FA7E3E6EDBF31BD5A8994D959F583B15, ___U3CsizeInBitsU3Ek__BackingField_12)); }
	inline uint32_t get_U3CsizeInBitsU3Ek__BackingField_12() const { return ___U3CsizeInBitsU3Ek__BackingField_12; }
	inline uint32_t* get_address_of_U3CsizeInBitsU3Ek__BackingField_12() { return &___U3CsizeInBitsU3Ek__BackingField_12; }
	inline void set_U3CsizeInBitsU3Ek__BackingField_12(uint32_t value)
	{
		___U3CsizeInBitsU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CformatU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(ControlItem_t88696CA6FA7E3E6EDBF31BD5A8994D959F583B15, ___U3CformatU3Ek__BackingField_13)); }
	inline FourCC_t8D38CDCEAD02FE53A6C2E9AF01354E4E5AB400A9  get_U3CformatU3Ek__BackingField_13() const { return ___U3CformatU3Ek__BackingField_13; }
	inline FourCC_t8D38CDCEAD02FE53A6C2E9AF01354E4E5AB400A9 * get_address_of_U3CformatU3Ek__BackingField_13() { return &___U3CformatU3Ek__BackingField_13; }
	inline void set_U3CformatU3Ek__BackingField_13(FourCC_t8D38CDCEAD02FE53A6C2E9AF01354E4E5AB400A9  value)
	{
		___U3CformatU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_U3CflagsU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(ControlItem_t88696CA6FA7E3E6EDBF31BD5A8994D959F583B15, ___U3CflagsU3Ek__BackingField_14)); }
	inline int32_t get_U3CflagsU3Ek__BackingField_14() const { return ___U3CflagsU3Ek__BackingField_14; }
	inline int32_t* get_address_of_U3CflagsU3Ek__BackingField_14() { return &___U3CflagsU3Ek__BackingField_14; }
	inline void set_U3CflagsU3Ek__BackingField_14(int32_t value)
	{
		___U3CflagsU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_U3CarraySizeU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(ControlItem_t88696CA6FA7E3E6EDBF31BD5A8994D959F583B15, ___U3CarraySizeU3Ek__BackingField_15)); }
	inline int32_t get_U3CarraySizeU3Ek__BackingField_15() const { return ___U3CarraySizeU3Ek__BackingField_15; }
	inline int32_t* get_address_of_U3CarraySizeU3Ek__BackingField_15() { return &___U3CarraySizeU3Ek__BackingField_15; }
	inline void set_U3CarraySizeU3Ek__BackingField_15(int32_t value)
	{
		___U3CarraySizeU3Ek__BackingField_15 = value;
	}

	inline static int32_t get_offset_of_U3CdefaultStateU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(ControlItem_t88696CA6FA7E3E6EDBF31BD5A8994D959F583B15, ___U3CdefaultStateU3Ek__BackingField_16)); }
	inline PrimitiveValue_t70D8365A1CB2217C466D27D6820362EF4331A0C8  get_U3CdefaultStateU3Ek__BackingField_16() const { return ___U3CdefaultStateU3Ek__BackingField_16; }
	inline PrimitiveValue_t70D8365A1CB2217C466D27D6820362EF4331A0C8 * get_address_of_U3CdefaultStateU3Ek__BackingField_16() { return &___U3CdefaultStateU3Ek__BackingField_16; }
	inline void set_U3CdefaultStateU3Ek__BackingField_16(PrimitiveValue_t70D8365A1CB2217C466D27D6820362EF4331A0C8  value)
	{
		___U3CdefaultStateU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_U3CminValueU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(ControlItem_t88696CA6FA7E3E6EDBF31BD5A8994D959F583B15, ___U3CminValueU3Ek__BackingField_17)); }
	inline PrimitiveValue_t70D8365A1CB2217C466D27D6820362EF4331A0C8  get_U3CminValueU3Ek__BackingField_17() const { return ___U3CminValueU3Ek__BackingField_17; }
	inline PrimitiveValue_t70D8365A1CB2217C466D27D6820362EF4331A0C8 * get_address_of_U3CminValueU3Ek__BackingField_17() { return &___U3CminValueU3Ek__BackingField_17; }
	inline void set_U3CminValueU3Ek__BackingField_17(PrimitiveValue_t70D8365A1CB2217C466D27D6820362EF4331A0C8  value)
	{
		___U3CminValueU3Ek__BackingField_17 = value;
	}

	inline static int32_t get_offset_of_U3CmaxValueU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(ControlItem_t88696CA6FA7E3E6EDBF31BD5A8994D959F583B15, ___U3CmaxValueU3Ek__BackingField_18)); }
	inline PrimitiveValue_t70D8365A1CB2217C466D27D6820362EF4331A0C8  get_U3CmaxValueU3Ek__BackingField_18() const { return ___U3CmaxValueU3Ek__BackingField_18; }
	inline PrimitiveValue_t70D8365A1CB2217C466D27D6820362EF4331A0C8 * get_address_of_U3CmaxValueU3Ek__BackingField_18() { return &___U3CmaxValueU3Ek__BackingField_18; }
	inline void set_U3CmaxValueU3Ek__BackingField_18(PrimitiveValue_t70D8365A1CB2217C466D27D6820362EF4331A0C8  value)
	{
		___U3CmaxValueU3Ek__BackingField_18 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.InputSystem.Layouts.InputControlLayout/ControlItem
struct ControlItem_t88696CA6FA7E3E6EDBF31BD5A8994D959F583B15_marshaled_pinvoke
{
	InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED_marshaled_pinvoke ___U3CnameU3Ek__BackingField_0;
	InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED_marshaled_pinvoke ___U3ClayoutU3Ek__BackingField_1;
	InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED_marshaled_pinvoke ___U3CvariantsU3Ek__BackingField_2;
	char* ___U3CuseStateFromU3Ek__BackingField_3;
	char* ___U3CdisplayNameU3Ek__BackingField_4;
	char* ___U3CshortDisplayNameU3Ek__BackingField_5;
	ReadOnlyArray_1_t606E37DB0F1B75FCB4C4A029A0F6C2B4AE7DED75  ___U3CusagesU3Ek__BackingField_6;
	ReadOnlyArray_1_t606E37DB0F1B75FCB4C4A029A0F6C2B4AE7DED75  ___U3CaliasesU3Ek__BackingField_7;
	ReadOnlyArray_1_t644486DA5FB2B247FC64733F192E2894F46CFCF5  ___U3CparametersU3Ek__BackingField_8;
	ReadOnlyArray_1_tC87F80F9582CEDF1B66B5875A22591316B710C6E  ___U3CprocessorsU3Ek__BackingField_9;
	uint32_t ___U3CoffsetU3Ek__BackingField_10;
	uint32_t ___U3CbitU3Ek__BackingField_11;
	uint32_t ___U3CsizeInBitsU3Ek__BackingField_12;
	FourCC_t8D38CDCEAD02FE53A6C2E9AF01354E4E5AB400A9  ___U3CformatU3Ek__BackingField_13;
	int32_t ___U3CflagsU3Ek__BackingField_14;
	int32_t ___U3CarraySizeU3Ek__BackingField_15;
	PrimitiveValue_t70D8365A1CB2217C466D27D6820362EF4331A0C8_marshaled_pinvoke ___U3CdefaultStateU3Ek__BackingField_16;
	PrimitiveValue_t70D8365A1CB2217C466D27D6820362EF4331A0C8_marshaled_pinvoke ___U3CminValueU3Ek__BackingField_17;
	PrimitiveValue_t70D8365A1CB2217C466D27D6820362EF4331A0C8_marshaled_pinvoke ___U3CmaxValueU3Ek__BackingField_18;
};
// Native definition for COM marshalling of UnityEngine.InputSystem.Layouts.InputControlLayout/ControlItem
struct ControlItem_t88696CA6FA7E3E6EDBF31BD5A8994D959F583B15_marshaled_com
{
	InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED_marshaled_com ___U3CnameU3Ek__BackingField_0;
	InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED_marshaled_com ___U3ClayoutU3Ek__BackingField_1;
	InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED_marshaled_com ___U3CvariantsU3Ek__BackingField_2;
	Il2CppChar* ___U3CuseStateFromU3Ek__BackingField_3;
	Il2CppChar* ___U3CdisplayNameU3Ek__BackingField_4;
	Il2CppChar* ___U3CshortDisplayNameU3Ek__BackingField_5;
	ReadOnlyArray_1_t606E37DB0F1B75FCB4C4A029A0F6C2B4AE7DED75  ___U3CusagesU3Ek__BackingField_6;
	ReadOnlyArray_1_t606E37DB0F1B75FCB4C4A029A0F6C2B4AE7DED75  ___U3CaliasesU3Ek__BackingField_7;
	ReadOnlyArray_1_t644486DA5FB2B247FC64733F192E2894F46CFCF5  ___U3CparametersU3Ek__BackingField_8;
	ReadOnlyArray_1_tC87F80F9582CEDF1B66B5875A22591316B710C6E  ___U3CprocessorsU3Ek__BackingField_9;
	uint32_t ___U3CoffsetU3Ek__BackingField_10;
	uint32_t ___U3CbitU3Ek__BackingField_11;
	uint32_t ___U3CsizeInBitsU3Ek__BackingField_12;
	FourCC_t8D38CDCEAD02FE53A6C2E9AF01354E4E5AB400A9  ___U3CformatU3Ek__BackingField_13;
	int32_t ___U3CflagsU3Ek__BackingField_14;
	int32_t ___U3CarraySizeU3Ek__BackingField_15;
	PrimitiveValue_t70D8365A1CB2217C466D27D6820362EF4331A0C8_marshaled_com ___U3CdefaultStateU3Ek__BackingField_16;
	PrimitiveValue_t70D8365A1CB2217C466D27D6820362EF4331A0C8_marshaled_com ___U3CminValueU3Ek__BackingField_17;
	PrimitiveValue_t70D8365A1CB2217C466D27D6820362EF4331A0C8_marshaled_com ___U3CmaxValueU3Ek__BackingField_18;
};

// System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>>
struct  Enumerator_tF2D0EEBD6B861D9127DD6DC36B733FD42070858C 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_t9B1CABCBD4B6B7781E348FF332B32B333B543BA8 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184  ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_tF2D0EEBD6B861D9127DD6DC36B733FD42070858C, ___list_0)); }
	inline List_1_t9B1CABCBD4B6B7781E348FF332B32B333B543BA8 * get_list_0() const { return ___list_0; }
	inline List_1_t9B1CABCBD4B6B7781E348FF332B32B333B543BA8 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t9B1CABCBD4B6B7781E348FF332B32B333B543BA8 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_tF2D0EEBD6B861D9127DD6DC36B733FD42070858C, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_tF2D0EEBD6B861D9127DD6DC36B733FD42070858C, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_tF2D0EEBD6B861D9127DD6DC36B733FD42070858C, ___current_3)); }
	inline KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184  value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___current_3))->___key_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&((&(((&___current_3))->___value_1))->___stringValue_4))->___text_0))->___m_String_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___current_3))->___value_1))->___arrayValue_5), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___current_3))->___value_1))->___objectValue_6), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___current_3))->___value_1))->___anyValue_7), (void*)NULL);
		#endif
	}
};


// System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.NamedValue>
struct  Enumerator_tDEE846E92D2B22781FB09541CB1600E5398D3A20 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_tDC9592305A8D24F0DBC4A23F1719292FCB2887C1 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077  ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_tDEE846E92D2B22781FB09541CB1600E5398D3A20, ___list_0)); }
	inline List_1_tDC9592305A8D24F0DBC4A23F1719292FCB2887C1 * get_list_0() const { return ___list_0; }
	inline List_1_tDC9592305A8D24F0DBC4A23F1719292FCB2887C1 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_tDC9592305A8D24F0DBC4A23F1719292FCB2887C1 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_tDEE846E92D2B22781FB09541CB1600E5398D3A20, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_tDEE846E92D2B22781FB09541CB1600E5398D3A20, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_tDEE846E92D2B22781FB09541CB1600E5398D3A20, ___current_3)); }
	inline NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077  get_current_3() const { return ___current_3; }
	inline NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077  value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___current_3))->___U3CnameU3Ek__BackingField_1), (void*)NULL);
	}
};


// System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Layouts.InputControlLayout/ControlItem>
struct  Enumerator_tA2E47432D9BF33FCB9EAF5B6C3720221AACA50C1 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_tB7B59A258A4A84FCB5A2CC59F934C8B230EA963F * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	ControlItem_t88696CA6FA7E3E6EDBF31BD5A8994D959F583B15  ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_tA2E47432D9BF33FCB9EAF5B6C3720221AACA50C1, ___list_0)); }
	inline List_1_tB7B59A258A4A84FCB5A2CC59F934C8B230EA963F * get_list_0() const { return ___list_0; }
	inline List_1_tB7B59A258A4A84FCB5A2CC59F934C8B230EA963F ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_tB7B59A258A4A84FCB5A2CC59F934C8B230EA963F * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_tA2E47432D9BF33FCB9EAF5B6C3720221AACA50C1, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_tA2E47432D9BF33FCB9EAF5B6C3720221AACA50C1, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_tA2E47432D9BF33FCB9EAF5B6C3720221AACA50C1, ___current_3)); }
	inline ControlItem_t88696CA6FA7E3E6EDBF31BD5A8994D959F583B15  get_current_3() const { return ___current_3; }
	inline ControlItem_t88696CA6FA7E3E6EDBF31BD5A8994D959F583B15 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(ControlItem_t88696CA6FA7E3E6EDBF31BD5A8994D959F583B15  value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___current_3))->___U3CnameU3Ek__BackingField_0))->___m_StringOriginalCase_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___current_3))->___U3CnameU3Ek__BackingField_0))->___m_StringLowerCase_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___current_3))->___U3ClayoutU3Ek__BackingField_1))->___m_StringOriginalCase_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___current_3))->___U3ClayoutU3Ek__BackingField_1))->___m_StringLowerCase_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___current_3))->___U3CvariantsU3Ek__BackingField_2))->___m_StringOriginalCase_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___current_3))->___U3CvariantsU3Ek__BackingField_2))->___m_StringLowerCase_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___current_3))->___U3CuseStateFromU3Ek__BackingField_3), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___current_3))->___U3CdisplayNameU3Ek__BackingField_4), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___current_3))->___U3CshortDisplayNameU3Ek__BackingField_5), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___current_3))->___U3CusagesU3Ek__BackingField_6))->___m_Array_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___current_3))->___U3CaliasesU3Ek__BackingField_7))->___m_Array_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___current_3))->___U3CparametersU3Ek__BackingField_8))->___m_Array_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___current_3))->___U3CprocessorsU3Ek__BackingField_9))->___m_Array_0), (void*)NULL);
		#endif
	}
};


// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,System.Boolean>
struct  Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,UnityEngine.InputSystem.Utilities.InternedString>
struct  Func_2_tB181F4C9A24C0634DF4A10CFBFFE6B45B3796BBC  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,System.Object>
struct  Func_2_t711512467B454C91EB8F3FB19CD6D141AAB5B6B1  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.InputSystem.Utilities.NamedValue,System.Boolean>
struct  Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.InputSystem.Utilities.NamedValue,UnityEngine.InputSystem.Utilities.InternedString>
struct  Func_2_tA21678F1D9F9F4617D645755285D28EDE5F9AE44  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.InputSystem.Utilities.NamedValue,System.Object>
struct  Func_2_t5B101C0B26ADAA3B1A77BC50D51CC42364E5409E  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.InputSystem.Layouts.InputControlLayout/ControlItem,System.Boolean>
struct  Func_2_t6D3B67831BA6BBB7266E343D7EAEFF60C17F92DF  : public MulticastDelegate_t
{
public:

public:
};


// System.Linq.Enumerable/Iterator`1<UnityEngine.InputSystem.Layouts.InputControlLayout/ControlItem>
struct  Iterator_1_t2711EA98CD5C8E6E87A241C66556EFDB99842473  : public RuntimeObject
{
public:
	// System.Int32 System.Linq.Enumerable/Iterator`1::threadId
	int32_t ___threadId_0;
	// System.Int32 System.Linq.Enumerable/Iterator`1::state
	int32_t ___state_1;
	// TSource System.Linq.Enumerable/Iterator`1::current
	ControlItem_t88696CA6FA7E3E6EDBF31BD5A8994D959F583B15  ___current_2;

public:
	inline static int32_t get_offset_of_threadId_0() { return static_cast<int32_t>(offsetof(Iterator_1_t2711EA98CD5C8E6E87A241C66556EFDB99842473, ___threadId_0)); }
	inline int32_t get_threadId_0() const { return ___threadId_0; }
	inline int32_t* get_address_of_threadId_0() { return &___threadId_0; }
	inline void set_threadId_0(int32_t value)
	{
		___threadId_0 = value;
	}

	inline static int32_t get_offset_of_state_1() { return static_cast<int32_t>(offsetof(Iterator_1_t2711EA98CD5C8E6E87A241C66556EFDB99842473, ___state_1)); }
	inline int32_t get_state_1() const { return ___state_1; }
	inline int32_t* get_address_of_state_1() { return &___state_1; }
	inline void set_state_1(int32_t value)
	{
		___state_1 = value;
	}

	inline static int32_t get_offset_of_current_2() { return static_cast<int32_t>(offsetof(Iterator_1_t2711EA98CD5C8E6E87A241C66556EFDB99842473, ___current_2)); }
	inline ControlItem_t88696CA6FA7E3E6EDBF31BD5A8994D959F583B15  get_current_2() const { return ___current_2; }
	inline ControlItem_t88696CA6FA7E3E6EDBF31BD5A8994D959F583B15 * get_address_of_current_2() { return &___current_2; }
	inline void set_current_2(ControlItem_t88696CA6FA7E3E6EDBF31BD5A8994D959F583B15  value)
	{
		___current_2 = value;
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___current_2))->___U3CnameU3Ek__BackingField_0))->___m_StringOriginalCase_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___current_2))->___U3CnameU3Ek__BackingField_0))->___m_StringLowerCase_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___current_2))->___U3ClayoutU3Ek__BackingField_1))->___m_StringOriginalCase_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___current_2))->___U3ClayoutU3Ek__BackingField_1))->___m_StringLowerCase_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___current_2))->___U3CvariantsU3Ek__BackingField_2))->___m_StringOriginalCase_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___current_2))->___U3CvariantsU3Ek__BackingField_2))->___m_StringLowerCase_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___current_2))->___U3CuseStateFromU3Ek__BackingField_3), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___current_2))->___U3CdisplayNameU3Ek__BackingField_4), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___current_2))->___U3CshortDisplayNameU3Ek__BackingField_5), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___current_2))->___U3CusagesU3Ek__BackingField_6))->___m_Array_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___current_2))->___U3CaliasesU3Ek__BackingField_7))->___m_Array_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___current_2))->___U3CparametersU3Ek__BackingField_8))->___m_Array_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___current_2))->___U3CprocessorsU3Ek__BackingField_9))->___m_Array_0), (void*)NULL);
		#endif
	}
};


// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.UIElements.StyleSelectorPart,UnityEngine.InputSystem.Utilities.InternedString>
struct  WhereSelectListIterator_2_tCACD1A54916845C9EA0DA248A5D1197354F1AB99  : public Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_tE7DC2F192C483F9E5722779E3C2C1910490AAE59 * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_tFBBF89B72C697489D82FBE8FF822B191D2EE9A41 * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_tC861559498255F6677610CED32D2161012D8D897  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tCACD1A54916845C9EA0DA248A5D1197354F1AB99, ___source_3)); }
	inline List_1_tE7DC2F192C483F9E5722779E3C2C1910490AAE59 * get_source_3() const { return ___source_3; }
	inline List_1_tE7DC2F192C483F9E5722779E3C2C1910490AAE59 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_tE7DC2F192C483F9E5722779E3C2C1910490AAE59 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tCACD1A54916845C9EA0DA248A5D1197354F1AB99, ___predicate_4)); }
	inline Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tCACD1A54916845C9EA0DA248A5D1197354F1AB99, ___selector_5)); }
	inline Func_2_tFBBF89B72C697489D82FBE8FF822B191D2EE9A41 * get_selector_5() const { return ___selector_5; }
	inline Func_2_tFBBF89B72C697489D82FBE8FF822B191D2EE9A41 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_tFBBF89B72C697489D82FBE8FF822B191D2EE9A41 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tCACD1A54916845C9EA0DA248A5D1197354F1AB99, ___enumerator_6)); }
	inline Enumerator_tC861559498255F6677610CED32D2161012D8D897  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_tC861559498255F6677610CED32D2161012D8D897 * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_tC861559498255F6677610CED32D2161012D8D897  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___m_Value_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___tempData_2), (void*)NULL);
		#endif
	}
};


// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.UIElements.StyleSelectorPart,System.Object>
struct  WhereSelectListIterator_2_t0F9C36BA8F331B1C4C968AB639F343F642078798  : public Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_tE7DC2F192C483F9E5722779E3C2C1910490AAE59 * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_t43979C5A872BA9ED7A923D533A01683A98D71957 * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_tC861559498255F6677610CED32D2161012D8D897  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t0F9C36BA8F331B1C4C968AB639F343F642078798, ___source_3)); }
	inline List_1_tE7DC2F192C483F9E5722779E3C2C1910490AAE59 * get_source_3() const { return ___source_3; }
	inline List_1_tE7DC2F192C483F9E5722779E3C2C1910490AAE59 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_tE7DC2F192C483F9E5722779E3C2C1910490AAE59 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t0F9C36BA8F331B1C4C968AB639F343F642078798, ___predicate_4)); }
	inline Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t0F9C36BA8F331B1C4C968AB639F343F642078798, ___selector_5)); }
	inline Func_2_t43979C5A872BA9ED7A923D533A01683A98D71957 * get_selector_5() const { return ___selector_5; }
	inline Func_2_t43979C5A872BA9ED7A923D533A01683A98D71957 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t43979C5A872BA9ED7A923D533A01683A98D71957 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t0F9C36BA8F331B1C4C968AB639F343F642078798, ___enumerator_6)); }
	inline Enumerator_tC861559498255F6677610CED32D2161012D8D897  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_tC861559498255F6677610CED32D2161012D8D897 * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_tC861559498255F6677610CED32D2161012D8D897  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___m_Value_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___tempData_2), (void*)NULL);
		#endif
	}
};


// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,UnityEngine.InputSystem.Utilities.InternedString>
struct  WhereSelectListIterator_2_tF2D97D886F080B26C19947C7C6A56563B86B14D7  : public Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_tEC0B302C3EF23400C3EC92ED7672A715CA602068 * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_t48DD1C8E439811C492BAFE220A679DB57D7C9370 * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_tF2D95CC18A66F3DE682EAC9B88E865750DFAEF60  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tF2D97D886F080B26C19947C7C6A56563B86B14D7, ___source_3)); }
	inline List_1_tEC0B302C3EF23400C3EC92ED7672A715CA602068 * get_source_3() const { return ___source_3; }
	inline List_1_tEC0B302C3EF23400C3EC92ED7672A715CA602068 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_tEC0B302C3EF23400C3EC92ED7672A715CA602068 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tF2D97D886F080B26C19947C7C6A56563B86B14D7, ___predicate_4)); }
	inline Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tF2D97D886F080B26C19947C7C6A56563B86B14D7, ___selector_5)); }
	inline Func_2_t48DD1C8E439811C492BAFE220A679DB57D7C9370 * get_selector_5() const { return ___selector_5; }
	inline Func_2_t48DD1C8E439811C492BAFE220A679DB57D7C9370 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t48DD1C8E439811C492BAFE220A679DB57D7C9370 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tF2D97D886F080B26C19947C7C6A56563B86B14D7, ___enumerator_6)); }
	inline Enumerator_tF2D95CC18A66F3DE682EAC9B88E865750DFAEF60  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_tF2D95CC18A66F3DE682EAC9B88E865750DFAEF60 * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_tF2D95CC18A66F3DE682EAC9B88E865750DFAEF60  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&((&(((&___enumerator_6))->___current_3))->___stringValue_4))->___text_0))->___m_String_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___arrayValue_5), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___objectValue_6), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___anyValue_7), (void*)NULL);
		#endif
	}
};


// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,System.Object>
struct  WhereSelectListIterator_2_t2F7487B4BF5A9E71CBE06091C45AF7D98CF9F1AC  : public Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_tEC0B302C3EF23400C3EC92ED7672A715CA602068 * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_t1628B2E4EEC7A32D5B688E54415BC42EDA9D2899 * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_tF2D95CC18A66F3DE682EAC9B88E865750DFAEF60  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t2F7487B4BF5A9E71CBE06091C45AF7D98CF9F1AC, ___source_3)); }
	inline List_1_tEC0B302C3EF23400C3EC92ED7672A715CA602068 * get_source_3() const { return ___source_3; }
	inline List_1_tEC0B302C3EF23400C3EC92ED7672A715CA602068 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_tEC0B302C3EF23400C3EC92ED7672A715CA602068 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t2F7487B4BF5A9E71CBE06091C45AF7D98CF9F1AC, ___predicate_4)); }
	inline Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t2F7487B4BF5A9E71CBE06091C45AF7D98CF9F1AC, ___selector_5)); }
	inline Func_2_t1628B2E4EEC7A32D5B688E54415BC42EDA9D2899 * get_selector_5() const { return ___selector_5; }
	inline Func_2_t1628B2E4EEC7A32D5B688E54415BC42EDA9D2899 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t1628B2E4EEC7A32D5B688E54415BC42EDA9D2899 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t2F7487B4BF5A9E71CBE06091C45AF7D98CF9F1AC, ___enumerator_6)); }
	inline Enumerator_tF2D95CC18A66F3DE682EAC9B88E865750DFAEF60  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_tF2D95CC18A66F3DE682EAC9B88E865750DFAEF60 * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_tF2D95CC18A66F3DE682EAC9B88E865750DFAEF60  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&((&(((&___enumerator_6))->___current_3))->___stringValue_4))->___text_0))->___m_String_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___arrayValue_5), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___objectValue_6), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___anyValue_7), (void*)NULL);
		#endif
	}
};


// System.Linq.Enumerable/WhereArrayIterator`1<UnityEngine.InputSystem.Layouts.InputControlLayout/ControlItem>
struct  WhereArrayIterator_1_t27BA96004BE53807B1028D1D8BFA586A91E96030  : public Iterator_1_t2711EA98CD5C8E6E87A241C66556EFDB99842473
{
public:
	// TSource[] System.Linq.Enumerable/WhereArrayIterator`1::source
	ControlItemU5BU5D_t534859CA6A52E2EC9DDDCA52FE280E8D5CD598DB* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereArrayIterator`1::predicate
	Func_2_t6D3B67831BA6BBB7266E343D7EAEFF60C17F92DF * ___predicate_4;
	// System.Int32 System.Linq.Enumerable/WhereArrayIterator`1::index
	int32_t ___index_5;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereArrayIterator_1_t27BA96004BE53807B1028D1D8BFA586A91E96030, ___source_3)); }
	inline ControlItemU5BU5D_t534859CA6A52E2EC9DDDCA52FE280E8D5CD598DB* get_source_3() const { return ___source_3; }
	inline ControlItemU5BU5D_t534859CA6A52E2EC9DDDCA52FE280E8D5CD598DB** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(ControlItemU5BU5D_t534859CA6A52E2EC9DDDCA52FE280E8D5CD598DB* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereArrayIterator_1_t27BA96004BE53807B1028D1D8BFA586A91E96030, ___predicate_4)); }
	inline Func_2_t6D3B67831BA6BBB7266E343D7EAEFF60C17F92DF * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t6D3B67831BA6BBB7266E343D7EAEFF60C17F92DF ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t6D3B67831BA6BBB7266E343D7EAEFF60C17F92DF * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_index_5() { return static_cast<int32_t>(offsetof(WhereArrayIterator_1_t27BA96004BE53807B1028D1D8BFA586A91E96030, ___index_5)); }
	inline int32_t get_index_5() const { return ___index_5; }
	inline int32_t* get_address_of_index_5() { return &___index_5; }
	inline void set_index_5(int32_t value)
	{
		___index_5 = value;
	}
};


// System.Linq.Enumerable/WhereEnumerableIterator`1<UnityEngine.InputSystem.Layouts.InputControlLayout/ControlItem>
struct  WhereEnumerableIterator_1_t9A9C4F1C2DF243CCF60AE823D966E68DD70188D0  : public Iterator_1_t2711EA98CD5C8E6E87A241C66556EFDB99842473
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereEnumerableIterator`1::source
	RuntimeObject* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereEnumerableIterator`1::predicate
	Func_2_t6D3B67831BA6BBB7266E343D7EAEFF60C17F92DF * ___predicate_4;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/WhereEnumerableIterator`1::enumerator
	RuntimeObject* ___enumerator_5;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereEnumerableIterator_1_t9A9C4F1C2DF243CCF60AE823D966E68DD70188D0, ___source_3)); }
	inline RuntimeObject* get_source_3() const { return ___source_3; }
	inline RuntimeObject** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(RuntimeObject* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereEnumerableIterator_1_t9A9C4F1C2DF243CCF60AE823D966E68DD70188D0, ___predicate_4)); }
	inline Func_2_t6D3B67831BA6BBB7266E343D7EAEFF60C17F92DF * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t6D3B67831BA6BBB7266E343D7EAEFF60C17F92DF ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t6D3B67831BA6BBB7266E343D7EAEFF60C17F92DF * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_5() { return static_cast<int32_t>(offsetof(WhereEnumerableIterator_1_t9A9C4F1C2DF243CCF60AE823D966E68DD70188D0, ___enumerator_5)); }
	inline RuntimeObject* get_enumerator_5() const { return ___enumerator_5; }
	inline RuntimeObject** get_address_of_enumerator_5() { return &___enumerator_5; }
	inline void set_enumerator_5(RuntimeObject* value)
	{
		___enumerator_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumerator_5), (void*)value);
	}
};


// System.Linq.Enumerable/WhereListIterator`1<UnityEngine.InputSystem.Layouts.InputControlLayout/ControlItem>
struct  WhereListIterator_1_t578D3A8880FE8DFFE9A8B7462A4D3004DEDB1FD0  : public Iterator_1_t2711EA98CD5C8E6E87A241C66556EFDB99842473
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereListIterator`1::source
	List_1_tB7B59A258A4A84FCB5A2CC59F934C8B230EA963F * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereListIterator`1::predicate
	Func_2_t6D3B67831BA6BBB7266E343D7EAEFF60C17F92DF * ___predicate_4;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereListIterator`1::enumerator
	Enumerator_tA2E47432D9BF33FCB9EAF5B6C3720221AACA50C1  ___enumerator_5;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereListIterator_1_t578D3A8880FE8DFFE9A8B7462A4D3004DEDB1FD0, ___source_3)); }
	inline List_1_tB7B59A258A4A84FCB5A2CC59F934C8B230EA963F * get_source_3() const { return ___source_3; }
	inline List_1_tB7B59A258A4A84FCB5A2CC59F934C8B230EA963F ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_tB7B59A258A4A84FCB5A2CC59F934C8B230EA963F * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereListIterator_1_t578D3A8880FE8DFFE9A8B7462A4D3004DEDB1FD0, ___predicate_4)); }
	inline Func_2_t6D3B67831BA6BBB7266E343D7EAEFF60C17F92DF * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t6D3B67831BA6BBB7266E343D7EAEFF60C17F92DF ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t6D3B67831BA6BBB7266E343D7EAEFF60C17F92DF * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_5() { return static_cast<int32_t>(offsetof(WhereListIterator_1_t578D3A8880FE8DFFE9A8B7462A4D3004DEDB1FD0, ___enumerator_5)); }
	inline Enumerator_tA2E47432D9BF33FCB9EAF5B6C3720221AACA50C1  get_enumerator_5() const { return ___enumerator_5; }
	inline Enumerator_tA2E47432D9BF33FCB9EAF5B6C3720221AACA50C1 * get_address_of_enumerator_5() { return &___enumerator_5; }
	inline void set_enumerator_5(Enumerator_tA2E47432D9BF33FCB9EAF5B6C3720221AACA50C1  value)
	{
		___enumerator_5 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_5))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___enumerator_5))->___current_3))->___U3CnameU3Ek__BackingField_0))->___m_StringOriginalCase_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___enumerator_5))->___current_3))->___U3CnameU3Ek__BackingField_0))->___m_StringLowerCase_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___enumerator_5))->___current_3))->___U3ClayoutU3Ek__BackingField_1))->___m_StringOriginalCase_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___enumerator_5))->___current_3))->___U3ClayoutU3Ek__BackingField_1))->___m_StringLowerCase_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___enumerator_5))->___current_3))->___U3CvariantsU3Ek__BackingField_2))->___m_StringOriginalCase_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___enumerator_5))->___current_3))->___U3CvariantsU3Ek__BackingField_2))->___m_StringLowerCase_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_5))->___current_3))->___U3CuseStateFromU3Ek__BackingField_3), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_5))->___current_3))->___U3CdisplayNameU3Ek__BackingField_4), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_5))->___current_3))->___U3CshortDisplayNameU3Ek__BackingField_5), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___enumerator_5))->___current_3))->___U3CusagesU3Ek__BackingField_6))->___m_Array_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___enumerator_5))->___current_3))->___U3CaliasesU3Ek__BackingField_7))->___m_Array_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___enumerator_5))->___current_3))->___U3CparametersU3Ek__BackingField_8))->___m_Array_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___enumerator_5))->___current_3))->___U3CprocessorsU3Ek__BackingField_9))->___m_Array_0), (void*)NULL);
		#endif
	}
};


// System.Linq.Enumerable/WhereSelectListIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,UnityEngine.InputSystem.Utilities.InternedString>
struct  WhereSelectListIterator_2_tF2F8745320F9D29534105CFDF2FADABD42A2D3F7  : public Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_t9B1CABCBD4B6B7781E348FF332B32B333B543BA8 * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_tB181F4C9A24C0634DF4A10CFBFFE6B45B3796BBC * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_tF2D0EEBD6B861D9127DD6DC36B733FD42070858C  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tF2F8745320F9D29534105CFDF2FADABD42A2D3F7, ___source_3)); }
	inline List_1_t9B1CABCBD4B6B7781E348FF332B32B333B543BA8 * get_source_3() const { return ___source_3; }
	inline List_1_t9B1CABCBD4B6B7781E348FF332B32B333B543BA8 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_t9B1CABCBD4B6B7781E348FF332B32B333B543BA8 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tF2F8745320F9D29534105CFDF2FADABD42A2D3F7, ___predicate_4)); }
	inline Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tF2F8745320F9D29534105CFDF2FADABD42A2D3F7, ___selector_5)); }
	inline Func_2_tB181F4C9A24C0634DF4A10CFBFFE6B45B3796BBC * get_selector_5() const { return ___selector_5; }
	inline Func_2_tB181F4C9A24C0634DF4A10CFBFFE6B45B3796BBC ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_tB181F4C9A24C0634DF4A10CFBFFE6B45B3796BBC * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tF2F8745320F9D29534105CFDF2FADABD42A2D3F7, ___enumerator_6)); }
	inline Enumerator_tF2D0EEBD6B861D9127DD6DC36B733FD42070858C  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_tF2D0EEBD6B861D9127DD6DC36B733FD42070858C * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_tF2D0EEBD6B861D9127DD6DC36B733FD42070858C  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___key_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&((&((&(((&___enumerator_6))->___current_3))->___value_1))->___stringValue_4))->___text_0))->___m_String_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___enumerator_6))->___current_3))->___value_1))->___arrayValue_5), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___enumerator_6))->___current_3))->___value_1))->___objectValue_6), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___enumerator_6))->___current_3))->___value_1))->___anyValue_7), (void*)NULL);
		#endif
	}
};


// System.Linq.Enumerable/WhereSelectListIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,System.Object>
struct  WhereSelectListIterator_2_t15BF3EF128C005C26DE8132D49E71B3C1FE6E211  : public Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_t9B1CABCBD4B6B7781E348FF332B32B333B543BA8 * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_t711512467B454C91EB8F3FB19CD6D141AAB5B6B1 * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_tF2D0EEBD6B861D9127DD6DC36B733FD42070858C  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t15BF3EF128C005C26DE8132D49E71B3C1FE6E211, ___source_3)); }
	inline List_1_t9B1CABCBD4B6B7781E348FF332B32B333B543BA8 * get_source_3() const { return ___source_3; }
	inline List_1_t9B1CABCBD4B6B7781E348FF332B32B333B543BA8 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_t9B1CABCBD4B6B7781E348FF332B32B333B543BA8 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t15BF3EF128C005C26DE8132D49E71B3C1FE6E211, ___predicate_4)); }
	inline Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t15BF3EF128C005C26DE8132D49E71B3C1FE6E211, ___selector_5)); }
	inline Func_2_t711512467B454C91EB8F3FB19CD6D141AAB5B6B1 * get_selector_5() const { return ___selector_5; }
	inline Func_2_t711512467B454C91EB8F3FB19CD6D141AAB5B6B1 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t711512467B454C91EB8F3FB19CD6D141AAB5B6B1 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t15BF3EF128C005C26DE8132D49E71B3C1FE6E211, ___enumerator_6)); }
	inline Enumerator_tF2D0EEBD6B861D9127DD6DC36B733FD42070858C  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_tF2D0EEBD6B861D9127DD6DC36B733FD42070858C * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_tF2D0EEBD6B861D9127DD6DC36B733FD42070858C  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___key_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&((&((&(((&___enumerator_6))->___current_3))->___value_1))->___stringValue_4))->___text_0))->___m_String_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___enumerator_6))->___current_3))->___value_1))->___arrayValue_5), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___enumerator_6))->___current_3))->___value_1))->___objectValue_6), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___enumerator_6))->___current_3))->___value_1))->___anyValue_7), (void*)NULL);
		#endif
	}
};


// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,UnityEngine.InputSystem.Utilities.InternedString>
struct  WhereSelectListIterator_2_t84216899116C33A67088960F25CA2D6E1DDC786D  : public Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_tDC9592305A8D24F0DBC4A23F1719292FCB2887C1 * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_tA21678F1D9F9F4617D645755285D28EDE5F9AE44 * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_tDEE846E92D2B22781FB09541CB1600E5398D3A20  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t84216899116C33A67088960F25CA2D6E1DDC786D, ___source_3)); }
	inline List_1_tDC9592305A8D24F0DBC4A23F1719292FCB2887C1 * get_source_3() const { return ___source_3; }
	inline List_1_tDC9592305A8D24F0DBC4A23F1719292FCB2887C1 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_tDC9592305A8D24F0DBC4A23F1719292FCB2887C1 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t84216899116C33A67088960F25CA2D6E1DDC786D, ___predicate_4)); }
	inline Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t84216899116C33A67088960F25CA2D6E1DDC786D, ___selector_5)); }
	inline Func_2_tA21678F1D9F9F4617D645755285D28EDE5F9AE44 * get_selector_5() const { return ___selector_5; }
	inline Func_2_tA21678F1D9F9F4617D645755285D28EDE5F9AE44 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_tA21678F1D9F9F4617D645755285D28EDE5F9AE44 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t84216899116C33A67088960F25CA2D6E1DDC786D, ___enumerator_6)); }
	inline Enumerator_tDEE846E92D2B22781FB09541CB1600E5398D3A20  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_tDEE846E92D2B22781FB09541CB1600E5398D3A20 * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_tDEE846E92D2B22781FB09541CB1600E5398D3A20  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___U3CnameU3Ek__BackingField_1), (void*)NULL);
		#endif
	}
};


// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,System.Object>
struct  WhereSelectListIterator_2_tA4D5B2B639DCB29B4C84236B3E2651F17B522430  : public Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_tDC9592305A8D24F0DBC4A23F1719292FCB2887C1 * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_t5B101C0B26ADAA3B1A77BC50D51CC42364E5409E * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_tDEE846E92D2B22781FB09541CB1600E5398D3A20  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tA4D5B2B639DCB29B4C84236B3E2651F17B522430, ___source_3)); }
	inline List_1_tDC9592305A8D24F0DBC4A23F1719292FCB2887C1 * get_source_3() const { return ___source_3; }
	inline List_1_tDC9592305A8D24F0DBC4A23F1719292FCB2887C1 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_tDC9592305A8D24F0DBC4A23F1719292FCB2887C1 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tA4D5B2B639DCB29B4C84236B3E2651F17B522430, ___predicate_4)); }
	inline Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tA4D5B2B639DCB29B4C84236B3E2651F17B522430, ___selector_5)); }
	inline Func_2_t5B101C0B26ADAA3B1A77BC50D51CC42364E5409E * get_selector_5() const { return ___selector_5; }
	inline Func_2_t5B101C0B26ADAA3B1A77BC50D51CC42364E5409E ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t5B101C0B26ADAA3B1A77BC50D51CC42364E5409E * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tA4D5B2B639DCB29B4C84236B3E2651F17B522430, ___enumerator_6)); }
	inline Enumerator_tDEE846E92D2B22781FB09541CB1600E5398D3A20  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_tDEE846E92D2B22781FB09541CB1600E5398D3A20 * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_tDEE846E92D2B22781FB09541CB1600E5398D3A20  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___U3CnameU3Ek__BackingField_1), (void*)NULL);
		#endif
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// System.Object[]
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// UnityEngine.InputSystem.Layouts.InputControlLayout/ControlItem[]
struct ControlItemU5BU5D_t534859CA6A52E2EC9DDDCA52FE280E8D5CD598DB  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) ControlItem_t88696CA6FA7E3E6EDBF31BD5A8994D959F583B15  m_Items[1];

public:
	inline ControlItem_t88696CA6FA7E3E6EDBF31BD5A8994D959F583B15  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline ControlItem_t88696CA6FA7E3E6EDBF31BD5A8994D959F583B15 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, ControlItem_t88696CA6FA7E3E6EDBF31BD5A8994D959F583B15  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)&((&((m_Items + index)->___U3CnameU3Ek__BackingField_0))->___m_StringOriginalCase_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((m_Items + index)->___U3CnameU3Ek__BackingField_0))->___m_StringLowerCase_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((m_Items + index)->___U3ClayoutU3Ek__BackingField_1))->___m_StringOriginalCase_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((m_Items + index)->___U3ClayoutU3Ek__BackingField_1))->___m_StringLowerCase_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((m_Items + index)->___U3CvariantsU3Ek__BackingField_2))->___m_StringOriginalCase_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((m_Items + index)->___U3CvariantsU3Ek__BackingField_2))->___m_StringLowerCase_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___U3CuseStateFromU3Ek__BackingField_3), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___U3CdisplayNameU3Ek__BackingField_4), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___U3CshortDisplayNameU3Ek__BackingField_5), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((m_Items + index)->___U3CusagesU3Ek__BackingField_6))->___m_Array_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((m_Items + index)->___U3CaliasesU3Ek__BackingField_7))->___m_Array_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((m_Items + index)->___U3CparametersU3Ek__BackingField_8))->___m_Array_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((m_Items + index)->___U3CprocessorsU3Ek__BackingField_9))->___m_Array_0), (void*)NULL);
		#endif
	}
	inline ControlItem_t88696CA6FA7E3E6EDBF31BD5A8994D959F583B15  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline ControlItem_t88696CA6FA7E3E6EDBF31BD5A8994D959F583B15 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, ControlItem_t88696CA6FA7E3E6EDBF31BD5A8994D959F583B15  value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)&((&((m_Items + index)->___U3CnameU3Ek__BackingField_0))->___m_StringOriginalCase_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((m_Items + index)->___U3CnameU3Ek__BackingField_0))->___m_StringLowerCase_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((m_Items + index)->___U3ClayoutU3Ek__BackingField_1))->___m_StringOriginalCase_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((m_Items + index)->___U3ClayoutU3Ek__BackingField_1))->___m_StringLowerCase_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((m_Items + index)->___U3CvariantsU3Ek__BackingField_2))->___m_StringOriginalCase_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((m_Items + index)->___U3CvariantsU3Ek__BackingField_2))->___m_StringLowerCase_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___U3CuseStateFromU3Ek__BackingField_3), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___U3CdisplayNameU3Ek__BackingField_4), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___U3CshortDisplayNameU3Ek__BackingField_5), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((m_Items + index)->___U3CusagesU3Ek__BackingField_6))->___m_Array_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((m_Items + index)->___U3CaliasesU3Ek__BackingField_7))->___m_Array_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((m_Items + index)->___U3CparametersU3Ek__BackingField_8))->___m_Array_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((m_Items + index)->___U3CprocessorsU3Ek__BackingField_9))->___m_Array_0), (void*)NULL);
		#endif
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>[]
struct KeyValuePair_2U5BU5D_tC4ED6A2024348F6B2CB712DC491F7E57AEF32630  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184  m_Items[1];

public:
	inline KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___key_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&((&((m_Items + index)->___value_1))->___stringValue_4))->___text_0))->___m_String_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((m_Items + index)->___value_1))->___arrayValue_5), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((m_Items + index)->___value_1))->___objectValue_6), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((m_Items + index)->___value_1))->___anyValue_7), (void*)NULL);
		#endif
	}
	inline KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184  value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___key_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&((&((m_Items + index)->___value_1))->___stringValue_4))->___text_0))->___m_String_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((m_Items + index)->___value_1))->___arrayValue_5), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((m_Items + index)->___value_1))->___objectValue_6), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((m_Items + index)->___value_1))->___anyValue_7), (void*)NULL);
		#endif
	}
};
// UnityEngine.InputSystem.Utilities.InternedString[]
struct InternedStringU5BU5D_t1EAE8433C8EF7C76894673801EE0EE9F6833E9A4  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  m_Items[1];

public:
	inline InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___m_StringOriginalCase_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___m_StringLowerCase_1), (void*)NULL);
		#endif
	}
	inline InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___m_StringOriginalCase_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___m_StringLowerCase_1), (void*)NULL);
		#endif
	}
};
// UnityEngine.InputSystem.Utilities.NameAndParameters[]
struct NameAndParametersU5BU5D_t960DDB52F44DD1231B0C626E9C859F1E0B2F5EAD  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA  m_Items[1];

public:
	inline NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___U3CnameU3Ek__BackingField_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((m_Items + index)->___U3CparametersU3Ek__BackingField_1))->___m_Array_0), (void*)NULL);
		#endif
	}
	inline NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA  value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___U3CnameU3Ek__BackingField_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((m_Items + index)->___U3CparametersU3Ek__BackingField_1))->___m_Array_0), (void*)NULL);
		#endif
	}
};
// UnityEngine.InputSystem.Utilities.NamedValue[]
struct NamedValueU5BU5D_t16F5D30B70B5FEDA89AE16CFD61FACEC7DF85391  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077  m_Items[1];

public:
	inline NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___U3CnameU3Ek__BackingField_1), (void*)NULL);
	}
	inline NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077  value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___U3CnameU3Ek__BackingField_1), (void*)NULL);
	}
};
// UnityEngine.UIElements.StyleSelectorPart[]
struct StyleSelectorPartU5BU5D_tD8762DADE4104C7D73D7B821DF7E5BBC87417BB7  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) StyleSelectorPart_t707EDC970FC0F3E91E56DCBC178672A120426D54  m_Items[1];

public:
	inline StyleSelectorPart_t707EDC970FC0F3E91E56DCBC178672A120426D54  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline StyleSelectorPart_t707EDC970FC0F3E91E56DCBC178672A120426D54 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, StyleSelectorPart_t707EDC970FC0F3E91E56DCBC178672A120426D54  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___m_Value_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___tempData_2), (void*)NULL);
		#endif
	}
	inline StyleSelectorPart_t707EDC970FC0F3E91E56DCBC178672A120426D54  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline StyleSelectorPart_t707EDC970FC0F3E91E56DCBC178672A120426D54 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, StyleSelectorPart_t707EDC970FC0F3E91E56DCBC178672A120426D54  value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___m_Value_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___tempData_2), (void*)NULL);
		#endif
	}
};
// UnityEngine.InputSystem.Utilities.Substring[]
struct SubstringU5BU5D_tE332884993470F5CF7BAAA950414E4E24691A8D0  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F  m_Items[1];

public:
	inline Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___m_String_0), (void*)NULL);
	}
	inline Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F  value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___m_String_0), (void*)NULL);
	}
};
// UnityEngine.InputSystem.Utilities.JsonParser/JsonValue[]
struct JsonValueU5BU5D_t2E1CBAA099182C024E4921379907D707DA049098  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB  m_Items[1];

public:
	inline JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)&((&((&((m_Items + index)->___stringValue_4))->___text_0))->___m_String_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___arrayValue_5), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___objectValue_6), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___anyValue_7), (void*)NULL);
		#endif
	}
	inline JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB  value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)&((&((&((m_Items + index)->___stringValue_4))->___text_0))->___m_String_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___arrayValue_5), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___objectValue_6), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___anyValue_7), (void*)NULL);
		#endif
	}
};


// !0 System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject * Enumerator_get_Current_m4C91D0E84532DF10C654917487D82CB0AB27693B_gshared_inline (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_m2E56233762839CE55C67E00AC8DD3D4D3F6C0DF0_gshared (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Layouts.InputControlLayout/ControlItem>::get_Current()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR ControlItem_t88696CA6FA7E3E6EDBF31BD5A8994D959F583B15  Enumerator_get_Current_m98F1CC86098DFE6B87C86B2C5333FDAAAE66AF69_gshared_inline (Enumerator_tA2E47432D9BF33FCB9EAF5B6C3720221AACA50C1 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Layouts.InputControlLayout/ControlItem>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_mCB47A9BC10861CF31C4FA3899B554F329D90D65A_gshared (Enumerator_tA2E47432D9BF33FCB9EAF5B6C3720221AACA50C1 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>>::get_Current()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184  Enumerator_get_Current_m003BC78F9886D5E4E98734CD70C3997E467EB1EA_gshared_inline (Enumerator_tF2D0EEBD6B861D9127DD6DC36B733FD42070858C * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_m6A3C20CEFE29B5B761E6C9DBA053A2CE897E2515_gshared (Enumerator_tF2D0EEBD6B861D9127DD6DC36B733FD42070858C * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.InternedString>::get_Current()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  Enumerator_get_Current_mB23665A6F8B7C1F38400C3F98386D8EF12ABD346_gshared_inline (Enumerator_tD1ECE103520161C1BD172331D6968D3D184B3B6F * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.InternedString>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_mC755E973A650FFFB74A2F3E332EC10C3F035F15D_gshared (Enumerator_tD1ECE103520161C1BD172331D6968D3D184B3B6F * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.NameAndParameters>::get_Current()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA  Enumerator_get_Current_m95785F9B18C857920D970C981C5DD0A8B51E8A77_gshared_inline (Enumerator_tA2E0F83E15EA46A2B4B08AFB750700C6E1387FC0 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.NameAndParameters>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_mB1DCA082AFD65B868138F31F1604376EB2580343_gshared (Enumerator_tA2E0F83E15EA46A2B4B08AFB750700C6E1387FC0 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.NamedValue>::get_Current()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077  Enumerator_get_Current_m16A2059A468713BFAF3492C3A7226F816CA64F7A_gshared_inline (Enumerator_tDEE846E92D2B22781FB09541CB1600E5398D3A20 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.NamedValue>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_m90AD9DC23572A512C0626351F44778A476DE51B0_gshared (Enumerator_tDEE846E92D2B22781FB09541CB1600E5398D3A20 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.UIElements.StyleSelectorPart>::get_Current()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR StyleSelectorPart_t707EDC970FC0F3E91E56DCBC178672A120426D54  Enumerator_get_Current_mEE518BAF9575D30AF8E5D39A599B90BD6B796AC5_gshared_inline (Enumerator_tC861559498255F6677610CED32D2161012D8D897 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.UIElements.StyleSelectorPart>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_mC5D34E64D92EABA8FE6CD4D97FF07A35F7F0DF25_gshared (Enumerator_tC861559498255F6677610CED32D2161012D8D897 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.Substring>::get_Current()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F  Enumerator_get_Current_mF0F69D342F9662149C2D0051C758AC18AEBFFB23_gshared_inline (Enumerator_t7C514ACB3FF8E3EFB210F5F45C9F18A715BB4F80 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.Substring>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_mC5829956B29271280A19B86852D365AA6497AB41_gshared (Enumerator_t7C514ACB3FF8E3EFB210F5F45C9F18A715BB4F80 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>::get_Current()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB  Enumerator_get_Current_m51D04AB64A27E7E8D833DAE19D8D7C342794D521_gshared_inline (Enumerator_tF2D95CC18A66F3DE682EAC9B88E865750DFAEF60 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_m084C0187069AC47C975802B0EB3F7257DB6F8507_gshared (Enumerator_tF2D95CC18A66F3DE682EAC9B88E865750DFAEF60 * __this, const RuntimeMethod* method);

// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Debug::LogException(System.Exception)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_LogException_m1BE957624F4DD291B1B4265D4A55A34EFAA8D7BA (Exception_t * ___exception0, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
inline RuntimeObject * Enumerator_get_Current_m4C91D0E84532DF10C654917487D82CB0AB27693B_inline (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * __this, const RuntimeMethod* method)
{
	return ((  RuntimeObject * (*) (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *, const RuntimeMethod*))Enumerator_get_Current_m4C91D0E84532DF10C654917487D82CB0AB27693B_gshared_inline)(__this, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
inline bool Enumerator_MoveNext_m2E56233762839CE55C67E00AC8DD3D4D3F6C0DF0 (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *, const RuntimeMethod*))Enumerator_MoveNext_m2E56233762839CE55C67E00AC8DD3D4D3F6C0DF0_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Layouts.InputControlLayout/ControlItem>::get_Current()
inline ControlItem_t88696CA6FA7E3E6EDBF31BD5A8994D959F583B15  Enumerator_get_Current_m98F1CC86098DFE6B87C86B2C5333FDAAAE66AF69_inline (Enumerator_tA2E47432D9BF33FCB9EAF5B6C3720221AACA50C1 * __this, const RuntimeMethod* method)
{
	return ((  ControlItem_t88696CA6FA7E3E6EDBF31BD5A8994D959F583B15  (*) (Enumerator_tA2E47432D9BF33FCB9EAF5B6C3720221AACA50C1 *, const RuntimeMethod*))Enumerator_get_Current_m98F1CC86098DFE6B87C86B2C5333FDAAAE66AF69_gshared_inline)(__this, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Layouts.InputControlLayout/ControlItem>::MoveNext()
inline bool Enumerator_MoveNext_mCB47A9BC10861CF31C4FA3899B554F329D90D65A (Enumerator_tA2E47432D9BF33FCB9EAF5B6C3720221AACA50C1 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_tA2E47432D9BF33FCB9EAF5B6C3720221AACA50C1 *, const RuntimeMethod*))Enumerator_MoveNext_mCB47A9BC10861CF31C4FA3899B554F329D90D65A_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>>::get_Current()
inline KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184  Enumerator_get_Current_m003BC78F9886D5E4E98734CD70C3997E467EB1EA_inline (Enumerator_tF2D0EEBD6B861D9127DD6DC36B733FD42070858C * __this, const RuntimeMethod* method)
{
	return ((  KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184  (*) (Enumerator_tF2D0EEBD6B861D9127DD6DC36B733FD42070858C *, const RuntimeMethod*))Enumerator_get_Current_m003BC78F9886D5E4E98734CD70C3997E467EB1EA_gshared_inline)(__this, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>>::MoveNext()
inline bool Enumerator_MoveNext_m6A3C20CEFE29B5B761E6C9DBA053A2CE897E2515 (Enumerator_tF2D0EEBD6B861D9127DD6DC36B733FD42070858C * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_tF2D0EEBD6B861D9127DD6DC36B733FD42070858C *, const RuntimeMethod*))Enumerator_MoveNext_m6A3C20CEFE29B5B761E6C9DBA053A2CE897E2515_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.InternedString>::get_Current()
inline InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  Enumerator_get_Current_mB23665A6F8B7C1F38400C3F98386D8EF12ABD346_inline (Enumerator_tD1ECE103520161C1BD172331D6968D3D184B3B6F * __this, const RuntimeMethod* method)
{
	return ((  InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  (*) (Enumerator_tD1ECE103520161C1BD172331D6968D3D184B3B6F *, const RuntimeMethod*))Enumerator_get_Current_mB23665A6F8B7C1F38400C3F98386D8EF12ABD346_gshared_inline)(__this, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.InternedString>::MoveNext()
inline bool Enumerator_MoveNext_mC755E973A650FFFB74A2F3E332EC10C3F035F15D (Enumerator_tD1ECE103520161C1BD172331D6968D3D184B3B6F * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_tD1ECE103520161C1BD172331D6968D3D184B3B6F *, const RuntimeMethod*))Enumerator_MoveNext_mC755E973A650FFFB74A2F3E332EC10C3F035F15D_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.NameAndParameters>::get_Current()
inline NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA  Enumerator_get_Current_m95785F9B18C857920D970C981C5DD0A8B51E8A77_inline (Enumerator_tA2E0F83E15EA46A2B4B08AFB750700C6E1387FC0 * __this, const RuntimeMethod* method)
{
	return ((  NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA  (*) (Enumerator_tA2E0F83E15EA46A2B4B08AFB750700C6E1387FC0 *, const RuntimeMethod*))Enumerator_get_Current_m95785F9B18C857920D970C981C5DD0A8B51E8A77_gshared_inline)(__this, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.NameAndParameters>::MoveNext()
inline bool Enumerator_MoveNext_mB1DCA082AFD65B868138F31F1604376EB2580343 (Enumerator_tA2E0F83E15EA46A2B4B08AFB750700C6E1387FC0 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_tA2E0F83E15EA46A2B4B08AFB750700C6E1387FC0 *, const RuntimeMethod*))Enumerator_MoveNext_mB1DCA082AFD65B868138F31F1604376EB2580343_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.NamedValue>::get_Current()
inline NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077  Enumerator_get_Current_m16A2059A468713BFAF3492C3A7226F816CA64F7A_inline (Enumerator_tDEE846E92D2B22781FB09541CB1600E5398D3A20 * __this, const RuntimeMethod* method)
{
	return ((  NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077  (*) (Enumerator_tDEE846E92D2B22781FB09541CB1600E5398D3A20 *, const RuntimeMethod*))Enumerator_get_Current_m16A2059A468713BFAF3492C3A7226F816CA64F7A_gshared_inline)(__this, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.NamedValue>::MoveNext()
inline bool Enumerator_MoveNext_m90AD9DC23572A512C0626351F44778A476DE51B0 (Enumerator_tDEE846E92D2B22781FB09541CB1600E5398D3A20 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_tDEE846E92D2B22781FB09541CB1600E5398D3A20 *, const RuntimeMethod*))Enumerator_MoveNext_m90AD9DC23572A512C0626351F44778A476DE51B0_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.UIElements.StyleSelectorPart>::get_Current()
inline StyleSelectorPart_t707EDC970FC0F3E91E56DCBC178672A120426D54  Enumerator_get_Current_mEE518BAF9575D30AF8E5D39A599B90BD6B796AC5_inline (Enumerator_tC861559498255F6677610CED32D2161012D8D897 * __this, const RuntimeMethod* method)
{
	return ((  StyleSelectorPart_t707EDC970FC0F3E91E56DCBC178672A120426D54  (*) (Enumerator_tC861559498255F6677610CED32D2161012D8D897 *, const RuntimeMethod*))Enumerator_get_Current_mEE518BAF9575D30AF8E5D39A599B90BD6B796AC5_gshared_inline)(__this, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.UIElements.StyleSelectorPart>::MoveNext()
inline bool Enumerator_MoveNext_mC5D34E64D92EABA8FE6CD4D97FF07A35F7F0DF25 (Enumerator_tC861559498255F6677610CED32D2161012D8D897 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_tC861559498255F6677610CED32D2161012D8D897 *, const RuntimeMethod*))Enumerator_MoveNext_mC5D34E64D92EABA8FE6CD4D97FF07A35F7F0DF25_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.Substring>::get_Current()
inline Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F  Enumerator_get_Current_mF0F69D342F9662149C2D0051C758AC18AEBFFB23_inline (Enumerator_t7C514ACB3FF8E3EFB210F5F45C9F18A715BB4F80 * __this, const RuntimeMethod* method)
{
	return ((  Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F  (*) (Enumerator_t7C514ACB3FF8E3EFB210F5F45C9F18A715BB4F80 *, const RuntimeMethod*))Enumerator_get_Current_mF0F69D342F9662149C2D0051C758AC18AEBFFB23_gshared_inline)(__this, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.Substring>::MoveNext()
inline bool Enumerator_MoveNext_mC5829956B29271280A19B86852D365AA6497AB41 (Enumerator_t7C514ACB3FF8E3EFB210F5F45C9F18A715BB4F80 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_t7C514ACB3FF8E3EFB210F5F45C9F18A715BB4F80 *, const RuntimeMethod*))Enumerator_MoveNext_mC5829956B29271280A19B86852D365AA6497AB41_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>::get_Current()
inline JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB  Enumerator_get_Current_m51D04AB64A27E7E8D833DAE19D8D7C342794D521_inline (Enumerator_tF2D95CC18A66F3DE682EAC9B88E865750DFAEF60 * __this, const RuntimeMethod* method)
{
	return ((  JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB  (*) (Enumerator_tF2D95CC18A66F3DE682EAC9B88E865750DFAEF60 *, const RuntimeMethod*))Enumerator_get_Current_m51D04AB64A27E7E8D833DAE19D8D7C342794D521_gshared_inline)(__this, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>::MoveNext()
inline bool Enumerator_MoveNext_m084C0187069AC47C975802B0EB3F7257DB6F8507 (Enumerator_tF2D95CC18A66F3DE682EAC9B88E865750DFAEF60 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_tF2D95CC18A66F3DE682EAC9B88E865750DFAEF60 *, const RuntimeMethod*))Enumerator_MoveNext_m084C0187069AC47C975802B0EB3F7257DB6F8507_gshared)(__this, method);
}
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.InputSystem.Utilities.WhereObservable`1/Where<System.Object>::.ctor(UnityEngine.InputSystem.Utilities.WhereObservable`1<TValue>,System.IObserver`1<TValue>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Where__ctor_m96214A73DC6074CD241E1C2FA0FB52F1BFDC3F9A_gshared (Where_t5326AC9C3D27C847981552AE1DD31844DAB40D4D * __this, WhereObservable_1_t36D963808BB5D3A64EB26C52708AD22FCEAF8B3C * ___observable0, RuntimeObject* ___observer1, const RuntimeMethod* method)
{
	{
		// public Where(WhereObservable<TValue> observable, IObserver<TValue> observer)
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405((RuntimeObject *)__this, /*hidden argument*/NULL);
		// m_Observable = observable;
		WhereObservable_1_t36D963808BB5D3A64EB26C52708AD22FCEAF8B3C * L_0 = ___observable0;
		__this->set_m_Observable_0(L_0);
		// m_Observer = observer;
		RuntimeObject* L_1 = ___observer1;
		__this->set_m_Observer_1(L_1);
		// }
		return;
	}
}
// System.Void UnityEngine.InputSystem.Utilities.WhereObservable`1/Where<System.Object>::OnCompleted()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Where_OnCompleted_m4F102F046E54C75C6063F71958B4DC9799CCF4D7_gshared (Where_t5326AC9C3D27C847981552AE1DD31844DAB40D4D * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void UnityEngine.InputSystem.Utilities.WhereObservable`1/Where<System.Object>::OnError(System.Exception)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Where_OnError_mFC114D8179BE7224B85246C090614AEB49C739DF_gshared (Where_t5326AC9C3D27C847981552AE1DD31844DAB40D4D * __this, Exception_t * ___error0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// Debug.LogException(error);
		Exception_t * L_0 = ___error0;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_LogException_m1BE957624F4DD291B1B4265D4A55A34EFAA8D7BA((Exception_t *)L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void UnityEngine.InputSystem.Utilities.WhereObservable`1/Where<System.Object>::OnNext(TValue)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Where_OnNext_m0784FC3EB67B24788E2FFC49C39BB6F7D10895CA_gshared (Where_t5326AC9C3D27C847981552AE1DD31844DAB40D4D * __this, RuntimeObject * ___evt0, const RuntimeMethod* method)
{
	{
		// if (m_Observable.m_Predicate(evt))
		WhereObservable_1_t36D963808BB5D3A64EB26C52708AD22FCEAF8B3C * L_0 = (WhereObservable_1_t36D963808BB5D3A64EB26C52708AD22FCEAF8B3C *)__this->get_m_Observable_0();
		NullCheck(L_0);
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_1 = (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_0->get_m_Predicate_1();
		RuntimeObject * L_2 = ___evt0;
		NullCheck((Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_1);
		bool L_3;
		L_3 = ((  bool (*) (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_1, (RuntimeObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		if (!L_3)
		{
			goto IL_001f;
		}
	}
	{
		// m_Observer.OnNext(evt);
		RuntimeObject* L_4 = (RuntimeObject*)__this->get_m_Observer_1();
		RuntimeObject * L_5 = ___evt0;
		NullCheck((RuntimeObject*)L_4);
		InterfaceActionInvoker1< RuntimeObject * >::Invoke(0 /* System.Void System.IObserver`1<System.Object>::OnNext(!0) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 1), (RuntimeObject*)L_4, (RuntimeObject *)L_5);
	}

IL_001f:
	{
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereArrayIterator`1<System.Object>::.ctor(TSource[],System.Func`2<TSource,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereArrayIterator_1__ctor_mF414A72FECC73997DCDF19A7AA6270F0F5522A75_gshared (WhereArrayIterator_1_t7D84D638EB94F5CC3BE1B29D8FC781CA8CD15A86 * __this, ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ___source0, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate1, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TSource> System.Linq.Enumerable/WhereArrayIterator`1<System.Object>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 * WhereArrayIterator_1_Clone_m3A3D1C1E3B210B1C7A4B04670BFE91427026321D_gshared (WhereArrayIterator_1_t7D84D638EB94F5CC3BE1B29D8FC781CA8CD15A86 * __this, const RuntimeMethod* method)
{
	{
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_0 = (ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)__this->get_source_3();
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_1 = (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)__this->get_predicate_4();
		WhereArrayIterator_1_t7D84D638EB94F5CC3BE1B29D8FC781CA8CD15A86 * L_2 = (WhereArrayIterator_1_t7D84D638EB94F5CC3BE1B29D8FC781CA8CD15A86 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereArrayIterator_1_t7D84D638EB94F5CC3BE1B29D8FC781CA8CD15A86 *, ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_2, (ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)L_0, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)L_2;
	}
}
// System.Boolean System.Linq.Enumerable/WhereArrayIterator`1<System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereArrayIterator_1_MoveNext_m37A95072CA5380DE7F2D6B57990507C92F045BB3_gshared (WhereArrayIterator_1_t7D84D638EB94F5CC3BE1B29D8FC781CA8CD15A86 * __this, const RuntimeMethod* method)
{
	RuntimeObject * V_0 = NULL;
	{
		int32_t L_0 = (int32_t)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->get_state_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_0058;
		}
	}
	{
		goto IL_0042;
	}

IL_000b:
	{
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_1 = (ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)__this->get_source_3();
		int32_t L_2 = (int32_t)__this->get_index_5();
		NullCheck(L_1);
		int32_t L_3 = L_2;
		RuntimeObject * L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		V_0 = (RuntimeObject *)L_4;
		int32_t L_5 = (int32_t)__this->get_index_5();
		__this->set_index_5(((int32_t)il2cpp_codegen_add((int32_t)L_5, (int32_t)1)));
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_6 = (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)__this->get_predicate_4();
		RuntimeObject * L_7 = V_0;
		NullCheck((Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_6);
		bool L_8;
		L_8 = ((  bool (*) (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_6, (RuntimeObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		if (!L_8)
		{
			goto IL_0042;
		}
	}
	{
		RuntimeObject * L_9 = V_0;
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_current_2(L_9);
		return (bool)1;
	}

IL_0042:
	{
		int32_t L_10 = (int32_t)__this->get_index_5();
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_11 = (ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)__this->get_source_3();
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_11)->max_length))))))
		{
			goto IL_000b;
		}
	}
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		VirtualActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Object>::Dispose() */, (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
	}

IL_0058:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereArrayIterator`1<System.Object>::Where(System.Func`2<TSource,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereArrayIterator_1_Where_mE5E8D6ADDCEDE96B370E65753F39F8A0C0CEDBDD_gshared (WhereArrayIterator_1_t7D84D638EB94F5CC3BE1B29D8FC781CA8CD15A86 * __this, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate0, const RuntimeMethod* method)
{
	{
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_0 = (ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)__this->get_source_3();
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_1 = (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)__this->get_predicate_4();
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_2 = ___predicate0;
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_3;
		L_3 = ((  Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * (*) (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_1, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		WhereArrayIterator_1_t7D84D638EB94F5CC3BE1B29D8FC781CA8CD15A86 * L_4 = (WhereArrayIterator_1_t7D84D638EB94F5CC3BE1B29D8FC781CA8CD15A86 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereArrayIterator_1_t7D84D638EB94F5CC3BE1B29D8FC781CA8CD15A86 *, ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_4, (ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)L_0, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (RuntimeObject*)L_4;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereArrayIterator`1<UnityEngine.InputSystem.Layouts.InputControlLayout/ControlItem>::.ctor(TSource[],System.Func`2<TSource,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereArrayIterator_1__ctor_mFF8DF159767B6761C67EE69C84BA6881FE54F669_gshared (WhereArrayIterator_1_t27BA96004BE53807B1028D1D8BFA586A91E96030 * __this, ControlItemU5BU5D_t534859CA6A52E2EC9DDDCA52FE280E8D5CD598DB* ___source0, Func_2_t6D3B67831BA6BBB7266E343D7EAEFF60C17F92DF * ___predicate1, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t2711EA98CD5C8E6E87A241C66556EFDB99842473 *)__this);
		((  void (*) (Iterator_1_t2711EA98CD5C8E6E87A241C66556EFDB99842473 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t2711EA98CD5C8E6E87A241C66556EFDB99842473 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		ControlItemU5BU5D_t534859CA6A52E2EC9DDDCA52FE280E8D5CD598DB* L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t6D3B67831BA6BBB7266E343D7EAEFF60C17F92DF * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TSource> System.Linq.Enumerable/WhereArrayIterator`1<UnityEngine.InputSystem.Layouts.InputControlLayout/ControlItem>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t2711EA98CD5C8E6E87A241C66556EFDB99842473 * WhereArrayIterator_1_Clone_mBFC9ADDAEF05FCA3195B20116867AF08EE0B3F65_gshared (WhereArrayIterator_1_t27BA96004BE53807B1028D1D8BFA586A91E96030 * __this, const RuntimeMethod* method)
{
	{
		ControlItemU5BU5D_t534859CA6A52E2EC9DDDCA52FE280E8D5CD598DB* L_0 = (ControlItemU5BU5D_t534859CA6A52E2EC9DDDCA52FE280E8D5CD598DB*)__this->get_source_3();
		Func_2_t6D3B67831BA6BBB7266E343D7EAEFF60C17F92DF * L_1 = (Func_2_t6D3B67831BA6BBB7266E343D7EAEFF60C17F92DF *)__this->get_predicate_4();
		WhereArrayIterator_1_t27BA96004BE53807B1028D1D8BFA586A91E96030 * L_2 = (WhereArrayIterator_1_t27BA96004BE53807B1028D1D8BFA586A91E96030 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereArrayIterator_1_t27BA96004BE53807B1028D1D8BFA586A91E96030 *, ControlItemU5BU5D_t534859CA6A52E2EC9DDDCA52FE280E8D5CD598DB*, Func_2_t6D3B67831BA6BBB7266E343D7EAEFF60C17F92DF *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_2, (ControlItemU5BU5D_t534859CA6A52E2EC9DDDCA52FE280E8D5CD598DB*)L_0, (Func_2_t6D3B67831BA6BBB7266E343D7EAEFF60C17F92DF *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t2711EA98CD5C8E6E87A241C66556EFDB99842473 *)L_2;
	}
}
// System.Boolean System.Linq.Enumerable/WhereArrayIterator`1<UnityEngine.InputSystem.Layouts.InputControlLayout/ControlItem>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereArrayIterator_1_MoveNext_m41C083BCED6892313B5B5B38888E8249A938E79A_gshared (WhereArrayIterator_1_t27BA96004BE53807B1028D1D8BFA586A91E96030 * __this, const RuntimeMethod* method)
{
	ControlItem_t88696CA6FA7E3E6EDBF31BD5A8994D959F583B15  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t2711EA98CD5C8E6E87A241C66556EFDB99842473 *)__this)->get_state_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_0058;
		}
	}
	{
		goto IL_0042;
	}

IL_000b:
	{
		ControlItemU5BU5D_t534859CA6A52E2EC9DDDCA52FE280E8D5CD598DB* L_1 = (ControlItemU5BU5D_t534859CA6A52E2EC9DDDCA52FE280E8D5CD598DB*)__this->get_source_3();
		int32_t L_2 = (int32_t)__this->get_index_5();
		NullCheck(L_1);
		int32_t L_3 = L_2;
		ControlItem_t88696CA6FA7E3E6EDBF31BD5A8994D959F583B15  L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		V_0 = (ControlItem_t88696CA6FA7E3E6EDBF31BD5A8994D959F583B15 )L_4;
		int32_t L_5 = (int32_t)__this->get_index_5();
		__this->set_index_5(((int32_t)il2cpp_codegen_add((int32_t)L_5, (int32_t)1)));
		Func_2_t6D3B67831BA6BBB7266E343D7EAEFF60C17F92DF * L_6 = (Func_2_t6D3B67831BA6BBB7266E343D7EAEFF60C17F92DF *)__this->get_predicate_4();
		ControlItem_t88696CA6FA7E3E6EDBF31BD5A8994D959F583B15  L_7 = V_0;
		NullCheck((Func_2_t6D3B67831BA6BBB7266E343D7EAEFF60C17F92DF *)L_6);
		bool L_8;
		L_8 = ((  bool (*) (Func_2_t6D3B67831BA6BBB7266E343D7EAEFF60C17F92DF *, ControlItem_t88696CA6FA7E3E6EDBF31BD5A8994D959F583B15 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Func_2_t6D3B67831BA6BBB7266E343D7EAEFF60C17F92DF *)L_6, (ControlItem_t88696CA6FA7E3E6EDBF31BD5A8994D959F583B15 )L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		if (!L_8)
		{
			goto IL_0042;
		}
	}
	{
		ControlItem_t88696CA6FA7E3E6EDBF31BD5A8994D959F583B15  L_9 = V_0;
		((Iterator_1_t2711EA98CD5C8E6E87A241C66556EFDB99842473 *)__this)->set_current_2(L_9);
		return (bool)1;
	}

IL_0042:
	{
		int32_t L_10 = (int32_t)__this->get_index_5();
		ControlItemU5BU5D_t534859CA6A52E2EC9DDDCA52FE280E8D5CD598DB* L_11 = (ControlItemU5BU5D_t534859CA6A52E2EC9DDDCA52FE280E8D5CD598DB*)__this->get_source_3();
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_11)->max_length))))))
		{
			goto IL_000b;
		}
	}
	{
		NullCheck((Iterator_1_t2711EA98CD5C8E6E87A241C66556EFDB99842473 *)__this);
		VirtualActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<UnityEngine.InputSystem.Layouts.InputControlLayout/ControlItem>::Dispose() */, (Iterator_1_t2711EA98CD5C8E6E87A241C66556EFDB99842473 *)__this);
	}

IL_0058:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereArrayIterator`1<UnityEngine.InputSystem.Layouts.InputControlLayout/ControlItem>::Where(System.Func`2<TSource,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereArrayIterator_1_Where_mED79AF0DA46055CD07B016BEF877FC6C52ABAF8D_gshared (WhereArrayIterator_1_t27BA96004BE53807B1028D1D8BFA586A91E96030 * __this, Func_2_t6D3B67831BA6BBB7266E343D7EAEFF60C17F92DF * ___predicate0, const RuntimeMethod* method)
{
	{
		ControlItemU5BU5D_t534859CA6A52E2EC9DDDCA52FE280E8D5CD598DB* L_0 = (ControlItemU5BU5D_t534859CA6A52E2EC9DDDCA52FE280E8D5CD598DB*)__this->get_source_3();
		Func_2_t6D3B67831BA6BBB7266E343D7EAEFF60C17F92DF * L_1 = (Func_2_t6D3B67831BA6BBB7266E343D7EAEFF60C17F92DF *)__this->get_predicate_4();
		Func_2_t6D3B67831BA6BBB7266E343D7EAEFF60C17F92DF * L_2 = ___predicate0;
		Func_2_t6D3B67831BA6BBB7266E343D7EAEFF60C17F92DF * L_3;
		L_3 = ((  Func_2_t6D3B67831BA6BBB7266E343D7EAEFF60C17F92DF * (*) (Func_2_t6D3B67831BA6BBB7266E343D7EAEFF60C17F92DF *, Func_2_t6D3B67831BA6BBB7266E343D7EAEFF60C17F92DF *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t6D3B67831BA6BBB7266E343D7EAEFF60C17F92DF *)L_1, (Func_2_t6D3B67831BA6BBB7266E343D7EAEFF60C17F92DF *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		WhereArrayIterator_1_t27BA96004BE53807B1028D1D8BFA586A91E96030 * L_4 = (WhereArrayIterator_1_t27BA96004BE53807B1028D1D8BFA586A91E96030 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereArrayIterator_1_t27BA96004BE53807B1028D1D8BFA586A91E96030 *, ControlItemU5BU5D_t534859CA6A52E2EC9DDDCA52FE280E8D5CD598DB*, Func_2_t6D3B67831BA6BBB7266E343D7EAEFF60C17F92DF *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_4, (ControlItemU5BU5D_t534859CA6A52E2EC9DDDCA52FE280E8D5CD598DB*)L_0, (Func_2_t6D3B67831BA6BBB7266E343D7EAEFF60C17F92DF *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (RuntimeObject*)L_4;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereEnumerableIterator`1<UnityEngine.InputSystem.Utilities.InternedString>::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereEnumerableIterator_1__ctor_m5F50F9D94650403C46478960730798EEBC1532D5_gshared (WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B * __this, RuntimeObject* ___source0, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * ___predicate1, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		((  void (*) (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		RuntimeObject* L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TSource> System.Linq.Enumerable/WhereEnumerableIterator`1<UnityEngine.InputSystem.Utilities.InternedString>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 * WhereEnumerableIterator_1_Clone_m95303B9A5C9A7F4A042121475E26FE8311FC6F62_gshared (WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_source_3();
		Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * L_1 = (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)__this->get_predicate_4();
		WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B * L_2 = (WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B *, RuntimeObject*, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_2, (RuntimeObject*)L_0, (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)L_2;
	}
}
// System.Void System.Linq.Enumerable/WhereEnumerableIterator`1<UnityEngine.InputSystem.Utilities.InternedString>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereEnumerableIterator_1_Dispose_mA40F5A988993D9164CE725BAB56475328E7A1667_gshared (WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_enumerator_5();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		RuntimeObject* L_1 = (RuntimeObject*)__this->get_enumerator_5();
		NullCheck((RuntimeObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, (RuntimeObject*)L_1);
	}

IL_0013:
	{
		__this->set_enumerator_5((RuntimeObject*)NULL);
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		((  void (*) (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		return;
	}
}
// System.Boolean System.Linq.Enumerable/WhereEnumerableIterator`1<UnityEngine.InputSystem.Utilities.InternedString>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereEnumerableIterator_1_MoveNext_mFC5FA5DD09FE51B5807F74A15AF81C22F4139784_gshared (WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_004e;
		}
	}
	{
		goto IL_0061;
	}

IL_0011:
	{
		RuntimeObject* L_3 = (RuntimeObject*)__this->get_source_3();
		NullCheck((RuntimeObject*)L_3);
		RuntimeObject* L_4;
		L_4 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<UnityEngine.InputSystem.Utilities.InternedString>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 5), (RuntimeObject*)L_3);
		__this->set_enumerator_5(L_4);
		((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->set_state_1(2);
		goto IL_004e;
	}

IL_002b:
	{
		RuntimeObject* L_5 = (RuntimeObject*)__this->get_enumerator_5();
		NullCheck((RuntimeObject*)L_5);
		InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  L_6;
		L_6 = InterfaceFuncInvoker0< InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<UnityEngine.InputSystem.Utilities.InternedString>::get_Current() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 6), (RuntimeObject*)L_5);
		V_1 = (InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED )L_6;
		Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * L_7 = (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)__this->get_predicate_4();
		InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  L_8 = V_1;
		NullCheck((Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)L_7);
		bool L_9;
		L_9 = ((  bool (*) (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *, InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)L_7, (InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED )L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		if (!L_9)
		{
			goto IL_004e;
		}
	}
	{
		InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  L_10 = V_1;
		((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->set_current_2(L_10);
		return (bool)1;
	}

IL_004e:
	{
		RuntimeObject* L_11 = (RuntimeObject*)__this->get_enumerator_5();
		NullCheck((RuntimeObject*)L_11);
		bool L_12;
		L_12 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, (RuntimeObject*)L_11);
		if (L_12)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		VirtualActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<UnityEngine.InputSystem.Utilities.InternedString>::Dispose() */, (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
	}

IL_0061:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereEnumerableIterator`1<UnityEngine.InputSystem.Utilities.InternedString>::Where(System.Func`2<TSource,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereEnumerableIterator_1_Where_mE751DD2AFAA0B60F3A8E9F73A65BF1DB402BE527_gshared (WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B * __this, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * ___predicate0, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_source_3();
		Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * L_1 = (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)__this->get_predicate_4();
		Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * L_2 = ___predicate0;
		Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * L_3;
		L_3 = ((  Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * (*) (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)L_1, (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B * L_4 = (WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B *, RuntimeObject*, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_4, (RuntimeObject*)L_0, (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (RuntimeObject*)L_4;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereEnumerableIterator`1<System.Object>::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereEnumerableIterator_1__ctor_m594DCC37C543ABDFD0B4E28564ADF6B885D7BD5C_gshared (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 * __this, RuntimeObject* ___source0, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate1, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		RuntimeObject* L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TSource> System.Linq.Enumerable/WhereEnumerableIterator`1<System.Object>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 * WhereEnumerableIterator_1_Clone_m9B2D91B445245AE2614D777AE33792AE230C7986_gshared (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_source_3();
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_1 = (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)__this->get_predicate_4();
		WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 * L_2 = (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *, RuntimeObject*, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_2, (RuntimeObject*)L_0, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)L_2;
	}
}
// System.Void System.Linq.Enumerable/WhereEnumerableIterator`1<System.Object>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereEnumerableIterator_1_Dispose_m4E1339513102BB6B49AD33EDB569D3FFD24ED023_gshared (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_enumerator_5();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		RuntimeObject* L_1 = (RuntimeObject*)__this->get_enumerator_5();
		NullCheck((RuntimeObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, (RuntimeObject*)L_1);
	}

IL_0013:
	{
		__this->set_enumerator_5((RuntimeObject*)NULL);
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		return;
	}
}
// System.Boolean System.Linq.Enumerable/WhereEnumerableIterator`1<System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereEnumerableIterator_1_MoveNext_m6D8A420AEB325BF252721010781EF31CF64D73FF_gshared (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	RuntimeObject * V_1 = NULL;
	{
		int32_t L_0 = (int32_t)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_004e;
		}
	}
	{
		goto IL_0061;
	}

IL_0011:
	{
		RuntimeObject* L_3 = (RuntimeObject*)__this->get_source_3();
		NullCheck((RuntimeObject*)L_3);
		RuntimeObject* L_4;
		L_4 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 5), (RuntimeObject*)L_3);
		__this->set_enumerator_5(L_4);
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_state_1(2);
		goto IL_004e;
	}

IL_002b:
	{
		RuntimeObject* L_5 = (RuntimeObject*)__this->get_enumerator_5();
		NullCheck((RuntimeObject*)L_5);
		RuntimeObject * L_6;
		L_6 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 6), (RuntimeObject*)L_5);
		V_1 = (RuntimeObject *)L_6;
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_7 = (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)__this->get_predicate_4();
		RuntimeObject * L_8 = V_1;
		NullCheck((Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_7);
		bool L_9;
		L_9 = ((  bool (*) (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_7, (RuntimeObject *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		if (!L_9)
		{
			goto IL_004e;
		}
	}
	{
		RuntimeObject * L_10 = V_1;
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_current_2(L_10);
		return (bool)1;
	}

IL_004e:
	{
		RuntimeObject* L_11 = (RuntimeObject*)__this->get_enumerator_5();
		NullCheck((RuntimeObject*)L_11);
		bool L_12;
		L_12 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, (RuntimeObject*)L_11);
		if (L_12)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		VirtualActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Object>::Dispose() */, (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
	}

IL_0061:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereEnumerableIterator`1<System.Object>::Where(System.Func`2<TSource,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereEnumerableIterator_1_Where_m53573212F7876F6BD2C970D8D31B378A76928584_gshared (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 * __this, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate0, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_source_3();
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_1 = (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)__this->get_predicate_4();
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_2 = ___predicate0;
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_3;
		L_3 = ((  Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * (*) (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_1, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 * L_4 = (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *, RuntimeObject*, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_4, (RuntimeObject*)L_0, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (RuntimeObject*)L_4;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereEnumerableIterator`1<UnityEngine.InputSystem.Layouts.InputControlLayout/ControlItem>::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereEnumerableIterator_1__ctor_mD3294AA8A80D4184B09EA08BFB45F317B8FBDE2C_gshared (WhereEnumerableIterator_1_t9A9C4F1C2DF243CCF60AE823D966E68DD70188D0 * __this, RuntimeObject* ___source0, Func_2_t6D3B67831BA6BBB7266E343D7EAEFF60C17F92DF * ___predicate1, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t2711EA98CD5C8E6E87A241C66556EFDB99842473 *)__this);
		((  void (*) (Iterator_1_t2711EA98CD5C8E6E87A241C66556EFDB99842473 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t2711EA98CD5C8E6E87A241C66556EFDB99842473 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		RuntimeObject* L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t6D3B67831BA6BBB7266E343D7EAEFF60C17F92DF * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TSource> System.Linq.Enumerable/WhereEnumerableIterator`1<UnityEngine.InputSystem.Layouts.InputControlLayout/ControlItem>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t2711EA98CD5C8E6E87A241C66556EFDB99842473 * WhereEnumerableIterator_1_Clone_m7B418E3EFB752E7C9E3A42AF7434909B9943F2E1_gshared (WhereEnumerableIterator_1_t9A9C4F1C2DF243CCF60AE823D966E68DD70188D0 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_source_3();
		Func_2_t6D3B67831BA6BBB7266E343D7EAEFF60C17F92DF * L_1 = (Func_2_t6D3B67831BA6BBB7266E343D7EAEFF60C17F92DF *)__this->get_predicate_4();
		WhereEnumerableIterator_1_t9A9C4F1C2DF243CCF60AE823D966E68DD70188D0 * L_2 = (WhereEnumerableIterator_1_t9A9C4F1C2DF243CCF60AE823D966E68DD70188D0 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereEnumerableIterator_1_t9A9C4F1C2DF243CCF60AE823D966E68DD70188D0 *, RuntimeObject*, Func_2_t6D3B67831BA6BBB7266E343D7EAEFF60C17F92DF *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_2, (RuntimeObject*)L_0, (Func_2_t6D3B67831BA6BBB7266E343D7EAEFF60C17F92DF *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t2711EA98CD5C8E6E87A241C66556EFDB99842473 *)L_2;
	}
}
// System.Void System.Linq.Enumerable/WhereEnumerableIterator`1<UnityEngine.InputSystem.Layouts.InputControlLayout/ControlItem>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereEnumerableIterator_1_Dispose_mD1328E49F5430F691287B7B89F9975BB0CB1E5B5_gshared (WhereEnumerableIterator_1_t9A9C4F1C2DF243CCF60AE823D966E68DD70188D0 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_enumerator_5();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		RuntimeObject* L_1 = (RuntimeObject*)__this->get_enumerator_5();
		NullCheck((RuntimeObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, (RuntimeObject*)L_1);
	}

IL_0013:
	{
		__this->set_enumerator_5((RuntimeObject*)NULL);
		NullCheck((Iterator_1_t2711EA98CD5C8E6E87A241C66556EFDB99842473 *)__this);
		((  void (*) (Iterator_1_t2711EA98CD5C8E6E87A241C66556EFDB99842473 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Iterator_1_t2711EA98CD5C8E6E87A241C66556EFDB99842473 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		return;
	}
}
// System.Boolean System.Linq.Enumerable/WhereEnumerableIterator`1<UnityEngine.InputSystem.Layouts.InputControlLayout/ControlItem>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereEnumerableIterator_1_MoveNext_mFB56964239F3E18416CEE065ADD1979E58A51E6C_gshared (WhereEnumerableIterator_1_t9A9C4F1C2DF243CCF60AE823D966E68DD70188D0 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	ControlItem_t88696CA6FA7E3E6EDBF31BD5A8994D959F583B15  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t2711EA98CD5C8E6E87A241C66556EFDB99842473 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_004e;
		}
	}
	{
		goto IL_0061;
	}

IL_0011:
	{
		RuntimeObject* L_3 = (RuntimeObject*)__this->get_source_3();
		NullCheck((RuntimeObject*)L_3);
		RuntimeObject* L_4;
		L_4 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<UnityEngine.InputSystem.Layouts.InputControlLayout/ControlItem>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 5), (RuntimeObject*)L_3);
		__this->set_enumerator_5(L_4);
		((Iterator_1_t2711EA98CD5C8E6E87A241C66556EFDB99842473 *)__this)->set_state_1(2);
		goto IL_004e;
	}

IL_002b:
	{
		RuntimeObject* L_5 = (RuntimeObject*)__this->get_enumerator_5();
		NullCheck((RuntimeObject*)L_5);
		ControlItem_t88696CA6FA7E3E6EDBF31BD5A8994D959F583B15  L_6;
		L_6 = InterfaceFuncInvoker0< ControlItem_t88696CA6FA7E3E6EDBF31BD5A8994D959F583B15  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<UnityEngine.InputSystem.Layouts.InputControlLayout/ControlItem>::get_Current() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 6), (RuntimeObject*)L_5);
		V_1 = (ControlItem_t88696CA6FA7E3E6EDBF31BD5A8994D959F583B15 )L_6;
		Func_2_t6D3B67831BA6BBB7266E343D7EAEFF60C17F92DF * L_7 = (Func_2_t6D3B67831BA6BBB7266E343D7EAEFF60C17F92DF *)__this->get_predicate_4();
		ControlItem_t88696CA6FA7E3E6EDBF31BD5A8994D959F583B15  L_8 = V_1;
		NullCheck((Func_2_t6D3B67831BA6BBB7266E343D7EAEFF60C17F92DF *)L_7);
		bool L_9;
		L_9 = ((  bool (*) (Func_2_t6D3B67831BA6BBB7266E343D7EAEFF60C17F92DF *, ControlItem_t88696CA6FA7E3E6EDBF31BD5A8994D959F583B15 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t6D3B67831BA6BBB7266E343D7EAEFF60C17F92DF *)L_7, (ControlItem_t88696CA6FA7E3E6EDBF31BD5A8994D959F583B15 )L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		if (!L_9)
		{
			goto IL_004e;
		}
	}
	{
		ControlItem_t88696CA6FA7E3E6EDBF31BD5A8994D959F583B15  L_10 = V_1;
		((Iterator_1_t2711EA98CD5C8E6E87A241C66556EFDB99842473 *)__this)->set_current_2(L_10);
		return (bool)1;
	}

IL_004e:
	{
		RuntimeObject* L_11 = (RuntimeObject*)__this->get_enumerator_5();
		NullCheck((RuntimeObject*)L_11);
		bool L_12;
		L_12 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, (RuntimeObject*)L_11);
		if (L_12)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t2711EA98CD5C8E6E87A241C66556EFDB99842473 *)__this);
		VirtualActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<UnityEngine.InputSystem.Layouts.InputControlLayout/ControlItem>::Dispose() */, (Iterator_1_t2711EA98CD5C8E6E87A241C66556EFDB99842473 *)__this);
	}

IL_0061:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereEnumerableIterator`1<UnityEngine.InputSystem.Layouts.InputControlLayout/ControlItem>::Where(System.Func`2<TSource,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereEnumerableIterator_1_Where_m815DF7A59ACAF95D5D08AC61D37344F47428785B_gshared (WhereEnumerableIterator_1_t9A9C4F1C2DF243CCF60AE823D966E68DD70188D0 * __this, Func_2_t6D3B67831BA6BBB7266E343D7EAEFF60C17F92DF * ___predicate0, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_source_3();
		Func_2_t6D3B67831BA6BBB7266E343D7EAEFF60C17F92DF * L_1 = (Func_2_t6D3B67831BA6BBB7266E343D7EAEFF60C17F92DF *)__this->get_predicate_4();
		Func_2_t6D3B67831BA6BBB7266E343D7EAEFF60C17F92DF * L_2 = ___predicate0;
		Func_2_t6D3B67831BA6BBB7266E343D7EAEFF60C17F92DF * L_3;
		L_3 = ((  Func_2_t6D3B67831BA6BBB7266E343D7EAEFF60C17F92DF * (*) (Func_2_t6D3B67831BA6BBB7266E343D7EAEFF60C17F92DF *, Func_2_t6D3B67831BA6BBB7266E343D7EAEFF60C17F92DF *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((Func_2_t6D3B67831BA6BBB7266E343D7EAEFF60C17F92DF *)L_1, (Func_2_t6D3B67831BA6BBB7266E343D7EAEFF60C17F92DF *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		WhereEnumerableIterator_1_t9A9C4F1C2DF243CCF60AE823D966E68DD70188D0 * L_4 = (WhereEnumerableIterator_1_t9A9C4F1C2DF243CCF60AE823D966E68DD70188D0 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereEnumerableIterator_1_t9A9C4F1C2DF243CCF60AE823D966E68DD70188D0 *, RuntimeObject*, Func_2_t6D3B67831BA6BBB7266E343D7EAEFF60C17F92DF *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_4, (RuntimeObject*)L_0, (Func_2_t6D3B67831BA6BBB7266E343D7EAEFF60C17F92DF *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (RuntimeObject*)L_4;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereListIterator`1<System.Object>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereListIterator_1__ctor_m91B13EE23652B507A1FB0FFC1A899D74B02BC2CC_gshared (WhereListIterator_1_t42618389DB998070E03A982D15FA39BCA1DB56BD * __this, List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * ___source0, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate1, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TSource> System.Linq.Enumerable/WhereListIterator`1<System.Object>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 * WhereListIterator_1_Clone_m8EE565E449EC7ED5A956B6A2B06EB01C953D25DE_gshared (WhereListIterator_1_t42618389DB998070E03A982D15FA39BCA1DB56BD * __this, const RuntimeMethod* method)
{
	{
		List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * L_0 = (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *)__this->get_source_3();
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_1 = (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)__this->get_predicate_4();
		WhereListIterator_1_t42618389DB998070E03A982D15FA39BCA1DB56BD * L_2 = (WhereListIterator_1_t42618389DB998070E03A982D15FA39BCA1DB56BD *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereListIterator_1_t42618389DB998070E03A982D15FA39BCA1DB56BD *, List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_2, (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *)L_0, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)L_2;
	}
}
// System.Boolean System.Linq.Enumerable/WhereListIterator`1<System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereListIterator_1_MoveNext_m11D0FD0206FC9B236608A1150FB26790BA09B2E5_gshared (WhereListIterator_1_t42618389DB998070E03A982D15FA39BCA1DB56BD * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	RuntimeObject * V_1 = NULL;
	{
		int32_t L_0 = (int32_t)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_004e;
		}
	}
	{
		goto IL_0061;
	}

IL_0011:
	{
		List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * L_3 = (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *)__this->get_source_3();
		NullCheck((List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *)L_3);
		Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  L_4;
		L_4 = ((  Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  (*) (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_5(L_4);
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_state_1(2);
		goto IL_004e;
	}

IL_002b:
	{
		Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * L_5 = (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)__this->get_address_of_enumerator_5();
		RuntimeObject * L_6;
		L_6 = Enumerator_get_Current_m4C91D0E84532DF10C654917487D82CB0AB27693B_inline((Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (RuntimeObject *)L_6;
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_7 = (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)__this->get_predicate_4();
		RuntimeObject * L_8 = V_1;
		NullCheck((Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_7);
		bool L_9;
		L_9 = ((  bool (*) (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_7, (RuntimeObject *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_9)
		{
			goto IL_004e;
		}
	}
	{
		RuntimeObject * L_10 = V_1;
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_current_2(L_10);
		return (bool)1;
	}

IL_004e:
	{
		Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * L_11 = (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)__this->get_address_of_enumerator_5();
		bool L_12;
		L_12 = Enumerator_MoveNext_m2E56233762839CE55C67E00AC8DD3D4D3F6C0DF0((Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		if (L_12)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		VirtualActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Object>::Dispose() */, (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
	}

IL_0061:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereListIterator`1<System.Object>::Where(System.Func`2<TSource,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereListIterator_1_Where_mDC96B7EB196AB4B26C70F6F471D1E9B813FF14A5_gshared (WhereListIterator_1_t42618389DB998070E03A982D15FA39BCA1DB56BD * __this, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate0, const RuntimeMethod* method)
{
	{
		List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * L_0 = (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *)__this->get_source_3();
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_1 = (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)__this->get_predicate_4();
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_2 = ___predicate0;
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_3;
		L_3 = ((  Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * (*) (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9)->methodPointer)((Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_1, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9));
		WhereListIterator_1_t42618389DB998070E03A982D15FA39BCA1DB56BD * L_4 = (WhereListIterator_1_t42618389DB998070E03A982D15FA39BCA1DB56BD *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereListIterator_1_t42618389DB998070E03A982D15FA39BCA1DB56BD *, List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_4, (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *)L_0, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (RuntimeObject*)L_4;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereListIterator`1<UnityEngine.InputSystem.Layouts.InputControlLayout/ControlItem>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereListIterator_1__ctor_m7A086FA1D6A90DBD0FD7B4996CE17C48A4DFD4AC_gshared (WhereListIterator_1_t578D3A8880FE8DFFE9A8B7462A4D3004DEDB1FD0 * __this, List_1_tB7B59A258A4A84FCB5A2CC59F934C8B230EA963F * ___source0, Func_2_t6D3B67831BA6BBB7266E343D7EAEFF60C17F92DF * ___predicate1, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t2711EA98CD5C8E6E87A241C66556EFDB99842473 *)__this);
		((  void (*) (Iterator_1_t2711EA98CD5C8E6E87A241C66556EFDB99842473 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t2711EA98CD5C8E6E87A241C66556EFDB99842473 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_tB7B59A258A4A84FCB5A2CC59F934C8B230EA963F * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t6D3B67831BA6BBB7266E343D7EAEFF60C17F92DF * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TSource> System.Linq.Enumerable/WhereListIterator`1<UnityEngine.InputSystem.Layouts.InputControlLayout/ControlItem>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t2711EA98CD5C8E6E87A241C66556EFDB99842473 * WhereListIterator_1_Clone_m3D21177C911E9AE9C0FBC049D3FBFEBD1E794633_gshared (WhereListIterator_1_t578D3A8880FE8DFFE9A8B7462A4D3004DEDB1FD0 * __this, const RuntimeMethod* method)
{
	{
		List_1_tB7B59A258A4A84FCB5A2CC59F934C8B230EA963F * L_0 = (List_1_tB7B59A258A4A84FCB5A2CC59F934C8B230EA963F *)__this->get_source_3();
		Func_2_t6D3B67831BA6BBB7266E343D7EAEFF60C17F92DF * L_1 = (Func_2_t6D3B67831BA6BBB7266E343D7EAEFF60C17F92DF *)__this->get_predicate_4();
		WhereListIterator_1_t578D3A8880FE8DFFE9A8B7462A4D3004DEDB1FD0 * L_2 = (WhereListIterator_1_t578D3A8880FE8DFFE9A8B7462A4D3004DEDB1FD0 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereListIterator_1_t578D3A8880FE8DFFE9A8B7462A4D3004DEDB1FD0 *, List_1_tB7B59A258A4A84FCB5A2CC59F934C8B230EA963F *, Func_2_t6D3B67831BA6BBB7266E343D7EAEFF60C17F92DF *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_2, (List_1_tB7B59A258A4A84FCB5A2CC59F934C8B230EA963F *)L_0, (Func_2_t6D3B67831BA6BBB7266E343D7EAEFF60C17F92DF *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t2711EA98CD5C8E6E87A241C66556EFDB99842473 *)L_2;
	}
}
// System.Boolean System.Linq.Enumerable/WhereListIterator`1<UnityEngine.InputSystem.Layouts.InputControlLayout/ControlItem>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereListIterator_1_MoveNext_mB41A55432824A2CA3929296D2BF4FCD3634A92CE_gshared (WhereListIterator_1_t578D3A8880FE8DFFE9A8B7462A4D3004DEDB1FD0 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	ControlItem_t88696CA6FA7E3E6EDBF31BD5A8994D959F583B15  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t2711EA98CD5C8E6E87A241C66556EFDB99842473 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_004e;
		}
	}
	{
		goto IL_0061;
	}

IL_0011:
	{
		List_1_tB7B59A258A4A84FCB5A2CC59F934C8B230EA963F * L_3 = (List_1_tB7B59A258A4A84FCB5A2CC59F934C8B230EA963F *)__this->get_source_3();
		NullCheck((List_1_tB7B59A258A4A84FCB5A2CC59F934C8B230EA963F *)L_3);
		Enumerator_tA2E47432D9BF33FCB9EAF5B6C3720221AACA50C1  L_4;
		L_4 = ((  Enumerator_tA2E47432D9BF33FCB9EAF5B6C3720221AACA50C1  (*) (List_1_tB7B59A258A4A84FCB5A2CC59F934C8B230EA963F *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_tB7B59A258A4A84FCB5A2CC59F934C8B230EA963F *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_5(L_4);
		((Iterator_1_t2711EA98CD5C8E6E87A241C66556EFDB99842473 *)__this)->set_state_1(2);
		goto IL_004e;
	}

IL_002b:
	{
		Enumerator_tA2E47432D9BF33FCB9EAF5B6C3720221AACA50C1 * L_5 = (Enumerator_tA2E47432D9BF33FCB9EAF5B6C3720221AACA50C1 *)__this->get_address_of_enumerator_5();
		ControlItem_t88696CA6FA7E3E6EDBF31BD5A8994D959F583B15  L_6;
		L_6 = Enumerator_get_Current_m98F1CC86098DFE6B87C86B2C5333FDAAAE66AF69_inline((Enumerator_tA2E47432D9BF33FCB9EAF5B6C3720221AACA50C1 *)(Enumerator_tA2E47432D9BF33FCB9EAF5B6C3720221AACA50C1 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (ControlItem_t88696CA6FA7E3E6EDBF31BD5A8994D959F583B15 )L_6;
		Func_2_t6D3B67831BA6BBB7266E343D7EAEFF60C17F92DF * L_7 = (Func_2_t6D3B67831BA6BBB7266E343D7EAEFF60C17F92DF *)__this->get_predicate_4();
		ControlItem_t88696CA6FA7E3E6EDBF31BD5A8994D959F583B15  L_8 = V_1;
		NullCheck((Func_2_t6D3B67831BA6BBB7266E343D7EAEFF60C17F92DF *)L_7);
		bool L_9;
		L_9 = ((  bool (*) (Func_2_t6D3B67831BA6BBB7266E343D7EAEFF60C17F92DF *, ControlItem_t88696CA6FA7E3E6EDBF31BD5A8994D959F583B15 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t6D3B67831BA6BBB7266E343D7EAEFF60C17F92DF *)L_7, (ControlItem_t88696CA6FA7E3E6EDBF31BD5A8994D959F583B15 )L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_9)
		{
			goto IL_004e;
		}
	}
	{
		ControlItem_t88696CA6FA7E3E6EDBF31BD5A8994D959F583B15  L_10 = V_1;
		((Iterator_1_t2711EA98CD5C8E6E87A241C66556EFDB99842473 *)__this)->set_current_2(L_10);
		return (bool)1;
	}

IL_004e:
	{
		Enumerator_tA2E47432D9BF33FCB9EAF5B6C3720221AACA50C1 * L_11 = (Enumerator_tA2E47432D9BF33FCB9EAF5B6C3720221AACA50C1 *)__this->get_address_of_enumerator_5();
		bool L_12;
		L_12 = Enumerator_MoveNext_mCB47A9BC10861CF31C4FA3899B554F329D90D65A((Enumerator_tA2E47432D9BF33FCB9EAF5B6C3720221AACA50C1 *)(Enumerator_tA2E47432D9BF33FCB9EAF5B6C3720221AACA50C1 *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		if (L_12)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t2711EA98CD5C8E6E87A241C66556EFDB99842473 *)__this);
		VirtualActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<UnityEngine.InputSystem.Layouts.InputControlLayout/ControlItem>::Dispose() */, (Iterator_1_t2711EA98CD5C8E6E87A241C66556EFDB99842473 *)__this);
	}

IL_0061:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereListIterator`1<UnityEngine.InputSystem.Layouts.InputControlLayout/ControlItem>::Where(System.Func`2<TSource,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereListIterator_1_Where_mE010910BCB00266309A4EA720B34A7FD5F0C4793_gshared (WhereListIterator_1_t578D3A8880FE8DFFE9A8B7462A4D3004DEDB1FD0 * __this, Func_2_t6D3B67831BA6BBB7266E343D7EAEFF60C17F92DF * ___predicate0, const RuntimeMethod* method)
{
	{
		List_1_tB7B59A258A4A84FCB5A2CC59F934C8B230EA963F * L_0 = (List_1_tB7B59A258A4A84FCB5A2CC59F934C8B230EA963F *)__this->get_source_3();
		Func_2_t6D3B67831BA6BBB7266E343D7EAEFF60C17F92DF * L_1 = (Func_2_t6D3B67831BA6BBB7266E343D7EAEFF60C17F92DF *)__this->get_predicate_4();
		Func_2_t6D3B67831BA6BBB7266E343D7EAEFF60C17F92DF * L_2 = ___predicate0;
		Func_2_t6D3B67831BA6BBB7266E343D7EAEFF60C17F92DF * L_3;
		L_3 = ((  Func_2_t6D3B67831BA6BBB7266E343D7EAEFF60C17F92DF * (*) (Func_2_t6D3B67831BA6BBB7266E343D7EAEFF60C17F92DF *, Func_2_t6D3B67831BA6BBB7266E343D7EAEFF60C17F92DF *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9)->methodPointer)((Func_2_t6D3B67831BA6BBB7266E343D7EAEFF60C17F92DF *)L_1, (Func_2_t6D3B67831BA6BBB7266E343D7EAEFF60C17F92DF *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9));
		WhereListIterator_1_t578D3A8880FE8DFFE9A8B7462A4D3004DEDB1FD0 * L_4 = (WhereListIterator_1_t578D3A8880FE8DFFE9A8B7462A4D3004DEDB1FD0 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereListIterator_1_t578D3A8880FE8DFFE9A8B7462A4D3004DEDB1FD0 *, List_1_tB7B59A258A4A84FCB5A2CC59F934C8B230EA963F *, Func_2_t6D3B67831BA6BBB7266E343D7EAEFF60C17F92DF *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_4, (List_1_tB7B59A258A4A84FCB5A2CC59F934C8B230EA963F *)L_0, (Func_2_t6D3B67831BA6BBB7266E343D7EAEFF60C17F92DF *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (RuntimeObject*)L_4;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.InputSystem.Utilities.WhereObservable`1<System.Object>::.ctor(System.IObservable`1<TValue>,System.Func`2<TValue,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereObservable_1__ctor_m62004F4F4A1FF36799925D48D340B3DCCD349889_gshared (WhereObservable_1_t36D963808BB5D3A64EB26C52708AD22FCEAF8B3C * __this, RuntimeObject* ___source0, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate1, const RuntimeMethod* method)
{
	{
		// public WhereObservable(IObservable<TValue> source, Func<TValue, bool> predicate)
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405((RuntimeObject *)__this, /*hidden argument*/NULL);
		// m_Source = source;
		RuntimeObject* L_0 = ___source0;
		__this->set_m_Source_0(L_0);
		// m_Predicate = predicate;
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_1 = ___predicate1;
		__this->set_m_Predicate_1(L_1);
		// }
		return;
	}
}
// System.IDisposable UnityEngine.InputSystem.Utilities.WhereObservable`1<System.Object>::Subscribe(System.IObserver`1<TValue>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereObservable_1_Subscribe_mF9FC7ECB6F1CF642D039ADF51E854EBE497A7F36_gshared (WhereObservable_1_t36D963808BB5D3A64EB26C52708AD22FCEAF8B3C * __this, RuntimeObject* ___observer0, const RuntimeMethod* method)
{
	{
		// return m_Source.Subscribe(new Where(this, observer));
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_m_Source_0();
		RuntimeObject* L_1 = ___observer0;
		Where_t5326AC9C3D27C847981552AE1DD31844DAB40D4D * L_2 = (Where_t5326AC9C3D27C847981552AE1DD31844DAB40D4D *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 0));
		((  void (*) (Where_t5326AC9C3D27C847981552AE1DD31844DAB40D4D *, WhereObservable_1_t36D963808BB5D3A64EB26C52708AD22FCEAF8B3C *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1)->methodPointer)(L_2, (WhereObservable_1_t36D963808BB5D3A64EB26C52708AD22FCEAF8B3C *)__this, (RuntimeObject*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1));
		NullCheck((RuntimeObject*)L_0);
		RuntimeObject* L_3;
		L_3 = InterfaceFuncInvoker1< RuntimeObject*, RuntimeObject* >::Invoke(0 /* System.IDisposable System.IObservable`1<System.Object>::Subscribe(System.IObserver`1<!0>) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2), (RuntimeObject*)L_0, (RuntimeObject*)L_2);
		return (RuntimeObject*)L_3;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectArrayIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,UnityEngine.InputSystem.Utilities.InternedString>::.ctor(TSource[],System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectArrayIterator_2__ctor_mE7F681FABF57600F66CD1215F0E3D156467328C2_gshared (WhereSelectArrayIterator_2_tB9FDF4ABA25271C5465121B8CD088BB04D7A03F9 * __this, KeyValuePair_2U5BU5D_tC4ED6A2024348F6B2CB712DC491F7E57AEF32630* ___source0, Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 * ___predicate1, Func_2_tB181F4C9A24C0634DF4A10CFBFFE6B45B3796BBC * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		((  void (*) (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		KeyValuePair_2U5BU5D_tC4ED6A2024348F6B2CB712DC491F7E57AEF32630* L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_tB181F4C9A24C0634DF4A10CFBFFE6B45B3796BBC * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectArrayIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,UnityEngine.InputSystem.Utilities.InternedString>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 * WhereSelectArrayIterator_2_Clone_m46E278C6AFF6460C0B36F55C3978D3B3230FD8F4_gshared (WhereSelectArrayIterator_2_tB9FDF4ABA25271C5465121B8CD088BB04D7A03F9 * __this, const RuntimeMethod* method)
{
	{
		KeyValuePair_2U5BU5D_tC4ED6A2024348F6B2CB712DC491F7E57AEF32630* L_0 = (KeyValuePair_2U5BU5D_tC4ED6A2024348F6B2CB712DC491F7E57AEF32630*)__this->get_source_3();
		Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 * L_1 = (Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 *)__this->get_predicate_4();
		Func_2_tB181F4C9A24C0634DF4A10CFBFFE6B45B3796BBC * L_2 = (Func_2_tB181F4C9A24C0634DF4A10CFBFFE6B45B3796BBC *)__this->get_selector_5();
		WhereSelectArrayIterator_2_tB9FDF4ABA25271C5465121B8CD088BB04D7A03F9 * L_3 = (WhereSelectArrayIterator_2_tB9FDF4ABA25271C5465121B8CD088BB04D7A03F9 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectArrayIterator_2_tB9FDF4ABA25271C5465121B8CD088BB04D7A03F9 *, KeyValuePair_2U5BU5D_tC4ED6A2024348F6B2CB712DC491F7E57AEF32630*, Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 *, Func_2_tB181F4C9A24C0634DF4A10CFBFFE6B45B3796BBC *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (KeyValuePair_2U5BU5D_tC4ED6A2024348F6B2CB712DC491F7E57AEF32630*)L_0, (Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 *)L_1, (Func_2_tB181F4C9A24C0634DF4A10CFBFFE6B45B3796BBC *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectArrayIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,UnityEngine.InputSystem.Utilities.InternedString>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectArrayIterator_2_MoveNext_m95F39B227EDAFB6B867EC7F18D59DB16CB94918F_gshared (WhereSelectArrayIterator_2_tB9FDF4ABA25271C5465121B8CD088BB04D7A03F9 * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->get_state_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_006b;
		}
	}
	{
		goto IL_0055;
	}

IL_000b:
	{
		KeyValuePair_2U5BU5D_tC4ED6A2024348F6B2CB712DC491F7E57AEF32630* L_1 = (KeyValuePair_2U5BU5D_tC4ED6A2024348F6B2CB712DC491F7E57AEF32630*)__this->get_source_3();
		int32_t L_2 = (int32_t)__this->get_index_6();
		NullCheck(L_1);
		int32_t L_3 = L_2;
		KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184  L_4 = (KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184 )(L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		V_0 = (KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184 )L_4;
		int32_t L_5 = (int32_t)__this->get_index_6();
		__this->set_index_6(((int32_t)il2cpp_codegen_add((int32_t)L_5, (int32_t)1)));
		Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 * L_6 = (Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 *)__this->get_predicate_4();
		if (!L_6)
		{
			goto IL_0041;
		}
	}
	{
		Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 * L_7 = (Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 *)__this->get_predicate_4();
		KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184  L_8 = V_0;
		NullCheck((Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 *)L_7);
		bool L_9;
		L_9 = ((  bool (*) (Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 *, KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 *)L_7, (KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184 )L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		if (!L_9)
		{
			goto IL_0055;
		}
	}

IL_0041:
	{
		Func_2_tB181F4C9A24C0634DF4A10CFBFFE6B45B3796BBC * L_10 = (Func_2_tB181F4C9A24C0634DF4A10CFBFFE6B45B3796BBC *)__this->get_selector_5();
		KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184  L_11 = V_0;
		NullCheck((Func_2_tB181F4C9A24C0634DF4A10CFBFFE6B45B3796BBC *)L_10);
		InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  L_12;
		L_12 = ((  InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  (*) (Func_2_tB181F4C9A24C0634DF4A10CFBFFE6B45B3796BBC *, KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5)->methodPointer)((Func_2_tB181F4C9A24C0634DF4A10CFBFFE6B45B3796BBC *)L_10, (KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184 )L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->set_current_2(L_12);
		return (bool)1;
	}

IL_0055:
	{
		int32_t L_13 = (int32_t)__this->get_index_6();
		KeyValuePair_2U5BU5D_tC4ED6A2024348F6B2CB712DC491F7E57AEF32630* L_14 = (KeyValuePair_2U5BU5D_tC4ED6A2024348F6B2CB712DC491F7E57AEF32630*)__this->get_source_3();
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_14)->max_length))))))
		{
			goto IL_000b;
		}
	}
	{
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		VirtualActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<UnityEngine.InputSystem.Utilities.InternedString>::Dispose() */, (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
	}

IL_006b:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectArrayIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,UnityEngine.InputSystem.Utilities.InternedString>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectArrayIterator_2_Where_m59BE57DCED0988521B161E34EF1BA01004A73F55_gshared (WhereSelectArrayIterator_2_tB9FDF4ABA25271C5465121B8CD088BB04D7A03F9 * __this, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B * L_1 = (WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 7));
		((  void (*) (WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B *, RuntimeObject*, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectArrayIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,System.Object>::.ctor(TSource[],System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectArrayIterator_2__ctor_m0DC76D9424BD9099C013190DC3300D69E823842E_gshared (WhereSelectArrayIterator_2_tEC7D2E75D2D8A9E1AA3D6F21AA9CBCB87EBD6617 * __this, KeyValuePair_2U5BU5D_tC4ED6A2024348F6B2CB712DC491F7E57AEF32630* ___source0, Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 * ___predicate1, Func_2_t711512467B454C91EB8F3FB19CD6D141AAB5B6B1 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		KeyValuePair_2U5BU5D_tC4ED6A2024348F6B2CB712DC491F7E57AEF32630* L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t711512467B454C91EB8F3FB19CD6D141AAB5B6B1 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectArrayIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,System.Object>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 * WhereSelectArrayIterator_2_Clone_m580D9F27CD31F03CB1B0EF01BC40107CCCEC9BE3_gshared (WhereSelectArrayIterator_2_tEC7D2E75D2D8A9E1AA3D6F21AA9CBCB87EBD6617 * __this, const RuntimeMethod* method)
{
	{
		KeyValuePair_2U5BU5D_tC4ED6A2024348F6B2CB712DC491F7E57AEF32630* L_0 = (KeyValuePair_2U5BU5D_tC4ED6A2024348F6B2CB712DC491F7E57AEF32630*)__this->get_source_3();
		Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 * L_1 = (Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 *)__this->get_predicate_4();
		Func_2_t711512467B454C91EB8F3FB19CD6D141AAB5B6B1 * L_2 = (Func_2_t711512467B454C91EB8F3FB19CD6D141AAB5B6B1 *)__this->get_selector_5();
		WhereSelectArrayIterator_2_tEC7D2E75D2D8A9E1AA3D6F21AA9CBCB87EBD6617 * L_3 = (WhereSelectArrayIterator_2_tEC7D2E75D2D8A9E1AA3D6F21AA9CBCB87EBD6617 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectArrayIterator_2_tEC7D2E75D2D8A9E1AA3D6F21AA9CBCB87EBD6617 *, KeyValuePair_2U5BU5D_tC4ED6A2024348F6B2CB712DC491F7E57AEF32630*, Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 *, Func_2_t711512467B454C91EB8F3FB19CD6D141AAB5B6B1 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (KeyValuePair_2U5BU5D_tC4ED6A2024348F6B2CB712DC491F7E57AEF32630*)L_0, (Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 *)L_1, (Func_2_t711512467B454C91EB8F3FB19CD6D141AAB5B6B1 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectArrayIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectArrayIterator_2_MoveNext_m7AEAB699347B26B88018B7CCE04F5BFEE34B5159_gshared (WhereSelectArrayIterator_2_tEC7D2E75D2D8A9E1AA3D6F21AA9CBCB87EBD6617 * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->get_state_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_006b;
		}
	}
	{
		goto IL_0055;
	}

IL_000b:
	{
		KeyValuePair_2U5BU5D_tC4ED6A2024348F6B2CB712DC491F7E57AEF32630* L_1 = (KeyValuePair_2U5BU5D_tC4ED6A2024348F6B2CB712DC491F7E57AEF32630*)__this->get_source_3();
		int32_t L_2 = (int32_t)__this->get_index_6();
		NullCheck(L_1);
		int32_t L_3 = L_2;
		KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184  L_4 = (KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184 )(L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		V_0 = (KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184 )L_4;
		int32_t L_5 = (int32_t)__this->get_index_6();
		__this->set_index_6(((int32_t)il2cpp_codegen_add((int32_t)L_5, (int32_t)1)));
		Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 * L_6 = (Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 *)__this->get_predicate_4();
		if (!L_6)
		{
			goto IL_0041;
		}
	}
	{
		Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 * L_7 = (Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 *)__this->get_predicate_4();
		KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184  L_8 = V_0;
		NullCheck((Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 *)L_7);
		bool L_9;
		L_9 = ((  bool (*) (Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 *, KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 *)L_7, (KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184 )L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		if (!L_9)
		{
			goto IL_0055;
		}
	}

IL_0041:
	{
		Func_2_t711512467B454C91EB8F3FB19CD6D141AAB5B6B1 * L_10 = (Func_2_t711512467B454C91EB8F3FB19CD6D141AAB5B6B1 *)__this->get_selector_5();
		KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184  L_11 = V_0;
		NullCheck((Func_2_t711512467B454C91EB8F3FB19CD6D141AAB5B6B1 *)L_10);
		RuntimeObject * L_12;
		L_12 = ((  RuntimeObject * (*) (Func_2_t711512467B454C91EB8F3FB19CD6D141AAB5B6B1 *, KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5)->methodPointer)((Func_2_t711512467B454C91EB8F3FB19CD6D141AAB5B6B1 *)L_10, (KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184 )L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_current_2(L_12);
		return (bool)1;
	}

IL_0055:
	{
		int32_t L_13 = (int32_t)__this->get_index_6();
		KeyValuePair_2U5BU5D_tC4ED6A2024348F6B2CB712DC491F7E57AEF32630* L_14 = (KeyValuePair_2U5BU5D_tC4ED6A2024348F6B2CB712DC491F7E57AEF32630*)__this->get_source_3();
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_14)->max_length))))))
		{
			goto IL_000b;
		}
	}
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		VirtualActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Object>::Dispose() */, (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
	}

IL_006b:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectArrayIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,System.Object>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectArrayIterator_2_Where_m1E0BDCB85CDD769F1853E084C53160E9A3643C86_gshared (WhereSelectArrayIterator_2_tEC7D2E75D2D8A9E1AA3D6F21AA9CBCB87EBD6617 * __this, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 * L_1 = (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 7));
		((  void (*) (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *, RuntimeObject*, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectArrayIterator`2<UnityEngine.InputSystem.Utilities.InternedString,UnityEngine.InputSystem.Utilities.InternedString>::.ctor(TSource[],System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectArrayIterator_2__ctor_m442A8CF5B0D2260CB7237EE58C44BF6EF9F5BAE8_gshared (WhereSelectArrayIterator_2_t45B551CB9059F02D47B0F25BE9ECC3561DF4A23D * __this, InternedStringU5BU5D_t1EAE8433C8EF7C76894673801EE0EE9F6833E9A4* ___source0, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * ___predicate1, Func_2_t1AF155D27A4D78882342D697E4A6DE7CE594545B * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		((  void (*) (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		InternedStringU5BU5D_t1EAE8433C8EF7C76894673801EE0EE9F6833E9A4* L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t1AF155D27A4D78882342D697E4A6DE7CE594545B * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectArrayIterator`2<UnityEngine.InputSystem.Utilities.InternedString,UnityEngine.InputSystem.Utilities.InternedString>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 * WhereSelectArrayIterator_2_Clone_m8537740789DECB4FF54093BFA6638E5A10F72040_gshared (WhereSelectArrayIterator_2_t45B551CB9059F02D47B0F25BE9ECC3561DF4A23D * __this, const RuntimeMethod* method)
{
	{
		InternedStringU5BU5D_t1EAE8433C8EF7C76894673801EE0EE9F6833E9A4* L_0 = (InternedStringU5BU5D_t1EAE8433C8EF7C76894673801EE0EE9F6833E9A4*)__this->get_source_3();
		Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * L_1 = (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)__this->get_predicate_4();
		Func_2_t1AF155D27A4D78882342D697E4A6DE7CE594545B * L_2 = (Func_2_t1AF155D27A4D78882342D697E4A6DE7CE594545B *)__this->get_selector_5();
		WhereSelectArrayIterator_2_t45B551CB9059F02D47B0F25BE9ECC3561DF4A23D * L_3 = (WhereSelectArrayIterator_2_t45B551CB9059F02D47B0F25BE9ECC3561DF4A23D *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectArrayIterator_2_t45B551CB9059F02D47B0F25BE9ECC3561DF4A23D *, InternedStringU5BU5D_t1EAE8433C8EF7C76894673801EE0EE9F6833E9A4*, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *, Func_2_t1AF155D27A4D78882342D697E4A6DE7CE594545B *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (InternedStringU5BU5D_t1EAE8433C8EF7C76894673801EE0EE9F6833E9A4*)L_0, (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)L_1, (Func_2_t1AF155D27A4D78882342D697E4A6DE7CE594545B *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectArrayIterator`2<UnityEngine.InputSystem.Utilities.InternedString,UnityEngine.InputSystem.Utilities.InternedString>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectArrayIterator_2_MoveNext_mFCBFF69B59F71556E9A12021DC8216509DBC34B0_gshared (WhereSelectArrayIterator_2_t45B551CB9059F02D47B0F25BE9ECC3561DF4A23D * __this, const RuntimeMethod* method)
{
	InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->get_state_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_006b;
		}
	}
	{
		goto IL_0055;
	}

IL_000b:
	{
		InternedStringU5BU5D_t1EAE8433C8EF7C76894673801EE0EE9F6833E9A4* L_1 = (InternedStringU5BU5D_t1EAE8433C8EF7C76894673801EE0EE9F6833E9A4*)__this->get_source_3();
		int32_t L_2 = (int32_t)__this->get_index_6();
		NullCheck(L_1);
		int32_t L_3 = L_2;
		InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		V_0 = (InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED )L_4;
		int32_t L_5 = (int32_t)__this->get_index_6();
		__this->set_index_6(((int32_t)il2cpp_codegen_add((int32_t)L_5, (int32_t)1)));
		Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * L_6 = (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)__this->get_predicate_4();
		if (!L_6)
		{
			goto IL_0041;
		}
	}
	{
		Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * L_7 = (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)__this->get_predicate_4();
		InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  L_8 = V_0;
		NullCheck((Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)L_7);
		bool L_9;
		L_9 = ((  bool (*) (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *, InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)L_7, (InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED )L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		if (!L_9)
		{
			goto IL_0055;
		}
	}

IL_0041:
	{
		Func_2_t1AF155D27A4D78882342D697E4A6DE7CE594545B * L_10 = (Func_2_t1AF155D27A4D78882342D697E4A6DE7CE594545B *)__this->get_selector_5();
		InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  L_11 = V_0;
		NullCheck((Func_2_t1AF155D27A4D78882342D697E4A6DE7CE594545B *)L_10);
		InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  L_12;
		L_12 = ((  InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  (*) (Func_2_t1AF155D27A4D78882342D697E4A6DE7CE594545B *, InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5)->methodPointer)((Func_2_t1AF155D27A4D78882342D697E4A6DE7CE594545B *)L_10, (InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED )L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->set_current_2(L_12);
		return (bool)1;
	}

IL_0055:
	{
		int32_t L_13 = (int32_t)__this->get_index_6();
		InternedStringU5BU5D_t1EAE8433C8EF7C76894673801EE0EE9F6833E9A4* L_14 = (InternedStringU5BU5D_t1EAE8433C8EF7C76894673801EE0EE9F6833E9A4*)__this->get_source_3();
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_14)->max_length))))))
		{
			goto IL_000b;
		}
	}
	{
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		VirtualActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<UnityEngine.InputSystem.Utilities.InternedString>::Dispose() */, (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
	}

IL_006b:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectArrayIterator`2<UnityEngine.InputSystem.Utilities.InternedString,UnityEngine.InputSystem.Utilities.InternedString>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectArrayIterator_2_Where_m54BFCE66D91B424A54F39003826DC2C031099651_gshared (WhereSelectArrayIterator_2_t45B551CB9059F02D47B0F25BE9ECC3561DF4A23D * __this, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B * L_1 = (WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 7));
		((  void (*) (WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B *, RuntimeObject*, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectArrayIterator`2<UnityEngine.InputSystem.Utilities.InternedString,System.Object>::.ctor(TSource[],System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectArrayIterator_2__ctor_m1298BA5BA1956DF0F705D67818CB7E3A74BDB475_gshared (WhereSelectArrayIterator_2_t297D221F6AEFE060ABD891ACCBE86FD625D9F687 * __this, InternedStringU5BU5D_t1EAE8433C8EF7C76894673801EE0EE9F6833E9A4* ___source0, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * ___predicate1, Func_2_tA2BD34BB8D97AD282016BD4CB5B8675E4AF85B45 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		InternedStringU5BU5D_t1EAE8433C8EF7C76894673801EE0EE9F6833E9A4* L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_tA2BD34BB8D97AD282016BD4CB5B8675E4AF85B45 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectArrayIterator`2<UnityEngine.InputSystem.Utilities.InternedString,System.Object>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 * WhereSelectArrayIterator_2_Clone_m48D50D2F52888AB8FEEC67AD6DBC94379BFFEFBE_gshared (WhereSelectArrayIterator_2_t297D221F6AEFE060ABD891ACCBE86FD625D9F687 * __this, const RuntimeMethod* method)
{
	{
		InternedStringU5BU5D_t1EAE8433C8EF7C76894673801EE0EE9F6833E9A4* L_0 = (InternedStringU5BU5D_t1EAE8433C8EF7C76894673801EE0EE9F6833E9A4*)__this->get_source_3();
		Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * L_1 = (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)__this->get_predicate_4();
		Func_2_tA2BD34BB8D97AD282016BD4CB5B8675E4AF85B45 * L_2 = (Func_2_tA2BD34BB8D97AD282016BD4CB5B8675E4AF85B45 *)__this->get_selector_5();
		WhereSelectArrayIterator_2_t297D221F6AEFE060ABD891ACCBE86FD625D9F687 * L_3 = (WhereSelectArrayIterator_2_t297D221F6AEFE060ABD891ACCBE86FD625D9F687 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectArrayIterator_2_t297D221F6AEFE060ABD891ACCBE86FD625D9F687 *, InternedStringU5BU5D_t1EAE8433C8EF7C76894673801EE0EE9F6833E9A4*, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *, Func_2_tA2BD34BB8D97AD282016BD4CB5B8675E4AF85B45 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (InternedStringU5BU5D_t1EAE8433C8EF7C76894673801EE0EE9F6833E9A4*)L_0, (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)L_1, (Func_2_tA2BD34BB8D97AD282016BD4CB5B8675E4AF85B45 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectArrayIterator`2<UnityEngine.InputSystem.Utilities.InternedString,System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectArrayIterator_2_MoveNext_m1F6B0E2B67D69ADA0C779AAE77BBAED95543FB77_gshared (WhereSelectArrayIterator_2_t297D221F6AEFE060ABD891ACCBE86FD625D9F687 * __this, const RuntimeMethod* method)
{
	InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->get_state_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_006b;
		}
	}
	{
		goto IL_0055;
	}

IL_000b:
	{
		InternedStringU5BU5D_t1EAE8433C8EF7C76894673801EE0EE9F6833E9A4* L_1 = (InternedStringU5BU5D_t1EAE8433C8EF7C76894673801EE0EE9F6833E9A4*)__this->get_source_3();
		int32_t L_2 = (int32_t)__this->get_index_6();
		NullCheck(L_1);
		int32_t L_3 = L_2;
		InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		V_0 = (InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED )L_4;
		int32_t L_5 = (int32_t)__this->get_index_6();
		__this->set_index_6(((int32_t)il2cpp_codegen_add((int32_t)L_5, (int32_t)1)));
		Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * L_6 = (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)__this->get_predicate_4();
		if (!L_6)
		{
			goto IL_0041;
		}
	}
	{
		Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * L_7 = (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)__this->get_predicate_4();
		InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  L_8 = V_0;
		NullCheck((Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)L_7);
		bool L_9;
		L_9 = ((  bool (*) (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *, InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)L_7, (InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED )L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		if (!L_9)
		{
			goto IL_0055;
		}
	}

IL_0041:
	{
		Func_2_tA2BD34BB8D97AD282016BD4CB5B8675E4AF85B45 * L_10 = (Func_2_tA2BD34BB8D97AD282016BD4CB5B8675E4AF85B45 *)__this->get_selector_5();
		InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  L_11 = V_0;
		NullCheck((Func_2_tA2BD34BB8D97AD282016BD4CB5B8675E4AF85B45 *)L_10);
		RuntimeObject * L_12;
		L_12 = ((  RuntimeObject * (*) (Func_2_tA2BD34BB8D97AD282016BD4CB5B8675E4AF85B45 *, InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5)->methodPointer)((Func_2_tA2BD34BB8D97AD282016BD4CB5B8675E4AF85B45 *)L_10, (InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED )L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_current_2(L_12);
		return (bool)1;
	}

IL_0055:
	{
		int32_t L_13 = (int32_t)__this->get_index_6();
		InternedStringU5BU5D_t1EAE8433C8EF7C76894673801EE0EE9F6833E9A4* L_14 = (InternedStringU5BU5D_t1EAE8433C8EF7C76894673801EE0EE9F6833E9A4*)__this->get_source_3();
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_14)->max_length))))))
		{
			goto IL_000b;
		}
	}
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		VirtualActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Object>::Dispose() */, (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
	}

IL_006b:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectArrayIterator`2<UnityEngine.InputSystem.Utilities.InternedString,System.Object>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectArrayIterator_2_Where_m21C4B48752198477D7ADFC5ABA34402064D49361_gshared (WhereSelectArrayIterator_2_t297D221F6AEFE060ABD891ACCBE86FD625D9F687 * __this, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 * L_1 = (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 7));
		((  void (*) (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *, RuntimeObject*, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectArrayIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,UnityEngine.InputSystem.Utilities.InternedString>::.ctor(TSource[],System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectArrayIterator_2__ctor_m1CDA06841FA5412FA4FA1D07A789352F3852408C_gshared (WhereSelectArrayIterator_2_tA56FBB916D6D4ED801C2818530D724C0C701D490 * __this, NameAndParametersU5BU5D_t960DDB52F44DD1231B0C626E9C859F1E0B2F5EAD* ___source0, Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 * ___predicate1, Func_2_tEA2235809AB0E1AB8425745179691D76B519B0C5 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		((  void (*) (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		NameAndParametersU5BU5D_t960DDB52F44DD1231B0C626E9C859F1E0B2F5EAD* L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_tEA2235809AB0E1AB8425745179691D76B519B0C5 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectArrayIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,UnityEngine.InputSystem.Utilities.InternedString>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 * WhereSelectArrayIterator_2_Clone_mF41274953637D82A37C99AAEAB5856A1D9A8E9F4_gshared (WhereSelectArrayIterator_2_tA56FBB916D6D4ED801C2818530D724C0C701D490 * __this, const RuntimeMethod* method)
{
	{
		NameAndParametersU5BU5D_t960DDB52F44DD1231B0C626E9C859F1E0B2F5EAD* L_0 = (NameAndParametersU5BU5D_t960DDB52F44DD1231B0C626E9C859F1E0B2F5EAD*)__this->get_source_3();
		Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 * L_1 = (Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 *)__this->get_predicate_4();
		Func_2_tEA2235809AB0E1AB8425745179691D76B519B0C5 * L_2 = (Func_2_tEA2235809AB0E1AB8425745179691D76B519B0C5 *)__this->get_selector_5();
		WhereSelectArrayIterator_2_tA56FBB916D6D4ED801C2818530D724C0C701D490 * L_3 = (WhereSelectArrayIterator_2_tA56FBB916D6D4ED801C2818530D724C0C701D490 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectArrayIterator_2_tA56FBB916D6D4ED801C2818530D724C0C701D490 *, NameAndParametersU5BU5D_t960DDB52F44DD1231B0C626E9C859F1E0B2F5EAD*, Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 *, Func_2_tEA2235809AB0E1AB8425745179691D76B519B0C5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (NameAndParametersU5BU5D_t960DDB52F44DD1231B0C626E9C859F1E0B2F5EAD*)L_0, (Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 *)L_1, (Func_2_tEA2235809AB0E1AB8425745179691D76B519B0C5 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectArrayIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,UnityEngine.InputSystem.Utilities.InternedString>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectArrayIterator_2_MoveNext_m2A28B048E8501021861A67C3707038B71A355925_gshared (WhereSelectArrayIterator_2_tA56FBB916D6D4ED801C2818530D724C0C701D490 * __this, const RuntimeMethod* method)
{
	NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->get_state_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_006b;
		}
	}
	{
		goto IL_0055;
	}

IL_000b:
	{
		NameAndParametersU5BU5D_t960DDB52F44DD1231B0C626E9C859F1E0B2F5EAD* L_1 = (NameAndParametersU5BU5D_t960DDB52F44DD1231B0C626E9C859F1E0B2F5EAD*)__this->get_source_3();
		int32_t L_2 = (int32_t)__this->get_index_6();
		NullCheck(L_1);
		int32_t L_3 = L_2;
		NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA  L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		V_0 = (NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA )L_4;
		int32_t L_5 = (int32_t)__this->get_index_6();
		__this->set_index_6(((int32_t)il2cpp_codegen_add((int32_t)L_5, (int32_t)1)));
		Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 * L_6 = (Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 *)__this->get_predicate_4();
		if (!L_6)
		{
			goto IL_0041;
		}
	}
	{
		Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 * L_7 = (Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 *)__this->get_predicate_4();
		NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA  L_8 = V_0;
		NullCheck((Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 *)L_7);
		bool L_9;
		L_9 = ((  bool (*) (Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 *, NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 *)L_7, (NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA )L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		if (!L_9)
		{
			goto IL_0055;
		}
	}

IL_0041:
	{
		Func_2_tEA2235809AB0E1AB8425745179691D76B519B0C5 * L_10 = (Func_2_tEA2235809AB0E1AB8425745179691D76B519B0C5 *)__this->get_selector_5();
		NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA  L_11 = V_0;
		NullCheck((Func_2_tEA2235809AB0E1AB8425745179691D76B519B0C5 *)L_10);
		InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  L_12;
		L_12 = ((  InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  (*) (Func_2_tEA2235809AB0E1AB8425745179691D76B519B0C5 *, NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5)->methodPointer)((Func_2_tEA2235809AB0E1AB8425745179691D76B519B0C5 *)L_10, (NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA )L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->set_current_2(L_12);
		return (bool)1;
	}

IL_0055:
	{
		int32_t L_13 = (int32_t)__this->get_index_6();
		NameAndParametersU5BU5D_t960DDB52F44DD1231B0C626E9C859F1E0B2F5EAD* L_14 = (NameAndParametersU5BU5D_t960DDB52F44DD1231B0C626E9C859F1E0B2F5EAD*)__this->get_source_3();
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_14)->max_length))))))
		{
			goto IL_000b;
		}
	}
	{
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		VirtualActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<UnityEngine.InputSystem.Utilities.InternedString>::Dispose() */, (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
	}

IL_006b:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectArrayIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,UnityEngine.InputSystem.Utilities.InternedString>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectArrayIterator_2_Where_m7CC22A87D123571D92A52A59C64CF8F2008A84FC_gshared (WhereSelectArrayIterator_2_tA56FBB916D6D4ED801C2818530D724C0C701D490 * __this, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B * L_1 = (WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 7));
		((  void (*) (WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B *, RuntimeObject*, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectArrayIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,System.Object>::.ctor(TSource[],System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectArrayIterator_2__ctor_m45887BC43E87416594EAC6B2F4F1BF6BFA09EB3B_gshared (WhereSelectArrayIterator_2_t8E1E4D86BDBA319DD5FB2D615DE93CAE92D68506 * __this, NameAndParametersU5BU5D_t960DDB52F44DD1231B0C626E9C859F1E0B2F5EAD* ___source0, Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 * ___predicate1, Func_2_tF7304610FB007335AC870B51C8FE2092628DC0DB * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		NameAndParametersU5BU5D_t960DDB52F44DD1231B0C626E9C859F1E0B2F5EAD* L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_tF7304610FB007335AC870B51C8FE2092628DC0DB * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectArrayIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,System.Object>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 * WhereSelectArrayIterator_2_Clone_m6525E56F01C1488F6EFE074F84FDB3EEB8CAF396_gshared (WhereSelectArrayIterator_2_t8E1E4D86BDBA319DD5FB2D615DE93CAE92D68506 * __this, const RuntimeMethod* method)
{
	{
		NameAndParametersU5BU5D_t960DDB52F44DD1231B0C626E9C859F1E0B2F5EAD* L_0 = (NameAndParametersU5BU5D_t960DDB52F44DD1231B0C626E9C859F1E0B2F5EAD*)__this->get_source_3();
		Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 * L_1 = (Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 *)__this->get_predicate_4();
		Func_2_tF7304610FB007335AC870B51C8FE2092628DC0DB * L_2 = (Func_2_tF7304610FB007335AC870B51C8FE2092628DC0DB *)__this->get_selector_5();
		WhereSelectArrayIterator_2_t8E1E4D86BDBA319DD5FB2D615DE93CAE92D68506 * L_3 = (WhereSelectArrayIterator_2_t8E1E4D86BDBA319DD5FB2D615DE93CAE92D68506 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectArrayIterator_2_t8E1E4D86BDBA319DD5FB2D615DE93CAE92D68506 *, NameAndParametersU5BU5D_t960DDB52F44DD1231B0C626E9C859F1E0B2F5EAD*, Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 *, Func_2_tF7304610FB007335AC870B51C8FE2092628DC0DB *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (NameAndParametersU5BU5D_t960DDB52F44DD1231B0C626E9C859F1E0B2F5EAD*)L_0, (Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 *)L_1, (Func_2_tF7304610FB007335AC870B51C8FE2092628DC0DB *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectArrayIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectArrayIterator_2_MoveNext_m53A035B711769E36F1AA738FE393E25BBB3F5455_gshared (WhereSelectArrayIterator_2_t8E1E4D86BDBA319DD5FB2D615DE93CAE92D68506 * __this, const RuntimeMethod* method)
{
	NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->get_state_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_006b;
		}
	}
	{
		goto IL_0055;
	}

IL_000b:
	{
		NameAndParametersU5BU5D_t960DDB52F44DD1231B0C626E9C859F1E0B2F5EAD* L_1 = (NameAndParametersU5BU5D_t960DDB52F44DD1231B0C626E9C859F1E0B2F5EAD*)__this->get_source_3();
		int32_t L_2 = (int32_t)__this->get_index_6();
		NullCheck(L_1);
		int32_t L_3 = L_2;
		NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA  L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		V_0 = (NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA )L_4;
		int32_t L_5 = (int32_t)__this->get_index_6();
		__this->set_index_6(((int32_t)il2cpp_codegen_add((int32_t)L_5, (int32_t)1)));
		Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 * L_6 = (Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 *)__this->get_predicate_4();
		if (!L_6)
		{
			goto IL_0041;
		}
	}
	{
		Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 * L_7 = (Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 *)__this->get_predicate_4();
		NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA  L_8 = V_0;
		NullCheck((Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 *)L_7);
		bool L_9;
		L_9 = ((  bool (*) (Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 *, NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 *)L_7, (NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA )L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		if (!L_9)
		{
			goto IL_0055;
		}
	}

IL_0041:
	{
		Func_2_tF7304610FB007335AC870B51C8FE2092628DC0DB * L_10 = (Func_2_tF7304610FB007335AC870B51C8FE2092628DC0DB *)__this->get_selector_5();
		NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA  L_11 = V_0;
		NullCheck((Func_2_tF7304610FB007335AC870B51C8FE2092628DC0DB *)L_10);
		RuntimeObject * L_12;
		L_12 = ((  RuntimeObject * (*) (Func_2_tF7304610FB007335AC870B51C8FE2092628DC0DB *, NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5)->methodPointer)((Func_2_tF7304610FB007335AC870B51C8FE2092628DC0DB *)L_10, (NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA )L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_current_2(L_12);
		return (bool)1;
	}

IL_0055:
	{
		int32_t L_13 = (int32_t)__this->get_index_6();
		NameAndParametersU5BU5D_t960DDB52F44DD1231B0C626E9C859F1E0B2F5EAD* L_14 = (NameAndParametersU5BU5D_t960DDB52F44DD1231B0C626E9C859F1E0B2F5EAD*)__this->get_source_3();
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_14)->max_length))))))
		{
			goto IL_000b;
		}
	}
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		VirtualActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Object>::Dispose() */, (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
	}

IL_006b:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectArrayIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,System.Object>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectArrayIterator_2_Where_mAB0A4DCFEF7FFBDC87EAF80F750FCD40D7CD21FE_gshared (WhereSelectArrayIterator_2_t8E1E4D86BDBA319DD5FB2D615DE93CAE92D68506 * __this, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 * L_1 = (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 7));
		((  void (*) (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *, RuntimeObject*, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectArrayIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,UnityEngine.InputSystem.Utilities.InternedString>::.ctor(TSource[],System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectArrayIterator_2__ctor_m39F1842A958F6DF6A8C09ACD4D46C5EFF02CAF0F_gshared (WhereSelectArrayIterator_2_t81C380953EA0951A36339AC8CE79A9B039372941 * __this, NamedValueU5BU5D_t16F5D30B70B5FEDA89AE16CFD61FACEC7DF85391* ___source0, Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 * ___predicate1, Func_2_tA21678F1D9F9F4617D645755285D28EDE5F9AE44 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		((  void (*) (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		NamedValueU5BU5D_t16F5D30B70B5FEDA89AE16CFD61FACEC7DF85391* L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_tA21678F1D9F9F4617D645755285D28EDE5F9AE44 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectArrayIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,UnityEngine.InputSystem.Utilities.InternedString>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 * WhereSelectArrayIterator_2_Clone_m1EE832EB86360FF5AC0799C12FABFE22379C91A0_gshared (WhereSelectArrayIterator_2_t81C380953EA0951A36339AC8CE79A9B039372941 * __this, const RuntimeMethod* method)
{
	{
		NamedValueU5BU5D_t16F5D30B70B5FEDA89AE16CFD61FACEC7DF85391* L_0 = (NamedValueU5BU5D_t16F5D30B70B5FEDA89AE16CFD61FACEC7DF85391*)__this->get_source_3();
		Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 * L_1 = (Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 *)__this->get_predicate_4();
		Func_2_tA21678F1D9F9F4617D645755285D28EDE5F9AE44 * L_2 = (Func_2_tA21678F1D9F9F4617D645755285D28EDE5F9AE44 *)__this->get_selector_5();
		WhereSelectArrayIterator_2_t81C380953EA0951A36339AC8CE79A9B039372941 * L_3 = (WhereSelectArrayIterator_2_t81C380953EA0951A36339AC8CE79A9B039372941 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectArrayIterator_2_t81C380953EA0951A36339AC8CE79A9B039372941 *, NamedValueU5BU5D_t16F5D30B70B5FEDA89AE16CFD61FACEC7DF85391*, Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 *, Func_2_tA21678F1D9F9F4617D645755285D28EDE5F9AE44 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (NamedValueU5BU5D_t16F5D30B70B5FEDA89AE16CFD61FACEC7DF85391*)L_0, (Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 *)L_1, (Func_2_tA21678F1D9F9F4617D645755285D28EDE5F9AE44 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectArrayIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,UnityEngine.InputSystem.Utilities.InternedString>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectArrayIterator_2_MoveNext_mD55783EB2AE232C9A7EAD5C7E571D022DF423C99_gshared (WhereSelectArrayIterator_2_t81C380953EA0951A36339AC8CE79A9B039372941 * __this, const RuntimeMethod* method)
{
	NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->get_state_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_006b;
		}
	}
	{
		goto IL_0055;
	}

IL_000b:
	{
		NamedValueU5BU5D_t16F5D30B70B5FEDA89AE16CFD61FACEC7DF85391* L_1 = (NamedValueU5BU5D_t16F5D30B70B5FEDA89AE16CFD61FACEC7DF85391*)__this->get_source_3();
		int32_t L_2 = (int32_t)__this->get_index_6();
		NullCheck(L_1);
		int32_t L_3 = L_2;
		NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077  L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		V_0 = (NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077 )L_4;
		int32_t L_5 = (int32_t)__this->get_index_6();
		__this->set_index_6(((int32_t)il2cpp_codegen_add((int32_t)L_5, (int32_t)1)));
		Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 * L_6 = (Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 *)__this->get_predicate_4();
		if (!L_6)
		{
			goto IL_0041;
		}
	}
	{
		Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 * L_7 = (Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 *)__this->get_predicate_4();
		NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077  L_8 = V_0;
		NullCheck((Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 *)L_7);
		bool L_9;
		L_9 = ((  bool (*) (Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 *, NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 *)L_7, (NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077 )L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		if (!L_9)
		{
			goto IL_0055;
		}
	}

IL_0041:
	{
		Func_2_tA21678F1D9F9F4617D645755285D28EDE5F9AE44 * L_10 = (Func_2_tA21678F1D9F9F4617D645755285D28EDE5F9AE44 *)__this->get_selector_5();
		NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077  L_11 = V_0;
		NullCheck((Func_2_tA21678F1D9F9F4617D645755285D28EDE5F9AE44 *)L_10);
		InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  L_12;
		L_12 = ((  InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  (*) (Func_2_tA21678F1D9F9F4617D645755285D28EDE5F9AE44 *, NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5)->methodPointer)((Func_2_tA21678F1D9F9F4617D645755285D28EDE5F9AE44 *)L_10, (NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077 )L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->set_current_2(L_12);
		return (bool)1;
	}

IL_0055:
	{
		int32_t L_13 = (int32_t)__this->get_index_6();
		NamedValueU5BU5D_t16F5D30B70B5FEDA89AE16CFD61FACEC7DF85391* L_14 = (NamedValueU5BU5D_t16F5D30B70B5FEDA89AE16CFD61FACEC7DF85391*)__this->get_source_3();
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_14)->max_length))))))
		{
			goto IL_000b;
		}
	}
	{
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		VirtualActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<UnityEngine.InputSystem.Utilities.InternedString>::Dispose() */, (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
	}

IL_006b:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectArrayIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,UnityEngine.InputSystem.Utilities.InternedString>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectArrayIterator_2_Where_m20211252C5302DFDC3824AEA6A26239993556A86_gshared (WhereSelectArrayIterator_2_t81C380953EA0951A36339AC8CE79A9B039372941 * __this, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B * L_1 = (WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 7));
		((  void (*) (WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B *, RuntimeObject*, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectArrayIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,System.Object>::.ctor(TSource[],System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectArrayIterator_2__ctor_mF37FD86281232A0662CF69467E87F17C4ABF7E43_gshared (WhereSelectArrayIterator_2_tFFAF318BF43576D4F2880331F0ECAB9792119697 * __this, NamedValueU5BU5D_t16F5D30B70B5FEDA89AE16CFD61FACEC7DF85391* ___source0, Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 * ___predicate1, Func_2_t5B101C0B26ADAA3B1A77BC50D51CC42364E5409E * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		NamedValueU5BU5D_t16F5D30B70B5FEDA89AE16CFD61FACEC7DF85391* L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t5B101C0B26ADAA3B1A77BC50D51CC42364E5409E * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectArrayIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,System.Object>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 * WhereSelectArrayIterator_2_Clone_mACCF6BE9FABC371C7D9DFB944CA24C447069C79C_gshared (WhereSelectArrayIterator_2_tFFAF318BF43576D4F2880331F0ECAB9792119697 * __this, const RuntimeMethod* method)
{
	{
		NamedValueU5BU5D_t16F5D30B70B5FEDA89AE16CFD61FACEC7DF85391* L_0 = (NamedValueU5BU5D_t16F5D30B70B5FEDA89AE16CFD61FACEC7DF85391*)__this->get_source_3();
		Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 * L_1 = (Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 *)__this->get_predicate_4();
		Func_2_t5B101C0B26ADAA3B1A77BC50D51CC42364E5409E * L_2 = (Func_2_t5B101C0B26ADAA3B1A77BC50D51CC42364E5409E *)__this->get_selector_5();
		WhereSelectArrayIterator_2_tFFAF318BF43576D4F2880331F0ECAB9792119697 * L_3 = (WhereSelectArrayIterator_2_tFFAF318BF43576D4F2880331F0ECAB9792119697 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectArrayIterator_2_tFFAF318BF43576D4F2880331F0ECAB9792119697 *, NamedValueU5BU5D_t16F5D30B70B5FEDA89AE16CFD61FACEC7DF85391*, Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 *, Func_2_t5B101C0B26ADAA3B1A77BC50D51CC42364E5409E *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (NamedValueU5BU5D_t16F5D30B70B5FEDA89AE16CFD61FACEC7DF85391*)L_0, (Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 *)L_1, (Func_2_t5B101C0B26ADAA3B1A77BC50D51CC42364E5409E *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectArrayIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectArrayIterator_2_MoveNext_mEC3A29C9A876C550770D7C9A7EA46628438D8847_gshared (WhereSelectArrayIterator_2_tFFAF318BF43576D4F2880331F0ECAB9792119697 * __this, const RuntimeMethod* method)
{
	NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->get_state_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_006b;
		}
	}
	{
		goto IL_0055;
	}

IL_000b:
	{
		NamedValueU5BU5D_t16F5D30B70B5FEDA89AE16CFD61FACEC7DF85391* L_1 = (NamedValueU5BU5D_t16F5D30B70B5FEDA89AE16CFD61FACEC7DF85391*)__this->get_source_3();
		int32_t L_2 = (int32_t)__this->get_index_6();
		NullCheck(L_1);
		int32_t L_3 = L_2;
		NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077  L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		V_0 = (NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077 )L_4;
		int32_t L_5 = (int32_t)__this->get_index_6();
		__this->set_index_6(((int32_t)il2cpp_codegen_add((int32_t)L_5, (int32_t)1)));
		Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 * L_6 = (Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 *)__this->get_predicate_4();
		if (!L_6)
		{
			goto IL_0041;
		}
	}
	{
		Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 * L_7 = (Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 *)__this->get_predicate_4();
		NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077  L_8 = V_0;
		NullCheck((Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 *)L_7);
		bool L_9;
		L_9 = ((  bool (*) (Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 *, NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 *)L_7, (NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077 )L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		if (!L_9)
		{
			goto IL_0055;
		}
	}

IL_0041:
	{
		Func_2_t5B101C0B26ADAA3B1A77BC50D51CC42364E5409E * L_10 = (Func_2_t5B101C0B26ADAA3B1A77BC50D51CC42364E5409E *)__this->get_selector_5();
		NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077  L_11 = V_0;
		NullCheck((Func_2_t5B101C0B26ADAA3B1A77BC50D51CC42364E5409E *)L_10);
		RuntimeObject * L_12;
		L_12 = ((  RuntimeObject * (*) (Func_2_t5B101C0B26ADAA3B1A77BC50D51CC42364E5409E *, NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5)->methodPointer)((Func_2_t5B101C0B26ADAA3B1A77BC50D51CC42364E5409E *)L_10, (NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077 )L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_current_2(L_12);
		return (bool)1;
	}

IL_0055:
	{
		int32_t L_13 = (int32_t)__this->get_index_6();
		NamedValueU5BU5D_t16F5D30B70B5FEDA89AE16CFD61FACEC7DF85391* L_14 = (NamedValueU5BU5D_t16F5D30B70B5FEDA89AE16CFD61FACEC7DF85391*)__this->get_source_3();
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_14)->max_length))))))
		{
			goto IL_000b;
		}
	}
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		VirtualActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Object>::Dispose() */, (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
	}

IL_006b:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectArrayIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,System.Object>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectArrayIterator_2_Where_m1B1541B16EB7333AC7180CCB4F616C2F24C203E1_gshared (WhereSelectArrayIterator_2_tFFAF318BF43576D4F2880331F0ECAB9792119697 * __this, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 * L_1 = (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 7));
		((  void (*) (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *, RuntimeObject*, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectArrayIterator`2<System.Object,UnityEngine.InputSystem.Utilities.InternedString>::.ctor(TSource[],System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectArrayIterator_2__ctor_mB244D21215BDA679973B21593919E6C1406ED6F7_gshared (WhereSelectArrayIterator_2_t2A766812BE346746DE135AD9B855143B4FF483E4 * __this, ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ___source0, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate1, Func_2_t964229B6ADD2B008268547ED2BC3C090AF218065 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		((  void (*) (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t964229B6ADD2B008268547ED2BC3C090AF218065 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectArrayIterator`2<System.Object,UnityEngine.InputSystem.Utilities.InternedString>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 * WhereSelectArrayIterator_2_Clone_m4034491E503F6522BE832849B8EF1C1C91D84DC1_gshared (WhereSelectArrayIterator_2_t2A766812BE346746DE135AD9B855143B4FF483E4 * __this, const RuntimeMethod* method)
{
	{
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_0 = (ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)__this->get_source_3();
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_1 = (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)__this->get_predicate_4();
		Func_2_t964229B6ADD2B008268547ED2BC3C090AF218065 * L_2 = (Func_2_t964229B6ADD2B008268547ED2BC3C090AF218065 *)__this->get_selector_5();
		WhereSelectArrayIterator_2_t2A766812BE346746DE135AD9B855143B4FF483E4 * L_3 = (WhereSelectArrayIterator_2_t2A766812BE346746DE135AD9B855143B4FF483E4 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectArrayIterator_2_t2A766812BE346746DE135AD9B855143B4FF483E4 *, ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, Func_2_t964229B6ADD2B008268547ED2BC3C090AF218065 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)L_0, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_1, (Func_2_t964229B6ADD2B008268547ED2BC3C090AF218065 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectArrayIterator`2<System.Object,UnityEngine.InputSystem.Utilities.InternedString>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectArrayIterator_2_MoveNext_mA5A7AB2192E50BDF33B2E4834D62ADA3246099BE_gshared (WhereSelectArrayIterator_2_t2A766812BE346746DE135AD9B855143B4FF483E4 * __this, const RuntimeMethod* method)
{
	RuntimeObject * V_0 = NULL;
	{
		int32_t L_0 = (int32_t)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->get_state_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_006b;
		}
	}
	{
		goto IL_0055;
	}

IL_000b:
	{
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_1 = (ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)__this->get_source_3();
		int32_t L_2 = (int32_t)__this->get_index_6();
		NullCheck(L_1);
		int32_t L_3 = L_2;
		RuntimeObject * L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		V_0 = (RuntimeObject *)L_4;
		int32_t L_5 = (int32_t)__this->get_index_6();
		__this->set_index_6(((int32_t)il2cpp_codegen_add((int32_t)L_5, (int32_t)1)));
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_6 = (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)__this->get_predicate_4();
		if (!L_6)
		{
			goto IL_0041;
		}
	}
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_7 = (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)__this->get_predicate_4();
		RuntimeObject * L_8 = V_0;
		NullCheck((Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_7);
		bool L_9;
		L_9 = ((  bool (*) (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_7, (RuntimeObject *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		if (!L_9)
		{
			goto IL_0055;
		}
	}

IL_0041:
	{
		Func_2_t964229B6ADD2B008268547ED2BC3C090AF218065 * L_10 = (Func_2_t964229B6ADD2B008268547ED2BC3C090AF218065 *)__this->get_selector_5();
		RuntimeObject * L_11 = V_0;
		NullCheck((Func_2_t964229B6ADD2B008268547ED2BC3C090AF218065 *)L_10);
		InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  L_12;
		L_12 = ((  InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  (*) (Func_2_t964229B6ADD2B008268547ED2BC3C090AF218065 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5)->methodPointer)((Func_2_t964229B6ADD2B008268547ED2BC3C090AF218065 *)L_10, (RuntimeObject *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->set_current_2(L_12);
		return (bool)1;
	}

IL_0055:
	{
		int32_t L_13 = (int32_t)__this->get_index_6();
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_14 = (ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)__this->get_source_3();
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_14)->max_length))))))
		{
			goto IL_000b;
		}
	}
	{
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		VirtualActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<UnityEngine.InputSystem.Utilities.InternedString>::Dispose() */, (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
	}

IL_006b:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectArrayIterator`2<System.Object,UnityEngine.InputSystem.Utilities.InternedString>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectArrayIterator_2_Where_m1A316C44EBBEAFED195680008F626E544EAB6D1B_gshared (WhereSelectArrayIterator_2_t2A766812BE346746DE135AD9B855143B4FF483E4 * __this, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B * L_1 = (WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 7));
		((  void (*) (WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B *, RuntimeObject*, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectArrayIterator`2<System.Object,System.Object>::.ctor(TSource[],System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectArrayIterator_2__ctor_mBE564417A2815F82772933FB7A3D54223CC4F590_gshared (WhereSelectArrayIterator_2_tA706D5B1608A9A8F1BF43C6E5D9D682C901DB244 * __this, ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ___source0, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate1, Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectArrayIterator`2<System.Object,System.Object>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 * WhereSelectArrayIterator_2_Clone_mB5BF522301FDD86CDCB6187F82E28F37092539A9_gshared (WhereSelectArrayIterator_2_tA706D5B1608A9A8F1BF43C6E5D9D682C901DB244 * __this, const RuntimeMethod* method)
{
	{
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_0 = (ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)__this->get_source_3();
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_1 = (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)__this->get_predicate_4();
		Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 * L_2 = (Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 *)__this->get_selector_5();
		WhereSelectArrayIterator_2_tA706D5B1608A9A8F1BF43C6E5D9D682C901DB244 * L_3 = (WhereSelectArrayIterator_2_tA706D5B1608A9A8F1BF43C6E5D9D682C901DB244 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectArrayIterator_2_tA706D5B1608A9A8F1BF43C6E5D9D682C901DB244 *, ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)L_0, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_1, (Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectArrayIterator`2<System.Object,System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectArrayIterator_2_MoveNext_mCF08A119CF0CC000264B5B6BA5EC4B40CC9640CC_gshared (WhereSelectArrayIterator_2_tA706D5B1608A9A8F1BF43C6E5D9D682C901DB244 * __this, const RuntimeMethod* method)
{
	RuntimeObject * V_0 = NULL;
	{
		int32_t L_0 = (int32_t)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->get_state_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_006b;
		}
	}
	{
		goto IL_0055;
	}

IL_000b:
	{
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_1 = (ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)__this->get_source_3();
		int32_t L_2 = (int32_t)__this->get_index_6();
		NullCheck(L_1);
		int32_t L_3 = L_2;
		RuntimeObject * L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		V_0 = (RuntimeObject *)L_4;
		int32_t L_5 = (int32_t)__this->get_index_6();
		__this->set_index_6(((int32_t)il2cpp_codegen_add((int32_t)L_5, (int32_t)1)));
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_6 = (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)__this->get_predicate_4();
		if (!L_6)
		{
			goto IL_0041;
		}
	}
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_7 = (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)__this->get_predicate_4();
		RuntimeObject * L_8 = V_0;
		NullCheck((Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_7);
		bool L_9;
		L_9 = ((  bool (*) (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_7, (RuntimeObject *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		if (!L_9)
		{
			goto IL_0055;
		}
	}

IL_0041:
	{
		Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 * L_10 = (Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 *)__this->get_selector_5();
		RuntimeObject * L_11 = V_0;
		NullCheck((Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 *)L_10);
		RuntimeObject * L_12;
		L_12 = ((  RuntimeObject * (*) (Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5)->methodPointer)((Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 *)L_10, (RuntimeObject *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_current_2(L_12);
		return (bool)1;
	}

IL_0055:
	{
		int32_t L_13 = (int32_t)__this->get_index_6();
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_14 = (ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)__this->get_source_3();
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_14)->max_length))))))
		{
			goto IL_000b;
		}
	}
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		VirtualActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Object>::Dispose() */, (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
	}

IL_006b:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectArrayIterator`2<System.Object,System.Object>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectArrayIterator_2_Where_m6E91C090E874655848CBCAEF235B7DB5B244DA03_gshared (WhereSelectArrayIterator_2_tA706D5B1608A9A8F1BF43C6E5D9D682C901DB244 * __this, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 * L_1 = (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 7));
		((  void (*) (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *, RuntimeObject*, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectArrayIterator`2<UnityEngine.UIElements.StyleSelectorPart,UnityEngine.InputSystem.Utilities.InternedString>::.ctor(TSource[],System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectArrayIterator_2__ctor_m392F090B8485FA3E6BBDCBB2BB5B6130E57DA070_gshared (WhereSelectArrayIterator_2_t0EBFFCD87F6BDF27E6B0AB944A8106A0C0ABC3AE * __this, StyleSelectorPartU5BU5D_tD8762DADE4104C7D73D7B821DF7E5BBC87417BB7* ___source0, Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC * ___predicate1, Func_2_tFBBF89B72C697489D82FBE8FF822B191D2EE9A41 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		((  void (*) (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		StyleSelectorPartU5BU5D_tD8762DADE4104C7D73D7B821DF7E5BBC87417BB7* L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_tFBBF89B72C697489D82FBE8FF822B191D2EE9A41 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectArrayIterator`2<UnityEngine.UIElements.StyleSelectorPart,UnityEngine.InputSystem.Utilities.InternedString>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 * WhereSelectArrayIterator_2_Clone_m6E76F62420CAE4A3DB583EC26A3E3B307A0C7BBE_gshared (WhereSelectArrayIterator_2_t0EBFFCD87F6BDF27E6B0AB944A8106A0C0ABC3AE * __this, const RuntimeMethod* method)
{
	{
		StyleSelectorPartU5BU5D_tD8762DADE4104C7D73D7B821DF7E5BBC87417BB7* L_0 = (StyleSelectorPartU5BU5D_tD8762DADE4104C7D73D7B821DF7E5BBC87417BB7*)__this->get_source_3();
		Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC * L_1 = (Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC *)__this->get_predicate_4();
		Func_2_tFBBF89B72C697489D82FBE8FF822B191D2EE9A41 * L_2 = (Func_2_tFBBF89B72C697489D82FBE8FF822B191D2EE9A41 *)__this->get_selector_5();
		WhereSelectArrayIterator_2_t0EBFFCD87F6BDF27E6B0AB944A8106A0C0ABC3AE * L_3 = (WhereSelectArrayIterator_2_t0EBFFCD87F6BDF27E6B0AB944A8106A0C0ABC3AE *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectArrayIterator_2_t0EBFFCD87F6BDF27E6B0AB944A8106A0C0ABC3AE *, StyleSelectorPartU5BU5D_tD8762DADE4104C7D73D7B821DF7E5BBC87417BB7*, Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC *, Func_2_tFBBF89B72C697489D82FBE8FF822B191D2EE9A41 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (StyleSelectorPartU5BU5D_tD8762DADE4104C7D73D7B821DF7E5BBC87417BB7*)L_0, (Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC *)L_1, (Func_2_tFBBF89B72C697489D82FBE8FF822B191D2EE9A41 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectArrayIterator`2<UnityEngine.UIElements.StyleSelectorPart,UnityEngine.InputSystem.Utilities.InternedString>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectArrayIterator_2_MoveNext_m087B9DB1DE43AA4069BE9F397E06C50FBADD43AF_gshared (WhereSelectArrayIterator_2_t0EBFFCD87F6BDF27E6B0AB944A8106A0C0ABC3AE * __this, const RuntimeMethod* method)
{
	StyleSelectorPart_t707EDC970FC0F3E91E56DCBC178672A120426D54  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->get_state_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_006b;
		}
	}
	{
		goto IL_0055;
	}

IL_000b:
	{
		StyleSelectorPartU5BU5D_tD8762DADE4104C7D73D7B821DF7E5BBC87417BB7* L_1 = (StyleSelectorPartU5BU5D_tD8762DADE4104C7D73D7B821DF7E5BBC87417BB7*)__this->get_source_3();
		int32_t L_2 = (int32_t)__this->get_index_6();
		NullCheck(L_1);
		int32_t L_3 = L_2;
		StyleSelectorPart_t707EDC970FC0F3E91E56DCBC178672A120426D54  L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		V_0 = (StyleSelectorPart_t707EDC970FC0F3E91E56DCBC178672A120426D54 )L_4;
		int32_t L_5 = (int32_t)__this->get_index_6();
		__this->set_index_6(((int32_t)il2cpp_codegen_add((int32_t)L_5, (int32_t)1)));
		Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC * L_6 = (Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC *)__this->get_predicate_4();
		if (!L_6)
		{
			goto IL_0041;
		}
	}
	{
		Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC * L_7 = (Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC *)__this->get_predicate_4();
		StyleSelectorPart_t707EDC970FC0F3E91E56DCBC178672A120426D54  L_8 = V_0;
		NullCheck((Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC *)L_7);
		bool L_9;
		L_9 = ((  bool (*) (Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC *, StyleSelectorPart_t707EDC970FC0F3E91E56DCBC178672A120426D54 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC *)L_7, (StyleSelectorPart_t707EDC970FC0F3E91E56DCBC178672A120426D54 )L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		if (!L_9)
		{
			goto IL_0055;
		}
	}

IL_0041:
	{
		Func_2_tFBBF89B72C697489D82FBE8FF822B191D2EE9A41 * L_10 = (Func_2_tFBBF89B72C697489D82FBE8FF822B191D2EE9A41 *)__this->get_selector_5();
		StyleSelectorPart_t707EDC970FC0F3E91E56DCBC178672A120426D54  L_11 = V_0;
		NullCheck((Func_2_tFBBF89B72C697489D82FBE8FF822B191D2EE9A41 *)L_10);
		InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  L_12;
		L_12 = ((  InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  (*) (Func_2_tFBBF89B72C697489D82FBE8FF822B191D2EE9A41 *, StyleSelectorPart_t707EDC970FC0F3E91E56DCBC178672A120426D54 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5)->methodPointer)((Func_2_tFBBF89B72C697489D82FBE8FF822B191D2EE9A41 *)L_10, (StyleSelectorPart_t707EDC970FC0F3E91E56DCBC178672A120426D54 )L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->set_current_2(L_12);
		return (bool)1;
	}

IL_0055:
	{
		int32_t L_13 = (int32_t)__this->get_index_6();
		StyleSelectorPartU5BU5D_tD8762DADE4104C7D73D7B821DF7E5BBC87417BB7* L_14 = (StyleSelectorPartU5BU5D_tD8762DADE4104C7D73D7B821DF7E5BBC87417BB7*)__this->get_source_3();
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_14)->max_length))))))
		{
			goto IL_000b;
		}
	}
	{
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		VirtualActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<UnityEngine.InputSystem.Utilities.InternedString>::Dispose() */, (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
	}

IL_006b:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectArrayIterator`2<UnityEngine.UIElements.StyleSelectorPart,UnityEngine.InputSystem.Utilities.InternedString>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectArrayIterator_2_Where_mE699D323E72F58B0567ECEBFFCEF7F2E9C0713EB_gshared (WhereSelectArrayIterator_2_t0EBFFCD87F6BDF27E6B0AB944A8106A0C0ABC3AE * __this, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B * L_1 = (WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 7));
		((  void (*) (WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B *, RuntimeObject*, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectArrayIterator`2<UnityEngine.UIElements.StyleSelectorPart,System.Object>::.ctor(TSource[],System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectArrayIterator_2__ctor_m908D7DF1C7A1B8649C3B470043AFE7B67897E4BD_gshared (WhereSelectArrayIterator_2_t49E50A395D6004220E3A7ACD1AFFD129393B098E * __this, StyleSelectorPartU5BU5D_tD8762DADE4104C7D73D7B821DF7E5BBC87417BB7* ___source0, Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC * ___predicate1, Func_2_t43979C5A872BA9ED7A923D533A01683A98D71957 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		StyleSelectorPartU5BU5D_tD8762DADE4104C7D73D7B821DF7E5BBC87417BB7* L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t43979C5A872BA9ED7A923D533A01683A98D71957 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectArrayIterator`2<UnityEngine.UIElements.StyleSelectorPart,System.Object>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 * WhereSelectArrayIterator_2_Clone_m959B042A36E50FCE8540B1863C966C1622FB1219_gshared (WhereSelectArrayIterator_2_t49E50A395D6004220E3A7ACD1AFFD129393B098E * __this, const RuntimeMethod* method)
{
	{
		StyleSelectorPartU5BU5D_tD8762DADE4104C7D73D7B821DF7E5BBC87417BB7* L_0 = (StyleSelectorPartU5BU5D_tD8762DADE4104C7D73D7B821DF7E5BBC87417BB7*)__this->get_source_3();
		Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC * L_1 = (Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC *)__this->get_predicate_4();
		Func_2_t43979C5A872BA9ED7A923D533A01683A98D71957 * L_2 = (Func_2_t43979C5A872BA9ED7A923D533A01683A98D71957 *)__this->get_selector_5();
		WhereSelectArrayIterator_2_t49E50A395D6004220E3A7ACD1AFFD129393B098E * L_3 = (WhereSelectArrayIterator_2_t49E50A395D6004220E3A7ACD1AFFD129393B098E *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectArrayIterator_2_t49E50A395D6004220E3A7ACD1AFFD129393B098E *, StyleSelectorPartU5BU5D_tD8762DADE4104C7D73D7B821DF7E5BBC87417BB7*, Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC *, Func_2_t43979C5A872BA9ED7A923D533A01683A98D71957 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (StyleSelectorPartU5BU5D_tD8762DADE4104C7D73D7B821DF7E5BBC87417BB7*)L_0, (Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC *)L_1, (Func_2_t43979C5A872BA9ED7A923D533A01683A98D71957 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectArrayIterator`2<UnityEngine.UIElements.StyleSelectorPart,System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectArrayIterator_2_MoveNext_m2AA535DF25B4B83D4B63F80BD506F866DDDA7307_gshared (WhereSelectArrayIterator_2_t49E50A395D6004220E3A7ACD1AFFD129393B098E * __this, const RuntimeMethod* method)
{
	StyleSelectorPart_t707EDC970FC0F3E91E56DCBC178672A120426D54  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->get_state_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_006b;
		}
	}
	{
		goto IL_0055;
	}

IL_000b:
	{
		StyleSelectorPartU5BU5D_tD8762DADE4104C7D73D7B821DF7E5BBC87417BB7* L_1 = (StyleSelectorPartU5BU5D_tD8762DADE4104C7D73D7B821DF7E5BBC87417BB7*)__this->get_source_3();
		int32_t L_2 = (int32_t)__this->get_index_6();
		NullCheck(L_1);
		int32_t L_3 = L_2;
		StyleSelectorPart_t707EDC970FC0F3E91E56DCBC178672A120426D54  L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		V_0 = (StyleSelectorPart_t707EDC970FC0F3E91E56DCBC178672A120426D54 )L_4;
		int32_t L_5 = (int32_t)__this->get_index_6();
		__this->set_index_6(((int32_t)il2cpp_codegen_add((int32_t)L_5, (int32_t)1)));
		Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC * L_6 = (Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC *)__this->get_predicate_4();
		if (!L_6)
		{
			goto IL_0041;
		}
	}
	{
		Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC * L_7 = (Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC *)__this->get_predicate_4();
		StyleSelectorPart_t707EDC970FC0F3E91E56DCBC178672A120426D54  L_8 = V_0;
		NullCheck((Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC *)L_7);
		bool L_9;
		L_9 = ((  bool (*) (Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC *, StyleSelectorPart_t707EDC970FC0F3E91E56DCBC178672A120426D54 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC *)L_7, (StyleSelectorPart_t707EDC970FC0F3E91E56DCBC178672A120426D54 )L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		if (!L_9)
		{
			goto IL_0055;
		}
	}

IL_0041:
	{
		Func_2_t43979C5A872BA9ED7A923D533A01683A98D71957 * L_10 = (Func_2_t43979C5A872BA9ED7A923D533A01683A98D71957 *)__this->get_selector_5();
		StyleSelectorPart_t707EDC970FC0F3E91E56DCBC178672A120426D54  L_11 = V_0;
		NullCheck((Func_2_t43979C5A872BA9ED7A923D533A01683A98D71957 *)L_10);
		RuntimeObject * L_12;
		L_12 = ((  RuntimeObject * (*) (Func_2_t43979C5A872BA9ED7A923D533A01683A98D71957 *, StyleSelectorPart_t707EDC970FC0F3E91E56DCBC178672A120426D54 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5)->methodPointer)((Func_2_t43979C5A872BA9ED7A923D533A01683A98D71957 *)L_10, (StyleSelectorPart_t707EDC970FC0F3E91E56DCBC178672A120426D54 )L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_current_2(L_12);
		return (bool)1;
	}

IL_0055:
	{
		int32_t L_13 = (int32_t)__this->get_index_6();
		StyleSelectorPartU5BU5D_tD8762DADE4104C7D73D7B821DF7E5BBC87417BB7* L_14 = (StyleSelectorPartU5BU5D_tD8762DADE4104C7D73D7B821DF7E5BBC87417BB7*)__this->get_source_3();
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_14)->max_length))))))
		{
			goto IL_000b;
		}
	}
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		VirtualActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Object>::Dispose() */, (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
	}

IL_006b:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectArrayIterator`2<UnityEngine.UIElements.StyleSelectorPart,System.Object>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectArrayIterator_2_Where_mB857A340223EEBD5BD3D5032C7F1D6B2B9526CFD_gshared (WhereSelectArrayIterator_2_t49E50A395D6004220E3A7ACD1AFFD129393B098E * __this, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 * L_1 = (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 7));
		((  void (*) (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *, RuntimeObject*, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectArrayIterator`2<UnityEngine.InputSystem.Utilities.Substring,UnityEngine.InputSystem.Utilities.InternedString>::.ctor(TSource[],System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectArrayIterator_2__ctor_mD9F8618A7D9A41942F576B5649617BBF98AC4AEB_gshared (WhereSelectArrayIterator_2_t17AAB090C1BEA20967939542652510CB0E77A3FB * __this, SubstringU5BU5D_tE332884993470F5CF7BAAA950414E4E24691A8D0* ___source0, Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 * ___predicate1, Func_2_t14F6F4D7FBF558A20782EDC48D80473E4800048E * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		((  void (*) (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		SubstringU5BU5D_tE332884993470F5CF7BAAA950414E4E24691A8D0* L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t14F6F4D7FBF558A20782EDC48D80473E4800048E * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectArrayIterator`2<UnityEngine.InputSystem.Utilities.Substring,UnityEngine.InputSystem.Utilities.InternedString>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 * WhereSelectArrayIterator_2_Clone_mD7F1CDE8D5768E189E2BC8DD0445525D3F0F922C_gshared (WhereSelectArrayIterator_2_t17AAB090C1BEA20967939542652510CB0E77A3FB * __this, const RuntimeMethod* method)
{
	{
		SubstringU5BU5D_tE332884993470F5CF7BAAA950414E4E24691A8D0* L_0 = (SubstringU5BU5D_tE332884993470F5CF7BAAA950414E4E24691A8D0*)__this->get_source_3();
		Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 * L_1 = (Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 *)__this->get_predicate_4();
		Func_2_t14F6F4D7FBF558A20782EDC48D80473E4800048E * L_2 = (Func_2_t14F6F4D7FBF558A20782EDC48D80473E4800048E *)__this->get_selector_5();
		WhereSelectArrayIterator_2_t17AAB090C1BEA20967939542652510CB0E77A3FB * L_3 = (WhereSelectArrayIterator_2_t17AAB090C1BEA20967939542652510CB0E77A3FB *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectArrayIterator_2_t17AAB090C1BEA20967939542652510CB0E77A3FB *, SubstringU5BU5D_tE332884993470F5CF7BAAA950414E4E24691A8D0*, Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 *, Func_2_t14F6F4D7FBF558A20782EDC48D80473E4800048E *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (SubstringU5BU5D_tE332884993470F5CF7BAAA950414E4E24691A8D0*)L_0, (Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 *)L_1, (Func_2_t14F6F4D7FBF558A20782EDC48D80473E4800048E *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectArrayIterator`2<UnityEngine.InputSystem.Utilities.Substring,UnityEngine.InputSystem.Utilities.InternedString>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectArrayIterator_2_MoveNext_m22370BCFBB5F50C5BDD7F93DA2D67975FE15C831_gshared (WhereSelectArrayIterator_2_t17AAB090C1BEA20967939542652510CB0E77A3FB * __this, const RuntimeMethod* method)
{
	Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->get_state_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_006b;
		}
	}
	{
		goto IL_0055;
	}

IL_000b:
	{
		SubstringU5BU5D_tE332884993470F5CF7BAAA950414E4E24691A8D0* L_1 = (SubstringU5BU5D_tE332884993470F5CF7BAAA950414E4E24691A8D0*)__this->get_source_3();
		int32_t L_2 = (int32_t)__this->get_index_6();
		NullCheck(L_1);
		int32_t L_3 = L_2;
		Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F  L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		V_0 = (Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F )L_4;
		int32_t L_5 = (int32_t)__this->get_index_6();
		__this->set_index_6(((int32_t)il2cpp_codegen_add((int32_t)L_5, (int32_t)1)));
		Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 * L_6 = (Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 *)__this->get_predicate_4();
		if (!L_6)
		{
			goto IL_0041;
		}
	}
	{
		Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 * L_7 = (Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 *)__this->get_predicate_4();
		Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F  L_8 = V_0;
		NullCheck((Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 *)L_7);
		bool L_9;
		L_9 = ((  bool (*) (Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 *, Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 *)L_7, (Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F )L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		if (!L_9)
		{
			goto IL_0055;
		}
	}

IL_0041:
	{
		Func_2_t14F6F4D7FBF558A20782EDC48D80473E4800048E * L_10 = (Func_2_t14F6F4D7FBF558A20782EDC48D80473E4800048E *)__this->get_selector_5();
		Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F  L_11 = V_0;
		NullCheck((Func_2_t14F6F4D7FBF558A20782EDC48D80473E4800048E *)L_10);
		InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  L_12;
		L_12 = ((  InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  (*) (Func_2_t14F6F4D7FBF558A20782EDC48D80473E4800048E *, Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5)->methodPointer)((Func_2_t14F6F4D7FBF558A20782EDC48D80473E4800048E *)L_10, (Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F )L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->set_current_2(L_12);
		return (bool)1;
	}

IL_0055:
	{
		int32_t L_13 = (int32_t)__this->get_index_6();
		SubstringU5BU5D_tE332884993470F5CF7BAAA950414E4E24691A8D0* L_14 = (SubstringU5BU5D_tE332884993470F5CF7BAAA950414E4E24691A8D0*)__this->get_source_3();
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_14)->max_length))))))
		{
			goto IL_000b;
		}
	}
	{
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		VirtualActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<UnityEngine.InputSystem.Utilities.InternedString>::Dispose() */, (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
	}

IL_006b:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectArrayIterator`2<UnityEngine.InputSystem.Utilities.Substring,UnityEngine.InputSystem.Utilities.InternedString>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectArrayIterator_2_Where_m00155557806B6E2A78E0D9A64948E6CC5E0C3F67_gshared (WhereSelectArrayIterator_2_t17AAB090C1BEA20967939542652510CB0E77A3FB * __this, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B * L_1 = (WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 7));
		((  void (*) (WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B *, RuntimeObject*, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectArrayIterator`2<UnityEngine.InputSystem.Utilities.Substring,System.Object>::.ctor(TSource[],System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectArrayIterator_2__ctor_mE842B3DC6D3168B66D862440F01CBBEFFF9A0C1F_gshared (WhereSelectArrayIterator_2_tAA2E097EE3422A3A4E4652B975AA568F28CF2C90 * __this, SubstringU5BU5D_tE332884993470F5CF7BAAA950414E4E24691A8D0* ___source0, Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 * ___predicate1, Func_2_t4EC2F095EDDC8079537CA0FEDC463635E599C9E4 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		SubstringU5BU5D_tE332884993470F5CF7BAAA950414E4E24691A8D0* L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t4EC2F095EDDC8079537CA0FEDC463635E599C9E4 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectArrayIterator`2<UnityEngine.InputSystem.Utilities.Substring,System.Object>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 * WhereSelectArrayIterator_2_Clone_m1659F49B04A4502B3161905BA35F49C65EC94E9E_gshared (WhereSelectArrayIterator_2_tAA2E097EE3422A3A4E4652B975AA568F28CF2C90 * __this, const RuntimeMethod* method)
{
	{
		SubstringU5BU5D_tE332884993470F5CF7BAAA950414E4E24691A8D0* L_0 = (SubstringU5BU5D_tE332884993470F5CF7BAAA950414E4E24691A8D0*)__this->get_source_3();
		Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 * L_1 = (Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 *)__this->get_predicate_4();
		Func_2_t4EC2F095EDDC8079537CA0FEDC463635E599C9E4 * L_2 = (Func_2_t4EC2F095EDDC8079537CA0FEDC463635E599C9E4 *)__this->get_selector_5();
		WhereSelectArrayIterator_2_tAA2E097EE3422A3A4E4652B975AA568F28CF2C90 * L_3 = (WhereSelectArrayIterator_2_tAA2E097EE3422A3A4E4652B975AA568F28CF2C90 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectArrayIterator_2_tAA2E097EE3422A3A4E4652B975AA568F28CF2C90 *, SubstringU5BU5D_tE332884993470F5CF7BAAA950414E4E24691A8D0*, Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 *, Func_2_t4EC2F095EDDC8079537CA0FEDC463635E599C9E4 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (SubstringU5BU5D_tE332884993470F5CF7BAAA950414E4E24691A8D0*)L_0, (Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 *)L_1, (Func_2_t4EC2F095EDDC8079537CA0FEDC463635E599C9E4 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectArrayIterator`2<UnityEngine.InputSystem.Utilities.Substring,System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectArrayIterator_2_MoveNext_mA334DFF4E7F010BED5D068561DC1BB457DF56C04_gshared (WhereSelectArrayIterator_2_tAA2E097EE3422A3A4E4652B975AA568F28CF2C90 * __this, const RuntimeMethod* method)
{
	Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->get_state_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_006b;
		}
	}
	{
		goto IL_0055;
	}

IL_000b:
	{
		SubstringU5BU5D_tE332884993470F5CF7BAAA950414E4E24691A8D0* L_1 = (SubstringU5BU5D_tE332884993470F5CF7BAAA950414E4E24691A8D0*)__this->get_source_3();
		int32_t L_2 = (int32_t)__this->get_index_6();
		NullCheck(L_1);
		int32_t L_3 = L_2;
		Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F  L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		V_0 = (Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F )L_4;
		int32_t L_5 = (int32_t)__this->get_index_6();
		__this->set_index_6(((int32_t)il2cpp_codegen_add((int32_t)L_5, (int32_t)1)));
		Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 * L_6 = (Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 *)__this->get_predicate_4();
		if (!L_6)
		{
			goto IL_0041;
		}
	}
	{
		Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 * L_7 = (Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 *)__this->get_predicate_4();
		Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F  L_8 = V_0;
		NullCheck((Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 *)L_7);
		bool L_9;
		L_9 = ((  bool (*) (Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 *, Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 *)L_7, (Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F )L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		if (!L_9)
		{
			goto IL_0055;
		}
	}

IL_0041:
	{
		Func_2_t4EC2F095EDDC8079537CA0FEDC463635E599C9E4 * L_10 = (Func_2_t4EC2F095EDDC8079537CA0FEDC463635E599C9E4 *)__this->get_selector_5();
		Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F  L_11 = V_0;
		NullCheck((Func_2_t4EC2F095EDDC8079537CA0FEDC463635E599C9E4 *)L_10);
		RuntimeObject * L_12;
		L_12 = ((  RuntimeObject * (*) (Func_2_t4EC2F095EDDC8079537CA0FEDC463635E599C9E4 *, Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5)->methodPointer)((Func_2_t4EC2F095EDDC8079537CA0FEDC463635E599C9E4 *)L_10, (Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F )L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_current_2(L_12);
		return (bool)1;
	}

IL_0055:
	{
		int32_t L_13 = (int32_t)__this->get_index_6();
		SubstringU5BU5D_tE332884993470F5CF7BAAA950414E4E24691A8D0* L_14 = (SubstringU5BU5D_tE332884993470F5CF7BAAA950414E4E24691A8D0*)__this->get_source_3();
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_14)->max_length))))))
		{
			goto IL_000b;
		}
	}
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		VirtualActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Object>::Dispose() */, (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
	}

IL_006b:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectArrayIterator`2<UnityEngine.InputSystem.Utilities.Substring,System.Object>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectArrayIterator_2_Where_mAF007B0A1BC8B6AFEC926E498A1996850E589B5A_gshared (WhereSelectArrayIterator_2_tAA2E097EE3422A3A4E4652B975AA568F28CF2C90 * __this, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 * L_1 = (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 7));
		((  void (*) (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *, RuntimeObject*, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectArrayIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,UnityEngine.InputSystem.Utilities.InternedString>::.ctor(TSource[],System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectArrayIterator_2__ctor_mFBC9069732E020646C66806354E4C88EEA0EB6AD_gshared (WhereSelectArrayIterator_2_t0255B8DCC00DD4A15730FE65749D22B88FA338F1 * __this, JsonValueU5BU5D_t2E1CBAA099182C024E4921379907D707DA049098* ___source0, Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 * ___predicate1, Func_2_t48DD1C8E439811C492BAFE220A679DB57D7C9370 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		((  void (*) (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		JsonValueU5BU5D_t2E1CBAA099182C024E4921379907D707DA049098* L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t48DD1C8E439811C492BAFE220A679DB57D7C9370 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectArrayIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,UnityEngine.InputSystem.Utilities.InternedString>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 * WhereSelectArrayIterator_2_Clone_m0EBA158E2CE500AA35D6625F671E17A5ECB8F3B1_gshared (WhereSelectArrayIterator_2_t0255B8DCC00DD4A15730FE65749D22B88FA338F1 * __this, const RuntimeMethod* method)
{
	{
		JsonValueU5BU5D_t2E1CBAA099182C024E4921379907D707DA049098* L_0 = (JsonValueU5BU5D_t2E1CBAA099182C024E4921379907D707DA049098*)__this->get_source_3();
		Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 * L_1 = (Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 *)__this->get_predicate_4();
		Func_2_t48DD1C8E439811C492BAFE220A679DB57D7C9370 * L_2 = (Func_2_t48DD1C8E439811C492BAFE220A679DB57D7C9370 *)__this->get_selector_5();
		WhereSelectArrayIterator_2_t0255B8DCC00DD4A15730FE65749D22B88FA338F1 * L_3 = (WhereSelectArrayIterator_2_t0255B8DCC00DD4A15730FE65749D22B88FA338F1 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectArrayIterator_2_t0255B8DCC00DD4A15730FE65749D22B88FA338F1 *, JsonValueU5BU5D_t2E1CBAA099182C024E4921379907D707DA049098*, Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 *, Func_2_t48DD1C8E439811C492BAFE220A679DB57D7C9370 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (JsonValueU5BU5D_t2E1CBAA099182C024E4921379907D707DA049098*)L_0, (Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 *)L_1, (Func_2_t48DD1C8E439811C492BAFE220A679DB57D7C9370 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectArrayIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,UnityEngine.InputSystem.Utilities.InternedString>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectArrayIterator_2_MoveNext_m0E2F376B1DA9C8EB35A0481F7F42F9438E4255CB_gshared (WhereSelectArrayIterator_2_t0255B8DCC00DD4A15730FE65749D22B88FA338F1 * __this, const RuntimeMethod* method)
{
	JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->get_state_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_006b;
		}
	}
	{
		goto IL_0055;
	}

IL_000b:
	{
		JsonValueU5BU5D_t2E1CBAA099182C024E4921379907D707DA049098* L_1 = (JsonValueU5BU5D_t2E1CBAA099182C024E4921379907D707DA049098*)__this->get_source_3();
		int32_t L_2 = (int32_t)__this->get_index_6();
		NullCheck(L_1);
		int32_t L_3 = L_2;
		JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB  L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		V_0 = (JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB )L_4;
		int32_t L_5 = (int32_t)__this->get_index_6();
		__this->set_index_6(((int32_t)il2cpp_codegen_add((int32_t)L_5, (int32_t)1)));
		Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 * L_6 = (Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 *)__this->get_predicate_4();
		if (!L_6)
		{
			goto IL_0041;
		}
	}
	{
		Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 * L_7 = (Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 *)__this->get_predicate_4();
		JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB  L_8 = V_0;
		NullCheck((Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 *)L_7);
		bool L_9;
		L_9 = ((  bool (*) (Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 *, JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 *)L_7, (JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB )L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		if (!L_9)
		{
			goto IL_0055;
		}
	}

IL_0041:
	{
		Func_2_t48DD1C8E439811C492BAFE220A679DB57D7C9370 * L_10 = (Func_2_t48DD1C8E439811C492BAFE220A679DB57D7C9370 *)__this->get_selector_5();
		JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB  L_11 = V_0;
		NullCheck((Func_2_t48DD1C8E439811C492BAFE220A679DB57D7C9370 *)L_10);
		InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  L_12;
		L_12 = ((  InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  (*) (Func_2_t48DD1C8E439811C492BAFE220A679DB57D7C9370 *, JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5)->methodPointer)((Func_2_t48DD1C8E439811C492BAFE220A679DB57D7C9370 *)L_10, (JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB )L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->set_current_2(L_12);
		return (bool)1;
	}

IL_0055:
	{
		int32_t L_13 = (int32_t)__this->get_index_6();
		JsonValueU5BU5D_t2E1CBAA099182C024E4921379907D707DA049098* L_14 = (JsonValueU5BU5D_t2E1CBAA099182C024E4921379907D707DA049098*)__this->get_source_3();
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_14)->max_length))))))
		{
			goto IL_000b;
		}
	}
	{
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		VirtualActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<UnityEngine.InputSystem.Utilities.InternedString>::Dispose() */, (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
	}

IL_006b:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectArrayIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,UnityEngine.InputSystem.Utilities.InternedString>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectArrayIterator_2_Where_mCB7FD62BF7B39A44C2E989C98C14263CEDC4AE30_gshared (WhereSelectArrayIterator_2_t0255B8DCC00DD4A15730FE65749D22B88FA338F1 * __this, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B * L_1 = (WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 7));
		((  void (*) (WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B *, RuntimeObject*, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectArrayIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,System.Object>::.ctor(TSource[],System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectArrayIterator_2__ctor_m07F9E743C07E717257075B7E6FFAD3C79D562F8B_gshared (WhereSelectArrayIterator_2_tED9B99EA45685E4AB08C352736644EB4CAFC9980 * __this, JsonValueU5BU5D_t2E1CBAA099182C024E4921379907D707DA049098* ___source0, Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 * ___predicate1, Func_2_t1628B2E4EEC7A32D5B688E54415BC42EDA9D2899 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		JsonValueU5BU5D_t2E1CBAA099182C024E4921379907D707DA049098* L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t1628B2E4EEC7A32D5B688E54415BC42EDA9D2899 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectArrayIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,System.Object>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 * WhereSelectArrayIterator_2_Clone_mFF974CEAFA0F737914B7E1A949692EC17D291D62_gshared (WhereSelectArrayIterator_2_tED9B99EA45685E4AB08C352736644EB4CAFC9980 * __this, const RuntimeMethod* method)
{
	{
		JsonValueU5BU5D_t2E1CBAA099182C024E4921379907D707DA049098* L_0 = (JsonValueU5BU5D_t2E1CBAA099182C024E4921379907D707DA049098*)__this->get_source_3();
		Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 * L_1 = (Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 *)__this->get_predicate_4();
		Func_2_t1628B2E4EEC7A32D5B688E54415BC42EDA9D2899 * L_2 = (Func_2_t1628B2E4EEC7A32D5B688E54415BC42EDA9D2899 *)__this->get_selector_5();
		WhereSelectArrayIterator_2_tED9B99EA45685E4AB08C352736644EB4CAFC9980 * L_3 = (WhereSelectArrayIterator_2_tED9B99EA45685E4AB08C352736644EB4CAFC9980 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectArrayIterator_2_tED9B99EA45685E4AB08C352736644EB4CAFC9980 *, JsonValueU5BU5D_t2E1CBAA099182C024E4921379907D707DA049098*, Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 *, Func_2_t1628B2E4EEC7A32D5B688E54415BC42EDA9D2899 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (JsonValueU5BU5D_t2E1CBAA099182C024E4921379907D707DA049098*)L_0, (Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 *)L_1, (Func_2_t1628B2E4EEC7A32D5B688E54415BC42EDA9D2899 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectArrayIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectArrayIterator_2_MoveNext_m8E866C5E50314276FE2604B4C236D4D0ED0DE12E_gshared (WhereSelectArrayIterator_2_tED9B99EA45685E4AB08C352736644EB4CAFC9980 * __this, const RuntimeMethod* method)
{
	JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->get_state_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_006b;
		}
	}
	{
		goto IL_0055;
	}

IL_000b:
	{
		JsonValueU5BU5D_t2E1CBAA099182C024E4921379907D707DA049098* L_1 = (JsonValueU5BU5D_t2E1CBAA099182C024E4921379907D707DA049098*)__this->get_source_3();
		int32_t L_2 = (int32_t)__this->get_index_6();
		NullCheck(L_1);
		int32_t L_3 = L_2;
		JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB  L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		V_0 = (JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB )L_4;
		int32_t L_5 = (int32_t)__this->get_index_6();
		__this->set_index_6(((int32_t)il2cpp_codegen_add((int32_t)L_5, (int32_t)1)));
		Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 * L_6 = (Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 *)__this->get_predicate_4();
		if (!L_6)
		{
			goto IL_0041;
		}
	}
	{
		Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 * L_7 = (Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 *)__this->get_predicate_4();
		JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB  L_8 = V_0;
		NullCheck((Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 *)L_7);
		bool L_9;
		L_9 = ((  bool (*) (Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 *, JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 *)L_7, (JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB )L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		if (!L_9)
		{
			goto IL_0055;
		}
	}

IL_0041:
	{
		Func_2_t1628B2E4EEC7A32D5B688E54415BC42EDA9D2899 * L_10 = (Func_2_t1628B2E4EEC7A32D5B688E54415BC42EDA9D2899 *)__this->get_selector_5();
		JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB  L_11 = V_0;
		NullCheck((Func_2_t1628B2E4EEC7A32D5B688E54415BC42EDA9D2899 *)L_10);
		RuntimeObject * L_12;
		L_12 = ((  RuntimeObject * (*) (Func_2_t1628B2E4EEC7A32D5B688E54415BC42EDA9D2899 *, JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5)->methodPointer)((Func_2_t1628B2E4EEC7A32D5B688E54415BC42EDA9D2899 *)L_10, (JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB )L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_current_2(L_12);
		return (bool)1;
	}

IL_0055:
	{
		int32_t L_13 = (int32_t)__this->get_index_6();
		JsonValueU5BU5D_t2E1CBAA099182C024E4921379907D707DA049098* L_14 = (JsonValueU5BU5D_t2E1CBAA099182C024E4921379907D707DA049098*)__this->get_source_3();
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_14)->max_length))))))
		{
			goto IL_000b;
		}
	}
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		VirtualActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Object>::Dispose() */, (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
	}

IL_006b:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectArrayIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,System.Object>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectArrayIterator_2_Where_mB0FF8DB561C91E578DD18C5A7EC439F6BCDE7263_gshared (WhereSelectArrayIterator_2_tED9B99EA45685E4AB08C352736644EB4CAFC9980 * __this, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 * L_1 = (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 7));
		((  void (*) (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *, RuntimeObject*, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,UnityEngine.InputSystem.Utilities.InternedString>::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2__ctor_mA02F86F07B05F6975C732878E7D65801302D07CA_gshared (WhereSelectEnumerableIterator_2_t5171F478E1C976F4C353EA25EC64567E44C86CA0 * __this, RuntimeObject* ___source0, Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 * ___predicate1, Func_2_tB181F4C9A24C0634DF4A10CFBFFE6B45B3796BBC * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		((  void (*) (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		RuntimeObject* L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_tB181F4C9A24C0634DF4A10CFBFFE6B45B3796BBC * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,UnityEngine.InputSystem.Utilities.InternedString>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 * WhereSelectEnumerableIterator_2_Clone_mE7EF2621601BBD49B5C822372D36A8FB867ACE2A_gshared (WhereSelectEnumerableIterator_2_t5171F478E1C976F4C353EA25EC64567E44C86CA0 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_source_3();
		Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 * L_1 = (Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 *)__this->get_predicate_4();
		Func_2_tB181F4C9A24C0634DF4A10CFBFFE6B45B3796BBC * L_2 = (Func_2_tB181F4C9A24C0634DF4A10CFBFFE6B45B3796BBC *)__this->get_selector_5();
		WhereSelectEnumerableIterator_2_t5171F478E1C976F4C353EA25EC64567E44C86CA0 * L_3 = (WhereSelectEnumerableIterator_2_t5171F478E1C976F4C353EA25EC64567E44C86CA0 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectEnumerableIterator_2_t5171F478E1C976F4C353EA25EC64567E44C86CA0 *, RuntimeObject*, Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 *, Func_2_tB181F4C9A24C0634DF4A10CFBFFE6B45B3796BBC *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (RuntimeObject*)L_0, (Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 *)L_1, (Func_2_tB181F4C9A24C0634DF4A10CFBFFE6B45B3796BBC *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)L_3;
	}
}
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,UnityEngine.InputSystem.Utilities.InternedString>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2_Dispose_mE75D949168D88B07AA3F34D49CC94AB44AE57674_gshared (WhereSelectEnumerableIterator_2_t5171F478E1C976F4C353EA25EC64567E44C86CA0 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_enumerator_6();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		RuntimeObject* L_1 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, (RuntimeObject*)L_1);
	}

IL_0013:
	{
		__this->set_enumerator_6((RuntimeObject*)NULL);
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		((  void (*) (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		return;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,UnityEngine.InputSystem.Utilities.InternedString>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectEnumerableIterator_2_MoveNext_m5692381D325B5682E63435202054A5169B54AFB6_gshared (WhereSelectEnumerableIterator_2_t5171F478E1C976F4C353EA25EC64567E44C86CA0 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		RuntimeObject* L_3 = (RuntimeObject*)__this->get_source_3();
		NullCheck((RuntimeObject*)L_3);
		RuntimeObject* L_4;
		L_4 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 5), (RuntimeObject*)L_3);
		__this->set_enumerator_6(L_4);
		((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		RuntimeObject* L_5 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_5);
		KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184  L_6;
		L_6 = InterfaceFuncInvoker0< KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>>::get_Current() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 6), (RuntimeObject*)L_5);
		V_1 = (KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184 )L_6;
		Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 * L_7 = (Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 * L_8 = (Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 *)__this->get_predicate_4();
		KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184  L_9 = V_1;
		NullCheck((Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 *, KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 *)L_8, (KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_tB181F4C9A24C0634DF4A10CFBFFE6B45B3796BBC * L_11 = (Func_2_tB181F4C9A24C0634DF4A10CFBFFE6B45B3796BBC *)__this->get_selector_5();
		KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184  L_12 = V_1;
		NullCheck((Func_2_tB181F4C9A24C0634DF4A10CFBFFE6B45B3796BBC *)L_11);
		InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  L_13;
		L_13 = ((  InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  (*) (Func_2_tB181F4C9A24C0634DF4A10CFBFFE6B45B3796BBC *, KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((Func_2_tB181F4C9A24C0634DF4A10CFBFFE6B45B3796BBC *)L_11, (KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184 )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		RuntimeObject* L_14 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_14);
		bool L_15;
		L_15 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, (RuntimeObject*)L_14);
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		VirtualActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<UnityEngine.InputSystem.Utilities.InternedString>::Dispose() */, (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,UnityEngine.InputSystem.Utilities.InternedString>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectEnumerableIterator_2_Where_m35C424673E2F0F4867052BFB52B35C8E1B94C866_gshared (WhereSelectEnumerableIterator_2_t5171F478E1C976F4C353EA25EC64567E44C86CA0 * __this, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B * L_1 = (WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 9));
		((  void (*) (WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B *, RuntimeObject*, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,System.Object>::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2__ctor_m866FE8D0C3F7896255E1B020ABA31779A9E852B6_gshared (WhereSelectEnumerableIterator_2_t0ACED5698603E2D2B4071096A021E0FA6102F459 * __this, RuntimeObject* ___source0, Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 * ___predicate1, Func_2_t711512467B454C91EB8F3FB19CD6D141AAB5B6B1 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		RuntimeObject* L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t711512467B454C91EB8F3FB19CD6D141AAB5B6B1 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,System.Object>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 * WhereSelectEnumerableIterator_2_Clone_mC5EA9A6B1EB6880263BC4E27D7C4F5831C5D60A3_gshared (WhereSelectEnumerableIterator_2_t0ACED5698603E2D2B4071096A021E0FA6102F459 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_source_3();
		Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 * L_1 = (Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 *)__this->get_predicate_4();
		Func_2_t711512467B454C91EB8F3FB19CD6D141AAB5B6B1 * L_2 = (Func_2_t711512467B454C91EB8F3FB19CD6D141AAB5B6B1 *)__this->get_selector_5();
		WhereSelectEnumerableIterator_2_t0ACED5698603E2D2B4071096A021E0FA6102F459 * L_3 = (WhereSelectEnumerableIterator_2_t0ACED5698603E2D2B4071096A021E0FA6102F459 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectEnumerableIterator_2_t0ACED5698603E2D2B4071096A021E0FA6102F459 *, RuntimeObject*, Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 *, Func_2_t711512467B454C91EB8F3FB19CD6D141AAB5B6B1 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (RuntimeObject*)L_0, (Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 *)L_1, (Func_2_t711512467B454C91EB8F3FB19CD6D141AAB5B6B1 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)L_3;
	}
}
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,System.Object>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2_Dispose_m9D3DD396CB8ED1F29264B395F3E371FFD2AA382F_gshared (WhereSelectEnumerableIterator_2_t0ACED5698603E2D2B4071096A021E0FA6102F459 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_enumerator_6();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		RuntimeObject* L_1 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, (RuntimeObject*)L_1);
	}

IL_0013:
	{
		__this->set_enumerator_6((RuntimeObject*)NULL);
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		return;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectEnumerableIterator_2_MoveNext_mEE8CA6C28994E2E78429899A99B8EB3C35A52E6E_gshared (WhereSelectEnumerableIterator_2_t0ACED5698603E2D2B4071096A021E0FA6102F459 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		RuntimeObject* L_3 = (RuntimeObject*)__this->get_source_3();
		NullCheck((RuntimeObject*)L_3);
		RuntimeObject* L_4;
		L_4 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 5), (RuntimeObject*)L_3);
		__this->set_enumerator_6(L_4);
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		RuntimeObject* L_5 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_5);
		KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184  L_6;
		L_6 = InterfaceFuncInvoker0< KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>>::get_Current() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 6), (RuntimeObject*)L_5);
		V_1 = (KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184 )L_6;
		Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 * L_7 = (Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 * L_8 = (Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 *)__this->get_predicate_4();
		KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184  L_9 = V_1;
		NullCheck((Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 *, KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 *)L_8, (KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t711512467B454C91EB8F3FB19CD6D141AAB5B6B1 * L_11 = (Func_2_t711512467B454C91EB8F3FB19CD6D141AAB5B6B1 *)__this->get_selector_5();
		KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184  L_12 = V_1;
		NullCheck((Func_2_t711512467B454C91EB8F3FB19CD6D141AAB5B6B1 *)L_11);
		RuntimeObject * L_13;
		L_13 = ((  RuntimeObject * (*) (Func_2_t711512467B454C91EB8F3FB19CD6D141AAB5B6B1 *, KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((Func_2_t711512467B454C91EB8F3FB19CD6D141AAB5B6B1 *)L_11, (KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184 )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		RuntimeObject* L_14 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_14);
		bool L_15;
		L_15 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, (RuntimeObject*)L_14);
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		VirtualActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Object>::Dispose() */, (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,System.Object>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectEnumerableIterator_2_Where_mAB6244C19F35C0497A9BAC697E0B0E119F57EC29_gshared (WhereSelectEnumerableIterator_2_t0ACED5698603E2D2B4071096A021E0FA6102F459 * __this, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 * L_1 = (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 9));
		((  void (*) (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *, RuntimeObject*, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.InternedString,UnityEngine.InputSystem.Utilities.InternedString>::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2__ctor_m54253502FA38BA0842892E4AA45343AF2BD172CD_gshared (WhereSelectEnumerableIterator_2_t0EB79FD57DE6939EBF78FA1D92312CB62DD3F2C6 * __this, RuntimeObject* ___source0, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * ___predicate1, Func_2_t1AF155D27A4D78882342D697E4A6DE7CE594545B * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		((  void (*) (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		RuntimeObject* L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t1AF155D27A4D78882342D697E4A6DE7CE594545B * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.InternedString,UnityEngine.InputSystem.Utilities.InternedString>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 * WhereSelectEnumerableIterator_2_Clone_mB3F69EC75E14BC525DD6F13660F89422DBE518FF_gshared (WhereSelectEnumerableIterator_2_t0EB79FD57DE6939EBF78FA1D92312CB62DD3F2C6 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_source_3();
		Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * L_1 = (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)__this->get_predicate_4();
		Func_2_t1AF155D27A4D78882342D697E4A6DE7CE594545B * L_2 = (Func_2_t1AF155D27A4D78882342D697E4A6DE7CE594545B *)__this->get_selector_5();
		WhereSelectEnumerableIterator_2_t0EB79FD57DE6939EBF78FA1D92312CB62DD3F2C6 * L_3 = (WhereSelectEnumerableIterator_2_t0EB79FD57DE6939EBF78FA1D92312CB62DD3F2C6 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectEnumerableIterator_2_t0EB79FD57DE6939EBF78FA1D92312CB62DD3F2C6 *, RuntimeObject*, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *, Func_2_t1AF155D27A4D78882342D697E4A6DE7CE594545B *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (RuntimeObject*)L_0, (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)L_1, (Func_2_t1AF155D27A4D78882342D697E4A6DE7CE594545B *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)L_3;
	}
}
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.InternedString,UnityEngine.InputSystem.Utilities.InternedString>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2_Dispose_mBE8FA402107BBA57A0E4E27EAF99DA825506A209_gshared (WhereSelectEnumerableIterator_2_t0EB79FD57DE6939EBF78FA1D92312CB62DD3F2C6 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_enumerator_6();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		RuntimeObject* L_1 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, (RuntimeObject*)L_1);
	}

IL_0013:
	{
		__this->set_enumerator_6((RuntimeObject*)NULL);
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		((  void (*) (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		return;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.InternedString,UnityEngine.InputSystem.Utilities.InternedString>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectEnumerableIterator_2_MoveNext_m63C0EC5F395EF1117EF74EC428A2B114BF889A6A_gshared (WhereSelectEnumerableIterator_2_t0EB79FD57DE6939EBF78FA1D92312CB62DD3F2C6 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		RuntimeObject* L_3 = (RuntimeObject*)__this->get_source_3();
		NullCheck((RuntimeObject*)L_3);
		RuntimeObject* L_4;
		L_4 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<UnityEngine.InputSystem.Utilities.InternedString>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 5), (RuntimeObject*)L_3);
		__this->set_enumerator_6(L_4);
		((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		RuntimeObject* L_5 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_5);
		InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  L_6;
		L_6 = InterfaceFuncInvoker0< InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<UnityEngine.InputSystem.Utilities.InternedString>::get_Current() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 6), (RuntimeObject*)L_5);
		V_1 = (InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED )L_6;
		Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * L_7 = (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * L_8 = (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)__this->get_predicate_4();
		InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  L_9 = V_1;
		NullCheck((Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *, InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)L_8, (InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t1AF155D27A4D78882342D697E4A6DE7CE594545B * L_11 = (Func_2_t1AF155D27A4D78882342D697E4A6DE7CE594545B *)__this->get_selector_5();
		InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  L_12 = V_1;
		NullCheck((Func_2_t1AF155D27A4D78882342D697E4A6DE7CE594545B *)L_11);
		InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  L_13;
		L_13 = ((  InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  (*) (Func_2_t1AF155D27A4D78882342D697E4A6DE7CE594545B *, InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((Func_2_t1AF155D27A4D78882342D697E4A6DE7CE594545B *)L_11, (InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		RuntimeObject* L_14 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_14);
		bool L_15;
		L_15 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, (RuntimeObject*)L_14);
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		VirtualActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<UnityEngine.InputSystem.Utilities.InternedString>::Dispose() */, (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.InternedString,UnityEngine.InputSystem.Utilities.InternedString>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectEnumerableIterator_2_Where_mF1B8498DED0156DD86B2ECA469FB59F63C171CE7_gshared (WhereSelectEnumerableIterator_2_t0EB79FD57DE6939EBF78FA1D92312CB62DD3F2C6 * __this, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B * L_1 = (WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 9));
		((  void (*) (WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B *, RuntimeObject*, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.InternedString,System.Object>::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2__ctor_mC9A64D4FB30CB23B7A074C8EE5B867A1DE3F5120_gshared (WhereSelectEnumerableIterator_2_t02B3FDD3CA4282B70A77A6A52D6AADC70EF74E8B * __this, RuntimeObject* ___source0, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * ___predicate1, Func_2_tA2BD34BB8D97AD282016BD4CB5B8675E4AF85B45 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		RuntimeObject* L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_tA2BD34BB8D97AD282016BD4CB5B8675E4AF85B45 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.InternedString,System.Object>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 * WhereSelectEnumerableIterator_2_Clone_m8F234DF95B087D6685C1DDDBD9BC1F9755073F73_gshared (WhereSelectEnumerableIterator_2_t02B3FDD3CA4282B70A77A6A52D6AADC70EF74E8B * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_source_3();
		Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * L_1 = (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)__this->get_predicate_4();
		Func_2_tA2BD34BB8D97AD282016BD4CB5B8675E4AF85B45 * L_2 = (Func_2_tA2BD34BB8D97AD282016BD4CB5B8675E4AF85B45 *)__this->get_selector_5();
		WhereSelectEnumerableIterator_2_t02B3FDD3CA4282B70A77A6A52D6AADC70EF74E8B * L_3 = (WhereSelectEnumerableIterator_2_t02B3FDD3CA4282B70A77A6A52D6AADC70EF74E8B *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectEnumerableIterator_2_t02B3FDD3CA4282B70A77A6A52D6AADC70EF74E8B *, RuntimeObject*, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *, Func_2_tA2BD34BB8D97AD282016BD4CB5B8675E4AF85B45 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (RuntimeObject*)L_0, (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)L_1, (Func_2_tA2BD34BB8D97AD282016BD4CB5B8675E4AF85B45 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)L_3;
	}
}
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.InternedString,System.Object>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2_Dispose_m409DA5CCF563EBBA46DBCDA4F89CC00A7609B963_gshared (WhereSelectEnumerableIterator_2_t02B3FDD3CA4282B70A77A6A52D6AADC70EF74E8B * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_enumerator_6();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		RuntimeObject* L_1 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, (RuntimeObject*)L_1);
	}

IL_0013:
	{
		__this->set_enumerator_6((RuntimeObject*)NULL);
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		return;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.InternedString,System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectEnumerableIterator_2_MoveNext_m9ECF3AF97756B1A4D98D95DAF9A652824D5306E3_gshared (WhereSelectEnumerableIterator_2_t02B3FDD3CA4282B70A77A6A52D6AADC70EF74E8B * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		RuntimeObject* L_3 = (RuntimeObject*)__this->get_source_3();
		NullCheck((RuntimeObject*)L_3);
		RuntimeObject* L_4;
		L_4 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<UnityEngine.InputSystem.Utilities.InternedString>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 5), (RuntimeObject*)L_3);
		__this->set_enumerator_6(L_4);
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		RuntimeObject* L_5 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_5);
		InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  L_6;
		L_6 = InterfaceFuncInvoker0< InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<UnityEngine.InputSystem.Utilities.InternedString>::get_Current() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 6), (RuntimeObject*)L_5);
		V_1 = (InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED )L_6;
		Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * L_7 = (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * L_8 = (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)__this->get_predicate_4();
		InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  L_9 = V_1;
		NullCheck((Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *, InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)L_8, (InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_tA2BD34BB8D97AD282016BD4CB5B8675E4AF85B45 * L_11 = (Func_2_tA2BD34BB8D97AD282016BD4CB5B8675E4AF85B45 *)__this->get_selector_5();
		InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  L_12 = V_1;
		NullCheck((Func_2_tA2BD34BB8D97AD282016BD4CB5B8675E4AF85B45 *)L_11);
		RuntimeObject * L_13;
		L_13 = ((  RuntimeObject * (*) (Func_2_tA2BD34BB8D97AD282016BD4CB5B8675E4AF85B45 *, InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((Func_2_tA2BD34BB8D97AD282016BD4CB5B8675E4AF85B45 *)L_11, (InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		RuntimeObject* L_14 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_14);
		bool L_15;
		L_15 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, (RuntimeObject*)L_14);
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		VirtualActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Object>::Dispose() */, (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.InternedString,System.Object>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectEnumerableIterator_2_Where_mF7F0991528F8AA9A7AE8A97C4D2A0C2335784CCF_gshared (WhereSelectEnumerableIterator_2_t02B3FDD3CA4282B70A77A6A52D6AADC70EF74E8B * __this, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 * L_1 = (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 9));
		((  void (*) (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *, RuntimeObject*, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,UnityEngine.InputSystem.Utilities.InternedString>::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2__ctor_m0421971BFCEAEF4C2EC4CB749C40E88C27C96B57_gshared (WhereSelectEnumerableIterator_2_t3B8EA5ED092FA87DEAECF94C466A54501B692B0B * __this, RuntimeObject* ___source0, Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 * ___predicate1, Func_2_tEA2235809AB0E1AB8425745179691D76B519B0C5 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		((  void (*) (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		RuntimeObject* L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_tEA2235809AB0E1AB8425745179691D76B519B0C5 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,UnityEngine.InputSystem.Utilities.InternedString>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 * WhereSelectEnumerableIterator_2_Clone_mD2FAE2C212DD2EF5E462B380AF331E301454B286_gshared (WhereSelectEnumerableIterator_2_t3B8EA5ED092FA87DEAECF94C466A54501B692B0B * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_source_3();
		Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 * L_1 = (Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 *)__this->get_predicate_4();
		Func_2_tEA2235809AB0E1AB8425745179691D76B519B0C5 * L_2 = (Func_2_tEA2235809AB0E1AB8425745179691D76B519B0C5 *)__this->get_selector_5();
		WhereSelectEnumerableIterator_2_t3B8EA5ED092FA87DEAECF94C466A54501B692B0B * L_3 = (WhereSelectEnumerableIterator_2_t3B8EA5ED092FA87DEAECF94C466A54501B692B0B *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectEnumerableIterator_2_t3B8EA5ED092FA87DEAECF94C466A54501B692B0B *, RuntimeObject*, Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 *, Func_2_tEA2235809AB0E1AB8425745179691D76B519B0C5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (RuntimeObject*)L_0, (Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 *)L_1, (Func_2_tEA2235809AB0E1AB8425745179691D76B519B0C5 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)L_3;
	}
}
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,UnityEngine.InputSystem.Utilities.InternedString>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2_Dispose_m636EAFD5143B6B31E8E967BE4C441F29FACDE9F9_gshared (WhereSelectEnumerableIterator_2_t3B8EA5ED092FA87DEAECF94C466A54501B692B0B * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_enumerator_6();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		RuntimeObject* L_1 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, (RuntimeObject*)L_1);
	}

IL_0013:
	{
		__this->set_enumerator_6((RuntimeObject*)NULL);
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		((  void (*) (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		return;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,UnityEngine.InputSystem.Utilities.InternedString>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectEnumerableIterator_2_MoveNext_m14F6C6C3DAE3AFC31E57F39E6B7258CD45E393D1_gshared (WhereSelectEnumerableIterator_2_t3B8EA5ED092FA87DEAECF94C466A54501B692B0B * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		RuntimeObject* L_3 = (RuntimeObject*)__this->get_source_3();
		NullCheck((RuntimeObject*)L_3);
		RuntimeObject* L_4;
		L_4 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<UnityEngine.InputSystem.Utilities.NameAndParameters>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 5), (RuntimeObject*)L_3);
		__this->set_enumerator_6(L_4);
		((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		RuntimeObject* L_5 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_5);
		NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA  L_6;
		L_6 = InterfaceFuncInvoker0< NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<UnityEngine.InputSystem.Utilities.NameAndParameters>::get_Current() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 6), (RuntimeObject*)L_5);
		V_1 = (NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA )L_6;
		Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 * L_7 = (Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 * L_8 = (Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 *)__this->get_predicate_4();
		NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA  L_9 = V_1;
		NullCheck((Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 *, NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 *)L_8, (NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_tEA2235809AB0E1AB8425745179691D76B519B0C5 * L_11 = (Func_2_tEA2235809AB0E1AB8425745179691D76B519B0C5 *)__this->get_selector_5();
		NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA  L_12 = V_1;
		NullCheck((Func_2_tEA2235809AB0E1AB8425745179691D76B519B0C5 *)L_11);
		InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  L_13;
		L_13 = ((  InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  (*) (Func_2_tEA2235809AB0E1AB8425745179691D76B519B0C5 *, NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((Func_2_tEA2235809AB0E1AB8425745179691D76B519B0C5 *)L_11, (NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		RuntimeObject* L_14 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_14);
		bool L_15;
		L_15 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, (RuntimeObject*)L_14);
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		VirtualActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<UnityEngine.InputSystem.Utilities.InternedString>::Dispose() */, (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,UnityEngine.InputSystem.Utilities.InternedString>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectEnumerableIterator_2_Where_m73557682E74398A2A1E977211F4BCD3AF4837A4F_gshared (WhereSelectEnumerableIterator_2_t3B8EA5ED092FA87DEAECF94C466A54501B692B0B * __this, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B * L_1 = (WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 9));
		((  void (*) (WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B *, RuntimeObject*, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,System.Object>::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2__ctor_mE8DDA462D57D4C2DF8D90EB842955DF8BFC2555F_gshared (WhereSelectEnumerableIterator_2_t9C8232C608C379652355BB860B0A6074ABF36E09 * __this, RuntimeObject* ___source0, Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 * ___predicate1, Func_2_tF7304610FB007335AC870B51C8FE2092628DC0DB * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		RuntimeObject* L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_tF7304610FB007335AC870B51C8FE2092628DC0DB * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,System.Object>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 * WhereSelectEnumerableIterator_2_Clone_m61D66A6C637A29125687D57C115CE11D506B36FD_gshared (WhereSelectEnumerableIterator_2_t9C8232C608C379652355BB860B0A6074ABF36E09 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_source_3();
		Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 * L_1 = (Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 *)__this->get_predicate_4();
		Func_2_tF7304610FB007335AC870B51C8FE2092628DC0DB * L_2 = (Func_2_tF7304610FB007335AC870B51C8FE2092628DC0DB *)__this->get_selector_5();
		WhereSelectEnumerableIterator_2_t9C8232C608C379652355BB860B0A6074ABF36E09 * L_3 = (WhereSelectEnumerableIterator_2_t9C8232C608C379652355BB860B0A6074ABF36E09 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectEnumerableIterator_2_t9C8232C608C379652355BB860B0A6074ABF36E09 *, RuntimeObject*, Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 *, Func_2_tF7304610FB007335AC870B51C8FE2092628DC0DB *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (RuntimeObject*)L_0, (Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 *)L_1, (Func_2_tF7304610FB007335AC870B51C8FE2092628DC0DB *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)L_3;
	}
}
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,System.Object>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2_Dispose_m736AE762C4A83A5BD995F757275214ADD2DDF3DA_gshared (WhereSelectEnumerableIterator_2_t9C8232C608C379652355BB860B0A6074ABF36E09 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_enumerator_6();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		RuntimeObject* L_1 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, (RuntimeObject*)L_1);
	}

IL_0013:
	{
		__this->set_enumerator_6((RuntimeObject*)NULL);
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		return;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectEnumerableIterator_2_MoveNext_mE572A528E5B2307AF20CA76CF260D192CE310BFD_gshared (WhereSelectEnumerableIterator_2_t9C8232C608C379652355BB860B0A6074ABF36E09 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		RuntimeObject* L_3 = (RuntimeObject*)__this->get_source_3();
		NullCheck((RuntimeObject*)L_3);
		RuntimeObject* L_4;
		L_4 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<UnityEngine.InputSystem.Utilities.NameAndParameters>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 5), (RuntimeObject*)L_3);
		__this->set_enumerator_6(L_4);
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		RuntimeObject* L_5 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_5);
		NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA  L_6;
		L_6 = InterfaceFuncInvoker0< NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<UnityEngine.InputSystem.Utilities.NameAndParameters>::get_Current() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 6), (RuntimeObject*)L_5);
		V_1 = (NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA )L_6;
		Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 * L_7 = (Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 * L_8 = (Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 *)__this->get_predicate_4();
		NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA  L_9 = V_1;
		NullCheck((Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 *, NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 *)L_8, (NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_tF7304610FB007335AC870B51C8FE2092628DC0DB * L_11 = (Func_2_tF7304610FB007335AC870B51C8FE2092628DC0DB *)__this->get_selector_5();
		NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA  L_12 = V_1;
		NullCheck((Func_2_tF7304610FB007335AC870B51C8FE2092628DC0DB *)L_11);
		RuntimeObject * L_13;
		L_13 = ((  RuntimeObject * (*) (Func_2_tF7304610FB007335AC870B51C8FE2092628DC0DB *, NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((Func_2_tF7304610FB007335AC870B51C8FE2092628DC0DB *)L_11, (NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		RuntimeObject* L_14 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_14);
		bool L_15;
		L_15 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, (RuntimeObject*)L_14);
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		VirtualActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Object>::Dispose() */, (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,System.Object>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectEnumerableIterator_2_Where_m9D605C401F4030917F57206D4099B64AC44FFCA4_gshared (WhereSelectEnumerableIterator_2_t9C8232C608C379652355BB860B0A6074ABF36E09 * __this, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 * L_1 = (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 9));
		((  void (*) (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *, RuntimeObject*, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,UnityEngine.InputSystem.Utilities.InternedString>::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2__ctor_mA18AC538E2E5E8E5D5118D09C87D06C65F3D1E95_gshared (WhereSelectEnumerableIterator_2_t382E0095C883AA01020D10E08F7F38BBA26A3EC5 * __this, RuntimeObject* ___source0, Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 * ___predicate1, Func_2_tA21678F1D9F9F4617D645755285D28EDE5F9AE44 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		((  void (*) (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		RuntimeObject* L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_tA21678F1D9F9F4617D645755285D28EDE5F9AE44 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,UnityEngine.InputSystem.Utilities.InternedString>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 * WhereSelectEnumerableIterator_2_Clone_m9AC3E963283F39C7E4E0F1E223ADAC3A9222B6CD_gshared (WhereSelectEnumerableIterator_2_t382E0095C883AA01020D10E08F7F38BBA26A3EC5 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_source_3();
		Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 * L_1 = (Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 *)__this->get_predicate_4();
		Func_2_tA21678F1D9F9F4617D645755285D28EDE5F9AE44 * L_2 = (Func_2_tA21678F1D9F9F4617D645755285D28EDE5F9AE44 *)__this->get_selector_5();
		WhereSelectEnumerableIterator_2_t382E0095C883AA01020D10E08F7F38BBA26A3EC5 * L_3 = (WhereSelectEnumerableIterator_2_t382E0095C883AA01020D10E08F7F38BBA26A3EC5 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectEnumerableIterator_2_t382E0095C883AA01020D10E08F7F38BBA26A3EC5 *, RuntimeObject*, Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 *, Func_2_tA21678F1D9F9F4617D645755285D28EDE5F9AE44 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (RuntimeObject*)L_0, (Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 *)L_1, (Func_2_tA21678F1D9F9F4617D645755285D28EDE5F9AE44 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)L_3;
	}
}
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,UnityEngine.InputSystem.Utilities.InternedString>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2_Dispose_m60E0BB66B459009D14D056C5503EA295B3F8A593_gshared (WhereSelectEnumerableIterator_2_t382E0095C883AA01020D10E08F7F38BBA26A3EC5 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_enumerator_6();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		RuntimeObject* L_1 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, (RuntimeObject*)L_1);
	}

IL_0013:
	{
		__this->set_enumerator_6((RuntimeObject*)NULL);
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		((  void (*) (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		return;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,UnityEngine.InputSystem.Utilities.InternedString>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectEnumerableIterator_2_MoveNext_m0AF7CC8342FBF822F9CCE844742A3DD50D1E7955_gshared (WhereSelectEnumerableIterator_2_t382E0095C883AA01020D10E08F7F38BBA26A3EC5 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		RuntimeObject* L_3 = (RuntimeObject*)__this->get_source_3();
		NullCheck((RuntimeObject*)L_3);
		RuntimeObject* L_4;
		L_4 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<UnityEngine.InputSystem.Utilities.NamedValue>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 5), (RuntimeObject*)L_3);
		__this->set_enumerator_6(L_4);
		((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		RuntimeObject* L_5 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_5);
		NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077  L_6;
		L_6 = InterfaceFuncInvoker0< NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<UnityEngine.InputSystem.Utilities.NamedValue>::get_Current() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 6), (RuntimeObject*)L_5);
		V_1 = (NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077 )L_6;
		Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 * L_7 = (Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 * L_8 = (Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 *)__this->get_predicate_4();
		NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077  L_9 = V_1;
		NullCheck((Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 *, NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 *)L_8, (NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_tA21678F1D9F9F4617D645755285D28EDE5F9AE44 * L_11 = (Func_2_tA21678F1D9F9F4617D645755285D28EDE5F9AE44 *)__this->get_selector_5();
		NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077  L_12 = V_1;
		NullCheck((Func_2_tA21678F1D9F9F4617D645755285D28EDE5F9AE44 *)L_11);
		InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  L_13;
		L_13 = ((  InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  (*) (Func_2_tA21678F1D9F9F4617D645755285D28EDE5F9AE44 *, NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((Func_2_tA21678F1D9F9F4617D645755285D28EDE5F9AE44 *)L_11, (NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077 )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		RuntimeObject* L_14 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_14);
		bool L_15;
		L_15 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, (RuntimeObject*)L_14);
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		VirtualActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<UnityEngine.InputSystem.Utilities.InternedString>::Dispose() */, (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,UnityEngine.InputSystem.Utilities.InternedString>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectEnumerableIterator_2_Where_m089E976F91886F10297CA030CCFFDCE097EDF14F_gshared (WhereSelectEnumerableIterator_2_t382E0095C883AA01020D10E08F7F38BBA26A3EC5 * __this, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B * L_1 = (WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 9));
		((  void (*) (WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B *, RuntimeObject*, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,System.Object>::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2__ctor_mB4DEA97BA61FAE550F10A6C4EFEAAC6D8F49F1B3_gshared (WhereSelectEnumerableIterator_2_tEBF9E61EFDABBBB02113A82648E32AC170921957 * __this, RuntimeObject* ___source0, Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 * ___predicate1, Func_2_t5B101C0B26ADAA3B1A77BC50D51CC42364E5409E * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		RuntimeObject* L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t5B101C0B26ADAA3B1A77BC50D51CC42364E5409E * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,System.Object>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 * WhereSelectEnumerableIterator_2_Clone_mF9281473FF30FD231DCCF6AD5C898EFDA029250C_gshared (WhereSelectEnumerableIterator_2_tEBF9E61EFDABBBB02113A82648E32AC170921957 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_source_3();
		Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 * L_1 = (Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 *)__this->get_predicate_4();
		Func_2_t5B101C0B26ADAA3B1A77BC50D51CC42364E5409E * L_2 = (Func_2_t5B101C0B26ADAA3B1A77BC50D51CC42364E5409E *)__this->get_selector_5();
		WhereSelectEnumerableIterator_2_tEBF9E61EFDABBBB02113A82648E32AC170921957 * L_3 = (WhereSelectEnumerableIterator_2_tEBF9E61EFDABBBB02113A82648E32AC170921957 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectEnumerableIterator_2_tEBF9E61EFDABBBB02113A82648E32AC170921957 *, RuntimeObject*, Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 *, Func_2_t5B101C0B26ADAA3B1A77BC50D51CC42364E5409E *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (RuntimeObject*)L_0, (Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 *)L_1, (Func_2_t5B101C0B26ADAA3B1A77BC50D51CC42364E5409E *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)L_3;
	}
}
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,System.Object>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2_Dispose_m3227B92605007A7258A7EF9DB10356FEE2BECBD0_gshared (WhereSelectEnumerableIterator_2_tEBF9E61EFDABBBB02113A82648E32AC170921957 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_enumerator_6();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		RuntimeObject* L_1 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, (RuntimeObject*)L_1);
	}

IL_0013:
	{
		__this->set_enumerator_6((RuntimeObject*)NULL);
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		return;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectEnumerableIterator_2_MoveNext_m1E2FACAEEE841F6C0552D3B6716DD2F988B2D757_gshared (WhereSelectEnumerableIterator_2_tEBF9E61EFDABBBB02113A82648E32AC170921957 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		RuntimeObject* L_3 = (RuntimeObject*)__this->get_source_3();
		NullCheck((RuntimeObject*)L_3);
		RuntimeObject* L_4;
		L_4 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<UnityEngine.InputSystem.Utilities.NamedValue>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 5), (RuntimeObject*)L_3);
		__this->set_enumerator_6(L_4);
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		RuntimeObject* L_5 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_5);
		NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077  L_6;
		L_6 = InterfaceFuncInvoker0< NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<UnityEngine.InputSystem.Utilities.NamedValue>::get_Current() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 6), (RuntimeObject*)L_5);
		V_1 = (NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077 )L_6;
		Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 * L_7 = (Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 * L_8 = (Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 *)__this->get_predicate_4();
		NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077  L_9 = V_1;
		NullCheck((Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 *, NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 *)L_8, (NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t5B101C0B26ADAA3B1A77BC50D51CC42364E5409E * L_11 = (Func_2_t5B101C0B26ADAA3B1A77BC50D51CC42364E5409E *)__this->get_selector_5();
		NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077  L_12 = V_1;
		NullCheck((Func_2_t5B101C0B26ADAA3B1A77BC50D51CC42364E5409E *)L_11);
		RuntimeObject * L_13;
		L_13 = ((  RuntimeObject * (*) (Func_2_t5B101C0B26ADAA3B1A77BC50D51CC42364E5409E *, NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((Func_2_t5B101C0B26ADAA3B1A77BC50D51CC42364E5409E *)L_11, (NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077 )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		RuntimeObject* L_14 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_14);
		bool L_15;
		L_15 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, (RuntimeObject*)L_14);
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		VirtualActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Object>::Dispose() */, (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,System.Object>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectEnumerableIterator_2_Where_mC7D44A08529DD455B0B8598986CF2EEF330CA1E5_gshared (WhereSelectEnumerableIterator_2_tEBF9E61EFDABBBB02113A82648E32AC170921957 * __this, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 * L_1 = (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 9));
		((  void (*) (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *, RuntimeObject*, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Object,UnityEngine.InputSystem.Utilities.InternedString>::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2__ctor_m91E09D9F1330D7E590C627CCC986AD6AF81765A6_gshared (WhereSelectEnumerableIterator_2_tE487654732A8A2F1E3A2D57412C08036EE861F38 * __this, RuntimeObject* ___source0, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate1, Func_2_t964229B6ADD2B008268547ED2BC3C090AF218065 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		((  void (*) (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		RuntimeObject* L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t964229B6ADD2B008268547ED2BC3C090AF218065 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Object,UnityEngine.InputSystem.Utilities.InternedString>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 * WhereSelectEnumerableIterator_2_Clone_m2EB753B8E98910228F5DE805D07F069E658B0AA0_gshared (WhereSelectEnumerableIterator_2_tE487654732A8A2F1E3A2D57412C08036EE861F38 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_source_3();
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_1 = (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)__this->get_predicate_4();
		Func_2_t964229B6ADD2B008268547ED2BC3C090AF218065 * L_2 = (Func_2_t964229B6ADD2B008268547ED2BC3C090AF218065 *)__this->get_selector_5();
		WhereSelectEnumerableIterator_2_tE487654732A8A2F1E3A2D57412C08036EE861F38 * L_3 = (WhereSelectEnumerableIterator_2_tE487654732A8A2F1E3A2D57412C08036EE861F38 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectEnumerableIterator_2_tE487654732A8A2F1E3A2D57412C08036EE861F38 *, RuntimeObject*, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, Func_2_t964229B6ADD2B008268547ED2BC3C090AF218065 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (RuntimeObject*)L_0, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_1, (Func_2_t964229B6ADD2B008268547ED2BC3C090AF218065 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)L_3;
	}
}
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Object,UnityEngine.InputSystem.Utilities.InternedString>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2_Dispose_m8C429758299D47FE1C7540A09508694C4937A0CF_gshared (WhereSelectEnumerableIterator_2_tE487654732A8A2F1E3A2D57412C08036EE861F38 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_enumerator_6();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		RuntimeObject* L_1 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, (RuntimeObject*)L_1);
	}

IL_0013:
	{
		__this->set_enumerator_6((RuntimeObject*)NULL);
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		((  void (*) (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		return;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Object,UnityEngine.InputSystem.Utilities.InternedString>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectEnumerableIterator_2_MoveNext_m8AB7D24A2E80EACC10188BA191189BDF6E632D40_gshared (WhereSelectEnumerableIterator_2_tE487654732A8A2F1E3A2D57412C08036EE861F38 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	RuntimeObject * V_1 = NULL;
	{
		int32_t L_0 = (int32_t)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		RuntimeObject* L_3 = (RuntimeObject*)__this->get_source_3();
		NullCheck((RuntimeObject*)L_3);
		RuntimeObject* L_4;
		L_4 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 5), (RuntimeObject*)L_3);
		__this->set_enumerator_6(L_4);
		((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		RuntimeObject* L_5 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_5);
		RuntimeObject * L_6;
		L_6 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 6), (RuntimeObject*)L_5);
		V_1 = (RuntimeObject *)L_6;
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_7 = (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_8 = (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)__this->get_predicate_4();
		RuntimeObject * L_9 = V_1;
		NullCheck((Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_8, (RuntimeObject *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t964229B6ADD2B008268547ED2BC3C090AF218065 * L_11 = (Func_2_t964229B6ADD2B008268547ED2BC3C090AF218065 *)__this->get_selector_5();
		RuntimeObject * L_12 = V_1;
		NullCheck((Func_2_t964229B6ADD2B008268547ED2BC3C090AF218065 *)L_11);
		InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  L_13;
		L_13 = ((  InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  (*) (Func_2_t964229B6ADD2B008268547ED2BC3C090AF218065 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((Func_2_t964229B6ADD2B008268547ED2BC3C090AF218065 *)L_11, (RuntimeObject *)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		RuntimeObject* L_14 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_14);
		bool L_15;
		L_15 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, (RuntimeObject*)L_14);
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		VirtualActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<UnityEngine.InputSystem.Utilities.InternedString>::Dispose() */, (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Object,UnityEngine.InputSystem.Utilities.InternedString>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectEnumerableIterator_2_Where_mE184536471E6032B53615ADA36A81FB47AC6708C_gshared (WhereSelectEnumerableIterator_2_tE487654732A8A2F1E3A2D57412C08036EE861F38 * __this, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B * L_1 = (WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 9));
		((  void (*) (WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B *, RuntimeObject*, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Object,System.Object>::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2__ctor_m1BAFD238D2366526A6B8210394785A8B8C8F6C6B_gshared (WhereSelectEnumerableIterator_2_tDAA8BFBEA68F81670F3F51C6200EBD26D7A8FBAB * __this, RuntimeObject* ___source0, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate1, Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		RuntimeObject* L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Object,System.Object>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 * WhereSelectEnumerableIterator_2_Clone_m6756C8EB7327B2820FDBDF5349E5E25BBA507B2C_gshared (WhereSelectEnumerableIterator_2_tDAA8BFBEA68F81670F3F51C6200EBD26D7A8FBAB * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_source_3();
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_1 = (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)__this->get_predicate_4();
		Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 * L_2 = (Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 *)__this->get_selector_5();
		WhereSelectEnumerableIterator_2_tDAA8BFBEA68F81670F3F51C6200EBD26D7A8FBAB * L_3 = (WhereSelectEnumerableIterator_2_tDAA8BFBEA68F81670F3F51C6200EBD26D7A8FBAB *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectEnumerableIterator_2_tDAA8BFBEA68F81670F3F51C6200EBD26D7A8FBAB *, RuntimeObject*, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (RuntimeObject*)L_0, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_1, (Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)L_3;
	}
}
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Object,System.Object>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2_Dispose_mAA70577DEF67CEC98FE677984AE2175B7D4E4D00_gshared (WhereSelectEnumerableIterator_2_tDAA8BFBEA68F81670F3F51C6200EBD26D7A8FBAB * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_enumerator_6();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		RuntimeObject* L_1 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, (RuntimeObject*)L_1);
	}

IL_0013:
	{
		__this->set_enumerator_6((RuntimeObject*)NULL);
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		return;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Object,System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectEnumerableIterator_2_MoveNext_m95AEE737A22EFFFE6557F448BF5AFCC6241D0BD7_gshared (WhereSelectEnumerableIterator_2_tDAA8BFBEA68F81670F3F51C6200EBD26D7A8FBAB * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	RuntimeObject * V_1 = NULL;
	{
		int32_t L_0 = (int32_t)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		RuntimeObject* L_3 = (RuntimeObject*)__this->get_source_3();
		NullCheck((RuntimeObject*)L_3);
		RuntimeObject* L_4;
		L_4 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 5), (RuntimeObject*)L_3);
		__this->set_enumerator_6(L_4);
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		RuntimeObject* L_5 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_5);
		RuntimeObject * L_6;
		L_6 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 6), (RuntimeObject*)L_5);
		V_1 = (RuntimeObject *)L_6;
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_7 = (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_8 = (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)__this->get_predicate_4();
		RuntimeObject * L_9 = V_1;
		NullCheck((Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_8, (RuntimeObject *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 * L_11 = (Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 *)__this->get_selector_5();
		RuntimeObject * L_12 = V_1;
		NullCheck((Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 *)L_11);
		RuntimeObject * L_13;
		L_13 = ((  RuntimeObject * (*) (Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 *)L_11, (RuntimeObject *)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		RuntimeObject* L_14 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_14);
		bool L_15;
		L_15 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, (RuntimeObject*)L_14);
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		VirtualActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Object>::Dispose() */, (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Object,System.Object>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectEnumerableIterator_2_Where_m633B78BD9B141AC15833D3B70781E6DC592E274C_gshared (WhereSelectEnumerableIterator_2_tDAA8BFBEA68F81670F3F51C6200EBD26D7A8FBAB * __this, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 * L_1 = (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 9));
		((  void (*) (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *, RuntimeObject*, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.UIElements.StyleSelectorPart,UnityEngine.InputSystem.Utilities.InternedString>::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2__ctor_mC9C8E118EAB5600DFA8279267C64C49F932D256F_gshared (WhereSelectEnumerableIterator_2_tCCC50B2F34EC31A45C5FDC2C7A98279D4C71C99B * __this, RuntimeObject* ___source0, Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC * ___predicate1, Func_2_tFBBF89B72C697489D82FBE8FF822B191D2EE9A41 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		((  void (*) (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		RuntimeObject* L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_tFBBF89B72C697489D82FBE8FF822B191D2EE9A41 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.UIElements.StyleSelectorPart,UnityEngine.InputSystem.Utilities.InternedString>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 * WhereSelectEnumerableIterator_2_Clone_m1317E216E06F700982851DFBB58E021C27CFABCD_gshared (WhereSelectEnumerableIterator_2_tCCC50B2F34EC31A45C5FDC2C7A98279D4C71C99B * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_source_3();
		Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC * L_1 = (Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC *)__this->get_predicate_4();
		Func_2_tFBBF89B72C697489D82FBE8FF822B191D2EE9A41 * L_2 = (Func_2_tFBBF89B72C697489D82FBE8FF822B191D2EE9A41 *)__this->get_selector_5();
		WhereSelectEnumerableIterator_2_tCCC50B2F34EC31A45C5FDC2C7A98279D4C71C99B * L_3 = (WhereSelectEnumerableIterator_2_tCCC50B2F34EC31A45C5FDC2C7A98279D4C71C99B *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectEnumerableIterator_2_tCCC50B2F34EC31A45C5FDC2C7A98279D4C71C99B *, RuntimeObject*, Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC *, Func_2_tFBBF89B72C697489D82FBE8FF822B191D2EE9A41 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (RuntimeObject*)L_0, (Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC *)L_1, (Func_2_tFBBF89B72C697489D82FBE8FF822B191D2EE9A41 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)L_3;
	}
}
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.UIElements.StyleSelectorPart,UnityEngine.InputSystem.Utilities.InternedString>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2_Dispose_m8A3A060F4E28E01E3450028C37CF6D03C74977C7_gshared (WhereSelectEnumerableIterator_2_tCCC50B2F34EC31A45C5FDC2C7A98279D4C71C99B * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_enumerator_6();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		RuntimeObject* L_1 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, (RuntimeObject*)L_1);
	}

IL_0013:
	{
		__this->set_enumerator_6((RuntimeObject*)NULL);
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		((  void (*) (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		return;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.UIElements.StyleSelectorPart,UnityEngine.InputSystem.Utilities.InternedString>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectEnumerableIterator_2_MoveNext_mFC665E27E7C8344D79257868F1692A4176D9977B_gshared (WhereSelectEnumerableIterator_2_tCCC50B2F34EC31A45C5FDC2C7A98279D4C71C99B * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	StyleSelectorPart_t707EDC970FC0F3E91E56DCBC178672A120426D54  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		RuntimeObject* L_3 = (RuntimeObject*)__this->get_source_3();
		NullCheck((RuntimeObject*)L_3);
		RuntimeObject* L_4;
		L_4 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<UnityEngine.UIElements.StyleSelectorPart>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 5), (RuntimeObject*)L_3);
		__this->set_enumerator_6(L_4);
		((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		RuntimeObject* L_5 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_5);
		StyleSelectorPart_t707EDC970FC0F3E91E56DCBC178672A120426D54  L_6;
		L_6 = InterfaceFuncInvoker0< StyleSelectorPart_t707EDC970FC0F3E91E56DCBC178672A120426D54  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<UnityEngine.UIElements.StyleSelectorPart>::get_Current() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 6), (RuntimeObject*)L_5);
		V_1 = (StyleSelectorPart_t707EDC970FC0F3E91E56DCBC178672A120426D54 )L_6;
		Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC * L_7 = (Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC * L_8 = (Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC *)__this->get_predicate_4();
		StyleSelectorPart_t707EDC970FC0F3E91E56DCBC178672A120426D54  L_9 = V_1;
		NullCheck((Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC *, StyleSelectorPart_t707EDC970FC0F3E91E56DCBC178672A120426D54 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC *)L_8, (StyleSelectorPart_t707EDC970FC0F3E91E56DCBC178672A120426D54 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_tFBBF89B72C697489D82FBE8FF822B191D2EE9A41 * L_11 = (Func_2_tFBBF89B72C697489D82FBE8FF822B191D2EE9A41 *)__this->get_selector_5();
		StyleSelectorPart_t707EDC970FC0F3E91E56DCBC178672A120426D54  L_12 = V_1;
		NullCheck((Func_2_tFBBF89B72C697489D82FBE8FF822B191D2EE9A41 *)L_11);
		InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  L_13;
		L_13 = ((  InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  (*) (Func_2_tFBBF89B72C697489D82FBE8FF822B191D2EE9A41 *, StyleSelectorPart_t707EDC970FC0F3E91E56DCBC178672A120426D54 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((Func_2_tFBBF89B72C697489D82FBE8FF822B191D2EE9A41 *)L_11, (StyleSelectorPart_t707EDC970FC0F3E91E56DCBC178672A120426D54 )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		RuntimeObject* L_14 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_14);
		bool L_15;
		L_15 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, (RuntimeObject*)L_14);
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		VirtualActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<UnityEngine.InputSystem.Utilities.InternedString>::Dispose() */, (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.UIElements.StyleSelectorPart,UnityEngine.InputSystem.Utilities.InternedString>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectEnumerableIterator_2_Where_mC460ECA45ABE88B1A3626347112B4C299E7512C0_gshared (WhereSelectEnumerableIterator_2_tCCC50B2F34EC31A45C5FDC2C7A98279D4C71C99B * __this, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B * L_1 = (WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 9));
		((  void (*) (WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B *, RuntimeObject*, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.UIElements.StyleSelectorPart,System.Object>::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2__ctor_m91490BBA74B5D2DA6D529D9F43BA89A12377D064_gshared (WhereSelectEnumerableIterator_2_tF480AE5ADD931EEE44A713040BA622E7577211AE * __this, RuntimeObject* ___source0, Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC * ___predicate1, Func_2_t43979C5A872BA9ED7A923D533A01683A98D71957 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		RuntimeObject* L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t43979C5A872BA9ED7A923D533A01683A98D71957 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.UIElements.StyleSelectorPart,System.Object>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 * WhereSelectEnumerableIterator_2_Clone_mF942928B23B283B92799C85D5510E760E063373B_gshared (WhereSelectEnumerableIterator_2_tF480AE5ADD931EEE44A713040BA622E7577211AE * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_source_3();
		Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC * L_1 = (Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC *)__this->get_predicate_4();
		Func_2_t43979C5A872BA9ED7A923D533A01683A98D71957 * L_2 = (Func_2_t43979C5A872BA9ED7A923D533A01683A98D71957 *)__this->get_selector_5();
		WhereSelectEnumerableIterator_2_tF480AE5ADD931EEE44A713040BA622E7577211AE * L_3 = (WhereSelectEnumerableIterator_2_tF480AE5ADD931EEE44A713040BA622E7577211AE *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectEnumerableIterator_2_tF480AE5ADD931EEE44A713040BA622E7577211AE *, RuntimeObject*, Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC *, Func_2_t43979C5A872BA9ED7A923D533A01683A98D71957 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (RuntimeObject*)L_0, (Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC *)L_1, (Func_2_t43979C5A872BA9ED7A923D533A01683A98D71957 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)L_3;
	}
}
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.UIElements.StyleSelectorPart,System.Object>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2_Dispose_mDD632322853DAC1666C0E8A4A83F99FB5C58C8B0_gshared (WhereSelectEnumerableIterator_2_tF480AE5ADD931EEE44A713040BA622E7577211AE * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_enumerator_6();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		RuntimeObject* L_1 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, (RuntimeObject*)L_1);
	}

IL_0013:
	{
		__this->set_enumerator_6((RuntimeObject*)NULL);
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		return;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.UIElements.StyleSelectorPart,System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectEnumerableIterator_2_MoveNext_m3013FC22F72997A91C2D7915F8266CB6FF73A60E_gshared (WhereSelectEnumerableIterator_2_tF480AE5ADD931EEE44A713040BA622E7577211AE * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	StyleSelectorPart_t707EDC970FC0F3E91E56DCBC178672A120426D54  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		RuntimeObject* L_3 = (RuntimeObject*)__this->get_source_3();
		NullCheck((RuntimeObject*)L_3);
		RuntimeObject* L_4;
		L_4 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<UnityEngine.UIElements.StyleSelectorPart>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 5), (RuntimeObject*)L_3);
		__this->set_enumerator_6(L_4);
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		RuntimeObject* L_5 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_5);
		StyleSelectorPart_t707EDC970FC0F3E91E56DCBC178672A120426D54  L_6;
		L_6 = InterfaceFuncInvoker0< StyleSelectorPart_t707EDC970FC0F3E91E56DCBC178672A120426D54  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<UnityEngine.UIElements.StyleSelectorPart>::get_Current() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 6), (RuntimeObject*)L_5);
		V_1 = (StyleSelectorPart_t707EDC970FC0F3E91E56DCBC178672A120426D54 )L_6;
		Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC * L_7 = (Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC * L_8 = (Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC *)__this->get_predicate_4();
		StyleSelectorPart_t707EDC970FC0F3E91E56DCBC178672A120426D54  L_9 = V_1;
		NullCheck((Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC *, StyleSelectorPart_t707EDC970FC0F3E91E56DCBC178672A120426D54 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC *)L_8, (StyleSelectorPart_t707EDC970FC0F3E91E56DCBC178672A120426D54 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t43979C5A872BA9ED7A923D533A01683A98D71957 * L_11 = (Func_2_t43979C5A872BA9ED7A923D533A01683A98D71957 *)__this->get_selector_5();
		StyleSelectorPart_t707EDC970FC0F3E91E56DCBC178672A120426D54  L_12 = V_1;
		NullCheck((Func_2_t43979C5A872BA9ED7A923D533A01683A98D71957 *)L_11);
		RuntimeObject * L_13;
		L_13 = ((  RuntimeObject * (*) (Func_2_t43979C5A872BA9ED7A923D533A01683A98D71957 *, StyleSelectorPart_t707EDC970FC0F3E91E56DCBC178672A120426D54 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((Func_2_t43979C5A872BA9ED7A923D533A01683A98D71957 *)L_11, (StyleSelectorPart_t707EDC970FC0F3E91E56DCBC178672A120426D54 )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		RuntimeObject* L_14 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_14);
		bool L_15;
		L_15 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, (RuntimeObject*)L_14);
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		VirtualActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Object>::Dispose() */, (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.UIElements.StyleSelectorPart,System.Object>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectEnumerableIterator_2_Where_m89256D223A9E3E4255617FF6404F430811F5EC4D_gshared (WhereSelectEnumerableIterator_2_tF480AE5ADD931EEE44A713040BA622E7577211AE * __this, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 * L_1 = (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 9));
		((  void (*) (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *, RuntimeObject*, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.Substring,UnityEngine.InputSystem.Utilities.InternedString>::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2__ctor_m11676BA14CCF4FEF85A2B93B0DC1FB4E6939DB6F_gshared (WhereSelectEnumerableIterator_2_tFDFC244728E14BAC93BE9869A64080C38A0B7F1D * __this, RuntimeObject* ___source0, Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 * ___predicate1, Func_2_t14F6F4D7FBF558A20782EDC48D80473E4800048E * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		((  void (*) (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		RuntimeObject* L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t14F6F4D7FBF558A20782EDC48D80473E4800048E * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.Substring,UnityEngine.InputSystem.Utilities.InternedString>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 * WhereSelectEnumerableIterator_2_Clone_m47AE9B1B9692E340CB00E76F09097F75FD758F47_gshared (WhereSelectEnumerableIterator_2_tFDFC244728E14BAC93BE9869A64080C38A0B7F1D * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_source_3();
		Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 * L_1 = (Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 *)__this->get_predicate_4();
		Func_2_t14F6F4D7FBF558A20782EDC48D80473E4800048E * L_2 = (Func_2_t14F6F4D7FBF558A20782EDC48D80473E4800048E *)__this->get_selector_5();
		WhereSelectEnumerableIterator_2_tFDFC244728E14BAC93BE9869A64080C38A0B7F1D * L_3 = (WhereSelectEnumerableIterator_2_tFDFC244728E14BAC93BE9869A64080C38A0B7F1D *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectEnumerableIterator_2_tFDFC244728E14BAC93BE9869A64080C38A0B7F1D *, RuntimeObject*, Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 *, Func_2_t14F6F4D7FBF558A20782EDC48D80473E4800048E *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (RuntimeObject*)L_0, (Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 *)L_1, (Func_2_t14F6F4D7FBF558A20782EDC48D80473E4800048E *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)L_3;
	}
}
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.Substring,UnityEngine.InputSystem.Utilities.InternedString>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2_Dispose_m5E0D3576F68517F538B8877D37526BC5DC3938D3_gshared (WhereSelectEnumerableIterator_2_tFDFC244728E14BAC93BE9869A64080C38A0B7F1D * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_enumerator_6();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		RuntimeObject* L_1 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, (RuntimeObject*)L_1);
	}

IL_0013:
	{
		__this->set_enumerator_6((RuntimeObject*)NULL);
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		((  void (*) (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		return;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.Substring,UnityEngine.InputSystem.Utilities.InternedString>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectEnumerableIterator_2_MoveNext_mE1401B0580A1237F67DEBBDB8E524C2CBA277BE7_gshared (WhereSelectEnumerableIterator_2_tFDFC244728E14BAC93BE9869A64080C38A0B7F1D * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		RuntimeObject* L_3 = (RuntimeObject*)__this->get_source_3();
		NullCheck((RuntimeObject*)L_3);
		RuntimeObject* L_4;
		L_4 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<UnityEngine.InputSystem.Utilities.Substring>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 5), (RuntimeObject*)L_3);
		__this->set_enumerator_6(L_4);
		((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		RuntimeObject* L_5 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_5);
		Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F  L_6;
		L_6 = InterfaceFuncInvoker0< Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<UnityEngine.InputSystem.Utilities.Substring>::get_Current() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 6), (RuntimeObject*)L_5);
		V_1 = (Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F )L_6;
		Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 * L_7 = (Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 * L_8 = (Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 *)__this->get_predicate_4();
		Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F  L_9 = V_1;
		NullCheck((Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 *, Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 *)L_8, (Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t14F6F4D7FBF558A20782EDC48D80473E4800048E * L_11 = (Func_2_t14F6F4D7FBF558A20782EDC48D80473E4800048E *)__this->get_selector_5();
		Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F  L_12 = V_1;
		NullCheck((Func_2_t14F6F4D7FBF558A20782EDC48D80473E4800048E *)L_11);
		InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  L_13;
		L_13 = ((  InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  (*) (Func_2_t14F6F4D7FBF558A20782EDC48D80473E4800048E *, Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((Func_2_t14F6F4D7FBF558A20782EDC48D80473E4800048E *)L_11, (Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		RuntimeObject* L_14 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_14);
		bool L_15;
		L_15 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, (RuntimeObject*)L_14);
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		VirtualActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<UnityEngine.InputSystem.Utilities.InternedString>::Dispose() */, (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.Substring,UnityEngine.InputSystem.Utilities.InternedString>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectEnumerableIterator_2_Where_mEE3D8F0C43D35E1B258D44FD96CE8772498F96A9_gshared (WhereSelectEnumerableIterator_2_tFDFC244728E14BAC93BE9869A64080C38A0B7F1D * __this, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B * L_1 = (WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 9));
		((  void (*) (WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B *, RuntimeObject*, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.Substring,System.Object>::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2__ctor_m17C4CBC9625B0A3482CCCCC25ADF40E3F1598A28_gshared (WhereSelectEnumerableIterator_2_t5E3BB9AFBEC27ED62FC129516C2D3E9FC81641B6 * __this, RuntimeObject* ___source0, Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 * ___predicate1, Func_2_t4EC2F095EDDC8079537CA0FEDC463635E599C9E4 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		RuntimeObject* L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t4EC2F095EDDC8079537CA0FEDC463635E599C9E4 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.Substring,System.Object>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 * WhereSelectEnumerableIterator_2_Clone_mC4FCCC637982BE25A352F7075A9CBD60E21F2136_gshared (WhereSelectEnumerableIterator_2_t5E3BB9AFBEC27ED62FC129516C2D3E9FC81641B6 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_source_3();
		Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 * L_1 = (Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 *)__this->get_predicate_4();
		Func_2_t4EC2F095EDDC8079537CA0FEDC463635E599C9E4 * L_2 = (Func_2_t4EC2F095EDDC8079537CA0FEDC463635E599C9E4 *)__this->get_selector_5();
		WhereSelectEnumerableIterator_2_t5E3BB9AFBEC27ED62FC129516C2D3E9FC81641B6 * L_3 = (WhereSelectEnumerableIterator_2_t5E3BB9AFBEC27ED62FC129516C2D3E9FC81641B6 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectEnumerableIterator_2_t5E3BB9AFBEC27ED62FC129516C2D3E9FC81641B6 *, RuntimeObject*, Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 *, Func_2_t4EC2F095EDDC8079537CA0FEDC463635E599C9E4 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (RuntimeObject*)L_0, (Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 *)L_1, (Func_2_t4EC2F095EDDC8079537CA0FEDC463635E599C9E4 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)L_3;
	}
}
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.Substring,System.Object>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2_Dispose_mEB51A4C958B4C9198E3CEA72900154448940EECA_gshared (WhereSelectEnumerableIterator_2_t5E3BB9AFBEC27ED62FC129516C2D3E9FC81641B6 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_enumerator_6();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		RuntimeObject* L_1 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, (RuntimeObject*)L_1);
	}

IL_0013:
	{
		__this->set_enumerator_6((RuntimeObject*)NULL);
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		return;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.Substring,System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectEnumerableIterator_2_MoveNext_mD60FF0AB4AC763AE4BA6EAE348FDC7EA509AEAE9_gshared (WhereSelectEnumerableIterator_2_t5E3BB9AFBEC27ED62FC129516C2D3E9FC81641B6 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		RuntimeObject* L_3 = (RuntimeObject*)__this->get_source_3();
		NullCheck((RuntimeObject*)L_3);
		RuntimeObject* L_4;
		L_4 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<UnityEngine.InputSystem.Utilities.Substring>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 5), (RuntimeObject*)L_3);
		__this->set_enumerator_6(L_4);
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		RuntimeObject* L_5 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_5);
		Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F  L_6;
		L_6 = InterfaceFuncInvoker0< Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<UnityEngine.InputSystem.Utilities.Substring>::get_Current() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 6), (RuntimeObject*)L_5);
		V_1 = (Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F )L_6;
		Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 * L_7 = (Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 * L_8 = (Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 *)__this->get_predicate_4();
		Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F  L_9 = V_1;
		NullCheck((Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 *, Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 *)L_8, (Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t4EC2F095EDDC8079537CA0FEDC463635E599C9E4 * L_11 = (Func_2_t4EC2F095EDDC8079537CA0FEDC463635E599C9E4 *)__this->get_selector_5();
		Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F  L_12 = V_1;
		NullCheck((Func_2_t4EC2F095EDDC8079537CA0FEDC463635E599C9E4 *)L_11);
		RuntimeObject * L_13;
		L_13 = ((  RuntimeObject * (*) (Func_2_t4EC2F095EDDC8079537CA0FEDC463635E599C9E4 *, Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((Func_2_t4EC2F095EDDC8079537CA0FEDC463635E599C9E4 *)L_11, (Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		RuntimeObject* L_14 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_14);
		bool L_15;
		L_15 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, (RuntimeObject*)L_14);
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		VirtualActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Object>::Dispose() */, (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.Substring,System.Object>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectEnumerableIterator_2_Where_m905DBEF1EBF04A3925F83CCAFEA855B492130484_gshared (WhereSelectEnumerableIterator_2_t5E3BB9AFBEC27ED62FC129516C2D3E9FC81641B6 * __this, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 * L_1 = (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 9));
		((  void (*) (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *, RuntimeObject*, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,UnityEngine.InputSystem.Utilities.InternedString>::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2__ctor_mBBD491824AE7AAB426D92B986304B33F8F994AEE_gshared (WhereSelectEnumerableIterator_2_t6D595CFCE85C8B8D77C138F46CCF2608E20474FE * __this, RuntimeObject* ___source0, Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 * ___predicate1, Func_2_t48DD1C8E439811C492BAFE220A679DB57D7C9370 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		((  void (*) (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		RuntimeObject* L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t48DD1C8E439811C492BAFE220A679DB57D7C9370 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,UnityEngine.InputSystem.Utilities.InternedString>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 * WhereSelectEnumerableIterator_2_Clone_m2CD1DF292C33DEE40AF63D63B46B78C3A162E8F0_gshared (WhereSelectEnumerableIterator_2_t6D595CFCE85C8B8D77C138F46CCF2608E20474FE * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_source_3();
		Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 * L_1 = (Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 *)__this->get_predicate_4();
		Func_2_t48DD1C8E439811C492BAFE220A679DB57D7C9370 * L_2 = (Func_2_t48DD1C8E439811C492BAFE220A679DB57D7C9370 *)__this->get_selector_5();
		WhereSelectEnumerableIterator_2_t6D595CFCE85C8B8D77C138F46CCF2608E20474FE * L_3 = (WhereSelectEnumerableIterator_2_t6D595CFCE85C8B8D77C138F46CCF2608E20474FE *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectEnumerableIterator_2_t6D595CFCE85C8B8D77C138F46CCF2608E20474FE *, RuntimeObject*, Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 *, Func_2_t48DD1C8E439811C492BAFE220A679DB57D7C9370 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (RuntimeObject*)L_0, (Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 *)L_1, (Func_2_t48DD1C8E439811C492BAFE220A679DB57D7C9370 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)L_3;
	}
}
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,UnityEngine.InputSystem.Utilities.InternedString>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2_Dispose_m97919E49A44E867E1572B62076D9B0C8D450899C_gshared (WhereSelectEnumerableIterator_2_t6D595CFCE85C8B8D77C138F46CCF2608E20474FE * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_enumerator_6();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		RuntimeObject* L_1 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, (RuntimeObject*)L_1);
	}

IL_0013:
	{
		__this->set_enumerator_6((RuntimeObject*)NULL);
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		((  void (*) (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		return;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,UnityEngine.InputSystem.Utilities.InternedString>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectEnumerableIterator_2_MoveNext_m6CFFBD72246D7FFFB93493009D52CE53C3043BA5_gshared (WhereSelectEnumerableIterator_2_t6D595CFCE85C8B8D77C138F46CCF2608E20474FE * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		RuntimeObject* L_3 = (RuntimeObject*)__this->get_source_3();
		NullCheck((RuntimeObject*)L_3);
		RuntimeObject* L_4;
		L_4 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 5), (RuntimeObject*)L_3);
		__this->set_enumerator_6(L_4);
		((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		RuntimeObject* L_5 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_5);
		JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB  L_6;
		L_6 = InterfaceFuncInvoker0< JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>::get_Current() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 6), (RuntimeObject*)L_5);
		V_1 = (JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB )L_6;
		Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 * L_7 = (Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 * L_8 = (Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 *)__this->get_predicate_4();
		JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB  L_9 = V_1;
		NullCheck((Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 *, JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 *)L_8, (JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t48DD1C8E439811C492BAFE220A679DB57D7C9370 * L_11 = (Func_2_t48DD1C8E439811C492BAFE220A679DB57D7C9370 *)__this->get_selector_5();
		JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB  L_12 = V_1;
		NullCheck((Func_2_t48DD1C8E439811C492BAFE220A679DB57D7C9370 *)L_11);
		InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  L_13;
		L_13 = ((  InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  (*) (Func_2_t48DD1C8E439811C492BAFE220A679DB57D7C9370 *, JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((Func_2_t48DD1C8E439811C492BAFE220A679DB57D7C9370 *)L_11, (JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		RuntimeObject* L_14 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_14);
		bool L_15;
		L_15 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, (RuntimeObject*)L_14);
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		VirtualActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<UnityEngine.InputSystem.Utilities.InternedString>::Dispose() */, (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,UnityEngine.InputSystem.Utilities.InternedString>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectEnumerableIterator_2_Where_m22BD3056CB42A0C82730F309A72EB02D4836194E_gshared (WhereSelectEnumerableIterator_2_t6D595CFCE85C8B8D77C138F46CCF2608E20474FE * __this, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B * L_1 = (WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 9));
		((  void (*) (WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B *, RuntimeObject*, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,System.Object>::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2__ctor_m71A21268AF3CA329E45D910C8E3ED0DCBF574C3D_gshared (WhereSelectEnumerableIterator_2_t4DAE42F4C708266EAC938F1C6E7FB762CDF50463 * __this, RuntimeObject* ___source0, Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 * ___predicate1, Func_2_t1628B2E4EEC7A32D5B688E54415BC42EDA9D2899 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		RuntimeObject* L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t1628B2E4EEC7A32D5B688E54415BC42EDA9D2899 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,System.Object>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 * WhereSelectEnumerableIterator_2_Clone_m965E3834DF17427D231312B474A658B4BB23281B_gshared (WhereSelectEnumerableIterator_2_t4DAE42F4C708266EAC938F1C6E7FB762CDF50463 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_source_3();
		Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 * L_1 = (Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 *)__this->get_predicate_4();
		Func_2_t1628B2E4EEC7A32D5B688E54415BC42EDA9D2899 * L_2 = (Func_2_t1628B2E4EEC7A32D5B688E54415BC42EDA9D2899 *)__this->get_selector_5();
		WhereSelectEnumerableIterator_2_t4DAE42F4C708266EAC938F1C6E7FB762CDF50463 * L_3 = (WhereSelectEnumerableIterator_2_t4DAE42F4C708266EAC938F1C6E7FB762CDF50463 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectEnumerableIterator_2_t4DAE42F4C708266EAC938F1C6E7FB762CDF50463 *, RuntimeObject*, Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 *, Func_2_t1628B2E4EEC7A32D5B688E54415BC42EDA9D2899 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (RuntimeObject*)L_0, (Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 *)L_1, (Func_2_t1628B2E4EEC7A32D5B688E54415BC42EDA9D2899 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)L_3;
	}
}
// System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,System.Object>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2_Dispose_mE2C310B640FAE6578FCDC4DEDAD4649A92761F95_gshared (WhereSelectEnumerableIterator_2_t4DAE42F4C708266EAC938F1C6E7FB762CDF50463 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_enumerator_6();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		RuntimeObject* L_1 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, (RuntimeObject*)L_1);
	}

IL_0013:
	{
		__this->set_enumerator_6((RuntimeObject*)NULL);
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		return;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectEnumerableIterator_2_MoveNext_m83E7C067395255D79680E4A165CF10B744DEE224_gshared (WhereSelectEnumerableIterator_2_t4DAE42F4C708266EAC938F1C6E7FB762CDF50463 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		RuntimeObject* L_3 = (RuntimeObject*)__this->get_source_3();
		NullCheck((RuntimeObject*)L_3);
		RuntimeObject* L_4;
		L_4 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 5), (RuntimeObject*)L_3);
		__this->set_enumerator_6(L_4);
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		RuntimeObject* L_5 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_5);
		JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB  L_6;
		L_6 = InterfaceFuncInvoker0< JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>::get_Current() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 6), (RuntimeObject*)L_5);
		V_1 = (JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB )L_6;
		Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 * L_7 = (Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 * L_8 = (Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 *)__this->get_predicate_4();
		JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB  L_9 = V_1;
		NullCheck((Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 *, JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 *)L_8, (JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t1628B2E4EEC7A32D5B688E54415BC42EDA9D2899 * L_11 = (Func_2_t1628B2E4EEC7A32D5B688E54415BC42EDA9D2899 *)__this->get_selector_5();
		JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB  L_12 = V_1;
		NullCheck((Func_2_t1628B2E4EEC7A32D5B688E54415BC42EDA9D2899 *)L_11);
		RuntimeObject * L_13;
		L_13 = ((  RuntimeObject * (*) (Func_2_t1628B2E4EEC7A32D5B688E54415BC42EDA9D2899 *, JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((Func_2_t1628B2E4EEC7A32D5B688E54415BC42EDA9D2899 *)L_11, (JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		RuntimeObject* L_14 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_14);
		bool L_15;
		L_15 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, (RuntimeObject*)L_14);
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		VirtualActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Object>::Dispose() */, (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,System.Object>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectEnumerableIterator_2_Where_m33A7E404614B17277CEF605F718549142F9CA8FB_gshared (WhereSelectEnumerableIterator_2_t4DAE42F4C708266EAC938F1C6E7FB762CDF50463 * __this, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 * L_1 = (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 9));
		((  void (*) (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *, RuntimeObject*, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,UnityEngine.InputSystem.Utilities.InternedString>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_mBCD3E43334FDBF732DA509C037ECB9DFA2F5BEC8_gshared (WhereSelectListIterator_2_tF2F8745320F9D29534105CFDF2FADABD42A2D3F7 * __this, List_1_t9B1CABCBD4B6B7781E348FF332B32B333B543BA8 * ___source0, Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 * ___predicate1, Func_2_tB181F4C9A24C0634DF4A10CFBFFE6B45B3796BBC * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		((  void (*) (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_t9B1CABCBD4B6B7781E348FF332B32B333B543BA8 * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_tB181F4C9A24C0634DF4A10CFBFFE6B45B3796BBC * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,UnityEngine.InputSystem.Utilities.InternedString>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 * WhereSelectListIterator_2_Clone_m6F8FA1AA64B103AD51B70227C30F96315BFB6B56_gshared (WhereSelectListIterator_2_tF2F8745320F9D29534105CFDF2FADABD42A2D3F7 * __this, const RuntimeMethod* method)
{
	{
		List_1_t9B1CABCBD4B6B7781E348FF332B32B333B543BA8 * L_0 = (List_1_t9B1CABCBD4B6B7781E348FF332B32B333B543BA8 *)__this->get_source_3();
		Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 * L_1 = (Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 *)__this->get_predicate_4();
		Func_2_tB181F4C9A24C0634DF4A10CFBFFE6B45B3796BBC * L_2 = (Func_2_tB181F4C9A24C0634DF4A10CFBFFE6B45B3796BBC *)__this->get_selector_5();
		WhereSelectListIterator_2_tF2F8745320F9D29534105CFDF2FADABD42A2D3F7 * L_3 = (WhereSelectListIterator_2_tF2F8745320F9D29534105CFDF2FADABD42A2D3F7 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_tF2F8745320F9D29534105CFDF2FADABD42A2D3F7 *, List_1_t9B1CABCBD4B6B7781E348FF332B32B333B543BA8 *, Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 *, Func_2_tB181F4C9A24C0634DF4A10CFBFFE6B45B3796BBC *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_t9B1CABCBD4B6B7781E348FF332B32B333B543BA8 *)L_0, (Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 *)L_1, (Func_2_tB181F4C9A24C0634DF4A10CFBFFE6B45B3796BBC *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,UnityEngine.InputSystem.Utilities.InternedString>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_m1A03C3D41BAF23708560F81A7A75B6683F092FEA_gshared (WhereSelectListIterator_2_tF2F8745320F9D29534105CFDF2FADABD42A2D3F7 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_t9B1CABCBD4B6B7781E348FF332B32B333B543BA8 * L_3 = (List_1_t9B1CABCBD4B6B7781E348FF332B32B333B543BA8 *)__this->get_source_3();
		NullCheck((List_1_t9B1CABCBD4B6B7781E348FF332B32B333B543BA8 *)L_3);
		Enumerator_tF2D0EEBD6B861D9127DD6DC36B733FD42070858C  L_4;
		L_4 = ((  Enumerator_tF2D0EEBD6B861D9127DD6DC36B733FD42070858C  (*) (List_1_t9B1CABCBD4B6B7781E348FF332B32B333B543BA8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_t9B1CABCBD4B6B7781E348FF332B32B333B543BA8 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_tF2D0EEBD6B861D9127DD6DC36B733FD42070858C * L_5 = (Enumerator_tF2D0EEBD6B861D9127DD6DC36B733FD42070858C *)__this->get_address_of_enumerator_6();
		KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184  L_6;
		L_6 = Enumerator_get_Current_m003BC78F9886D5E4E98734CD70C3997E467EB1EA_inline((Enumerator_tF2D0EEBD6B861D9127DD6DC36B733FD42070858C *)(Enumerator_tF2D0EEBD6B861D9127DD6DC36B733FD42070858C *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184 )L_6;
		Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 * L_7 = (Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 * L_8 = (Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 *)__this->get_predicate_4();
		KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184  L_9 = V_1;
		NullCheck((Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 *, KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 *)L_8, (KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_tB181F4C9A24C0634DF4A10CFBFFE6B45B3796BBC * L_11 = (Func_2_tB181F4C9A24C0634DF4A10CFBFFE6B45B3796BBC *)__this->get_selector_5();
		KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184  L_12 = V_1;
		NullCheck((Func_2_tB181F4C9A24C0634DF4A10CFBFFE6B45B3796BBC *)L_11);
		InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  L_13;
		L_13 = ((  InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  (*) (Func_2_tB181F4C9A24C0634DF4A10CFBFFE6B45B3796BBC *, KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_tB181F4C9A24C0634DF4A10CFBFFE6B45B3796BBC *)L_11, (KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184 )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_tF2D0EEBD6B861D9127DD6DC36B733FD42070858C * L_14 = (Enumerator_tF2D0EEBD6B861D9127DD6DC36B733FD42070858C *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_m6A3C20CEFE29B5B761E6C9DBA053A2CE897E2515((Enumerator_tF2D0EEBD6B861D9127DD6DC36B733FD42070858C *)(Enumerator_tF2D0EEBD6B861D9127DD6DC36B733FD42070858C *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		VirtualActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<UnityEngine.InputSystem.Utilities.InternedString>::Dispose() */, (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,UnityEngine.InputSystem.Utilities.InternedString>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_m829703E486592EF4C019F4397610B3838EFD6551_gshared (WhereSelectListIterator_2_tF2F8745320F9D29534105CFDF2FADABD42A2D3F7 * __this, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B * L_1 = (WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B *, RuntimeObject*, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,System.Object>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_m973F9D84BF137E55D8E386B5C71A647A78350BFF_gshared (WhereSelectListIterator_2_t15BF3EF128C005C26DE8132D49E71B3C1FE6E211 * __this, List_1_t9B1CABCBD4B6B7781E348FF332B32B333B543BA8 * ___source0, Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 * ___predicate1, Func_2_t711512467B454C91EB8F3FB19CD6D141AAB5B6B1 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_t9B1CABCBD4B6B7781E348FF332B32B333B543BA8 * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t711512467B454C91EB8F3FB19CD6D141AAB5B6B1 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,System.Object>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 * WhereSelectListIterator_2_Clone_m7DC24F431973FD8F048EBB404ABC4F5D627121E4_gshared (WhereSelectListIterator_2_t15BF3EF128C005C26DE8132D49E71B3C1FE6E211 * __this, const RuntimeMethod* method)
{
	{
		List_1_t9B1CABCBD4B6B7781E348FF332B32B333B543BA8 * L_0 = (List_1_t9B1CABCBD4B6B7781E348FF332B32B333B543BA8 *)__this->get_source_3();
		Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 * L_1 = (Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 *)__this->get_predicate_4();
		Func_2_t711512467B454C91EB8F3FB19CD6D141AAB5B6B1 * L_2 = (Func_2_t711512467B454C91EB8F3FB19CD6D141AAB5B6B1 *)__this->get_selector_5();
		WhereSelectListIterator_2_t15BF3EF128C005C26DE8132D49E71B3C1FE6E211 * L_3 = (WhereSelectListIterator_2_t15BF3EF128C005C26DE8132D49E71B3C1FE6E211 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_t15BF3EF128C005C26DE8132D49E71B3C1FE6E211 *, List_1_t9B1CABCBD4B6B7781E348FF332B32B333B543BA8 *, Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 *, Func_2_t711512467B454C91EB8F3FB19CD6D141AAB5B6B1 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_t9B1CABCBD4B6B7781E348FF332B32B333B543BA8 *)L_0, (Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 *)L_1, (Func_2_t711512467B454C91EB8F3FB19CD6D141AAB5B6B1 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_m176A20043994757122A68EC28CC9E0EE00E09233_gshared (WhereSelectListIterator_2_t15BF3EF128C005C26DE8132D49E71B3C1FE6E211 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_t9B1CABCBD4B6B7781E348FF332B32B333B543BA8 * L_3 = (List_1_t9B1CABCBD4B6B7781E348FF332B32B333B543BA8 *)__this->get_source_3();
		NullCheck((List_1_t9B1CABCBD4B6B7781E348FF332B32B333B543BA8 *)L_3);
		Enumerator_tF2D0EEBD6B861D9127DD6DC36B733FD42070858C  L_4;
		L_4 = ((  Enumerator_tF2D0EEBD6B861D9127DD6DC36B733FD42070858C  (*) (List_1_t9B1CABCBD4B6B7781E348FF332B32B333B543BA8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_t9B1CABCBD4B6B7781E348FF332B32B333B543BA8 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_tF2D0EEBD6B861D9127DD6DC36B733FD42070858C * L_5 = (Enumerator_tF2D0EEBD6B861D9127DD6DC36B733FD42070858C *)__this->get_address_of_enumerator_6();
		KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184  L_6;
		L_6 = Enumerator_get_Current_m003BC78F9886D5E4E98734CD70C3997E467EB1EA_inline((Enumerator_tF2D0EEBD6B861D9127DD6DC36B733FD42070858C *)(Enumerator_tF2D0EEBD6B861D9127DD6DC36B733FD42070858C *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184 )L_6;
		Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 * L_7 = (Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 * L_8 = (Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 *)__this->get_predicate_4();
		KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184  L_9 = V_1;
		NullCheck((Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 *, KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t0645ECA65003F2E3F39AF1AB141FB4F4DA620590 *)L_8, (KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t711512467B454C91EB8F3FB19CD6D141AAB5B6B1 * L_11 = (Func_2_t711512467B454C91EB8F3FB19CD6D141AAB5B6B1 *)__this->get_selector_5();
		KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184  L_12 = V_1;
		NullCheck((Func_2_t711512467B454C91EB8F3FB19CD6D141AAB5B6B1 *)L_11);
		RuntimeObject * L_13;
		L_13 = ((  RuntimeObject * (*) (Func_2_t711512467B454C91EB8F3FB19CD6D141AAB5B6B1 *, KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t711512467B454C91EB8F3FB19CD6D141AAB5B6B1 *)L_11, (KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184 )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_tF2D0EEBD6B861D9127DD6DC36B733FD42070858C * L_14 = (Enumerator_tF2D0EEBD6B861D9127DD6DC36B733FD42070858C *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_m6A3C20CEFE29B5B761E6C9DBA053A2CE897E2515((Enumerator_tF2D0EEBD6B861D9127DD6DC36B733FD42070858C *)(Enumerator_tF2D0EEBD6B861D9127DD6DC36B733FD42070858C *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		VirtualActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Object>::Dispose() */, (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>,System.Object>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_mC511E80ADFE03504DE4E3E2F4272D621407750CD_gshared (WhereSelectListIterator_2_t15BF3EF128C005C26DE8132D49E71B3C1FE6E211 * __this, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 * L_1 = (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *, RuntimeObject*, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.InternedString,UnityEngine.InputSystem.Utilities.InternedString>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_m675018DBF2E912221E746E30B15ABFB38B2093EE_gshared (WhereSelectListIterator_2_t7BDF9DF4ED7F42E4D8139C4BA35A0F055024799E * __this, List_1_tFD37D052760A236906215AF581C58EE13DE6A5DC * ___source0, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * ___predicate1, Func_2_t1AF155D27A4D78882342D697E4A6DE7CE594545B * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		((  void (*) (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_tFD37D052760A236906215AF581C58EE13DE6A5DC * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t1AF155D27A4D78882342D697E4A6DE7CE594545B * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.InternedString,UnityEngine.InputSystem.Utilities.InternedString>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 * WhereSelectListIterator_2_Clone_mDD348246F54AF49F48DD0A2075BD2C639C78E9BD_gshared (WhereSelectListIterator_2_t7BDF9DF4ED7F42E4D8139C4BA35A0F055024799E * __this, const RuntimeMethod* method)
{
	{
		List_1_tFD37D052760A236906215AF581C58EE13DE6A5DC * L_0 = (List_1_tFD37D052760A236906215AF581C58EE13DE6A5DC *)__this->get_source_3();
		Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * L_1 = (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)__this->get_predicate_4();
		Func_2_t1AF155D27A4D78882342D697E4A6DE7CE594545B * L_2 = (Func_2_t1AF155D27A4D78882342D697E4A6DE7CE594545B *)__this->get_selector_5();
		WhereSelectListIterator_2_t7BDF9DF4ED7F42E4D8139C4BA35A0F055024799E * L_3 = (WhereSelectListIterator_2_t7BDF9DF4ED7F42E4D8139C4BA35A0F055024799E *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_t7BDF9DF4ED7F42E4D8139C4BA35A0F055024799E *, List_1_tFD37D052760A236906215AF581C58EE13DE6A5DC *, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *, Func_2_t1AF155D27A4D78882342D697E4A6DE7CE594545B *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_tFD37D052760A236906215AF581C58EE13DE6A5DC *)L_0, (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)L_1, (Func_2_t1AF155D27A4D78882342D697E4A6DE7CE594545B *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.InternedString,UnityEngine.InputSystem.Utilities.InternedString>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_m828CFE7C90058763749CC34BE6FC3884C1511B76_gshared (WhereSelectListIterator_2_t7BDF9DF4ED7F42E4D8139C4BA35A0F055024799E * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_tFD37D052760A236906215AF581C58EE13DE6A5DC * L_3 = (List_1_tFD37D052760A236906215AF581C58EE13DE6A5DC *)__this->get_source_3();
		NullCheck((List_1_tFD37D052760A236906215AF581C58EE13DE6A5DC *)L_3);
		Enumerator_tD1ECE103520161C1BD172331D6968D3D184B3B6F  L_4;
		L_4 = ((  Enumerator_tD1ECE103520161C1BD172331D6968D3D184B3B6F  (*) (List_1_tFD37D052760A236906215AF581C58EE13DE6A5DC *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_tFD37D052760A236906215AF581C58EE13DE6A5DC *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_tD1ECE103520161C1BD172331D6968D3D184B3B6F * L_5 = (Enumerator_tD1ECE103520161C1BD172331D6968D3D184B3B6F *)__this->get_address_of_enumerator_6();
		InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  L_6;
		L_6 = Enumerator_get_Current_mB23665A6F8B7C1F38400C3F98386D8EF12ABD346_inline((Enumerator_tD1ECE103520161C1BD172331D6968D3D184B3B6F *)(Enumerator_tD1ECE103520161C1BD172331D6968D3D184B3B6F *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED )L_6;
		Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * L_7 = (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * L_8 = (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)__this->get_predicate_4();
		InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  L_9 = V_1;
		NullCheck((Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *, InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)L_8, (InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t1AF155D27A4D78882342D697E4A6DE7CE594545B * L_11 = (Func_2_t1AF155D27A4D78882342D697E4A6DE7CE594545B *)__this->get_selector_5();
		InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  L_12 = V_1;
		NullCheck((Func_2_t1AF155D27A4D78882342D697E4A6DE7CE594545B *)L_11);
		InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  L_13;
		L_13 = ((  InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  (*) (Func_2_t1AF155D27A4D78882342D697E4A6DE7CE594545B *, InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t1AF155D27A4D78882342D697E4A6DE7CE594545B *)L_11, (InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_tD1ECE103520161C1BD172331D6968D3D184B3B6F * L_14 = (Enumerator_tD1ECE103520161C1BD172331D6968D3D184B3B6F *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_mC755E973A650FFFB74A2F3E332EC10C3F035F15D((Enumerator_tD1ECE103520161C1BD172331D6968D3D184B3B6F *)(Enumerator_tD1ECE103520161C1BD172331D6968D3D184B3B6F *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		VirtualActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<UnityEngine.InputSystem.Utilities.InternedString>::Dispose() */, (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.InternedString,UnityEngine.InputSystem.Utilities.InternedString>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_mA77EA2D4A60F27ADDF82BF8101991380F9630CCD_gshared (WhereSelectListIterator_2_t7BDF9DF4ED7F42E4D8139C4BA35A0F055024799E * __this, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B * L_1 = (WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B *, RuntimeObject*, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.InternedString,System.Object>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_m7715111FC63E80CCD4A9665511927799E0D6455B_gshared (WhereSelectListIterator_2_t1C93CE1377887193667292BEED953C31167E2F75 * __this, List_1_tFD37D052760A236906215AF581C58EE13DE6A5DC * ___source0, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * ___predicate1, Func_2_tA2BD34BB8D97AD282016BD4CB5B8675E4AF85B45 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_tFD37D052760A236906215AF581C58EE13DE6A5DC * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_tA2BD34BB8D97AD282016BD4CB5B8675E4AF85B45 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.InternedString,System.Object>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 * WhereSelectListIterator_2_Clone_m31B2BBD6C113AC480DD42AF590FFA24A50DB7C42_gshared (WhereSelectListIterator_2_t1C93CE1377887193667292BEED953C31167E2F75 * __this, const RuntimeMethod* method)
{
	{
		List_1_tFD37D052760A236906215AF581C58EE13DE6A5DC * L_0 = (List_1_tFD37D052760A236906215AF581C58EE13DE6A5DC *)__this->get_source_3();
		Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * L_1 = (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)__this->get_predicate_4();
		Func_2_tA2BD34BB8D97AD282016BD4CB5B8675E4AF85B45 * L_2 = (Func_2_tA2BD34BB8D97AD282016BD4CB5B8675E4AF85B45 *)__this->get_selector_5();
		WhereSelectListIterator_2_t1C93CE1377887193667292BEED953C31167E2F75 * L_3 = (WhereSelectListIterator_2_t1C93CE1377887193667292BEED953C31167E2F75 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_t1C93CE1377887193667292BEED953C31167E2F75 *, List_1_tFD37D052760A236906215AF581C58EE13DE6A5DC *, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *, Func_2_tA2BD34BB8D97AD282016BD4CB5B8675E4AF85B45 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_tFD37D052760A236906215AF581C58EE13DE6A5DC *)L_0, (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)L_1, (Func_2_tA2BD34BB8D97AD282016BD4CB5B8675E4AF85B45 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.InternedString,System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_m05A521A2DB97CA5ED55BC44A411E2C680D0FD34C_gshared (WhereSelectListIterator_2_t1C93CE1377887193667292BEED953C31167E2F75 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_tFD37D052760A236906215AF581C58EE13DE6A5DC * L_3 = (List_1_tFD37D052760A236906215AF581C58EE13DE6A5DC *)__this->get_source_3();
		NullCheck((List_1_tFD37D052760A236906215AF581C58EE13DE6A5DC *)L_3);
		Enumerator_tD1ECE103520161C1BD172331D6968D3D184B3B6F  L_4;
		L_4 = ((  Enumerator_tD1ECE103520161C1BD172331D6968D3D184B3B6F  (*) (List_1_tFD37D052760A236906215AF581C58EE13DE6A5DC *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_tFD37D052760A236906215AF581C58EE13DE6A5DC *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_tD1ECE103520161C1BD172331D6968D3D184B3B6F * L_5 = (Enumerator_tD1ECE103520161C1BD172331D6968D3D184B3B6F *)__this->get_address_of_enumerator_6();
		InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  L_6;
		L_6 = Enumerator_get_Current_mB23665A6F8B7C1F38400C3F98386D8EF12ABD346_inline((Enumerator_tD1ECE103520161C1BD172331D6968D3D184B3B6F *)(Enumerator_tD1ECE103520161C1BD172331D6968D3D184B3B6F *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED )L_6;
		Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * L_7 = (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * L_8 = (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)__this->get_predicate_4();
		InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  L_9 = V_1;
		NullCheck((Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *, InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)L_8, (InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_tA2BD34BB8D97AD282016BD4CB5B8675E4AF85B45 * L_11 = (Func_2_tA2BD34BB8D97AD282016BD4CB5B8675E4AF85B45 *)__this->get_selector_5();
		InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  L_12 = V_1;
		NullCheck((Func_2_tA2BD34BB8D97AD282016BD4CB5B8675E4AF85B45 *)L_11);
		RuntimeObject * L_13;
		L_13 = ((  RuntimeObject * (*) (Func_2_tA2BD34BB8D97AD282016BD4CB5B8675E4AF85B45 *, InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_tA2BD34BB8D97AD282016BD4CB5B8675E4AF85B45 *)L_11, (InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_tD1ECE103520161C1BD172331D6968D3D184B3B6F * L_14 = (Enumerator_tD1ECE103520161C1BD172331D6968D3D184B3B6F *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_mC755E973A650FFFB74A2F3E332EC10C3F035F15D((Enumerator_tD1ECE103520161C1BD172331D6968D3D184B3B6F *)(Enumerator_tD1ECE103520161C1BD172331D6968D3D184B3B6F *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		VirtualActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Object>::Dispose() */, (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.InternedString,System.Object>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_mB2BEEA81CA1213EB80D0D112F05758A045F08736_gshared (WhereSelectListIterator_2_t1C93CE1377887193667292BEED953C31167E2F75 * __this, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 * L_1 = (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *, RuntimeObject*, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,UnityEngine.InputSystem.Utilities.InternedString>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_mA1AD2FE29B73C6CA5B5F0000F42DE0F3A7F2D310_gshared (WhereSelectListIterator_2_t97CD9F77D3626E616F5290B44E8448C47A64E44C * __this, List_1_tCBF5EB99F2E538DCB47A8C835DD4B9C024879393 * ___source0, Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 * ___predicate1, Func_2_tEA2235809AB0E1AB8425745179691D76B519B0C5 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		((  void (*) (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_tCBF5EB99F2E538DCB47A8C835DD4B9C024879393 * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_tEA2235809AB0E1AB8425745179691D76B519B0C5 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,UnityEngine.InputSystem.Utilities.InternedString>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 * WhereSelectListIterator_2_Clone_m16B1D0F26FB37576543BE4994CA98A8484B7D3F2_gshared (WhereSelectListIterator_2_t97CD9F77D3626E616F5290B44E8448C47A64E44C * __this, const RuntimeMethod* method)
{
	{
		List_1_tCBF5EB99F2E538DCB47A8C835DD4B9C024879393 * L_0 = (List_1_tCBF5EB99F2E538DCB47A8C835DD4B9C024879393 *)__this->get_source_3();
		Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 * L_1 = (Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 *)__this->get_predicate_4();
		Func_2_tEA2235809AB0E1AB8425745179691D76B519B0C5 * L_2 = (Func_2_tEA2235809AB0E1AB8425745179691D76B519B0C5 *)__this->get_selector_5();
		WhereSelectListIterator_2_t97CD9F77D3626E616F5290B44E8448C47A64E44C * L_3 = (WhereSelectListIterator_2_t97CD9F77D3626E616F5290B44E8448C47A64E44C *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_t97CD9F77D3626E616F5290B44E8448C47A64E44C *, List_1_tCBF5EB99F2E538DCB47A8C835DD4B9C024879393 *, Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 *, Func_2_tEA2235809AB0E1AB8425745179691D76B519B0C5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_tCBF5EB99F2E538DCB47A8C835DD4B9C024879393 *)L_0, (Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 *)L_1, (Func_2_tEA2235809AB0E1AB8425745179691D76B519B0C5 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,UnityEngine.InputSystem.Utilities.InternedString>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_m4777D4A5C41C8DE4E39F25E670BB727771649BBC_gshared (WhereSelectListIterator_2_t97CD9F77D3626E616F5290B44E8448C47A64E44C * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_tCBF5EB99F2E538DCB47A8C835DD4B9C024879393 * L_3 = (List_1_tCBF5EB99F2E538DCB47A8C835DD4B9C024879393 *)__this->get_source_3();
		NullCheck((List_1_tCBF5EB99F2E538DCB47A8C835DD4B9C024879393 *)L_3);
		Enumerator_tA2E0F83E15EA46A2B4B08AFB750700C6E1387FC0  L_4;
		L_4 = ((  Enumerator_tA2E0F83E15EA46A2B4B08AFB750700C6E1387FC0  (*) (List_1_tCBF5EB99F2E538DCB47A8C835DD4B9C024879393 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_tCBF5EB99F2E538DCB47A8C835DD4B9C024879393 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_tA2E0F83E15EA46A2B4B08AFB750700C6E1387FC0 * L_5 = (Enumerator_tA2E0F83E15EA46A2B4B08AFB750700C6E1387FC0 *)__this->get_address_of_enumerator_6();
		NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA  L_6;
		L_6 = Enumerator_get_Current_m95785F9B18C857920D970C981C5DD0A8B51E8A77_inline((Enumerator_tA2E0F83E15EA46A2B4B08AFB750700C6E1387FC0 *)(Enumerator_tA2E0F83E15EA46A2B4B08AFB750700C6E1387FC0 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA )L_6;
		Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 * L_7 = (Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 * L_8 = (Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 *)__this->get_predicate_4();
		NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA  L_9 = V_1;
		NullCheck((Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 *, NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 *)L_8, (NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_tEA2235809AB0E1AB8425745179691D76B519B0C5 * L_11 = (Func_2_tEA2235809AB0E1AB8425745179691D76B519B0C5 *)__this->get_selector_5();
		NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA  L_12 = V_1;
		NullCheck((Func_2_tEA2235809AB0E1AB8425745179691D76B519B0C5 *)L_11);
		InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  L_13;
		L_13 = ((  InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  (*) (Func_2_tEA2235809AB0E1AB8425745179691D76B519B0C5 *, NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_tEA2235809AB0E1AB8425745179691D76B519B0C5 *)L_11, (NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_tA2E0F83E15EA46A2B4B08AFB750700C6E1387FC0 * L_14 = (Enumerator_tA2E0F83E15EA46A2B4B08AFB750700C6E1387FC0 *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_mB1DCA082AFD65B868138F31F1604376EB2580343((Enumerator_tA2E0F83E15EA46A2B4B08AFB750700C6E1387FC0 *)(Enumerator_tA2E0F83E15EA46A2B4B08AFB750700C6E1387FC0 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		VirtualActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<UnityEngine.InputSystem.Utilities.InternedString>::Dispose() */, (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,UnityEngine.InputSystem.Utilities.InternedString>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_m82467D44FC7DE50D596021E69208E26F38B90B0D_gshared (WhereSelectListIterator_2_t97CD9F77D3626E616F5290B44E8448C47A64E44C * __this, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B * L_1 = (WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B *, RuntimeObject*, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,System.Object>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_m03D061CE7654047BBA8EBECD533D6137B6CF6BF7_gshared (WhereSelectListIterator_2_tDFC8E5CA0DE13C88A58AD7BCD17F1FC274A3F95D * __this, List_1_tCBF5EB99F2E538DCB47A8C835DD4B9C024879393 * ___source0, Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 * ___predicate1, Func_2_tF7304610FB007335AC870B51C8FE2092628DC0DB * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_tCBF5EB99F2E538DCB47A8C835DD4B9C024879393 * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_tF7304610FB007335AC870B51C8FE2092628DC0DB * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,System.Object>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 * WhereSelectListIterator_2_Clone_mE0858FB02DBB37B64D4DBF62DAEC4F460B925314_gshared (WhereSelectListIterator_2_tDFC8E5CA0DE13C88A58AD7BCD17F1FC274A3F95D * __this, const RuntimeMethod* method)
{
	{
		List_1_tCBF5EB99F2E538DCB47A8C835DD4B9C024879393 * L_0 = (List_1_tCBF5EB99F2E538DCB47A8C835DD4B9C024879393 *)__this->get_source_3();
		Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 * L_1 = (Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 *)__this->get_predicate_4();
		Func_2_tF7304610FB007335AC870B51C8FE2092628DC0DB * L_2 = (Func_2_tF7304610FB007335AC870B51C8FE2092628DC0DB *)__this->get_selector_5();
		WhereSelectListIterator_2_tDFC8E5CA0DE13C88A58AD7BCD17F1FC274A3F95D * L_3 = (WhereSelectListIterator_2_tDFC8E5CA0DE13C88A58AD7BCD17F1FC274A3F95D *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_tDFC8E5CA0DE13C88A58AD7BCD17F1FC274A3F95D *, List_1_tCBF5EB99F2E538DCB47A8C835DD4B9C024879393 *, Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 *, Func_2_tF7304610FB007335AC870B51C8FE2092628DC0DB *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_tCBF5EB99F2E538DCB47A8C835DD4B9C024879393 *)L_0, (Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 *)L_1, (Func_2_tF7304610FB007335AC870B51C8FE2092628DC0DB *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_mE0B90B840655F15B6B0C666C9499E597EF53B2B8_gshared (WhereSelectListIterator_2_tDFC8E5CA0DE13C88A58AD7BCD17F1FC274A3F95D * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_tCBF5EB99F2E538DCB47A8C835DD4B9C024879393 * L_3 = (List_1_tCBF5EB99F2E538DCB47A8C835DD4B9C024879393 *)__this->get_source_3();
		NullCheck((List_1_tCBF5EB99F2E538DCB47A8C835DD4B9C024879393 *)L_3);
		Enumerator_tA2E0F83E15EA46A2B4B08AFB750700C6E1387FC0  L_4;
		L_4 = ((  Enumerator_tA2E0F83E15EA46A2B4B08AFB750700C6E1387FC0  (*) (List_1_tCBF5EB99F2E538DCB47A8C835DD4B9C024879393 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_tCBF5EB99F2E538DCB47A8C835DD4B9C024879393 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_tA2E0F83E15EA46A2B4B08AFB750700C6E1387FC0 * L_5 = (Enumerator_tA2E0F83E15EA46A2B4B08AFB750700C6E1387FC0 *)__this->get_address_of_enumerator_6();
		NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA  L_6;
		L_6 = Enumerator_get_Current_m95785F9B18C857920D970C981C5DD0A8B51E8A77_inline((Enumerator_tA2E0F83E15EA46A2B4B08AFB750700C6E1387FC0 *)(Enumerator_tA2E0F83E15EA46A2B4B08AFB750700C6E1387FC0 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA )L_6;
		Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 * L_7 = (Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 * L_8 = (Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 *)__this->get_predicate_4();
		NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA  L_9 = V_1;
		NullCheck((Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 *, NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t90DF477EE61FD14286F8DF1EFED1C515C1109C02 *)L_8, (NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_tF7304610FB007335AC870B51C8FE2092628DC0DB * L_11 = (Func_2_tF7304610FB007335AC870B51C8FE2092628DC0DB *)__this->get_selector_5();
		NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA  L_12 = V_1;
		NullCheck((Func_2_tF7304610FB007335AC870B51C8FE2092628DC0DB *)L_11);
		RuntimeObject * L_13;
		L_13 = ((  RuntimeObject * (*) (Func_2_tF7304610FB007335AC870B51C8FE2092628DC0DB *, NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_tF7304610FB007335AC870B51C8FE2092628DC0DB *)L_11, (NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_tA2E0F83E15EA46A2B4B08AFB750700C6E1387FC0 * L_14 = (Enumerator_tA2E0F83E15EA46A2B4B08AFB750700C6E1387FC0 *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_mB1DCA082AFD65B868138F31F1604376EB2580343((Enumerator_tA2E0F83E15EA46A2B4B08AFB750700C6E1387FC0 *)(Enumerator_tA2E0F83E15EA46A2B4B08AFB750700C6E1387FC0 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		VirtualActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Object>::Dispose() */, (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,System.Object>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_m067C1148FAC31F37585CF8851B7365A195F7654B_gshared (WhereSelectListIterator_2_tDFC8E5CA0DE13C88A58AD7BCD17F1FC274A3F95D * __this, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 * L_1 = (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *, RuntimeObject*, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,UnityEngine.InputSystem.Utilities.InternedString>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_m5D2A38A90A4C8560D5B74EE1FD2798C3F69104EB_gshared (WhereSelectListIterator_2_t84216899116C33A67088960F25CA2D6E1DDC786D * __this, List_1_tDC9592305A8D24F0DBC4A23F1719292FCB2887C1 * ___source0, Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 * ___predicate1, Func_2_tA21678F1D9F9F4617D645755285D28EDE5F9AE44 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		((  void (*) (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_tDC9592305A8D24F0DBC4A23F1719292FCB2887C1 * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_tA21678F1D9F9F4617D645755285D28EDE5F9AE44 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,UnityEngine.InputSystem.Utilities.InternedString>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 * WhereSelectListIterator_2_Clone_m0C6784F9A83C7CBB7E839459EAC2158A61063554_gshared (WhereSelectListIterator_2_t84216899116C33A67088960F25CA2D6E1DDC786D * __this, const RuntimeMethod* method)
{
	{
		List_1_tDC9592305A8D24F0DBC4A23F1719292FCB2887C1 * L_0 = (List_1_tDC9592305A8D24F0DBC4A23F1719292FCB2887C1 *)__this->get_source_3();
		Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 * L_1 = (Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 *)__this->get_predicate_4();
		Func_2_tA21678F1D9F9F4617D645755285D28EDE5F9AE44 * L_2 = (Func_2_tA21678F1D9F9F4617D645755285D28EDE5F9AE44 *)__this->get_selector_5();
		WhereSelectListIterator_2_t84216899116C33A67088960F25CA2D6E1DDC786D * L_3 = (WhereSelectListIterator_2_t84216899116C33A67088960F25CA2D6E1DDC786D *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_t84216899116C33A67088960F25CA2D6E1DDC786D *, List_1_tDC9592305A8D24F0DBC4A23F1719292FCB2887C1 *, Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 *, Func_2_tA21678F1D9F9F4617D645755285D28EDE5F9AE44 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_tDC9592305A8D24F0DBC4A23F1719292FCB2887C1 *)L_0, (Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 *)L_1, (Func_2_tA21678F1D9F9F4617D645755285D28EDE5F9AE44 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,UnityEngine.InputSystem.Utilities.InternedString>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_m134796012884D59C6F5206A56BFD1CE69A5D87E9_gshared (WhereSelectListIterator_2_t84216899116C33A67088960F25CA2D6E1DDC786D * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_tDC9592305A8D24F0DBC4A23F1719292FCB2887C1 * L_3 = (List_1_tDC9592305A8D24F0DBC4A23F1719292FCB2887C1 *)__this->get_source_3();
		NullCheck((List_1_tDC9592305A8D24F0DBC4A23F1719292FCB2887C1 *)L_3);
		Enumerator_tDEE846E92D2B22781FB09541CB1600E5398D3A20  L_4;
		L_4 = ((  Enumerator_tDEE846E92D2B22781FB09541CB1600E5398D3A20  (*) (List_1_tDC9592305A8D24F0DBC4A23F1719292FCB2887C1 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_tDC9592305A8D24F0DBC4A23F1719292FCB2887C1 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_tDEE846E92D2B22781FB09541CB1600E5398D3A20 * L_5 = (Enumerator_tDEE846E92D2B22781FB09541CB1600E5398D3A20 *)__this->get_address_of_enumerator_6();
		NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077  L_6;
		L_6 = Enumerator_get_Current_m16A2059A468713BFAF3492C3A7226F816CA64F7A_inline((Enumerator_tDEE846E92D2B22781FB09541CB1600E5398D3A20 *)(Enumerator_tDEE846E92D2B22781FB09541CB1600E5398D3A20 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077 )L_6;
		Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 * L_7 = (Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 * L_8 = (Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 *)__this->get_predicate_4();
		NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077  L_9 = V_1;
		NullCheck((Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 *, NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 *)L_8, (NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_tA21678F1D9F9F4617D645755285D28EDE5F9AE44 * L_11 = (Func_2_tA21678F1D9F9F4617D645755285D28EDE5F9AE44 *)__this->get_selector_5();
		NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077  L_12 = V_1;
		NullCheck((Func_2_tA21678F1D9F9F4617D645755285D28EDE5F9AE44 *)L_11);
		InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  L_13;
		L_13 = ((  InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  (*) (Func_2_tA21678F1D9F9F4617D645755285D28EDE5F9AE44 *, NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_tA21678F1D9F9F4617D645755285D28EDE5F9AE44 *)L_11, (NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077 )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_tDEE846E92D2B22781FB09541CB1600E5398D3A20 * L_14 = (Enumerator_tDEE846E92D2B22781FB09541CB1600E5398D3A20 *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_m90AD9DC23572A512C0626351F44778A476DE51B0((Enumerator_tDEE846E92D2B22781FB09541CB1600E5398D3A20 *)(Enumerator_tDEE846E92D2B22781FB09541CB1600E5398D3A20 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		VirtualActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<UnityEngine.InputSystem.Utilities.InternedString>::Dispose() */, (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,UnityEngine.InputSystem.Utilities.InternedString>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_m11C95CBF9A8DB57518EE79217A2BC04BE351F1F1_gshared (WhereSelectListIterator_2_t84216899116C33A67088960F25CA2D6E1DDC786D * __this, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B * L_1 = (WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B *, RuntimeObject*, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,System.Object>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_m0C4FDB1BEBF1D7D0F56BEF11EC0B0FDE511D553C_gshared (WhereSelectListIterator_2_tA4D5B2B639DCB29B4C84236B3E2651F17B522430 * __this, List_1_tDC9592305A8D24F0DBC4A23F1719292FCB2887C1 * ___source0, Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 * ___predicate1, Func_2_t5B101C0B26ADAA3B1A77BC50D51CC42364E5409E * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_tDC9592305A8D24F0DBC4A23F1719292FCB2887C1 * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t5B101C0B26ADAA3B1A77BC50D51CC42364E5409E * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,System.Object>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 * WhereSelectListIterator_2_Clone_mC225200E9318225B5E71181D318B7A2995568DD2_gshared (WhereSelectListIterator_2_tA4D5B2B639DCB29B4C84236B3E2651F17B522430 * __this, const RuntimeMethod* method)
{
	{
		List_1_tDC9592305A8D24F0DBC4A23F1719292FCB2887C1 * L_0 = (List_1_tDC9592305A8D24F0DBC4A23F1719292FCB2887C1 *)__this->get_source_3();
		Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 * L_1 = (Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 *)__this->get_predicate_4();
		Func_2_t5B101C0B26ADAA3B1A77BC50D51CC42364E5409E * L_2 = (Func_2_t5B101C0B26ADAA3B1A77BC50D51CC42364E5409E *)__this->get_selector_5();
		WhereSelectListIterator_2_tA4D5B2B639DCB29B4C84236B3E2651F17B522430 * L_3 = (WhereSelectListIterator_2_tA4D5B2B639DCB29B4C84236B3E2651F17B522430 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_tA4D5B2B639DCB29B4C84236B3E2651F17B522430 *, List_1_tDC9592305A8D24F0DBC4A23F1719292FCB2887C1 *, Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 *, Func_2_t5B101C0B26ADAA3B1A77BC50D51CC42364E5409E *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_tDC9592305A8D24F0DBC4A23F1719292FCB2887C1 *)L_0, (Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 *)L_1, (Func_2_t5B101C0B26ADAA3B1A77BC50D51CC42364E5409E *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_m77A19246B97C7498CBBCBFE79DF6D9F38E285CC8_gshared (WhereSelectListIterator_2_tA4D5B2B639DCB29B4C84236B3E2651F17B522430 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_tDC9592305A8D24F0DBC4A23F1719292FCB2887C1 * L_3 = (List_1_tDC9592305A8D24F0DBC4A23F1719292FCB2887C1 *)__this->get_source_3();
		NullCheck((List_1_tDC9592305A8D24F0DBC4A23F1719292FCB2887C1 *)L_3);
		Enumerator_tDEE846E92D2B22781FB09541CB1600E5398D3A20  L_4;
		L_4 = ((  Enumerator_tDEE846E92D2B22781FB09541CB1600E5398D3A20  (*) (List_1_tDC9592305A8D24F0DBC4A23F1719292FCB2887C1 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_tDC9592305A8D24F0DBC4A23F1719292FCB2887C1 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_tDEE846E92D2B22781FB09541CB1600E5398D3A20 * L_5 = (Enumerator_tDEE846E92D2B22781FB09541CB1600E5398D3A20 *)__this->get_address_of_enumerator_6();
		NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077  L_6;
		L_6 = Enumerator_get_Current_m16A2059A468713BFAF3492C3A7226F816CA64F7A_inline((Enumerator_tDEE846E92D2B22781FB09541CB1600E5398D3A20 *)(Enumerator_tDEE846E92D2B22781FB09541CB1600E5398D3A20 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077 )L_6;
		Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 * L_7 = (Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 * L_8 = (Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 *)__this->get_predicate_4();
		NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077  L_9 = V_1;
		NullCheck((Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 *, NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t8C2CAC818FC3E766C41A2C0B700553B5BB7D94F5 *)L_8, (NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t5B101C0B26ADAA3B1A77BC50D51CC42364E5409E * L_11 = (Func_2_t5B101C0B26ADAA3B1A77BC50D51CC42364E5409E *)__this->get_selector_5();
		NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077  L_12 = V_1;
		NullCheck((Func_2_t5B101C0B26ADAA3B1A77BC50D51CC42364E5409E *)L_11);
		RuntimeObject * L_13;
		L_13 = ((  RuntimeObject * (*) (Func_2_t5B101C0B26ADAA3B1A77BC50D51CC42364E5409E *, NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t5B101C0B26ADAA3B1A77BC50D51CC42364E5409E *)L_11, (NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077 )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_tDEE846E92D2B22781FB09541CB1600E5398D3A20 * L_14 = (Enumerator_tDEE846E92D2B22781FB09541CB1600E5398D3A20 *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_m90AD9DC23572A512C0626351F44778A476DE51B0((Enumerator_tDEE846E92D2B22781FB09541CB1600E5398D3A20 *)(Enumerator_tDEE846E92D2B22781FB09541CB1600E5398D3A20 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		VirtualActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Object>::Dispose() */, (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,System.Object>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_m8B56E14BDF9AB258AA50C2791F5E4F158E594AEA_gshared (WhereSelectListIterator_2_tA4D5B2B639DCB29B4C84236B3E2651F17B522430 * __this, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 * L_1 = (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *, RuntimeObject*, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<System.Object,UnityEngine.InputSystem.Utilities.InternedString>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_mCB15F328FC569BB064AFDC629286FD6CE2739B76_gshared (WhereSelectListIterator_2_t4F7AA31B4289096C5D625263B8BBDA051331BE94 * __this, List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * ___source0, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate1, Func_2_t964229B6ADD2B008268547ED2BC3C090AF218065 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		((  void (*) (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t964229B6ADD2B008268547ED2BC3C090AF218065 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<System.Object,UnityEngine.InputSystem.Utilities.InternedString>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 * WhereSelectListIterator_2_Clone_m50EAA3F8391D3E01FE0BC123EFE6A1D85F1CE38A_gshared (WhereSelectListIterator_2_t4F7AA31B4289096C5D625263B8BBDA051331BE94 * __this, const RuntimeMethod* method)
{
	{
		List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * L_0 = (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *)__this->get_source_3();
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_1 = (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)__this->get_predicate_4();
		Func_2_t964229B6ADD2B008268547ED2BC3C090AF218065 * L_2 = (Func_2_t964229B6ADD2B008268547ED2BC3C090AF218065 *)__this->get_selector_5();
		WhereSelectListIterator_2_t4F7AA31B4289096C5D625263B8BBDA051331BE94 * L_3 = (WhereSelectListIterator_2_t4F7AA31B4289096C5D625263B8BBDA051331BE94 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_t4F7AA31B4289096C5D625263B8BBDA051331BE94 *, List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, Func_2_t964229B6ADD2B008268547ED2BC3C090AF218065 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *)L_0, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_1, (Func_2_t964229B6ADD2B008268547ED2BC3C090AF218065 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<System.Object,UnityEngine.InputSystem.Utilities.InternedString>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_mD6A4B29BF27C22CDE2D2B2CDA66FFD1FDB6E9A3F_gshared (WhereSelectListIterator_2_t4F7AA31B4289096C5D625263B8BBDA051331BE94 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	RuntimeObject * V_1 = NULL;
	{
		int32_t L_0 = (int32_t)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * L_3 = (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *)__this->get_source_3();
		NullCheck((List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *)L_3);
		Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  L_4;
		L_4 = ((  Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  (*) (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * L_5 = (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)__this->get_address_of_enumerator_6();
		RuntimeObject * L_6;
		L_6 = Enumerator_get_Current_m4C91D0E84532DF10C654917487D82CB0AB27693B_inline((Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (RuntimeObject *)L_6;
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_7 = (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_8 = (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)__this->get_predicate_4();
		RuntimeObject * L_9 = V_1;
		NullCheck((Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_8, (RuntimeObject *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t964229B6ADD2B008268547ED2BC3C090AF218065 * L_11 = (Func_2_t964229B6ADD2B008268547ED2BC3C090AF218065 *)__this->get_selector_5();
		RuntimeObject * L_12 = V_1;
		NullCheck((Func_2_t964229B6ADD2B008268547ED2BC3C090AF218065 *)L_11);
		InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  L_13;
		L_13 = ((  InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  (*) (Func_2_t964229B6ADD2B008268547ED2BC3C090AF218065 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t964229B6ADD2B008268547ED2BC3C090AF218065 *)L_11, (RuntimeObject *)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * L_14 = (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_m2E56233762839CE55C67E00AC8DD3D4D3F6C0DF0((Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		VirtualActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<UnityEngine.InputSystem.Utilities.InternedString>::Dispose() */, (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<System.Object,UnityEngine.InputSystem.Utilities.InternedString>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_m881D9174221ECFD779DB75C8750D301A9E0E8016_gshared (WhereSelectListIterator_2_t4F7AA31B4289096C5D625263B8BBDA051331BE94 * __this, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B * L_1 = (WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B *, RuntimeObject*, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<System.Object,System.Object>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_m112383219F3EDEF24E86F820E04818A99081C287_gshared (WhereSelectListIterator_2_t85B78DFF0573BC95A62C79D6088FA39DFEBE1AF2 * __this, List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * ___source0, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate1, Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<System.Object,System.Object>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 * WhereSelectListIterator_2_Clone_m24DA90361859DE2F3EA0C6F1E5F5835712EF36D6_gshared (WhereSelectListIterator_2_t85B78DFF0573BC95A62C79D6088FA39DFEBE1AF2 * __this, const RuntimeMethod* method)
{
	{
		List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * L_0 = (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *)__this->get_source_3();
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_1 = (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)__this->get_predicate_4();
		Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 * L_2 = (Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 *)__this->get_selector_5();
		WhereSelectListIterator_2_t85B78DFF0573BC95A62C79D6088FA39DFEBE1AF2 * L_3 = (WhereSelectListIterator_2_t85B78DFF0573BC95A62C79D6088FA39DFEBE1AF2 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_t85B78DFF0573BC95A62C79D6088FA39DFEBE1AF2 *, List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *)L_0, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_1, (Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<System.Object,System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_mEE0E8B173345B059100E0736D106FFAE0C2D29CA_gshared (WhereSelectListIterator_2_t85B78DFF0573BC95A62C79D6088FA39DFEBE1AF2 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	RuntimeObject * V_1 = NULL;
	{
		int32_t L_0 = (int32_t)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * L_3 = (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *)__this->get_source_3();
		NullCheck((List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *)L_3);
		Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  L_4;
		L_4 = ((  Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  (*) (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * L_5 = (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)__this->get_address_of_enumerator_6();
		RuntimeObject * L_6;
		L_6 = Enumerator_get_Current_m4C91D0E84532DF10C654917487D82CB0AB27693B_inline((Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (RuntimeObject *)L_6;
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_7 = (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_8 = (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)__this->get_predicate_4();
		RuntimeObject * L_9 = V_1;
		NullCheck((Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_8, (RuntimeObject *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 * L_11 = (Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 *)__this->get_selector_5();
		RuntimeObject * L_12 = V_1;
		NullCheck((Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 *)L_11);
		RuntimeObject * L_13;
		L_13 = ((  RuntimeObject * (*) (Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 *)L_11, (RuntimeObject *)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * L_14 = (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_m2E56233762839CE55C67E00AC8DD3D4D3F6C0DF0((Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		VirtualActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Object>::Dispose() */, (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<System.Object,System.Object>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_m068C5F13C6CAFE42F43AF753951CFFC12DFEF2E6_gshared (WhereSelectListIterator_2_t85B78DFF0573BC95A62C79D6088FA39DFEBE1AF2 * __this, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 * L_1 = (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *, RuntimeObject*, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.UIElements.StyleSelectorPart,UnityEngine.InputSystem.Utilities.InternedString>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_mD1D56B2383F0FA1599FD02B0DA422EFB09B47C74_gshared (WhereSelectListIterator_2_tCACD1A54916845C9EA0DA248A5D1197354F1AB99 * __this, List_1_tE7DC2F192C483F9E5722779E3C2C1910490AAE59 * ___source0, Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC * ___predicate1, Func_2_tFBBF89B72C697489D82FBE8FF822B191D2EE9A41 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		((  void (*) (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_tE7DC2F192C483F9E5722779E3C2C1910490AAE59 * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_tFBBF89B72C697489D82FBE8FF822B191D2EE9A41 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.UIElements.StyleSelectorPart,UnityEngine.InputSystem.Utilities.InternedString>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 * WhereSelectListIterator_2_Clone_m03516665355008197C975EC1CFD5F360EB0B2127_gshared (WhereSelectListIterator_2_tCACD1A54916845C9EA0DA248A5D1197354F1AB99 * __this, const RuntimeMethod* method)
{
	{
		List_1_tE7DC2F192C483F9E5722779E3C2C1910490AAE59 * L_0 = (List_1_tE7DC2F192C483F9E5722779E3C2C1910490AAE59 *)__this->get_source_3();
		Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC * L_1 = (Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC *)__this->get_predicate_4();
		Func_2_tFBBF89B72C697489D82FBE8FF822B191D2EE9A41 * L_2 = (Func_2_tFBBF89B72C697489D82FBE8FF822B191D2EE9A41 *)__this->get_selector_5();
		WhereSelectListIterator_2_tCACD1A54916845C9EA0DA248A5D1197354F1AB99 * L_3 = (WhereSelectListIterator_2_tCACD1A54916845C9EA0DA248A5D1197354F1AB99 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_tCACD1A54916845C9EA0DA248A5D1197354F1AB99 *, List_1_tE7DC2F192C483F9E5722779E3C2C1910490AAE59 *, Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC *, Func_2_tFBBF89B72C697489D82FBE8FF822B191D2EE9A41 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_tE7DC2F192C483F9E5722779E3C2C1910490AAE59 *)L_0, (Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC *)L_1, (Func_2_tFBBF89B72C697489D82FBE8FF822B191D2EE9A41 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.UIElements.StyleSelectorPart,UnityEngine.InputSystem.Utilities.InternedString>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_m433F1F18F9CBE1F0A503B81266850919917EC8D2_gshared (WhereSelectListIterator_2_tCACD1A54916845C9EA0DA248A5D1197354F1AB99 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	StyleSelectorPart_t707EDC970FC0F3E91E56DCBC178672A120426D54  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_tE7DC2F192C483F9E5722779E3C2C1910490AAE59 * L_3 = (List_1_tE7DC2F192C483F9E5722779E3C2C1910490AAE59 *)__this->get_source_3();
		NullCheck((List_1_tE7DC2F192C483F9E5722779E3C2C1910490AAE59 *)L_3);
		Enumerator_tC861559498255F6677610CED32D2161012D8D897  L_4;
		L_4 = ((  Enumerator_tC861559498255F6677610CED32D2161012D8D897  (*) (List_1_tE7DC2F192C483F9E5722779E3C2C1910490AAE59 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_tE7DC2F192C483F9E5722779E3C2C1910490AAE59 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_tC861559498255F6677610CED32D2161012D8D897 * L_5 = (Enumerator_tC861559498255F6677610CED32D2161012D8D897 *)__this->get_address_of_enumerator_6();
		StyleSelectorPart_t707EDC970FC0F3E91E56DCBC178672A120426D54  L_6;
		L_6 = Enumerator_get_Current_mEE518BAF9575D30AF8E5D39A599B90BD6B796AC5_inline((Enumerator_tC861559498255F6677610CED32D2161012D8D897 *)(Enumerator_tC861559498255F6677610CED32D2161012D8D897 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (StyleSelectorPart_t707EDC970FC0F3E91E56DCBC178672A120426D54 )L_6;
		Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC * L_7 = (Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC * L_8 = (Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC *)__this->get_predicate_4();
		StyleSelectorPart_t707EDC970FC0F3E91E56DCBC178672A120426D54  L_9 = V_1;
		NullCheck((Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC *, StyleSelectorPart_t707EDC970FC0F3E91E56DCBC178672A120426D54 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC *)L_8, (StyleSelectorPart_t707EDC970FC0F3E91E56DCBC178672A120426D54 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_tFBBF89B72C697489D82FBE8FF822B191D2EE9A41 * L_11 = (Func_2_tFBBF89B72C697489D82FBE8FF822B191D2EE9A41 *)__this->get_selector_5();
		StyleSelectorPart_t707EDC970FC0F3E91E56DCBC178672A120426D54  L_12 = V_1;
		NullCheck((Func_2_tFBBF89B72C697489D82FBE8FF822B191D2EE9A41 *)L_11);
		InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  L_13;
		L_13 = ((  InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  (*) (Func_2_tFBBF89B72C697489D82FBE8FF822B191D2EE9A41 *, StyleSelectorPart_t707EDC970FC0F3E91E56DCBC178672A120426D54 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_tFBBF89B72C697489D82FBE8FF822B191D2EE9A41 *)L_11, (StyleSelectorPart_t707EDC970FC0F3E91E56DCBC178672A120426D54 )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_tC861559498255F6677610CED32D2161012D8D897 * L_14 = (Enumerator_tC861559498255F6677610CED32D2161012D8D897 *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_mC5D34E64D92EABA8FE6CD4D97FF07A35F7F0DF25((Enumerator_tC861559498255F6677610CED32D2161012D8D897 *)(Enumerator_tC861559498255F6677610CED32D2161012D8D897 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		VirtualActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<UnityEngine.InputSystem.Utilities.InternedString>::Dispose() */, (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.UIElements.StyleSelectorPart,UnityEngine.InputSystem.Utilities.InternedString>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_m6F8251BD85B61ABE808D5C34799F381CAD31FC60_gshared (WhereSelectListIterator_2_tCACD1A54916845C9EA0DA248A5D1197354F1AB99 * __this, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B * L_1 = (WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B *, RuntimeObject*, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.UIElements.StyleSelectorPart,System.Object>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_m42EC49735B7C7E046D4C40F2356951E7C8DE55E1_gshared (WhereSelectListIterator_2_t0F9C36BA8F331B1C4C968AB639F343F642078798 * __this, List_1_tE7DC2F192C483F9E5722779E3C2C1910490AAE59 * ___source0, Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC * ___predicate1, Func_2_t43979C5A872BA9ED7A923D533A01683A98D71957 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_tE7DC2F192C483F9E5722779E3C2C1910490AAE59 * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t43979C5A872BA9ED7A923D533A01683A98D71957 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.UIElements.StyleSelectorPart,System.Object>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 * WhereSelectListIterator_2_Clone_m948D6D8640F7ECBA7E181A5E6428ED75B720A1B8_gshared (WhereSelectListIterator_2_t0F9C36BA8F331B1C4C968AB639F343F642078798 * __this, const RuntimeMethod* method)
{
	{
		List_1_tE7DC2F192C483F9E5722779E3C2C1910490AAE59 * L_0 = (List_1_tE7DC2F192C483F9E5722779E3C2C1910490AAE59 *)__this->get_source_3();
		Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC * L_1 = (Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC *)__this->get_predicate_4();
		Func_2_t43979C5A872BA9ED7A923D533A01683A98D71957 * L_2 = (Func_2_t43979C5A872BA9ED7A923D533A01683A98D71957 *)__this->get_selector_5();
		WhereSelectListIterator_2_t0F9C36BA8F331B1C4C968AB639F343F642078798 * L_3 = (WhereSelectListIterator_2_t0F9C36BA8F331B1C4C968AB639F343F642078798 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_t0F9C36BA8F331B1C4C968AB639F343F642078798 *, List_1_tE7DC2F192C483F9E5722779E3C2C1910490AAE59 *, Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC *, Func_2_t43979C5A872BA9ED7A923D533A01683A98D71957 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_tE7DC2F192C483F9E5722779E3C2C1910490AAE59 *)L_0, (Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC *)L_1, (Func_2_t43979C5A872BA9ED7A923D533A01683A98D71957 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.UIElements.StyleSelectorPart,System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_m0A73EF53DDB09F1752547E23BDC8C82C057D7D42_gshared (WhereSelectListIterator_2_t0F9C36BA8F331B1C4C968AB639F343F642078798 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	StyleSelectorPart_t707EDC970FC0F3E91E56DCBC178672A120426D54  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_tE7DC2F192C483F9E5722779E3C2C1910490AAE59 * L_3 = (List_1_tE7DC2F192C483F9E5722779E3C2C1910490AAE59 *)__this->get_source_3();
		NullCheck((List_1_tE7DC2F192C483F9E5722779E3C2C1910490AAE59 *)L_3);
		Enumerator_tC861559498255F6677610CED32D2161012D8D897  L_4;
		L_4 = ((  Enumerator_tC861559498255F6677610CED32D2161012D8D897  (*) (List_1_tE7DC2F192C483F9E5722779E3C2C1910490AAE59 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_tE7DC2F192C483F9E5722779E3C2C1910490AAE59 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_tC861559498255F6677610CED32D2161012D8D897 * L_5 = (Enumerator_tC861559498255F6677610CED32D2161012D8D897 *)__this->get_address_of_enumerator_6();
		StyleSelectorPart_t707EDC970FC0F3E91E56DCBC178672A120426D54  L_6;
		L_6 = Enumerator_get_Current_mEE518BAF9575D30AF8E5D39A599B90BD6B796AC5_inline((Enumerator_tC861559498255F6677610CED32D2161012D8D897 *)(Enumerator_tC861559498255F6677610CED32D2161012D8D897 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (StyleSelectorPart_t707EDC970FC0F3E91E56DCBC178672A120426D54 )L_6;
		Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC * L_7 = (Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC * L_8 = (Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC *)__this->get_predicate_4();
		StyleSelectorPart_t707EDC970FC0F3E91E56DCBC178672A120426D54  L_9 = V_1;
		NullCheck((Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC *, StyleSelectorPart_t707EDC970FC0F3E91E56DCBC178672A120426D54 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t0A3D12C95DB15867D9D7D83394FF45A41B67FDAC *)L_8, (StyleSelectorPart_t707EDC970FC0F3E91E56DCBC178672A120426D54 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t43979C5A872BA9ED7A923D533A01683A98D71957 * L_11 = (Func_2_t43979C5A872BA9ED7A923D533A01683A98D71957 *)__this->get_selector_5();
		StyleSelectorPart_t707EDC970FC0F3E91E56DCBC178672A120426D54  L_12 = V_1;
		NullCheck((Func_2_t43979C5A872BA9ED7A923D533A01683A98D71957 *)L_11);
		RuntimeObject * L_13;
		L_13 = ((  RuntimeObject * (*) (Func_2_t43979C5A872BA9ED7A923D533A01683A98D71957 *, StyleSelectorPart_t707EDC970FC0F3E91E56DCBC178672A120426D54 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t43979C5A872BA9ED7A923D533A01683A98D71957 *)L_11, (StyleSelectorPart_t707EDC970FC0F3E91E56DCBC178672A120426D54 )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_tC861559498255F6677610CED32D2161012D8D897 * L_14 = (Enumerator_tC861559498255F6677610CED32D2161012D8D897 *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_mC5D34E64D92EABA8FE6CD4D97FF07A35F7F0DF25((Enumerator_tC861559498255F6677610CED32D2161012D8D897 *)(Enumerator_tC861559498255F6677610CED32D2161012D8D897 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		VirtualActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Object>::Dispose() */, (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.UIElements.StyleSelectorPart,System.Object>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_m6B81858AD8958A42F481DD06A8844FC2B67AF736_gshared (WhereSelectListIterator_2_t0F9C36BA8F331B1C4C968AB639F343F642078798 * __this, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 * L_1 = (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *, RuntimeObject*, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.Substring,UnityEngine.InputSystem.Utilities.InternedString>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_mF1ED890C9F52C6E3EEF9D7C42FB5E94D3B65C60A_gshared (WhereSelectListIterator_2_t59101674E6772D817227A6D5C222AF34A4E0BBAD * __this, List_1_tEAD6E3282E028927B32F56E7892994D90D512467 * ___source0, Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 * ___predicate1, Func_2_t14F6F4D7FBF558A20782EDC48D80473E4800048E * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		((  void (*) (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_tEAD6E3282E028927B32F56E7892994D90D512467 * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t14F6F4D7FBF558A20782EDC48D80473E4800048E * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.Substring,UnityEngine.InputSystem.Utilities.InternedString>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 * WhereSelectListIterator_2_Clone_m345CD8E45C54CCC480CC4E9FFE80C11AE3840EFA_gshared (WhereSelectListIterator_2_t59101674E6772D817227A6D5C222AF34A4E0BBAD * __this, const RuntimeMethod* method)
{
	{
		List_1_tEAD6E3282E028927B32F56E7892994D90D512467 * L_0 = (List_1_tEAD6E3282E028927B32F56E7892994D90D512467 *)__this->get_source_3();
		Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 * L_1 = (Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 *)__this->get_predicate_4();
		Func_2_t14F6F4D7FBF558A20782EDC48D80473E4800048E * L_2 = (Func_2_t14F6F4D7FBF558A20782EDC48D80473E4800048E *)__this->get_selector_5();
		WhereSelectListIterator_2_t59101674E6772D817227A6D5C222AF34A4E0BBAD * L_3 = (WhereSelectListIterator_2_t59101674E6772D817227A6D5C222AF34A4E0BBAD *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_t59101674E6772D817227A6D5C222AF34A4E0BBAD *, List_1_tEAD6E3282E028927B32F56E7892994D90D512467 *, Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 *, Func_2_t14F6F4D7FBF558A20782EDC48D80473E4800048E *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_tEAD6E3282E028927B32F56E7892994D90D512467 *)L_0, (Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 *)L_1, (Func_2_t14F6F4D7FBF558A20782EDC48D80473E4800048E *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.Substring,UnityEngine.InputSystem.Utilities.InternedString>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_mD5555094E5DCBC5500AB258020155C83DF223930_gshared (WhereSelectListIterator_2_t59101674E6772D817227A6D5C222AF34A4E0BBAD * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_tEAD6E3282E028927B32F56E7892994D90D512467 * L_3 = (List_1_tEAD6E3282E028927B32F56E7892994D90D512467 *)__this->get_source_3();
		NullCheck((List_1_tEAD6E3282E028927B32F56E7892994D90D512467 *)L_3);
		Enumerator_t7C514ACB3FF8E3EFB210F5F45C9F18A715BB4F80  L_4;
		L_4 = ((  Enumerator_t7C514ACB3FF8E3EFB210F5F45C9F18A715BB4F80  (*) (List_1_tEAD6E3282E028927B32F56E7892994D90D512467 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_tEAD6E3282E028927B32F56E7892994D90D512467 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_t7C514ACB3FF8E3EFB210F5F45C9F18A715BB4F80 * L_5 = (Enumerator_t7C514ACB3FF8E3EFB210F5F45C9F18A715BB4F80 *)__this->get_address_of_enumerator_6();
		Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F  L_6;
		L_6 = Enumerator_get_Current_mF0F69D342F9662149C2D0051C758AC18AEBFFB23_inline((Enumerator_t7C514ACB3FF8E3EFB210F5F45C9F18A715BB4F80 *)(Enumerator_t7C514ACB3FF8E3EFB210F5F45C9F18A715BB4F80 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F )L_6;
		Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 * L_7 = (Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 * L_8 = (Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 *)__this->get_predicate_4();
		Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F  L_9 = V_1;
		NullCheck((Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 *, Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 *)L_8, (Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t14F6F4D7FBF558A20782EDC48D80473E4800048E * L_11 = (Func_2_t14F6F4D7FBF558A20782EDC48D80473E4800048E *)__this->get_selector_5();
		Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F  L_12 = V_1;
		NullCheck((Func_2_t14F6F4D7FBF558A20782EDC48D80473E4800048E *)L_11);
		InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  L_13;
		L_13 = ((  InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  (*) (Func_2_t14F6F4D7FBF558A20782EDC48D80473E4800048E *, Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t14F6F4D7FBF558A20782EDC48D80473E4800048E *)L_11, (Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_t7C514ACB3FF8E3EFB210F5F45C9F18A715BB4F80 * L_14 = (Enumerator_t7C514ACB3FF8E3EFB210F5F45C9F18A715BB4F80 *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_mC5829956B29271280A19B86852D365AA6497AB41((Enumerator_t7C514ACB3FF8E3EFB210F5F45C9F18A715BB4F80 *)(Enumerator_t7C514ACB3FF8E3EFB210F5F45C9F18A715BB4F80 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		VirtualActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<UnityEngine.InputSystem.Utilities.InternedString>::Dispose() */, (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.Substring,UnityEngine.InputSystem.Utilities.InternedString>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_m3D19A98D756928EB9914053ADB492549F85C8EED_gshared (WhereSelectListIterator_2_t59101674E6772D817227A6D5C222AF34A4E0BBAD * __this, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B * L_1 = (WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B *, RuntimeObject*, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.Substring,System.Object>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_m9851A6769481D7E21DBA32F17CFBC7EBD871348E_gshared (WhereSelectListIterator_2_t0CEFF5458EED043DC70F7CC9B97B5AB4D45324EB * __this, List_1_tEAD6E3282E028927B32F56E7892994D90D512467 * ___source0, Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 * ___predicate1, Func_2_t4EC2F095EDDC8079537CA0FEDC463635E599C9E4 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_tEAD6E3282E028927B32F56E7892994D90D512467 * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t4EC2F095EDDC8079537CA0FEDC463635E599C9E4 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.Substring,System.Object>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 * WhereSelectListIterator_2_Clone_mCCB6674E3EF065A64E9951E80464B5509588F028_gshared (WhereSelectListIterator_2_t0CEFF5458EED043DC70F7CC9B97B5AB4D45324EB * __this, const RuntimeMethod* method)
{
	{
		List_1_tEAD6E3282E028927B32F56E7892994D90D512467 * L_0 = (List_1_tEAD6E3282E028927B32F56E7892994D90D512467 *)__this->get_source_3();
		Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 * L_1 = (Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 *)__this->get_predicate_4();
		Func_2_t4EC2F095EDDC8079537CA0FEDC463635E599C9E4 * L_2 = (Func_2_t4EC2F095EDDC8079537CA0FEDC463635E599C9E4 *)__this->get_selector_5();
		WhereSelectListIterator_2_t0CEFF5458EED043DC70F7CC9B97B5AB4D45324EB * L_3 = (WhereSelectListIterator_2_t0CEFF5458EED043DC70F7CC9B97B5AB4D45324EB *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_t0CEFF5458EED043DC70F7CC9B97B5AB4D45324EB *, List_1_tEAD6E3282E028927B32F56E7892994D90D512467 *, Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 *, Func_2_t4EC2F095EDDC8079537CA0FEDC463635E599C9E4 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_tEAD6E3282E028927B32F56E7892994D90D512467 *)L_0, (Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 *)L_1, (Func_2_t4EC2F095EDDC8079537CA0FEDC463635E599C9E4 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.Substring,System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_m972DF209FA452810F4297BB14A1FCE26068498C2_gshared (WhereSelectListIterator_2_t0CEFF5458EED043DC70F7CC9B97B5AB4D45324EB * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_tEAD6E3282E028927B32F56E7892994D90D512467 * L_3 = (List_1_tEAD6E3282E028927B32F56E7892994D90D512467 *)__this->get_source_3();
		NullCheck((List_1_tEAD6E3282E028927B32F56E7892994D90D512467 *)L_3);
		Enumerator_t7C514ACB3FF8E3EFB210F5F45C9F18A715BB4F80  L_4;
		L_4 = ((  Enumerator_t7C514ACB3FF8E3EFB210F5F45C9F18A715BB4F80  (*) (List_1_tEAD6E3282E028927B32F56E7892994D90D512467 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_tEAD6E3282E028927B32F56E7892994D90D512467 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_t7C514ACB3FF8E3EFB210F5F45C9F18A715BB4F80 * L_5 = (Enumerator_t7C514ACB3FF8E3EFB210F5F45C9F18A715BB4F80 *)__this->get_address_of_enumerator_6();
		Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F  L_6;
		L_6 = Enumerator_get_Current_mF0F69D342F9662149C2D0051C758AC18AEBFFB23_inline((Enumerator_t7C514ACB3FF8E3EFB210F5F45C9F18A715BB4F80 *)(Enumerator_t7C514ACB3FF8E3EFB210F5F45C9F18A715BB4F80 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F )L_6;
		Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 * L_7 = (Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 * L_8 = (Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 *)__this->get_predicate_4();
		Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F  L_9 = V_1;
		NullCheck((Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 *, Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_tABEFD9EE472D61479A311CC97D097C1F3BE97EF9 *)L_8, (Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t4EC2F095EDDC8079537CA0FEDC463635E599C9E4 * L_11 = (Func_2_t4EC2F095EDDC8079537CA0FEDC463635E599C9E4 *)__this->get_selector_5();
		Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F  L_12 = V_1;
		NullCheck((Func_2_t4EC2F095EDDC8079537CA0FEDC463635E599C9E4 *)L_11);
		RuntimeObject * L_13;
		L_13 = ((  RuntimeObject * (*) (Func_2_t4EC2F095EDDC8079537CA0FEDC463635E599C9E4 *, Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t4EC2F095EDDC8079537CA0FEDC463635E599C9E4 *)L_11, (Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_t7C514ACB3FF8E3EFB210F5F45C9F18A715BB4F80 * L_14 = (Enumerator_t7C514ACB3FF8E3EFB210F5F45C9F18A715BB4F80 *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_mC5829956B29271280A19B86852D365AA6497AB41((Enumerator_t7C514ACB3FF8E3EFB210F5F45C9F18A715BB4F80 *)(Enumerator_t7C514ACB3FF8E3EFB210F5F45C9F18A715BB4F80 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		VirtualActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Object>::Dispose() */, (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.Substring,System.Object>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_m6022BBAB43CAB24C060A3D801B5FA693B955CCFD_gshared (WhereSelectListIterator_2_t0CEFF5458EED043DC70F7CC9B97B5AB4D45324EB * __this, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 * L_1 = (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *, RuntimeObject*, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,UnityEngine.InputSystem.Utilities.InternedString>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_m8A0DCD59636C8D6E3BB85EC8AC3DE2D3DCBE6C6A_gshared (WhereSelectListIterator_2_tF2D97D886F080B26C19947C7C6A56563B86B14D7 * __this, List_1_tEC0B302C3EF23400C3EC92ED7672A715CA602068 * ___source0, Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 * ___predicate1, Func_2_t48DD1C8E439811C492BAFE220A679DB57D7C9370 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		((  void (*) (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_tEC0B302C3EF23400C3EC92ED7672A715CA602068 * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t48DD1C8E439811C492BAFE220A679DB57D7C9370 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,UnityEngine.InputSystem.Utilities.InternedString>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 * WhereSelectListIterator_2_Clone_mD7E972ABEA439A6E8ADBCFF8AB3DDF9568465BC1_gshared (WhereSelectListIterator_2_tF2D97D886F080B26C19947C7C6A56563B86B14D7 * __this, const RuntimeMethod* method)
{
	{
		List_1_tEC0B302C3EF23400C3EC92ED7672A715CA602068 * L_0 = (List_1_tEC0B302C3EF23400C3EC92ED7672A715CA602068 *)__this->get_source_3();
		Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 * L_1 = (Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 *)__this->get_predicate_4();
		Func_2_t48DD1C8E439811C492BAFE220A679DB57D7C9370 * L_2 = (Func_2_t48DD1C8E439811C492BAFE220A679DB57D7C9370 *)__this->get_selector_5();
		WhereSelectListIterator_2_tF2D97D886F080B26C19947C7C6A56563B86B14D7 * L_3 = (WhereSelectListIterator_2_tF2D97D886F080B26C19947C7C6A56563B86B14D7 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_tF2D97D886F080B26C19947C7C6A56563B86B14D7 *, List_1_tEC0B302C3EF23400C3EC92ED7672A715CA602068 *, Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 *, Func_2_t48DD1C8E439811C492BAFE220A679DB57D7C9370 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_tEC0B302C3EF23400C3EC92ED7672A715CA602068 *)L_0, (Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 *)L_1, (Func_2_t48DD1C8E439811C492BAFE220A679DB57D7C9370 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,UnityEngine.InputSystem.Utilities.InternedString>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_m384329556CF83AB5951B336E016F47587A4F8758_gshared (WhereSelectListIterator_2_tF2D97D886F080B26C19947C7C6A56563B86B14D7 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_tEC0B302C3EF23400C3EC92ED7672A715CA602068 * L_3 = (List_1_tEC0B302C3EF23400C3EC92ED7672A715CA602068 *)__this->get_source_3();
		NullCheck((List_1_tEC0B302C3EF23400C3EC92ED7672A715CA602068 *)L_3);
		Enumerator_tF2D95CC18A66F3DE682EAC9B88E865750DFAEF60  L_4;
		L_4 = ((  Enumerator_tF2D95CC18A66F3DE682EAC9B88E865750DFAEF60  (*) (List_1_tEC0B302C3EF23400C3EC92ED7672A715CA602068 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_tEC0B302C3EF23400C3EC92ED7672A715CA602068 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_tF2D95CC18A66F3DE682EAC9B88E865750DFAEF60 * L_5 = (Enumerator_tF2D95CC18A66F3DE682EAC9B88E865750DFAEF60 *)__this->get_address_of_enumerator_6();
		JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB  L_6;
		L_6 = Enumerator_get_Current_m51D04AB64A27E7E8D833DAE19D8D7C342794D521_inline((Enumerator_tF2D95CC18A66F3DE682EAC9B88E865750DFAEF60 *)(Enumerator_tF2D95CC18A66F3DE682EAC9B88E865750DFAEF60 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB )L_6;
		Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 * L_7 = (Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 * L_8 = (Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 *)__this->get_predicate_4();
		JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB  L_9 = V_1;
		NullCheck((Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 *, JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 *)L_8, (JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t48DD1C8E439811C492BAFE220A679DB57D7C9370 * L_11 = (Func_2_t48DD1C8E439811C492BAFE220A679DB57D7C9370 *)__this->get_selector_5();
		JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB  L_12 = V_1;
		NullCheck((Func_2_t48DD1C8E439811C492BAFE220A679DB57D7C9370 *)L_11);
		InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  L_13;
		L_13 = ((  InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  (*) (Func_2_t48DD1C8E439811C492BAFE220A679DB57D7C9370 *, JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t48DD1C8E439811C492BAFE220A679DB57D7C9370 *)L_11, (JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_tF2D95CC18A66F3DE682EAC9B88E865750DFAEF60 * L_14 = (Enumerator_tF2D95CC18A66F3DE682EAC9B88E865750DFAEF60 *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_m084C0187069AC47C975802B0EB3F7257DB6F8507((Enumerator_tF2D95CC18A66F3DE682EAC9B88E865750DFAEF60 *)(Enumerator_tF2D95CC18A66F3DE682EAC9B88E865750DFAEF60 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
		VirtualActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<UnityEngine.InputSystem.Utilities.InternedString>::Dispose() */, (Iterator_1_t15923F5F84887B39E738E287948871764DDF85B5 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,UnityEngine.InputSystem.Utilities.InternedString>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_mAEEAAF8BD4212D23901A28088DAAC844A8E21151_gshared (WhereSelectListIterator_2_tF2D97D886F080B26C19947C7C6A56563B86B14D7 * __this, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B * L_1 = (WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t412B4757E48607FF130F2B4B03BFE90E55D75C9B *, RuntimeObject*, Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t7783409D7A11DB3C00435FEB35D43EBA686470B1 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,System.Object>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_mD73AEEBBBBF7D74E2E8D89D4D9F5703CD2FFC249_gshared (WhereSelectListIterator_2_t2F7487B4BF5A9E71CBE06091C45AF7D98CF9F1AC * __this, List_1_tEC0B302C3EF23400C3EC92ED7672A715CA602068 * ___source0, Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 * ___predicate1, Func_2_t1628B2E4EEC7A32D5B688E54415BC42EDA9D2899 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_tEC0B302C3EF23400C3EC92ED7672A715CA602068 * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t1628B2E4EEC7A32D5B688E54415BC42EDA9D2899 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,System.Object>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 * WhereSelectListIterator_2_Clone_m401E398C50A9FED4E32BEC38456421718A2104C7_gshared (WhereSelectListIterator_2_t2F7487B4BF5A9E71CBE06091C45AF7D98CF9F1AC * __this, const RuntimeMethod* method)
{
	{
		List_1_tEC0B302C3EF23400C3EC92ED7672A715CA602068 * L_0 = (List_1_tEC0B302C3EF23400C3EC92ED7672A715CA602068 *)__this->get_source_3();
		Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 * L_1 = (Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 *)__this->get_predicate_4();
		Func_2_t1628B2E4EEC7A32D5B688E54415BC42EDA9D2899 * L_2 = (Func_2_t1628B2E4EEC7A32D5B688E54415BC42EDA9D2899 *)__this->get_selector_5();
		WhereSelectListIterator_2_t2F7487B4BF5A9E71CBE06091C45AF7D98CF9F1AC * L_3 = (WhereSelectListIterator_2_t2F7487B4BF5A9E71CBE06091C45AF7D98CF9F1AC *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_t2F7487B4BF5A9E71CBE06091C45AF7D98CF9F1AC *, List_1_tEC0B302C3EF23400C3EC92ED7672A715CA602068 *, Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 *, Func_2_t1628B2E4EEC7A32D5B688E54415BC42EDA9D2899 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_tEC0B302C3EF23400C3EC92ED7672A715CA602068 *)L_0, (Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 *)L_1, (Func_2_t1628B2E4EEC7A32D5B688E54415BC42EDA9D2899 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_mFC90941FA2CA1C494E34BBDBE9B3AB8F50871AAD_gshared (WhereSelectListIterator_2_t2F7487B4BF5A9E71CBE06091C45AF7D98CF9F1AC * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_tEC0B302C3EF23400C3EC92ED7672A715CA602068 * L_3 = (List_1_tEC0B302C3EF23400C3EC92ED7672A715CA602068 *)__this->get_source_3();
		NullCheck((List_1_tEC0B302C3EF23400C3EC92ED7672A715CA602068 *)L_3);
		Enumerator_tF2D95CC18A66F3DE682EAC9B88E865750DFAEF60  L_4;
		L_4 = ((  Enumerator_tF2D95CC18A66F3DE682EAC9B88E865750DFAEF60  (*) (List_1_tEC0B302C3EF23400C3EC92ED7672A715CA602068 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_tEC0B302C3EF23400C3EC92ED7672A715CA602068 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_tF2D95CC18A66F3DE682EAC9B88E865750DFAEF60 * L_5 = (Enumerator_tF2D95CC18A66F3DE682EAC9B88E865750DFAEF60 *)__this->get_address_of_enumerator_6();
		JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB  L_6;
		L_6 = Enumerator_get_Current_m51D04AB64A27E7E8D833DAE19D8D7C342794D521_inline((Enumerator_tF2D95CC18A66F3DE682EAC9B88E865750DFAEF60 *)(Enumerator_tF2D95CC18A66F3DE682EAC9B88E865750DFAEF60 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB )L_6;
		Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 * L_7 = (Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 * L_8 = (Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 *)__this->get_predicate_4();
		JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB  L_9 = V_1;
		NullCheck((Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 *, JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t6041DAC226B42DAAC44BFD17E89C50AD0A962847 *)L_8, (JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t1628B2E4EEC7A32D5B688E54415BC42EDA9D2899 * L_11 = (Func_2_t1628B2E4EEC7A32D5B688E54415BC42EDA9D2899 *)__this->get_selector_5();
		JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB  L_12 = V_1;
		NullCheck((Func_2_t1628B2E4EEC7A32D5B688E54415BC42EDA9D2899 *)L_11);
		RuntimeObject * L_13;
		L_13 = ((  RuntimeObject * (*) (Func_2_t1628B2E4EEC7A32D5B688E54415BC42EDA9D2899 *, JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t1628B2E4EEC7A32D5B688E54415BC42EDA9D2899 *)L_11, (JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_tF2D95CC18A66F3DE682EAC9B88E865750DFAEF60 * L_14 = (Enumerator_tF2D95CC18A66F3DE682EAC9B88E865750DFAEF60 *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_m084C0187069AC47C975802B0EB3F7257DB6F8507((Enumerator_tF2D95CC18A66F3DE682EAC9B88E865750DFAEF60 *)(Enumerator_tF2D95CC18A66F3DE682EAC9B88E865750DFAEF60 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		VirtualActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Object>::Dispose() */, (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,System.Object>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_m9A674B320EB9177CEDD756B17E4934B1DDC91D38_gshared (WhereSelectListIterator_2_t2F7487B4BF5A9E71CBE06091C45AF7D98CF9F1AC * __this, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 * L_1 = (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *, RuntimeObject*, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject * Enumerator_get_Current_m4C91D0E84532DF10C654917487D82CB0AB27693B_gshared_inline (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_current_3();
		return (RuntimeObject *)L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR ControlItem_t88696CA6FA7E3E6EDBF31BD5A8994D959F583B15  Enumerator_get_Current_m98F1CC86098DFE6B87C86B2C5333FDAAAE66AF69_gshared_inline (Enumerator_tA2E47432D9BF33FCB9EAF5B6C3720221AACA50C1 * __this, const RuntimeMethod* method)
{
	{
		ControlItem_t88696CA6FA7E3E6EDBF31BD5A8994D959F583B15  L_0 = (ControlItem_t88696CA6FA7E3E6EDBF31BD5A8994D959F583B15 )__this->get_current_3();
		return (ControlItem_t88696CA6FA7E3E6EDBF31BD5A8994D959F583B15 )L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184  Enumerator_get_Current_m003BC78F9886D5E4E98734CD70C3997E467EB1EA_gshared_inline (Enumerator_tF2D0EEBD6B861D9127DD6DC36B733FD42070858C * __this, const RuntimeMethod* method)
{
	{
		KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184  L_0 = (KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184 )__this->get_current_3();
		return (KeyValuePair_2_t017C0505BFEB76D462E1367442B1C866DB044184 )L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  Enumerator_get_Current_mB23665A6F8B7C1F38400C3F98386D8EF12ABD346_gshared_inline (Enumerator_tD1ECE103520161C1BD172331D6968D3D184B3B6F * __this, const RuntimeMethod* method)
{
	{
		InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED  L_0 = (InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED )__this->get_current_3();
		return (InternedString_tA8F88C6E35F6EA6E9F774DC4DC7D003E1D6B4EED )L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA  Enumerator_get_Current_m95785F9B18C857920D970C981C5DD0A8B51E8A77_gshared_inline (Enumerator_tA2E0F83E15EA46A2B4B08AFB750700C6E1387FC0 * __this, const RuntimeMethod* method)
{
	{
		NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA  L_0 = (NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA )__this->get_current_3();
		return (NameAndParameters_t028B3A860264D254B13642649D34F0953C3B9DCA )L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077  Enumerator_get_Current_m16A2059A468713BFAF3492C3A7226F816CA64F7A_gshared_inline (Enumerator_tDEE846E92D2B22781FB09541CB1600E5398D3A20 * __this, const RuntimeMethod* method)
{
	{
		NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077  L_0 = (NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077 )__this->get_current_3();
		return (NamedValue_t5AB4CF64C63DEAC2470566CAFCE9571D88D18077 )L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR StyleSelectorPart_t707EDC970FC0F3E91E56DCBC178672A120426D54  Enumerator_get_Current_mEE518BAF9575D30AF8E5D39A599B90BD6B796AC5_gshared_inline (Enumerator_tC861559498255F6677610CED32D2161012D8D897 * __this, const RuntimeMethod* method)
{
	{
		StyleSelectorPart_t707EDC970FC0F3E91E56DCBC178672A120426D54  L_0 = (StyleSelectorPart_t707EDC970FC0F3E91E56DCBC178672A120426D54 )__this->get_current_3();
		return (StyleSelectorPart_t707EDC970FC0F3E91E56DCBC178672A120426D54 )L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F  Enumerator_get_Current_mF0F69D342F9662149C2D0051C758AC18AEBFFB23_gshared_inline (Enumerator_t7C514ACB3FF8E3EFB210F5F45C9F18A715BB4F80 * __this, const RuntimeMethod* method)
{
	{
		Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F  L_0 = (Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F )__this->get_current_3();
		return (Substring_t9EE457D8CFFFB970B2967B31E9D9AB465415571F )L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB  Enumerator_get_Current_m51D04AB64A27E7E8D833DAE19D8D7C342794D521_gshared_inline (Enumerator_tF2D95CC18A66F3DE682EAC9B88E865750DFAEF60 * __this, const RuntimeMethod* method)
{
	{
		JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB  L_0 = (JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB )__this->get_current_3();
		return (JsonValue_t547F68925DBBEF3A29A4E14D26153A04872154DB )L_0;
	}
}
